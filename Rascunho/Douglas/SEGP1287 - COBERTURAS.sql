

 if OBJECT_ID('tempdb..#Sinistro_Principal') IS NOT NULL 
 BEGIN
     DROP TABLE #Sinistro_Principal
 END

  if OBJECT_ID('tempdb..#EstimativaPagamento_Estimativa') IS NOT NULL 
 BEGIN
     DROP TABLE #EstimativaPagamento_Estimativa
 END

   if OBJECT_ID('tempdb..#Estimativa_Evento') IS NOT NULL 
 BEGIN
     DROP TABLE #Estimativa_Evento
 END



 
 

Set NoCount On                                                   
Declare @Tp_Componente_ID as Tinyint                             
Set @Tp_Componente_ID  = 0                                       
select Sinistro_Tb.proposta_id                               
     , Sinistro_Tb.sinistro_id                               
     , Sinistro_Tb.apolice_id                                
     , Sinistro_Tb.sucursal_seguradora_id                    
     , Sinistro_Tb.seguradora_cod_susep                      
     , Sinistro_Tb.ramo_id                                   
     , Sinistro_Tb.cliente_id                                
     , Sinistro_Tb.dt_ocorrencia_sinistro                    
     , sinistro_tb.situacao                                  
     , Proposta_Tb.Produto_ID                                
     , Sinistro_Tb.Moeda_ID                                  
     , Sinistro_Tb.Pgto_Parcial_Co_Seguro                    
  Into #Sinistro_Principal 
  From seguros_db.dbo.sinistro_tb sinistro_tb WITH (NOLOCK)   
  Join Seguros_Db.Dbo.Proposta_Tb Proposta_tb WITH (NOLOCK)   
    on Proposta_Tb.Proposta_ID = Sinistro_Tb.Proposta_ID     
 Where sinistro_id = 82202000021                    
Create table #EstimativaPagamento_Estimativa                                                                                 
     ( Dt_Inicio_Estimativa                          SmallDateTime                                                           
     , Dt_Fim_Estimativa                             SmallDateTime                                                           
     , Item_Val_Estimativa                           TinyInt                                                                 
     , Seq_Estimativa                                TinyInt                                                                 
     , Val_Estimado                                  Numeric(15,2)                                                           
     , Val_Pago                                      Numeric(15,2)                                                           
     , Val_Resseguro                                 Numeric(15,2)                                                           
     , Situacao                                      Char(1)                                                                 
     , Usuario                                       VarChar(20)                                                             
     , Num_Carta_Seguro_Aceito                       Int                                                                     
     , Perc_Re_Seguro_Quota                          Numeric(9,6)                                                            
     , Perc_Re_Seguro_Er                             Numeric(9,6)                                                            
     , Tipo                                          Char(1)                                                                 
     , Nossa_Parte                                   Numeric(9,6)                                                            
     , Dt_Inclusao                                   SmallDateTime                                                           
     , CountEstimativa                               Numeric(3)                                                              
     , Moeda_ID                                      Numeric(3)                                                              
     , Moeda_Sigla                                   VarChar(4)                                                              
     , pgto_parcial_co_seguro                        Char(1)                                                                 
     )                                                                                                                       
Create Index PK_Dt_Inicio_Estimativa             on #EstimativaPagamento_Estimativa (Seq_Estimativa, Dt_Inicio_Estimativa)   

 create table #Estimativa_Evento 
 (dt_evento                  smalldatetime, 
 situacao                    char(1), 
 evento_SEGBR                varchar(250), 
 descricao                   varchar(250), 
 tipo                        char(1), 
 usuario                     varchar(250), 
 seq_evento                  tinyint, 
 localizacao                 tinyint) 

Insert into #EstimativaPagamento_Estimativa                                                                                  
     ( Dt_Inicio_Estimativa                                                                                                  
     , Dt_Fim_Estimativa                                                                                                     
     , Item_Val_Estimativa                                                                                                   
     , Seq_Estimativa                                                                                                        
     , Val_Estimado                                                                                                          
     , Val_Pago                                                                                                              
     , Val_Resseguro                                                                                                         
     , Situacao                                                                                                              
     , Usuario                                                                                                               
     , Num_Carta_Seguro_Aceito                                                                                               
     , Perc_Re_Seguro_Quota                                                                                                  
     , Perc_Re_Seguro_Er                                                                                                     
     , Tipo                                                                                                                  
     , Nossa_Parte                                                                                                           
     , Dt_Inclusao                                                                                                           
     , Moeda_ID                                                                                                              
     , pgto_parcial_co_seguro                                                                                                
     )                                                                                                                       
Select Dt_Inicio_Estimativa                                                                                                  
     , Dt_Fim_Estimativa                                                                                                     
     , Item_Val_Estimativa                                                                                                   
     , Seq_Estimativa                                                                                                        
     , Val_Estimado                                  = IsNull(Val_Estimado,0)                                                
     , Val_Pago                                      = IsNull(Val_Pago,0)                                                    
     , Val_Resseguro                                 = IsNull(Val_Resseguro,0)                                               
     , Situacao                                      = IsNull(Sinistro_Estimativa_Tb.Situacao, '')                           
     , Sinistro_Estimativa_Tb.Usuario                                                                                        
     , Num_Carta_Seguro_Aceito                       = Isnull(Num_Carta_Seguro_Aceito, '')                                   
     , Perc_Re_Seguro_Quota                          = IsNull(Perc_Re_Seguro_Quota,0)                                        
     , Perc_Re_Seguro_Er                             = IsNull(Perc_Re_Seguro_Er,0)                                           
     , Tipo                                          = 'E'                                                                   
     , Nossa_Parte                                   = 0                                                                     
     , Sinistro_Estimativa_Tb.Dt_Inclusao                                                                                    
     , #Sinistro_Principal.Moeda_ID                                                                                          
     , #Sinistro_Principal.pgto_parcial_co_seguro                                                                            
  From Seguros_Db.Dbo.Sinistro_Estimativa_Tb          Sinistro_Estimativa_Tb WITH (NOLOCK)                                        
  Join #Sinistro_Principal                                                                                                   
   on #Sinistro_Principal.Sinistro_ID                = Sinistro_Estimativa_Tb.Sinistro_ID                                    
Where Sinistro_Estimativa_tb.seq_estimativa = (Select max (ALT.seq_estimativa) From Sinistro_Estimativa_tb ALT WITH (NOLOCK) where ALT.sinistro_id=#Sinistro_Principal.sinistro_id)
Order By Item_Val_Estimativa                                                                                                 
Select @Tp_Componente_ID                         = Tp_Componente_ID
  From Seguros_Db.Dbo.Escolha_Tp_Cob_Vida_Tb     Escolha_Tp_Cob_Vida_Tb WITH (NOLOCK)
  Join #Sinistro_Principal
    on #Sinistro_Principal.Proposta_ID           = Escolha_Tp_Cob_Vida_Tb.Proposta_ID
 Where Escolha_Tp_Cob_Vida_Tb.Tp_Componente_ID   in (19,20)
 Order by Tp_Componente_ID
Select *                                                                                                                     
     , Tp_Componente_ID      = @Tp_Componente_ID                                                                             
  From #Sinistro_Principal                                                                                                   



 if OBJECT_ID('tempdb..#EstimativaPagamento_CoberturaAfetada') IS NOT NULL 
 BEGIN
     DROP TABLE #EstimativaPagamento_CoberturaAfetada
 END

  if OBJECT_ID('tempdb..#Cobertura_Afetada') IS NOT NULL 
 BEGIN
     DROP TABLE #Cobertura_Afetada
 END

Create Table #EstimativaPagamento_CoberturaAfetada                                                                           
     ( Tp_Cobertura_ID                               Int                                                                     
     , Correcao                                      Numeric(15,2)                                                           
     , Estimado                                      Numeric(15,2)                                                           
     , Tp_Cobertura_Nome                             VarChar(60)                                                             
     , Tipo                                          VarChar(1)                                                              
  )                                                                                                                          
Select Sinistro_Cobertura_Tb.Tp_Cobertura_ID                                                                                 
     , Correcao                                      = isnull(Pgto_Sinistro_Cobertura_Tb.Val_Correcao,0)                     
     , Estimado                                      = isnull(Sinistro_Cobertura_Tb.Val_Estimativa,0)                        
  Into #Cobertura_Afetada                                                                                                    
  From Seguros_Db.Dbo.Sinistro_Cobertura_Tb          Sinistro_Cobertura_Tb WITH (NOLOCK)                                          
  Left Outer Join (Seguros_Db.Dbo.Pgto_Sinistro_Tb   Pgto_Sinistro_Tb WITH (NOLOCK)                                               
                inner Join Seguros_Db.Dbo.Pgto_Sinistro_Cobertura_Tb Pgto_Sinistro_Cobertura_Tb WITH (NOLOCK)                     
                   on Pgto_Sinistro_Cobertura_Tb.Sinistro_ID         = Pgto_Sinistro_Tb.Sinistro_ID                          
                  and Pgto_Sinistro_Cobertura_Tb.Beneficiario_ID     = Pgto_Sinistro_Tb.Beneficiario_ID                      
                  and Pgto_Sinistro_Cobertura_Tb.Seq_Pgto            = Pgto_Sinistro_Tb.Seq_Pgto                             
                  and Pgto_Sinistro_Cobertura_Tb.Item_Val_Estimativa = Pgto_Sinistro_Tb.Item_Val_Estimativa                  
                  and Pgto_Sinistro_Cobertura_Tb.Seq_Estimativa      = Pgto_Sinistro_Tb.Seq_Estimativa                       
                  and Pgto_Sinistro_Cobertura_Tb.Num_Parcela         = Pgto_Sinistro_Tb.Num_Parcela           )              
    on Sinistro_Cobertura_Tb.Sinistro_ID             = Pgto_Sinistro_Tb.Sinistro_ID                                          
   and Sinistro_Cobertura_Tb.Tp_Cobertura_ID         = Pgto_Sinistro_Cobertura_Tb.Tp_Cobertura_ID                            
   and Pgto_Sinistro_Tb.Situacao_OP                  not in ('c', 'e')                                                       
   and Pgto_Sinistro_Tb.Item_Val_Estimativa          = 1                                                                     
  Join #Sinistro_Principal                                                                                                   
 on #Sinistro_Principal.Sinistro_ID                  = Sinistro_Cobertura_Tb.SInistro_ID                                     
 Where Sinistro_Cobertura_Tb.Dt_Fim_Vigencia         is null                                                                 
Insert into #EstimativaPagamento_CoberturaAfetada                                                                            
Select Sinistro_Cobertura_Tb.Tp_Cobertura_ID                                                                                 
     , Sum(Correcao)                                                                                                         
     , Estimado - Sum(Correcao)                                                                                              
     , Tp_Cobertura_Tb.Nome                                                                                                  
     , 'E'                                                                                                                   
  From #Cobertura_Afetada                                                                                                    
  Join Seguros_Db.Dbo.Sinistro_Cobertura_Tb          Sinistro_Cobertura_Tb WITH (NOLOCK)                                          
    on Sinistro_Cobertura_Tb.Tp_Cobertura_ID         = #Cobertura_Afetada.Tp_Cobertura_ID                                    
  Join Seguros_Db.Dbo.Tp_Cobertura_Tb                Tp_Cobertura_Tb WITH (NOLOCK)                                                
    on Tp_Cobertura_Tb.Tp_Cobertura_ID               = #Cobertura_Afetada.Tp_Cobertura_ID                                    
  Join #Sinistro_Principal                                                                                                   
    on #Sinistro_Principal.Sinistro_ID               = Sinistro_Cobertura_Tb.Sinistro_ID                                     
 Where Sinistro_Cobertura_Tb.Dt_Fim_Vigencia is null                                                                         
 Group By Sinistro_Cobertura_Tb.Tp_Cobertura_ID                                                                              
     , Estimado                                                                                                              
     , Tp_Cobertura_Tb.Nome                                                                                                  
Drop table #Cobertura_Afetada                                                                                                




SELECT tp_emissao
  FROM seguros_db..apolice_tb WITH (NOLOCK)
 WHERE apolice_id = 2635
   AND ramo_id = 82





select e.tp_cobertura_id
, f.nome ,a.proposta_id
 from escolha_plano_tb a WITH (NOLOCK) 
 ,plano_tb b WITH (NOLOCK)
 ,tp_plano_tp_comp_tb c WITH (NOLOCK)
 ,tp_cob_comp_plano_tb d WITH (NOLOCK) 
,tp_cob_comp_tb e WITH (NOLOCK) 
 ,tp_cobertura_tb f WITH (NOLOCK) 
 where a.plano_id = b.plano_id 
     and a.dt_inicio_vigencia = b.dt_inicio_vigencia
     and a.produto_id = b.produto_id
     and b.tp_plano_id = c.tp_plano_id
     and b.tp_plano_id = d.tp_plano_id
     and c.tp_componente_id = e.tp_componente_id
     and d.tp_cob_comp_id = e.tp_cob_comp_id
     and e.tp_cobertura_id = f.tp_cobertura_id
     and a.proposta_id = 1129775--24370465
     --and convert(smalldatetime, convert(varchar(10), a.dt_escolha, 111)) <= '20200212' 
     --and ( convert(smalldatetime, convert(varchar(10), a.dt_fim_vigencia, 111)) >= '20200212' or 
     --a.dt_fim_vigencia IS NULL ) 
 and c.tp_componente_id = 1
 --and e.tp_cobertura_id = 5
 union 
 select b.tp_cobertura_id, c.nome 
 from escolha_tp_cob_vida_tb a WITH (NOLOCK), 
 tp_cob_comp_tb b WITH (NOLOCK), 
 tp_cobertura_tb c WITH (NOLOCK) 
 where a.tp_cob_comp_id = b.tp_cob_comp_id 
     and b.tp_cobertura_id = c.tp_cobertura_id 
 and a.proposta_id = 24370465
 --and ( convert(smalldatetime, convert(varchar(10), a.dt_fim_vigencia_esc, 111)) >= '20200212' 
 --or a.dt_fim_vigencia_esc is null ) 
 and b.tp_componente_id = 1
 union 
 select b.tp_cobertura_id, c.nome 
 from escolha_tp_cob_vida_aceito_tb a WITH (NOLOCK), 
 tp_cob_comp_tb b WITH (NOLOCK), 
 tp_cobertura_tb c WITH (NOLOCK) 
 where a.tp_cob_comp_id = b.tp_cob_comp_id 
 and b.tp_cobertura_id = c.tp_cobertura_id 
 and proposta_id = 24370465
 --and ( convert(smalldatetime, convert(varchar(10), a.dt_inicio_vigencia_esc, 111)) <= '20200212' 
 --and convert(smalldatetime, convert(varchar(10), a.dt_fim_vigencia_esc, 111)) >= '20200212' 
 --or  a.dt_fim_vigencia_esc is null ) 
 and b.tp_componente_id = 1
 union 
 select b.tp_cobertura_id, c.nome ,*
 from escolha_sub_grp_tp_cob_comp_tb a WITH (NOLOCK), 
 tp_cob_comp_tb b WITH (NOLOCK), 
 tp_cobertura_tb c WITH (NOLOCK) 
 where a.tp_cob_comp_id = b.tp_cob_comp_id 
 and b.tp_cobertura_id = c.tp_cobertura_id 
 and a.Apolice_id = 2635
 and a.ramo_id = 82
 and a.seguradora_cod_susep = 6785
 and a.sucursal_seguradora_id = 0
 --and b.tp_componente_id = 1
 --and c.tp_cobertura_id  = 5
 Union 
 select Tp_Cobertura_ID, Tp_Cobertura_Nome 
 from #EstimativaPagamento_CoberturaAfetada 
 order by 1


 select * from escolha_plano_tb where proposta_id = 24370465



select a.produto_id,b.tp_cobertura_id, b.nome,*
from tp_cob_item_prod_tb a
inner join tp_cobertura_tb b
on a.tp_cobertura_id = b.tp_cobertura_id
where A.produto_id in (225)
--order by produto_id


select distinct 
--e.tp_cobertura_id,
-- f.nome ,
--a.proposta_id
--g.*
g.produto_id
 from escolha_plano_tb a WITH (NOLOCK)
 ,proposta_tb g WITH (NOLOCK)
 ,plano_tb b WITH (NOLOCK)
 ,tp_plano_tp_comp_tb c WITH (NOLOCK)
 ,tp_cob_comp_plano_tb d WITH (NOLOCK) 
,tp_cob_comp_tb e WITH (NOLOCK) 
 ,tp_cobertura_tb f WITH (NOLOCK) 
 where a.plano_id = b.plano_id 
     and a.dt_inicio_vigencia = b.dt_inicio_vigencia
     and a.produto_id = b.produto_id
     and b.tp_plano_id = c.tp_plano_id
     and b.tp_plano_id = d.tp_plano_id
     and c.tp_componente_id = e.tp_componente_id
     and d.tp_cob_comp_id = e.tp_cob_comp_id
     and e.tp_cobertura_id = f.tp_cobertura_id
	 and g.proposta_id = a.proposta_id
     --and a.proposta_id = 1129775--24370465
     --and convert(smalldatetime, convert(varchar(10), a.dt_escolha, 111)) <= '20200212' 
     --and ( convert(smalldatetime, convert(varchar(10), a.dt_fim_vigencia, 111)) >= '20200212' or 
     --a.dt_fim_vigencia IS NULL ) 
 --and c.tp_componente_id = 1
and g.produto_id between  200 and 225
 and g.situacao = 'i'
 and e.tp_cobertura_id = 5
 order by g.produto_id


 SELECT DISTINCT
	--e.tp_cobertura_id,
	-- f.nome ,
	--a.proposta_id
	--g.*
	g.produto_id
FROM SEGUROS_DB.DBO.PROPOSTA_TB G WITH (NOLOCK)
INNER JOIN SEGUROS_DB.DBO.escolha_plano_tb A
	ON G.PROPOSTA_ID = A.PROPOSTA_ID
INNER JOIN SEGUROS_DB.DBO.plano_tb B
	ON A.dt_inicio_vigencia = B.dt_inicio_vigencia
		AND A.PRODUTO_ID = B.PRODUTO_ID
INNER JOIN SEGUROS_DB.DBO.tp_plano_tp_comp_tb c WITH (NOLOCK)
	ON b.tp_plano_id = c.tp_plano_id
INNER JOIN SEGUROS_DB.DBO.tp_cob_comp_plano_tb d WITH (NOLOCK)
	ON b.tp_plano_id = d.tp_plano_id
INNER JOIN SEGUROS_DB.DBO.tp_cob_comp_tb e WITH (NOLOCK)
	ON d.tp_cob_comp_id = e.tp_cob_comp_id
INNER JOIN SEGUROS_DB.DBO.tp_cobertura_tb f WITH (NOLOCK)
	ON e.tp_cobertura_id = f.tp_cobertura_id
WHERE 1 = 1
	--AND a.proposta_id = 1129775 --24370465
	--AND convert(SMALLDATETIME, convert(VARCHAR(10), a.dt_escolha, 111)) <= '20200212'
	--AND (
	--	convert(SMALLDATETIME, convert(VARCHAR(10), a.dt_fim_vigencia, 111)) >= '20200212'
	--	OR a.dt_fim_vigencia IS NULL
	--	)
	AND c.tp_componente_id = 1
	AND g.produto_id BETWEEN 200
		AND 225
	AND g.situacao = 'i'
	AND e.tp_cobertura_id = 5
ORDER BY g.produto_id
