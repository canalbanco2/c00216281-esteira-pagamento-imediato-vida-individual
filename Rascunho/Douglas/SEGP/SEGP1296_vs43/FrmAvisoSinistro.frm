VERSION 5.00
Object = "{5E9E78A0-531B-11CF-91F6-C2863C385E30}#1.0#0"; "MSFLXGRD.OCX"
Object = "{C932BA88-4374-101B-A56C-00AA003668DC}#1.1#0"; "MSMASK32.OCX"
Begin VB.Form FrmAvisoSinistro 
   Caption         =   "SEGP1296 - Consulta de Aviso - QUALIDADE - RURAL"
   ClientHeight    =   5940
   ClientLeft      =   5565
   ClientTop       =   2355
   ClientWidth     =   11520
   LinkTopic       =   "Form1"
   ScaleHeight     =   5940
   ScaleWidth      =   11520
   Begin VB.Frame frameReabertura 
      Caption         =   "Sinistro Reaberto"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   975
      Left            =   240
      TabIndex        =   58
      Top             =   4920
      Visible         =   0   'False
      Width           =   5415
      Begin VB.ComboBox cmbReabertura 
         Height          =   315
         Left            =   120
         Style           =   2  'Dropdown List
         TabIndex        =   59
         Top             =   495
         Width           =   5175
      End
      Begin VB.Label lbl 
         Caption         =   "Data da Reabertura e Usu�rio:"
         Height          =   255
         Index           =   75
         Left            =   120
         TabIndex        =   60
         Top             =   240
         Width           =   3015
      End
   End
   Begin VB.TextBox txtAuxiliar 
      Height          =   285
      Left            =   5760
      TabIndex        =   56
      Text            =   "Cancelado"
      Top             =   5520
      Width           =   975
   End
   Begin VB.Frame FmeEndereco_Ocorrencia 
      Caption         =   "Endere�o de Ocorr�ncia do Sinistro"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   1575
      Left            =   240
      TabIndex        =   49
      Top             =   3240
      Width           =   8925
      Begin VB.CommandButton BtnSelecionar_Proposta 
         Caption         =   "Selecionar da Proposta"
         Height          =   495
         Left            =   7380
         TabIndex        =   57
         Top             =   310
         Width           =   1305
      End
      Begin VB.ComboBox CmbCodigo_Objeto_Ocorrencia 
         Height          =   315
         Left            =   360
         Style           =   2  'Dropdown List
         TabIndex        =   14
         Top             =   480
         Width           =   1335
      End
      Begin VB.TextBox TxtMunicipio_Ocorrencia 
         Height          =   315
         Left            =   3840
         MaxLength       =   30
         TabIndex        =   17
         Top             =   1080
         Width           =   3735
      End
      Begin VB.TextBox TxtEstado_Ocorrencia 
         Height          =   315
         Left            =   7800
         MaxLength       =   2
         TabIndex        =   18
         Top             =   1080
         Width           =   855
      End
      Begin VB.TextBox TxtBairro_Ocorrencia 
         Height          =   315
         Left            =   240
         MaxLength       =   30
         TabIndex        =   16
         Top             =   1080
         Width           =   3345
      End
      Begin VB.TextBox TxtEndereco_Ocorrencia 
         Height          =   315
         Left            =   1800
         MaxLength       =   60
         TabIndex        =   15
         Top             =   480
         Width           =   5415
      End
      Begin VB.Label lblDadosGerais_Sinistrado 
         Caption         =   "Bairro:"
         Height          =   255
         Index           =   14
         Left            =   240
         TabIndex        =   54
         Top             =   840
         Width           =   1425
      End
      Begin VB.Label lblDadosGerais_Sinistrado 
         Caption         =   "Munic�pio :"
         Height          =   255
         Index           =   13
         Left            =   3840
         TabIndex        =   53
         Top             =   840
         Width           =   1455
      End
      Begin VB.Label lblDadosGerais_Sinistrado 
         Caption         =   "Endere�o :"
         Height          =   255
         Index           =   12
         Left            =   1800
         TabIndex        =   52
         Top             =   270
         Width           =   2295
      End
      Begin VB.Label lblDadosGerais_Sinistrado 
         Caption         =   "Estado:"
         Height          =   255
         Index           =   11
         Left            =   7800
         TabIndex        =   51
         Top             =   840
         Width           =   975
      End
      Begin VB.Label lblDadosGerais_Sinistrado 
         AutoSize        =   -1  'True
         Caption         =   "C�digo do Objeto:"
         Height          =   195
         Index           =   10
         Left            =   270
         TabIndex        =   50
         Top             =   255
         Width           =   1275
      End
   End
   Begin VB.Frame FmeDados_Sinistrado 
      Caption         =   "Sinistrado"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   1575
      Left            =   240
      TabIndex        =   41
      Top             =   3240
      Width           =   8925
      Begin VB.ComboBox CmbVidaSexo 
         Height          =   315
         Left            =   3600
         Style           =   2  'Dropdown List
         TabIndex        =   23
         Top             =   1080
         Width           =   1335
      End
      Begin VB.TextBox txtSinistro_Vida_Nome 
         Height          =   315
         Left            =   1920
         MaxLength       =   60
         TabIndex        =   20
         Top             =   480
         Width           =   6735
      End
      Begin VB.TextBox txtSinistrado_Cliente_ID 
         Height          =   315
         Left            =   240
         MaxLength       =   60
         TabIndex        =   21
         Top             =   1080
         Width           =   1425
      End
      Begin VB.TextBox txtSinistro_Vida_Sexo 
         Height          =   315
         Left            =   3600
         TabIndex        =   24
         Top             =   1080
         Visible         =   0   'False
         Width           =   255
      End
      Begin VB.CommandButton btnDadosGerais_Sinistrado_Cadastrar 
         Caption         =   "N&ovo"
         Height          =   375
         Left            =   13320
         TabIndex        =   42
         Top             =   840
         Visible         =   0   'False
         Width           =   1452
      End
      Begin MSMask.MaskEdBox mskCPF_Sinistrado 
         Height          =   315
         Left            =   240
         TabIndex        =   19
         Top             =   480
         Width           =   1455
         _ExtentX        =   2566
         _ExtentY        =   556
         _Version        =   393216
         HideSelection   =   0   'False
         MaxLength       =   14
         Format          =   "###.###.###-##"
         Mask            =   "###.###.###-##"
         PromptChar      =   "_"
      End
      Begin MSMask.MaskEdBox mskDtNasc_Sinistrado 
         Height          =   315
         Left            =   1920
         TabIndex        =   22
         Top             =   1080
         Width           =   1215
         _ExtentX        =   2143
         _ExtentY        =   556
         _Version        =   393216
         HideSelection   =   0   'False
         MaxLength       =   10
         Mask            =   "##/##/####"
         PromptChar      =   "_"
      End
      Begin VB.Label lblDadosGerais_Sinistrado 
         AutoSize        =   -1  'True
         Caption         =   "CPF :"
         Height          =   195
         Index           =   9
         Left            =   270
         TabIndex        =   47
         Top             =   255
         Width           =   390
      End
      Begin VB.Label lblDadosGerais_Sinistrado 
         Caption         =   "Sexo :"
         Height          =   255
         Index           =   8
         Left            =   3600
         TabIndex        =   46
         Top             =   840
         Width           =   1095
      End
      Begin VB.Label lblDadosGerais_Sinistrado 
         Caption         =   "Nome :"
         Height          =   255
         Index           =   7
         Left            =   1920
         TabIndex        =   45
         Top             =   270
         Width           =   2295
      End
      Begin VB.Label lblDadosGerais_Sinistrado 
         Caption         =   "Data de Nascimento :"
         Height          =   255
         Index           =   6
         Left            =   1920
         TabIndex        =   44
         Top             =   855
         Width           =   1455
      End
      Begin VB.Label lblDadosGerais_Sinistrado 
         Caption         =   "C�digo do Cliente :"
         Height          =   255
         Index           =   5
         Left            =   240
         TabIndex        =   43
         Top             =   840
         Width           =   1425
      End
   End
   Begin VB.CommandButton btnVoltar 
      Caption         =   "Voltar"
      Height          =   375
      Left            =   9840
      TabIndex        =   28
      Top             =   5400
      Width           =   1452
   End
   Begin VB.CommandButton btnAplicar 
      Caption         =   "Aplicar"
      Height          =   375
      Left            =   8160
      TabIndex        =   27
      Top             =   5400
      Visible         =   0   'False
      Width           =   1452
   End
   Begin VB.Frame FmeDados_Aviso 
      Caption         =   "Dados do Aviso"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   2955
      Left            =   240
      TabIndex        =   34
      Top             =   120
      Width           =   5880
      Begin VB.CheckBox chkPgto_Parcial_Co_Seguro 
         Caption         =   "Pgto. sobre nossa parte"
         Height          =   195
         Left            =   3600
         TabIndex        =   0
         Top             =   240
         Width           =   2055
      End
      Begin VB.TextBox txtMoeda 
         Enabled         =   0   'False
         Height          =   315
         Left            =   240
         Locked          =   -1  'True
         TabIndex        =   62
         Top             =   2520
         Width           =   975
      End
      Begin VB.ComboBox cboMoeda 
         Height          =   315
         Left            =   240
         Style           =   2  'Dropdown List
         TabIndex        =   63
         Top             =   2520
         Width           =   975
      End
      Begin VB.TextBox TxtNumero_Sinistro_Lider 
         Height          =   315
         Left            =   3690
         MaxLength       =   20
         TabIndex        =   6
         Top             =   2505
         Width           =   1935
      End
      Begin VB.ComboBox cboSubevento_Sinistro 
         Height          =   315
         Left            =   240
         Style           =   2  'Dropdown List
         TabIndex        =   1
         Top             =   1080
         Width           =   5470
      End
      Begin VB.ComboBox cboCausa_Sinistro 
         Height          =   315
         ItemData        =   "FrmAvisoSinistro.frx":0000
         Left            =   240
         List            =   "FrmAvisoSinistro.frx":0007
         Style           =   2  'Dropdown List
         TabIndex        =   64
         Top             =   480
         Width           =   5470
      End
      Begin VB.TextBox txtData_Inclusao 
         Height          =   315
         Left            =   4560
         Locked          =   -1  'True
         TabIndex        =   5
         TabStop         =   0   'False
         Text            =   "15/02/2012"
         Top             =   1755
         Width           =   1095
      End
      Begin MSMask.MaskEdBox mskData_Entrada 
         Height          =   315
         Left            =   3135
         TabIndex        =   4
         Top             =   1755
         Width           =   1215
         _ExtentX        =   2143
         _ExtentY        =   556
         _Version        =   393216
         HideSelection   =   0   'False
         MaxLength       =   10
         Mask            =   "##/##/####"
         PromptChar      =   "_"
      End
      Begin MSMask.MaskEdBox mskData_Ocorrencia 
         Height          =   315
         Left            =   240
         TabIndex        =   2
         Top             =   1755
         Width           =   1215
         _ExtentX        =   2143
         _ExtentY        =   556
         _Version        =   393216
         HideSelection   =   0   'False
         MaxLength       =   10
         Mask            =   "##/##/####"
         PromptChar      =   "_"
      End
      Begin MSMask.MaskEdBox mskData_Aviso 
         Height          =   315
         Left            =   1695
         TabIndex        =   3
         Top             =   1755
         Width           =   1215
         _ExtentX        =   2143
         _ExtentY        =   556
         _Version        =   393216
         HideSelection   =   0   'False
         MaxLength       =   10
         Mask            =   "##/##/####"
         PromptChar      =   "_"
      End
      Begin VB.Label Label1 
         Caption         =   "Moeda Sinistro"
         Height          =   255
         Left            =   240
         TabIndex        =   61
         Top             =   2280
         Width           =   1215
      End
      Begin VB.Label LblNumero_Sinistro_Lider 
         AutoSize        =   -1  'True
         Caption         =   "N�mero da Sinistro L�der"
         Height          =   195
         Left            =   3720
         TabIndex        =   55
         Top             =   2280
         Width           =   1875
      End
      Begin VB.Label lbl 
         Caption         =   "Subevento do Sinistro:"
         Height          =   255
         Index           =   94
         Left            =   240
         TabIndex        =   40
         Top             =   840
         Width           =   1935
      End
      Begin VB.Label lbl 
         Caption         =   "Evento do Sinistro:"
         Height          =   255
         Index           =   11
         Left            =   240
         TabIndex        =   39
         Top             =   240
         Width           =   1935
      End
      Begin VB.Label lbl 
         Caption         =   "Data Aviso:"
         Height          =   255
         Index           =   12
         Left            =   1695
         TabIndex        =   38
         Top             =   1515
         Width           =   975
      End
      Begin VB.Label lbl 
         Caption         =   "Data Ocorr�ncia:"
         Height          =   255
         Index           =   14
         Left            =   240
         TabIndex        =   37
         Top             =   1515
         Width           =   1335
      End
      Begin VB.Label lbl 
         Caption         =   "Data Entr. Seg.:"
         Height          =   255
         Index           =   13
         Left            =   3135
         TabIndex        =   36
         Top             =   1515
         Width           =   1215
      End
      Begin VB.Label lbl 
         Caption         =   "Data Inclus�o:"
         Height          =   255
         Index           =   82
         Left            =   4560
         TabIndex        =   35
         Top             =   1515
         Width           =   1215
      End
   End
   Begin VB.Frame FmeInformacoes_BB 
      Caption         =   "Informa��es BB"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   2925
      Left            =   6360
      TabIndex        =   31
      Top             =   120
      Width           =   4935
      Begin VB.CommandButton btnBBInativar 
         Caption         =   "Inativar"
         Height          =   375
         Left            =   3480
         TabIndex        =   12
         Top             =   1365
         Width           =   1305
      End
      Begin VB.CommandButton btnBBRemover 
         Caption         =   "Excluir >>"
         Height          =   375
         Left            =   3480
         TabIndex        =   13
         Top             =   2235
         Width           =   1305
      End
      Begin VB.CommandButton btnBBAdicionar 
         Caption         =   "<< Adicionar"
         Height          =   375
         Left            =   3480
         TabIndex        =   10
         Top             =   1800
         Width           =   1305
      End
      Begin VB.TextBox txtNumero_Sinistro_InformacaoBB 
         Height          =   315
         Left            =   1440
         MaxLength       =   13
         TabIndex        =   8
         Top             =   480
         Width           =   1305
      End
      Begin VB.CheckBox chkReintegracaoIS 
         Caption         =   "Reintegra��o de IS"
         Height          =   375
         Left            =   3000
         TabIndex        =   9
         Top             =   480
         Width           =   1770
      End
      Begin MSMask.MaskEdBox mskAgencia_Aviso 
         Height          =   315
         Left            =   240
         TabIndex        =   7
         Top             =   480
         Width           =   975
         _ExtentX        =   1720
         _ExtentY        =   556
         _Version        =   393216
         PromptInclude   =   0   'False
         MaxLength       =   4
         Mask            =   "####"
         PromptChar      =   " "
      End
      Begin MSFlexGridLib.MSFlexGrid GrdInformacao_SinistroBB 
         Height          =   1605
         Left            =   240
         TabIndex        =   11
         TabStop         =   0   'False
         Top             =   1080
         Width           =   3120
         _ExtentX        =   5503
         _ExtentY        =   2831
         _Version        =   393216
         Cols            =   3
         FixedCols       =   0
         FormatString    =   "N� Sinistro BB   | Situa��o  | Localiza��o"
      End
      Begin VB.Label lblNumero_Sinistro_BB 
         Caption         =   "N� Sinistro BB:"
         Height          =   255
         Left            =   1440
         TabIndex        =   33
         Top             =   240
         Width           =   1080
      End
      Begin VB.Label lbl 
         AutoSize        =   -1  'True
         Caption         =   "Ag�ncia Aviso:"
         Height          =   195
         Index           =   23
         Left            =   240
         TabIndex        =   32
         Top             =   240
         Width           =   1065
      End
   End
   Begin VB.Frame FmeDados_Endosso 
      Caption         =   "Dados do Endosso"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   1575
      Left            =   9360
      TabIndex        =   29
      Top             =   3240
      Width           =   1935
      Begin VB.TextBox txtEndosso 
         Height          =   330
         Left            =   180
         MaxLength       =   3
         TabIndex        =   25
         Text            =   "0"
         Top             =   495
         Width           =   825
      End
      Begin MSMask.MaskEdBox mskAverbacao 
         Height          =   315
         Left            =   120
         TabIndex        =   26
         Top             =   1095
         Visible         =   0   'False
         Width           =   1065
         _ExtentX        =   1879
         _ExtentY        =   556
         _Version        =   393216
         MaxLength       =   7
         Mask            =   "9999999"
         PromptChar      =   "_"
      End
      Begin VB.Label lblAverbacao 
         Caption         =   "Averba��o:"
         Height          =   195
         Left            =   150
         TabIndex        =   48
         Top             =   840
         Visible         =   0   'False
         Width           =   1035
      End
      Begin VB.Label lbl 
         Caption         =   "Endosso:"
         Height          =   195
         Index           =   90
         Left            =   165
         TabIndex        =   30
         Top             =   240
         Width           =   780
      End
   End
End
Attribute VB_Name = "FrmAvisoSinistro"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Public sOperacao As String
Public bytModoOperacao As Byte         '1-Inclusao / 2-Alteracao/Exclus�o / 3-Consulta
Public vSubevento_Sinistro As String
Public vEnd_Risco_id As Integer
Public Municipio_Id_Anterior
Public vSubevento_Sinistro_id As String
Public vAgencia_id As String
Public vReintegracao_IS As Boolean
Public vSinistro_Lider As String
Public vDt_Inicio_Vigencia_Seg As String
Public sSolicitante_id As String
Public Sinistro_Cliente_Id As Integer
Public vSinistro_BB As String
Public Tp_Altera_SinistroBB As String
Public vSelSinistroBB As Boolean
Public Incluiu_Novo_SinistroBB As Boolean
Public vExiste_Sinistro As Boolean
Private lConexaoLocal As Integer
Public sDt_inicio_vigencia_seg As String    'GABRIEL
Public bSaiDoLoop As Boolean            'GABRIEL
Public bEfetuouAlteracaoEndereco As Boolean    'GABRIEL
Public bCarregaEnderecoDaCombo As Boolean    'mathayde - 29
Public sSinistroBBExclusao As Variant
Public sSinistroBBInclusao As Variant
Public sSinistroBBAlteracao As Variant

'FLAVIO.BEZERRA - 16/07/2013 - MP Temporaria
Public strEndereco_Ocorrencia As String
Public strBairro_Ocorrencia As String
Public strMunicipio_Ocorrencia As String
Public strEstado_Ocorrencia As String
'FLAVIO.BEZERRA - 16/07/2013 - MP Temporaria
Private Sub btnAplicar_Click()
    Dim Itens_historico(0) As String
    Dim Num_Aviso_Cadastrado As String

'Debug.Print Format(Now, "hh:mm:ss:ms") 'cristovao.rodrigues 29/05/2013

    ''    'mathayde
    If Not Valida_Agencia Then
        Exit Sub
    End If
    
    'FLAVIO.BEZERRA - IN�CIO - 27/02/2013
    If sOperacaoCosseguro = "C" And cboMoeda.ListIndex = -1 Then
        MsgBox "Moeda Sinistro n�o cadastrada no aviso.", vbOKOnly + vbExclamation, "Aten��o"
        Preenche_Moeda
        Exit Sub
    ElseIf sOperacaoCosseguro <> "C" And txtMoeda.Tag = "" Then
        MsgBox "Moeda Sinistro n�o cadastrada no aviso.", vbOKOnly + vbExclamation, "Aten��o"
        Preenche_Moeda
        Exit Sub
    End If
    'FLAVIO.BEZERRA - FIM - 27/02/2013
    
    'cristovao.rodrigues 17/06/2013 apresenta erro ao executar VerificarExistenciaAviso e nao preencher data de ocorrencia
    If Not ValidaAvisoData Then
        Exit Sub
    End If
    
    'Paulo Pelegrini - MU-2017-042685 - 09/04/2018 (INI)
    If bytTipoRamo = bytTipoRamo_Vida And gbllProduto_ID <> 1140 Then
        If (DateDiff("d", mskData_Ocorrencia.Text, Data_Sistema) / 365) > 30.02 Then
            If MsgBox("A data do Aviso � maior que 30 anos da data do Evento." + vbNewLine + "Deseja Continuar mesmo assim?", vbYesNo + vbQuestion + vbDefaultButton2) = vbNo Then
                mskData_Ocorrencia.SetFocus
                Exit Sub
            End If
        End If
    End If
    'Paulo Pelegrini  - MU-2017-042685 - 09/04/2018 (FIM)
    
    'FLAVIO.BEZERRA - IN�CIO - 14/01/2013
    Num_Aviso_Cadastrado = VerificarExistenciaAviso
    If Num_Aviso_Cadastrado <> "0" And sOperacaoCosseguro = "C" Then
        MsgBox "N�mero de aviso da L�der cadastrado no(s) aviso(s): " & Num_Aviso_Cadastrado, vbOKOnly + vbExclamation, "Aten��o"
        TxtNumero_Sinistro_Lider.SetFocus
        Exit Sub
    End If
    'FLAVIO.BEZERRA - FIM - 14/01/2013
    
    If Not valida_estado(Municipio_Id_Anterior) Then
        Exit Sub
    End If
    
    'cristovao.rodrigues 27/05/2013, verificar se foi selecionado endereco de ocorrencia
    If CmbCodigo_Objeto_Ocorrencia.ListCount > 0 And vEnd_Risco_id = 0 And bytTipoRamo <> bytTipoRamo_Vida Then
        MsgBox "Necess�rio selecionar um endere�o de ocorr�ncia.", vbOKOnly, App.EXEName
        Exit Sub
    End If

    If cboCausa_Sinistro.ListIndex = -1 Then
        MsgBox "Escolha a Causa do Sinistro", vbOKOnly, App.EXEName
        Exit Sub
    End If

    If CmbVidaSexo.ListIndex = -1 Then
        MsgBox "Escolha o sexo!", vbOKOnly, App.EXEName
        Exit Sub
    End If
    If ValidaAvisoData Then
        If bytModoOperacao = 1 Then
            If Valida_Seguro() Then
                If Valida_endosso Then
                    If Inclui_Sinistro Then
                        If bytTipoRamo = bytTipoRamo_Vida Then
                            If Inclui_Sinistrado Then
                            
                            'Debug.Print Format(Now, "hh:mm:ss:ms") 'cristovao.rodrigues 29/05/2013
                            
                                MsgBox "Sinistro " & gbldSinistro_ID & " Criado com Sucesso", vbOKOnly, App.EXEName
                                txtAuxiliar.Text = gbldSinistro_ID
                                
                            End If
                        Else
                        
                            'Debug.Print Format(Now, "hh:mm:ss:ms") 'cristovao.rodrigues 29/05/2013
                            
                            MsgBox "Sinistro " & gbldSinistro_ID & " Criado com Sucesso", vbOKOnly, App.EXEName
                            txtAuxiliar.Text = gbldSinistro_ID
                            
                                                        
                        End If
                    Else
                        txtAuxiliar = "Cancelado"
                    End If
                Else
                    Exit Sub
                End If
            Else
                Exit Sub
            End If
        Else
            If Valida_Seguro() Then
                If Not Inclui_Evento("10009", 0, "NULL", Itens_historico) Then
                    TrataErroGeral "Inclus�o de Sinistrado"
                    Call FinalizarAplicacao
                End If
                If Altera_Sinistro Then
                    If bytTipoRamo = bytTipoRamo_Vida Then
                        If Altera_Sinistrado Then
                            MsgBox "Sinistro " & gbldSinistro_ID & " Alterado com Sucesso", vbOKOnly, App.EXEName
                            txtAuxiliar.Text = "OK"
                        End If
                    Else
                        MsgBox "Sinistro " & gbldSinistro_ID & " Alterado com Sucesso", vbOKOnly, App.EXEName
                        txtAuxiliar.Text = "OK"
                    End If
                End If
            Else
                Exit Sub
            End If
        End If
    Else
        Exit Sub
    End If
    btnVoltar_Click


End Sub

Private Sub btnBBAdicionar_Click()

    Dim vSql As String
    Dim rs As ADODB.Recordset
    Dim vIgual As Boolean

    If Valida_Agencia Then
        If Not IsNumeric(txtNumero_Sinistro_InformacaoBB) Then
            mensagem_erro 3, "N� Sinistro BB"
            txtNumero_Sinistro_InformacaoBB.SetFocus
            Exit Sub
        End If
        If Len(txtNumero_Sinistro_InformacaoBB) <> 11 And Len(txtNumero_Sinistro_InformacaoBB) <> 13 Then
            mensagem_erro 3, "N� Sinistro BB"
            txtNumero_Sinistro_InformacaoBB.SetFocus
            Exit Sub
        End If
        If CInt(Left(txtNumero_Sinistro_InformacaoBB, 4)) < 1950 And CInt(Left(txtNumero_Sinistro_InformacaoBB, 4)) > 3000 Then
            mensagem_erro 6, "Os quatro primeiros digitos do N� Sinistro BB devem ser correspondentes ao ano!"
            txtNumero_Sinistro_InformacaoBB.SetFocus
            Exit Sub
        End If
        If CInt(Mid(txtNumero_Sinistro_InformacaoBB, 5, 2)) < 1 Or _
           CInt(Mid(txtNumero_Sinistro_InformacaoBB, 5, 2)) > 12 Then
            mensagem_erro 6, "O quinto e o sexto digitos do N� Sinistro BB devem ser correspondentes a um m�s!"
            txtNumero_Sinistro_InformacaoBB.SetFocus
            Exit Sub
        End If


        vSinistro_BB = Nro_BB(gbldSinistro_ID)

        If vSinistro_BB <> "0" Then

            Incluiu_Novo_SinistroBB = True

        End If

        If GrdInformacao_SinistroBB.Rows > 1 Then

            If vSinistro_BB = txtNumero_Sinistro_InformacaoBB.Text Then    'N�o foi removido
                mensagem_erro 1, "N� Sinistro BB"
                txtNumero_Sinistro_InformacaoBB.SetFocus
                Exit Sub
            End If

        End If

        If bytModoOperacao = 1 Then

            vSql = "EXEC SEGS7729_SPS " & txtNumero_Sinistro_InformacaoBB.Text

        Else

            vSql = "        Select count(evento_BB) from interface_db..entrada_gtr_tb WITH (NOLOCK) "    'Nakata Flow 190.066 04/12/2006
            vSql = vSql & "  where sinistro_bb = " & txtNumero_Sinistro_InformacaoBB.Text
            vSql = vSql & "    and evento_BB = 2000 "

            Set rs = Conexao_ExecutarSQL(gsSIGLASISTEMA, _
                                         glAmbiente_id, _
                                         App.Title, _
                                         App.FileDescription, _
                                         vSql, _
                                         lConexaoLocal, True)

            If rs(0) = 0 Then
                'Demanda 17922837 - IN�CIO
'                mensagem_erro 6, "Numero do sinistro BB n�o informado pelo Banco."
'                Exit Sub
                            
                If bytModoOperacao = 2 And bytTipoRamo = 1 Then
                    'Verificar se o sinistro BB est� atribu�do a outro Sinistro AB
                    vSql = " Select count(1) From sinistro_bb_tb WITH (NOLOCK) "
                    vSql = vSql & " Where sinistro_bb = " & txtNumero_Sinistro_InformacaoBB.Text
                    
                    Set rs = Conexao_ExecutarSQL(gsSIGLASISTEMA, _
                                                 glAmbiente_id, _
                                                 App.Title, _
                                                 App.FileDescription, _
                                                 vSql, _
                                                 lConexaoLocal, True)
                                                 
                    If rs(0) > 0 Then
                        mensagem_erro 6, "Numero do sinistro BB j� atribu�do a outro sinistro AB."
                        Exit Sub
                    End If

                    rs.Close
                                
                    GoTo montaGrid
                                
                Else
                    mensagem_erro 6, "Numero do sinistro BB n�o informado pelo Banco."
                    Exit Sub
                End If
                'Demanda 17922837 - FIM
            End If
            rs.Close

            vSql = "SELECT COUNT(sinistro_id) FROM sinistro_bb_tb WITH (NOLOCK) WHERE sinistro_bb = " & txtNumero_Sinistro_InformacaoBB


        End If

        Set rs = Conexao_ExecutarSQL(gsSIGLASISTEMA, _
                                     glAmbiente_id, _
                                     App.Title, _
                                     App.FileDescription, _
                                     vSql, _
                                     lConexaoLocal, True)

        vIgual = False
        If rs(0) > 0 Then

            If vSinistro_BB = txtNumero_Sinistro_InformacaoBB Then
                vIgual = True
            End If

            If Not vIgual Then
                mensagem_erro 1, "N� Sinistro BB"
                txtNumero_Sinistro_InformacaoBB.SetFocus
                rs.Close
                Exit Sub
            End If
        End If
        rs.Close

        If Not vIgual Then

            If CLng(Left(txtNumero_Sinistro_InformacaoBB, 6)) >= 200211 Then

'AKIO.OKUNO - INICIO - 15/02/2013
'                vsql = "SELECT COUNT(sinistro_id) FROM sinistro_bb_tb WITH (NOLOCK) WHERE sinistro_bb = " & txtNumero_Sinistro_InformacaoBB
                vSql = "SELECT COUNT(Sinistro_BB)                                           " & vbNewLine
                vSql = vSql & " FROM INTERFACE_DB..entrada_GTR_Atual_tb WITH (NOLOCK)        " & vbNewLine
                vSql = vSql & " WHERE sinistro_BB = " & txtNumero_Sinistro_InformacaoBB & " " & vbNewLine
                vSql = vSql & "   AND evento_BB   = 2000                                    " & vbNewLine
'AKIO.OKUNO - FIM - 15/02/2013

                Set rs = Conexao_ExecutarSQL(gsSIGLASISTEMA, _
                                             glAmbiente_id, _
                                             App.Title, _
                                             App.FileDescription, _
                                             vSql, _
                                             lConexaoLocal, True)

'                If Not rs(0) > 0 Then      'AKIO.OKUNO - 15/02/2013
                If rs(0) = 0 Then           'AKIO.OKUNO - 15/02/2013
                    mensagem_erro 6, "O n�mero de aviso do sinistro n�o foi retornado pelo BB."
                    txtNumero_Sinistro_InformacaoBB.SetFocus
                    Exit Sub
                End If

                rs.Close

            End If

        End If




        If gblsSinistro_Situacao_ID = "5" Then
            mensagem_erro 6, "N�o � poss�vel incluir um N� de Sinistro BB para um sinistro reaberto como administrativo."
            Exit Sub
        End If

'Demanda 17922837 - IN�CIO
montaGrid:
'Demanda 17922837 - FIM

        Monta_GrdInformacao_SinistroBB

        Tp_Altera_SinistroBB = "Incluir"
        If sSinistroBBInclusao = "" Then
            sSinistroBBInclusao = txtNumero_Sinistro_InformacaoBB.Text
        Else
            sSinistroBBInclusao = sSinistroBBInclusao & "," & txtNumero_Sinistro_InformacaoBB.Text
        End If

        txtNumero_Sinistro_InformacaoBB.Text = ""
    End If
End Sub

Private Sub btnBBInativar_Click()
    Dim iTipo As String
    Dim iTipoOutros As String
    Dim i As Integer


    vSelSinistroBB = True

    With GrdInformacao_SinistroBB

        If .Rows = 1 Then Exit Sub

        If .Col = 0 Then
            If .TextMatrix(.RowSel, 0) = "" Then
                Exit Sub
            End If

            If UCase(.TextMatrix(.RowSel, 1)) = "INATIVO" Then

                MsgBox "A Situa��o do Sinistro BB n�o pode ser alterada.", vbOKOnly + vbExclamation, App.EXEName
                Exit Sub

            End If


            If gblsSinistro_Situacao_ID = "5" Then
                mensagem_erro 6, "N�o � poss�vel Inativar um Sinistro BB para um sinistro reaberto como administrativo."
                Exit Sub
            End If





            If Obtem_Localizacao_Sinistro_BB(txtNumero_Sinistro_InformacaoBB) = "AG�NCIA" Then
                If MsgBox("Esta altera��o ir� tornar o Sinistro BB Inativo e n�o poder� ser revertida." & vbCrLf & "Confirma a altera��o da situa��o do sinistro BB: " & .TextMatrix(.RowSel, GRID_SINISTRO_BB_ID) & " ?", vbYesNo + vbDefaultButton2 + vbCritical) = vbNo Then
                    Exit Sub
                End If
            End If

            iTipo = GrdInformacao_SinistroBB.Rows



            If iTipo = "2" And VerificaOutroAtivo(.TextMatrix(.RowSel, 0)) = False Then
                MsgBox "O Sinistro BB n�o pode ser Inativado pois � o �nico ativo.", vbOKOnly + vbCritical, App.EXEName
                Exit Sub
            End If





'            If iTipo = "2" Or iTipo = "3" Then         'AKIO.OKUNO - 21/11/2012
            If Val(iTipo) > 2 Then

                If vSelSinistroBB And GrdInformacao_SinistroBB.Rows > 1 Then

                    If txtNumero_Sinistro_InformacaoBB.Text = GrdInformacao_SinistroBB.TextMatrix(GrdInformacao_SinistroBB.Row, 0) Then
                        'Tp_Altera_SinistroBB = Tp_Altera_SinistroBB & "." & "Inativar"
                        Tp_Altera_SinistroBB = "Inativar"
                        GrdInformacao_SinistroBB.TextMatrix(.RowSel, 1) = "Inativo"
                    End If

                    If sSinistroBBInclusao = "" Then
                        sSinistroBBAlteracao = txtNumero_Sinistro_InformacaoBB.Text
                    Else
                        sSinistroBBAlteracao = sSinistroBBAlteracao & "," & txtNumero_Sinistro_InformacaoBB.Text
                    End If

                End If
            End If

        End If
    End With




End Sub

Private Sub btnBBRemover_Click()
    Dim iTotalSinistroBB As Integer
    '
    '    If Not PodeDesvincularAvisoBB(GrdInformacao_SinistroBB.TextMatrix(GrdInformacao_SinistroBB.Row, 0)) Then
    '        MsgBox "O aviso BB n�o pode ser desvinculado do aviso AB pois h� pagamento efetuado.", _
             '            vbInformation, "Aviso BB n�o desvinculado", vbOKOnly, App.EXEName
    '        Exit Sub
    '    End If

    If Not PodeDesvincularAvisoBB(GrdInformacao_SinistroBB.TextMatrix(GrdInformacao_SinistroBB.Row, 0)) Then
        MsgBox "O aviso BB n�o pode ser desvinculado do aviso AB pois h� pagamento efetuado.Aviso BB n�o desvinculado", vbInformation    ', _
                                                                                                                                         vbInformation, "Aviso BB n�o desvinculado", vbOKOnly, App.EXEName
        Exit Sub
    End If



    If txtNumero_Sinistro_InformacaoBB = GrdInformacao_SinistroBB.TextMatrix(GrdInformacao_SinistroBB.RowSel, 0) And _
       IsNumeric(GrdInformacao_SinistroBB.TextMatrix(GrdInformacao_SinistroBB.RowSel, 0)) Then


        'GrdInformacao_SinistroBB.RemoveItem (GrdInformacao_SinistroBB.Row)

        If GrdInformacao_SinistroBB.Rows = 2 Then
            GrdInformacao_SinistroBB.Rows = 1
        Else
            GrdInformacao_SinistroBB.RemoveItem (GrdInformacao_SinistroBB.RowSel)
            'sSinistroBBExclusao = txtNumero_Sinistro_InformacaoBB.Text
        End If

        If sSinistroBBExclusao = "" Then
            sSinistroBBExclusao = txtNumero_Sinistro_InformacaoBB.Text
        Else
            sSinistroBBExclusao = sSinistroBBExclusao & "," & txtNumero_Sinistro_InformacaoBB.Text
        End If

        'Tp_Altera_SinistroBB = Tp_Altera_SinistroBB & "." & "Excluir"
        Tp_Altera_SinistroBB = "Excluir"
    End If

    txtNumeroSinistroBB = ""
    txtNumero_Sinistro_InformacaoBB = ""
    mskAgencia_Aviso.Text = ""



End Sub

Private Sub BtnSelecionar_Proposta_Click()
    FrmEndereco_Escolher_Proposta.Show
    'HENRIQUE H. HENRIQUES - CONFITEC SP - INI
    If BlnExibirForm = False Then
        If FrmEndereco_Escolher_Proposta.Visible = True Then
          Unload FrmEndereco_Escolher_Proposta
        End If
    End If
    'HENRIQUE H. HENRIQUES - CONFITEC SP - FIM
End Sub

Private Sub btnVoltar_Click()

    Dim Retorno As Boolean


    If EstadoConexao(lConexaoLocal) = adStateOpen Then
        Retorno = FecharConexao(lConexaoLocal)
    End If

    'envia mensagem para aplica��o master
    Retorno = EnvioSaidaAuxiliar(txtAuxiliar.Text, FrmAvisoSinistro)

    End

End Sub

Private Sub cboCausa_Sinistro_Click()

'Call Monta_cmbSubeventoSinistro(cboCausa_Sinistro.ItemData(cboCausa_Sinistro.ListIndex))
    Call Monta_cboSubeventoSinistro(cboCausa_Sinistro.ItemData(cboCausa_Sinistro.ListIndex))    'F.BEZERRA 16/10/2012

End Sub


Private Sub CmbCodigo_Objeto_Ocorrencia_Click()
    Dim Anterior As Variant
    Dim i As Integer

    'mathayde - 29
    If Not bCarregaEnderecoDaCombo Then
        Exit Sub
    End If

    If FrmEndereco_Escolher_Proposta.GrdEndereco_Risco.Rows > 1 Then

        If Not bEfetuouAlteracaoEndereco Then
            For i = 1 To CmbCodigo_Objeto_Ocorrencia.ListCount
                If Trim(Left(CmbCodigo_Objeto_Ocorrencia.Text, 3)) = FrmEndereco_Escolher_Proposta.GrdEndereco_Risco.TextMatrix(i, 0) Then
                    TxtEndereco_Ocorrencia = FrmEndereco_Escolher_Proposta.GrdEndereco_Risco.TextMatrix(i, 1)
                    TxtBairro_Ocorrencia = FrmEndereco_Escolher_Proposta.GrdEndereco_Risco.TextMatrix(i, 2)
                    TxtMunicipio_Ocorrencia = FrmEndereco_Escolher_Proposta.GrdEndereco_Risco.TextMatrix(i, 3)
                    TxtEstado_Ocorrencia = FrmEndereco_Escolher_Proposta.GrdEndereco_Risco.TextMatrix(i, 4)
                    Exit Sub
                End If
            Next
        End If

        bEfetuouAlteracaoEndereco = False
    End If


    If CmbCodigo_Objeto_Ocorrencia.Text = "" Then
        Exit Sub
    End If

    If gbldSinistro_ID = 0 Then

        Anterior = vEnd_Risco_id
        vEnd_Risco_id = Trim(Left(CmbCodigo_Objeto_Ocorrencia.Text, 3))
        vDt_Inicio_Vigencia_Seg = Trim(Right(CmbCodigo_Objeto_Ocorrencia.Text, 10))

        If FrmEndereco_Escolher_Proposta.Existe_Resseguro(True) = True And Anterior <> vEnd_Risco_id And Anterior <> 0 Then

            If MsgBox("O endere�o de risco mudou e existe resseguro. " & _
                      "Confirma ajuste dos percentuais sugeridos " & _
                      "relativos a este novo endere�o de risco?", _
                      vbYesNo + vbQuestion + vbDefaultButton2) = vbYes Then


                FrmEndereco_Escolher_Proposta.iCod_Objeto_Segurado = vEnd_Risco_id

                For i = 0 To CmbCodigo_Objeto_Ocorrencia.ListCount
                    If Trim(CmbCodigo_Objeto_Ocorrencia.List(i)) = i Then
                        bSaiDoLoop = True
                        CmbCodigo_Objeto_Ocorrencia.ListIndex = i
                        Exit For
                    End If
                Next

                If FrmEndereco_Escolher_Proposta.Consulta_Pendencia_Analise_Resseguro(False) = True Then

                    Call FrmEndereco_Escolher_Proposta.Busca_Perc_Resseguro(gbllProposta_ID, txtEndosso.Text, Val(Left(CmbCodigo_Objeto_Ocorrencia.Text, 3)))    'Busca o percentual sugerido

                End If

            Else

                bSaiDoLoop = True

            End If
        End If
    Else

        Anterior = FrmEndereco_Escolher_Proposta.iCod_Objeto_Segurado
        FrmEndereco_Escolher_Proposta.iCod_Objeto_Segurado = Trim(Left(CmbCodigo_Objeto_Ocorrencia.Text, 3))
        vDt_Inicio_Vigencia_Seg = Trim(Right(CmbCodigo_Objeto_Ocorrencia.Text, 10))

        If FrmEndereco_Escolher_Proposta.Existe_Resseguro(False) = True And Anterior <> Trim(Left(CmbCodigo_Objeto_Ocorrencia.Text, 3)) Then
            If MsgBox("O endere�o de risco mudou e existe resseguro. " & _
                      "Confirma nova estimativa com os percentuais " & _
                      "relativos a este novo endere�o de risco?", _
                      vbYesNo + vbQuestion + vbDefaultButton2) = vbYes Then


                If FrmEndereco_Escolher_Proposta.Consulta_Pendencia_Analise_Resseguro(False) = True Then
                    'Call FrmEndereco_Escolher_Proposta.Busca_Perc_Resseguro(txtNumeroProposta.Text, txtEndosso.Text, Val(Left(CmbCodigo_Objeto_Ocorrencia.Text, 3))) 'Busca o percentual sugerido
                    Call FrmEndereco_Escolher_Proposta.Busca_Perc_Resseguro(gbllProposta_ID, txtEndosso.Text, Val(Left(CmbCodigo_Objeto_Ocorrencia.Text, 3)))    'Busca o percentual sugerido
                End If

            End If
        End If
    End If

End Sub

Private Sub CmbCodigo_Objeto_Ocorrencia_LostFocus()

    bEfetuouAlteracaoEndereco = True
    Call CmbCodigo_Objeto_Ocorrencia_Click

End Sub

Private Sub Form_Load()

    On Error GoTo Erro
    If Not bCarregou_Form Then
    
    
    '1-Inclusao / 2-Altera��o / 3-Consulta
    'InicializaModoOperacao Me, bytModoOperacao

    bytModoOperacao = 1
    glAmbiente_id = 3
    gbllProposta_ID = 24217980
    bytTipoRamo = bytTipoRamo_Vida
    Data_Sistema = Now()
    sSolicitante_id = 1004251
    
        InicializaVariaveisLocal

        IniciarConexao                                      'RETIRAR - VEM PELO PRIMEIRO FORM

        InicializaInterfaceLocal
        
'        InicializaInterface_Versao Me                      'FLAVIO.BEZERRA - RETIRADO - 13/03/2013

    End If

    Exit Sub
Erro:
    Call TrataErroGeral("Form_Load", Me.Caption)
    Call FinalizarAplicacao
End Sub

Private Sub IniciarConexao()
    On Error GoTo Erro
'    If EstadoConexao(vNunCx) = adStateClosed Then
        lConexaoLocal = AbrirConexao(gsSIGLASISTEMA, glAmbiente_id, App.Title, App.FileDescription)
'    End If

    vNunCx = lConexaoLocal

    Exit Sub

Erro:
    Call TrataErroGeral("IniciarConexao", Me.Caption)
    Call FinalizarAplicacao


End Sub
Private Sub InicializaVariaveisLocal()

    Dim sParametroLocal As String

    If Not Trata_Parametros(Command) Then
       ' FinalizarAplicacao
    Else
        Ambiente = ConvAmbiente(glAmbiente_id)

        sParametroLocal = Left(Command, InStrRev(Command, " "))
        aParametros = Split(sParametroLocal, ".")

        bytTipoRamo = aParametros(0)
        gsParamAplicacao = aParametros(1)
        bytModoOperacao = Trim(gsParamAplicacao)

        If bytModoOperacao = 1 Then

            If UBound(aParametros) >= 2 Then
                gbllProposta_ID = aParametros(2)
            End If

            If UBound(aParametros) >= 3 Then
                sSolicitante_id = aParametros(3)
            End If

        Else

            If UBound(aParametros) >= 2 Then
                gbldSinistro_ID = aParametros(2)
            End If

        End If



        bCarregou_Form = True
        bCarregou_AbaDadosGerais = False
        bCarregou_AbaDetalhamento = False
        bCarregou_AbaHistorico = False
        bCarregou_AbaEstimativas = False
        bCarregou_AbaDadosEspecificos = False


        If UBound(aParametros) >= 4 Then

            sOperacaoCosseguro = Trim(aParametros(4))

        End If


'AKIO.OKUNO - MP - INICIO - 15/02/2013
        If UBound(aParametros) >= 5 Then
            Data_Sistema = aParametros(3)
        Else
            Data_Sistema = ""
        End If
    
'        Data_Sistema = "15/02/2013"
'AKIO.OKUNO - MP - FIM - 15/02/2013
    
    End If



End Sub
Private Sub InicializaInterfaceLocal()
    On Error GoTo Erro

    Screen.MousePointer = vbHourglass

    Call CentraFrm(Me)

    LimpaCamposTelaLocal Me  'SERA ALTERADO

    If Data_Sistema = "" Then       'AKIO.OKUNO - MP - 15/02/2013
        ObterDataSistema (gsSIGLASISTEMA)
    End If                          'AKIO.OKUNO - MP - 15/02/2013
    
   
    InicializaModoOperacaoLocal

    Monta_cmbCausaSinistro

    CarregaComboSexo

    Preenche_CmbReabertura

    Preenche_Moeda

    InicializaInterfaceLocal_Form

    Monta_FmeEndereco_Ocorrencia (gbllProposta_ID)


    If bytModoOperacao <> 1 Then
        CarregaDados_Aviso
    Else
        If bytTipoRamo = bytTipoRamo_Vida Then
            Call Monta_FrameSinistrado
        End If
    End If


    Screen.MousePointer = vbNormal

    Exit Sub

Erro:
    Call TrataErroGeral("InicializaInterfaceLocal", Me.Caption)
    Call FinalizarAplicacao
    Exit Sub

    Resume
End Sub

Private Sub LimpaCamposTelaLocal(Optional objObjeto As Object)
    On Error GoTo Erro

    If Not objObjeto Is Nothing Then
        LimpaCamposTela objObjeto
    End If

    Exit Sub

Erro:
    Call TrataErroGeral("LimpaCamposTelaLocal", Me.Caption)
    Call FinalizarAplicacao

End Sub
Private Sub SelecionaDados_AbaDadosGerais()
    Dim sSql         As String
    Dim RsEndereco   As ADODB.Recordset   'FLAVIO.BEZERRA - 16/07/2013 - MP Temporaria
    
    On Error GoTo Erro

    MousePointer = vbHourglass
    
    sSql = ""
'petrauskas, ajuste SQL2014
'    sSQL = sSQL & " if isnull(OBJECT_ID('tempdb..#Sinistro_Principal'),0) >0" & vbNewLine
    sSql = sSql & " if OBJECT_ID('tempdb..#Sinistro_Principal') IS NOT NULL" & vbNewLine
    sSql = sSql & " BEGIN" & vbNewLine
    sSql = sSql & "     DROP TABLE #Sinistro_Principal" & vbNewLine
    sSql = sSql & " END" & vbNewLine
    sSql = sSql & "Create Table #Sinistro_Principal "
    sSql = sSql & "     ( Sinistro_ID                                   Numeric(11)         " & vbNewLine   ' -- Sinistro_Tb
    sSql = sSql & "     , Sucursal_Seguradora_ID                        Numeric(5)          " & vbNewLine  ' -- Sinistro_Tb
    sSql = sSql & "     , Seguradora_Cod_Susep                          Numeric(5)          " & vbNewLine  ' -- Sinistro_Tb
    sSql = sSql & "     , Apolice_ID                                    Numeric(9)          " & vbNewLine  ' -- Sinistro_Tb
    sSql = sSql & "     , Ramo_ID                                       Int                 " & vbNewLine  ' -- Sinistro_Tb
    sSql = sSql & "     , Ramo_Nome                                     VarChar(60)         " & vbNewLine  ' -- Ramo_Tb
    sSql = sSql & "     , Seguradora_Nome_Lider                         VarChar(60)         " & vbNewLine  ' -- Seguradora_Tb (Nome Lider)
    sSql = sSql & "     , Apolice_Dt_Inicio_Vigencia                    SmallDateTime       " & vbNewLine  ' -- Apolice_Tb
    sSql = sSql & "     , Apolice_Dt_Fim_Vigencia                       SmallDateTime       " & vbNewLine  ' -- Apolice_Tb
    sSql = sSql & "     , Apolice_Dt_Emissao                            SmallDateTime       " & vbNewLine  ' -- Apolice_Tb
    sSql = sSql & "     , Proposta_ID                                   Numeric(9)          " & vbNewLine  ' -- Sinistro_Tb
    sSql = sSql & "     , Produto_ID                                    Int                 " & vbNewLine  ' -- Proposta_Tb
    sSql = sSql & "     , Produto_Nome                                  VarChar(60)         " & vbNewLine  ' -- Produto_Tb
    sSql = sSql & "     , Certificado_Dt_Inicio_Vigencia                SmallDateTime       " & vbNewLine  ' -- Certificado_Tb
    sSql = sSql & "     , Certificado_Dt_Fim_Vigencia                   SmallDateTime       " & vbNewLine  ' -- Certificado_Tb
    sSql = sSql & "     , Certficado_Dt_Emissao                         SmallDateTime       " & vbNewLine  ' -- Certificado_Tb
    sSql = sSql & "     , Quantidade_Endossos                           Int                 " & vbNewLine  ' -- Count Endosso_Tb ???
    sSql = sSql & "     , Proposta_Situacao                             VarChar(1)          " & vbNewLine  ' -- Proposta_Tb
    sSql = sSql & "     , Proposta_Situacao_Descricao                   VarChar(60)         " & vbNewLine  ' -- Dado_Basico_Db..Situacao_Proposta_tb.Descricao
    sSql = sSql & "     , Tp_Cobertura_Nome                             VarChar(60)         " & vbNewLine  ' -- Tp_Cobertura_Tb
    sSql = sSql & "     , Corretor_Nome                                 VarChar(60)         " & vbNewLine  ' -- Corretor_Tb
    sSql = sSql & "     , Cliente_ID                                    Int                 " & vbNewLine  ' -- Proposta_Tb (Prop_Cliente_ID)
    sSql = sSql & "     , Cliente_Nome                                  VarChar(60)         " & vbNewLine  ' -- Cliente_Tb
    sSql = sSql & "     , Tp_Componente_ID                              TinyInt             " & vbNewLine  ' -- Escolha_Tp_Cob_Vida_Tp
    sSql = sSql & "     , Tp_Componente_Nome                            VarChar(60)         " & vbNewLine  ' -- Tp_Componente_Tb
    sSql = sSql & "     , Evento_Sinistro_ID                            Int                 " & vbNewLine  ' -- Sinistro_Tb
    sSql = sSql & "     , Evento_Sinistro_Nome                          VarChar(60)         " & vbNewLine  ' -- Evento_Sinistro_Tb
    sSql = sSql & "     , SubEvento_Sinistro_ID                         Int                 " & vbNewLine  ' -- Sinistro_Tb
    sSql = sSql & "     , SubEvento_Sinistro_Nome                       VarChar(60)         " & vbNewLine  ' -- SubEvento_Sinistro_Tb
    sSql = sSql & "     , Dt_Ocorrencia_Sinistro                        SmallDateTime       " & vbNewLine  ' -- Sinistro_Tb
    sSql = sSql & "     , Dt_Entrada_Seguradora                         SmallDateTime       " & vbNewLine  ' -- Sinistro_Tb
    sSql = sSql & "     , Dt_Aviso_Sinistro                             SmallDateTime       " & vbNewLine  ' -- Sinistro_Tb
    sSql = sSql & "     , Dt_Inclusao                                   SmallDateTIme       " & vbNewLine  ' -- Sinistro_Tb
    sSql = sSql & "     , Agencia_ID                                    Numeric(4)          " & vbNewLine  ' -- Sinistro_Tb
    sSql = sSql & "     , Sinistro_Vida_Nome                            VarChar(60)         " & vbNewLine  ' -- Sinistro_Vida_Tb (Sinistrado)
    sSql = sSql & "     , Sinistro_Vida_CPF                             VarChar(11)         " & vbNewLine  ' -- Sinistro_Vida_Tb (Sinistrado)
    sSql = sSql & "     , Sinistro_Vida_Dt_Nasc                         DateTime            " & vbNewLine  ' -- Sinistro_Vida_Tb (Sinistrado)
    sSql = sSql & "     , Sinistro_Vida_Sexo                            Char(1)             " & vbNewLine  ' -- Sinistro_Vida_Tb (Sinistrado)
    sSql = sSql & "     , Sinistrado_Cliente_ID                         Int                 " & vbNewLine  ' -- Pessoa_Fisica_Tb (Pf_Cliente_ID) (Sinistrado)
    sSql = sSql & "     , Solicitante_ID                                Int                 " & vbNewLine  ' -- Sinistro_Tb (Solicitante)
    sSql = sSql & "     , Solicitante_Sinistro_Nome                     VarChar(60)         " & vbNewLine  ' -- Solicitante_Sinistro_Tb (Solicitante)
    sSql = sSql & "     , Solicitante_Sinistro_Endereco                 VarChar(60)         " & vbNewLine  ' -- Solicitante_Sinistro_Tb (Solicitante)
    sSql = sSql & "     , Solicitante_Sinistro_Bairro                   VarChar(30)         " & vbNewLine  ' -- Solicitante_Sinistro_Tb (Solicitante)
    sSql = sSql & "     , Solicitante_Sinistro_Municipio_ID             Numeric(3)          " & vbNewLine  ' -- Solicitante_Sinistro_Tb (Solicitante)
    sSql = sSql & "     , Solicitante_Sinistro_Municipio                VarChar(60)         " & vbNewLine  ' -- Solicitante_Sinistro_Tb (Solicitante)
    sSql = sSql & "     , Solicitante_Municipio_Nome                    VarChar(60)         " & vbNewLine  ' -- Municipio_Tb (Nome) (Solicitante)
    sSql = sSql & "     , Solicitante_Sinistro_DDD1                     VarChar(4)          " & vbNewLine  ' -- Solicitante_Sinistro_Tb (Solicitante)
    sSql = sSql & "     , Solicitante_Sinistro_Telefone1                VarChar(9)          " & vbNewLine  ' -- Solicitante_Sinistro_Tb (Solicitante)
    sSql = sSql & "     , Solicitante_Sinistro_DDD2                     VarChar(4)          " & vbNewLine  ' -- Solicitante_Sinistro_Tb (Solicitante)
    sSql = sSql & "     , Solicitante_Sinistro_Telefone2                VarChar(9)          " & vbNewLine  ' -- Solicitante_Sinistro_Tb (Solicitante)
    sSql = sSql & "     , Solicitante_Sinistro_DDD3                     VarChar(4)          " & vbNewLine  ' -- Solicitante_Sinistro_Tb (Solicitante)
    sSql = sSql & "     , Solicitante_Sinistro_Telefone3                VarChar(9)          " & vbNewLine  ' -- Solicitante_Sinistro_Tb (Solicitante)
    sSql = sSql & "     , Solicitante_Sinistro_DDD4                     VarChar(4)          " & vbNewLine  ' -- Solicitante_Sinistro_Tb (Solicitante)
    sSql = sSql & "     , Solicitante_Sinistro_Telefone4                VarChar(9)          " & vbNewLine  ' -- Solicitante_Sinistro_Tb (Solicitante)
    sSql = sSql & "     , Solicitante_Sinistro_DDD5                     VarChar(4)          " & vbNewLine  ' -- Solicitante_Sinistro_Tb (Solicitante)
    sSql = sSql & "     , Solicitante_Sinistro_Telefone5                VarChar(9)          " & vbNewLine  ' -- Solicitante_Sinistro_Tb (Solicitante)
    sSql = sSql & "     , Solicitante_Sinistro_DDD_Fax                  VarChar(4)          " & vbNewLine  ' -- Solicitante_Sinistro_Tb (Solicitante)
    sSql = sSql & "     , Solicitante_Sinistro_Telefone_Fax             VarChar(9)          " & vbNewLine  ' -- Solicitante_Sinistro_Tb (Solicitante)
    sSql = sSql & "     , Solicitante_Sinistro_Tp_Telefone1             Char(1)             " & vbNewLine  ' -- Solicitante_Sinistro_Tb (Solicitante)
    sSql = sSql & "     , Solicitante_Sinistro_Tp_Telefone2             Char(1)             " & vbNewLine  ' -- Solicitante_Sinistro_Tb (Solicitante)
    sSql = sSql & "     , Solicitante_Sinistro_Tp_Telefone3             Char(1)             " & vbNewLine  ' -- Solicitante_Sinistro_Tb (Solicitante)
    sSql = sSql & "     , Solicitante_Sinistro_Tp_Telefone4             Char(1)             " & vbNewLine  ' -- Solicitante_Sinistro_Tb (Solicitante)
    sSql = sSql & "     , Solicitante_Sinistro_Tp_Telefone5             Char(1)             " & vbNewLine  ' -- Solicitante_Sinistro_Tb (Solicitante)
    sSql = sSql & "     , Solicitante_Sinistro_Ramal1                   Char(1)             " & vbNewLine  ' -- Solicitante_Sinistro_Tb (Solicitante)
    sSql = sSql & "     , Solicitante_Sinistro_Ramal2                   Char(1)             " & vbNewLine  ' -- Solicitante_Sinistro_Tb (Solicitante)
    sSql = sSql & "     , Solicitante_Sinistro_Ramal3                   Char(1)             " & vbNewLine  ' -- Solicitante_Sinistro_Tb (Solicitante)
    sSql = sSql & "     , Solicitante_Sinistro_Ramal4                   Char(1)             " & vbNewLine  ' -- Solicitante_Sinistro_Tb (Solicitante)
    sSql = sSql & "     , Solicitante_Sinistro_Ramal5                   Char(1)             " & vbNewLine  ' -- Solicitante_Sinistro_Tb (Solicitante)
    sSql = sSql & "     , Solicitante_Sinistro_Estado                   Char(2)             " & vbNewLine  ' -- Solicitante_Sinistro_Tb (Solicitante)
    sSql = sSql & "     , Sinistro_Causa_Observacao_Item                Int                 " & vbNewLine  ' -- Sinistro_Causa_Observacao_Tb
    sSql = sSql & "     , Sinistro_Causa_Observacao                     VarChar(300)        " & vbNewLine  ' -- Sinistro_Causa_Observacao_Tb
    sSql = sSql & "     , Sinistro_Situacao_ID                          Int                 " & vbNewLine  ' -- Sinistro_Tb
    sSql = sSql & "     , Sinistro_Situacao                             VarChar(60)         " & vbNewLine  ' -- Dado_Basico_Db..Sinistro_Situacao_Tb
    sSql = sSql & "     , Max_Endosso_ID                                Int                 " & vbNewLine  ' -- Endosso_Tb
    'Para consulta
    sSql = sSql & "     , Apolice_Terceiros_Num_Ordem_Co_Seguro_Aceito          Numeric(11,0)           " & vbNewLine
    sSql = sSql & "     , Apolice_Terceiros_Seg_Cod_Susep_Lider                 Numeric(5,0)            " & vbNewLine
    sSql = sSql & "     , Apolice_Terceiros_Ramo_ID                             Int                     " & vbNewLine
    sSql = sSql & "     , Apolice_Terceiros_Sucursal_Seg_Lider                  Numeric(5,0)            " & vbNewLine
    'Campos do frame Cosseguro
    sSql = sSql & "     , Cosseguro_Aceito_Num_Apolice_Lider                    Numeric(12,0)           " & vbNewLine
    sSql = sSql & "     , Cosseguro_Aceito_Ramo_Lider                           Int                     " & vbNewLine
    sSql = sSql & "     , Cosseguro_Aceito_Sucursal_Seg_Lider                   Numeric(5,0)            " & vbNewLine
    sSql = sSql & "     , Cosseguro_Aceito_Seg_Cod_Susep_Lider                  Numeric(5,0)            " & vbNewLine
    sSql = sSql & "     , Apolice_Terceiros_Apolice_ID                          Numeric(9,0)            " & vbNewLine
    sSql = sSql & "     , Seguradora_Nome                                       VarChar(60)             " & vbNewLine
    sSql = sSql & "     , Cosseguro_Ramo_Nome                                   VarChar(60)             " & vbNewLine
    sSql = sSql & "     , Sucursal_Seguradora_Nome                              VarChar(60)             " & vbNewLine
    sSql = sSql & "     , Sinistro_Endereco                                     VarChar(60)             " & vbNewLine
    sSql = sSql & "     , Sinistro_Bairro                                       VarChar(30)             " & vbNewLine
    sSql = sSql & "     , Sinistro_Estado                                       VarChar(20)             " & vbNewLine
    sSql = sSql & "     , Sinistro_Municipio                                    VarChar(30)             " & vbNewLine
    sSql = sSql & "     , Sinistro_Municipio_id                                 numeric(3)              " & vbNewLine
    sSql = sSql & "     , Sinistro_Reintegracao_IS                              char(1)                 " & vbNewLine
    sSql = sSql & "     , sinistro_lider_id                                     numeric(13)             " & vbNewLine
    sSql = sSql & "     , pgto_parcial_co_seguro                                char(1)                 " & vbNewLine
    sSql = sSql & "     , cod_objeto_sinistrado                                 int                " & vbNewLine
    sSql = sSql & "     ) " & vbNewLine
    sSql = sSql & "Create Index PK_Sinistro_ID                          on #Sinistro_Principal (Sinistro_ID)" & vbNewLine
    sSql = sSql & "Create Index FK_Proposta_ID                          on #Sinistro_Principal (Proposta_ID)" & vbNewLine
    sSql = sSql & "Create Index FK_Evento_Sinistro_ID                   on #Sinistro_Principal (Evento_Sinistro_ID)" & vbNewLine
    sSql = sSql & "Create Index FK_SubEvento_Sinistro_ID                on #Sinistro_Principal (SubEvento_Sinistro_ID)" & vbNewLine
    sSql = sSql & "Create Index FK_A                                    on #Sinistro_Principal (Sucursal_Seguradora_ID, Seguradora_Cod_Susep, Apolice_ID, Ramo_ID)" & vbNewLine
    sSql = sSql & "Create Index FK_Seguradora_Cod_Susep                 on #Sinistro_Principal (Seguradora_Cod_Susep)" & vbNewLine
    sSql = sSql & "Create Index FK_Cliente_ID                           on #Sinistro_Principal (Cliente_ID)" & vbNewLine
    sSql = sSql & "Create Index FK_Solicitante_ID                       on #Sinistro_Principal (Solicitante_ID)" & vbNewLine
    sSql = sSql & "" & vbNewLine
'     Call Conexao_ExecutarSQL(gsSIGLASISTEMA, _
                             glAmbiente_id, _
                             App.Title, _
                             App.FileDescription, _
                             sSQL, _
                             lConexaoLocal, _
                             False)




    '-->> Dados do Frame: Seguro                                                                                                                                                                                                             & vbNewLine
    '-- Dados do Sinistro                                                                                                                                                                                                                    & vbNewLine
'    sSQL = ""
    sSql = sSql & "Insert into #Sinistro_Principal " & vbNewLine
    sSql = sSql & "          ( Sinistro_ID                      " & vbNewLine
    sSql = sSql & "          , Sucursal_Seguradora_ID           " & vbNewLine
    sSql = sSql & "          , Seguradora_Cod_Susep             " & vbNewLine
    sSql = sSql & "          , Apolice_ID                       " & vbNewLine
    sSql = sSql & "          , Ramo_ID                          " & vbNewLine
    sSql = sSql & "          , Proposta_ID" & vbNewLine
    sSql = sSql & "          , Evento_Sinistro_ID" & vbNewLine
    sSql = sSql & "          , SubEvento_Sinistro_ID    " & vbNewLine
    sSql = sSql & "          , Dt_Ocorrencia_Sinistro" & vbNewLine
    sSql = sSql & "          , Dt_Entrada_Seguradora" & vbNewLine
    sSql = sSql & "          , Dt_Aviso_Sinistro    " & vbNewLine
    sSql = sSql & "          , Dt_Inclusao          " & vbNewLine
    sSql = sSql & "          , Agencia_ID" & vbNewLine
    sSql = sSql & "          , Solicitante_ID                       " & vbNewLine
    sSql = sSql & "          , Sinistro_Situacao_ID" & vbNewLine
    sSql = sSql & "          , Sinistro_Endereco " & vbNewLine
    sSql = sSql & "          , Sinistro_Bairro " & vbNewLine
    sSql = sSql & "          , Sinistro_Municipio " & vbNewLine
    sSql = sSql & "          , Sinistro_Municipio_id " & vbNewLine
    sSql = sSql & "          , Sinistro_Estado " & vbNewLine
    sSql = sSql & "          , Sinistro_Reintegracao_IS " & vbNewLine
    sSql = sSql & "          , sinistro_lider_id" & vbNewLine
    sSql = sSql & "          , pgto_parcial_co_seguro" & vbNewLine
    sSql = sSql & "          )" & vbNewLine
    sSql = sSql & "Select Sinistro_ID                       " & vbNewLine
    sSql = sSql & "     , Sucursal_Seguradora_ID            " & vbNewLine
    sSql = sSql & "     , Seguradora_Cod_Susep              " & vbNewLine
    sSql = sSql & "     , Apolice_ID                        " & vbNewLine
    sSql = sSql & "     , Ramo_ID                           " & vbNewLine
    sSql = sSql & "     , Proposta_ID           " & vbNewLine
    sSql = sSql & "     , Evento_Sinistro_ID            " & vbNewLine
    sSql = sSql & "     , SubEvento_Sinistro_ID " & vbNewLine
    sSql = sSql & "     , Dt_Ocorrencia_Sinistro" & vbNewLine
    sSql = sSql & "     , Dt_Entrada_Seguradora" & vbNewLine
    sSql = sSql & "     , Dt_Aviso_Sinistro " & vbNewLine
    sSql = sSql & "     , Dt_Inclusao           " & vbNewLine
    sSql = sSql & "     , Agencia_ID" & vbNewLine
    sSql = sSql & "     , Solicitante_ID                        " & vbNewLine
    sSql = sSql & "     , Situacao" & vbNewLine
    sSql = sSql & "     , Endereco " & vbNewLine
    sSql = sSql & "     , Bairro " & vbNewLine
    sSql = sSql & "     , Municipio " & vbNewLine
    sSql = sSql & "     , Municipio_id " & vbNewLine
    sSql = sSql & "     , Estado " & vbNewLine
    sSql = sSql & "     , processa_reintegracao_IS " & vbNewLine
    sSql = sSql & "     , sinistro_id_lider" & vbNewLine
    sSql = sSql & "     , pgto_parcial_co_seguro" & vbNewLine
    sSql = sSql & "  From Seguros_Db.Dbo.Sinistro_Tb                    Sinistro_Tb  WITH (NOLOCK) " & vbNewLine
    sSql = sSql & " Where Sinistro_ID = " & gbldSinistro_ID

    Call Conexao_ExecutarSQL(gsSIGLASISTEMA, _
                             glAmbiente_id, _
                             App.Title, _
                             App.FileDescription, _
                             sSql, _
                             lConexaoLocal, _
                             False)


    '--Situacao Sinistro
    sSql = ""
    sSql = sSql & "Update #Sinistro_Principal" & vbNewLine
    sSql = sSql & "   Set Sinistro_Situacao                             = Sinistro_Situacao_Tb.Situacao" & vbNewLine
    sSql = sSql & "  From Dado_Basico_Db..Sinistro_Situacao_Tb          Sinistro_Situacao_Tb  WITH (NOLOCK) " & vbNewLine
    sSql = sSql & "  Join #Sinistro_Principal  WITH (NOLOCK) " & vbNewLine
    sSql = sSql & "    on #Sinistro_Principal.Sinistro_Situacao_ID      = Sinistro_Situacao_Tb.Situacao_ID" & vbNewLine
    sSql = sSql & "" & vbNewLine
    '-- Ramo_Tb                                                                                                                                                                                                                              & vbNewLine
    sSql = sSql & "Update #Sinistro_Principal" & vbNewLine
    sSql = sSql & "   Set Ramo_Nome                                     = Ramo_Tb.Nome" & vbNewLine
    sSql = sSql & "  From Seguros_Db.Dbo.Ramo_Tb                        Ramo_Tb  WITH (NOLOCK) " & vbNewLine
    sSql = sSql & "  Join #Sinistro_Principal  WITH (NOLOCK) " & vbNewLine
    sSql = sSql & " on #Sinistro_Principal.Ramo_ID                      = Ramo_Tb.Ramo_ID" & vbNewLine
    '-- Lider                                                                                                                                                                                                                                & vbNewLine
    sSql = sSql & "Update #Sinistro_Principal" & vbNewLine
    sSql = sSql & "   Set Seguradora_Nome_Lider                         = Seguradora_Tb.Nome" & vbNewLine
    sSql = sSql & "  From Seguros_Db.Dbo.Seguradora_Tb                  Seguradora_Tb WITH (NOLOCK, Index = PK_seguradora)" & vbNewLine
    sSql = sSql & "  Join #Sinistro_Principal                           WITH (NOLOCK, Index = FK_Seguradora_Cod_Susep)" & vbNewLine
    sSql = sSql & "    on #Sinistro_Principal.Seguradora_Cod_Susep      = Seguradora_Tb.Seguradora_Cod_Susep " & vbNewLine
    sSql = sSql & "" & vbNewLine
    '-- Objeto Sinistrado                                                                                                                                                                                                                                & vbNewLine
    sSql = sSql & "Update #Sinistro_Principal" & vbNewLine
    sSql = sSql & "   Set cod_objeto_sinistrado = sinistro_re_tb.cod_objeto_segurado" & vbNewLine
    sSql = sSql & "  From Seguros_Db.Dbo.sinistro_re_tb sinistro_re_tb" & vbNewLine
    sSql = sSql & "  Join #Sinistro_Principal    " & vbNewLine
    sSql = sSql & "    on #Sinistro_Principal.sinistro_id      = sinistro_re_tb.sinistro_id " & vbNewLine
    sSql = sSql & "" & vbNewLine
    '-- Solicitante                                                                                                                                                                                                                          & vbNewLine
    sSql = sSql & "Update #Sinistro_Principal" & vbNewLine
    sSql = sSql & "   Set Solicitante_Sinistro_Nome                     = Solicitante_Sinistro_Tb.Nome" & vbNewLine
    sSql = sSql & "     , Solicitante_Sinistro_Endereco                 = Solicitante_Sinistro_Tb.Endereco" & vbNewLine
    sSql = sSql & "     , Solicitante_Sinistro_Bairro                   = Solicitante_Sinistro_Tb.Bairro" & vbNewLine
    sSql = sSql & "     , Solicitante_Sinistro_Municipio_ID             = Solicitante_Sinistro_Tb.Municipio_ID" & vbNewLine
    sSql = sSql & "     , Solicitante_Sinistro_Municipio                = Solicitante_Sinistro_Tb.Municipio " & vbNewLine
    sSql = sSql & "     , Solicitante_Sinistro_DDD1                     = Solicitante_Sinistro_Tb.DDD" & vbNewLine
    sSql = sSql & "     , Solicitante_Sinistro_Telefone1                = Solicitante_Sinistro_Tb.Telefone" & vbNewLine
    sSql = sSql & "     , Solicitante_Sinistro_DDD2                     = Solicitante_Sinistro_Tb.DDD1" & vbNewLine
    sSql = sSql & "     , Solicitante_Sinistro_Telefone2                = Solicitante_Sinistro_Tb.Telefone1" & vbNewLine
    sSql = sSql & "     , Solicitante_Sinistro_DDD3                     = Solicitante_Sinistro_Tb.DDD2" & vbNewLine
    sSql = sSql & "     , Solicitante_Sinistro_Telefone3                = Solicitante_Sinistro_Tb.Telefone2" & vbNewLine
    sSql = sSql & "     , Solicitante_Sinistro_DDD4                     = Solicitante_Sinistro_Tb.DDD3" & vbNewLine
    sSql = sSql & "     , Solicitante_Sinistro_Telefone4                = Solicitante_Sinistro_Tb.Telefone3" & vbNewLine
    sSql = sSql & "     , Solicitante_Sinistro_DDD5                     = Solicitante_Sinistro_Tb.DDD4" & vbNewLine
    sSql = sSql & "     , Solicitante_Sinistro_Telefone5                = Solicitante_Sinistro_Tb.Telefone4" & vbNewLine
    sSql = sSql & "     , Solicitante_Sinistro_Tp_Telefone1             = Solicitante_Sinistro_Tb.Tp_Telefone" & vbNewLine
    sSql = sSql & "     , Solicitante_Sinistro_Tp_Telefone2             = Solicitante_Sinistro_Tb.Tp_Telefone1" & vbNewLine
    sSql = sSql & "     , Solicitante_Sinistro_Tp_Telefone3             = Solicitante_Sinistro_Tb.Tp_Telefone2" & vbNewLine
    sSql = sSql & "     , Solicitante_Sinistro_Tp_Telefone4             = Solicitante_Sinistro_Tb.Tp_Telefone3" & vbNewLine
    sSql = sSql & "     , Solicitante_Sinistro_Tp_Telefone5             = Solicitante_Sinistro_Tb.Tp_Telefone4" & vbNewLine
    sSql = sSql & "     , Solicitante_Sinistro_Ramal1                   = Solicitante_Sinistro_Tb.Ramal" & vbNewLine
    sSql = sSql & "     , Solicitante_Sinistro_Ramal2                   = Solicitante_Sinistro_Tb.Ramal1" & vbNewLine
    sSql = sSql & "     , Solicitante_Sinistro_Ramal3                   = Solicitante_Sinistro_Tb.Ramal2" & vbNewLine
    sSql = sSql & "     , Solicitante_Sinistro_Ramal4                   = Solicitante_Sinistro_Tb.Ramal3" & vbNewLine
    sSql = sSql & "     , Solicitante_Sinistro_Ramal5                   = Solicitante_Sinistro_Tb.Ramal4" & vbNewLine
    sSql = sSql & "     , Solicitante_Sinistro_Estado                   = Case When Solicitante_Sinistro_Tb.Estado = 'XX' or Solicitante_Sinistro_Tb.Estado = '--' " & vbNewLine
    sSql = sSql & "                                                            Then ''" & vbNewLine
    sSql = sSql & "                                                            Else Solicitante_Sinistro_Tb.Estado" & vbNewLine
    sSql = sSql & "                                                       End" & vbNewLine
    sSql = sSql & "     , Solicitante_Sinistro_DDD_Fax                  = Solicitante_Sinistro_Tb.DDD_Fax" & vbNewLine
    sSql = sSql & "     , Solicitante_Sinistro_Telefone_Fax             = Solicitante_Sinistro_Tb.Telefone_Fax" & vbNewLine
    sSql = sSql & "  From Seguros_Db.Dbo.Solicitante_Sinistro_Tb        Solicitante_Sinistro_Tb WITH (NOLOCK, Index = PK_solicitante_sinistro)" & vbNewLine
    sSql = sSql & "  Join #Sinistro_Principal                           WITH (NOLOCK,  Index = FK_Solicitante_ID)" & vbNewLine
    sSql = sSql & "    on #Sinistro_Principal.Solicitante_ID            = Solicitante_Sinistro_Tb.Solicitante_ID" & vbNewLine
    sSql = sSql & "" & vbNewLine
    '-- Dados do municipio
    sSql = sSql & "Update #Sinistro_Principal" & vbNewLine
    sSql = sSql & "   set Solicitante_Municipio_Nome                    = Municipio_Tb.Nome" & vbNewLine
    sSql = sSql & "  From #Sinistro_Principal WITH (NOLOCK)                          " & vbNewLine
    sSql = sSql & "  Join Seguros_Db.Dbo.Municipio_Tb                   Municipio_Tb WITH (NOLOCK, Index = PK_municipio)" & vbNewLine
    sSql = sSql & "    on Municipio_Tb.Municipio_ID                     = #Sinistro_Principal.Solicitante_Sinistro_Municipio_ID" & vbNewLine
    '-- Dados da Proposta                                                                                                                                                                                                                    & vbNewLine
    sSql = sSql & "Update #Sinistro_Principal" & vbNewLine
    sSql = sSql & "   Set Produto_ID                                    = Proposta_Tb.Produto_ID" & vbNewLine
    sSql = sSql & "     , Proposta_Situacao                             = Proposta_Tb.Situacao" & vbNewLine
    sSql = sSql & "     , Cliente_ID                                    = Proposta_Tb.Prop_Cliente_ID" & vbNewLine
    sSql = sSql & "     , Quantidade_Endossos                           = Proposta_Tb.Num_Endosso" & vbNewLine
    sSql = sSql & "  From Seguros_Db.Dbo.Proposta_Tb                    Proposta_Tb  WITH (NOLOCK) " & vbNewLine
    sSql = sSql & "  Join #Sinistro_Principal                           WITH (NOLOCK,  Index = FK_Proposta_ID)" & vbNewLine
    sSql = sSql & "    on Proposta_Tb.Proposta_ID                       = #Sinistro_Principal.Proposta_ID" & vbNewLine
    sSql = sSql & "" & vbNewLine
    '-- Dados do Produto
    sSql = sSql & "Update #Sinistro_Principal" & vbNewLine
    sSql = sSql & "   set Produto_Nome                                  = Produto_Tb.Nome   " & vbNewLine
    sSql = sSql & "  From Seguros_Db.Dbo.Produto_Tb                     Produto_Tb  WITH (NOLOCK) " & vbNewLine
    sSql = sSql & "  Join #Sinistro_Principal  WITH (NOLOCK) " & vbNewLine
    sSql = sSql & "    on #Sinistro_Principal.Produto_ID                = Produto_Tb.Produto_ID " & vbNewLine
    sSql = sSql & "" & vbNewLine
    '-- Situacao Proposta                                                                                                                                                                                                                    & vbNewLine
    sSql = sSql & "Update #Sinistro_Principal" & vbNewLine
    sSql = sSql & "   Set Proposta_Situacao_Descricao                   = Situacao_Proposta_Tb.Descricao_Situacao" & vbNewLine
    sSql = sSql & "  From Dado_Basico_Db..Situacao_Proposta_Tb          Situacao_Proposta_Tb WITH (NOLOCK) " & vbNewLine
    sSql = sSql & "  Join #Sinistro_Principal  WITH (NOLOCK) " & vbNewLine
    sSql = sSql & "    on #Sinistro_Principal.Proposta_Situacao         = upper(Situacao_Proposta_Tb.Situacao)" & vbNewLine
    sSql = sSql & "" & vbNewLine

    Call Conexao_ExecutarSQL(gsSIGLASISTEMA, _
                             glAmbiente_id, _
                             App.Title, _
                             App.FileDescription, _
                             sSql, _
                             lConexaoLocal, _
                             False)
    '-- Dados da Apolice                                                                                                                                                                                                                     & vbNewLine
    sSql = ""
    sSql = sSql & "Declare @Max_Endosso_ID                              int" & vbNewLine
    sSql = sSql & "" & vbNewLine
    sSql = sSql & "Select @Max_Endosso_ID                               = Max(Endosso_Tb.Endosso_ID)" & vbNewLine
    sSql = sSql & "  From Seguros_Db.Dbo.Endosso_Tb Endosso_Tb           WITH (NOLOCK) " & vbNewLine
    sSql = sSql & "  Join #Sinistro_Principal  WITH (NOLOCK) " & vbNewLine
    sSql = sSql & "    on #Sinistro_Principal.Proposta_ID               = Endosso_Tb.Proposta_ID" & vbNewLine
    sSql = sSql & " Where Endosso_Tb.Tp_Endosso_ID                      = 250" & vbNewLine
    sSql = sSql & " " & vbNewLine
    sSql = sSql & "UPdate #Sinistro_Principal" & vbNewLine
    sSql = sSql & "   Set Max_Endosso_ID = @Max_Endosso_ID" & vbNewLine
    sSql = sSql & "" & vbNewLine
    sSql = sSql & "UPdate #Sinistro_Principal" & vbNewLine
    sSql = sSql & "   Set Apolice_Dt_Inicio_Vigencia                    = Proposta_Adesao_Tb.Dt_Inicio_Vigencia" & vbNewLine
    sSql = sSql & "  From #Sinistro_Principal " & vbNewLine
    sSql = sSql & "  Join Seguros_Db.Dbo.Proposta_Adesao_Tb             Proposta_Adesao_Tb  WITH (NOLOCK) "
    sSql = sSql & "    on #Sinistro_Principal.Proposta_ID               = Proposta_Adesao_Tb.Proposta_ID" & vbNewLine
    sSql = sSql & " Where #Sinistro_Principal.Produto_ID                = 721" & vbNewLine
    sSql = sSql & "" & vbNewLine
    sSql = sSql & "UPdate #Sinistro_Principal" & vbNewLine
    sSql = sSql & "   Set Apolice_Dt_Fim_Vigencia                       = Endosso_Tb.Dt_Fim_Vigencia_End" & vbNewLine
    sSql = sSql & "  From Seguros_Db.Dbo.Endosso_Tb                     Endosso_Tb  WITH (NOLOCK) " & vbNewLine
    sSql = sSql & "  Join #Sinistro_Principal" & vbNewLine
    sSql = sSql & "    on #Sinistro_Principal.Proposta_ID               = Endosso_Tb.Proposta_ID" & vbNewLine
    sSql = sSql & " Where Endosso_Tb.Endosso_ID                         = @Max_Endosso_ID" & vbNewLine
    sSql = sSql & "   and Endosso_Tb.Tp_Endosso_ID                      = 250" & vbNewLine
    sSql = sSql & "   and #Sinistro_Principal.Produto_ID                = 721" & vbNewLine
    sSql = sSql & "" & vbNewLine
    sSql = sSql & "Select Proposta_Adesao_Tb.Proposta_ID" & vbNewLine
    sSql = sSql & "     , Proposta_Adesao_Tb.Dt_Inicio_Vigencia" & vbNewLine
    sSql = sSql & "     , Proposta_Adesao_Tb.Dt_Fim_Vigencia" & vbNewLine
    sSql = sSql & "  Into #Proposta_Adesao" & vbNewLine
    sSql = sSql & "  From Seguros_Db.Dbo.Proposta_Adesao_Tb             Proposta_Adesao_Tb  WITH (NOLOCK) " & vbNewLine
    sSql = sSql & "  Join #Sinistro_Principal" & vbNewLine
    sSql = sSql & "    on #Sinistro_Principal.Proposta_ID               = Proposta_Adesao_Tb.Proposta_ID" & vbNewLine
    sSql = sSql & "" & vbNewLine
    sSql = sSql & "Update #Sinistro_Principal" & vbNewLine
    sSql = sSql & "   Set Apolice_Dt_Inicio_Vigencia                    = Dt_Inicio_Vigencia" & vbNewLine
    sSql = sSql & "  , Apolice_Dt_Fim_Vigencia                          = Dt_Fim_Vigencia" & vbNewLine
    sSql = sSql & "  From #Proposta_Adesao" & vbNewLine
    sSql = sSql & "  Join #Sinistro_Principal" & vbNewLine
    sSql = sSql & "    on #Sinistro_Principal.Proposta_ID               = #Proposta_Adesao.Proposta_ID " & vbNewLine
    sSql = sSql & "   and (#Sinistro_Principal.Apolice_Dt_Inicio_Vigencia is null" & vbNewLine
    sSql = sSql & "    and #Sinistro_Principal.Apolice_Dt_Fim_Vigencia  is null" & vbNewLine
    sSql = sSql & "    and #Sinistro_Principal.Produto_ID               = 721 )" & vbNewLine
    sSql = sSql & "    or (#Sinistro_Principal.Produto_ID               <> 721)" & vbNewLine
    sSql = sSql & "" & vbNewLine
    sSql = sSql & "UPdate #Sinistro_Principal" & vbNewLine
    sSql = sSql & "   Set Apolice_Dt_Inicio_Vigencia                    = Apolice_Tb.Dt_Inicio_Vigencia" & vbNewLine
    sSql = sSql & "     , Apolice_Dt_Fim_Vigencia                       = Apolice_Tb.Dt_Fim_Vigencia" & vbNewLine
    sSql = sSql & "     , Apolice_Dt_Emissao                                = Apolice_Tb.Dt_Emissao" & vbNewLine
    sSql = sSql & "  From Seguros_Db.Dbo.Apolice_Tb Apolice_Tb           WITH (NOLOCK) " & vbNewLine
    sSql = sSql & "  Join #Sinistro_Principal" & vbNewLine
    sSql = sSql & "    on #Sinistro_Principal.Apolice_ID                = Apolice_Tb.Apolice_ID" & vbNewLine
    sSql = sSql & "   and #Sinistro_Principal.Sucursal_Seguradora_ID    = Apolice_Tb.Sucursal_Seguradora_ID" & vbNewLine
    sSql = sSql & "   and #Sinistro_Principal.Seguradora_Cod_Susep      = Apolice_Tb.Seguradora_Cod_Susep" & vbNewLine
    sSql = sSql & "   and #Sinistro_Principal.Ramo_ID                   = Apolice_Tb.Ramo_ID" & vbNewLine
    sSql = sSql & "  left Join #Proposta_Adesao" & vbNewLine
    sSql = sSql & "    on #Sinistro_Principal.Proposta_ID               = #Proposta_Adesao.Proposta_ID " & vbNewLine
    sSql = sSql & " Where #Sinistro_Principal.Produto_ID                <> 721" & vbNewLine
    sSql = sSql & "   and #Proposta_Adesao.Proposta_ID                  is null" & vbNewLine
    sSql = sSql & "" & vbNewLine
    '-- Dados do Certificado                                                                                                                                                                                                                 & vbNewLine
    sSql = sSql & "Update #Sinistro_Principal" & vbNewLine
    sSql = sSql & "   Set Certificado_Dt_Inicio_Vigencia                = Certificado_Tb.Dt_Inicio_Vigencia" & vbNewLine
    sSql = sSql & "  , Certificado_Dt_Fim_Vigencia                      = Certificado_Tb.Dt_Fim_Vigencia" & vbNewLine
    sSql = sSql & "  , Certficado_Dt_Emissao                            = Certificado_Tb.Dt_Emissao" & vbNewLine
    sSql = sSql & "  From Seguros_Db.Dbo.Certificado_Tb                 Certificado_Tb  WITH (NOLOCK) " & vbNewLine
    sSql = sSql & "  Join #Sinistro_Principal                      WITH (Index = FK_Proposta_ID)" & vbNewLine
    sSql = sSql & "    on Certificado_Tb.Proposta_ID                    = #Sinistro_Principal.Proposta_ID   " & vbNewLine
    sSql = sSql & "" & vbNewLine
    sSql = sSql & "" & vbNewLine
    sSql = sSql & "Select Top 1 Corretor_Tb.Nome" & vbNewLine
    sSql = sSql & "  , Corretor_Tb.Situacao" & vbNewLine
    sSql = sSql & "  Into #Corretor" & vbNewLine
    sSql = sSql & "  From Seguros_Db.Dbo.Corretor_Tb                    Corretor_Tb  WITH (NOLOCK) " & vbNewLine
    sSql = sSql & "  Join Seguros_Db.Dbo.Corretagem_Tb                  Corretagem_Tb  WITH (NOLOCK) " & vbNewLine
    sSql = sSql & "    on Corretagem_Tb.Corretor_ID                     = Corretor_Tb.Corretor_ID" & vbNewLine
    sSql = sSql & "  Join Seguros_Db.Dbo.Proposta_Tb                    Proposta_Tb  WITH (NOLOCK) " & vbNewLine
    sSql = sSql & "    on Proposta_Tb.Proposta_ID                       = Corretagem_Tb.Proposta_ID" & vbNewLine
    sSql = sSql & "  Join #Sinistro_Principal" & vbNewLine
    sSql = sSql & "    on #Sinistro_Principal.Produto_ID                = Proposta_Tb.Produto_ID" & vbNewLine
    sSql = sSql & " Where Corretor_Tb.Situacao = 'a'    " & vbNewLine
    sSql = sSql & "" & vbNewLine
    sSql = sSql & "" & vbNewLine
    sSql = sSql & "Update #Sinistro_Principal" & vbNewLine
    sSql = sSql & "   Set Corretor_Nome                                 = #Corretor.Nome" & vbNewLine
    sSql = sSql & "  From #Corretor" & vbNewLine
    sSql = sSql & "" & vbNewLine
    '-- Cliente_Tb                                                                                                                                                                                                                           & vbNewLine
    sSql = sSql & "Update #Sinistro_Principal" & vbNewLine
    sSql = sSql & "   Set Cliente_Nome                                  = Cliente_Tb.Nome" & vbNewLine
    sSql = sSql & "  From Seguros_Db.Dbo.Cliente_Tb                     Cliente_Tb  WITH (NOLOCK) " & vbNewLine
    sSql = sSql & "  Join #Sinistro_Principal                      WITH (Index = FK_Cliente_ID)" & vbNewLine
    sSql = sSql & "    on #Sinistro_Principal.Cliente_ID                = Cliente_Tb.Cliente_ID " & vbNewLine
    sSql = sSql & "" & vbNewLine
    '-- Tp_Cobertura_Tb                                                                                                                                                                                                                      & vbNewLine
    '-- Cobertura Principal                                                                                                                                                                                                                  & vbNewLine
    sSql = sSql & "Update #Sinistro_Principal" & vbNewLine
    sSql = sSql & "   Set Tp_Componente_Nome                            = Case When Proposta_Complementar_Tb.Prop_Cliente_ID is null" & vbNewLine
    sSql = sSql & "                                                         Then 'TITULAR'" & vbNewLine
    sSql = sSql & "                                                         Else 'CONJUGE'" & vbNewLine
    sSql = sSql & "                                                       End" & vbNewLine
    sSql = sSql & "  From #Sinistro_Principal" & vbNewLine
    sSql = sSql & "  Left Join Seguros_Db.Dbo.Proposta_Complementar_Tb  Proposta_Complementar_Tb   WITH (NOLOCK) " & vbNewLine
    sSql = sSql & "    on Proposta_Complementar_Tb.Prop_Cliente_ID      = #Sinistro_Principal.CLiente_ID" & vbNewLine
    sSql = sSql & "" & vbNewLine
    sSql = sSql & "Update #Sinistro_Principal" & vbNewLine
    sSql = sSql & "   Set Tp_Cobertura_Nome                             = Tp_Cobertura_Tb.Nome" & vbNewLine
    sSql = sSql & "  From Seguros_Db.Dbo.Tp_Cob_Comp_Tb                 Tp_Cob_Comp_Tb  WITH (NOLOCK) " & vbNewLine
    sSql = sSql & "  Join Seguros_Db.Dbo.Tp_Cobertura_Tb                Tp_Cobertura_Tb  WITH (NOLOCK) " & vbNewLine
    sSql = sSql & "    on Tp_Cobertura_Tb.Tp_Cobertura_ID               = Tp_Cob_Comp_Tb.Tp_Cobertura_ID" & vbNewLine
    sSql = sSql & "  Join Seguros_Db.Dbo.Escolha_Tp_Cob_Vida_Tb         Escolha_Tp_Cob_Vida_Tb  WITH (NOLOCK) " & vbNewLine
    sSql = sSql & "    on Escolha_Tp_Cob_Vida_Tb.Tp_Cob_Comp_ID         = Tp_Cob_Comp_Tb.Tp_Cob_Comp_ID" & vbNewLine
    sSql = sSql & "   and Escolha_Tp_Cob_Vida_Tb.Tp_Componente_ID       = Tp_Cob_Comp_Tb.Tp_Componente_ID " & vbNewLine
    sSql = sSql & "  Join Seguros_Db.Dbo.Tp_Componente_Tb               Tp_Componente_Tb  WITH (NOLOCK) " & vbNewLine
    sSql = sSql & "    on Tp_Componente_Tb.Tp_Componente_ID             = Tp_Cob_Comp_Tb.Tp_Componente_ID " & vbNewLine
    sSql = sSql & "  Join #Sinistro_Principal                      WITH (Index = FK_Proposta_ID)" & vbNewLine
    sSql = sSql & "    on #Sinistro_Principal.Proposta_ID               = Escolha_Tp_Cob_Vida_Tb.Proposta_ID" & vbNewLine
    sSql = sSql & " Where Escolha_Tp_Cob_Vida_Tb.Class_Tp_Cobertura     = 'B'" & vbNewLine
    sSql = sSql & "" & vbNewLine

    '-->> Dados do Frame: Aviso                                                                                                                                                                                                              & vbNewLine
    '-- Evento_Sinistro_Tb                                                                                                                                                                                                                   & vbNewLine
    sSql = sSql & "UPdate #Sinistro_Principal" & vbNewLine
    sSql = sSql & "   Set Evento_Sinistro_Nome                          = Evento_Sinistro_Tb.Nome" & vbNewLine
    sSql = sSql & "  From #Sinistro_Principal                      WITH (Index = FK_Evento_Sinistro_ID)" & vbNewLine
    sSql = sSql & "  Join Seguros_Db.Dbo.Evento_Sinistro_Tb             Evento_Sinistro_Tb  WITH (NOLOCK) " & vbNewLine
    sSql = sSql & "    on Evento_Sinistro_Tb.Evento_Sinistro_ID         = #Sinistro_Principal.Evento_Sinistro_ID " & vbNewLine
    sSql = sSql & "" & vbNewLine
    '-- SubEvento_Sinistro_Tb                                                                                                                                                                                                                & vbNewLine
    sSql = sSql & "UPdate #Sinistro_Principal" & vbNewLine
    sSql = sSql & "   Set SubEvento_Sinistro_Nome                       = SubEvento_Sinistro_Tb.Nome" & vbNewLine
    sSql = sSql & "  From #Sinistro_Principal                      WITH (Index = FK_SubEvento_Sinistro_ID)" & vbNewLine
    sSql = sSql & "  Join Seguros_Db.Dbo.SubEvento_Sinistro_Tb          SubEvento_Sinistro_Tb  WITH (NOLOCK) " & vbNewLine
    sSql = sSql & "    on SubEvento_Sinistro_Tb.SubEvento_Sinistro_ID   = #Sinistro_Principal.SubEvento_Sinistro_ID " & vbNewLine
    sSql = sSql & "     " & vbNewLine
    'Dados da Grid: Aviso" & vbNewLine
    sSql = sSql & "Select Sinistro_BB_Tb.Sinistro_Bb" & vbNewLine
    sSql = sSql & "  , Sinistro_BB_Tb.Dt_Fim_Vigencia" & vbNewLine
    sSql = sSql & "  , Agencia_ID                                       = #Sinistro_Principal.Agencia_ID" & vbNewLine
    sSql = sSql & "  , Situacao                                         = Case When Sinistro_BB_Tb.Dt_Fim_Vigencia is Null" & vbNewLine
    sSql = sSql & "                                                       Then 'Ativo'" & vbNewLine
    sSql = sSql & "                                                       Else 'Inativo'" & vbNewLine
    sSql = sSql & "                                                       End" & vbNewLine
    sSql = sSql & "  Into #DadosGerais_Aviso" & vbNewLine
    sSql = sSql & "  From Seguros_Db.Dbo.Sinistro_BB_Tb                 Sinistro_BB_Tb  WITH (NOLOCK) " & vbNewLine
    sSql = sSql & "  Join #Sinistro_Principal                      WITH (Index = PK_Sinistro_ID)" & vbNewLine
    sSql = sSql & "    on #Sinistro_Principal.Sinistro_ID               = Sinistro_BB_Tb.Sinistro_ID" & vbNewLine
    sSql = sSql & " Order By Sinistro_BB" & vbNewLine
    sSql = sSql & "     " & vbNewLine
    sSql = sSql & "     " & vbNewLine

    'DADOS DO SINISTRADO" & vbNewLine
    sSql = sSql & "Update #Sinistro_Principal " & vbNewLine
    sSql = sSql & "   Set Sinistro_Vida_Nome                            = IsNull(Sinistro_Vida_Tb.Nome, '')" & vbNewLine
    sSql = sSql & "     , Sinistro_Vida_CPF                             = IsNull(Sinistro_Vida_Tb.Cpf, '')" & vbNewLine
    sSql = sSql & "     , Sinistro_Vida_Dt_Nasc                         = IsNull(Sinistro_Vida_Tb.Dt_Nascimento, '')" & vbNewLine
    sSql = sSql & "     , Sinistro_Vida_Sexo                            = IsNull(Sinistro_Vida_Tb.Sexo, '') " & vbNewLine
    sSql = sSql & "  From Seguros_Db.Dbo.Sinistro_Vida_Tb               Sinistro_Vida_Tb  WITH (NOLOCK) " & vbNewLine
    sSql = sSql & "  Join #Sinistro_Principal                      WITH (Index = PK_Sinistro_ID)" & vbNewLine
    sSql = sSql & "    on #Sinistro_Principal.Sinistro_ID               = Sinistro_Vida_Tb.Sinistro_ID" & vbNewLine
    sSql = sSql & "     " & vbNewLine
    sSql = sSql & "Update #Sinistro_Principal" & vbNewLine
    sSql = sSql & "   Set Sinistrado_Cliente_ID                         = Pessoa_Fisica_Tb.Pf_Cliente_ID" & vbNewLine
    sSql = sSql & "  From Seguros_Db.Dbo.Pessoa_Fisica_Tb               Pessoa_Fisica_Tb  WITH (NOLOCK) " & vbNewLine
    sSql = sSql & "  Join #Sinistro_Principal " & vbNewLine
    sSql = sSql & "    on #Sinistro_Principal.Sinistro_Vida_CPF         = Pessoa_Fisica_Tb.CPF" & vbNewLine
    sSql = sSql & "   and #Sinistro_Principal.Sinistro_Vida_Dt_Nasc     = Pessoa_Fisica_Tb.Dt_Nascimento" & vbNewLine
    sSql = sSql & "   and #Sinistro_Principal.Sinistro_Vida_Sexo        = Pessoa_Fisica_Tb.Sexo" & vbNewLine

    'DADOS DO COSSEGURO
    sSql = sSql & "UPdate #Sinistro_Principal                                                                                   " & vbNewLine
    sSql = sSql & "   Set Apolice_Terceiros_Apolice_ID                      = Apolice_Terceiros_tb.apolice_id                   " & vbNewLine
    sSql = sSql & "     , Apolice_Terceiros_Num_Ordem_Co_Seguro_Aceito      = Apolice_Terceiros_Tb.num_ordem_co_seguro_aceito   " & vbNewLine
    sSql = sSql & "     , Apolice_Terceiros_Seg_Cod_Susep_Lider             = Apolice_Terceiros_tb.seg_cod_susep_lider          " & vbNewLine
    sSql = sSql & "     , Apolice_Terceiros_Ramo_ID                         = Apolice_Terceiros_Tb.Ramo_ID                      " & vbNewLine
    sSql = sSql & "     , Apolice_Terceiros_Sucursal_Seg_Lider              = Apolice_Terceiros_Tb.Sucursal_Seg_Lider           " & vbNewLine
    sSql = sSql & "  from Seguros_Db.Dbo.Apolice_Terceiros_Tb               Apolice_Terceiros_Tb WITH (NOLOCK)                       " & vbNewLine
    sSql = sSql & "  Join #Sinistro_Principal                                                                                   " & vbNewLine
    sSql = sSql & "    on #Sinistro_Principal.Apolice_ID                    = Apolice_Terceiros_Tb.Apolice_ID                   " & vbNewLine
    sSql = sSql & "   and #Sinistro_Principal.Ramo_ID                       = Apolice_Terceiros_Tb.Ramo_ID                      " & vbNewLine
    sSql = sSql & "   and #Sinistro_Principal.Sucursal_Seguradora_ID        = Apolice_Terceiros_Tb.Sucursal_Seguradora_ID       " & vbNewLine
    sSql = sSql & "   and #Sinistro_Principal.Seguradora_Cod_Susep          = Apolice_Terceiros_Tb.Seguradora_Cod_Susep         " & vbNewLine

    sSql = sSql & "UPdate #Sinistro_Principal                                                                                   " & vbNewLine
    sSql = sSql & "   Set Cosseguro_Aceito_Num_Apolice_Lider                = Co_seguro_Aceito_tb.num_apolice_lider             " & vbNewLine
    sSql = sSql & "     , Cosseguro_Aceito_Ramo_Lider                       = Co_seguro_Aceito_tb.ramo_lider                    " & vbNewLine
    sSql = sSql & "     , Cosseguro_Aceito_Sucursal_Seg_Lider               = Co_seguro_Aceito_tb.sucursal_seg_lider            " & vbNewLine
    sSql = sSql & "     , Cosseguro_Aceito_Seg_Cod_Susep_Lider              = Co_seguro_Aceito_tb.seg_cod_susep_lider           " & vbNewLine
    sSql = sSql & "  From Seguros_Db.Dbo.Co_Seguro_Aceito_tb                Co_Seguro_Aceito_tb WITH (NOLOCK)                                            " & vbNewLine
    sSql = sSql & "  Join #Sinistro_Principal                                                                                                       " & vbNewLine
    sSql = sSql & "    on #Sinistro_Principal.Apolice_Terceiros_Num_Ordem_Co_Seguro_Aceito      = Co_seguro_Aceito_Tb.num_ordem_co_seguro_aceito    " & vbNewLine
    sSql = sSql & "   and #Sinistro_Principal.Apolice_Terceiros_Seg_Cod_Susep_Lider             = Co_seguro_Aceito_Tb.seg_cod_susep_lider           " & vbNewLine
    sSql = sSql & "   and #Sinistro_Principal.Apolice_Terceiros_Sucursal_Seg_Lider              = Co_seguro_Aceito_Tb.sucursal_seg_lider            " & vbNewLine

    sSql = sSql & "UPdate #Sinistro_Principal                                                                                                       " & vbNewLine
    sSql = sSql & "   Set Seguradora_Nome                                   = Seguradora_Tb.Nome                                                    " & vbNewLine
    sSql = sSql & "  From Seguros_Db.Dbo.Seguradora_Tb                      Seguradora_Tb WITH (NOLOCK)                                                  " & vbNewLine
    sSql = sSql & "  Join #Sinistro_Principal                                                                                                       " & vbNewLine
    sSql = sSql & "    on #Sinistro_Principal.Cosseguro_Aceito_Seg_Cod_Susep_Lider = Seguradora_Tb.Seguradora_Cod_Susep                              " & vbNewLine

    sSql = sSql & "Update #Sinistro_Principal                                                                               " & vbNewLine
    sSql = sSql & "   Set Cosseguro_Ramo_Nome                               = Ramo_Tb.Nome                                  " & vbNewLine
    sSql = sSql & "  From Seguros_Db.Dbo.Ramo_Tb Ramo_Tb WITH (NOLOCK)                                                           " & vbNewLine
    sSql = sSql & "  Join #Sinistro_Principal                                                                               " & vbNewLine
    sSql = sSql & "    on #Sinistro_Principal.Cosseguro_Aceito_Ramo_Lider = Ramo_Tb.Ramo_ID                                 " & vbNewLine

    sSql = sSql & "Update #Sinistro_Principal                                                                               " & vbNewLine
    sSql = sSql & "   Set Sucursal_Seguradora_Nome                          = Sucursal_Seguradora_Tb.Nome                   " & vbNewLine
    sSql = sSql & "  From Seguros_Db.Dbo.Sucursal_Seguradora_Tb             Sucursal_Seguradora_Tb WITH (NOLOCK)                 " & vbNewLine
    sSql = sSql & "  Join #Sinistro_Principal                                                                               " & vbNewLine
    sSql = sSql & "    on #Sinistro_Principal.Cosseguro_Aceito_Sucursal_Seg_Lider = Sucursal_Seguradora_Tb.Sucursal_Seguradora_ID        " & vbNewLine
    sSql = sSql & "   and #Sinistro_Principal.Cosseguro_Aceito_Seg_Cod_Susep_Lider = Sucursal_Seguradora_Tb.Seguradora_Cod_Susep " & vbNewLine

    Call Conexao_ExecutarSQL(gsSIGLASISTEMA, _
                             glAmbiente_id, _
                             App.Title, _
                             App.FileDescription, _
                             sSql, _
                             lConexaoLocal, _
                             False)


    If (bytTipoRamo = bytTipoRamo_RE Or bytTipoRamo = bytTipoRamo_Rural) Then    'mathayde - 29
        sSql = ""
'petrauskas, ajuste SQL2014
'        sSQL = sSQL & " if isnull(OBJECT_ID('tempdb..#Sinistro_Principal_Endereco_Risco'),0) >0" & vbNewLine
        sSql = sSql & " if OBJECT_ID('tempdb..#Sinistro_Principal_Endereco_Risco') IS NOT NULL" & vbNewLine
        sSql = sSql & " BEGIN" & vbNewLine
        sSql = sSql & "     DROP TABLE #Sinistro_Principal_Endereco_Risco" & vbNewLine
        sSql = sSql & " END" & vbNewLine
        sSql = sSql & "SELECT cod_objeto_segurado       = b.cod_objeto_segurado                                     " & vbNewLine
        sSql = sSql & "     , end_risco_id              = a.end_risco_id                                                " & vbNewLine
        sSql = sSql & "     , Endereco                  = a.endereco                                                    " & vbNewLine
        sSql = sSql & "     , Bairro                    = a.bairro                                                      " & vbNewLine
        sSql = sSql & "     , Municipio                 = a.municipio                                                   " & vbNewLine
        sSql = sSql & "     , Estado                    = a.estado                                                      " & vbNewLine
        sSql = sSql & "     , Dt_inicio_Vigencia_Seg    = b.dt_inicio_vigencia_seg                                      " & vbNewLine
        sSql = sSql & "     , Dt_Fim_Vigencia_Seg       = b.dt_fim_vigencia_seg                                         " & vbNewLine
        sSql = sSql & "     , Tipo                      = 1                                                         " & vbNewLine
        sSql = sSql & "  Into #Sinistro_Principal_Endereco_Risco                                                        " & vbNewLine
        sSql = sSql & "  FROM endereco_risco_tb a  WITH (NOLOCK) , seguro_residencial_tb b WITH (NOLOCK)                " & vbNewLine
        sSql = sSql & "  Join #Sinistro_Principal                                                                       " & vbNewLine
        sSql = sSql & "    on #Sinistro_Principal.Proposta_ID = b.Proposta_ID                                           " & vbNewLine
        sSql = sSql & " Where a.Proposta_id = b.Proposta_id                                                         " & vbNewLine
        sSql = sSql & "   AND a.end_risco_id = b.end_risco_id                                                           " & vbNewLine
        sSql = sSql & "   AND b.dt_fim_vigencia_seg IS NULL                                                         " & vbNewLine
        sSql = sSql & " Union                                                                                        " & vbNewLine
        sSql = sSql & "SELECT cod_objeto_segurado       = c.cod_objeto_segurado                                      " & vbNewLine
        sSql = sSql & "     , end_risco_id              = a.end_risco_id                                             " & vbNewLine
        sSql = sSql & "     , Endereco                  = a.endereco                                                 " & vbNewLine
        sSql = sSql & "     , Bairro                    = a.bairro                                                   " & vbNewLine
        sSql = sSql & "     , Municipio                 = a.municipio                                                " & vbNewLine
        sSql = sSql & "     , Estado                    = a.estado                                                   " & vbNewLine
        sSql = sSql & "     , Dt_inicio_Vigencia_Seg    = c.dt_inicio_vigencia_seg                                   " & vbNewLine
        sSql = sSql & "     , Dt_Fim_Vigencia_Seg       = c.dt_fim_vigencia_seg                                      " & vbNewLine
        sSql = sSql & "     , Tipo                      = 2                                                          " & vbNewLine
        sSql = sSql & " FROM endereco_risco_tb a  WITH (NOLOCK) , seguro_empresarial_tb c WITH (NOLOCK)                  " & vbNewLine
        sSql = sSql & "  Join #Sinistro_Principal                                                                    " & vbNewLine
        sSql = sSql & "    on #Sinistro_Principal.Proposta_ID = c.Proposta_ID                                        " & vbNewLine
        sSql = sSql & " Where a.Proposta_id = c.Proposta_id                                                      " & vbNewLine
        sSql = sSql & " AND a.end_risco_id = c.end_risco_id                                                      " & vbNewLine
        sSql = sSql & " AND c.dt_fim_vigencia_seg IS NULL                                                            " & vbNewLine
        sSql = sSql & " Union                                                                                        " & vbNewLine
        sSql = sSql & "SELECT cod_objeto_segurado       = d.cod_objeto_segurado                                      " & vbNewLine
        sSql = sSql & "     , end_risco_id              = a.end_risco_id                                             " & vbNewLine
        sSql = sSql & "     , Endereco                  = a.endereco                                                 " & vbNewLine
        sSql = sSql & "     , Bairro                    = a.bairro                                                   " & vbNewLine
        sSql = sSql & "     , Municipio                 = a.municipio                                                " & vbNewLine
        sSql = sSql & "     , Estado                    = a.estado                                                   " & vbNewLine
        sSql = sSql & "     , Dt_inicio_Vigencia_Seg    = d.dt_inicio_vigencia_seg                                   " & vbNewLine
        sSql = sSql & "     , Dt_Fim_Vigencia_Seg       = d.dt_fim_vigencia_seg                                      " & vbNewLine
        sSql = sSql & "     , Tipo                      = 3                                                           " & vbNewLine
        sSql = sSql & " FROM endereco_risco_tb a  WITH (NOLOCK) , seguro_condominio_tb d WITH (NOLOCK)                   " & vbNewLine
        sSql = sSql & "  Join #Sinistro_Principal                                                                    " & vbNewLine
        sSql = sSql & "    on #Sinistro_Principal.Proposta_ID = d.Proposta_ID                                        " & vbNewLine
        sSql = sSql & " Where a.Proposta_id = d.Proposta_id                                                      " & vbNewLine
        sSql = sSql & " AND a.end_risco_id = d.end_risco_id                                                      " & vbNewLine
        sSql = sSql & " AND d.dt_fim_vigencia_seg IS NULL                                                            " & vbNewLine
        sSql = sSql & " Union                                                                                        " & vbNewLine
        sSql = sSql & "SELECT cod_objeto_segurado       = e.cod_objeto_segurado                                      " & vbNewLine
        sSql = sSql & "     , end_risco_id              = a.end_risco_id                                             " & vbNewLine
        sSql = sSql & "     , Endereco                  = a.endereco                                                 " & vbNewLine
        sSql = sSql & "     , Bairro                    = a.bairro                                                   " & vbNewLine
        sSql = sSql & "     , Municipio                 = a.municipio                                                " & vbNewLine
        sSql = sSql & "     , Estado                    = a.estado                                                   " & vbNewLine
        sSql = sSql & "     , Dt_inicio_Vigencia_Seg    = e.dt_inicio_vigencia_seg                                   " & vbNewLine
        sSql = sSql & "     , Dt_Fim_Vigencia_Seg       = e.dt_fim_vigencia_seg                                      " & vbNewLine
        sSql = sSql & "     , Tipo                      = 4                                                          " & vbNewLine
        sSql = sSql & " FROM endereco_risco_tb a  WITH (NOLOCK) , seguro_maquinas_tb e WITH (NOLOCK)                     " & vbNewLine
        sSql = sSql & "  Join #Sinistro_Principal                                                                    " & vbNewLine
        sSql = sSql & "    on #Sinistro_Principal.Proposta_ID = e.Proposta_ID                                        " & vbNewLine
        sSql = sSql & " Where a.Proposta_id = e.Proposta_id                                                      " & vbNewLine
        sSql = sSql & " AND a.end_risco_id = e.end_risco_id                                                      " & vbNewLine
        sSql = sSql & " AND e.dt_fim_vigencia_seg IS NULL                                                            " & vbNewLine
        sSql = sSql & " Union                                                                                        " & vbNewLine
        sSql = sSql & "SELECT cod_objeto_segurado       = f.cod_objeto_segurado                                      " & vbNewLine
        sSql = sSql & "     , end_risco_id              = a.end_risco_id                                             " & vbNewLine
        sSql = sSql & "     , Endereco                  = a.endereco                                                 " & vbNewLine
        sSql = sSql & "     , Bairro                    = a.bairro                                                   " & vbNewLine
        sSql = sSql & "     , Municipio                 = a.municipio                                                " & vbNewLine
        sSql = sSql & "     , Estado                    = a.estado                                                   " & vbNewLine
        sSql = sSql & "     , Dt_inicio_Vigencia_Seg    = f.dt_inicio_vigencia_seg                                   " & vbNewLine
        sSql = sSql & "     , Dt_Fim_Vigencia_Seg       = f.dt_fim_vigencia_seg                                      " & vbNewLine
        sSql = sSql & "     , Tipo                      = 5                                                          " & vbNewLine
        sSql = sSql & " FROM endereco_risco_tb a  WITH (NOLOCK) , seguro_aceito_tb f WITH (NOLOCK)                   " & vbNewLine
        sSql = sSql & "  Join #Sinistro_Principal                                                                   " & vbNewLine
        sSql = sSql & "    on #Sinistro_Principal.Proposta_ID = f.Proposta_ID                                       " & vbNewLine
        sSql = sSql & " Where a.Proposta_id = f.Proposta_id                                                     " & vbNewLine
        sSql = sSql & " AND a.end_risco_id = f.end_risco_id                                                     " & vbNewLine
        sSql = sSql & " AND f.dt_fim_vigencia_seg IS NULL                                                           " & vbNewLine
        sSql = sSql & " Union                                                                                       " & vbNewLine
        sSql = sSql & "SELECT cod_objeto_segurado       = g.cod_objeto_segurado                                     " & vbNewLine
        sSql = sSql & "     , end_risco_id              = a.end_risco_id                                            " & vbNewLine
        sSql = sSql & "     , Endereco                  = a.endereco                                                " & vbNewLine
        sSql = sSql & "     , Bairro                    = a.bairro                                                  " & vbNewLine
        sSql = sSql & "     , Municipio                 = a.municipio                                               " & vbNewLine
        sSql = sSql & "     , Estado                    = a.estado                                                  " & vbNewLine
        sSql = sSql & "     , Dt_inicio_Vigencia_Seg    = g.dt_inicio_vigencia_seg                                  " & vbNewLine
        sSql = sSql & "     , Dt_Fim_Vigencia_Seg       = g.dt_fim_vigencia_seg                                     " & vbNewLine
        sSql = sSql & "     , Tipo                      = 6                                                         " & vbNewLine
        sSql = sSql & " FROM endereco_risco_tb a  WITH (NOLOCK) , seguro_avulso_tb g WITH (NOLOCK)                  " & vbNewLine
        sSql = sSql & "  Join #Sinistro_Principal                                                                   " & vbNewLine
        sSql = sSql & "    on #Sinistro_Principal.Proposta_ID = g.Proposta_ID                                       " & vbNewLine
        sSql = sSql & " Where a.Proposta_id = g.Proposta_id                                                     " & vbNewLine
        sSql = sSql & " AND a.end_risco_id = g.end_risco_id                                                     " & vbNewLine
        sSql = sSql & " AND g.dt_fim_vigencia_seg IS NULL                                                           " & vbNewLine
        sSql = sSql & " Union                                                                                       " & vbNewLine
        sSql = sSql & "SELECT cod_objeto_segurado       = h.cod_objeto_segurado                                     " & vbNewLine
        sSql = sSql & "     , end_risco_id              = NULL                                                      " & vbNewLine
        sSql = sSql & "     , Endereco                  = h.desc_mercadoria                                         " & vbNewLine
        sSql = sSql & "     , Bairro                    = NULL                                                      " & vbNewLine
        sSql = sSql & "     , Municipio                 = NULL                                                      " & vbNewLine
        sSql = sSql & "     , Estado                    = NULL                                                       " & vbNewLine
        sSql = sSql & "     , Dt_inicio_Vigencia_Seg    = h.dt_inicio_vigencia_seg                                  " & vbNewLine
        sSql = sSql & "     , Dt_Fim_Vigencia_Seg       = h.dt_fim_vigencia_seg                                     " & vbNewLine
        sSql = sSql & "     , Tipo                      = 7                                                         " & vbNewLine
        sSql = sSql & " FROM seguro_transporte_tb h WITH (NOLOCK)                                                    " & vbNewLine
        sSql = sSql & "  Join #Sinistro_Principal                                                                   " & vbNewLine
        sSql = sSql & "    on #Sinistro_Principal.Proposta_ID = h.Proposta_ID                                       " & vbNewLine
        sSql = sSql & " Where h.dt_fim_vigencia_seg IS NULL                                                     " & vbNewLine
        sSql = sSql & " Union                                                                                       " & vbNewLine
        sSql = sSql & "SELECT cod_objeto_segurado       = h.cod_objeto_segurado                                     " & vbNewLine
        sSql = sSql & "     , end_risco_id              = NULL                                                      " & vbNewLine
        sSql = sSql & "     , Endereco                  = h.desc_mercadoria                                         " & vbNewLine
        sSql = sSql & "     , Bairro                    = NULL                                                      " & vbNewLine
        sSql = sSql & "     , Municipio                 = NULL                                                      " & vbNewLine
        sSql = sSql & "     , Estado                    = NULL                                                      " & vbNewLine
        sSql = sSql & "     , Dt_inicio_Vigencia_Seg    = h.dt_inicio_vigencia_seg                                  " & vbNewLine
        sSql = sSql & "     , Dt_Fim_Vigencia_Seg       = h.dt_fim_vigencia_seg                                     " & vbNewLine
        sSql = sSql & "     , Tipo                      = 7                                                         " & vbNewLine
        sSql = sSql & " FROM seguro_transporte_tb h WITH (NOLOCK)                                                    " & vbNewLine
        sSql = sSql & "  Join #Sinistro_Principal                                                                   " & vbNewLine
        sSql = sSql & "    on #Sinistro_Principal.Proposta_ID = h.Proposta_ID                                       " & vbNewLine
        sSql = sSql & " Where dt_fim_vigencia_seg = (select max(dt_fim_vigencia_seg)                                " & vbNewLine
        sSql = sSql & "                                from seguro_transporte_tb h1 WITH (NOLOCK)                    " & vbNewLine
        sSql = sSql & "                                Join #Sinistro_Principal                                     " & vbNewLine
        sSql = sSql & "                                  on #Sinistro_Principal.Proposta_ID = h1.Proposta_ID        " & vbNewLine
        sSql = sSql & "                               Where h1.cod_objeto_segurado = h.cod_objeto_segurado          " & vbNewLine
        sSql = sSql & "                                 and h1.dt_fim_vigencia_seg is not null)                 " & vbNewLine
        sSql = sSql & " and not exists            (select 1 from seguro_transporte_tb h2 WITH (NOLOCK)               " & vbNewLine
        sSql = sSql & "                                Join #Sinistro_Principal                                     " & vbNewLine
        sSql = sSql & "                                  on #Sinistro_Principal.Proposta_ID = h2.Proposta_ID         " & vbNewLine
        sSql = sSql & "                               Where h2.cod_objeto_segurado = h.cod_objeto_segurado          " & vbNewLine
        sSql = sSql & "                                 and h2.dt_fim_vigencia_seg is null)                     " & vbNewLine
        sSql = sSql & " Union                                                                                       " & vbNewLine
        sSql = sSql & "SELECT cod_objeto_segurado       = i.cod_objeto_segurado                                     " & vbNewLine
        sSql = sSql & "     , end_risco_id              = a.end_risco_id                                            " & vbNewLine
        sSql = sSql & "     , Endereco                  = a.endereco                                                " & vbNewLine
        sSql = sSql & "     , Bairro                    = a.bairro                                                  " & vbNewLine
        sSql = sSql & "     , Municipio                 = a.municipio                                               " & vbNewLine
        sSql = sSql & "     , Estado                    = a.estado                                                  " & vbNewLine
        sSql = sSql & "     , Dt_inicio_Vigencia_Seg    = i.dt_inicio_vigencia_seg                                  " & vbNewLine
        sSql = sSql & "     , Dt_Fim_Vigencia_Seg       = i.dt_fim_vigencia_seg                                     " & vbNewLine
        sSql = sSql & "     , Tipo                      = 8                                                         " & vbNewLine
        sSql = sSql & " FROM endereco_risco_tb a WITH (NOLOCK) , seguro_generico_tb i WITH (NOLOCK)                   " & vbNewLine
        sSql = sSql & "  Join #Sinistro_Principal                                                                   " & vbNewLine
        sSql = sSql & "    on #Sinistro_Principal.Proposta_ID = i.Proposta_ID                                       " & vbNewLine
        sSql = sSql & " Where a.Proposta_id = i.Proposta_id                                                     " & vbNewLine
        sSql = sSql & " AND a.end_risco_id = i.end_risco_id                                                     " & vbNewLine
        sSql = sSql & " AND i.dt_fim_vigencia_seg IS NULL                                                           " & vbNewLine
        sSql = sSql & " UNION                                                                                       " & vbNewLine
        sSql = sSql & "SELECT cod_objeto_segurado       = j.cod_objeto_segurado                                     " & vbNewLine
        sSql = sSql & "     , end_risco_id              = NULL                                                      " & vbNewLine
        sSql = sSql & "     , Endereco                  = NULL                                                      " & vbNewLine
        sSql = sSql & "     , Bairro                    = NULL                                                      " & vbNewLine
        sSql = sSql & "     , Municipio                 = NULL                                                      " & vbNewLine
        sSql = sSql & "     , Estado                    = NULL                                                      " & vbNewLine
        sSql = sSql & "     , Dt_inicio_Vigencia_Seg    = j.dt_inicio_vigencia_seg                                  " & vbNewLine
        sSql = sSql & "     , Dt_Fim_Vigencia_Seg       = NULL                                                      " & vbNewLine
        sSql = sSql & "     , Tipo                      = 9                                                          " & vbNewLine
        sSql = sSql & " FROM seguro_penhor_rural_tb j WITH (NOLOCK)                                              " & vbNewLine
        sSql = sSql & "  Join #Sinistro_Principal                                                                   " & vbNewLine
        sSql = sSql & "    on #Sinistro_Principal.Proposta_ID = j.Proposta_ID                                       " & vbNewLine
        sSql = sSql & " UNION                                                                                       " & vbNewLine
        sSql = sSql & "SELECT cod_objeto_segurado       = k.cod_objeto_segurado                                     " & vbNewLine
        sSql = sSql & "     , end_risco_id              = NULL                                                      " & vbNewLine
        sSql = sSql & "     , Endereco                  = NULL                                                      " & vbNewLine
        sSql = sSql & "     , Bairro                    = NULL                                                      " & vbNewLine
        sSql = sSql & "     , Municipio                 = NULL                                                      " & vbNewLine
        sSql = sSql & "     , Estado                    = NULL                                                      " & vbNewLine
        sSql = sSql & "     , Dt_inicio_Vigencia_Seg    = k.dt_inicio_vigencia_seg                                  " & vbNewLine
        sSql = sSql & "     , Dt_Fim_Vigencia_Seg       = NULL                                                      " & vbNewLine
        sSql = sSql & "     , Tipo                      = 10                                                        " & vbNewLine
        sSql = sSql & " FROM seguro_consorcio_quebra_garantia_tb k WITH (NOLOCK)                                     " & vbNewLine
        sSql = sSql & "  Join #Sinistro_Principal                                                                   " & vbNewLine
        sSql = sSql & "    on #Sinistro_Principal.Proposta_ID = k.Proposta_ID                                       " & vbNewLine
        
        Call Conexao_ExecutarSQL(gsSIGLASISTEMA, _
                                glAmbiente_id, _
                                App.Title, _
                                App.FileDescription, _
                                sSql, _
                                lConexaoLocal, _
                                False)
        
'FLAVIO.BEZERRA - 16/07/2013 - MP Temporaria
        sSql = "" & vbNewLine
        sSql = sSql & " Select Endereco,Bairro,Municipio,Estado                                                     " & vbNewLine
        sSql = sSql & "   From #Sinistro_Principal_Endereco_Risco                                                   " & vbNewLine
        
        
        Set RsEndereco = Conexao_ExecutarSQL(gsSIGLASISTEMA, _
                                            glAmbiente_id, _
                                            App.Title, _
                                            App.FileDescription, _
                                            sSql, _
                                            lConexaoLocal, _
                                            True)
        
        'RICARDO TOLEDO - CONFITEC SP - INC000004382997 - INI
        If Not RsEndereco.EOF Then
            strEndereco_Ocorrencia = IIf(IsNull(RsEndereco.Fields!Endereco), "", RsEndereco.Fields!Endereco)
            strBairro_Ocorrencia = IIf(IsNull(RsEndereco.Fields!Bairro), "", RsEndereco.Fields!Bairro)
            strMunicipio_Ocorrencia = IIf(IsNull(RsEndereco.Fields!Municipio), "", RsEndereco.Fields!Municipio)
            strEstado_Ocorrencia = IIf(IsNull(RsEndereco.Fields!Estado), "", RsEndereco.Fields!Estado)
        Else
            MsgBox "N�o foi localizado endere�o de risco, para este sinistro!", vbOKOnly + vbInformation, "SEGP1296"
            strEstado_Ocorrencia = ""
            strMunicipio_Ocorrencia = ""
            strBairro_Ocorrencia = ""
            strEndereco_Ocorrencia = ""
        End If
        'RICARDO TOLEDO - CONFITEC SP - INC000004382997 - FIM
        
        Set RsEndereco = Nothing
'FLAVIO.BEZERRA - 16/07/2013 - MP Temporaria
                  
    End If

    Exit Sub

Erro:
    Call TrataErroGeral("SelecionaDados_AbaDadosGerais", Me.Caption)
    Call FinalizarAplicacao
    Resume
End Sub
Private Sub SelecionaDados_Form()
    Dim sSql As String


    MousePointer = vbHourglass

'FLAVIO.BEZERRA - 16/07/2013 - MP Temporaria
'    sSql = ""
'    sSql = sSql & " if isnull(OBJECT_ID('tempdb..#Sinistro_BB'),0) >0" & vbNewLine
'    sSql = sSql & " BEGIN" & vbNewLine
'    sSql = sSql & "     DROP TABLE #Sinistro_BB" & vbNewLine
'    sSql = sSql & " END" & vbNewLine
'    sSql = sSql & "" & vbNewLine
'    sSql = sSql & " Select Sinistro_BB" & vbNewLine
'    sSql = sSql & "      , Sinistro_ID" & vbNewLine
'    sSql = sSql & "      , Dt_Fim_VIgencia " & vbNewLine
'    sSql = sSql & "   Into #Sinistro_BB" & vbNewLine
'    sSql = sSql & "   From Sinistro_Bb_Tb  WITH (NOLOCK) " & vbNewLine
'    sSql = sSql & "  Where Sinistro_ID =  " & gbldSinistro_ID & vbNewLine
'    sSql = sSql & "    and Dt_Fim_Vigencia is null" & vbNewLine


'    sSql = sSql & " if isnull(OBJECT_ID('tempdb..#Entrada_GTR'),0) >0               " & vbNewLine
'    sSql = sSql & " BEGIN                                                           " & vbNewLine
'    sSql = sSql & "     DROP TABLE #Entrada_GTR                                     " & vbNewLine
'    sSql = sSql & " END                                                             " & vbNewLine
'    sSql = sSql & "                                                                 " & vbNewLine
'    sSql = sSql & "Select Entrada_GTR_Tb.*                                          " & vbNewLine
'    sSql = sSql & "  Into #Entrada_GTR                                              " & vbNewLine
'    sSql = sSql & "  From Interface_Db..Entrada_GTR_Tb Entrada_GTR_Tb WITH (NOLOCK) " & vbNewLine
'    sSql = sSql & "  Join #Sinistro_BB                                              " & vbNewLine
'    sSql = sSql & "    on #Sinistro_BB.Sinistro_BB = Entrada_GTR_Tb.Sinistro_BB     " & vbNewLine
'    sSql = sSql & "   and Evento_BB         = 2000                                  " & vbNewLine

'    Call Conexao_ExecutarSQL(gsSIGLASISTEMA, _
'                            glAmbiente_id, _
'                            App.Title, _
'                            App.FileDescription, _
'                            sSql, _
'                            lConexaoLocal, _
'                            False)


'    sSql = sSql & " if isnull(OBJECT_ID('tempdb..#Seguro_Fraude_Historico'),0) >0" & vbNewLine
'    sSql = sSql & " BEGIN" & vbNewLine
'    sSql = sSql & "     DROP TABLE #Seguro_Fraude_Historico" & vbNewLine
'    sSql = sSql & " END" & vbNewLine
'    sSql = sSql & "" & vbNewLine
'    sSql = sSql & "Select Top 1 Fase_ID" & vbNewLine
'    sSql = sSql & "     , Nivel_ID " & vbNewLine
'    sSql = sSql & "  Into #Seguro_Fraude_Historico    " & vbNewLine
'    sSql = sSql & "  From Seguro_Fraude_Db..Historico_Tb  WITH (NOLOCK) " & vbNewLine
'    sSql = sSql & " Where Sinistro_ID = " & IIf(IsNull(gbldSinistro_ID) Or gbldSinistro_ID = 0, 0, gbldSinistro_ID) & vbNewLine
'    sSql = sSql & " Order By Dt_Inclusao Desc" & vbNewLine


'    sSql = sSql & "Select Evento_ID                                                                                     " & vbNewLine
'    sSql = sSql & "     , Evento_SegBr_ID                                                                               " & vbNewLine
'    sSql = sSql & "     , Evento_BB_Id                                                                                  " & vbNewLine
'    sSql = sSql & "     , Num_Recibo                                                                                    " & vbNewLine
'    sSql = sSql & "  Into #Evento_SegBR_Sinistro_Atual_Temp                                                             " & vbNewLine
'    sSql = sSql & "  From evento_SEGBR_sinistro_atual_tb WITH (NOLOCK)                                                               " & vbNewLine
'    sSql = sSql & "  Join #Sinistro_BB                                                                                  " & vbNewLine
'    sSql = sSql & "    on #Sinistro_BB.Sinistro_ID  = evento_SEGBR_sinistro_atual_tb.Sinistro_ID                        " & vbNewLine
'    sSql = sSql & "                                                                                                     " & vbNewLine
'    sSql = sSql & "Insert into #Evento_SegBR_Sinistro_Atual_Temp                                                        " & vbNewLine
'    sSql = sSql & "Select Evento_ID                                                                                     " & vbNewLine
'    sSql = sSql & "     , Evento_SegBr_ID                                                                               " & vbNewLine
'    sSql = sSql & "     , Evento_BB_Id                                                                                  " & vbNewLine
'    sSql = sSql & "     , Num_Recibo                                                                                    " & vbNewLine
'    sSql = sSql & "  From evento_SEGBR_sinistro_hist_2004_tb   WITH (NOLOCK)                                                       " & vbNewLine
'    sSql = sSql & "  Join #Sinistro_BB                                                                                  " & vbNewLine
'    sSql = sSql & "    on #Sinistro_BB.Sinistro_ID  = evento_SEGBR_sinistro_hist_2004_tb.Sinistro_ID                    " & vbNewLine
'    sSql = sSql & "                                                                                                     " & vbNewLine
'    sSql = sSql & "Insert into #Evento_SegBR_Sinistro_Atual_Temp                                                        " & vbNewLine
'    sSql = sSql & "Select Evento_ID                                                                                     " & vbNewLine
'    sSql = sSql & "     , Evento_SegBr_ID                                                                               " & vbNewLine
'    sSql = sSql & "     , Evento_BB_Id                                                                                  " & vbNewLine
'    sSql = sSql & "     , Num_Recibo                                                                                    " & vbNewLine
'    sSql = sSql & "  From evento_SEGBR_sinistro_hist_2003_tb        WITH (NOLOCK)                                                   " & vbNewLine
'    sSql = sSql & "  Join #Sinistro_BB                                                                                  " & vbNewLine
'    sSql = sSql & "    on #Sinistro_BB.Sinistro_ID  = evento_SEGBR_sinistro_hist_2003_tb.Sinistro_ID                    " & vbNewLine
'    sSql = sSql & "                                                                                                     " & vbNewLine
'    sSql = sSql & "Select Evento_ID                                                                                     " & vbNewLine
'    sSql = sSql & "     , Evento_SegBr_ID                                                                               " & vbNewLine
'    sSql = sSql & "  Into #Evento_SegBR_Sinistro_Atual                                                                  " & vbNewLine
'    sSql = sSql & "  From #Evento_SegBR_Sinistro_Atual_Temp                                                             " & vbNewLine
'    sSql = sSql & " Where #Evento_SegBR_Sinistro_Atual_Temp.Evento_BB_Id is Not Null                                    " & vbNewLine
'    sSql = sSql & "  and (#Evento_SegBR_Sinistro_Atual_Temp.Num_Recibo is Null                                          " & vbNewLine
'    sSql = sSql & "      or #Evento_SegBR_Sinistro_Atual_Temp.Num_Recibo = 0                                            " & vbNewLine
'    sSql = sSql & "      or #Evento_SegBR_Sinistro_Atual_Temp.Evento_BB_ID = 2009)                                      " & vbNewLine
'    sSql = sSql & "  and #Evento_SegBR_Sinistro_Atual_Temp.Evento_BB_ID Not In (1150, 1151, 1152, 1153, 1154, 1181)     " & vbNewLine
'    sSql = sSql & "                                                                                                     " & vbNewLine
'    sSql = sSql & "Select Evento_ID = Max(isnull(#Evento_SegBR_Sinistro_Atual.Evento_ID,0))                             " & vbNewLine
'    sSql = sSql & "  Into #Evento_ID                                                                                    " & vbNewLine
'    sSql = sSql & "  From Evento_SegBr_Tb   WITH (NOLOCK)                                                                            " & vbNewLine
'    sSql = sSql & "  Join #Evento_SegBR_Sinistro_Atual                                                                  " & vbNewLine
'    sSql = sSql & "    on #Evento_SegBR_Sinistro_Atual.Evento_SegBr_ID = Evento_SegBr_Tb.Evento_SegBr_ID                " & vbNewLine
'    sSql = sSql & " Where Evento_SegBr_Tb.Prox_Localizacao_Processo is Not Null                                         " & vbNewLine
'    sSql = sSql & "                                                                                                     " & vbNewLine
'    sSql = sSql & "Select Prox_Localizacao_Processo                                                                     " & vbNewLine
'    sSql = sSql & "     , Evento_Segbr_Tb.Evento_BB_ID                                                                  " & vbNewLine
'    sSql = sSql & "     , #Sinistro_BB.Sinistro_BB                                                                      " & vbNewLine
'    sSql = sSql & "  Into #Localizacao                                                                                  " & vbNewLine
'    sSql = sSql & "  From Sinistro_Tb Sinistro_Tb WITH (NOLOCK)                                                              " & vbNewLine
'    sSql = sSql & "  Join #Sinistro_BB #Sinistro_BB WITH (NOLOCK)                                                            " & vbNewLine
'    sSql = sSql & "    on #Sinistro_BB.Sinistro_ID = Sinistro_Tb.Sinistro_ID                                            " & vbNewLine
'    sSql = sSql & "   and #Sinistro_BB.Dt_Fim_VIgencia is Null                                                          " & vbNewLine
'    sSql = sSql & "  Left Join #Evento_ID                                                                               " & vbNewLine
'    sSql = sSql & " on #Evento_ID.Evento_ID <> 0                                                                         " & vbNewLine
'    sSql = sSql & "  Join Evento_SegBr_Sinistro_Tb Evento_SegBr_Sinistro_Tb WITH (NOLOCK)                                    " & vbNewLine
'    sSql = sSql & "    on Evento_SegBr_Sinistro_Tb.Sinistro_ID = #Sinistro_BB.Sinistro_ID                               " & vbNewLine
'    sSql = sSql & "   and Evento_SegBr_Sinistro_Tb.Sinistro_BB = #Sinistro_BB.Sinistro_BB                               " & vbNewLine
'    sSql = sSql & "   and Evento_SegBr_Sinistro_Tb.Evento_ID = #Evento_ID.Evento_ID                                     " & vbNewLine
'    sSql = sSql & "  Join Evento_SegBr_Tb Evento_SegBr_Tb WITH (NOLOCK)                                                      " & vbNewLine
'    sSql = sSql & "    on Evento_SegBr_Tb.Evento_SegBr_Id = Evento_SegBr_Sinistro_Tb.Evento_SegBr_ID                    "

'    Call Conexao_ExecutarSQL(gsSIGLASISTEMA, _
'                             glAmbiente_id, _
'                             App.Title, _
'                             App.FileDescription, _
'                             sSql, _
'                             lConexaoLocal, _
'                             False)
'
'
'    Exit Sub
'FLAVIO.BEZERRA - 16/07/2013 - MP Temporaria

Erro:
    Call TrataErroGeral("SelecionaDados_AbaDadosGerais", Me.Caption)
    Call FinalizarAplicacao


End Sub
Private Sub SelecionaDados(bAba As Byte)

    Dim bProcessa As Boolean

    bProcessa = True

    SelecionaDados_AbaDadosGerais
    'SelecionaDados_Form           'FLAVIO.BEZERRA - 16/07/2013 - MP Temporaria

End Sub
Private Sub CarregaDados_Aviso()
    Dim rsRecordSet As ADODB.Recordset
    Dim sSql As String

    On Error GoTo Erro

    MousePointer = vbHourglass

    SelecionaDados 0


    sSql = ""
    sSql = sSql & "Select * " & vbCrLf
    sSql = sSql & "  From #Sinistro_Principal"

    Set rsRecordSet = Conexao_ExecutarSQL(gsSIGLASISTEMA, _
                                          glAmbiente_id, _
                                          App.Title, _
                                          App.FileDescription, _
                                          sSql, _
                                          lConexaoLocal, True)
    With rsRecordSet
        If Not .EOF Then

            Monta_FrameEndosso

            gbllApolice_ID = rsRecordSet("apolice_id")
            gbllRamo_ID = rsRecordSet("ramo_id")
            gbllSeguradora_Cod_Susep = rsRecordSet("seguradora_cod_susep")
            gbllSucursal_Seguradora_ID = rsRecordSet("sucursal_seguradora_id")
            Municipio_Id_Anterior = rsRecordSet("sinistro_municipio_id")
            gblsSinistro_Situacao_ID = rsRecordSet("sinistro_situacao_id")
            vSubevento_Sinistro_id = IIf(IsNull(rsRecordSet("Subevento_sinistro_id")), -1, rsRecordSet("Subevento_sinistro_id"))
            vSubevento_Sinistro = IIf(IsNull(rsRecordSet("SubEvento_Sinistro_Nome")), " ", rsRecordSet("SubEvento_Sinistro_Nome"))
            vAgencia_id = IIf(IsNull(rsRecordSet("agencia_id")), " ", rsRecordSet("agencia_id"))
            vReintegracao_IS = IIf(UCase(rsRecordSet("Sinistro_reintegracao_is")) = "S", True, False)
            vSinistro_Lider = IIf(IsNull(rsRecordSet("sinistro_lider_id")), 0, rsRecordSet("sinistro_lider_id"))

            '            'Dados do Frame: Aviso
            If Not IsNull(.Fields!Evento_Sinistro_Nome) Then
            '    cboCausa_Sinistro.Text = .Fields!Evento_Sinistro_Nome & ""
            'Cleber Sardo - In�cio - INC000004328329 - Tratamento do campo - Data 16/05/2014
             Dim iDx As Integer
                cboCausa_Sinistro.AddItem (.Fields!Evento_Sinistro_Nome)
                cboCausa_Sinistro.ItemData(cboCausa_Sinistro.NewIndex) = .Fields!Evento_Sinistro_id
                For iDx = 0 To cboCausa_Sinistro.ListCount - 1
                    If cboCausa_Sinistro.ItemData(iDx) = .Fields!Evento_Sinistro_id Then
                        cboCausa_Sinistro.ListIndex = Val(iDx)
                        Exit For
                    End If
                Next iDx
            'Cleber Sardo - Fim - INC000004328329 - Tratamento do campo - Data 16/05/2014
            End If

            If Not IsNull(.Fields!SubEvento_Sinistro_Nome) Then
            '    cboSubevento_Sinistro = .Fields!SubEvento_Sinistro_Nome & ""
            'Cleber Sardo - Fim - INC000004490696 - Tratamento do campo - Data 15/01/2015
                cboSubevento_Sinistro.AddItem (.Fields!SubEvento_Sinistro_Nome)
                cboSubevento_Sinistro.ItemData(cboSubevento_Sinistro.NewIndex) = .Fields!Subevento_sinistro_id
                For iDx = 0 To cboSubevento_Sinistro.ListCount - 1
                    If cboSubevento_Sinistro.ItemData(iDx) = .Fields!Subevento_sinistro_id Then
                        cboSubevento_Sinistro.ListIndex = Val(iDx)
                        Exit For
                    End If
                Next iDx
            'Cleber Sardo - Fim - INC000004490696 - Tratamento do campo - Data 15/01/2015
            End If

            If vSinistro_Lider <> 0 Then
                TxtNumero_Sinistro_Lider = vSinistro_Lider & ""
            End If

            If Not IsNull(.Fields!Dt_Ocorrencia_Sinistro) Then
                mskData_Ocorrencia = Format(.Fields!Dt_Ocorrencia_Sinistro, "dd/mm/yyyy")
            Else
                mskData_Ocorrencia.SetFocus
            End If
            If Not IsNull(.Fields!dt_entrada_seguradora) Then
                mskData_Entrada = Format(.Fields!dt_entrada_seguradora, "dd/mm/yyyy")
            Else
                mskData_Entrada.SetFocus
            End If
            If Not IsNull(.Fields!Dt_Aviso_Sinistro) Then
                mskData_Aviso = Format(.Fields!Dt_Aviso_Sinistro, "dd/mm/yyyy")
            Else
                mskData_Aviso.SetFocus
            End If
            If Not IsNull(.Fields!Dt_Inclusao) Then
                txtData_Inclusao = Format(.Fields!Dt_Inclusao, "dd/mm/yyyy")
            Else
                txtData_Inclusao = ""
            End If

            If vReintegracao_IS Then
                chkReintegracaoIS.Value = 1
            Else
                chkReintegracaoIS.Value = 0
            End If

            If sOperacaoCosseguro <> "C" Then

                If Not IsNull(.Fields!agencia_id) Then
                    mskAgencia_Aviso = .Fields!agencia_id
                ElseIf mskAgencia_Aviso.Visible = True Then
                    mskAgencia_Aviso.SetFocus
                End If
                CarregaDadosGrid_AbaDadosGerais_Aviso
            End If


            If bytTipoRamo = bytTipoRamo_Vida Then

                ' Dados do Frame: Sinistrado
                txtSinistro_Vida_Nome.Text = .Fields!Sinistro_Vida_Nome & ""
                If Not IsNull(.Fields!Sinistro_Vida_CPF) Then
                    'CORRE��O DE VALORES DIFERENTES DO TAMANHO DA MASCARA
                    'INC000004348598 - HENRIQUE H. HENRIQUES - CONFITEC SP - INI
                    'mskCPF_Sinistrado = Format(.Fields!Sinistro_Vida_CPF, "&&&.&&&.&&&-&&")
                    If Len(.Fields!Sinistro_Vida_CPF) = 11 Then
                       mskCPF_Sinistrado = Format(.Fields!Sinistro_Vida_CPF, "&&&.&&&.&&&-&&")
                    End If
                    'INC000004348598 - HENRIQUE H. HENRIQUES - CONFITEC SP - FIM
                    'CORRE��O DE VALORES DIFERENTES DO TAMANHO DA MASCARA
                End If

                If Not IsNull(.Fields!Sinistro_Vida_Dt_Nasc) Then
                    mskDtNasc_Sinistrado = Format(.Fields!Sinistro_Vida_Dt_Nasc, "dd/mm/yyyy")
                End If

                If Not IsNull(.Fields!Sinistro_Vida_Dt_Nasc) Then
                    mskDtNasc_Sinistrado.Text = Format(.Fields!Sinistro_Vida_Dt_Nasc, "dd/mm/yyyy")
                End If

                If Not IsNull(.Fields!pgto_parcial_co_seguro) Then
                    chkPgto_Parcial_Co_Seguro.Value = IIf(.Fields!pgto_parcial_co_seguro = "s", 1, 0)
                End If
                
                '----------------------------------------------------------------------------------------------------------------
                'Ricardo Toledo : 21/08/2015 : INC000004679370 : Inicio
                'A sintaxe abaixo estava ocasionando erro de Type mismatch quando n�o retornava registro
                'txtSinistro_Vida_Sexo = IIf(IsNull(.Fields!Sinistro_Vida_Sexo), "", IIf(.Fields!Sinistro_Vida_Sexo = "M", "MASCULINO", "FEMININO"))
                '                    CmbVidaSexo.Text = IIf(IsNull(.Fields!Sinistro_Vida_Sexo), "", IIf(.Fields!Sinistro_Vida_Sexo = "M", "MASCULINO", "FEMININO"))
                'CmbVidaSexo.ListIndex = IIf(IsNull(.Fields!Sinistro_Vida_Sexo), "", IIf(.Fields!Sinistro_Vida_Sexo = "M", 0, 1))
                If Not IsNull(.Fields!Sinistro_Vida_Sexo) Then
                    CmbVidaSexo.ListIndex = IIf(.Fields!Sinistro_Vida_Sexo = "M", 0, 1)
                End If
                'Ricardo Toledo : 21/08/2015 : INC000004679370 : Fim
                '----------------------------------------------------------------------------------------------------------------
                txtSinistrado_Cliente_ID.Text = .Fields!Sinistrado_Cliente_ID & ""
                                
                                'Henrique H. Henriques - AJUSTE DA CARGA DOS DADOS DO SINISTRO - ESTAVA PERDENDO AS INFORMA��ES DO ENDERE�O SINISTRADO QUANDO ERA REALIZADA A MANUTEN��O VIA SEGP1296 - INI
                If Not IsNull(.Fields!Sinistro_Endereco) Then
                    TxtEndereco_Ocorrencia.Text = Trim(.Fields!Sinistro_Endereco)
                Else
                    TxtEndereco_Ocorrencia.Text = ""
                End If
                
                If Not IsNull(.Fields!Sinistro_Bairro) Then
                    TxtBairro_Ocorrencia.Text = Trim(.Fields!Sinistro_Bairro)
                Else
                    TxtBairro_Ocorrencia.Text = ""
                End If
                
                If Not IsNull(.Fields!Sinistro_Estado) Then
                    TxtEstado_Ocorrencia.Text = Trim(.Fields!Sinistro_Estado)
                Else
                    TxtEstado_Ocorrencia.Text = ""
                End If
                
                If Not IsNull(.Fields!Sinistro_Municipio) Then
                    TxtMunicipio_Ocorrencia.Text = Trim(.Fields!Sinistro_Municipio)
                Else
                    TxtMunicipio_Ocorrencia.Text = ""
                End If
                
                If IsNull(.Fields!Sinistro_Municipio_id) = False And IsNumeric(.Fields!Sinistro_Municipio_id) = True Then
                    vMunicipio_id = .Fields!Sinistro_Municipio_id
                Else
                    vMunicipio_id = 0
                End If
                'Henrique H. Henriques - AJUSTE DA CARGA DOS DADOS DO SINISTRO - ESTAVA PERDENDO AS INFORMA��ES DO ENDERE�O SINISTRADO QUANDO ERA REALIZADA A MANUTEN��O VIA SEGP1296 - FIM
            End If

        End If
    End With

    bCarregou_AbaDadosGerais = True

    MousePointer = vbNormal

    Set rsRecordSet = Nothing
    Exit Sub
    Resume
Erro:
    Call TrataErroGeral("CarregaDados_Aviso", Me.Caption)
    Call FinalizarAplicacao

End Sub

Private Sub InicializaModoProcessamentoObjeto(objObjeto As Object, bTipo As Byte)
    DoEvents

    With objObjeto
        Select Case bTipo
        Case 1              'Vai carregar
            If TypeOf objObjeto Is MSFlexGrid Then
                .BackColorFixed = &HC0C0C0
                .ForeColorFixed = &H808080
            Else
                .BackColor = &HC0C0C0
            End If
        Case 2              'Carregado
            If TypeOf objObjeto Is MSFlexGrid Then
                .BackColorFixed = &H8000000F
                .ForeColorFixed = &H80000012
            Else
                .BackColor = -2147483643
            End If
        End Select
    End With

End Sub
Private Sub InicializaCargaDados_LimpezaTemporario()

    Dim sSql As String

    On Error GoTo TrataErro

    'alterado para cima, e ao inves de apagar registros, dropar a tablea
    sSql = ""
    If EstadoConexao(lConexaoLocal) = adStateClosed Then
        lConexaoLocal = AbrirConexao(gsSIGLASISTEMA, _
                                     glAmbiente_id, _
                                     App.Title, _
                                     App.FileDescription)
    Else
'petrauskas, ajuste SQL2014
'        sSQL = " if isnull(OBJECT_ID('tempdb..#tempsel_tb'),0) >0" & vbNewLine
        sSql = " if OBJECT_ID('tempdb..#tempsel_tb') IS NOT NULL" & vbNewLine
        sSql = sSql & " BEGIN" & vbNewLine
        sSql = sSql & "     DROP TABLE #tempsel_tb" & vbNewLine
        sSql = sSql & " END" & vbNewLine
    End If

    sSql = sSql & " create table #tempsel_tb( " & vbCrLf
    sSql = sSql & "     marca                   char(1)         null, " & vbCrLf
    sSql = sSql & "     apolice_id              numeric(9,0)    null, " & vbCrLf
    sSql = sSql & "     sucursal_seguradora_id  numeric(5,0)    null, " & vbCrLf
    sSql = sSql & "     seguradora_cod_susep    numeric(5,0)    null, " & vbCrLf
    sSql = sSql & "     ramo_id                 int             null, " & vbCrLf
    sSql = sSql & "     nomeramo                varchar(60)     null, " & vbCrLf
    sSql = sSql & "     proposta_id             numeric(9,0)    null, " & vbCrLf
    sSql = sSql & "     numendosso              int             null, " & vbCrLf
    sSql = sSql & "     sitproposta             char(1)         null, " & vbCrLf
    sSql = sSql & "     produto_id              int             null, " & vbCrLf
    sSql = sSql & "     nomeproduto             varchar(60)     null, " & vbCrLf
    sSql = sSql & "     cliente_id              int             null, " & vbCrLf
    sSql = sSql & "     nomecliente             varchar(60)     null, " & vbCrLf
    sSql = sSql & "     tipocomponente          char(1)         null, " & vbCrLf
    sSql = sSql & "     sinistro_id             numeric(11,0)   null, " & vbCrLf
    sSql = sSql & "     situacao                char(1)         null, " & vbCrLf
    sSql = sSql & "     situacao_desc           varchar(30)     null, " & vbCrLf
    sSql = sSql & "     situacao_evento         char(1)         null, " & vbCrLf
    sSql = sSql & "     sit_evento_desc         varchar(20)     null, " & vbCrLf
    sSql = sSql & "     sucursal_nome           varchar(60)     null, " & vbCrLf
    sSql = sSql & "     endosso                 int             null, " & vbCrLf
    sSql = sSql & "     sinistro_id_lider       numeric(20,0)   null, " & vbCrLf
    sSql = sSql & "     processa_reintegracao_is char(1)        null, " & vbCrLf
    sSql = sSql & "     historico               varchar(100)    null, " & vbCrLf
    sSql = sSql & "     Dt_Ocorrencia_Sinistro  SmallDateTime   null)"


    '    vNunCx = lConexaoLocal

    Call Conexao_ExecutarSQL(gsSIGLASISTEMA, _
                             glAmbiente_id, _
                             App.Title, _
                             App.FileDescription, _
                             sSql, _
                             lConexaoLocal, _
                             False)
    Exit Sub

TrataErro:
    Call TrataErroGeral("InicializarCargaDados_LimpezaTemporario", Me.name)

End Sub
Private Sub CarregaDadosGrid_AbaDadosGerais_Aviso()
    Dim rsRecordSet As ADODB.Recordset
    Dim sSql As String
    Dim sLinha As String

    On Error GoTo Erro

    sSql = ""
    sSql = sSql & "Select * " & vbCrLf
    sSql = sSql & "  From #DadosGerais_Aviso"

    Set rsRecordSet = Conexao_ExecutarSQL(gsSIGLASISTEMA, _
                                          glAmbiente_id, _
                                          App.Title, _
                                          App.FileDescription, _
                                          sSql, _
                                          lConexaoLocal, _
                                          True)
    With rsRecordSet
        Do While Not .EOF
             'FLAVIO.BEZERRA - IN�CIO - 25/01/2013
'            sLinha = .Fields!Sinistro_BB & vbTab
'            sLinha = sLinha & .Fields!Situacao & ""
'            GrdInformacao_SinistroBB.AddItem sLinha
'            .MoveNext
            sLinha = .Fields!Sinistro_BB & vbTab
            sLinha = sLinha & .Fields!Situacao & vbTab
            sLinha = sLinha & Obtem_Localizacao_Sinistro_BB(.Fields!Sinistro_BB) & ""
            GrdInformacao_SinistroBB.AddItem sLinha
            .MoveNext
            'FLAVIO.BEZERRA - FIM - 25/01/2013
        Loop
    End With


    Set rsRecordSet = Nothing
    Exit Sub

Erro:
    Call TrataErroGeral("CarregaDadosGrid_AbaDadosGerais_Aviso", Me.Caption)
    Call FinalizarAplicacao

End Sub

Sub Monta_cmbCausaSinistro()

    Dim Rst_Causa As Recordset
    Dim vSql As String
    Dim lEvento_sinistro_id As Long
    
    'Victor - 02/10/2013 - 17905311 - IN�CIO
    Dim Rst_Rural_Estudo As Recordset
    'Victor - 02/10/2013 - 17905311 - FIM

    On Error GoTo Trata_Erro


    If bytModoOperacao = 1 Then
'AKIO.OKUNO - MP - INICIO - 19/02/2013
'        vsql = "select a.ramo_id ,a.proposta_id, a.prop_cliente_id as cliente_id,a.produto_id, b.seguradora_cod_susep , c.sucursal_seguradora_id,d.apolice_id" & vbNewLine
'        vsql = vsql + "from seguros_db.dbo.proposta_tb a  WITH (NOLOCK) " & vbNewLine
'        vsql = vsql + "join seguros_db.dbo.produto_tb b  WITH (NOLOCK) " & vbNewLine
'        vsql = vsql + "on b.produto_id = a.produto_id" & vbNewLine
'        vsql = vsql + "join seguros_db.dbo.sucursal_seguradora_tb c  WITH (NOLOCK) " & vbNewLine
'        vsql = vsql + "on c.seguradora_cod_susep = b.seguradora_cod_susep" & vbNewLine
'        vsql = vsql + "join seguros_db.dbo.apolice_tb d  WITH (NOLOCK) " & vbNewLine
'        vsql = vsql + "on a.proposta_id = d.proposta_id" & vbNewLine
'        vsql = vsql + "where a.proposta_id =  " & gbllProposta_ID & vbNewLine
'
'        vsql = vsql + " Union" & vbNewLine
'
'        vsql = vsql + "select a.ramo_id,a.proposta_id, a.prop_cliente_id as cliente_id,a.produto_id, b.seguradora_cod_susep , c.sucursal_seguradora_id,e.apolice_id" & vbNewLine
'        vsql = vsql + "from seguros_db.dbo.proposta_tb a  WITH (NOLOCK) " & vbNewLine
'        vsql = vsql + "join seguros_db.dbo.produto_tb b  WITH (NOLOCK) " & vbNewLine
'        vsql = vsql + "on b.produto_id = a.produto_id" & vbNewLine
'        vsql = vsql + "join seguros_db.dbo.sucursal_seguradora_tb c  WITH (NOLOCK) " & vbNewLine
'        vsql = vsql + "on c.seguradora_cod_susep = b.seguradora_cod_susep" & vbNewLine
'        vsql = vsql + "join seguros_db.dbo.proposta_adesao_tb e  WITH (NOLOCK) " & vbNewLine
'        vsql = vsql + "on a.proposta_id = e.proposta_id" & vbNewLine
'        vsql = vsql + "Where a.proposta_id = " & gbllProposta_ID

        'Victor - 02/10/2013 - 17905311 - IN�CIO
        vSql = "Select count(*) AS qtde_rural " & vbNewLine
        vSql = vSql & " From proposta_tb " & vbNewLine
        vSql = vSql & " Where proposta_id = " & gbllProposta_ID & vbNewLine
        vSql = vSql & " And produto_id in (1152,1204) " & vbNewLine
        vSql = vSql & " And situacao in ('a','e') " & vbNewLine
        
        Set Rst_Rural_Estudo = Conexao_ExecutarSQL(gsSIGLASISTEMA, _
                                            glAmbiente_id, _
                                            App.Title, _
                                            App.FileDescription, _
                                            vSql, _
                                            lConexaoLocal, True)

        qtdeRuralEstudo = Rst_Rural_Estudo("qtde_rural")
        
        vSql = ""
        
        If qtdeRuralEstudo > 0 Then
        
            vSql = "Select Proposta_Tb.Ramo_ID                                                                   as Ramo_id                         " & vbNewLine
            vSql = vSql & "     , Proposta_Tb.Proposta_ID                                                               as Proposta_id                                " & vbNewLine
            vSql = vSql & "     , Proposta_Tb.Prop_Cliente_ID                                                           as Cliente_ID                      " & vbNewLine
            vSql = vSql & "     , Proposta_Tb.Produto_ID                                                                as Produto_id                                " & vbNewLine
            vSql = vSql & "     , 6785                                                                                     as Seguradora_Cod_Susep            " & vbNewLine
            vSql = vSql & "     , 0                                                                                     as Sucursal_Seguradora_ID          " & vbNewLine
            vSql = vSql & "     , 0                                                                                     as Apolice_ID                      " & vbNewLine
            vSql = vSql & "  From Seguros_Db.Dbo.Proposta_Tb                                                            Proposta_Tb WITH (NOLOCK)            " & vbNewLine
            vSql = vSql & "  Left Join Seguros_Db.Dbo.Proposta_Fechada_Tb                                                Proposta_Fechada_Tb WITH (NOLOCK)     " & vbNewLine
            vSql = vSql & "    on Proposta_Fechada_Tb.Proposta_ID                                                        = Proposta_Tb.Proposta_ID       " & vbNewLine
            vSql = vSql & " Where Proposta_Tb.Proposta_ID                                                               = " & gbllProposta_ID
            
        Else
        'Victor - 02/10/2013 - 17905311 - FIM
        
            vSql = vSql & "Select Proposta_Tb.Ramo_ID                                                                                                   " & vbNewLine
            vSql = vSql & "     , Proposta_Tb.Proposta_ID                                                                                               " & vbNewLine
            vSql = vSql & "     , Proposta_Tb.Prop_Cliente_ID                                                           Cliente_ID                      " & vbNewLine
            vSql = vSql & "     , Proposta_Tb.Produto_ID                                                                                                " & vbNewLine
            vSql = vSql & "     , IsNull(Apolice_Tb.Seguradora_Cod_Susep, Proposta_Adesao_Tb.Seguradora_Cod_Susep)      Seguradora_Cod_Susep            " & vbNewLine
            vSql = vSql & "     , IsNull(Apolice_Tb.Sucursal_Seguradora_ID, Proposta_Adesao_Tb.Sucursal_Seguradora_ID)  Sucursal_Seguradora_ID          " & vbNewLine
            vSql = vSql & "     , Isnull(Apolice_Tb.Apolice_ID , Proposta_Adesao_Tb.Apolice_ID)                         Apolice_ID                      " & vbNewLine
            vSql = vSql & "  From Seguros_Db.Dbo.Proposta_Tb                                                            Proposta_Tb WITH (NOLOCK)            " & vbNewLine
            vSql = vSql & "  left Join Seguros_Db.Dbo.Apolice_Tb                                                        Apolice_Tb  WITH (NOLOCK)            " & vbNewLine
            vSql = vSql & "    on Apolice_Tb.Proposta_ID                                                                = Proposta_Tb.Proposta_ID       " & vbNewLine
            vSql = vSql & "  Left Join Seguros_Db.Dbo.Proposta_Adesao_Tb                                                Proposta_Adesao_Tb WITH (NOLOCK)     " & vbNewLine
            vSql = vSql & "    on Proposta_Adesao_Tb.Proposta_ID                                                        = Proposta_Tb.Proposta_ID       " & vbNewLine
            vSql = vSql & " Where Proposta_Tb.Proposta_ID                                                               = " & gbllProposta_ID
        
        'Victor - 02/10/2013 - 17905311 - IN�CIO
        End If
        'Victor - 02/10/2013 - 17905311 - FIM
        
'AKIO.OKUNO - MP - FIM - 19/02/2013
    Else

        vSql = "select proposta_id,ramo_id,cliente_id from seguros_db.dbo.sinistro_tb WITH (NOLOCK) " & vbNewLine
        vSql = vSql + "where sinistro_tb.sinistro_id = " & gbldSinistro_ID

    End If


    Set Rst_Causa = Conexao_ExecutarSQL(gsSIGLASISTEMA, _
                                        glAmbiente_id, _
                                        App.Title, _
                                        App.FileDescription, _
                                        vSql, _
                                        lConexaoLocal, True)

    If Not Rst_Causa.EOF Then

        gbllRamo_ID = Rst_Causa("ramo_id")
        gbllProp_Cliente_ID = Rst_Causa("cliente_id")
        gbllProposta_ID = Rst_Causa("proposta_id")

        If bytModoOperacao = 1 Then

            gbllProduto_ID = Rst_Causa("produto_id")
            gbllSeguradora_Cod_Susep = Rst_Causa("seguradora_cod_susep")
            gbllSucursal_Seguradora_ID = Rst_Causa("sucursal_seguradora_id")
            gbllApolice_ID = Rst_Causa("apolice_id")

        End If

    Else

        MsgBox "Erro na Monta_cmbCausaSinistro, a Aplica��o ser� Finalizada!", vbOKOnly, App.EXEName
        Call FinalizarAplicacao

    End If


    Rst_Causa.Close

    vSql = "SELECT Evento_Sinistro_tb.Evento_Sinistro_id, Evento_Sinistro_tb.Nome " & vbNewLine
    vSql = vSql + ", Evento_Sinistro_tb.causa_multipla" & vbNewLine  'asouza - 31.01.06
    vSql = vSql + "FROM Evento_Sinistro_tb WITH (NOLOCK) " & vbNewLine
    vSql = vSql + "INNER JOIN Evento_Sinistro_Ramo_tb WITH (NOLOCK) " & vbNewLine
    vSql = vSql + "ON Evento_Sinistro_tb.Evento_Sinistro_id = " & vbNewLine
    vSql = vSql + "Evento_Sinistro_Ramo_tb.Evento_Sinistro_id " & vbNewLine
    vSql = vSql + "WHERE Evento_Sinistro_Ramo_tb.Ramo_id = " & gbllRamo_ID & vbNewLine
    vSql = vSql + " ORDER BY Evento_sinistro_tb.Nome"

    Set Rst_Causa = Conexao_ExecutarSQL(gsSIGLASISTEMA, _
                                        glAmbiente_id, _
                                        App.Title, _
                                        App.FileDescription, _
                                        vSql, _
                                        lConexaoLocal, True)

    With cboCausa_Sinistro
        .Clear
        If Not Rst_Causa.EOF Then
            While Not Rst_Causa.EOF
                .AddItem Rst_Causa(1)
                .ItemData(.NewIndex) = Rst_Causa(0)
                lEvento_sinistro_id = Rst_Causa(0)
                Rst_Causa.MoveNext
            Wend
        End If
        Rst_Causa.Close

        ' Inclui tamb�m a causa gen�rica
        .AddItem "EVENTO GENERICO"
        .ItemData(.NewIndex) = CAUSA_GENERICA_SEGUR

        FrmAvisoSinistro.cboCausa_Sinistro.AddItem "N"
        FrmAvisoSinistro.cboCausa_Sinistro.ItemData(FrmAvisoSinistro.cboCausa_Sinistro.NewIndex) = CAUSA_GENERICA_SEGUR


    End With

    Exit Sub
    Resume
Trata_Erro:
    TrataErroGeral "Monta_cmbCausaSinistro"
End Sub
Sub Monta_cboSubeventoSinistro(ByVal Evento As Integer)
'Sub Monta_cmbSubeventoSinistro(ByVal Evento As Integer) 'F.BEZERRA 16/10/2012

    Dim rs As Recordset
    Dim vSql As String

    vSql = vSql + "SELECT subevento_sinistro_tb.subevento_sinistro_id," & vbNewLine
    vSql = vSql + "       subevento_sinistro_tb.Nome" & vbNewLine
    vSql = vSql + "FROM evento_subevento_sinistro_tb WITH (NOLOCK) " & vbNewLine
    vSql = vSql + "JOIN subevento_sinistro_tb WITH (NOLOCK) " & vbNewLine
    vSql = vSql + "  ON evento_subevento_sinistro_tb.subevento_sinistro_id = subevento_sinistro_tb.subevento_sinistro_id" & vbNewLine
    vSql = vSql + "WHERE evento_subevento_sinistro_tb.evento_sinistro_id = " & Evento & vbNewLine
    vSql = vSql + "ORDER BY subevento_sinistro_tb.nome"

    Set rs = Conexao_ExecutarSQL(gsSIGLASISTEMA, _
                                 glAmbiente_id, _
                                 App.Title, _
                                 App.FileDescription, _
                                 vSql, _
                                 lConexaoLocal, True)

    With cboSubevento_Sinistro
        .Clear
        If Not rs.EOF Then
            While Not rs.EOF
                .AddItem rs(1)
                .ItemData(.NewIndex) = rs(0)

                rs.MoveNext
            Wend
        End If
        rs.Close

    End With


End Sub
Public Sub Monta_FrameEndosso()

    Dim rs As ADODB.Recordset, SQL As String



    SQL = "SELECT s.endosso_id, sre.num_averbacao"
    SQL = SQL & " FROM sinistro_tb s WITH (NOLOCK) "
    SQL = SQL & "   LEFT JOIN sinistro_re_tb sre WITH (NOLOCK) "
    SQL = SQL & "       ON (sre.sinistro_id             = s.sinistro_id)"
    SQL = SQL & " WHERE s.sinistro_id            = " & gbldSinistro_ID

    Set rs = Conexao_ExecutarSQL(gsSIGLASISTEMA, _
                                 glAmbiente_id, _
                                 App.Title, _
                                 App.FileDescription, _
                                 SQL, _
                                 lConexaoLocal, True)

    If Not rs.EOF Then
        If Not IsNull(rs(1)) And vTranspInternacional = True Then
            mskAverbacao.Text = IIf(Len(rs(1)) < 7, CDbl(rs(1)) & String(7 - Len(rs(1)), "_"), CDbl(rs(1)))
        End If
        If Not IsNull(rs(0)) Then
            txtEndosso.Text = rs(0)
        Else
            txtEndosso.Text = "0"
            vEndosso_Ant = 0
        End If
    End If
    rs.Close

End Sub

Public Function Altera_Sinistro() As Boolean

    Dim rs As ADODB.Recordset, rsRecordSet As ADODB.Recordset, vSql As String
    Dim Itens_historico(20) As String, Cont As Integer
    Dim vDtAviso As String, vDtEntrada As String
    Dim vDtOcorrencia As String, VCausa As String
    Dim vMunicipio_id As String

    Dim dMoeda        As Double 'FLAVIO.BEZERRA - 13/03/2013

    On Error GoTo Trata_Erro

    Altera_Sinistro = True
    
    'FLAVIO.BEZERRA - INICIO - 13/03/2013
    If cboMoeda.ListIndex <> -1 Then
        dMoeda = cboMoeda.ItemData(cboMoeda.ListIndex)
    Else
        dMoeda = txtMoeda.Tag
    End If
    'FLAVIO.BEZERRA - FIM - 13/03/2013
    
    If Not Manutencao_SinistroBB Then

        Altera_Sinistro = True
        Exit Function

    End If

    vSql = "SELECT b.nome, a.dt_aviso_sinistro, "
    vSql = vSql & " a.dt_ocorrencia_sinistro, a.dt_entrada_seguradora "
    vSql = vSql & " FROM sinistro_tb a WITH (NOLOCK) JOIN evento_sinistro_tb b WITH (NOLOCK) "
    vSql = vSql & " ON a.evento_sinistro_id = b.evento_sinistro_id "
    vSql = vSql & " WHERE a.sinistro_id = " & gbldSinistro_ID

    Set rs = Conexao_ExecutarSQL(gsSIGLASISTEMA, _
                                 glAmbiente_id, _
                                 App.Title, _
                                 App.FileDescription, _
                                 vSql, _
                                 lConexaoLocal, True)
    VCausa = rs(0)
    vDtAviso = Format(rs(1), "dd/mm/yyyy")
    vDtOcorrencia = Format(rs(2), "dd/mm/yyyy")
    vDtEntrada = Format(rs(3), "dd/mm/yyyy")
    rs.Close

    ' Verifica se houve altera��o na causa do sinistro.
    If cboCausa_Sinistro.Text <> VCausa Then
        Itens_historico(0) = "EVENTO ANTERIOR: " & VCausa
        If Not Inclui_Evento("10006", gblsSinistro_Situacao_ID, "NULL", Itens_historico) Then
            Altera_Sinistro = False
            MsgBox "Erro Inclus�o Evento Altera��o Causa Sinistro", vbOKOnly + vbCritical, App.EXEName

            Exit Function
        End If
        Itens_historico(0) = ""    ' Limpa para os pr�ximos eventos
    End If

    ' Contador para as linhas do hist�rico.
    Cont = -1    ' come�a do -1 pois se incrementar vai para o �ndice 0.

'FLAVIO.BEZERRA - 18/07/2013 - MP TEMPORARIA
    If bytTipoRamo = bytTipoRamo_RE Or bytTipoRamo = bytTipoRamo_Rural Then
'        sSql = ""
'        sSql = sSql & "Select * " & vbCrLf
'        sSql = sSql & "  From #Sinistro_Principal_Endereco_Risco"
'
'        Set rsRecordSet = Conexao_ExecutarSQL(gsSIGLASISTEMA, _
'                                              glAmbiente_id, _
'                                              App.Title, _
'                                              App.FileDescription, _
'                                              sSql, _
'                                              lConexaoLocal, True)
'
'
'        ' Checa se houve altera��o nos dados cadastrais do sinistro
'        If TxtEndereco_Ocorrencia <> rsRecordSet("endereco") Then
'            Cont = Cont + 1
'            Itens_historico(Cont) = "ENDER. SINISTRO ANTERIOR: " & rsRecordSet("endereco")
'        End If
'
'        If TxtBairro_Ocorrencia <> rsRecordSet("bairro") Then
'            Cont = Cont + 1
'            Itens_historico(Cont) = "BAIRRO SINISTRO ANTERIOR: " & rsRecordSet("bairro")
'        End If
'
'        If Trim(TxtMunicipio_Ocorrencia) <> rsRecordSet("municipio") Then
'            Cont = Cont + 1
'            Itens_historico(Cont) = "MUNIC. SINISTRO ANTERIOR: " & rsRecordSet("municipio")
'        End If
'
'        If TxtEstado_Ocorrencia <> rsRecordSet("estado") Then
'            Cont = Cont + 1
'            Itens_historico(Cont) = "UF SINISTRO ANTERIOR: " & rsRecordSet("estado")
'        End If
'
'        rsRecordSet.Close
        
        
        If TxtEndereco_Ocorrencia <> strEndereco_Ocorrencia Then
            Cont = Cont + 1
            Itens_historico(Cont) = "ENDER. SINISTRO ANTERIOR: " & strEndereco_Ocorrencia
        End If

        If TxtBairro_Ocorrencia <> strBairro_Ocorrencia Then
            Cont = Cont + 1
            Itens_historico(Cont) = "BAIRRO SINISTRO ANTERIOR: " & strBairro_Ocorrencia
        End If

        If Trim(TxtMunicipio_Ocorrencia) <> strMunicipio_Ocorrencia Then
            Cont = Cont + 1
            Itens_historico(Cont) = "MUNIC. SINISTRO ANTERIOR: " & strMunicipio_Ocorrencia
        End If

        If TxtEstado_Ocorrencia <> strEstado_Ocorrencia Then
            Cont = Cont + 1
            Itens_historico(Cont) = "UF SINISTRO ANTERIOR: " & strEstado_Ocorrencia
        End If
        
    End If

    If Format(mskData_Aviso, "DD/MM/YYYY") <> vDtAviso Then
        Cont = Cont + 1
        Itens_historico(Cont) = "DT. AVISO SINISTRO ANTERIOR: " & vDtAviso
    End If

    If Format(mskData_Ocorrencia, "DD/MM/YYYY") <> vDtOcorrencia Then
        Cont = Cont + 1
        Itens_historico(Cont) = "DT. OCORR�NCIA ANTERIOR: " & vDtOcorrencia
    End If

    If Format(mskData_Entrada, "DD/MM/YYYY") <> vDtEntrada Then
        Cont = Cont + 1
        Itens_historico(Cont) = "DT. ENTRADA SEGURADORA ANTERIOR: " & vDtEntrada
    End If

    If mskAgencia_Aviso <> vAgencia_id Then
        Cont = Cont + 1
        Itens_historico(Cont) = "AG�NCIA ANTERIOR: " & vAgencia_id
        vAgencia_id = mskAgencia_Aviso
    End If

    If IIf(chkReintegracaoIS.Value = 1, True, False) <> vReintegracao_IS Then
        Cont = Cont + 1
        Itens_historico(Cont) = "SOLICITA��O DE REINTEGRA��O ANTERIOR: " & IIf(vReintegracao_IS, "SIM", "N�O")
        vReintegracao_IS = IIf(chkReintegracaoIS.Value = 1, True, False)
    End If



    If Trim(TxtNumero_Sinistro_Lider.Text) <> vSinistro_Lider And sOperacaoCosseguro = "C" Then
        Cont = Cont + 1
        Itens_historico(Cont) = "No. SINISTRO L�DER ANTERIOR: " & vSinistro_Lider
    End If
    'asouza 2202
    If Trim(cboSubevento_Sinistro.Text) <> Trim(vSubevento_Sinistro) Then
        Cont = Cont + 1
        Itens_historico(Cont) = "EVENTO ANTERIOR: " & VCausa
        Cont = Cont + 1
        Itens_historico(Cont) = "SUBEVENTO ANTERIOR: " & vSubevento_Sinistro
        If vDetalhamento = "" Then
            vDetalhamento = "EVENTO ANTERIOR: " & VCausa & Chr(10)
            vDetalhamento = vDetalhamento & "SUBEVENTO ANTERIOR: " & vSubevento_Sinistro
        Else

        End If
    End If


    If Cont > -1 Then    ' S� inclui se houve alguma linha de hist�rico
        ' 10007: Altera��o de dados cadastrais
        If Not Inclui_Evento("10007", gblsSinistro_Situacao_ID, "NULL", Itens_historico) Then
            Altera_Sinistro = False
            MsgBox "Erro Inclus�o Evento Altera��o Endere�o Sinistro", vbOKOnly + vbCritical, App.EXEName
            Exit Function
        End If
    End If



    If cboSubevento_Sinistro.ListIndex = -1 Then
        vSubevento_Sinistro_id = ""
    Else
        'vSubevento_Sinistro_id = cmbSubeventoSinistro.ItemData(cmbSubeventoSinistro.ListIndex)
        vSubevento_Sinistro_id = cboSubevento_Sinistro.ItemData(cboSubevento_Sinistro.ListIndex)    'F.BEZERRA 16/10/2012
    End If

    ' Verifica se houve altera��o no endosso do sinistro.
    If Val(txtEndosso.Text) <> vEndosso_Ant Then
        Itens_historico(0) = "ENDOSSO ANTERIOR: " & vEndosso_Ant
        vEndosso_Ant = Val(txtEndosso.Text)
        If Not Inclui_Evento("10084", gblsSinistro_Situacao_ID, "NULL", Itens_historico) Then
            Altera_Sinistro = False
            MsgBox "Erro Inclus�o Evento Altera��o de Endosso de Sinistro", vbOKOnly + vbCritical, App.EXEName
            RetornarTransacao (lConexaoLocal)
            Exit Function
        End If
        Itens_historico(0) = ""    ' Limpa para os pr�ximos eventos
    End If

    ' Tenta encontrar um municipio_id para o munic�pio cadastrado.
    If TxtEndereco_Ocorrencia <> "" And vEnd_Risco_id <> 0 Then
'FLAVIO.BEZERRA - 17/07/2013
'        vsql = "SELECT municipio_id FROM Endereco_risco_tb WITH (NOLOCK) " & _
'               "WHERE endereco_risco_tb.proposta_id = " & gbllProposta_ID & _
'             "  AND endereco_risco_tb.end_risco_id = " & vEnd_Risco_id

        vSql = " SET NOCOUNT ON                                        " & _
             "SELECT municipio_id FROM Endereco_risco_tb WITH (NOLOCK)  " & _
               "WHERE endereco_risco_tb.proposta_id = " & gbllProposta_ID & _
             "  AND endereco_risco_tb.end_risco_id = " & vEnd_Risco_id

        Set rs = Conexao_ExecutarSQL(gsSIGLASISTEMA, _
                                     glAmbiente_id, _
                                     App.Title, _
                                     App.FileDescription, _
                                     vSql, _
                                     lConexaoLocal, True)
        If Not rs.EOF Then
            'Ricardo Toledo (Confitec) : 09/10/2014 : INC000004425037 : Inicio
            'Para o sinistro contido nesse incidente o registro estava retornando normalmente, por�m, o municipio estava NULL
            'vMunicipio_id = rs(0)
            vMunicipio_id = IIf(IsNull(rs(0)), "999", rs(0))
            'Ricardo Toledo (Confitec) : 09/10/2014 : INC000004425037 : Fim
        Else
            vMunicipio_id = "999"
        End If
'FLAVIO.BEZERRA - 17/07/2013
        rs.Close

    ElseIf TxtEndereco_Ocorrencia <> "" Then

        If Municipio_Id_Anterior <> "" Then
            vMunicipio_id = Municipio_Id_Anterior
        Else
            vMunicipio_id = "999"
        End If
    Else
        vMunicipio_id = "999"
    End If


    AbrirTransacao (lConexaoLocal)

    vSql = "EXEC sinistro_spu "
    vSql = vSql & "  @sinistro_id = " & gbldSinistro_ID
    vSql = vSql & ", @apolice_id = " & gbllApolice_ID
    vSql = vSql & ", @sucursal_seguradora_id = " & gbllSucursal_Seguradora_ID
    vSql = vSql & ", @seguradora_cod_susep = " & gbllSeguradora_Cod_Susep
    vSql = vSql & ", @ramo_id = " & gbllRamo_ID
    vSql = vSql & ", @evento_sinistro_id = " & cboCausa_Sinistro.ItemData(cboCausa_Sinistro.ListIndex)
    vSql = vSql & ", @dt_aviso_sinistro = '" & Format(mskData_Aviso, "yyyymmdd") & "'"
    vSql = vSql & ", @dt_ocorrencia_sinistro = '" & Format(mskData_Ocorrencia, "yyyymmdd") & "'"
    vSql = vSql & ", @dt_entrada_seguradora = '" & Format(mskData_Entrada, "yyyymmdd") & "'"
    vSql = vSql & ", @endereco = " & IIf(TxtEndereco_Ocorrencia <> "", "'" & TxtEndereco_Ocorrencia & "'", "NULL")
    vSql = vSql & ", @bairro = " & IIf(TxtBairro_Ocorrencia <> "", "'" & TxtBairro_Ocorrencia & "'", "NULL")
    vSql = vSql & ", @municipio_id = " & vMunicipio_id
    vSql = vSql & ", @municipio = " & IIf(TxtMunicipio_Ocorrencia <> "", "'" & TxtMunicipio_Ocorrencia & "'", "NULL")
    vSql = vSql & ", @estado = '" & IIf(TxtEstado_Ocorrencia <> "", TxtEstado_Ocorrencia, "XX") & "'"
    vSql = vSql & ", @agencia_id = " & IIf(mskAgencia_Aviso <> "", mskAgencia_Aviso, "NULL")
    vSql = vSql & ", @banco_id = 1"
    vSql = vSql & ", @situacao = '" & gblsSinistro_Situacao_ID & "'"
    vSql = vSql & ", @usuario = '" & cUserName & "'"
    vSql = vSql & ", @sinistro_id_lider = " & IIf(Trim(TxtNumero_Sinistro_Lider.Text) = "", "NULL", Trim(TxtNumero_Sinistro_Lider.Text))
    vSql = vSql & ", @endosso_id = " & IIf(Val(txtEndosso.Text) = 0, "NULL", txtEndosso.Text)
    'FLAVIO.BEZERRA - INICIO - 13/03/2013
    'vsql = vsql & "NULL, '"
    vSql = vSql & ", @moeda_id = " & dMoeda
    'FLAVIO.BEZERRA - FIM - 13/03/2013
    vSql = vSql & ", @processa_reintegracao_is = '" & IIf(chkReintegracaoIS.Value = 1, "s", "n") & "'"

    If cboSubevento_Sinistro.ListIndex = -1 Then
        vSql = vSql & ", @subevento_sinistro_id = " & "NULL"
    Else
        vSql = vSql & ", @subevento_sinistro_id = " & cboSubevento_Sinistro.ItemData(cboSubevento_Sinistro.ListIndex)
    End If
    vSql = vSql & ", @passivel_ressarcimento = NULL"
    
    'RCA 485 - ignora se status_sistema estiver bloqueado
    vSql = vSql & ", @ignorabloqueio = 1"

    Call Conexao_ExecutarSQL(gsSIGLASISTEMA, _
                             glAmbiente_id, _
                             App.Title, _
                             App.FileDescription, _
                             vSql, _
                             lConexaoLocal, False)

    ' Poderia ser uma �nica SP, mas antes os processos eram separados
'    vSql = "EXEC sinistro_marca_pgto_parc_spu " & _
'           gbldSinistro_ID & "," & gbllApolice_ID & "," & _
'           gbllSucursal_Seguradora_ID & "," & gbllSucursal_Seguradora_ID & "," & gbllRamo_ID & ",'" & _
'           cUserName & "', 'n' "                                                                           'FLAVIO.BEZERRA - 18/01/2013
    
    'FLAVIO.BEZERRA - IN�CIO - 18/01/2013
    vSql = "EXEC sinistro_marca_pgto_parc_spu "
    vSql = vSql & "  @sinistro_id = " & gbldSinistro_ID
    vSql = vSql & ", @apolice_id = " & gbllApolice_ID
    vSql = vSql & ", @sucursal_seguradora_id = " & gbllSucursal_Seguradora_ID
    vSql = vSql & ", @seguradora_cod_susep = " & gbllSucursal_Seguradora_ID
    vSql = vSql & ", @ramo_id = " & gbllRamo_ID
    vSql = vSql & ", @usuario = '" & cUserName & "'"
    vSql = vSql & ", @pgto_parcial = '" & IIf(chkPgto_Parcial_Co_Seguro.Value = 1, "s", "n") & "'"
       
    'RCA 485 - ignora se status_sistema estiver bloqueado
    vSql = vSql & ", @ignorabloqueio = 1"
    
    'FLAVIO.BEZERRA - FIM - 18/01/2013

    Call Conexao_ExecutarSQL(gsSIGLASISTEMA, _
                             glAmbiente_id, _
                             App.Title, _
                             App.FileDescription, _
                             vSql, _
                             lConexaoLocal, False)


    If Not Altera_Sinistro_RE Then
        Altera_Sinistro = False
        'mathayde
        RetornarTransacao (lConexaoLocal)
        Exit Function
    End If

    ConfirmarTransacao (lConexaoLocal)

    Exit Function
    Resume
Trata_Erro:
    TrataErroGeral "Altera��o Sinistro"
    RetornarTransacao (lConexaoLocal)
    Altera_Sinistro = False

End Function
Public Function Manutencao_SinistroBB() As Boolean

    Dim rs, Rs1, Rs2 As ADODB.Recordset
    Dim vSql As String
    Dim Itens_historico(0) As String
    Dim sTipo_Operacao() As String
    Dim sSinistroBB As String

    'Demanda 17922837 - IN�CIO
    Dim iDetalhamento As Integer
    Dim iLinha As Integer
    Dim iEvento As Long
    Dim sRemessa As String
    Dim iSaida As Long
    Dim Linha As String
    Dim apolice_id As Long
    Dim sucursal_seguradora_id As Long
    Dim seguradora_cod_susep As Long
    Dim ramo_id As Integer
    Dim data_atual As String
    
    data_atual = Right("0000" & Year(Date), 4) & Right("00" & Month(Date), 2) & Right("00" & Day(Date), 2)
    'Demanda 17922837 - FIM

    On Error GoTo Trata_Erro


    Manutencao_SinistroBB = True

    'sTipo_Operacao = Split(Tp_Altera_SinistroBB, ".")

    If GrdInformacao_SinistroBB.Rows > 0 Then
        '             If Tp_Altera_SinistroBB = "Incluir" Then ' quando inclui
        If sSinistroBBInclusao <> "" Then
            sSinistroBBInclusao = Split(sSinistroBBInclusao, ",")

            For a = 0 To UBound(sSinistroBBInclusao)
                sSinistroBB = sSinistroBBInclusao(a)

                'Demanda 17922837 - IN�CIO
                vSql = "Select Count(1) From evento_segbr_sinistro_atual_tb WITH (NOLOCK) "
                vSql = vSql & " Where sinistro_id = " & gbldSinistro_ID
                vSql = vSql & " And evento_bb_id = 1100 "
                vSql = vSql & " And IsNull(sinistro_BB,0) = 0 "
                vSql = vSql & " And banco_aviso_id = 1 "
    
                Set rs = Conexao_ExecutarSQL(gsSIGLASISTEMA, _
                                             glAmbiente_id, _
                                             App.Title, _
                                             App.FileDescription, _
                                             vSql, _
                                             lConexaoLocal, True)
    
                If rs(0) > 0 And bytModoOperacao = 2 And bytTipoRamo = 1 Then
                
                    'Inserir o Detalhamento
                    vSql = "Select Top 1 apolice_id, sucursal_seguradora_id, seguradora_cod_susep, ramo_id " & _
                            "From sinistro_detalhamento_tb WITH (NOLOCK) " & _
                            "Where sinistro_id = " & gbldSinistro_ID
                    
                    Set rs = Conexao_ExecutarSQL(gsSIGLASISTEMA, _
                             glAmbiente_id, _
                             App.Title, _
                             App.FileDescription, _
                             vSql, _
                             lConexaoLocal, True)
                    
                    If Not rs.EOF Then
                        apolice_id = rs(0)
                        sucursal_seguradora_id = rs(1)
                        seguradora_cod_susep = rs(2)
                        ramo_id = rs(3)
                    End If
                    
                    rs.Close
                                
                    vSql = "EXEC sinistro_detalhamento_spi "
                    vSql = vSql & gbldSinistro_ID & ","
                    vSql = vSql & apolice_id & ","
                    vSql = vSql & sucursal_seguradora_id & ","
                    vSql = vSql & seguradora_cod_susep & ","
                    vSql = vSql & ramo_id & ","
                    vSql = vSql & "'" & data_atual & "',"
                    vSql = vSql & " 0,"
                    vSql = vSql & " 'n',"
                    vSql = vSql & "'" & cUserName & "'"
                                                                            
                    Call Conexao_ExecutarSQL(gsSIGLASISTEMA, _
                                     glAmbiente_id, _
                                     App.Title, _
                                     App.FileDescription, _
                                     vSql, _
                                     vNunCx, _
                                     False, _
                                     False)
                                                                        
                    vSql = "Select IsNull(Max(detalhamento_id),0) From sinistro_detalhamento_tb WITH (NOLOCK) Where sinistro_id = " & gbldSinistro_ID
                    
                    Set rs = Conexao_ExecutarSQL(gsSIGLASISTEMA, _
                             glAmbiente_id, _
                             App.Title, _
                             App.FileDescription, _
                             vSql, _
                             lConexaoLocal, True)
                    
                    iDetalhamento = rs(0)
                    rs.Close
                    
                    vSql = "Select IsNull(Max(linha_id),0) + 1 From sinistro_linha_detalhamento_tb WITH (NOLOCK) Where sinistro_id = " & gbldSinistro_ID & " and detalhamento_id = " & iDetalhamento
                    
                    Set rs = Conexao_ExecutarSQL(gsSIGLASISTEMA, _
                             glAmbiente_id, _
                             App.Title, _
                             App.FileDescription, _
                             vSql, _
                             lConexaoLocal, True)
                    
                    iLinha = rs(0)
                    rs.Close
                    
                    Linha = "Disponibiliza��o manual do Sinistro BB N� " & sSinistroBB
                    
                    vSql = "EXEC sinistro_linha_detalhe_spi "
                    vSql = vSql & gbldSinistro_ID & ","
                    vSql = vSql & apolice_id & ","
                    vSql = vSql & sucursal_seguradora_id & ","
                    vSql = vSql & seguradora_cod_susep & ","
                    vSql = vSql & ramo_id & ","
                    vSql = vSql & iDetalhamento & ","
                    vSql = vSql & iLinha & ","
                    vSql = vSql & "'" & Linha & "',"
                    vSql = vSql & "'" & cUserName & "'"
                                                                            
                    Call Conexao_ExecutarSQL(gsSIGLASISTEMA, _
                                     glAmbiente_id, _
                                     App.Title, _
                                     App.FileDescription, _
                                     vSql, _
                                     vNunCx, _
                                     False, _
                                     False)
                
                    'Atualizar a situa��o do sinistro
                    vSql = "Select IsNull(evento_id,0), IsNull(Convert(Char, num_remessa),'') From evento_segbr_sinistro_atual_tb WITH (NOLOCK) Where sinistro_id = " & gbldSinistro_ID & " And evento_bb_id = 1100"
                    
                    Set rs = Conexao_ExecutarSQL(gsSIGLASISTEMA, _
                             glAmbiente_id, _
                             App.Title, _
                             App.FileDescription, _
                             vSql, _
                             lConexaoLocal, True)
                    
                    iEvento = rs(0)
                    sRemessa = Trim(rs(1))
                    
                    rs.Close
                    
                    If iEvento > 0 Then
                                                        
                        'Atualizar a situa��o do sinistro no tabel�o
                        vSql = "EXEC evento_segbr_sinistro_situacao_aviso_spu "
                        vSql = vSql & iEvento & ","
                        vSql = vSql & "'BB',"
                        vSql = vSql & "'" & cUserName & "',"
                        vSql = vSql & "'A',"
                        vSql = vSql & "Null,"
                        vSql = vSql & "Null,"
                        vSql = vSql & sSinistroBB
                        
                        Call Conexao_ExecutarSQL(gsSIGLASISTEMA, _
                                            glAmbiente_id, _
                                            App.Title, _
                                            App.FileDescription, _
                                            vSql, _
                                            vNunCx, _
                                            False, _
                                            False)
                    End If
                    
                    'Atualizar a tabela sinistro_tb para sem_comunicacao_bb = null
                    vSql = "EXEC SEGS10268_SPU "
                    vSql = vSql & gbldSinistro_ID & ","
                    vSql = vSql & "Null"
                    
                    Call Conexao_ExecutarSQL(gsSIGLASISTEMA, _
                                        glAmbiente_id, _
                                        App.Title, _
                                        App.FileDescription, _
                                        vSql, _
                                        vNunCx, _
                                        False, _
                                        False)
                
                    If sRemessa <> "" Then
            
                        'Dar baixa no log
                        vSql = "Select IsNull(saida_gtr_id,0) "
                        vSql = vSql & " From interface_db.dbo.saida_gtr_atual_tb WITH (NOLOCK) "
                        vSql = vSql & " Where cod_remessa = '" & sRemessa & "' "
                        vSql = vSql & " And evento_BB = 1100 "
                        vSql = vSql & " And situacao_mq in ('N','P','E')"
                        
                        Set rs = Conexao_ExecutarSQL(gsSIGLASISTEMA, _
                                 glAmbiente_id, _
                                 App.Title, _
                                 App.FileDescription, _
                                 vSql, _
                                 lConexaoLocal, True)
                        
                        If Not rs.EOF Then
                            iSaida = rs(0)
                            
                            rs.Close
                            
                            If iSaida > 0 Then
                            
                                vSql = "EXEC interface_db.dbo.correcao_remessa_spu "
                                vSql = vSql & iSaida & ","
                                vSql = vSql & "'t',"
                                vSql = vSql & "1,"
                                vSql = vSql & "'" & cUserName & "'"
                                
                                Call Conexao_ExecutarSQL(gsSIGLASISTEMA, _
                                                    glAmbiente_id, _
                                                    App.Title, _
                                                    App.FileDescription, _
                                                    vSql, _
                                                    vNunCx, _
                                                    False, _
                                                    False)
                                
                            End If
                        End If
                    End If
                End If
                'Demanda 17922837 - FIM


                If bytModoOperacao <> 1 Then
                    Itens_historico(0) = "N� SINISTRO BB: " & sSinistroBB
                    If Not Inclui_Evento("10022", gblsSinistro_Situacao_ID, "NULL", Itens_historico, , , sSinistroBB) Then
                        Manutencao_SinistroBB = False
                        MsgBox "Erro Inclus�o Evento Exclus�o de N�mero Sinistro BB", vbOKOnly + vbCritical, App.EXEName
                        RetornarTransacao (lConexaoLocal)
                        Exit Function
                    End If

                ElseIf Incluiu_Novo_SinistroBB = True Then

                    vSql = " SELECT * FROM web_seguros_db..robo_aviso_sinistro_log WITH (NOLOCK) "
                    vSql = vSql & " WHERE   num_aviso = '" & sSinistroBB & "'"

                    Set Rs1 = Conexao_ExecutarSQL(gsSIGLASISTEMA, _
                                                  glAmbiente_id, _
                                                  App.Title, _
                                                  App.FileDescription, _
                                                  vSql, _
                                                  lConexaoLocal, True)

                    If Rs1.EOF Then
                        Rs1.Close

                        'Rafael Oshiro 25/02/2005
                        vSql = "SELECT 1 "
                        vSql = vSql & " FROM interface_db..entrada_gtr_tb e WITH (NOLOCK) "
                        vSql = vSql & " WHERE Sinistro_BB = '" & sSinistroBB & "'"
                        vSql = vSql & "   AND evento_bb = 2000 "
                        vSql = vSql & "   AND NOT EXISTS (SELECT * FROM interface_db..saida_gtr_tb WITH (NOLOCK) WHERE sinistro_bb = e.sinistro_bb and evento_bb = 1100) "
                        vSql = vSql & "   AND NOT EXISTS (SELECT * FROM sinistro_bb_tb WITH (NOLOCK) WHERE sinistro_bb = e.sinistro_bb) "

                        Set Rs2 = Conexao_ExecutarSQL(gsSIGLASISTEMA, _
                                                      glAmbiente_id, _
                                                      App.Title, _
                                                      App.FileDescription, _
                                                      vSql, _
                                                      lConexaoLocal, True)

                        If Rs2.EOF Then

                            Manutencao_SinistroBB = False
                            MsgBox "SinistroBB informado n�o est� no LOG. Inclus�o cancelada.", vbOKOnly + vbCritical, App.EXEName
                            Exit Function

                        End If
                        Rs2.Close

                    End If
                End If

                '                sSinistroBBInclusao = Split(sSinistroBBInclusao, ",")
                '
                '                For a = 0 To UBound(sSinistroBBInclusao)
                '                    sSinistroBB = sSinistroBBInclusao(a)

                vSql = vSql & vbNewLine
                vSql = vSql & "EXEC sinistro_bb_spi "
                vSql = vSql & " @sinistro_id = " & gbldSinistro_ID
                vSql = vSql & ", @apolice_id = " & gbllApolice_ID
                vSql = vSql & ", @sucursal_seguradora_id = " & gbllSucursal_Seguradora_ID
                vSql = vSql & ", @seguradora_cod_susep = " & gbllSeguradora_Cod_Susep
                vSql = vSql & ", @ramo_id = " & gbllRamo_ID
                vSql = vSql & ", @sinistro_bb = " & sSinistroBB
                vSql = vSql & ", @usuario = '" & cUserName & "'"
                vSql = vSql & ", @dt_inicio_vigencia = '" & Format(CStr(gsDHEntrada), "YYYY-MM-DD") & "'"
                
                'RCA 485 - ignora se status_sistema estiver bloqueado
                vSql = vSql & ", @ignorabloqueio = 1"

                If vSql <> "" Then
                    Call Conexao_ExecutarSQL(gsSIGLASISTEMA, _
                                             glAmbiente_id, _
                                             App.Title, _
                                             App.FileDescription, _
                                             vSql, _
                                             lConexaoLocal, False)
                    vSql = ""

                End If

            Next
        End If
        'ElseIf Tp_Altera_SinistroBB = "Inativar" Then
        If sSinistroBBAlteracao <> "" Then

            sSinistroBBAlteracao = Split(sSinistroBBAlteracao, ",")

            For a = 0 To UBound(sSinistroBBAlteracao)
                sSinistroBB = sSinistroBBAlteracao(a)

                If bytModoOperacao <> 1 Then    ' canelamento de sinistro
                    Itens_historico(0) = "N� SINISTRO BB: " & sSinistroBB

                    If Not Inclui_Evento("10129", gblsSinistro_Situacao_ID, "NULL", Itens_historico, , , sSinistroBB) Then

                        Manutencao_SinistroBB = False
                        MsgBox "Erro Inclus�o Evento Cancelamento de Sinistro BB", vbOKOnly + vbCritical, App.EXEName

                        RetornarTransacao (lConexaoLocal)

                        Exit Function

                    End If

                End If

                '
                '                sSinistroBBAlteracao = Split(sSinistroBBAlteracao, ",")
                '
                '                For a = 0 To UBound(sSinistroBBAlteracao)
                '                    sSinistroBB = sSinistroBBAlteracao(a)

                vSql = vSql & vbNewLine
                vSql = vSql & "EXEC sinistro_bb_spu "
                vSql = vSql & gbldSinistro_ID
                vSql = vSql & ", " & gbllApolice_ID
                vSql = vSql & ", " & gbllSucursal_Seguradora_ID
                vSql = vSql & ", " & gbllSeguradora_Cod_Susep
                vSql = vSql & ", " & gbllRamo_ID
                vSql = vSql & ", " & sSinistroBB
                vSql = vSql & ", '" & cUserName & "'"
                vSql = vSql & ", '" & Format(CStr(gsDHEntrada), "YYYYMMDD") & "'"

                If vSql <> "" Then
                    Call Conexao_ExecutarSQL(gsSIGLASISTEMA, _
                                             glAmbiente_id, _
                                             App.Title, _
                                             App.FileDescription, _
                                             vSql, _
                                             lConexaoLocal, False)
                    vSql = ""

                End If
                '                Next

                If Obtem_GTR_Implantado(CStr(gbldSinistro_ID)) And Sinistro_GTR(sSinistroBB) And _
                   Not Existe_Pgtos(sSinistroBB) And Not Existe_Indeferimento(sSinistroBB) Then

                    If Not Gera_Cancelamento_Sinistro_BB(sSinistroBB) Then

                        Manutencao_SinistroBB = False
                        MsgBox "Erro gera��o do cancelamento do Sinistro BB", vbOKOnly + vbCritical, App.EXEName
                        Exit Function

                    End If
                End If
            Next
        End If
        'ElseIf Tp_Altera_SinistroBB = "Excluir" Then ' quando clica no bot�o excluir se for excluir o sinistro_bb
        'ElseIf sTipo_Operacao(2) = "Excluir" Then
        If sSinistroBBExclusao <> "" Then
            If bytModoOperacao <> 1 Then
                Itens_historico(0) = "N� SINISTRO BB: " & sSinistroBB
                If Not Inclui_Evento("10023", gblsSinistro_Situacao_ID, "NULL", Itens_historico, , , sSinistroBB) Then

                    Manutencao_SinistroBB = False
                    MsgBox "Erro Inclus�o Evento Exclus�o de N�mero Sinistro BB", vbOKOnly + vbCritical, App.EXEName

                    RetornarTransacao (lConexaoLocal)

                    Exit Function

                End If
            End If

            sSinistroBBExclusao = Split(sSinistroBBExclusao, ",")

            For a = 0 To UBound(sSinistroBBExclusao)
                sSinistroBB = sSinistroBBExclusao(a)

                vSql = vSql & vbNewLine
                vSql = vSql & "EXEC sinistro_bb_spd "
                vSql = vSql & "  @sinistro_id = " & gbldSinistro_ID
                vSql = vSql & ", @apolice_id  = " & gbllApolice_ID
                vSql = vSql & ", @sucursal_seguradora_id = " & gbllSucursal_Seguradora_ID
                vSql = vSql & ", @seguradora_cod_susep = " & gbllSeguradora_Cod_Susep
                vSql = vSql & ", @ramo_id = " & gbllRamo_ID
                'mathayde
                'vsql = vsql & ", " & txtNumero_Sinistro_InformacaoBB.Text
                vSql = vSql & ", @sinistro_bb = " & sSinistroBB
                vSql = vSql & ", @usuario = '" & cUserName & "'"
                
                'RCA 485 - ignora se status_sistema estiver bloqueado
                vSql = vSql & ", @ignorabloqueio = 1"

                If vSql <> "" Then
                    Call Conexao_ExecutarSQL(gsSIGLASISTEMA, _
                                             glAmbiente_id, _
                                             App.Title, _
                                             App.FileDescription, _
                                             vSql, _
                                             lConexaoLocal, False)
                    vSql = ""

                End If
            Next
        End If
        'End If
    End If


    Exit Function

Trata_Erro:
    TrataErroGeral "Manuten��o de Sinistro BB"
    RetornarTransacao (lConexaoLocal)
    Manutencao_SinistroBB = False
    Resume
End Function


Public Function Altera_Sinistro_RE() As Boolean


    Altera_Sinistro_RE = True
    On Error GoTo Trata_Erro

    If bytTipoRamo = bytTipoRamo_Vida Then
        Exit Function
    End If


    'Altera Sinistro Ramos Elementares
    vSql = "EXEC sinistro_re_spu "
    vSql = vSql & "  @sinistro_id = " & gbldSinistro_ID
    vSql = vSql & ", @apolice_id = " & gbllApolice_ID
    vSql = vSql & ", @sucursal_seguradora_id = " & gbllSucursal_Seguradora_ID
    vSql = vSql & ", @seguradora_cod_susep = " & gbllSeguradora_Cod_Susep
    vSql = vSql & ", @ramo_id = " & gbllRamo_ID
    vSql = vSql & ", @cod_objeto_segurado = " & CInt(Left(Trim(CmbCodigo_Objeto_Ocorrencia), 10))
    vSql = vSql & ", @dt_inicio_vigencia_seg = '" & Format(vDt_Inicio_Vigencia_Seg, "yyyymmdd") & "'"
    vSql = vSql & ", @usuario = '" & cUserName & "'"

    If vTranspInternacional = True Then
        vSql = vSql & ", @num_averbacao = " & IIf((mskAverbacao.Visible = True And mskAverbacao.Text = "_______") Or mskAverbacao.Visible = False, "NULL", mskAverbacao.ClipText)
    End If
    
    'RCA 485 - ignora se status_sistema estiver bloqueado
    vSql = vSql & ", @ignorabloqueio = 1"

    Call Conexao_ExecutarSQL(gsSIGLASISTEMA, _
                             glAmbiente_id, _
                             App.Title, _
                             App.FileDescription, _
                             vSql, _
                             lConexaoLocal, _
                             False, _
                             False)


    Exit Function

Trata_Erro:
    TrataErroGeral "Altera��o de Sinistro Ramos Elementares"
    Altera_Sinistro_RE = False

End Function
Private Sub InicializaInterfaceLocal_Form()
    Dim sTipoProcesso_Descricao As String
    Dim avulso As Boolean

    txtAuxiliar.Visible = False
    txtAuxiliar.Text = "Cancelado"

    btnVoltar.Visible = True
    btnAplicar.Visible = Not (bytModoOperacao = 3)
    FmeInformacoes_BB.Enabled = Not (sOperacaoCosseguro = "C")
    FmeDados_Aviso.Visible = True
    FmeDados_Endosso.Visible = True
    FmeDados_Sinistrado.Visible = (bytTipoRamo = bytTipoRamo_Vida)
    FmeEndereco_Ocorrencia.Visible = Not (bytTipoRamo = bytTipoRamo_Vida)
    btnBBAdicionar.Visible = Not (bytModoOperacao = 3)
    btnBBRemover.Visible = False
    btnBBInativar.Visible = False
    BtnSelecionar_Proposta.Visible = (bytModoOperacao <> 3 Or bytTipoRamo <> bytTipoRamo_Vida)    'GABRIEL
    txtNumero_Sinistro_InformacaoBB.Visible = Not (bytModoOperacao = 3)

    LblNumero_Sinistro_Lider.Visible = (sOperacaoCosseguro = "C")
    TxtNumero_Sinistro_Lider.Visible = (sOperacaoCosseguro = "C")

    vSelSinistroBB = False

    '    FmeDados_Sinistrado.Enabled = Not (bytModoOperacao = 3)
    '    FmeEndereco_Ocorrencia.Enabled = Not (bytModoOperacao = 3)
    '    FmeDados_Aviso.Enabled = Not (bytModoOperacao = 3)
    '    FmeDados_Endosso.Enabled = Not (bytModoOperacao = 3)
    '    FmeInformacoes_BB.Enabled = Not (bytModoOperacao = 3)

    FmeDados_Sinistrado.Enabled = Not (sOperacaoCosseguro = "C")    'RALVES
    FmeInformacoes_BB.Enabled = Not (sOperacaoCosseguro = "C")   'RALVES

    chkReintegracaoIS.Enabled = False

    lblNumero_Sinistro_BB.Visible = Not (bytModoOperacao = 3)

    'txtData_Inclusao.Text = Format(CStr(gsDHEntrada), "dd/mm/yyyy")
    txtData_Inclusao.Text = Format(CStr(Data_Sistema), "dd/mm/yyyy")

    If bytTipoRamo <> bytTipoRamo_Vida Then
        If Verifica_Transporte(avulso) Then    'Habilita os campos de transporte internacional
            vTranspInternacional = True
            If avulso Then
                mskAverbacao.Visible = False
                lblAverbacao.Visible = False
            Else
                mskAverbacao.Visible = True
                lblAverbacao.Visible = True
            End If
        End If
    End If


    'If Produto_Permite_Reintegracao_IS(CStr(gbllProduto_ID), CStr(gbllRamo_ID)) Or gbllProduto_ID = 1204 Or gbllProduto_ID = 1152 Then

    If bytModoOperacao <> 1 Then
        chkReintegracaoIS.Enabled = True
    End If

    'Else
    'chkReintegracaoIS.Enabled = False
    'End If


    Select Case bytModoOperacao
    Case 1, 2

        btnVoltar.Caption = "Cancelar"
        sTipoProcesso_Descricao = "Manuten��o"

    Case 3

        btnVoltar.Caption = "Voltar"
        sTipoProcesso_Descricao = "Consulta"

    End Select

    'FLAVIO.BEZERRA - IN�CIO - 28/01/2013
    If sOperacaoCosseguro <> "C" Then
        chkPgto_Parcial_Co_Seguro.Visible = False
    End If
    
    If bytModoOperacao <> strTipoProcesso_Avaliar_Cosseguro Or _
    sOperacaoCosseguro = "C" And bytModoOperacao <> strTipoProcesso_Avaliar Then
        
        chkPgto_Parcial_Co_Seguro.Enabled = False
    End If
    'FLAVIO.BEZERRA - FIM - 28/01/2013
    
    If sOperacaoCosseguro = "C" Then
        Me.Caption = App.EXEName & " - " & sTipoProcesso_Descricao & " de Aviso do Sinistro Cosseguro - " & Ambiente
    Else
        Me.Caption = App.EXEName & " - " & sTipoProcesso_Descricao & " de Aviso do Sinistro - " & Ambiente
    End If
    
End Sub


Public Sub Monta_FmeEndereco_Ocorrencia(lProposta_ID As Long)

    Dim vSql As String
    Dim Rst_EnderecoRisco As ADODB.Recordset
    Dim Rst_BairroRisco As ADODB.Recordset


    If bytTipoRamo <> bytTipoRamo_RE And bytTipoRamo <> bytTipoRamo_Rural Then
        Exit Sub
    End If

    vSql = "SELECT b.cod_objeto_segurado, a.end_risco_id, a.endereco, a.bairro, a.municipio, a.estado, b.dt_inicio_vigencia_seg, b.dt_fim_vigencia_seg, 1"
    vSql = vSql & " FROM endereco_risco_tb a  WITH (NOLOCK) , seguro_residencial_tb b WITH (NOLOCK) "
    vSql = vSql & " Where b.Proposta_id = " & lProposta_ID
    vSql = vSql & " AND a.Proposta_id = b.Proposta_id"
    vSql = vSql & " AND a.end_risco_id = b.end_risco_id"
    vSql = vSql & " AND b.dt_fim_vigencia_seg IS NULL"

    vSql = vSql & " Union"
    vSql = vSql & " SELECT c.cod_objeto_segurado, a.end_risco_id, a.endereco, a.bairro, a.municipio, a.estado, c.dt_inicio_vigencia_seg, c.dt_fim_vigencia_seg, 2"
    vSql = vSql & " FROM endereco_risco_tb a  WITH (NOLOCK) , seguro_empresarial_tb c WITH (NOLOCK) "
    vSql = vSql & " Where c.Proposta_id = " & lProposta_ID
    vSql = vSql & " AND a.Proposta_id = c.Proposta_id"
    vSql = vSql & " AND a.end_risco_id = c.end_risco_id"
    vSql = vSql & " AND c.dt_fim_vigencia_seg IS NULL"

    vSql = vSql & " Union"
    vSql = vSql & " SELECT d.cod_objeto_segurado, a.end_risco_id, a.endereco, a.bairro, a.municipio, a.estado, d.dt_inicio_vigencia_seg, d.dt_fim_vigencia_seg, 3"
    vSql = vSql & " FROM endereco_risco_tb a  WITH (NOLOCK) , seguro_condominio_tb d WITH (NOLOCK) "
    vSql = vSql & " Where d.Proposta_id = " & lProposta_ID
    vSql = vSql & " AND a.Proposta_id = d.Proposta_id"
    vSql = vSql & " AND a.end_risco_id = d.end_risco_id"
    vSql = vSql & " AND d.dt_fim_vigencia_seg IS NULL"

    vSql = vSql & " Union"
    vSql = vSql & " SELECT e.cod_objeto_segurado, a.end_risco_id, a.endereco, a.bairro, a.municipio, a.estado, e.dt_inicio_vigencia_seg, e.dt_fim_vigencia_seg, 4"
    vSql = vSql & " FROM endereco_risco_tb a  WITH (NOLOCK) , seguro_maquinas_tb e WITH (NOLOCK) "
    vSql = vSql & " Where e.Proposta_id = " & lProposta_ID
    vSql = vSql & " AND a.Proposta_id = e.Proposta_id"
    vSql = vSql & " AND a.end_risco_id = e.end_risco_id"
    vSql = vSql & " AND e.dt_fim_vigencia_seg IS NULL"

    vSql = vSql & " Union"
    vSql = vSql & " SELECT f.cod_objeto_segurado, a.end_risco_id, a.endereco, a.bairro, a.municipio, a.estado, f.dt_inicio_vigencia_seg, f.dt_fim_vigencia_seg, 5"
    vSql = vSql & " FROM endereco_risco_tb a  WITH (NOLOCK) , seguro_aceito_tb f WITH (NOLOCK) "
    vSql = vSql & " Where f.Proposta_id = " & lProposta_ID
    vSql = vSql & " AND a.Proposta_id = f.Proposta_id"
    vSql = vSql & " AND a.end_risco_id = f.end_risco_id"
    vSql = vSql & " AND f.dt_fim_vigencia_seg IS NULL"

    vSql = vSql & " Union"
    vSql = vSql & " SELECT g.cod_objeto_segurado, a.end_risco_id, a.endereco, a.bairro, a.municipio, a.estado, g.dt_inicio_vigencia_seg, g.dt_fim_vigencia_seg, 6"
    vSql = vSql & " FROM endereco_risco_tb a  WITH (NOLOCK) , seguro_avulso_tb g WITH (NOLOCK) "
    vSql = vSql & " Where g.Proposta_id = " & lProposta_ID
    vSql = vSql & " AND a.Proposta_id = g.Proposta_id"
    vSql = vSql & " AND a.end_risco_id = g.end_risco_id"
    vSql = vSql & " AND g.dt_fim_vigencia_seg IS NULL"

    vSql = vSql & " Union"
    vSql = vSql & " SELECT h.cod_objeto_segurado, NULL, h.desc_mercadoria, NULL, NULL, NULL, h.dt_inicio_vigencia_seg, h.dt_fim_vigencia_seg, 7"
    vSql = vSql & " FROM seguro_transporte_tb h WITH (NOLOCK) "
    vSql = vSql & " Where h.Proposta_id = " & lProposta_ID
    vSql = vSql & " AND h.dt_fim_vigencia_seg IS NULL"

    vSql = vSql & " Union"
    vSql = vSql & " SELECT h.cod_objeto_segurado, NULL, h.desc_mercadoria, NULL, NULL, NULL, h.dt_inicio_vigencia_seg, h.dt_fim_vigencia_seg, 7"
    vSql = vSql & " FROM seguro_transporte_tb h WITH (NOLOCK) "
    vSql = vSql & " Where h.Proposta_id = " & lProposta_ID
    vSql = vSql & " and dt_fim_vigencia_seg = (select max(dt_fim_vigencia_seg)"
    vSql = vSql & "                            from seguro_transporte_tb h1 WITH (NOLOCK) "
    vSql = vSql & "                            where h1.Proposta_id = " & lProposta_ID
    vSql = vSql & "                            and h1.cod_objeto_segurado = h.cod_objeto_segurado"
    vSql = vSql & "                            and h1.dt_fim_vigencia_seg is not null)"
    vSql = vSql & " and not exists            (select 1 from seguro_transporte_tb h2 WITH (NOLOCK) "
    vSql = vSql & "                            where h2.Proposta_id = " & lProposta_ID
    vSql = vSql & "                            and h2.cod_objeto_segurado = h.cod_objeto_segurado"
    vSql = vSql & "                            and h2.dt_fim_vigencia_seg is null)"


    vSql = vSql & " Union"
    vSql = vSql & " SELECT i.cod_objeto_segurado, a.end_risco_id, a.endereco, a.bairro, a.municipio, a.estado, i.dt_inicio_vigencia_seg, i.dt_fim_vigencia_seg, 8"
    vSql = vSql & " FROM endereco_risco_tb a WITH (NOLOCK) , seguro_generico_tb i WITH (NOLOCK) "
    vSql = vSql & " Where i.Proposta_id = " & lProposta_ID
    vSql = vSql & " AND a.Proposta_id = i.Proposta_id"
    vSql = vSql & " AND a.end_risco_id = i.end_risco_id"
    vSql = vSql & " AND i.dt_fim_vigencia_seg IS NULL"


    vSql = vSql & " UNION"
    vSql = vSql & " SELECT j.cod_objeto_segurado, NULL, NULL, NULL, NULL, NULL, j.dt_inicio_vigencia_seg, NULL, 9"
    vSql = vSql & " FROM seguro_penhor_rural_tb j WITH (NOLOCK) "
    vSql = vSql & " WHERE j.proposta_id = " & lProposta_ID

    vSql = vSql & " UNION"
    vSql = vSql & " SELECT k.cod_objeto_segurado, NULL, NULL, NULL, NULL, NULL, k.dt_inicio_vigencia_seg, NULL, 10"
    vSql = vSql & " FROM seguro_consorcio_quebra_garantia_tb k WITH (NOLOCK) "
    vSql = vSql & " WHERE k.proposta_id = " & lProposta_ID


    Set Rst_EnderecoRisco = Conexao_ExecutarSQL(gsSIGLASISTEMA, _
                                                glAmbiente_id, _
                                                App.Title, _
                                                App.FileDescription, _
                                                vSql, _
                                                lConexaoLocal, True)


    If Not Rst_EnderecoRisco.EOF Then

        If bytModoOperacao <> 3 Then
            FmeEndereco_Ocorrencia.Enabled = (Rst_EnderecoRisco(8) <> "7")
        End If

        CmbCodigo_Objeto_Ocorrencia.Clear

        While Not Rst_EnderecoRisco.EOF


            '            If bytModoOperacao = 3 Then
            '                CmbCodigo_Objeto_Ocorrencia.ListIndex = Rst_EnderecoRisco(0)
            '            Else
            CmbCodigo_Objeto_Ocorrencia.AddItem Rst_EnderecoRisco(0) & Space(200) & IIf(IsNull(Rst_EnderecoRisco(6)), "", Format(Rst_EnderecoRisco(6), "dd/mm/yyyy"))
            CmbCodigo_Objeto_Ocorrencia.ItemData(CmbCodigo_Objeto_Ocorrencia.NewIndex) = Rst_EnderecoRisco(0)
            '            End If

            If Rst_EnderecoRisco(3) = "" Or IsNull(Rst_EnderecoRisco(3)) Then

                vSql = "      Select Endereco = ISNULL(Endereco,''),bairro=ISNULL(bairro,''),municipio=ISNULL(municipio,''),estado=ISNULL(estado,'')" & vbNewLine
                vSql = vSql & "  from seguros_db.dbo.sinistro_tb  WITH (NOLOCK) " & vbNewLine
                vSql = vSql & "where sinistro_id = " & gbldSinistro_ID

                Set Rst_BairroRisco = Conexao_ExecutarSQL(gsSIGLASISTEMA, _
                                                          glAmbiente_id, _
                                                          App.Title, _
                                                          App.FileDescription, _
                                                          vSql, _
                                                          lConexaoLocal, True)


                If Not Rst_BairroRisco.EOF Then
                    'TxtEndereco_Ocorrencia.Text = Rst_BairroRisco(0)
                    'TxtBairro_Ocorrencia.Text = MudaAspaSimples(Rst_BairroRisco(1))
                    'TxtMunicipio_Ocorrencia.Text = IIf(IsNull(Rst_BairroRisco(2)), "", Rst_BairroRisco(2))
                    'TxtEstado_Ocorrencia.Text = Rst_BairroRisco(3)
                    TxtEndereco_Ocorrencia.Text = Rst_BairroRisco(0) & ""    'F.BEZERRA 16/10/2012
                    TxtBairro_Ocorrencia.Text = MudaAspaSimples(Rst_BairroRisco(1)) & ""
                    TxtMunicipio_Ocorrencia.Text = IIf(IsNull(Rst_BairroRisco(2)), "", Rst_BairroRisco(2)) & ""
                    TxtEstado_Ocorrencia.Text = Rst_BairroRisco(3) & ""
                End If
                Rst_BairroRisco.Close

            Else

                'TxtEndereco_Ocorrencia.Text = Rst_EnderecoRisco(2)
                'TxtBairro_Ocorrencia.Text = MudaAspaSimples(Rst_EnderecoRisco(3))
                'TxtMunicipio_Ocorrencia.Text = IIf(IsNull(Rst_EnderecoRisco(4)), "", Rst_EnderecoRisco(4))
                'TxtEstado_Ocorrencia.Text = Rst_EnderecoRisco(5)
                'vDt_Inicio_Vigencia_Seg = Rst_EnderecoRisco(6)
                TxtEndereco_Ocorrencia.Text = Rst_EnderecoRisco(2) & ""    'F.BEZERRA 16/10/2012
                TxtBairro_Ocorrencia.Text = MudaAspaSimples(Rst_EnderecoRisco(3)) & ""
                TxtMunicipio_Ocorrencia.Text = IIf(IsNull(Rst_EnderecoRisco(4)), "", Rst_EnderecoRisco(4)) & ""
                TxtEstado_Ocorrencia.Text = Rst_EnderecoRisco(5) & ""
                vDt_Inicio_Vigencia_Seg = Rst_EnderecoRisco(6) & ""

            End If

            Rst_EnderecoRisco.MoveNext
        Wend
        Rst_EnderecoRisco.Close

    End If
    '    End If
    With CmbCodigo_Objeto_Ocorrencia
        If .ListCount <> 0 Then
            .ListIndex = 0
            'cristovao.rodrigues 17/06/2013 atribuir primeiro valor para vEnd_Risco_id
            vEnd_Risco_id = Trim(Left(.Text, 3))
        End If
    End With

    bCarregaEnderecoDaCombo = True

End Sub

Public Function Verifica_Transporte(ByRef avulso As Boolean) As Boolean
    Dim vSql As String
    Dim rs As ADODB.Recordset

    Verifica_Transporte = False

    If gbllRamo_ID <> 22 Then
        Exit Function
    End If

    vSql = "SELECT proposta_bb FROM proposta_transporte_tb WITH (NOLOCK) " & _
           "WHERE proposta_id = " & gbllProposta_ID

    Set rs = Conexao_ExecutarSQL(gsSIGLASISTEMA, _
                                 glAmbiente_id, _
                                 App.Title, _
                                 App.FileDescription, _
                                 vSql, _
                                 lConexaoLocal, _
                                 True, _
                                 False)
    If Not rs.EOF Then
        Verifica_Transporte = True
        avulso = True
        rs.Close
        Exit Function
    End If

    rs.Close

    vSql = "SELECT fatura_id FROM fatura_tb WITH (NOLOCK) " & _
         " WHERE apolice_id = " & gbllApolice_ID & _
         " AND sucursal_seguradora_id = " & gbllSucursal_Seguradora_ID & _
         " AND seguradora_cod_susep = " & gbllSeguradora_Cod_Susep & _
         " AND ramo_id = " & gbllRamo_ID

    Set rs = Conexao_ExecutarSQL(gsSIGLASISTEMA, _
                                 glAmbiente_id, _
                                 App.Title, _
                                 App.FileDescription, _
                                 vSql, _
                                 lConexaoLocal, _
                                 True, _
                                 False)

    If Not rs.EOF Then
        Verifica_Transporte = True
        rs.Close
    Else
        rs.Close
        vSql = "SELECT num_cobranca FROM agendamento_cobranca_tb WITH (NOLOCK) " & _
             " WHERE apolice_id = " & gbllApolice_ID & _
             " AND proposta_id = " & gbllProposta_ID


        Set rs = Conexao_ExecutarSQL(gsSIGLASISTEMA, _
                                     glAmbiente_id, _
                                     App.Title, _
                                     App.FileDescription, _
                                     vSql, _
                                     lConexaoLocal, _
                                     True, _
                                     False)

        If rs.EOF Then
            Verifica_Transporte = True
        End If
        rs.Close
    End If

End Function

Public Function Altera_Sinistrado() As Boolean

    Dim rs As ADODB.Recordset, vSql As String
    Dim SegNome As String, SegCPF As String
    Dim SegDtNasc As String, SegSexo As String
    Dim Itens_historico(0) As String


    AbrirTransacao (lConexaoLocal)

    Altera_Sinistrado = True

    On Error GoTo Trata_Erro

    If bytTipoRamo = bytTipoRamo_RE Or bytTipoRamo = bytTipoRamo_Rural Then
        Exit Function
    End If

    If sOperacaoCosseguro = "C" Then
        SegNome = "Sinistro Cosseguro Aceito"
        SegCPF = ""
        SegSexo = "M"    'Existe regra na tabela para sexo
        SegDtNasc = ""
    Else
        SegNome = Trim(MudaAspaSimples(txtSinistro_Vida_Nome.Text))
        SegCPF = mskCPF_Sinistrado.ClipText
        'SegSexo = IIf(txtSinistro_Vida_Sexo = "MASCULINO", "M", "F")
        SegSexo = IIf(CmbVidaSexo.Text = "Masculino", "M", "F")
        SegDtNasc = Format(CStr(mskDtNasc_Sinistrado.Text), "yyyymmdd")
    End If


    ' Altera Sinistrado
    vSql = "EXEC sinistro_vida_spu "
    vSql = vSql & "  @Sinistro_id = " & gbldSinistro_ID
    vSql = vSql & ", @Apolice_id = " & gbllApolice_ID
    vSql = vSql & ", @Sucursal_seguradora_id = " & gbllSucursal_Seguradora_ID
    vSql = vSql & ", @Seguradora_cod_susep = " & gbllSeguradora_Cod_Susep
    vSql = vSql & ", @Ramo_id = " & gbllRamo_ID
    vSql = vSql & ", @Nome = '" & SegNome & "'"
    vSql = vSql & ", @CPF = '" & SegCPF & "'"
    vSql = vSql & ", @Sexo = '" & SegSexo & "'"
    vSql = vSql & ", @Dt_Nascimento = '" & SegDtNasc & "'"
    vSql = vSql & ", @usuario = '" & cUserName & "'"
    
    'RCA 485 - ignora se status_sistema estiver bloqueado
    vSql = vSql & ", @ignorabloqueio = 1"

    Call Conexao_ExecutarSQL(gsSIGLASISTEMA, _
                             glAmbiente_id, _
                             App.Title, _
                             App.FileDescription, _
                             vSql, _
                             lConexaoLocal, False)

    If vExisteSinistro Then
        Itens_historico(0) = "NOME: " & SegNome
        If Not Inclui_Evento("10009", gblsSinistro_Situacao_ID, "NULL", Itens_historico) Then
            GoTo Trata_Erro
        End If
    End If

    ConfirmarTransacao (lConexaoLocal)

    Exit Function

Trata_Erro:
    TrataErroGeral "Altera��o de Sinistrado"
    RetornarTransacao (lConexaoLocal)
    Altera_Sinistrado = False

End Function

Public Function Inclui_Sinistrado() As Boolean

    Dim vSql As String
    Dim SegNome As String, SegCPF As String
    Dim SegDtNasc As String, SegSexo As String
    Dim Itens_historico(0) As String

    Inclui_Sinistrado = True

    AbrirTransacao (lConexaoLocal)

    On Error GoTo Trata_Erro

    If bytTipoRamo = bytTipoRamo_Rural Or bytTipoRamo = bytTipoRamo_RE Then
        Exit Function
    End If

    If sOperacaoCosseguro = "C" Then
        SegNome = "Sinistro Cosseguro Aceito"
        SegCPF = ""
        SegSexo = "M"    'Existe regra na tabela para sexo
        SegDtNasc = ""
    Else
        SegNome = Trim(MudaAspaSimples(txtSinistro_Vida_Nome.Text))
        SegCPF = mskCPF_Sinistrado.ClipText
        'SegSexo = IIf(txtSinistro_Vida_Sexo.Text = "Masculino", "M", "F")
        SegSexo = IIf(CmbVidaSexo.Text = "Masculino", "M", "F")
        SegDtNasc = Format(CStr(mskDtNasc_Sinistrado.Text), "yyyymmdd")
    End If


    ' Inclui Sinistrado
    vSql = "EXEC sinistro_vida_spi "
    vSql = vSql & "  @Sinistro_id = " & gbldSinistro_ID
    vSql = vSql & ", @Apolice_id = " & gbllApolice_ID
    vSql = vSql & ", @Sucursal_seguradora_id = " & gbllSucursal_Seguradora_ID
    vSql = vSql & ", @Seguradora_cod_susep = " & gbllSeguradora_Cod_Susep
    vSql = vSql & ", @Ramo_id = " & gbllRamo_ID
    vSql = vSql & ", @Nome = '" & SegNome & "'"
    vSql = vSql & ", @CPF = '" & SegCPF & "'"
    vSql = vSql & ", @Sexo = '" & SegSexo & "'"
    vSql = vSql & ", @Dt_Nascimento = '" & SegDtNasc & "'"
    vSql = vSql & ", @usuario = '" & cUserName & "'"
    
    'RCA 485 - ignora se status_sistema estiver bloqueado
    vSql = vSql & ", @ignorabloqueio = 1"

    Call Conexao_ExecutarSQL(gsSIGLASISTEMA, _
                             glAmbiente_id, _
                             App.Title, _
                             App.FileDescription, _
                             vSql, _
                             lConexaoLocal, False)
    ConfirmarTransacao (lConexaoLocal)

    If vExisteSinistro Then
        Itens_historico(0) = "NOME: " & SegNome
        If Not Inclui_Evento("10009", gblsSinistro_Situacao_ID, "NULL", Itens_historico) Then
            GoTo Trata_Erro
        End If
    End If

    Exit Function

Trata_Erro:
    TrataErroGeral "Inclus�o de Sinistrado"
    RetornarTransacao (lConexaoLocal)
    Inclui_Sinistrado = False
    Resume
End Function

Public Function Valida_Seguro() As Boolean
    Dim vSql As String
    Dim rs As ADODB.Recordset
    Dim SegNome As String, SegCPF As String
    Dim SegDtNasc As String, SegSexo As String


    Valida_Seguro = True

    '    If FmeEndereco_Ocorrencia.Visible Then
    '
    '        Valida_Seguro = False
    '        mensagem_erro 2, "Endere�o Sinistro"
    '
    '        Exit Function
    '    End If


    If bytTipoRamo = bytTipoRamo_RE Or bytTipoRamo = bytTipoRamo_Rural Then
        'valida cod objeto segurado
        If CmbCodigo_Objeto_Ocorrencia.Text = "" Then
            Valida_Seguro = False
            mensagem_erro 6, "O preenchimento do C�digo Objeto Segurado � obrigat�rio para Ramos Elementares"

            CmbCodigo_Objeto_Ocorrencia.SetFocus
            Exit Function
        End If

        ' valida endereco: txtEndereco
        TxtEndereco_Ocorrencia.Text = MudaAspaSimples(TxtEndereco_Ocorrencia.Text)

        If Trim(TxtEndereco_Ocorrencia.Text) = "" Then
            Valida_Seguro = False
            mensagem_erro 6, "O preenchimento do Endere�o de Ocorr�ncia � obrigat�rio para Ramos Elementares"

            TxtEndereco_Ocorrencia.SetFocus
            Exit Function
        End If

        ' ajusta bairro: txtBairro
        TxtBairro_Ocorrencia.Text = MudaAspaSimples(TxtBairro_Ocorrencia.Text)

        If vTranspInternacional = False Then
            ' valida municipio: txtMunicipio
            TxtMunicipio_Ocorrencia.Text = MudaAspaSimples(TxtMunicipio_Ocorrencia.Text)
            If Trim(TxtMunicipio_Ocorrencia.Text) = "" Then
                Valida_Seguro = False
                mensagem_erro 6, "O preenchimento do Munic�pio de Ocorr�ncia � obrigat�rio para Ramos Elementares"

                TxtMunicipio_Ocorrencia.SetFocus
                Exit Function
            End If

            ' valida estado: txtEstado
            TxtEstado_Ocorrencia.Text = Trim(TxtEstado_Ocorrencia.Text)
            TxtEstado_Ocorrencia.Text = MudaAspaSimples(TxtEstado_Ocorrencia.Text)

            If (Trim(TxtEstado_Ocorrencia.Text) = "") Then
                Valida_Seguro = False
                mensagem_erro 6, "O preenchimento do Estado de Ocorr�ncia � obrigat�rio para Ramos Elementares"

                TxtEstado_Ocorrencia.SetFocus
                Exit Function
            Else
                If InStr("AC AL AM AP BA CE DF ES GO MA MG MS MT PA PB PE PI PR RJ RN RO RR RS SE SP TO XX SC", TxtEstado_Ocorrencia.Text) = 0 Or _
                   (Len(TxtEstado_Ocorrencia.Text) < 2) Then
                    Valida_Seguro = False
                    mensagem_erro 3, "Estado de Ocorr�ncia"

                    TxtEstado_Ocorrencia.SetFocus
                    Exit Function
                End If
            End If
        End If


    End If

    If (bytTipoRamo = bytTipoRamo_Vida) And (sOperacaoCosseguro <> "C") Then

        If Trim(mskCPF_Sinistrado.ClipText) <> "" Then

            If Not CPF_Ok(mskCPF_Sinistrado.ClipText) Then
                Valida_Seguro = False
                mensagem_erro 3, "CPF"

                mskCPF_Sinistrado.SetFocus
                Exit Function
            End If

        Else

            Valida_Seguro = False
            mensagem_erro 3, "CPF"

            mskCPF_Sinistrado.SetFocus
            Exit Function
        End If

        If (Trim(txtSinistro_Vida_Nome.Text) = "") Or _
           (IsNumeric(txtSinistro_Vida_Nome.Text)) Then
            Valida_Seguro = False
            mensagem_erro 3, "Nome"

            txtSinistro_Vida_Nome.SetFocus
            Exit Function
        End If

        If Trim(mskDtNasc_Sinistrado.ClipText) <> "" Then
            If Not VerificaData2(mskDtNasc_Sinistrado.Text) Then
                Valida_Seguro = False
                mensagem_erro 3, "Data Nascimento"

                mskDtNasc_Sinistrado.SetFocus
                Exit Function
            End If
        Else
            Valida_Seguro = False
            mensagem_erro 3, "Data Nascimento"

            mskDtNasc_Sinistrado.SetFocus
            Exit Function
        End If

        If Trim(mskData_Ocorrencia.ClipText) <> "" Then
            If Not VerificaData2(mskData_Ocorrencia.Text) Then
                Valida_Seguro = False
                mensagem_erro 3, "Data Ocorr�ncia"
                mskData_Ocorrencia.SetFocus
                Exit Function
            End If
        Else
            Valida_Seguro = False
            mensagem_erro 3, "Data Ocorr�ncia"
            mskData_Ocorrencia.SetFocus
            Exit Function
        End If

        If Trim(mskData_Aviso.ClipText) <> "" Then
            If Not VerificaData2(mskData_Aviso.Text) Then
                Valida_Seguro = False
                mensagem_erro 3, "Data Aviso"
                mskData_Aviso.SetFocus
                Exit Function
            End If
        Else
            Valida_Seguro = False
            mensagem_erro 3, "Data Aviso"
            mskData_Aviso.SetFocus
            Exit Function
        End If

        If Trim(mskData_Entrada.ClipText) <> "" Then
            If Not VerificaData2(mskData_Entrada.Text) Then
                Valida_Seguro = False
                mensagem_erro 3, "Data Entrada"
                mskData_Entrada.SetFocus
                Exit Function
            End If
        Else
            Valida_Seguro = False
            mensagem_erro 3, "Data Entrada"
            mskData_Entrada.SetFocus
            Exit Function
        End If

        '        If txtSinistro_Vida_Sexo.Text = "" Then
        '            Valida_Seguro = False
        '            mensagem_erro 2, "Sexo"
        '
        '            txtSinistro_Vida_Sexo.SetFocus
        '            Exit Function
        '        End If

        If CmbVidaSexo.Text = "" Then
            Valida_Seguro = False
            mensagem_erro 2, "Sexo"
            CmbVidaSexo.SetFocus
            Exit Function
        End If

        SegNome = Trim(MudaAspaSimples(txtSinistro_Vida_Nome.Text))
        SegCPF = mskCPF_Sinistrado.ClipText
        SegSexo = IIf(CmbVidaSexo.Text = "Masculino", "M", "F")
        SegDtNasc = Format(mskDtNasc_Sinistrado.Text, "yyyymmdd")


        vExiste_Sinistro = False

        If bytModoOperacao = 1 Then    'Se for inclus�o, checa se j� existe um sinistro
            'pra esta pessoa.

            vSql = "SELECT sinistro_id, ramo_id, sucursal_seguradora_id,"
            vSql = vSql & " seguradora_cod_susep, apolice_id FROM Sinistro_Vida_tb WITH (NOLOCK) "
            vSql = vSql & " WHERE CPF = '" & SegCPF & "'"
            '            vSql = vSql & " WHERE CPF = '" & CPF_Sinistrado & "'"

            Set rs = Conexao_ExecutarSQL(gsSIGLASISTEMA, _
                                         glAmbiente_id, _
                                         App.Title, _
                                         App.FileDescription, _
                                         vSql, _
                                         lConexaoLocal, True)
            If Not rs.EOF Then
                If MsgBox("J� existe uma ocorr�ncia com este segurado." & vbNewLine & _
                        " Sinistro n� " & rs("sinistro_id") & vbNewLine & _
                        " Ramo " & rs("ramo_id") & vbNewLine & _
                        " Sucursal n� " & rs("sucursal_seguradora_id") & vbNewLine & _
                        " C�d. Seguradora " & rs("seguradora_cod_susep") & vbNewLine & _
                        " Ap�lice n� " & rs("apolice_id") & vbNewLine & vbNewLine & _
                          "Confirma novo cadastro ?", vbOKCancel, "Aten��o") = vbCancel Then

                    Valida_Seguro = False
                    Exit Function
                Else
                    vExiste_Sinistro = True
                End If
            End If
        End If
    End If
End Function

Public Function Inclui_Sinistro() As Boolean

    Dim rs As ADODB.Recordset, vSql As String
    Dim Rs1 As ADODB.Recordset, bSql As String
    Dim Itens_historico(0) As String, vMunicipio_id As String, vSolicitante As Long, vSeg_cod_susep_lider As Integer, vSeguradora As String
    Dim dMoeda As Double

    AbrirTransacao (lConexaoLocal)

    Inclui_Sinistro = True

    On Error GoTo Trata_Erro
    
    If cboMoeda.ListIndex <> -1 Then
        dMoeda = cboMoeda.ItemData(cboMoeda.ListIndex)
    Else
        dMoeda = txtMoeda.Tag
    End If
    
    If Busca_Numero_Sinistro Then
        If Not Manutencao_SinistroBB Then
            Inclui_Sinistro = False
            Exit Function
        End If

        If txtEndereco <> "" Then
            vSql = "SELECT municipio_id, municipio, estado FROM Endereco_risco_tb WITH (NOLOCK) " & _
                   "WHERE endereco_risco_tb.proposta_id = " & gbllProposta_ID & _
                 " AND endereco_risco_tb.end_risco_id = " & vEnd_Risco_id

            Set rs = Conexao_ExecutarSQL(gsSIGLASISTEMA, _
                                         glAmbiente_id, _
                                         App.Title, _
                                         App.FileDescription, _
                                         vSql, _
                                         lConexaoLocal, True)

            'edfaria - Confitec - 23/09/08 - demanda 548094
            If Not rs.EOF Then
                If IsNull(rs(0)) Then
                    bSql = "select municipio_id from municipio_tb WITH (NOLOCK) " & _
                           "where nome = '" & rs(1) & "'" & _
                         " And estado = '" & rs(2) & "'"

                    Set Rs1 = Conexao_ExecutarSQL(gsSIGLASISTEMA, _
                                                  glAmbiente_id, _
                                                  App.Title, _
                                                  App.FileDescription, _
                                                  bSql, _
                                                  lConexaoLocal, True)

                    If Not Rs1.EOF Then
                        vMunicipio_id = Rs1(0)
                        Rs1.Close

                    Else
                        vMunicipio_id = "999"
                    End If
                Else
                    vMunicipio_id = rs(0)
                End If
            Else
                vMunicipio_id = "999"
            End If
            rs.Close
        Else
            vMunicipio_id = "999"
        End If

        If sOperacaoCosseguro = "C" Then
            'RALVES SOLICITANTE COSSEGURO
            bSql = "SELECT ISNULL(MAX(solicitante_id),0) from solicitante_sinistro_tb WITH (NOLOCK) "

            Set Rs1 = Conexao_ExecutarSQL(gsSIGLASISTEMA, _
                                          glAmbiente_id, _
                                          App.Title, _
                                          App.FileDescription, _
                                          bSql, _
                                          lConexaoLocal, True)

            If Not Rs1.EOF Then
                vSolicitante = Rs1(0) + 1
            Else
                vSolicitante = 1
            End If

            Rs1.Close

            bSql = "SELECT Co_Seguro_Aceito_tb.seg_cod_susep_lider" & _
                 "  FROM seguros_db..Apolice_Terceiros_Tb Apolice_Terceiros_Tb WITH (NOLOCK) " & _
                 "  JOIN seguros_db..Co_Seguro_Aceito_tb Co_Seguro_Aceito_tb " & _
                 "    ON Co_seguro_Aceito_Tb.num_ordem_co_seguro_aceito = Apolice_Terceiros_Tb.num_ordem_co_seguro_aceito " & _
                 "   AND Co_seguro_Aceito_Tb.seg_cod_susep_lider        = Apolice_Terceiros_Tb.seg_cod_susep_lider " & _
                 "   AND Co_seguro_Aceito_Tb.sucursal_seg_lider         = Apolice_Terceiros_Tb.sucursal_seg_lider " & _
                 " WHERE Apolice_Terceiros_Tb.Apolice_ID             = " & gbllApolice_ID & _
                 "   AND Apolice_Terceiros_Tb.Ramo_ID                = " & gbllRamo_ID & _
                 "   AND Apolice_Terceiros_Tb.Sucursal_Seguradora_ID = " & gbllSucursal_Seguradora_ID & _
                 "   AND Apolice_Terceiros_Tb.Seguradora_Cod_Susep   = " & gbllSeguradora_Cod_Susep

            Set Rs1 = Conexao_ExecutarSQL(gsSIGLASISTEMA, _
                                          glAmbiente_id, _
                                          App.Title, _
                                          App.FileDescription, _
                                          bSql, _
                                          lConexaoLocal, True)

            If Not Rs1.EOF Then
                vSeg_cod_susep_lider = Rs1(0)
            End If

            Rs1.Close

            vSql = "SELECT nome FROM seguradora_tb WITH (NOLOCK) WHERE seguradora_cod_susep = " & vSeg_cod_susep_lider

            Set rs = Conexao_ExecutarSQL(gsSIGLASISTEMA, _
                                         glAmbiente_id, _
                                         App.Title, _
                                         App.FileDescription, _
                                         vSql, _
                                         lConexaoLocal, True)
            If Not rs.EOF Then
                vSeguradora = rs(0)
            End If

            vSql = "EXEC solicitante_sinistro_spi " & _
                   vSolicitante & "," & _
                   "'" & vSeguradora & "'" & "," & _
                   "NULL" & "," & _
                   "NULL" & "," & _
                   999 & "," & "NULL" & ", " & _
                   "'XX'" & "," & _
                   "NULL" & "," & _
                   "NULL" & "," & _
                   "NULL" & "," & _
                   "NULL" & ", " & "'" & cUserName & "'"

            Call Conexao_ExecutarSQL(gsSIGLASISTEMA, _
                                     glAmbiente_id, _
                                     App.Title, _
                                     App.FileDescription, _
                                     vSql, _
                                     lConexaoLocal, False)
        End If

        'Victor - 02/10/2013 - 17905311 - IN�CIO
'        vsql = "EXEC sinistro_spi " & _
'               gbldSinistro_ID & "," & _
'               gbllApolice_ID & "," & _
'               gbllSucursal_Seguradora_ID & "," & _
'               gbllSeguradora_Cod_Susep & "," & _
'               gbllRamo_ID & ","
               
        If qtdeRuralEstudo = 0 Then
            vSql = "EXEC sinistro_spi " & _
                    " @sinistro_id = " & gbldSinistro_ID & "," & _
                    " @apolice_id = " & gbllApolice_ID & "," & _
                    " @sucursal_seguradora_id = " & gbllSucursal_Seguradora_ID & "," & _
                    " @seguradora_cod_susep = " & gbllSeguradora_Cod_Susep & "," & _
                    " @ramo_id = " & gbllRamo_ID & ","
        Else
            vSql = "EXEC sinistro_spi " & _
                    " @sinistro_id = " & gbldSinistro_ID & "," & _
                    " @apolice_id = Null" & "," & _
                    " @sucursal_seguradora_id = " & gbllSucursal_Seguradora_ID & "," & _
                    " @seguradora_cod_susep = " & gbllSeguradora_Cod_Susep & "," & _
                    " @ramo_id = " & gbllRamo_ID & ","
        End If
        'Victor - 02/10/2013 - 17905311 - FIM
               
        If sOperacaoCosseguro = "C" Then
            'vsql = vsql & IIf(IsNumeric(vSolicitante), Null, vSolicitante) & "," 'cristovao.rodrigues 06/11/2012 linha substituida
            vSql = vSql & " @solicitante_id = " & IIf(IsNumeric(vSolicitante), vSolicitante, "null") & ","
        Else
            
            'vsql = vsql & IIf(sSolicitante_id = "", "' '", sSolicitante_id) & ","
            vSql = vSql & " @solicitante_id = " & IIf(Replace(Trim(sSolicitante_id), "0", "") = "", "NUll", sSolicitante_id) & ","
        End If
        vSql = vSql & " @proposta_id = " & gbllProposta_ID & "," & _
               " @evento_sinistro_id = " & cboCausa_Sinistro.ItemData(cboCausa_Sinistro.ListIndex) & _
               ", @dt_aviso_sinistro = '" & Format(mskData_Aviso.Text, "yyyymmdd") & "'" & _
               ", @dt_ocorrencia_sinistro = '" & Format(mskData_Ocorrencia.Text, "yyyymmdd") & "'" & _
               ", @dt_entrada_seguradora = '" & Format(mskData_Entrada.Text, "yyyymmdd") & "'" & _
               "," & _
               " @endereco = " & IIf(TxtEndereco_Ocorrencia <> "", "'" & TxtEndereco_Ocorrencia & "'", "NULL") & "," & _
               " @bairro = " & IIf(TxtBairro_Ocorrencia <> "", "'" & TxtBairro_Ocorrencia & "'", "NULL") & "," & _
               " @municipio_id = " & IIf(vMunicipio_id <> "", vMunicipio_id, 999) & ", " & _
               " @municipio = " & IIf(TxtMunicipio_Ocorrencia <> "", "'" & TxtMunicipio_Ocorrencia & "'", "NULL") & _
               ", @estado = '" & IIf(TxtEstado_Ocorrencia <> "", TxtEstado_Ocorrencia, "XX") & "'" & ", " & _
               " @agencia_id = " & IIf(mskAgencia_Aviso <> "", mskAgencia_Aviso, "NULL") & "," & _
               " @banco_id = 1, " & _
               " @cliente_id = " & gbllProp_Cliente_ID & "," & _
               " @situacao = '0'," & _
               " @usuario = '" & cUserName & "', " & _
               " @sinistro_id_lider = " & IIf(Trim(TxtNumero_Sinistro_Lider.Text) = "", "NULL", Trim(TxtNumero_Sinistro_Lider.Text)) & ", " & _
               " @endosso_id = " & IIf(Val(txtEndosso.Text) = 0, "NULL", txtEndosso.Text) & ", " & _
               " @moeda_id = " & dMoeda & "," & _
               " @processa_reintegracao_is = '" & IIf(chkReintegracaoIS.Value = 1, "s", "n") & "',"

        If cboSubevento_Sinistro.ListIndex = -1 Then
            vSql = vSql & " @subevento_sinistro_id = null"
        Else
            vSql = vSql & " @subevento_sinistro_id = " & cboSubevento_Sinistro.ItemData(cboSubevento_Sinistro.ListIndex)
        End If
        vSql = vSql & ", @tp_ramo_id = NULL"
        
        'RCA 485 - ignora se status_sistema estiver bloqueado
        vSql = vSql & ", @ignorabloqueio = 1"


        Call Conexao_ExecutarSQL(gsSIGLASISTEMA, _
                                 glAmbiente_id, _
                                 App.Title, _
                                 App.FileDescription, _
                                 vSql, _
                                 lConexaoLocal, False)


        ' Poderia ser uma �nica SP, faz antes os processos eram separados
'        vSql = "EXEC sinistro_marca_pgto_parc_spu " & _                                                    'FLAVIO.BEZERRA - 18/01/2013
'               gbldSinistro_ID & "," & gbllApolice_ID & "," & _
'               gbllSucursal_Seguradora_ID & "," & gbllSeguradora_Cod_Susep & "," & gbllRamo_ID & ",'" & _
'               cUserName & "', '" & IIf(chkReintegracaoIS.Value = 1, "s", "n") & "'"
        
        'FLAVIO.BEZERRA - IN�CIO - 18/01/2013
        vSql = "EXEC sinistro_marca_pgto_parc_spu "
        vSql = vSql & "  @sinistro_id = " & gbldSinistro_ID
        vSql = vSql & ", @apolice_id = " & gbllApolice_ID
        vSql = vSql & ", @sucursal_seguradora_id = " & gbllSucursal_Seguradora_ID
        vSql = vSql & ", @seguradora_cod_susep = " & gbllSeguradora_Cod_Susep
        vSql = vSql & ", @ramo_id = " & gbllRamo_ID
        vSql = vSql & ", @usuario = '" & cUserName & "'"
        vSql = vSql & ", @pgto_parcial = '" & IIf(chkPgto_Parcial_Co_Seguro.Value = 1, "s", "n") & "'"
               
        'RCA 485 - ignora se status_sistema estiver bloqueado
        vSql = vSql & ", @ignorabloqueio = 1"
        
        'FLAVIO.BEZERRA - FIM - 18/01/2013
        
        Call Conexao_ExecutarSQL(gsSIGLASISTEMA, _
                                 glAmbiente_id, _
                                 App.Title, _
                                 App.FileDescription, _
                                 vSql, _
                                 lConexaoLocal, False)




        'GENJUNIOR - FLOW 18225203 - AJUSTE DO M�DULO DE EXIG�NCIA
        'GERANDO REGISTRO DA EXIG�NCIA B�SICA PARA SINISTROS CI
        vSql = ""
        vSql = vSql & " EXEC interface_db.dbo.SMQS00193_SPI " & gbldSinistro_ID & " "

        Call Conexao_ExecutarSQL(gsSIGLASISTEMA, _
                                 glAmbiente_id, _
                                 App.Title, _
                                 App.FileDescription, _
                                 vSql, _
                                 lConexaoLocal, False)
        'FIM GENJUNIOR

        Itens_historico(0) = "N� DO SINISTRO: " & gbldSinistro_ID
        If Not Inclui_Evento("10002", "0", "NULL", Itens_historico) Then
            Inclui_Sinistro = False
            MsgBox "Erro Inclus�o Evento Registro do Aviso", vbOKOnly + vbCritical, App.EXEName
            RetornarTransacao (lConexaoLocal)
            Exit Function

        End If



        If bytTipoRamo <> bytTipoRamo_Vida Then
            If Not Inclui_Sinistro_RE Then
                Inclui_Sinistro = False
                Exit Function
            End If
        End If
        ConfirmarTransacao (lConexaoLocal)
    Else
        RetornarTransacao (lConexaoLocal)
    End If

    Exit Function

Trata_Erro:
    TrataErroGeral "Inclus�o Sinistro"
    Inclui_Sinistro = False
Resume
End Function

Public Function Busca_Numero_Sinistro() As Boolean

    Dim rs As ADODB.Recordset
    Dim vSql As String


    Busca_Numero_Sinistro = True
    On Error GoTo Trata_Erro

    '******************************************************************************************'
    'Anderson Fiuza GPTI FLOW:2707850 29/01/2010
    'Substitui�ao da procedure chave_sinistro_spu pelo smqs0006_spu chamada pela procedure abaixo,
    'pois na hora de retornar o numero do sinistro_id est� apresentando erro de overflow,
    'ocasionando o "pulo" de sequencial de sinistro.
    '******************************************************************************************'


    vSql = "SET NOCOUNT ON EXEC SEGS8151_SPS '" & Format(Trim(Str(gbllRamo_ID)), "00") & "',"
    vSql = vSql & gbllSucursal_Seguradora_ID & "," & gbllSeguradora_Cod_Susep & ",'" & cUserName & "'"


    '    vSql = " declare @sinistro_id as numeric"
    '    vSql = vSql = "EXEC seguros_db..smqs0006_spu '" & Format(Trim(Str(gbllRamo_ID)), "00") & "'," & _
         '    gbllSucursal_Seguradora_ID & "," & gbllSeguradora_Cod_Susep & ",'" & cUserName & "', @sinistro_id output  "

    Set rs = Conexao_ExecutarSQL(gsSIGLASISTEMA, _
                                 glAmbiente_id, _
                                 App.Title, _
                                 App.FileDescription, _
                                 vSql, _
                                 lConexaoLocal, _
                                 True, _
                                 False)


    If Not rs.EOF Then
        gbldSinistro_ID = CDbl(rs(0))
    End If
    rs.Close

    Exit Function

Trata_Erro:
    TrataErroGeral "Busca N�mero Sinistro"
    Busca_Numero_Sinistro = False
End Function

Public Function Inclui_Sinistro_RE() As Boolean

    Dim vSql As String


    Inclui_Sinistro_RE = True
    On Error GoTo Trata_Erro



    'Inclui Sinistro Ramos Elementares
    vSql = "EXEC sinistro_re_spi "
    vSql = vSql & "  @sinistro_id = " & gbldSinistro_ID
    vSql = vSql & ", @apolice_id = " & gbllApolice_ID
    vSql = vSql & ", @sucursal_seguradora_id = " & gbllSucursal_Seguradora_ID
    vSql = vSql & ", @seguradora_cod_susep = " & gbllSeguradora_Cod_Susep
    vSql = vSql & ", @ramo_id = " & gbllRamo_ID
    vSql = vSql & ", @cod_objeto_segurado = " & vEnd_Risco_id
    vSql = vSql & ", @dt_inicio_vigencia_seg = '" & Format(vDt_Inicio_Vigencia_Seg, "yyyymmdd") & "'"
    vSql = vSql & ", @usuario = '" & cUserName & "'"

    If vTranspInternacional = True Then
        vSql = vSql & ", @num_averbacao = " & IIf((mskAverbacao.Visible = True And mskAverbacao.Text = "_______") Or mskAverbacao.Visible = False, "NULL", mskAverbacao.ClipText)
    End If
    
    'RCA 485 - ignora se status_sistema estiver bloqueado
    vSql = vSql & ", @ignorabloqueio = 1"

    Call Conexao_ExecutarSQL(gsSIGLASISTEMA, _
                             glAmbiente_id, _
                             App.Title, _
                             App.FileDescription, _
                             vSql, _
                             lConexaoLocal, False)


    Exit Function

Trata_Erro:
    TrataErroGeral "Inclus�o de Sinistro Ramos Elementares"
    Inclui_Sinistro_RE = False

End Function

Public Function Produto_Permite_Reintegracao_IS(produto_id As String, ramo_id As String) As Boolean

    Dim SQL As String, rs As ADODB.Recordset

    SQL = "SELECT processa_reducao_IS"
    SQL = SQL & " FROM  ITEM_PRODUTO_TB WITH (NOLOCK) "
    SQL = SQL & " WHERE Produto_id  = " & produto_id
    SQL = SQL & " AND   Ramo_id     = " & ramo_id

    Set rs = Conexao_ExecutarSQL(gsSIGLASISTEMA, _
                                 glAmbiente_id, _
                                 App.Title, _
                                 App.FileDescription, _
                                 SQL, _
                                 lConexaoLocal, True)

    If Not rs.EOF Then
        If IsNull(rs(0)) Or rs(0) = "n" Then
            Produto_Permite_Reintegracao_IS = False
        Else
            Produto_Permite_Reintegracao_IS = True
        End If
    End If
    rs.Close

End Function

Private Function Valida_Agencia() As Boolean

    Dim vSql As String, rs As ADODB.Recordset

    Valida_Agencia = True

    If bytModoOperacao = 3 Then
        If Trim(mskAgencia_Aviso.ClipText) = "" And GrdInformacao_SinistroBB.Rows > 1 Then
            mensagem_erro 3, "Ag�ncia Aviso"
            If mskAgencia_Aviso.Enabled Then
                mskAgencia_Aviso.SetFocus
            End If
            Valida_Agencia = False
            Exit Function
        End If

        If Trim(mskAgencia_Aviso.ClipText) <> "" Then
            vSql = "SELECT count(agencia_id) FROM Agencia_tb WITH (NOLOCK) " & _
                   "WHERE agencia_id = " & mskAgencia_Aviso & _
                 "  AND banco_id = 1"

            Set rs = Conexao_ExecutarSQL(gsSIGLASISTEMA, _
                                         glAmbiente_id, _
                                         App.Title, _
                                         App.FileDescription, _
                                         vSql, _
                                         lConexaoLocal, True)

            If rs(0) = 0 Then
                mensagem_erro 6, "Ag�ncia n�o cadastrada!"
                mskAgencia_Aviso.SetFocus
                rs.Close
                Valida_Agencia = False
                Exit Function
            End If
            rs.Close
        End If
    Else

        If Trim(mskAgencia_Aviso.ClipText) = "" And GrdInformacao_SinistroBB.Rows > 1 Then
            mensagem_erro 3, "Ag�ncia Aviso"
            If mskAgencia_Aviso.Enabled Then
                mskAgencia_Aviso.SetFocus
            End If
            Valida_Agencia = False
            Exit Function
        End If

        If Trim(mskAgencia_Aviso.ClipText) <> "" Then
            vSql = "SELECT count(agencia_id) FROM Agencia_tb WITH (NOLOCK) "
            vSql = vSql & " WHERE agencia_id = " & mskAgencia_Aviso
            vSql = vSql & "   AND banco_id = 1"

            Set rs = Conexao_ExecutarSQL(gsSIGLASISTEMA, _
                                         glAmbiente_id, _
                                         App.Title, _
                                         App.FileDescription, _
                                         vSql, _
                                         lConexaoLocal, True)

            If rs(0) = 0 Then
                mensagem_erro 6, "Ag�ncia n�o cadastrada!"
                mskAgencia_Aviso.SetFocus

                rs.Close
                Valida_Agencia = False
                Exit Function
            End If
            rs.Close
        End If
    End If

End Function

Private Function Nro_BB(sinistro_id)
    Dim rSQL As String
    Dim rsRecordSet As ADODB.Recordset

    rSQL = ""
    rSQL = rSQL & "Select sinistro_bb "
    rSQL = rSQL & " From sinistro_bb_tb WITH (NOLOCK) "
    rSQL = rSQL & " Where Sinistro_id = " & sinistro_id
    rSQL = rSQL & " And dt_fim_vigencia Is Null"

    Set rsRecordSet = Conexao_ExecutarSQL(gsSIGLASISTEMA, _
                                          glAmbiente_id, _
                                          App.Title, _
                                          App.FileDescription, _
                                          rSQL, _
                                          lConexaoLocal, True)

    If Not rsRecordSet.EOF Then
        Nro_BB = rsRecordSet(0)
    Else
        Nro_BB = 0
    End If

End Function

Public Function PodeDesvincularAvisoBB(dSinistroBB As Double) As Boolean

    Dim rs As ADODB.Recordset
    Dim sSql As String

    sSql = "select count(*) from pgto_sinistro_tb WITH (NOLOCK) where sinistro_bb = " & _
           dSinistroBB & " and situacao_op = 'a'"

    Set rs = Conexao_ExecutarSQL(gsSIGLASISTEMA, _
                                 glAmbiente_id, _
                                 App.Title, _
                                 App.FileDescription, _
                                 sSql, _
                                 lConexaoLocal, True)

    PodeDesvincularAvisoBB = (rs(0) = 0)

    Set rs = Nothing

End Function

Private Sub GrdInformacao_SinistroBB_Click()

    vSelSinistroBB = True
    txtNumero_Sinistro_InformacaoBB.Text = GrdInformacao_SinistroBB.TextMatrix(GrdInformacao_SinistroBB.RowSel, 0)

    If Not IsNumeric(txtNumero_Sinistro_InformacaoBB.Text) Then

        MsgBox "Sinistro BB Inv�lido ou N�o Informado", vbOKOnly, App.EXEName
        txtNumero_Sinistro_InformacaoBB.Text = ""
        vSelSinistroBB = False
        Exit Sub
    Else
        btnBBRemover.Visible = True
    End If

    If bytModoOperacao <> 1 Then
        btnBBInativar.Visible = True
    End If

End Sub

Public Sub Monta_GrdInformacao_SinistroBB()

    If bytModoOperacao <> 1 Then

        GrdInformacao_SinistroBB.AddItem txtNumero_Sinistro_InformacaoBB.Text & Chr(9) & "Ativo" & Chr(9) & Obtem_Localizacao_Sinistro_BB(txtNumero_Sinistro_InformacaoBB.Text)

        GrdInformacao_SinistroBB.Col = 0
        GrdInformacao_SinistroBB.ColSel = 0
        GrdInformacao_SinistroBB.Sort = 0
        GrdInformacao_SinistroBB.Refresh

    Else

        GrdInformacao_SinistroBB.Rows = 1

        If GrdInformacao_SinistroBB.Rows > 0 Then

            GrdInformacao_SinistroBB.Row = GrdInformacao_SinistroBB.Rows - 1
            GrdInformacao_SinistroBB.AddItem txtNumero_Sinistro_InformacaoBB & Chr(9) & " Ativo" & Chr(9) & Obtem_Localizacao_Sinistro_BB(txtNumero_Sinistro_InformacaoBB.Text)

        End If

        GrdInformacao_SinistroBB.Col = 0
        GrdInformacao_SinistroBB.ColSel = 0
        GrdInformacao_SinistroBB.Sort = 0
        GrdInformacao_SinistroBB.Refresh

    End If

End Sub
'FLAVIO.BEZERRA - 06/02/2013 - MOVIDO PARA O modSEGP1285, ASSIM PODENDO SER USADO  NA ABA DE DADOS GERAIS.
'Public Function Obtem_Localizacao_Sinistro_BB(ByVal OSinistro_BB As String) As String
'
'    Dim SQL As String
'    Dim rs As ADODB.Recordset
'    Dim Localizacao_Aux As Byte
'
'    On Error GoTo TrataErro
'
'    SQL = "SELECT evento_BB, situacao, e.dt_inclusao, ev.prox_localizacao_processo"
'    SQL = SQL & " FROM INTERFACE_DB..entrada_GTR_tb e  WITH (NOLOCK) "
'    SQL = SQL & "   INNER JOIN evento_SEGBR_tb ev WITH (NOLOCK) "
'    SQL = SQL & "       ON ev.evento_BB_id = e.evento_BB"
'    SQL = SQL & " WHERE Sinistro_BB = " & OSinistro_BB
'    SQL = SQL & "   AND Situacao    = 'T'"
'    SQL = SQL & " UNION"
'    SQL = SQL & " SELECT evento_BB, situacao, s.dt_inclusao, ev.prox_localizacao_processo"
'    SQL = SQL & " FROM  INTERFACE_DB..saida_GTR_tb s WITH (NOLOCK) "
'    SQL = SQL & "   INNER JOIN evento_SEGBR_tb ev WITH (NOLOCK) "
'    SQL = SQL & "       ON ev.evento_BB_id = s.evento_BB"
'    SQL = SQL & " WHERE Sinistro_BB = " & OSinistro_BB
'    SQL = SQL & "   AND Situacao    = 'T'"
'    SQL = SQL & " ORDER BY e.dt_inclusao"
'
'    Set rs = Conexao_ExecutarSQL(gsSIGLASISTEMA, _
'                                 glAmbiente_id, _
'                                 App.Title, _
'                                 App.FileDescription, _
'                                 SQL, _
'                                 lConexaoLocal, True)
'
'    If rs.EOF Then
'        rs.Close
'        Obtem_Localizacao_Sinistro_BB = "SEM GTR"
'        Exit Function
'    End If
'
'    While Not rs.EOF
'        If Not IsNull(rs!Prox_Localizacao_Processo) Then
'            Localizacao_Aux = rs!Prox_Localizacao_Processo
'        End If
'        rs.MoveNext
'    Wend
'    rs.Close
'
'    Select Case Localizacao_Aux
'    Case 0: Obtem_Localizacao_Sinistro_BB = "SEM GTR"
'    Case 1: Obtem_Localizacao_Sinistro_BB = "AG�NCIA"
'    Case 2: Obtem_Localizacao_Sinistro_BB = "SEGURADORA"
'    Case 3: Obtem_Localizacao_Sinistro_BB = "ENCERRADO"
'    End Select
'
'    Exit Function    '
'
'TrataErro:
'    TrataErroGeral "OBTEM_LOCALIZACAO_SINISTRO_BB"
'End Function

Public Function VerificaOutroAtivo(ByVal OSinistro_BB As String) As Boolean


    Dim SQL As String
    Dim rs As ADODB.Recordset
    Dim rsAux As ADODB.Recordset
    Dim cont_ativos As Integer



    On Error GoTo TrataErro


    SQL = "SELECT distinct 'ativo' = count(*)"
    SQL = SQL & " FROM sinistro_BB_tb WITH (NOLOCK) "
    SQL = SQL & " WHERE sinistro_id             = " & gbldSinistro_ID
    SQL = SQL & "   AND DT_FIM_VIGENCIA IS NULL"
    SQL = SQL & "   AND sinistro_BB             <> " & OSinistro_BB

    Set rs = Conexao_ExecutarSQL(gsSIGLASISTEMA, _
                                 glAmbiente_id, _
                                 App.Title, _
                                 App.FileDescription, _
                                 SQL, _
                                 lConexaoLocal, True)

    If rs.EOF Then
        rs.Close
        VerificaOutroAtivo = False
        Exit Function
    Else
        If rs!ativo > 0 Then

            cont_ativos = 0
            SQL = "SELECT DT_FIM_VIGENCIA"
            SQL = SQL & " FROM sinistro_BB_tb WITH (NOLOCK) "
            SQL = SQL & " WHERE sinistro_id             = " & gbldSinistro_ID

            Set rsAux = Conexao_ExecutarSQL(gsSIGLASISTEMA, _
                                            glAmbiente_id, _
                                            App.Title, _
                                            App.FileDescription, _
                                            SQL, _
                                            lConexaoLocal, True)

            While Not rsAux.EOF



                If IsNull(rsAux("dt_fim_vigencia")) Then
                    cont_ativos = cont_ativos + 1
                End If

                rsAux.MoveNext
            Wend

            rsAux.Close

            If cont_ativos > 1 Then
                VerificaOutroAtivo = True
            Else
                VerificaOutroAtivo = False
            End If

        Else
            VerificaOutroAtivo = False
        End If

    End If
    rs.Close

    Exit Function

TrataErro:
    TrataErroGeral "VerificaInativo"

End Function

Public Function Sinistro_GTR(ByVal OSinistro_BB) As Boolean

    Dim rs As ADODB.Recordset
    Dim vSql As String

    On Error GoTo Trata_Erro

    vSql = "SELECT COUNT(*) "
    vSql = vSql & " FROM INTERFACE_DB..entrada_GTR_tb WITH (NOLOCK) "
    vSql = vSql & " WHERE sinistro_BB = " & OSinistro_BB
    vSql = vSql & "   AND evento_BB   = 2000"

    Set rs = Conexao_ExecutarSQL(gsSIGLASISTEMA, _
                                 glAmbiente_id, _
                                 App.Title, _
                                 App.FileDescription, _
                                 vSql, _
                                 lConexaoLocal, True)

    Sinistro_GTR = (rs(0) > 0)

    rs.Close

    Exit Function
    Resume
Trata_Erro:
    TrataErroGeral "Sinistro_GTR"
    Sinistro_GTR = False

End Function

Public Function Existe_Pgtos(ByVal OSinistro_BB As String) As Boolean


    Dim SQL As String
    Dim rs As ADODB.Recordset
    Dim Dt_inicio_vigencia As Date
    Dim Dt_Ultimo_Pgto As Date

    On Error GoTo TrataErro


    SQL = "SELECT dt_inicio_vigencia = ISNULL(dt_inicio_vigencia, dt_inclusao)"
    SQL = SQL & " FROM sinistro_BB_tb WITH (NOLOCK) "
    SQL = SQL & " WHERE sinistro_id             = " & gbldSinistro_ID
    SQL = SQL & "   AND sinistro_BB             = " & OSinistro_BB

    Set rs = Conexao_ExecutarSQL(gsSIGLASISTEMA, _
                                 glAmbiente_id, _
                                 App.Title, _
                                 App.FileDescription, _
                                 SQL, _
                                 lConexaoLocal, True)

    If rs.EOF Then
        rs.Close
        Exit Function
    Else
        Dt_inicio_vigencia = Format(rs("dt_inicio_vigencia"), "dd/mm/yyyy")
    End If

    rs.Close

    SQL = "SELECT dt_ultimo_pgto = ISNULL(max(dt_solicitacao_pgto), '19000101')"
    SQL = SQL & " FROM pgto_sinistro_tb WITH (NOLOCK) "
    SQL = SQL & " WHERE sinistro_id              = " & gbldSinistro_ID
    SQL = SQL & "   AND item_val_estimativa      = 1"               ' somente indeniza��o
    SQL = SQL & "   AND situacao_op              = 'a'"             ' pagamentos aprovados
    SQL = SQL & "   AND acerto_id                IS NOT NULL "      ' j� viraram voucher
    SQL = SQL & "   AND num_parcela = 1"                            ' n�o � rean�lise de resseguro

    Set rs = Conexao_ExecutarSQL(gsSIGLASISTEMA, _
                                 glAmbiente_id, _
                                 App.Title, _
                                 App.FileDescription, _
                                 SQL, _
                                 lConexaoLocal, True)


    Dt_Ultimo_Pgto = Format(rs("dt_ultimo_pgto"), "dd/mm/yyyy")
    rs.Close

    Existe_Pgtos = (Dt_Ultimo_Pgto >= Dt_inicio_vigencia)

    Exit Function

TrataErro:
    TrataErroGeral "EXISTE_PGTOS"

End Function

Public Function Existe_Indeferimento(ByVal OSinistro_BB As String) As Boolean


    Dim SQL As String
    Dim rs As ADODB.Recordset

    On Error GoTo TrataErro

    Existe_Indeferimento = False

    SQL = "SELECT evento_BB"
    SQL = SQL & " FROM INTERFACE_DB..saida_GTR_tb S WITH (NOLOCK) "
    SQL = SQL & "      INNER JOIN INTERFACE_DB..saida_GTR_registro_tb SR WITH (NOLOCK) "
    SQL = SQL & "         ON SR.saida_GTR_id = S.saida_GTR_id"
    SQL = SQL & " WHERE S.sinistro_BB = " & OSinistro_BB
    SQL = SQL & "   AND SR.reg_cd_tip = 'c'"
    '---------------------------------------------------
    'Ricardo Toledo : 05/07/2016 : INC000004985619 : INI
    'Est� gerando erro quando o caracter n�o � num�rico
    'SQL = SQL & "   AND convert(int, SUBSTRING(SR.registro, 26, 2)) = 0"
    SQL = SQL & "AND (case when (isnumeric(SUBSTRING(SR.registro, 26, 2)) = 1) then convert(int, SUBSTRING(SR.registro, 26, 2)) else 1 end) = 0"
    'Ricardo Toledo : 05/07/2016 : INC000004985619 : FIM
    '---------------------------------------------------
    SQL = SQL & " ORDER BY S.saida_GTR_id DESC"

    Set rs = Conexao_ExecutarSQL(gsSIGLASISTEMA, _
                                 glAmbiente_id, _
                                 App.Title, _
                                 App.FileDescription, _
                                 SQL, _
                                 lConexaoLocal, True)

    If Not rs.EOF Then
        Existe_Indeferimento = (rs("evento_BB") = 1140)
    End If
    rs.Close

    Exit Function
    Resume
TrataErro:
    TrataErroGeral "EXISTE_INDEFERIMENTO"

End Function
Private Function Gera_Cancelamento_Sinistro_BB(ByVal OSinistro_BB As String) As Boolean

    Dim rs As ADODB.Recordset
    Dim vSql As String, Linha_id As Integer
    Dim Linha As String
    Dim Pos_final As Integer, Pos_inicial As Integer
    Dim Itens_historico(4) As String, Evento_SEGBR_id As String
    Dim vDetalhamento_Aux As String
    Dim Indice As Byte
    Dim Detalhamento_id As String

    On Error GoTo Trata_Erro

    Gera_Cancelamento_Sinistro_BB = True


    If Verifica_Homologacao(OSinistro_BB) Then


        vSql = "Select evento_bb_id from evento_segbr_sinistro_tb WITH (NOLOCK) where sinistro_bb=" & OSinistro_BB & " and (evento_bb_id in (1160,1140,1180,1190,1120)) and evento_id = (select max(evento_id) from evento_segbr_sinistro_tb WITH (NOLOCK) where sinistro_bb = " & OSinistro_BB & " and (evento_bb_id in (1160,1140,1190,1180,1120)))"
        Set rs = Conexao_ExecutarSQL(gsSIGLASISTEMA, _
                                     glAmbiente_id, _
                                     App.Title, _
                                     App.FileDescription, _
                                     vSql, _
                                     lConexaoLocal, True)

        If Not rs.EOF Then    'Verifica se j� teve algum evento cadastrado

            If rs(0) = 1180 Or rs(0) = 1190 Then    'Verifica se o �ltimo evento foi de reativa��o

                'Procurar um evento de encerramento anterior ao �ltimo evento
                vSql = "Select TOP 1 (Evento_SEGBR_id) from evento_segbr_sinistro_tb WITH (NOLOCK) where sinistro_bb = " & OSinistro_BB & " and evento_bb_id in (1160,1140)"
                Set rs = rdocn.OpenResultset(vSql)

                Evento_SEGBR_id = rs("Evento_SEGBR_id")

            Else    'O �ltimo evento � o 1120 ou 1140 ou 1160

                Exit Function    'Sair da fun��o, pois n�o dever� inserir nenhum evento.

            End If

        Else    'nunca teve evento cadastrado - inserir o c�digo 1120 de cancelamento

            '* Solicita��o de cancelamento de aviso de sinistro
            Evento_SEGBR_id = "10102"    '(Evento BB 1120 - Cancelamento)

        End If

    Else

        Evento_SEGBR_id = "10135"    '(Evento BB 1121 - Solicita��o do cancelamento do aviso de sinistro)

    End If

    vTp_detalhamento = 4
    Indice = 1

    ' Testa para ver se existe algo preenchido no detalhe
    vDetalhamento = "CANCELAMENTO POR DUPLICIDADE DE REGISTRO. SINISTROS ATIVOS: " & Chr(10)
    vSql = "SELECT sinistro_BB"
    vSql = vSql & " FROM sinistro_BB_tb WITH (NOLOCK) "
    vSql = vSql & " WHERE sinistro_id            = " & gbldSinistro_ID
    vSql = vSql & "   AND sinistro_BB            <> " & OSinistro_BB
    vSql = vSql & "   AND dt_fim_vigencia        IS NULL"

    Set rs = Conexao_ExecutarSQL(gsSIGLASISTEMA, _
                                 glAmbiente_id, _
                                 App.Title, _
                                 App.FileDescription, _
                                 vSql, _
                                 lConexaoLocal, True)
    While Not rs.EOF
        If Len(vDetalhamento_Aux) + 13 >= Indice * 70 Then
            vDetalhamento_Aux = vDetalhamento_Aux & Chr(10)
            Indice = Indice + 1
        End If
        vDetalhamento_Aux = vDetalhamento_Aux & rs!Sinistro_BB & ", "
        rs.MoveNext
    Wend
    rs.Close

    '* Monta o detalhamento
    If vDetalhamento_Aux <> "" Then
        vDetalhamento = vDetalhamento & Left(vDetalhamento_Aux, Len(vDetalhamento_Aux) - 2)
    End If


    vSql = "EXEC sinistro_detalhamento_spi "
    vSql = vSql & gbldSinistro_ID
    vSql = vSql & ", " & gbllApolice_ID
    vSql = vSql & ", " & gbllSucursal_Seguradora_ID
    vSql = vSql & ", " & gbllSeguradora_Cod_Susep
    vSql = vSql & ", " & gbllRamo_ID
    vSql = vSql & ", '" & Format(Data_Sistema, "yyyymmdd") & "'"
    vSql = vSql & ", " & vTp_detalhamento
    vSql = vSql & ", 'S'"
    vSql = vSql & ", '" & cUserName & "'"

    Set rs = Conexao_ExecutarSQL(gsSIGLASISTEMA, _
                                 glAmbiente_id, _
                                 App.Title, _
                                 App.FileDescription, _
                                 vSql, _
                                 lConexaoLocal, True)
    Detalhamento_id = rs(0)
    rs.Close

    If Right(vDetalhamento, 1) <> Chr(10) Then
        vDetalhamento = vDetalhamento & Chr(10)
    End If

    ' Trata a primeira linha
    Pos_final = InStr(Pos_inicial + 1, vDetalhamento, Chr(10))
    Pos_inicial = 0
    Linha_id = 1

    While Pos_final <> 0
        Linha = Mid(vDetalhamento, Pos_inicial + 1, Pos_final - Pos_inicial - 1)
        Pos_inicial = Pos_final
        Pos_final = InStr(Pos_inicial + 1, vDetalhamento, Chr(10))

        ' GRAVA A LINHA
        vSql = "EXEC sinistro_linha_detalhe_spi "
        vSql = vSql & gbldSinistro_ID
        vSql = vSql & ", " & gbllApolice_ID
        vSql = vSql & ", " & gbllSucursal_Seguradora_ID
        vSql = vSql & ", " & gbllSeguradora_Cod_Susep
        vSql = vSql & ", " & gbllRamo_ID
        vSql = vSql & ", " & Detalhamento_id
        vSql = vSql & ", " & Linha_id
        vSql = vSql & ", '" & MudaAspaSimples(Linha) & "'"
        vSql = vSql & ", '" & cUserName & "'"

        Call Conexao_ExecutarSQL(gsSIGLASISTEMA, _
                                 glAmbiente_id, _
                                 App.Title, _
                                 App.FileDescription, _
                                 SQL, _
                                 lConexaoLocal, False)
        Linha_id = Linha_id + 1

    Wend

    Itens_historico(0) = "SINISTRO BB N� " & OSinistro_BB & " CANCELADO."

    If Not Inclui_Evento(Evento_SEGBR_id, gblsSinistro_Situacao_ID, "NULL", Itens_historico, "", Detalhamento_id, OSinistro_BB) Then

        Gera_Cancelamento_Sinistro_BB = False
        MsgBox "Erro na Inclus�o do Evento de Exig�ncia", vbOKOnly + vbCritical, App.EXEName
        Exit Function

    End If

    vDetalhamento = ""

    Exit Function

Trata_Erro:
    TrataErroGeral "Manuten��o de Detalhes"
    Gera_Cancelamento_Sinistro_BB = False

End Function



Private Function Verifica_Homologacao(ByVal OSinistro_BB As String) As Boolean

    Dim rs As ADODB.Recordset
    Dim vSql As String

    On Error GoTo Trata_Erro

    Verifica_Homologacao = False

    vSql = "SELECT * FROM INTERFACE_DB..ENTRADA_GTR_TB WITH (NOLOCK) "
    vSql = vSql & " WHERE SINISTRO_BB = " & OSinistro_BB
    vSql = vSql & " AND EVENTO_BB = 2003"

    Set rs = Conexao_ExecutarSQL(gsSIGLASISTEMA, _
                                 glAmbiente_id, _
                                 App.Title, _
                                 App.FileDescription, _
                                 vSql, _
                                 lConexaoLocal, True)

    If Not rs.EOF Then
        Verifica_Homologacao = True
    End If


    Exit Function

Trata_Erro:
    TrataErroGeral "Verifica_Homologacao"
    Verifica_Homologacao = False

End Function

Sub IniciaPesquisaClienteId()


    If mskCPF_Sinistrado.ClipText <> "" Then

        txtSinistrado_Cliente_ID.Text = ObterClienteId(mskCPF_Sinistrado.ClipText)

    Else

        txtSinistrado_Cliente_ID.Text = "0"
    End If


End Sub

Private Function ObterClienteId(ByVal sCPF As String) As Long

    Dim rs As ADODB.Recordset
    Dim sSql As String
    Dim lClienteId As String

    On Error GoTo TratarErro

    MousePointer = vbHourglass

    DoEvents

    lClienteId = 0

    sSql = ""
    sSql = sSql & "SELECT cliente_tb.cliente_id,  cliente_tb.nome " & vbNewLine
    sSql = sSql & " ,pessoa_fisica_tb.sexo,  cliente_tb.cliente_id , pessoa_fisica_tb.dt_nascimento" & vbNewLine
    sSql = sSql & "  FROM cliente_tb WITH (NOLOCK) " & vbNewLine
    sSql = sSql & "  JOIN pessoa_fisica_tb WITH (NOLOCK) " & vbNewLine
    sSql = sSql & "    ON pessoa_fisica_tb.pf_cliente_id = cliente_tb.cliente_id " & vbNewLine
    sSql = sSql & " WHERE pessoa_fisica_tb.cpf = '" & sCPF & "'" & vbNewLine

    Set rs = Conexao_ExecutarSQL(gsSIGLASISTEMA, _
                                 glAmbiente_id, _
                                 App.Title, _
                                 App.FileDescription, _
                                 sSql, _
                                 lConexaoLocal, True)

    If Not rs.EOF Then

        lClienteId = rs("cliente_id")
        txtSinistro_Vida_Nome = rs("nome")
        mskDtNasc_Sinistrado = Format(CStr(rs("dt_nascimento")), "dd/mm/yyyy")

        '        If Not IsNull(rs("sexo")) Then
        '            txtSinistro_Vida_Sexo = IIf(rs("sexo") = "M", "Masculino", "Feminino")
        '        Else
        '            txtSinistro_Vida_Sexo = " "
        '        End If
        If Not IsNull(rs("sexo")) Then
            'CmbVidaSexo.Text = IIf(rs("sexo") = "M", "Masculino", "Feminino")
            CmbVidaSexo.ListIndex = IIf(rs("sexo") = "M", 0, 1)
        Else
            'CmbVidaSexo.Text = " " 'Cleber Sardo - Data 27/11/2017 - Corrigindo erro na comunica��o - IM00139600
            CmbVidaSexo.ListIndex = 2
        End If

        ObterClienteId = lClienteId
    Else

        ObterClienteId = 0

    End If

    rs.Close
    Set rs = Nothing



    MousePointer = vbDefault

    Exit Function

TratarErro:
    Call TrataErroGeral("ObterClienteId")
    Call FinalizarAplicacao

End Function


Private Sub mskCPF_Sinistrado_LostFocus()

    Call IniciaPesquisaClienteId

End Sub

Public Sub Monta_FrameSinistrado()

    Dim rs As ADODB.Recordset, SQL As String
    Dim Rs2 As ADODB.Recordset



    SQL = "SELECT b.cliente_id, nome = ISNULL(b.nome,''), cpf = ISNULL(c.cpf,''), "
    SQL = SQL & " dt_nasc = ISNULL(c.dt_nascimento,''), sexo = ISNULL(c.sexo,'') "
    SQL = SQL & " FROM proposta_vida_grupo_tb a  WITH (NOLOCK) , cliente_tb b  WITH (NOLOCK) , pessoa_fisica_tb c WITH (NOLOCK) "
    SQL = SQL & " WHERE a.proposta_id = " & gbllProposta_ID
    SQL = SQL & "   AND a.dt_fim_vigencia IS NULL "
    SQL = SQL & "   AND a.cliente_id = b.cliente_id "
    SQL = SQL & "   AND b.cliente_id = c.pf_cliente_id "

    Set rs = Conexao_ExecutarSQL(gsSIGLASISTEMA, _
                                 glAmbiente_id, _
                                 App.Title, _
                                 App.FileDescription, _
                                 SQL, _
                                 lConexaoLocal, True)

    If rs.EOF Then

        If sOperacaoCosseguro <> "C" Then

            SQL = "SELECT nome = ISNULL(a.nome,''), cpf = ISNULL(b.cpf,''), "
            SQL = SQL & " dt_nasc = ISNULL(b.dt_nascimento,''), sexo = ISNULL(b.sexo,'') "
            SQL = SQL & " FROM cliente_tb a  WITH (NOLOCK) , pessoa_fisica_tb b WITH (NOLOCK) "
            SQL = SQL & " WHERE a.cliente_id = b.pf_cliente_id "
            SQL = SQL & " AND a.cliente_id = " & gbllProp_Cliente_ID

            Set Rs2 = Conexao_ExecutarSQL(gsSIGLASISTEMA, _
                                          glAmbiente_id, _
                                          App.Title, _
                                          App.FileDescription, _
                                          SQL, _
                                          lConexaoLocal, True)

            If Not Rs2.EOF Then

                txtSinistrado_Cliente_ID.Text = gbllProp_Cliente_ID
                mskCPF_Sinistrado = Format(Rs2!CPF, "&&&.&&&.&&&-&&")
                txtSinistro_Vida_Nome = Rs2!Nome
                mskDtNasc_Sinistrado = IIf(Rs2!dt_nasc = "01/01/1900", "__/__/____", Format(Rs2!dt_nasc, "dd/mm/yyyy"))
                'txtSinistro_Vida_Sexo = IIf(Trim(Rs2!Sexo) <> "", IIf(Rs2!Sexo = "M", "Masculino", "Feminino"), " ")
                'CmbVidaSexo.Text = IIf(Trim(Rs2!Sexo) <> "", IIf(Rs2!Sexo = "M", "Masculino", "Feminino"), " ")
                
                'Cleber Sardo - Inicio - INC000004324577 - Data 09/05/2014 - Corre��o da condi��o
                'CmbVidaSexo.ListIndex = IIf(Trim(Rs2!Sexo) <> "", IIf(Rs2!Sexo = "M", 0, 1), " ")
                'If Not IsNull(Rs2!Sexo) Then
                    'CmbVidaSexo.Text = IIf(Trim(Rs2!Sexo) <> "", IIf(Rs2!Sexo = "M", "Masculino", "Feminino"), " ")
                    'CmbVidaSexo.ListIndex = IIf(Trim(Rs2!Sexo) <> "", IIf(Rs2!Sexo = "M", 0, 1), " ")
                'End If
                 If (Rs2!Sexo) <> " " Then
                    CmbVidaSexo.ListIndex = IIf(Rs2!Sexo = "M", 0, 1)
                 End If
                'Cleber Sardo - Fim - INC000004324577 - Data 09/05/2014 - Corre��o da condi��o
            End If
            Rs2.Close
        End If

    Else

        txtSinistrado_Cliente_ID.Text = gbllProp_Cliente_ID
        mskCPF_Sinistrado = Format(rs!CPF, "&&&.&&&.&&&-&&")
        txtSinistro_Vida_Nome = rs!Nome
        mskDtNasc_Sinistrado = IIf(rs!dt_nasc = "01/01/1900", "__/__/____", Format(Rs2!dt_nasc, "dd/mm/yyyy"))
        'txtSinistro_Vida_Sexo = IIf(Trim(rs!Sexo) <> "", IIf(rs!Sexo = "M", "Masculino", "Feminino"), " ")
        CmbVidaSexo.Text = IIf(Trim(rs!Sexo) <> "", IIf(rs!Sexo = "M", "Masculino", "Feminino"), " ")
    End If

    rs.Close


End Sub

Public Function ValidaAvisoData() As Boolean

    ValidaAvisoData = True

    '    If bytModoOperacao = 1 Then
    '        Call Busca_Numero_Sinistro
    '    End If



    If Not VerificaData2(mskData_Ocorrencia) Then
        ValidaAvisoData = False
        mensagem_erro 3, "Data Ocorr�ncia"

        If mskData_Ocorrencia.Enabled Then
            mskData_Ocorrencia.SetFocus
        End If
        Exit Function
    End If

    If Format(mskData_Ocorrencia.Text, "yyyymmdd") > Format(txtData_Inclusao.Text, "yyyymmdd") Then
        ValidaAvisoData = False
        
        'Alan Neves - 04/03/2013 - Inicio - Acrescentado valida��o na mensagem de acordo com o tipo do Modo Opera��o - Inicio
        'Email Grazi - 04/03/2013 - 10:39 - Assunto: ENC: Protocolo: 13315010 cpf 49796100649
        'mensagem_erro 6, "Data Ocorr�ncia maior que a atual!"
        If bytModoOperacao = 2 Then
           mensagem_erro 6, "Data Ocorr�ncia maior que a data de inclus�o!"
        Else
           mensagem_erro 6, "Data Ocorr�ncia maior que a atual!"
        End If
        ' Alan Neves - 04/03/2013 - Fim
        
        If mskData_Ocorrencia.Enabled Then
            mskData_Ocorrencia.SetFocus
        End If
        Exit Function
    End If


    '    If bytTipoRamo <> bytTipoRamo_Vida And bytModoOperacao = 1 And CStr(Left(gbldSinistro_ID, 2)) <> "21" And _
         '    (CStr(Left(gbldSinistro_ID, 2)) <> "22" And Format(mskData_Ocorrencia.Text, "yyyymmdd") <= "20030331") Then


    'If mskFimVigApolice <> "" Then

    '            If bytModoOperacao <> 2 Then
    '                 If gbllProduto_ID <> 5 And gbllProduto_ID <> 424 Then
    '                        If Format(mskData_Ocorrencia, "yyyymmdd") < Format(mskIniVigApolice, "yyyymmdd") Or _
                             '                             Format(mskData_Ocorrencia, "yyyymmdd") > Format(mskFimVigApolice, "yyyymmdd") Then
    '                             ValidaAvisoData = False
    '                             mensagem_erro 6, "Data Ocorr�ncia fora da vig�ncia da ap�lice!"
    '
    '                             If mskData_Ocorrencia.Enabled Then
    '                                 mskData_Ocorrencia.SetFocus
    '                             End If
    '                             Exit Function
    '                        End If
    '                 Else
    '                        If Format(mskData_Ocorrencia, "yyyymmdd") < Format(mskIniVigApolice, "yyyymmdd") Then
    '                             ValidaAvisoData = False
    '                             mensagem_erro 6, "Data Ocorr�ncia fora da vig�ncia da ap�lice!"
    '                             If mskData_Ocorrencia.Enabled Then
    '                                 mskData_Ocorrencia.SetFocus
    '                             End If
    '                             Exit Function
    '                         End If
    '                 End If
    '            End If
    '        'Else
    '
    '            If bytModoOperacao <> 2 Then
    '
    '                If Format(mskData_Ocorrencia, "yyyymmdd") < Format(mskIniVigApolice, "yyyymmdd") Then
    '                    ValidaAvisoData = False
    '                    mensagem_erro 6, "Data Ocorr�ncia fora da vig�ncia da ap�lice!"
    '
    '                    If mskData_Ocorrencia.Enabled Then
    '                        mskData_Ocorrencia.SetFocus
    '                    End If
    '                    Exit Function
    '                End If
    '
    '            End If
    '        'End If
    '    End If

    If Not VerificaData2(mskData_Aviso) Then
        ValidaAvisoData = False
        mensagem_erro 3, "Data Aviso"

        If mskData_Aviso.Enabled Then
            mskData_Aviso.SetFocus
        End If
        Exit Function
    End If

    If Format(mskData_Ocorrencia.Text, "yyyymmdd") > Format(mskData_Aviso.Text, "yyyymmdd") Then
        ValidaAvisoData = False
        mensagem_erro 6, "Data Aviso menor que a Data de Ocorr�ncia!"

        If mskData_Aviso.Enabled Then
            mskData_Aviso.SetFocus
        End If
        Exit Function
    End If

    If Format(mskData_Aviso, "yyyymmdd") > Format(Data_Sistema, "yyyymmdd") Then
        ValidaAvisoData = False
        mensagem_erro 6, "Data Aviso maior que a atual!"
        'sstabDados.Tab = 1
        'If mskDataAviso.Enabled Then
        mskData_Aviso.SetFocus
        'End If
        Exit Function
    End If


    If bytModoOperacao <> 1 Then

        'Sincronizando com as altera��es realizadas pelo matilde - fad�o
        '        If (Month(mskData_Aviso) <> Month(Format(mskData_Aviso.Text, "yyyymmdd"))) Or _
                 '           (Year(mskData_Aviso) <> Year(Format(mskData_Aviso.Text, "yyyymmdd"))) Then
        If (Month(mskData_Aviso) <> Month(mskData_Aviso.Text)) Or _
           (Year(mskData_Aviso) <> Year(mskData_Aviso.Text)) Then
            ValidaAvisoData = False
            mensagem_erro 6, "O m�s da Data de Aviso n�o pode ser alterado!"

            If mskData_Aviso.Enabled Then
                mskData_Aviso.SetFocus
            End If
            Exit Function
        End If

    End If

    If Not VerificaData2(mskData_Entrada) Then
        ValidaAvisoData = False
        mensagem_erro 3, "Data Entrada"

        If mskData_Entrada.Enabled Then
            mskData_Entrada.SetFocus
        End If
        Exit Function
    End If

    If Format(mskData_Entrada, "yyyymmdd") > Format(Data_Sistema, "yyyymmdd") Then
        ValidaAvisoData = False
        mensagem_erro 6, "Data de Entrada maior que a atual!"

        If mskData_Entrada.Enabled Then
            mskData_Entrada.SetFocus
        End If
        Exit Function
    End If

    If Format(mskData_Entrada, "yyyymmdd") < Format(mskData_Aviso, "yyyymmdd") Then
        ValidaAvisoData = False
        mensagem_erro 6, "Data de Entrada menor que a Data de Aviso!"

        If mskData_Entrada.Enabled Then
            mskData_Entrada.SetFocus
        End If
        Exit Function
    End If

    ' Se n�o estiver mais no aviso, n�o pode mudar o m�s da entrada.
    ' (Dados cont�beis seriam alterados).
    If bytModoOperacao <> 1 Then

        If (Month(mskData_Entrada) <> Month(mskData_Entrada)) Or _
           (Year(mskData_Entrada) <> Year(mskData_Entrada)) Then
            ValidaAvisoData = False
            mensagem_erro 6, "O m�s da Data de Entrada n�o pode ser alterado!"

            If mskData_Entrada.Enabled Then
                mskData_Entrada.SetFocus
            End If
            Exit Function
        End If
    End If

End Function
Function VerificaData2(ByVal DataBase As Variant) As Boolean
    Dim dia, mes, ano

    If Not IsDate(DataBase) Then
        VerificaData2 = False
        Exit Function
    End If

    dia = Mid(DataBase, 1, 2)
    mes = Mid(DataBase, 4, 2)
    ano = Mid(DataBase, 7, 4)

    Select Case mes
    Case 1, 3, 5, 7, 8, 10, 12
        If dia > 31 Then
            VerificaData2 = False
            Exit Function
        End If
    Case 4, 6, 9, 11
        If dia > 30 Then
            VerificaData2 = False
            Exit Function
        End If
    Case 2
        If (ano Mod 4) = 0 Then
            If dia > 29 Then
                VerificaData2 = False
                Exit Function
            End If
        Else
            If dia > 28 Then
                VerificaData2 = False
                Exit Function
            End If
        End If
    Case Else
        VerificaData2 = False
        Exit Function
    End Select

    If ano < 1900 Or ano > 2099 Then
        VerificaData2 = False
        Exit Function
    End If


    VerificaData2 = True

End Function
Public Sub CarregaComboSexo()
    On Error GoTo Trata_Erro

    While CmbVidaSexo.ListCount > 0
        CmbVidaSexo.RemoveItem (0)
    Wend
    CmbVidaSexo.AddItem ("Masculino")
    CmbVidaSexo.ItemData(0) = 0

    CmbVidaSexo.AddItem ("Feminino")
    CmbVidaSexo.ItemData(1) = 1
    'Cleber Sardo - Data 27/11/2017 - Corrigindo erro na comunica��o - IM00139600
    CmbVidaSexo.AddItem ("")
    CmbVidaSexo.ItemData(2) = 2

    CmbVidaSexo.ListIndex = 0
    Exit Sub

Trata_Erro:
    TrataErroGeral "CarregaComboSexo"
End Sub

Sub Preenche_CmbReabertura()

    Dim sDataUsu As String
    Dim Data_Reabertura As String
    Dim vSql As String
    Dim rs As ADODB.Recordset

    On Error GoTo Trata_Erro


    vSql = "SELECT DISTINCT dt_evento, usuario FROM sinistro_historico_tb WITH (NOLOCK) " & _
           "WHERE sinistro_historico_tb.sinistro_id = " & gbldSinistro_ID & _
         "  AND sinistro_historico_tb.evento_segbr_id IN (10005, 10103, 10130)"


    Set rs = Conexao_ExecutarSQL(gsSIGLASISTEMA, _
                                 glAmbiente_id, _
                                 App.Title, _
                                 App.FileDescription, _
                                 vSql, _
                                 lConexaoLocal, True)

    While Not rs.EOF
        frameReabertura.Visible = True
        Data_Reabertura = Format(rs(0), "dd/mm/yyyy")
        sDataUsu = Data_Reabertura & "-" & rs(1)
        cmbReabertura.AddItem sDataUsu
        cmbReabertura.ListIndex = 0
        rs.MoveNext
    Wend
    rs.Close

    Exit Sub

Trata_Erro:
    TrataErroGeral "Preenche_CmbReabertura"
End Sub

Private Sub txtEndosso_keypress(KeyAscii As Integer)


    If Not IsNumeric(Chr(KeyAscii)) And KeyAscii <> 8 Then
        KeyAscii = 0
    End If


End Sub

Private Sub txtNumero_Sinistro_InformacaoBB_keypress(KeyAscii As Integer)

    If Not IsNumeric(Chr(KeyAscii)) And KeyAscii <> 8 Then
        KeyAscii = 0
    End If

End Sub

Private Sub TxtNumero_Sinistro_Lider_KeyPress(KeyAscii As Integer)

    If Not IsNumeric(Chr(KeyAscii)) And KeyAscii <> 8 Then
        KeyAscii = 0
    End If

End Sub


Sub Preenche_Moeda()
    With FrmAvisoSinistro

        .cboMoeda.Visible = False
        vSql = "SELECT sigla, moeda_id " & _
               "FROM proposta_fechada_tb INNER JOIN Moeda_tb " & _
               "ON proposta_fechada_tb.seguro_moeda_id = moeda_tb.moeda_id " & _
               "WHERE proposta_fechada_tb.proposta_id = " & gbllProposta_ID

        Set rs = Conexao_ExecutarSQL(gsSIGLASISTEMA, _
                                     glAmbiente_id, _
                                     App.Title, _
                                     App.FileDescription, _
                                     vSql, _
                                     lConexaoLocal, True)

        If Not rs.EOF Then
            .txtMoeda = rs(0)
            .txtMoeda.Tag = rs(1)
        Else
            rs.Close
            vSql = "SELECT sigla, moeda_id " & _
                   "FROM proposta_adesao_tb INNER JOIN Moeda_tb " & _
                   "ON proposta_adesao_tb.seguro_moeda_id = moeda_tb.moeda_id " & _
                   "WHERE proposta_adesao_tb.proposta_id = " & gbllProposta_ID
            Set rs = Conexao_ExecutarSQL(gsSIGLASISTEMA, _
                                         glAmbiente_id, _
                                         App.Title, _
                                         App.FileDescription, _
                                         vSql, _
                                         lConexaoLocal, True)
            If Not rs.EOF Then
                .txtMoeda = rs(0)
                .txtMoeda.Tag = rs(1)
            End If
        End If
        rs.Close

        '* ----------------------------------------------------------------------------- *'
        '* Inclu�do por Junior em 09/08/2002                                             *'
        '* Se for cosseguro, ent�o mostra a cbo com as moedas Atual e do SEGURO          *'
        '* ----------------------------------------------------------------------------- *'
        If sOperacaoCosseguro = "C" Then
            .cboMoeda.Visible = True
            txtMoeda.Visible = False
            'Monta cboMoeda com a moeda Atual e a moeda da ap�lice
            vSql = "SELECT moeda_id = p.val_parametro, m.sigla"
            vSql = vSql & " FROM ps_parametro_tb p"
            vSql = vSql & "     INNER JOIN moeda_tb m"
            vSql = vSql & "         ON (m.moeda_id = p.val_parametro)"
            vSql = vSql & " WHERE p.parametro = 'MOEDA ATUAL'"
            Set rs = Conexao_ExecutarSQL(gsSIGLASISTEMA, _
                                         glAmbiente_id, _
                                         App.Title, _
                                         App.FileDescription, _
                                         vSql, _
                                         lConexaoLocal, True)
            If Not rs.EOF Then
                .cboMoeda.AddItem rs("sigla")
                .cboMoeda.ItemData(.cboMoeda.NewIndex) = rs("moeda_id")
            End If
            rs.Close

            'Obt�m a moeda da proposta
            vSql = "SELECT a.seguro_moeda_id, m.sigla"
            vSql = vSql & " FROM proposta_adesao_tb a"
            vSql = vSql & "     INNER JOIN moeda_tb m"
            vSql = vSql & "         ON (m.moeda_id = a.seguro_moeda_id)"
            vSql = vSql & " WHERE a.Proposta_id = " & gbllProposta_ID
            vSql = vSql & " UNION "
            vSql = vSql & " SELECT f.seguro_moeda_id, m.sigla"
            vSql = vSql & " FROM proposta_fechada_tb f"
            vSql = vSql & "     INNER JOIN moeda_tb m"
            vSql = vSql & "         ON (m.moeda_id = f.seguro_moeda_id)"
            vSql = vSql & " WHERE f.Proposta_id = " & gbllProposta_ID
            Set rs = Conexao_ExecutarSQL(gsSIGLASISTEMA, _
                                         glAmbiente_id, _
                                         App.Title, _
                                         App.FileDescription, _
                                         vSql, _
                                         lConexaoLocal, True)
            If Not rs.EOF Then
                If .cboMoeda.ListCount > 0 Then
                    If .cboMoeda.ItemData(0) <> Val(rs("seguro_moeda_id")) Then
                        .cboMoeda.AddItem rs("sigla")
                        .cboMoeda.ItemData(.cboMoeda.NewIndex) = rs("seguro_moeda_id")
                    End If
                Else
                    .cboMoeda.AddItem rs("sigla")
                    .cboMoeda.ItemData(.cboMoeda.NewIndex) = rs("seguro_moeda_id")
                End If
            End If
            rs.Close
        End If
    End With
End Sub

Private Function Valida_endosso() As Boolean

    Dim SQL As String, rs As Recordset

    Valida_endosso = True

    'mathayde - 29
    If txtEndosso.Text = "" Then
        Exit Function
    End If


    SQL = "SELECT count(*) "
    SQL = SQL & " FROM endosso_tb WITH (NOLOCK) "
    SQL = SQL & " WHERE endosso_id = " & IIf(Len(txtEndosso.Text) = 0, 0, Val(txtEndosso.Text))
    SQL = SQL & "   AND proposta_id = " & gbllProposta_ID

    Set rs = Conexao_ExecutarSQL(gsSIGLASISTEMA, _
                                 glAmbiente_id, _
                                 App.Title, _
                                 App.FileDescription, _
                                 SQL, _
                                 lConexaoLocal, True)
    If Not rs.EOF Then
        If rs(0) = 0 Then
            If MsgBox("O endosso n�o existe para a proposta " & gbllProposta_ID & "." & vbNewLine & _
                      "Favor verificar o endosso correspondente para esta proposta", vbInformation, "Aten��o") Then
                Valida_endosso = False
            End If
        End If
    End If
    rs.Close
End Function

'mathayde - 29
Private Function valida_estado(ByVal iMunicipio As Integer) As Boolean
    Dim SQL As String, rs As Recordset
    Dim rRs As Recordset

    valida_estado = False

    sSql = ""
    sSql = sSql & "SELECT estado"
    sSql = sSql & "  From seguros_db.dbo.municipio_tb"
    sSql = sSql & " Where municipio_id = " & iMunicipio
    sSql = sSql & "   and estado like '%" & TxtEstado_Ocorrencia.Text & "%'"

    Set rRs = Conexao_ExecutarSQL(gsSIGLASISTEMA, _
                                  glAmbiente_id, _
                                  App.Title, _
                                  App.FileDescription, _
                                  sSql, _
                                  lConexaoLocal, True)


    If Not rRs.EOF Then
        valida_estado = True
    Else
        MsgBox ("Estado inv�lido para esse munic�pio!")
    End If

    rRs.Close
    Set rRs = Nothing

End Function

Private Sub InicializaModoOperacaoLocal()
    On Error GoTo Erro

    '1-Inclusao / 2-Altera��o / 3-Consulta
    'InicializaModoOperacao Me, bytModoOperacao

    If bytModoOperacao = 3 Then
        If bytTipoRamo = bytTipoRamo_Vida Then
            cboCausa_Sinistro.Locked = True
            cboCausa_Sinistro.Locked = True
            cboSubevento_Sinistro.Locked = True
            TxtNumero_Sinistro_Lider.Locked = True
            mskData_Ocorrencia.Enabled = False
            mskData_Entrada.Enabled = False
            mskData_Aviso.Enabled = False
            txtData_Inclusao.Locked = True
            txtData_Inclusao.Locked = True
            chkReintegracaoIS.Enabled = False
            mskAgencia_Aviso.Enabled = False
            mskAgencia_Aviso.Enabled = False
            txtSinistro_Vida_Nome.Locked = False
            mskCPF_Sinistrado.Enabled = False
            mskDtNasc_Sinistrado.Enabled = False
            mskDtNasc_Sinistrado.Enabled = False
            txtSinistrado_Cliente_ID.Locked = False
            TxtEndereco_Ocorrencia.Locked = True
            txtSinistro_Vida_Nome.Locked = True
            txtSinistrado_Cliente_ID.Locked = True
            CmbVidaSexo.Locked = True
            txtEndosso.Locked = True
            mskAverbacao.Enabled = False
            cmbReabertura.Locked = True
        End If
    End If
    
    ' Francisco H. Berrocal
    ' Data de ocorr�ncia do sinistro n�o pode ser alterado ap�s aviso de sinistro
    If bytModoOperacao = 2 Then
        mskData_Ocorrencia.Enabled = False
    End If
    
    Exit Sub

Erro:
    Call TrataErroGeral("InicializaModoOperacao", "frmPrestadores")
    Call FinalizarAplicacao

End Sub

Private Function VerificarExistenciaAviso() As String   'FL�VIO.BEZERRA - 14/01/2013
Dim rs As Recordset
Dim sSql As String
VerificarExistenciaAviso = "0"

sSql = "" & vbNewLine
sSql = "select s.sinistro_id" & vbNewLine
sSql = sSql & " from sinistro_tb s WITH (NOLOCK) " & vbNewLine
sSql = sSql & " inner join apolice_tb a WITH (NOLOCK) " & vbNewLine
sSql = sSql & " on s.proposta_id = a.proposta_id" & vbNewLine
sSql = sSql & " inner join apolice_terceiros_tb b WITH (NOLOCK) " & vbNewLine
sSql = sSql & " on a.sucursal_seguradora_id = b.sucursal_seguradora_id" & vbNewLine
sSql = sSql & " and a.seguradora_cod_susep = b.seguradora_cod_susep" & vbNewLine
sSql = sSql & " and a.apolice_id = b.apolice_id" & vbNewLine
sSql = sSql & " and a.ramo_id = b.ramo_id" & vbNewLine
sSql = sSql & " Where s.Sinistro_id_Lider = " & IIf(TxtNumero_Sinistro_Lider.Text = "", 0, TxtNumero_Sinistro_Lider.Text) & vbNewLine
'sSQL = sSQL & " and seg_cod_susep_lider = " & txtSeguradora_Nome.Tag & vbNewLine
sSql = sSql & " and dt_ocorrencia_sinistro = '" & Format(mskData_Ocorrencia.Text, "yyyymmdd") & "'" & vbNewLine
sSql = sSql & " order by s.sinistro_id" & vbNewLine
Set rs = Conexao_ExecutarSQL(gsSIGLASISTEMA, _
                             glAmbiente_id, _
                             App.Title, _
                             App.FileDescription, _
                             sSql, _
                             lConexaoLocal, _
                             True)
                             
If rs.EOF Then
    Exit Function
End If

VerificarExistenciaAviso = ""
While Not rs.EOF
    VerificarExistenciaAviso = VerificarExistenciaAviso & rs.Fields!sinistro_id & ","
rs.MoveNext
Wend
End Function



