VERSION 5.00
Object = "{5E9E78A0-531B-11CF-91F6-C2863C385E30}#1.0#0"; "MSFLXGRD.OCX"
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.1#0"; "MSCOMCTL.OCX"
Begin VB.Form FrmEndereco_Escolher_Proposta 
   Caption         =   "Segp"
   ClientHeight    =   2685
   ClientLeft      =   60
   ClientTop       =   450
   ClientWidth     =   9735
   LinkTopic       =   "Form1"
   ScaleHeight     =   2685
   ScaleWidth      =   9735
   StartUpPosition =   3  'Windows Default
   Begin VB.CommandButton btnAplicar_Endereco_Escolher_Proposta 
      Caption         =   "Aplicar"
      Height          =   375
      Left            =   6480
      TabIndex        =   3
      Top             =   1920
      Width           =   1452
   End
   Begin VB.CommandButton BtnCancelar_Endereco_Proposta 
      Caption         =   "Cancelar"
      Height          =   375
      Left            =   8040
      TabIndex        =   2
      Top             =   1920
      Width           =   1452
   End
   Begin MSFlexGridLib.MSFlexGrid GrdEndereco_Risco 
      Height          =   1485
      Left            =   240
      TabIndex        =   0
      Top             =   240
      Width           =   9255
      _ExtentX        =   16325
      _ExtentY        =   2619
      _Version        =   393216
      Cols            =   7
      FixedCols       =   0
      AllowBigSelection=   0   'False
      FormatString    =   $"FrmEndereco_Escolher_Proposta.frx":0000
   End
   Begin MSComctlLib.StatusBar StatusBar 
      Align           =   2  'Align Bottom
      Height          =   285
      Left            =   0
      TabIndex        =   1
      Top             =   2400
      Width           =   9735
      _ExtentX        =   17171
      _ExtentY        =   503
      Style           =   1
      _Version        =   393216
      BeginProperty Panels {8E3867A5-8586-11D1-B16A-00C0F0283628} 
         NumPanels       =   1
         BeginProperty Panel1 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
         EndProperty
      EndProperty
   End
End
Attribute VB_Name = "FrmEndereco_Escolher_Proposta"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
'HENRIQUE H. HENRIQUES - CONFITEC INI
Public BlnExibirForm As Boolean
Public iCod_Objeto_Segurado As Integer

Private Sub btnAplicar_Endereco_Escolher_Proposta_Click()

    Dim Anterior As Variant
    Dim i As Integer



    If gbldSinistro_ID = 0 Then

        Anterior = FrmAvisoSinistro.vEnd_Risco_id
        FrmAvisoSinistro.vEnd_Risco_id = GrdEndereco_Risco.TextMatrix(GrdEndereco_Risco.Row, 0)
        vDtInicioSeg = GrdEndereco_Risco.TextMatrix(GrdEndereco_Risco.Row, 5)

        If Existe_Resseguro(True) = True And Anterior <> vEnd_Risco_id And Anterior <> 0 Then

            If MsgBox("O endere�o de risco mudou e existe resseguro. " & _
                      "Confirma ajuste dos percentuais sugeridos " & _
                      "relativos a este novo endere�o de risco?", _
                      vbYesNo + vbQuestion + vbDefaultButton2) = vbYes Then


                iCod_Objeto_Segurado = vEnd_Risco_id

                For i = 0 To FrmAvisoSinistro.CmbCodigo_Objeto_Ocorrencia.ListCount

                    If Trim(FrmAvisoSinistro.CmbCodigo_Objeto_Ocorrencia.List(i)) = i Then
                        FrmAvisoSinistro.CmbCodigo_Objeto_Ocorrencia.ListIndex = i
                        Exit For
                    End If
                Next


                If Consulta_Pendencia_Analise_Resseguro(False) = True Then
                    Call Busca_Perc_Resseguro(gbllProposta_ID, IIf(FrmAvisoSinistro.txtEndosso.Text = "", 0, FrmAvisoSinistro.txtEndosso.Text), Val(Left(FrmAvisoSinistro.CmbCodigo_Objeto_Ocorrencia.Text, 3)))    'Busca o percentual sugerido
                End If



                FrmAvisoSinistro.bSaiDoLoop = True
            Else

                FrmAvisoSinistro.bSaiDoLoop = True
            End If
        End If
    Else


        Anterior = FrmAvisoSinistro.vEnd_Risco_id

        For i = 1 To FrmAvisoSinistro.CmbCodigo_Objeto_Ocorrencia.ListCount

            FrmAvisoSinistro.CmbCodigo_Objeto_Ocorrencia.ListIndex = i - 1


            If Trim(Left(FrmAvisoSinistro.CmbCodigo_Objeto_Ocorrencia.Text, 3)) = GrdEndereco_Risco.TextMatrix(GrdEndereco_Risco.Row, 0) Then
                Exit For
            End If

        Next
        ' FrmAvisoSinistro.CmbCodigo_Objeto_Ocorrencia.ListIndex = GrdEndereco_Risco.TextMatrix(GrdEndereco_Risco.Row, 0)

        FrmAvisoSinistro.sDt_inicio_vigencia_seg = GrdEndereco_Risco.TextMatrix(GrdEndereco_Risco.Row, 5)

        If Existe_Resseguro(False) = True And Anterior <> FrmAvisoSinistro.CmbCodigo_Objeto_Ocorrencia.Text Then

            If MsgBox("O endere�o de risco mudou e existe resseguro. " & _
                      "Confirma nova estimativa com os percentuais " & _
                      "relativos a este novo endere�o de risco?", _
                      vbYesNo + vbQuestion + vbDefaultButton2) = vbYes Then

                '                    Call btnEstimativaAdicionar_Click 'Inclui a nova estimativa

                If Consulta_Pendencia_Analise_Resseguro(False) = True Then
                    Call Busca_Perc_Resseguro(gbllProposta_ID, FrmAvisoSinistro.txtEndosso.Text, Val(Left(FrmAvisoSinistro.CmbCodigo_Objeto_Ocorrencia.Text, 3)))    'Busca o percentual sugerido

                End If

            End If
        End If

    End If


    For i = 1 To FrmAvisoSinistro.CmbCodigo_Objeto_Ocorrencia.ListCount

        If FrmAvisoSinistro.bSaiDoLoop Then
            FrmAvisoSinistro.bSaiDoLoop = False
            Exit For
        End If


        FrmAvisoSinistro.CmbCodigo_Objeto_Ocorrencia.ListIndex = i - 1

        If Trim(FrmAvisoSinistro.CmbCodigo_Objeto_Ocorrencia.Text) = GrdEndereco_Risco.TextMatrix(GrdEndereco_Risco.Row, 0) Then
            Exit For
        End If

    Next

    If vEnd_Risco_id <> Anterior Then

        FrmAvisoSinistro.TxtEndereco_Ocorrencia = GrdEndereco_Risco.TextMatrix(GrdEndereco_Risco.Row, 1)
        FrmAvisoSinistro.TxtBairro_Ocorrencia = GrdEndereco_Risco.TextMatrix(GrdEndereco_Risco.Row, 2)
        FrmAvisoSinistro.TxtMunicipio_Ocorrencia = GrdEndereco_Risco.TextMatrix(GrdEndereco_Risco.Row, 3)
        FrmAvisoSinistro.TxtEstado_Ocorrencia = GrdEndereco_Risco.TextMatrix(GrdEndereco_Risco.Row, 4)

    End If


    'BtnCancelar_Endereco_Escolher_Proposta_Click
    BtnCancelar_Endereco_Proposta_Click

End Sub








Private Sub BtnCancelar_Endereco_Proposta_Click()

    Unload Me

End Sub

Private Sub Form_Load()

    InicializaInterfaceLocal_Form

    Monta_GrdEndereco_Risco


End Sub


Private Sub InicializaInterfaceLocal_Form()
    Dim sTipoProcesso_Descricao As String


    Call CentraFrm(Me)

    Select Case bytModoOperacao
    Case "5"
        sTipoProcesso_Descricao = "Aviso de Sinistro"

    Case "A"
        sTipoProcesso_Descricao = "Aviso de Sinistro Cosseguro"
    End Select



    Me.Caption = App.EXEName & " - " & sTipoProcesso_Descricao & " - " & Ambiente


End Sub



Public Sub Monta_GrdEndereco_Risco()

    Dim vSql As String, Linha As String
    Dim Rst_EnderecoRisco As ADODB.Recordset

    vSql = "SELECT b.cod_objeto_segurado, a.end_risco_id, a.endereco, a.bairro, a.municipio, a.estado, b.dt_inicio_vigencia_seg, b.dt_fim_vigencia_seg, 1"
    vSql = vSql & " FROM endereco_risco_tb a  WITH (NOLOCK) , seguro_residencial_tb b WITH (NOLOCK) "
    vSql = vSql & " Where b.Proposta_id = " & gbllProposta_ID
    vSql = vSql & " AND a.Proposta_id = b.Proposta_id"
    vSql = vSql & " AND a.end_risco_id = b.end_risco_id"
    vSql = vSql & " AND b.dt_fim_vigencia_seg IS NULL"

    vSql = vSql & " Union"
    vSql = vSql & " SELECT c.cod_objeto_segurado, a.end_risco_id, a.endereco, a.bairro, a.municipio, a.estado, c.dt_inicio_vigencia_seg, c.dt_fim_vigencia_seg, 2"
    vSql = vSql & " FROM endereco_risco_tb a  WITH (NOLOCK) , seguro_empresarial_tb c WITH (NOLOCK) "
    vSql = vSql & " Where c.Proposta_id = " & gbllProposta_ID
    vSql = vSql & " AND a.Proposta_id = c.Proposta_id"
    vSql = vSql & " AND a.end_risco_id = c.end_risco_id"
    vSql = vSql & " AND c.dt_fim_vigencia_seg IS NULL"

    vSql = vSql & " Union"
    vSql = vSql & " SELECT d.cod_objeto_segurado, a.end_risco_id, a.endereco, a.bairro, a.municipio, a.estado, d.dt_inicio_vigencia_seg, d.dt_fim_vigencia_seg, 3"
    vSql = vSql & " FROM endereco_risco_tb a  WITH (NOLOCK) , seguro_condominio_tb d WITH (NOLOCK) "
    vSql = vSql & " Where d.Proposta_id = " & gbllProposta_ID
    vSql = vSql & " AND a.Proposta_id = d.Proposta_id"
    vSql = vSql & " AND a.end_risco_id = d.end_risco_id"
    vSql = vSql & " AND d.dt_fim_vigencia_seg IS NULL"

    vSql = vSql & " Union"
    vSql = vSql & " SELECT e.cod_objeto_segurado, a.end_risco_id, a.endereco, a.bairro, a.municipio, a.estado, e.dt_inicio_vigencia_seg, e.dt_fim_vigencia_seg, 4"
    vSql = vSql & " FROM endereco_risco_tb a  WITH (NOLOCK) , seguro_maquinas_tb e WITH (NOLOCK) "
    vSql = vSql & " Where e.Proposta_id = " & gbllProposta_ID
    vSql = vSql & " AND a.Proposta_id = e.Proposta_id"
    vSql = vSql & " AND a.end_risco_id = e.end_risco_id"
    vSql = vSql & " AND e.dt_fim_vigencia_seg IS NULL"

    vSql = vSql & " Union"
    vSql = vSql & " SELECT f.cod_objeto_segurado, a.end_risco_id, a.endereco, a.bairro, a.municipio, a.estado, f.dt_inicio_vigencia_seg, f.dt_fim_vigencia_seg, 5"
    vSql = vSql & " FROM endereco_risco_tb a  WITH (NOLOCK) , seguro_aceito_tb f WITH (NOLOCK) "
    vSql = vSql & " Where f.Proposta_id = " & gbllProposta_ID
    vSql = vSql & " AND a.Proposta_id = f.Proposta_id"
    vSql = vSql & " AND a.end_risco_id = f.end_risco_id"
    vSql = vSql & " AND f.dt_fim_vigencia_seg IS NULL"

    vSql = vSql & " Union"
    vSql = vSql & " SELECT g.cod_objeto_segurado, a.end_risco_id, a.endereco, a.bairro, a.municipio, a.estado, g.dt_inicio_vigencia_seg, g.dt_fim_vigencia_seg, 6"
    vSql = vSql & " FROM endereco_risco_tb a  WITH (NOLOCK) , seguro_avulso_tb g WITH (NOLOCK) "
    vSql = vSql & " Where g.Proposta_id = " & gbllProposta_ID
    vSql = vSql & " AND a.Proposta_id = g.Proposta_id"
    vSql = vSql & " AND a.end_risco_id = g.end_risco_id"
    vSql = vSql & " AND g.dt_fim_vigencia_seg IS NULL"

    vSql = vSql & " Union"
    vSql = vSql & " SELECT h.cod_objeto_segurado, NULL, h.desc_mercadoria, NULL, NULL, NULL, h.dt_inicio_vigencia_seg, h.dt_fim_vigencia_seg, 7"
    vSql = vSql & " FROM seguro_transporte_tb h WITH (NOLOCK) "
    vSql = vSql & " Where h.Proposta_id = " & gbllProposta_ID
    vSql = vSql & " AND h.dt_fim_vigencia_seg IS NULL"


    vSql = vSql & " Union"
    vSql = vSql & " SELECT h.cod_objeto_segurado, NULL, h.desc_mercadoria, NULL, NULL, NULL, h.dt_inicio_vigencia_seg, h.dt_fim_vigencia_seg, 7"
    vSql = vSql & " FROM seguro_transporte_tb h WITH (NOLOCK) "
    vSql = vSql & " Where h.Proposta_id = " & gbllProposta_ID
    vSql = vSql & " and dt_fim_vigencia_seg = (select max(dt_fim_vigencia_seg)"
    vSql = vSql & "                            from seguro_transporte_tb h1 WITH (NOLOCK) "
    vSql = vSql & "                            where h1.Proposta_id = " & gbllProposta_ID
    vSql = vSql & "                            and h1.cod_objeto_segurado = h.cod_objeto_segurado"
    vSql = vSql & "                            and h1.dt_fim_vigencia_seg is not null)"
    vSql = vSql & " and not exists            (select 1 from seguro_transporte_tb h2 WITH (NOLOCK) "
    vSql = vSql & "                            where h2.Proposta_id = " & gbllProposta_ID
    vSql = vSql & "                            and h2.cod_objeto_segurado = h.cod_objeto_segurado"
    vSql = vSql & "                            and h2.dt_fim_vigencia_seg is null)"


    vSql = vSql & " Union"
    vSql = vSql & " SELECT i.cod_objeto_segurado, a.end_risco_id, a.endereco, a.bairro, a.municipio, a.estado, i.dt_inicio_vigencia_seg, i.dt_fim_vigencia_seg, 8"
    vSql = vSql & " FROM endereco_risco_tb a WITH (NOLOCK) , seguro_generico_tb i WITH (NOLOCK) "
    vSql = vSql & " Where i.Proposta_id = " & gbllProposta_ID
    vSql = vSql & " AND a.Proposta_id = i.Proposta_id"
    vSql = vSql & " AND a.end_risco_id = i.end_risco_id"
    vSql = vSql & " AND i.dt_fim_vigencia_seg IS NULL"

    vSql = vSql & " UNION"
    vSql = vSql & " SELECT j.cod_objeto_segurado, NULL, NULL, NULL, NULL, NULL, j.dt_inicio_vigencia_seg, NULL, 9"
    vSql = vSql & " FROM seguro_penhor_rural_tb j WITH (NOLOCK) "
    vSql = vSql & " WHERE j.proposta_id = " & gbllProposta_ID

    vSql = vSql & " UNION"
    vSql = vSql & " SELECT k.cod_objeto_segurado, NULL, NULL, NULL, NULL, NULL, k.dt_inicio_vigencia_seg, NULL, 10"
    vSql = vSql & " FROM seguro_consorcio_quebra_garantia_tb k WITH (NOLOCK) "
    vSql = vSql & " WHERE k.proposta_id = " & gbllProposta_ID
    '''''''''''''''''''

    Set Rst_EnderecoRisco = Conexao_ExecutarSQL(gsSIGLASISTEMA, _
                                                glAmbiente_id, _
                                                App.Title, _
                                                App.FileDescription, _
                                                vSql, _
                                                lConexaoLocal, True)


    GrdEndereco_Risco.Rows = 1


    While Not Rst_EnderecoRisco.EOF

        If Rst_EnderecoRisco(8) <> "7" Then    ' TI
            GrdEndereco_Risco.Row = GrdEndereco_Risco.Rows - 1
            Linha = Rst_EnderecoRisco(0) & vbTab & _
                    Rst_EnderecoRisco(2) & vbTab & _
                    Rst_EnderecoRisco(3) & vbTab & _
                    Rst_EnderecoRisco(4) & vbTab & _
                    Rst_EnderecoRisco(5) & vbTab & _
                    IIf(IsNull(Rst_EnderecoRisco(6)), "", Format(Rst_EnderecoRisco(6), "dd/mm/yyyy")) & vbTab & _
                    IIf(IsNull(Rst_EnderecoRisco(7)), "", Format(Rst_EnderecoRisco(7), "dd/mm/yyyy"))

            GrdEndereco_Risco.AddItem Linha




        Else

            FrmAvisoSinistro.BtnSelecionar_Proposta.Visible = False

            BtnCancelar_Endereco_Proposta_Click
        End If

        Rst_EnderecoRisco.MoveNext

    Wend

    Rst_EnderecoRisco.Close

    GrdEndereco_Risco.Col = 1
    GrdEndereco_Risco.ColSel = 1
    GrdEndereco_Risco.Sort = 0
    GrdEndereco_Risco.Refresh

    StatusBar.SimpleText = "Selecione um dos endere�os de risco para ser o endere�o do sinistro"

    If GrdEndereco_Risco.Rows = 1 Then

        FrmAvisoSinistro.BtnSelecionar_Proposta.Enabled = False
        MsgBox "S� h� um Endere�o para a Proposta " & gbllProposta_ID & "", vbOKOnly, App.EXEName
        'HENRIQUE H. HENRIQUES - CONFITEC SP - INI
        BlnExibirForm = False
        'HENRIQUE H. HENRIQUES - CONFITEC SP - FIM
    End If



End Sub

Public Function Existe_Resseguro(Aviso As Boolean) As Boolean

    Dim rs As ADODB.Recordset, vSql As String

    On Error GoTo Trata_Erro

    If bytTipoRamo = bytTipoRamo_Vida Then    ' Vida n�o calcula resseguro

        Existe_Resseguro = False
        Exit Function

    ElseIf (gbllRamo_ID = 21) Or (gbllRamo_ID = 22) Or (gbllRamo_ID = 23) Or (gbllRamo_ID = 61) Or (gbllRamo_ID = 63) Then
        Existe_Resseguro = True
        Exit Function
    End If

    vSql = "SELECT proposta_id "
    vSql = vSql & " FROM proposta_fechada_tb WITH (NOLOCK) "
    vSql = vSql & " WHERE proposta_id = " & gbllProposta_ID
    vSql = vSql & " AND situacao_re_seguro in ('s','r') "
    vSql = vSql & " UNION SELECT proposta_id "
    vSql = vSql & " FROM proposta_adesao_tb WITH (NOLOCK) "
    vSql = vSql & " WHERE proposta_id = " & gbllProposta_ID
    vSql = vSql & " AND situacao_re_seguro in ('s','r') "

    Set rs = Conexao_ExecutarSQL(gsSIGLASISTEMA, _
                                 glAmbiente_id, _
                                 App.Title, _
                                 App.FileDescription, _
                                 vSql, _
                                 lConexaoLocal, True)

    If (Not rs.EOF) Then
        rs.Close
        Existe_Resseguro = True
    Else
        rs.Close

        If FrmAvisoSinistro.bytModoOperacao = 1 Then

            Existe_Resseguro = False

        Else

            vSql = "SELECT * FROM sinistro_estimativa_tb WITH (NOLOCK) "
            vSql = vSql & " WHERE sinistro_id = " & gbldSinistro_ID
            vSql = vSql & "   AND isnull(val_resseguro,0) <> 0 "

            Set rs = Conexao_ExecutarSQL(gsSIGLASISTEMA, _
                                         glAmbiente_id, _
                                         App.Title, _
                                         App.FileDescription, _
                                         vSql, _
                                         lConexaoLocal, True)

            If rs.EOF Then
                Existe_Resseguro = False
            Else
                Existe_Resseguro = True
            End If
            rs.Close
        End If
    End If

    Exit Function
Trata_Erro:

    TrataErroGeral "Existe Resseguro"
    Existe_Resseguro = False

End Function
Public Function Consulta_Pendencia_Analise_Resseguro(pagamento As Boolean) As Boolean

    Dim rs As ADODB.Recordset
    Dim vSql As String

    On Error GoTo Trata_Erro

    Consulta_Pendencia_Analise_Resseguro = True


    vSql = "SELECT a.situacao"
    vSql = vSql & " FROM resseg_db..re_Seguro_Item_tb a  WITH (NOLOCK) , resseg_db..Composicao_Res_Item_tb b WITH (NOLOCK) "
    vSql = vSql & " WHERE a.proposta_id = " & gbllProposta_ID
    vSql = vSql & "   AND a.cod_objeto_segurado = " & FrmAvisoSinistro.CmbCodigo_Objeto_Ocorrencia.ItemData(FrmAvisoSinistro.CmbCodigo_Objeto_Ocorrencia.ListIndex)
    vSql = vSql & "   AND a.re_seguro_item_id = b.re_seguro_item_id"
    vSql = vSql & "   AND b.cod_item_financeiro = 'PREMIO'"

    If Val("0" & FrmAvisoSinistro.txtEndosso.Text) = 0 Then
        vSql = vSql & " AND isnull(a.endosso_id, 0) = 0 "
    Else
        vSql = vSql & " AND a.endosso_id = " & FrmAvisoSinistro.txtEndosso.Text
    End If

    Set rs = Conexao_ExecutarSQL(gsSIGLASISTEMA, _
                                 glAmbiente_id, _
                                 App.Title, _
                                 App.FileDescription, _
                                 vSql, _
                                 lConexaoLocal, True)

    If Not rs.EOF Then
        While Not rs.EOF

            If rs(0) = "R" Then
                If pagamento = True Then
                    mensagem_erro 6, "Esta ap�lice encontra-se em estudo de resseguro, portanto nenhum" & _
                                   " pagamento pode ser aprovado." & Chr(13) & _
                                     "Favor contactar a �rea de re-seguro."
                End If
                Consulta_Pendencia_Analise_Resseguro = False
                Exit Function
            End If
            rs.MoveNext
        Wend
    End If

    Exit Function

Trata_Erro:
    Consulta_Pendencia_Analise_Resseguro = False
    TrataErroGeral "Consulta_Pendencia_Analise_Resseguro"

End Function

Public Sub Busca_Perc_Resseguro(ByVal lPropostaId As Long, _
                                ByVal lEndossoId As Long, _
                                ByVal lCodObjetoSegurado As Long)

    Dim sSql As String
    Dim MensalAnual As String

    On Error GoTo Trata_Erro

    sSql = "EXEC resseg_db.dbo.totaliza_valor_resseguro_sinistro_spu "
    sSql = sSql & "NULL, "
    sSql = sSql & "NULL, "
    sSql = sSql & "NULL, "
    sSql = sSql & "NULL, "
    sSql = sSql & "NULL, "
    sSql = sSql & "NULL, "
    sSql = sSql & lPropostaId & ","
    sSql = sSql & lEndossoId & ","
    sSql = sSql & lCodObjetoSegurado & ","
    sSql = sSql & " 0,"
    sSql = sSql & "NULL, "
    sSql = sSql & "NULL, "
    sSql = sSql & "NULL "

    Call Conexao_ExecutarSQL(gsSIGLASISTEMA, _
                             glAmbiente_id, _
                             App.Title, _
                             App.FileDescription, _
                             sSql, _
                             lConexaoLocal, False)

    Exit Sub

Trata_Erro:
    TrataErroGeral "Busca_Perc_Resseguro"

End Sub






