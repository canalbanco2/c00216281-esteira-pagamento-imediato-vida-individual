VERSION 5.00
Object = "{1CB70356-FEA2-11D4-87FA-00805F396245}#1.0#0"; "mask2s.OCX"
Object = "{5E9E78A0-531B-11CF-91F6-C2863C385E30}#1.0#0"; "MSFLXGRD.OCX"
Object = "{C932BA88-4374-101B-A56C-00AA003668DC}#1.1#0"; "MSMASK32.OCX"
Begin VB.Form frmEstimativa 
   Caption         =   "SEGP1287 - Consulta de Estimativa - VIDA"
   ClientHeight    =   7260
   ClientLeft      =   6030
   ClientTop       =   2100
   ClientWidth     =   11145
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   ScaleHeight     =   7260
   ScaleWidth      =   11145
   Begin VB.TextBox txtAuxiliar 
      Height          =   285
      Left            =   120
      TabIndex        =   19
      Text            =   "Cancelado"
      Top             =   7080
      Width           =   975
   End
   Begin VB.Frame frameValores 
      Caption         =   "Valores"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   7050
      Left            =   120
      TabIndex        =   0
      Top             =   120
      Width           =   10935
      Begin MSFlexGridLib.MSFlexGrid grdEnderecoRisco 
         Height          =   1365
         Left            =   240
         TabIndex        =   20
         Top             =   3240
         Width           =   8775
         _ExtentX        =   15478
         _ExtentY        =   2408
         _Version        =   393216
         Cols            =   5
         FixedCols       =   0
         AllowBigSelection=   0   'False
         FormatString    =   $"frmEstimativa.frx":0000
      End
      Begin VB.TextBox Text1 
         Height          =   375
         Left            =   9360
         TabIndex        =   16
         Text            =   "Text1"
         Top             =   360
         Width           =   1215
      End
      Begin VB.CommandButton btnOK_Estimativa 
         Caption         =   "Aplicar"
         Height          =   375
         Left            =   9240
         TabIndex        =   10
         Top             =   6000
         Width           =   1452
      End
      Begin VB.ComboBox cmbCoberturaAfetada 
         Height          =   315
         Left            =   255
         Style           =   2  'Dropdown List
         TabIndex        =   1
         Top             =   540
         Width           =   6720
      End
      Begin VB.CommandButton btnVoltar_Estimativa 
         Caption         =   "Voltar"
         Height          =   375
         Left            =   9240
         TabIndex        =   11
         Top             =   6480
         Width           =   1452
      End
      Begin VB.CommandButton btnCoberturaAlterar 
         Caption         =   "<< Alterar >>"
         Height          =   375
         Left            =   9240
         TabIndex        =   8
         Top             =   1440
         Width           =   1452
      End
      Begin VB.CommandButton btnCoberturaAdicionar 
         Caption         =   "<<  Adicionar"
         Height          =   375
         Left            =   9240
         TabIndex        =   7
         Top             =   960
         Width           =   1452
      End
      Begin VB.CommandButton btnCoberturaRemover 
         Caption         =   "Excluir >>"
         Height          =   375
         Left            =   9240
         TabIndex        =   9
         Top             =   1920
         Width           =   1452
      End
      Begin Mask2S.ConMask2S mskValorPrejuizo 
         Height          =   375
         Left            =   7180
         TabIndex        =   2
         Top             =   510
         Width           =   1815
         _ExtentX        =   3201
         _ExtentY        =   661
         mask            =   ""
         text            =   "0,00"
         locked          =   0   'False
         enabled         =   -1  'True
      End
      Begin MSFlexGridLib.MSFlexGrid grdEstimativasPagamentos_Estimativas 
         Height          =   2145
         Left            =   240
         TabIndex        =   6
         Top             =   4770
         Width           =   8775
         _ExtentX        =   15478
         _ExtentY        =   3784
         _Version        =   393216
         Rows            =   8
         Cols            =   6
         GridLineWidth   =   2
         FormatString    =   "^Item                             |^Valor             |^Pago/Recebido  |^Saldo            |^Resseguro                  |^Sit.  "
      End
      Begin Mask2S.ConMask2S mskPercER 
         Height          =   315
         Left            =   7800
         TabIndex        =   4
         Top             =   3480
         Visible         =   0   'False
         Width           =   705
         _ExtentX        =   1244
         _ExtentY        =   556
         mask            =   ""
         text            =   "0,000"
         locked          =   0   'False
         enabled         =   -1  'True
      End
      Begin Mask2S.ConMask2S mskPercResseguro 
         Height          =   375
         Left            =   7320
         TabIndex        =   3
         Top             =   3240
         Visible         =   0   'False
         Width           =   705
         _ExtentX        =   1244
         _ExtentY        =   661
         mask            =   ""
         text            =   "0,000"
         locked          =   0   'False
         enabled         =   -1  'True
      End
      Begin MSMask.MaskEdBox mskNumCarta 
         Height          =   285
         Left            =   1440
         TabIndex        =   5
         Top             =   3240
         Visible         =   0   'False
         Width           =   1215
         _ExtentX        =   2143
         _ExtentY        =   503
         _Version        =   393216
         MaxLength       =   9
         Mask            =   "#########"
         PromptChar      =   "_"
      End
      Begin MSFlexGridLib.MSFlexGrid grdEstimativasPagamentos_Coberturas 
         Height          =   2145
         Left            =   240
         TabIndex        =   13
         Top             =   960
         Width           =   8775
         _ExtentX        =   15478
         _ExtentY        =   3784
         _Version        =   393216
         Cols            =   3
         FixedCols       =   0
         AllowBigSelection=   0   'False
         FormatString    =   $"frmEstimativa.frx":0097
      End
      Begin VB.Label lblNumCarta 
         AutoSize        =   -1  'True
         Caption         =   "N�m. da Carta:"
         Height          =   195
         Left            =   240
         TabIndex        =   12
         Top             =   3240
         Visible         =   0   'False
         Width           =   1215
      End
      Begin VB.Label lblPercER 
         AutoSize        =   -1  'True
         Caption         =   "% ER:"
         Height          =   195
         Left            =   8280
         TabIndex        =   18
         Top             =   3240
         Visible         =   0   'False
         Width           =   435
      End
      Begin VB.Label lblPercResseguro 
         Caption         =   "% Quota:"
         Height          =   255
         Left            =   6555
         TabIndex        =   17
         Top             =   3240
         Visible         =   0   'False
         Width           =   975
      End
      Begin VB.Label lblValorPrejuizo 
         Caption         =   "Valor Preju�zo Informado:"
         Height          =   255
         Left            =   7170
         TabIndex        =   15
         Top             =   300
         Width           =   1815
      End
      Begin VB.Label lblCobertura 
         Caption         =   "Selecione uma Cobertura:"
         Height          =   255
         Left            =   270
         TabIndex        =   14
         Top             =   300
         Width           =   1935
      End
   End
End
Attribute VB_Name = "frmEstimativa"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
'Conexao
Private lConexaoLocal           As Integer
Private bytModoOperacao         As Byte    '1-Inclusao / 2-Alteracao / 3-Consulta

Private sOperacaoCoseguro       As String
Private dSinistro_ID            As String
Private SinDT_Ocorrencia        As String
Private vTpComponente           As String
Private SinApolice_Id           As String
'Private SinProposta_Id         As String
Private SinTpComponenteId       As String
Private SinSeguradora_Id        As String
Private SinSucursal_Id          As String
Private vProduto                As String
Private SinCliente_Id           As String
Private SinEndosso_Id           As String
Private SinSituacao_id          As String

'C00216281-ESTEIRA-PAGAMENTO-IMEDIATO-VIDA-INDIVIDUAL
Private SinEvento_sinistro_id As Integer
Private SinRamo_Id            As Integer
Private PgtoDt_aviso           As String
Private PgtoImediatoSituacao  As Integer
Private PgtoTp_sinistro_parametro_id As Integer
'C00216281 - FIM


Private gridRow                 As Integer
Private gridCol                 As Integer
Private nSeq_Estimativa         As Long
Private nSeq_Estimativa_Ultima  As Long
Private nSeq_Estima_Prox        As Long
'Private nCorrecao              As Double

Private sAdicionar              As Boolean
Private sAlterar                As Boolean
Private sExcluir                As Boolean
Private Tecnico_Atual           As String

'Controlam os valores da estimativa anterior
Private vValorAnt(7)            As Double
Private vResseguroAnt(7)        As Double
Private vCalcRessAnt(7)         As Double
Private vSaldoAnt(7)            As Double
Private vPercResseguroAnt       As Double
Private vPercERAnt              As Double
Public vErroEstimativa          As Boolean

Public COB_GENERICA_SEGUR       As Integer

Private bolDefiniu_Operacao_Cosseguro           As Boolean  'AKIO.OKUNO - MP - 14/02/2013
Private bytGTR_Implantado                       As Byte     'AKIO.OKUNO - 06/05/2013
Private sSinistro_BB                            As String   'AKIO.OKUNO - 07/05/2013

Private Function obter_valor_is(ByVal proposta_id As Long, ByVal produto_id As Long, ByVal cobertura_id As Long) As Double
'+-----------------------------------------------------------
'| 19406709 - Melhoria nos limites de sinistro e emissao
'| RN04 - petrauskas - 03/2017
'| somente para produtos afetados pela demanda


  Dim rs As ADODB.Recordset
  Dim sSQL As String
  Dim sTabela As String
  Dim vIS As Double
  Dim bAux As Boolean
  Dim dt_aviso_sinistro As Date
  
  vIS = 0
  bAux = False
  dt_aviso_sinistro = Empty
  
  Select Case produto_id
    Case 8
      'sSQL = "SELECT val_is FROM seguros_db.dbo.escolha_tp_cob_maq_tb WITH (NOLOCK) WHERE proposta_id = " & proposta_id & " AND tp_cobertura_id = " & cobertura_id & " AND dt_fim_vigencia_esc IS NULL"
      sTabela = "seguros_db.dbo.escolha_tp_cob_maq_tb"
      '+---------------------------------------------------------------------------------------------
      '|00421597-seguro-faturamento-pecuario -ADICIONADO NOVO PRODUTO "1240" NO CASE ABAIXO |06/06/18|
          Case 155, 156, 227, 228, 229, 300, 650, 680, 701, 1152, 1201, 1204, 1226, 1227, 1240
      'sSQL = "SELECT val_is FROM seguros_db.dbo.escolha_tp_cob_generico_tb WITH (NOLOCK) WHERE proposta_id = " & proposta_id & " AND tp_cobertura_id = " & cobertura_id & " AND dt_fim_vigencia_esc IS NULL"
      sTabela = "seguros_db.dbo.escolha_tp_cob_generico_tb"
      Call CarregaDadosValores_PercentualResseguro 'Chamado IM00168720 - Cleber Sardo - Data 26.12.2017 - Corrigindo a consulta do valor da IS - Solicitado por Rodrigo de Souza Robalo
    Case 1210
      'sSQL = "SELECT val_is FROM seguros_db.dbo.escolha_tp_cob_avulso_tb WITH (NOLOCK) WHERE proposta_id = " & proposta_id & " AND tp_cobertura_id = " & cobertura_id & " AND dt_fim_vigencia_esc IS NULL"
      sTabela = "seguros_db.dbo.escolha_tp_cob_avulso_tb"
    Case Else
      'sSQL = "SELECT val_is FROM seguros_db.dbo.escolha_tp_cob_generico_tb WITH (NOLOCK) WHERE proposta_id = " & proposta_id & " AND tp_cobertura_id = " & cobertura_id & " AND dt_fim_vigencia_esc IS NULL"
      sTabela = "seguros_db.dbo.escolha_tp_cob_generico_tb"
  End Select

  'Chamado IM00168720 - Cleber Sardo - Data 26.12.2017 - Corrigindo a consulta do valor da IS - - Solicitado por Rodrigo de Souza Robalo
  If gblsCod_Objeto_Segurado <> "" Then
    sSQL = "SELECT val_is FROM " & sTabela & " WITH (NOLOCK) WHERE proposta_id = " & proposta_id & " AND tp_cobertura_id = " & cobertura_id & " AND dt_fim_vigencia_esc IS NULL and cod_objeto_segurado = " & gblsCod_Objeto_Segurado & ""
  Else
    sSQL = "SELECT val_is FROM " & sTabela & " WITH (NOLOCK) WHERE proposta_id = " & proposta_id & " AND tp_cobertura_id = " & cobertura_id & " AND dt_fim_vigencia_esc IS NULL"
  End If
  Set rs = Conexao_ExecutarSQL(gsSIGLASISTEMA, _
                               glAmbiente_id, _
                               App.Title, _
                               App.FileDescription, _
                               sSQL, _
                               lConexaoLocal, _
                               True)
  
  If Not rs.EOF Then
    If Not IsNull(rs("val_IS")) Then
      vIS = CDbl("0" & rs("val_IS"))
      bAux = True
    End If
  End If
  rs.Close
  
  If Not bAux Then
    sSQL = "SELECT E.val_is, E.dt_fim_vigencia_esc, S.dt_ocorrencia_sinistro " & vbCrLf _
         & "FROM " & sTabela & " E WITH (NOLOCK) " & vbCrLf _
         & "INNER JOIN (SELECT proposta_id, MAX(num_endosso) as num_endosso " & vbCrLf _
         & "            FROM " & sTabela & " WITH (NOLOCK) " & vbCrLf _
         & "            WHERE proposta_id = " & proposta_id & " " & vbCrLf _
         & "            GROUP BY proposta_id) M ON E.proposta_id = M.proposta_id " & vbCrLf _
         & "                                   AND E.num_endosso = M.num_endosso " & vbCrLf _
         & "INNER JOIN seguros_db.dbo.sinistro_tb S ON E.proposta_id = S.proposta_id " & vbCrLf _
         & "                                       AND S.sinistro_id = " & gbldSinistro_ID & " " & vbCrLf _
         & "WHERE E.tp_cobertura_id = " & cobertura_id
  
    Set rs = Conexao_ExecutarSQL(gsSIGLASISTEMA, _
                                 glAmbiente_id, _
                                 App.Title, _
                                 App.FileDescription, _
                                 sSQL, _
                                 lConexaoLocal, _
                                 True)
  
    If Not rs.EOF Then
      If Not IsNull(rs("val_IS")) And Not IsNull(rs("dt_fim_vigencia_esc")) And Not IsNull(rs("dt_ocorrencia_sinistro")) Then
        If rs("dt_fim_vigencia_esc") > rs("dt_ocorrencia_sinistro") Then
          vIS = CDbl("0" & rs("val_IS"))
        End If
      End If
      bAux = True
    
    End If
    rs.Close
  End If
  
  If Not bAux And InStr(",155,156,227,650,680,1152,1201,1226,1227,", "," & produto_id & ",") > 0 Then
    sSQL = "SELECT SUM(val_is) AS val_is FROM seguros_db.dbo.escolha_tp_cob_avulso_tb WITH (NOLOCK) WHERE proposta_id = " & proposta_id & " AND tp_cobertura_id = " & cobertura_id & " AND dt_fim_vigencia_esc IS NULL"
    Set rs = Conexao_ExecutarSQL(gsSIGLASISTEMA, _
                                 glAmbiente_id, _
                                 App.Title, _
                                 App.FileDescription, _
                                 sSQL, _
                                 lConexaoLocal, _
                                 True)
    
    If Not rs.EOF Then
      If Not IsNull(rs("val_IS")) Then
        vIS = CDbl("0" & rs("val_IS"))
        bAux = True
      End If
    End If
    rs.Close
  
    If Not bAux Then
      sTabela = "seguros_db.dbo.escolha_tp_cob_avulso_tb"
      
      sSQL = "SELECT E.val_is, E.dt_fim_vigencia_esc, S.dt_ocorrencia_sinistro " & vbCrLf _
           & "FROM " & sTabela & " E WITH (NOLOCK) " & vbCrLf _
           & "INNER JOIN (SELECT proposta_id, MAX(num_endosso) as num_endosso " & vbCrLf _
           & "            FROM " & sTabela & " WITH (NOLOCK) " & vbCrLf _
           & "            WHERE proposta_id = " & proposta_id & " " & vbCrLf _
           & "            GROUP BY proposta_id) M ON E.proposta_id = M.proposta_id " & vbCrLf _
           & "                                   AND E.num_endosso = M.num_endosso " & vbCrLf _
           & "INNER JOIN seguros_db.dbo.sinistro_tb S ON E.proposta_id = S.proposta_id " & vbCrLf _
           & "                                       AND S.sinistro_id = " & gbldSinistro_ID & " " & vbCrLf _
           & "WHERE E.tp_cobertura_id = " & cobertura_id
    
      Set rs = Conexao_ExecutarSQL(gsSIGLASISTEMA, _
                                   glAmbiente_id, _
                                   App.Title, _
                                   App.FileDescription, _
                                   sSQL, _
                                   lConexaoLocal, _
                                   True)
    
      If Not rs.EOF Then
        If Not IsNull(rs("val_IS")) And Not IsNull(rs("dt_fim_vigencia_esc")) And Not IsNull(rs("dt_ocorrencia_sinistro")) Then
          If rs("dt_fim_vigencia_esc") > rs("dt_ocorrencia_sinistro") Then
            vIS = CDbl("0" & rs("val_IS"))
          End If
        End If
        bAux = True
      
      End If
      rs.Close
    
    End If
  
  End If
  
  Set rs = Nothing
  
  obter_valor_is = vIS

End Function

Private Sub cmbCoberturaAfetada_Click()
    On Error GoTo TrataErro
    
    Dim rsRecordSet As ADODB.Recordset
    Dim sSQL As String
    
    If sOperacaoCoseguro <> "C" Then
        If glAmbiente_id = 6 Or glAmbiente_id = 7 Or glAmbiente_id = 8 Then
            If cmbCoberturaAfetada.ListIndex <> -1 Then
                If nSeq_Estimativa = 1 Then
                            
                                    sSQL = " DECLARE @ramo_id INT" & vbNewLine & vbNewLine
                    sSQL = sSQL & " SELECT @ramo_id = s.ramo_id" & vbNewLine
                    sSQL = sSQL & " FROM #Sinistro_Principal s" & vbNewLine & vbNewLine
                    sSQL = sSQL & " SELECT cm.val_custo_medio" & vbNewLine
                    sSQL = sSQL & " FROM seguros_db.dbo.custo_medio_tb cm with(NOLOCK)" & vbNewLine
                    sSQL = sSQL & " WHERE cm.produto_id = " & vProduto & vbNewLine
                    sSQL = sSQL & " AND cm.ramo_id = @ramo_id" & vbNewLine
                    sSQL = sSQL & " AND cm.tp_cobertura_id = " & cmbCoberturaAfetada.ItemData(cmbCoberturaAfetada.ListIndex) & vbNewLine
                    sSQL = sSQL & " AND cm.dt_fim_vigencia IS NULL" & vbNewLine
                    
                    Set rsRecordSet = Conexao_ExecutarSQL(gsSIGLASISTEMA, _
                                                          glAmbiente_id, _
                                                          App.Title, _
                                                          App.FileDescription, _
                                                          sSQL, _
                                                          lConexaoLocal, _
                                                          True)
                                                          
                    If Not rsRecordSet.EOF Then
                        If Not IsNull(rsRecordSet!val_custo_medio) Then
                            mskValorPrejuizo.Text = Format(rsRecordSet!val_custo_medio, "#,##0.00")
                        End If
                    Else
                        mskValorPrejuizo.Text = Format(0#, "#,##0.00")
                    End If
                End If
            End If
        End If
    End If

    
    Exit Sub
    
TrataErro:
    Call TrataErroGeral("cmbCoberturaAfetada_Click", "frmEstimativa")
    Call FinalizarAplicacao
End Sub

'Private vIndice                As Integer


'Public Const strTipoProcesso_Avisar = 1          '"I"
'Public Const intSinistro_Avaliar = 2             '"AV"
'Public Const intSinistro_Consulta = 3            '"C"
'Public Const intSinistro_Reabrir = 4             '"R"
'Public Const intSinistro_Encerrar = 5            '"E"

'Collections
'Private cDados_Estimativa     As New Collection

Private Sub Form_Load()
    On Error GoTo TrataErro
    
    gbldSinistro_ID = 93202000105#
    bytModoOperacao = 1
    glAmbiente_id = 3
    gbllProposta_ID = 24217980
    dSinistro_ID = 93202000105#
    strTipoProcesso = 2 'strTipoProcesso_Avisar
    SinEndosso_Id = 0
    cUserName = "C00216281"
    
    bytTipoRamo = bytTipoRamo_Vida
    Data_Sistema = Now()
    
    InicializaVariaveisLocal

    IniciarConexao

    InicializaInterfaceLocal
    
    InicializaInterface_Versao Me
    
    Exit Sub

TrataErro:
    Call TrataErroGeral("FormLoad", "frmEstimativa")
    Call FinalizarAplicacao
End Sub

Private Sub InicializaVariaveisLocal()
    Dim sSQL As String
    Dim aParametros() As String
    Dim aParametros1() As String
    Dim bItemParametro As Byte
    Dim sParametroLocal As String
    Dim sParametro1 As String

    On Error GoTo TrataErro

    If Not Trata_Parametros(Command) Or InStr(1, Command, " ") = 0 Then
        'FinalizarAplicacao    'cristovao.rodrigues 06/06 descomentar
    Else
        '        cUserName = "MATHAYDE" 'C_JOSBARROS
        '        glAmbiente_id = 3
        '        sParametroLocal = Command

        Ambiente = ConvAmbiente(glAmbiente_id)
        'Exemplo de Command: 1,1,93201010872,369118 ,19,87,113,173,225,188,213,134,222,105,38,15,90,48,3,168,151,191,235,131,247,16,96,3,85,57,65,164,214,179
        ' 1,3,93200907477,29  -- ramo, operacao, sinistro, estimativa
        sParametroLocal = Left(Command, InStrRev(Command, " "))

        'Separa o primeiro bloco do Command, antes do espa�o: 1,6,93201205103,442119
        aParametros = Split(sParametroLocal, ".")

        '        'Separa os par�metros do primeiro par�metro do primeiro bloco: 1.6 = (0)1 e (1)6
        '        sParametro1 = aParametros(0)
        '        aParametro1 = Split(sParametro1, ".")

        bytTipoRamo = aParametros(0)
        gsParamAplicacao = aParametros(1)
        sOperacao = gsParamAplicacao
        bytModoOperacao = Trim(gsParamAplicacao)
        'strTipoProcesso = sOperacao


        If UBound(aParametros) >= 2 Then
            dSinistro_ID = aParametros(2)
        Else
            dSinistro_ID = 0
        End If

        gbldSinistro_ID = dSinistro_ID

        'parametro recebe o numero da estimativa que devera ser visualizada
        If UBound(aParametros) >= 3 Then
            If aParametros(3) = " " Or aParametros(3) = "" Then    'cristovao.rodrigues 27/09/2012
                nSeq_Estimativa = 0
            Else
                nSeq_Estimativa = aParametros(3)
            End If
            'nSeq_Estimativa = IIf(sOperacao = 1, 0, IIf(aParametros(3) = " ", 0, aParametros(3)))
        Else
            nSeq_Estimativa = 0
        End If

        '        If nSeq_Estimativa = 0 And sOperacao = 3 Then
        '            MsgBox "Problema para leitura do parametro recebido."
        '            GoTo TrataErro
        '        End If

        'cristovao.rodrigues 27/09/2012
        If UBound(aParametros) >= 4 Then
            strTipoProcesso = Trim(aParametros(4))
        End If

'AKIO.OKUNO - MP - INICIO - 14/02/2013
        If UBound(aParametros) >= 5 Then
            Data_Sistema = Trim(aParametros(5))
        Else
            Data_Sistema = ""
        End If

        If UBound(aParametros) >= 6 Then
            sOperacaoCosseguro = Trim(aParametros(6))
            bolDefiniu_Operacao_Cosseguro = True
        Else
            bolDefiniu_Operacao_Cosseguro = False
        End If
        
        If UBound(aParametros) >= 7 Then
            vProduto = Trim(aParametros(7))
        Else
            vProduto = ""
        End If
        
'AKIO.OKUNO - INICIO - 06/05/2013
        If UBound(aParametros) >= 8 Then
            bytGTR_Implantado = Trim(aParametros(8))
        Else
            bytGTR_Implantado = 2
        End If
        
        If UBound(aParametros) >= 9 Then
            dSinistro_BB = Trim(aParametros(9))
        Else
            dSinistro_BB = ""
        End If
        
        'MU-2018-052406- Ajuste autom�tico das Reservas � SEGBR (Victor Fonseca)
        'Verifica o par�metro de cosseguro
        If UBound(aParametros) >= 10 Then
            sOperacaoCoseguro = Trim(aParametros(10))
        Else
            sOperacaoCoseguro = ""
        End If
'AKIO.OKUNO - FIM - 06/05/2013
'
'        Data_Sistema = "15/02/2013"
'        sOperacaoCosseguro = ""
'        bolDefiniu_Operacao_Cosseguro = True
'        vProduto = 109
'
'AKIO.OKUNO - MP - FIM - 14/02/2013
        
        sAlterar = False
        sAdicionar = False
        sExcluir = False

        SinEndosso_Id = 0
        Tecnico_Atual = "0"
        COB_GENERICA_SEGUR = 1
        
        'MU-2018-052406- Ajuste autom�tico das Reservas � SEGBR (Victor Fonseca)
        'Verifica as condi��es para exibi��o do label referente ao custo m�dio
        If sOperacaoCoseguro <> "C" Then
            If glAmbiente_id = 6 Or glAmbiente_id = 7 Or glAmbiente_id = 8 Then
                lblValorPrejuizo.Caption = "Valor do Custo M�dio:"
            End If
        End If

                

    End If

    Exit Sub
    
TrataErro:
    Call TrataErroGeral("IniciolizavariaveisLocal", "frmEstimativa")
    Call FinalizarAplicacao
End Sub

Private Sub IniciarConexao()
    On Error GoTo Erro

    Dim vNunCx As Integer    'cristovao.rodrigues retirar
    vNunCx = 1

    '    If EstadoConexao(vNunCx) = adStateClosed Then
    lConexaoLocal = AbrirConexao(gsSIGLASISTEMA, glAmbiente_id, App.Title, App.FileDescription)
    '    End If

    'lConexaoLocal = vNunCx

    Exit Sub

Erro:
    Call TrataErroGeral("IniciarConexao", "frmEstimativa")
    Call FinalizarAplicacao

End Sub

Private Sub InicializaInterfaceLocal()

    On Error GoTo TrataErro

    Screen.MousePointer = vbHourglass

    If ValidaSegurancaLocal Then

        Call CentraFrm(Me)

        If Data_Sistema = "" Then   'AKIO.OKUNO - MP - 14/02/2013
            BuscaStatusSistema
        End If                      'AKIO.OKUNO - MP - 14/02/2013

        InicializaInterfaceLocal_Form 'cristovao.rodrigues 04/04/2013 troca de lugar

        LimpaCamposTelaLocal Me                                         'SERA ALTERADO

'        CriaTemporaria             'FLAVIO.BEZERRA - 07/03/2013 - 'AKIO.OKUNO / F.BEZERRA - RETIRADO - 12/03/2013

        CarregaDadosEstimativa                                        'FLAVIO.BEZERRA - 07/03/2013 - PASSOU A UTILIZAR A CARGA DE COBERTURA DO MODULO.
'        SelecionaDados_AbaEstimativa_CoberturaAfetada lConexaoLocal    'FLAVIO.BEZERRA - 07/03/2013 - PASSOU A UTILIZAR A CARGA DE COBERTURA DO MODULO. 'AKIO.OKUNO / F.BEZERRA - RETIRADO - 12/03/2013
        
        CarregaDadosSinistro

        Monta_cmbCoberturaAfetada

        CarregaDadosGrid_AbaEstimativasPagamentos_Valores 1

        CarregaDadosGrid_AbaEstimativasPagamentos_Valores 2

        CarregaDadosGrid_EnderecoRisco                                  'AKIO.OKUNO - 08/10/2012

        InicializaModoOperacaoLocal

        'InicializaInterfaceLocal_Form 'cristovao.rodrigues 04/04/2013 verificar esta apagando valores mskPercResseguro

        InicializaInterfaceLocal_Grid_AbaEstimativasPagamentos

        'cristovao.rodrigues 27/09/2012
        '        RetornaValorPercentualResseguro        'AKIO.OKUNO - 08/10/2012

    Else

        FinalizarAplicacao

    End If

    Screen.MousePointer = vbNormal

    Exit Sub

TrataErro:
    Call TrataErroGeral("InicializaInterface", "frmEstimativa")
    Call FinalizarAplicacao

End Sub

Private Function ValidaSegurancaLocal() As Boolean
'ValidaSegurancaLocal = Seguranca("SEGP1287", "Consulta de Estimativas")
    ValidaSegurancaLocal = True
End Function

Private Sub InicializaInterfaceLocal_Form()
    Dim sTipoProcesso_Descricao As String

    Select Case bytModoOperacao
    Case 1
        btnOK_Estimativa.Visible = True
        'btnCoberturaRemover.Enabled = False possibilida de excluir uma cobertura incluida
        btnVoltar_Estimativa.Caption = "Cancelar"
        'grdEstimativasPagamentos_Estimativas.Enabled = False
        sTipoProcesso_Descricao = "Manuten��o"
    Case 2
        btnOK_Estimativa.Visible = True
        'btnCoberturaAdicionar.Enabled = False
        btnCoberturaRemover.Enabled = True
        btnVoltar_Estimativa.Caption = "Cancelar"
        sTipoProcesso_Descricao = "Manuten��o"
    Case 3
        btnOK_Estimativa.Visible = False
        btnCoberturaAdicionar.Enabled = False
        btnCoberturaAlterar.Enabled = False
        btnCoberturaRemover.Enabled = False
        grdEstimativasPagamentos_Estimativas.Enabled = False
        sTipoProcesso_Descricao = "Consulta"
    End Select

    'F.BEZERRA - INICIO - 19/10/2012
    'AKIO.OKUNO - INICIO - 08/10/2012
    '    Select Case bytTipoRamo
    '        Case bytTipoRamo_Vida
    '            With fmeEnderecoRisco
    '                .Visible = False
    '                Me.Height = 6915
    '            End With
    '            frameValores.Top = 120
    '
    '        Case bytTipoRamo_RE, bytTipoRamo_Rural
    '            If Me.Height < 7000 Then
    '                With fmeEnderecoRisco
    '                    .Visible = True
    '                    .Top = 120
    '                    frameValores.Top = .Top + .Height + 120
    '                    Me.Height = Me.Height + .Height + 120
    '                End With
    '            End If
    '    End Select
    'AKIO.OKUNO - FIM - 08/10/2012
    'F.BEZERRA - FIM - 19/10/2012

    Text1.Visible = False
    txtAuxiliar.Text = "Cancelado"
    txtAuxiliar.Visible = False

    'cristovao.rodrigues ativar botoes
    '    btnCoberturaAlterar.Enabled = False
    '    btnCoberturaRemover.Enabled = False
    '    btnCoberturaAdicionar.Enabled = False
    '    btnOK_Estimativa.Enabled = False

    mskValorPrejuizo.mask = "9,2" 'NTendencia
    mskValorPrejuizo.Text = "0,00"
    mskPercResseguro.Text = "0,00"
    mskPercER.Text = "0,00"

    mskPercResseguro.Enabled = False
    mskPercER.Enabled = False

    'strTipoProcesso

    If sOperacaoCosseguro = "C" Then
        lblNumCarta.Visible = True
        mskNumCarta.Visible = True
    Else
        lblNumCarta.Visible = False
        mskNumCarta.Visible = False
    End If

    Me.Caption = App.EXEName & " - " & sTipoProcesso_Descricao & " Estimativa - " & Ambiente
    
    Ambiente = "/* " & Ambiente & " */ "   'AKIO.OKUNO - 07/05/2013
    
    InicializaInterface_FormCaption frmEstimativa, bytTipoRamo, gbldSinistro_ID          'AKIO.OKUNO - 07/05/2013

    Me.Caption = Mid(Me.Caption, 1, Len(Trim(Me.Caption)) - 1) & " - Estimativa " & nSeq_Estimativa & ")" 'AKIO.OKUNO - 07/05/2013"
End Sub

Private Sub InicializaModoOperacaoLocal()
    On Error GoTo Erro

    '1-Inclusao / 2-Altera��o / 3-Consulta
    InicializaModoOperacao Me, bytModoOperacao

    Exit Sub

Erro:
    Call TrataErroGeral("InicializaModoOperacao", "frmEstimativa")
    Call FinalizarAplicacao

End Sub
'AKIO.OKUNO - CriaTemporaria - 06/05/2013
'PROCEDURE SUBSTITUIDA PELA CarregaDadosEstimativa
'Private Sub CriaTemporaria()
'
'    Dim sSQL As String
'
'    On Error GoTo TrataErro
'
'    'FLAVIO.BEZERRA - INICIO - 07/03/2013
''    sSQL = sSQL & "Create Table #EstimativaPagamento_CoberturaAfetada                                                                           " & vbNewLine
''    sSQL = sSQL & "     ( Tp_Cobertura_ID                               Int                                                                     " & vbNewLine
''    sSQL = sSQL & "     , Correcao                                      Numeric(15,2)                                                           " & vbNewLine
''    sSQL = sSQL & "     , Estimado                                      Numeric(15,2)                                                           " & vbNewLine
''    sSQL = sSQL & "     , Tp_Cobertura_Nome                             VarChar(60)                                                             " & vbNewLine
''    sSQL = sSQL & "     , Tipo                                          VarChar(1)                                                              " & vbNewLine
''    sSQL = sSQL & "  )                                                                                                                          " & vbNewLine
'    sSQL = sSQL & "Select sinistro_id = " & gbldSinistro_ID & vbNewLine
'    sSQL = sSQL & "  Into #Sinistro_Principal " & vbNewLine
'    'FLAVIO.BEZERRA - FIM - 07/03/2013                                                                                                         " & vbNewLine
'
'    sSQL = sSQL & vbCrLf
'
'    sSQL = sSQL & "Create table #EstimativaPagamento_Estimativa                                                                                 " & vbNewLine
'    sSQL = sSQL & "     ( Dt_Inicio_Estimativa                          SmallDateTime                                                           " & vbNewLine
'    sSQL = sSQL & "     , Dt_Fim_Estimativa                             SmallDateTime                                                           " & vbNewLine
'    sSQL = sSQL & "     , Item_Val_Estimativa                           TinyInt                                                                 " & vbNewLine
'    sSQL = sSQL & "     , Seq_Estimativa                                TinyInt                                                                 " & vbNewLine
'    sSQL = sSQL & "     , Val_Estimado                                  Numeric(15,2)                                                           " & vbNewLine
'    sSQL = sSQL & "     , Val_Pago                                      Numeric(15,2)                                                           " & vbNewLine
'    sSQL = sSQL & "     , Val_Resseguro                                 Numeric(15,2)                                                           " & vbNewLine
'    sSQL = sSQL & "     , Situacao                                      Char(1)                                                                 " & vbNewLine
'    sSQL = sSQL & "     , Usuario                                       VarChar(20)                                                             " & vbNewLine
'    sSQL = sSQL & "     , Num_Carta_Seguro_Aceito                       Int                                                                     " & vbNewLine
'    sSQL = sSQL & "     , Perc_Re_Seguro_Quota                          Numeric(9,6)                                                            " & vbNewLine
'    sSQL = sSQL & "     , Perc_Re_Seguro_Er                             Numeric(9,6)                                                            " & vbNewLine
'    sSQL = sSQL & "     , Tipo                                          Char(1)                                                                 " & vbNewLine
'    sSQL = sSQL & "     , Nossa_Parte                                   Numeric(9,6)                                                            " & vbNewLine
'    sSQL = sSQL & "     , Dt_Inclusao                                   SmallDateTime                                                           " & vbNewLine
'    sSQL = sSQL & "     , CountEstimativa                               Numeric(3)                                                              " & vbNewLine
'    sSQL = sSQL & "     , Moeda_ID                                      Numeric(3)                                                              " & vbNewLine           '-- Sinistro_Tb (Estimativa)
'    sSQL = sSQL & "     , Moeda_Sigla                                   VarChar(4)                                                              " & vbNewLine           '-- Moeda_Tb (Estimativa)
'    sSQL = sSQL & "     , pgto_parcial_co_seguro                        Char(1)                                                                 " & vbNewLine           '-- Sinistro_Tb.pgto_parcial_co_seguro
'    sSQL = sSQL & "     )                                                                                                                       " & vbNewLine
'    sSQL = sSQL & "Create Index PK_Dt_Inicio_Estimativa             on #EstimativaPagamento_Estimativa (Seq_Estimativa, Dt_Inicio_Estimativa)   " & vbNewLine
'
'    'cristovao.rodrigues 03/09/2012
'    sSQL = sSQL & vbCrLf
'    sSQL = sSQL & " create table #Estimativa_Evento " & vbCrLf
'    sSQL = sSQL & " (dt_evento                  smalldatetime, " & vbCrLf
'    sSQL = sSQL & " situacao                    char(1), " & vbCrLf
'    sSQL = sSQL & " evento_SEGBR                varchar(250), " & vbCrLf
'    sSQL = sSQL & " descricao                   varchar(250), " & vbCrLf
'    sSQL = sSQL & " tipo                        char(1), " & vbCrLf
'    sSQL = sSQL & " usuario                     varchar(250), " & vbCrLf
'    sSQL = sSQL & " seq_evento                  tinyint, " & vbCrLf
'    sSQL = sSQL & " localizacao                 tinyint) " & vbCrLf
'
'    Call Conexao_ExecutarSQL(gsSIGLASISTEMA, _
'                             glAmbiente_id, _
'                             App.Title, _
'                             App.FileDescription, _
'                             sSQL, _
'                             lConexaoLocal, _
'                             False)
'
'
'    Exit Sub
'
'TrataErro:
'    Call TrataErroGeral("CriaTemporaria", "frmEstimativa")
'    Call FinalizarAplicacao
'
'End Sub
'AKIO.OKUNO - CarregaDadosEstimativa (REFORMULA��O) - 06/05/2013
'Private Sub CarregaDadosEstimativa()
'
'    Dim sSQL As String
'
'    On Error GoTo TrataErro
'
''AKIO.OKUNO - MP - INICIO - 15/02/2013
'    sSQL = ""
''AKIO.OKUNO / F.BEZERRA - INICIO - 12/03/2013
''    sSQL = sSQL & "Create Table #EstimativaPagamento_CoberturaAfetada                                                                           " & vbNewLine
''    sSQL = sSQL & "     ( Tp_Cobertura_ID                               Int                                                                     " & vbNewLine
''    sSQL = sSQL & "     , Correcao                                      Numeric(15,2)                                                           " & vbNewLine
''    sSQL = sSQL & "     , Estimado                                      Numeric(15,2)                                                           " & vbNewLine
''    sSQL = sSQL & "     , Tp_Cobertura_Nome                             VarChar(60)                                                             " & vbNewLine
''    sSQL = sSQL & "     , Tipo                                          VarChar(1)                                                              " & vbNewLine
''    sSQL = sSQL & "  )                                                                                                                          " & vbNewLine
''
''    sSQL = sSQL & vbCrLf
'    sSQL = sSQL & "Select sinistro_id = " & gbldSinistro_ID & vbNewLine
'    sSQL = sSQL & "  Into #Sinistro_Principal " & vbNewLine
''AKIO.OKUNO / F.BEZERRA - FIM - 12/03/2013
'
'    sSQL = sSQL & "Create table #EstimativaPagamento_Estimativa                                                                                 " & vbNewLine
'    sSQL = sSQL & "     ( Dt_Inicio_Estimativa                          SmallDateTime                                                           " & vbNewLine
'    sSQL = sSQL & "     , Dt_Fim_Estimativa                             SmallDateTime                                                           " & vbNewLine
'    sSQL = sSQL & "     , Item_Val_Estimativa                           TinyInt                                                                 " & vbNewLine
'    sSQL = sSQL & "     , Seq_Estimativa                                TinyInt                                                                 " & vbNewLine
'    sSQL = sSQL & "     , Val_Estimado                                  Numeric(15,2)                                                           " & vbNewLine
'    sSQL = sSQL & "     , Val_Pago                                      Numeric(15,2)                                                           " & vbNewLine
'    sSQL = sSQL & "     , Val_Resseguro                                 Numeric(15,2)                                                           " & vbNewLine
'    sSQL = sSQL & "     , Situacao                                      Char(1)                                                                 " & vbNewLine
'    sSQL = sSQL & "     , Usuario                                       VarChar(20)                                                             " & vbNewLine
'    sSQL = sSQL & "     , Num_Carta_Seguro_Aceito                       Int                                                                     " & vbNewLine
'    sSQL = sSQL & "     , Perc_Re_Seguro_Quota                          Numeric(9,6)                                                            " & vbNewLine
'    sSQL = sSQL & "     , Perc_Re_Seguro_Er                             Numeric(9,6)                                                            " & vbNewLine
'    sSQL = sSQL & "     , Tipo                                          Char(1)                                                                 " & vbNewLine
'    sSQL = sSQL & "     , Nossa_Parte                                   Numeric(9,6)                                                            " & vbNewLine
'    sSQL = sSQL & "     , Dt_Inclusao                                   SmallDateTime                                                           " & vbNewLine
'    sSQL = sSQL & "     , CountEstimativa                               Numeric(3)                                                              " & vbNewLine
'    sSQL = sSQL & "     , Moeda_ID                                      Numeric(3)                                                              " & vbNewLine           '-- Sinistro_Tb (Estimativa)
'    sSQL = sSQL & "     , Moeda_Sigla                                   VarChar(4)                                                              " & vbNewLine           '-- Moeda_Tb (Estimativa)
'    sSQL = sSQL & "     , pgto_parcial_co_seguro                        Char(1)                                                                 " & vbNewLine           '-- Sinistro_Tb.pgto_parcial_co_seguro
'    sSQL = sSQL & "     )                                                                                                                       " & vbNewLine
'    sSQL = sSQL & "Create Index PK_Dt_Inicio_Estimativa             on #EstimativaPagamento_Estimativa (Seq_Estimativa, Dt_Inicio_Estimativa)   " & vbNewLine
'
'    sSQL = sSQL & vbCrLf
'    sSQL = sSQL & " create table #Estimativa_Evento " & vbCrLf
'    sSQL = sSQL & " (dt_evento                  smalldatetime, " & vbCrLf
'    sSQL = sSQL & " situacao                    char(1), " & vbCrLf
'    sSQL = sSQL & " evento_SEGBR                varchar(250), " & vbCrLf
'    sSQL = sSQL & " descricao                   varchar(250), " & vbCrLf
'    sSQL = sSQL & " tipo                        char(1), " & vbCrLf
'    sSQL = sSQL & " usuario                     varchar(250), " & vbCrLf
'    sSQL = sSQL & " seq_evento                  tinyint, " & vbCrLf
'    sSQL = sSQL & " localizacao                 tinyint) " & vbCrLf
'    sSQL = sSQL & vbCrLf
''AKIO.OKUNO - MP - FIM - 15/02/2013
'
'    sSQL = sSQL & "Insert into #EstimativaPagamento_Estimativa                                                                                  " & vbNewLine
'    sSQL = sSQL & "     ( Dt_Inicio_Estimativa                                                                                                  " & vbNewLine
'    sSQL = sSQL & "     , Dt_Fim_Estimativa                                                                                                     " & vbNewLine
'    sSQL = sSQL & "     , Item_Val_Estimativa                                                                                                   " & vbNewLine
'    sSQL = sSQL & "     , Seq_Estimativa                                                                                                        " & vbNewLine
'    sSQL = sSQL & "     , Val_Estimado                                                                                                          " & vbNewLine
'    sSQL = sSQL & "     , Val_Pago                                                                                                              " & vbNewLine
'    sSQL = sSQL & "     , Val_Resseguro                                                                                                         " & vbNewLine
'    sSQL = sSQL & "     , Situacao                                                                                                              " & vbNewLine
'    sSQL = sSQL & "     , Usuario                                                                                                               " & vbNewLine
'    sSQL = sSQL & "     , Num_Carta_Seguro_Aceito                                                                                               " & vbNewLine
'    sSQL = sSQL & "     , Perc_Re_Seguro_Quota                                                                                                  " & vbNewLine
'    sSQL = sSQL & "     , Perc_Re_Seguro_Er                                                                                                     " & vbNewLine
'    sSQL = sSQL & "     , Tipo                                                                                                                  " & vbNewLine
'    sSQL = sSQL & "     , Nossa_Parte                                                                                                           " & vbNewLine
'    sSQL = sSQL & "     , Dt_Inclusao                                                                                                           " & vbNewLine
'    sSQL = sSQL & "     , Moeda_ID                                                                                                              " & vbNewLine
'    sSQL = sSQL & "     , pgto_parcial_co_seguro                                                                                                " & vbNewLine
'    sSQL = sSQL & "     )                                                                                                                       " & vbNewLine
'    sSQL = sSQL & "Select Dt_Inicio_Estimativa                                                                                                  " & vbNewLine
'    sSQL = sSQL & "     , Dt_Fim_Estimativa                                                                                                     " & vbNewLine
'    sSQL = sSQL & "     , Item_Val_Estimativa                                                                                                   " & vbNewLine
'    sSQL = sSQL & "     , Seq_Estimativa                                                                                                        " & vbNewLine
'    sSQL = sSQL & "     , Val_Estimado                                  = IsNull(Val_Estimado,0)                                                " & vbNewLine
'    sSQL = sSQL & "     , Val_Pago                                      = IsNull(Val_Pago,0)                                                    " & vbNewLine
'    sSQL = sSQL & "     , Val_Resseguro                                 = IsNull(Val_Resseguro,0)                                               " & vbNewLine
'    sSQL = sSQL & "     , Situacao                                      = IsNull(Sinistro_Estimativa_Tb.Situacao, '')                           " & vbNewLine
'    sSQL = sSQL & "     , Sinistro_Estimativa_Tb.Usuario                                                                                        " & vbNewLine
'    sSQL = sSQL & "     , Num_Carta_Seguro_Aceito                       = Isnull(Num_Carta_Seguro_Aceito, '')                                   " & vbNewLine
'    sSQL = sSQL & "     , Perc_Re_Seguro_Quota                          = IsNull(Perc_Re_Seguro_Quota,0)                                        " & vbNewLine
'    sSQL = sSQL & "     , Perc_Re_Seguro_Er                             = IsNull(Perc_Re_Seguro_Er,0)                                           " & vbNewLine
'    sSQL = sSQL & "     , Tipo                                          = 'E'                                                                   " & vbNewLine
'    sSQL = sSQL & "     , Nossa_Parte                                   = 0                                                                     " & vbNewLine
'    sSQL = sSQL & "     , Sinistro_Estimativa_Tb.Dt_Inclusao                                                                                    " & vbNewLine
'    sSQL = sSQL & "     , Sinistro_Tb.Moeda_ID                                                                                                  " & vbNewLine
'    sSQL = sSQL & "     , Sinistro_Tb.pgto_parcial_co_seguro                                                                                    " & vbNewLine
'    sSQL = sSQL & "  From Seguros_Db.Dbo.Sinistro_Estimativa_Tb          Sinistro_Estimativa_Tb WITH (NOLOCK)                                         " & vbNewLine
'    sSQL = sSQL & " -- Join #Sinistro_Principal                                                                                                    " & vbNewLine
'    sSQL = sSQL & " --  on #Sinistro_Principal.Sinistro_ID                = Sinistro_Estimativa_Tb.Sinistro_ID                                    " & vbNewLine
'    sSQL = sSQL & " Join Seguros_Db.Dbo.Sinistro_Tb                     Sinistro_Tb WITH (NOLOCK)                                                    " & vbNewLine
'    sSQL = sSQL & "   on Sinistro_Tb.Sinistro_ID                        = Sinistro_Estimativa_Tb.Sinistro_ID                                      " & vbNewLine
'    sSQL = sSQL & " Where Sinistro_Tb.Sinistro_ID = " & dSinistro_ID & vbCrLf
'    sSQL = sSQL & "Order By Seq_Estimativa desc                                                                                                     " & vbNewLine
'    sSQL = sSQL & " , Item_Val_Estimativa                                                                                                       " & vbNewLine
'
'
''AKIO.OKUNO - MP - 15/02/2013
''    Call Conexao_ExecutarSQL(gsSIGLASISTEMA, _
'                             glAmbiente_id, _
'                             App.Title, _
'                             App.FileDescription, _
'                             sSQL, _
'                             lConexaoLocal, _
'                             False)
'
''    sSQL = ""  'AKIO.OKUNO - MP - 15/02/2013
''AKIO.OKUNO / F.BEZERRA - INICIO - 12/03/2013
''    sSQL = sSQL & " Select Sinistro_Cobertura_Tb.Tp_Cobertura_ID, tp_cobertura_tb.nome                                          " & vbCrLf
''    sSQL = sSQL & "  , Correcao = isnull(sum(pgto_sinistro_cobertura_tb.val_correcao),0)                                        " & vbCrLf
''    'sSql = sSql & "  , Correcao                                      = isnull(Pgto_Sinistro_Cobertura_Tb.Val_Correcao,0)    " & vbCrLf
''    sSQL = sSQL & "  , Estimado = sinistro_cobertura_tb.val_estimativa - isnull(sum(pgto_sinistro_cobertura_tb.val_correcao),0) " & vbCrLf
''    'sSql = sSql & "  , Estimado                                      = isnull(Sinistro_Cobertura_Tb.Val_Estimativa,0)       " & vbCrLf
''    sSQL = sSQL & " Into #Cobertura_Afetada                                                                                 " & vbCrLf
''    sSQL = sSQL & " From Seguros_Db.Dbo.Sinistro_Cobertura_Tb          Sinistro_Cobertura_Tb WITH (NOLOCK)                       " & vbCrLf
''    sSQL = sSQL & " Left Outer Join (Seguros_Db.Dbo.Pgto_Sinistro_Tb   Pgto_Sinistro_Tb WITH (NOLOCK)                            " & vbCrLf
''    sSQL = sSQL & "             inner Join Seguros_Db.Dbo.Pgto_Sinistro_Cobertura_Tb Pgto_Sinistro_Cobertura_Tb WITH (NOLOCK)    " & vbCrLf
''    sSQL = sSQL & "                on Pgto_Sinistro_Cobertura_Tb.Sinistro_ID         = Pgto_Sinistro_Tb.Sinistro_ID         " & vbCrLf
''    sSQL = sSQL & "               and Pgto_Sinistro_Cobertura_Tb.Beneficiario_ID     = Pgto_Sinistro_Tb.Beneficiario_ID     " & vbCrLf
''    sSQL = sSQL & "               and Pgto_Sinistro_Cobertura_Tb.Seq_Pgto            = Pgto_Sinistro_Tb.Seq_Pgto            " & vbCrLf
''    sSQL = sSQL & "               and Pgto_Sinistro_Cobertura_Tb.Item_Val_Estimativa = Pgto_Sinistro_Tb.Item_Val_Estimativa " & vbCrLf
''    sSQL = sSQL & "               and Pgto_Sinistro_Cobertura_Tb.Seq_Estimativa      = Pgto_Sinistro_Tb.Seq_Estimativa      " & vbCrLf
''    sSQL = sSQL & "               and Pgto_Sinistro_Cobertura_Tb.Num_Parcela         = Pgto_Sinistro_Tb.Num_Parcela         " & vbCrLf
''    sSQL = sSQL & "               and (Pgto_Sinistro_Tb.seq_estimativa is null or Pgto_Sinistro_Tb.seq_estimativa = " & nSeq_Estimativa & " ))" & vbCrLf ' FLAVIO.BEZERRA - 07/02/2013
''    sSQL = sSQL & " on Sinistro_Cobertura_Tb.Sinistro_ID             = Pgto_Sinistro_Tb.Sinistro_ID                         " & vbCrLf
''    sSQL = sSQL & " and Sinistro_Cobertura_Tb.Tp_Cobertura_ID         = Pgto_Sinistro_Cobertura_Tb.Tp_Cobertura_ID          " & vbCrLf
''    sSQL = sSQL & " and Pgto_Sinistro_Tb.Situacao_OP                  not in ('c', 'e')                                     " & vbCrLf
''    sSQL = sSQL & " and Pgto_Sinistro_Tb.Item_Val_Estimativa          = 1                                                   " & vbCrLf
''    sSQL = sSQL & " Join Seguros_Db.Dbo.Tp_Cobertura_Tb                Tp_Cobertura_Tb WITH (NOLOCK)                             " & vbCrLf
''    sSQL = sSQL & " on Tp_Cobertura_Tb.Tp_Cobertura_ID               = Sinistro_Cobertura_Tb.Tp_Cobertura_ID                " & vbCrLf
''    sSQL = sSQL & " --  Join #Sinistro_Principal                                                                            " & vbCrLf
''    sSQL = sSQL & " -- on #Sinistro_Principal.Sinistro_ID                  = Sinistro_Cobertura_Tb.SInistro_ID              " & vbCrLf
''    sSQL = sSQL & " Where Sinistro_Cobertura_Tb.Dt_Fim_Vigencia         is null                                             " & vbCrLf
''    sSQL = sSQL & " and Sinistro_Cobertura_Tb.SInistro_ID = " & dSinistro_ID & vbCrLf
''    'sSQL = sSQL & " and (Pgto_Sinistro_Tb.seq_estimativa is null or Pgto_Sinistro_Tb.seq_estimativa = " & nSeq_Estimativa & " )" & vbCrLf ' FLAVIO.BEZERRA - 24/01/2013
''    sSQL = sSQL & " group by Sinistro_Cobertura_Tb.Tp_Cobertura_ID, tp_cobertura_tb.nome, " & vbCrLf
''    sSQL = sSQL & "          Pgto_Sinistro_Cobertura_Tb.Val_Correcao , Sinistro_Cobertura_Tb.Val_Estimativa " & vbCrLf
''AKIO.OKUNO / F.BEZERRA - FIM - 12/03/2013
''AKIO.OKUNO - MP - 15/02/2013
''    Call Conexao_ExecutarSQL(gsSIGLASISTEMA, _
'                             glAmbiente_id, _
'                             App.Title, _
'                             App.FileDescription, _
'                             sSQL, _
'                             lConexaoLocal, _
'                             False)
'
''    sSQL = ""  'AKIO.OKUNO - MP - 15/02/2013
''AKIO.OKUNO / F.BEZERRA - INICIO - 12/03/2013
''    sSQL = sSQL & " Insert into #EstimativaPagamento_CoberturaAfetada                                         " & vbCrLf
''    sSQL = sSQL & " Select Sinistro_Cobertura_Tb.Tp_Cobertura_ID                                                " & vbCrLf
''    sSQL = sSQL & "      , Sum(Correcao)            correcao_cob                                                " & vbCrLf
''    sSQL = sSQL & "      , Estimado - Sum(Correcao) pago_cob                                                   " & vbCrLf
''    sSQL = sSQL & "      , Tp_Cobertura_Tb.Nome                                                                 " & vbCrLf
''    sSQL = sSQL & "      , 'E'                      situacao                                                            " & vbCrLf
''    sSQL = sSQL & " --  into #EstimativaPagamento_CoberturaAfetada                                         " & vbCrLf
''    sSQL = sSQL & "   From #Cobertura_Afetada                                                                   " & vbCrLf
''    sSQL = sSQL & "   Join Seguros_Db.Dbo.Sinistro_Cobertura_Tb          Sinistro_Cobertura_Tb WITH (NOLOCK)         " & vbCrLf
''    sSQL = sSQL & "     on Sinistro_Cobertura_Tb.Tp_Cobertura_ID         = #Cobertura_Afetada.Tp_Cobertura_ID   " & vbCrLf
''    sSQL = sSQL & "   Join Seguros_Db.Dbo.Tp_Cobertura_Tb                Tp_Cobertura_Tb WITH (NOLOCK)               " & vbCrLf
''    sSQL = sSQL & "     on Tp_Cobertura_Tb.Tp_Cobertura_ID               = #Cobertura_Afetada.Tp_Cobertura_ID   " & vbCrLf
''    sSQL = sSQL & "--  Join #Sinistro_Principal                                                                                                   " & vbNewLine
''    sSQL = sSQL & "--    on #Sinistro_Principal.Sinistro_ID               = Sinistro_Cobertura_Tb.Sinistro_ID                                     " & vbNewLine
''    sSQL = sSQL & " Where Sinistro_Cobertura_Tb.Dt_Fim_Vigencia is null                                                                         " & vbNewLine
''    sSQL = sSQL & " and   Sinistro_Cobertura_Tb.SInistro_ID = " & dSinistro_ID & vbCrLf
''    sSQL = sSQL & "  Group By Sinistro_Cobertura_Tb.Tp_Cobertura_ID                                             " & vbCrLf
''    sSQL = sSQL & "      , Estimado                                                                             " & vbCrLf
''    sSQL = sSQL & "      , Tp_Cobertura_Tb.Nome                                                                 " & vbCrLf
''AKIO.OKUNO / F.BEZERRA - FIM - 12/03/2013
'    'sSQL = sSQL & " Drop table #Cobertura_Afetada                                                               " & vbCrLf
'
'    Call Conexao_ExecutarSQL(gsSIGLASISTEMA, _
'                             glAmbiente_id, _
'                             App.Title, _
'                             App.FileDescription, _
'                             sSQL, _
'                             lConexaoLocal, _
'                             False)
'
'    SelecionaDados_AbaEstimativa_CoberturaAfetadaLocal lConexaoLocal
'
'
'    Exit Sub
'
'TrataErro:
'    Call TrataErroGeral("CarregaDadosEstimativa", "FrmEstimativa")
'    Call FinalizarAplicacao
'
'End Sub

'AKIO.OKUNO - CriaTemporaria - 06/05/2013
Private Sub CarregaDadosEstimativa()

    Dim sSQL As String

    On Error GoTo TrataErro

    sSQL = ""
    sSQL = sSQL & "Set NoCount On                                                   " & vbNewLine
    sSQL = sSQL & "Declare @Tp_Componente_ID as Tinyint                             " & vbNewLine
    
    sSQL = sSQL & "Set @Tp_Componente_ID  = 0                                       " & vbNewLine
    
    If vProduto = "" Then   'SE O PRODUTO_ID N�O VEIO PELO PAR�METRO
        sSQL = sSQL & "select Sinistro_Tb.proposta_id                               " & vbNewLine
        sSQL = sSQL & "     , Sinistro_Tb.sinistro_id                               " & vbNewLine
        sSQL = sSQL & "     , Sinistro_Tb.apolice_id                                " & vbNewLine
        sSQL = sSQL & "     , Sinistro_Tb.sucursal_seguradora_id                    " & vbNewLine
        sSQL = sSQL & "     , Sinistro_Tb.seguradora_cod_susep                      " & vbNewLine
        sSQL = sSQL & "     , Sinistro_Tb.ramo_id                                   " & vbNewLine
        sSQL = sSQL & "     , Sinistro_Tb.cliente_id                                " & vbNewLine
        sSQL = sSQL & "     , Sinistro_Tb.dt_ocorrencia_sinistro                    " & vbNewLine
        sSQL = sSQL & "     , sinistro_tb.situacao                                  " & vbNewLine
        sSQL = sSQL & "     , Proposta_Tb.Produto_ID                                " & vbNewLine
        sSQL = sSQL & "     , Sinistro_Tb.Moeda_ID                                  " & vbNewLine
        sSQL = sSQL & "     , Sinistro_Tb.Pgto_Parcial_Co_Seguro                    " & vbNewLine
        'C00216281-ESTEIRA-PAGAMENTO-IMEDIATO-VIDA-INDIVIDUAL
        sSQL = sSQL & "     , Sinistro_Tb.evento_sinistro_id                        " & vbNewLine
        'C00216281 - FIM
        sSQL = sSQL & "  Into #Sinistro_Principal " & vbNewLine
        sSQL = sSQL & "  From seguros_db.dbo.sinistro_tb sinistro_tb WITH (NOLOCK)   " & vbNewLine
        sSQL = sSQL & "  Join Seguros_Db.Dbo.Proposta_Tb Proposta_tb WITH (NOLOCK)   " & vbNewLine
        sSQL = sSQL & "    on Proposta_Tb.Proposta_ID = Sinistro_Tb.Proposta_ID     " & vbNewLine
        sSQL = sSQL & " Where sinistro_id = " & dSinistro_ID & "                    " & vbNewLine
    Else
        sSQL = sSQL & "select proposta_id                                           " & vbNewLine
        sSQL = sSQL & "     , sinistro_id                                           " & vbNewLine
        sSQL = sSQL & "     , apolice_id                                            " & vbNewLine
        sSQL = sSQL & "     , sucursal_seguradora_id                                " & vbNewLine
        sSQL = sSQL & "     , seguradora_cod_susep                                  " & vbNewLine
        sSQL = sSQL & "     , ramo_id                                               " & vbNewLine
        sSQL = sSQL & "     , cliente_id                                            " & vbNewLine
        sSQL = sSQL & "     , dt_ocorrencia_sinistro                                " & vbNewLine
        sSQL = sSQL & "     , sinistro_tb.situacao                                  " & vbNewLine
        sSQL = sSQL & "     , Sinistro_Tb.Moeda_ID                                  " & vbNewLine
        sSQL = sSQL & "     , Sinistro_Tb.Pgto_Parcial_Co_Seguro                    " & vbNewLine
        sSQL = sSQL & "  Into #Sinistro_Principal " & vbNewLine
        sSQL = sSQL & "  From seguros_db.dbo.sinistro_tb WITH (NOLOCK)               " & vbNewLine
        sSQL = sSQL & " Where sinistro_id = " & dSinistro_ID & "                    " & vbNewLine
    End If
    
    sSQL = sSQL & "Create table #EstimativaPagamento_Estimativa                                                                                 " & vbNewLine
    sSQL = sSQL & "     ( Dt_Inicio_Estimativa                          SmallDateTime                                                           " & vbNewLine
    sSQL = sSQL & "     , Dt_Fim_Estimativa                             SmallDateTime                                                           " & vbNewLine
    sSQL = sSQL & "     , Item_Val_Estimativa                           TinyInt                                                                 " & vbNewLine
    sSQL = sSQL & "     , Seq_Estimativa                                TinyInt                                                                 " & vbNewLine
    sSQL = sSQL & "     , Val_Estimado                                  Numeric(15,2)                                                           " & vbNewLine
    sSQL = sSQL & "     , Val_Pago                                      Numeric(15,2)                                                           " & vbNewLine
    sSQL = sSQL & "     , Val_Resseguro                                 Numeric(15,2)                                                           " & vbNewLine
    sSQL = sSQL & "     , Situacao                                      Char(1)                                                                 " & vbNewLine
    sSQL = sSQL & "     , Usuario                                       VarChar(20)                                                             " & vbNewLine
    sSQL = sSQL & "     , Num_Carta_Seguro_Aceito                       Int                                                                     " & vbNewLine
    sSQL = sSQL & "     , Perc_Re_Seguro_Quota                          Numeric(9,6)                                                            " & vbNewLine
    sSQL = sSQL & "     , Perc_Re_Seguro_Er                             Numeric(9,6)                                                            " & vbNewLine
    sSQL = sSQL & "     , Tipo                                          Char(1)                                                                 " & vbNewLine
    sSQL = sSQL & "     , Nossa_Parte                                   Numeric(9,6)                                                            " & vbNewLine
    sSQL = sSQL & "     , Dt_Inclusao                                   SmallDateTime                                                           " & vbNewLine
    sSQL = sSQL & "     , CountEstimativa                               Numeric(3)                                                              " & vbNewLine
    sSQL = sSQL & "     , Moeda_ID                                      Numeric(3)                                                              " & vbNewLine           '-- Sinistro_Tb (Estimativa)
    sSQL = sSQL & "     , Moeda_Sigla                                   VarChar(4)                                                              " & vbNewLine           '-- Moeda_Tb (Estimativa)
    sSQL = sSQL & "     , pgto_parcial_co_seguro                        Char(1)                                                                 " & vbNewLine           '-- Sinistro_Tb.pgto_parcial_co_seguro
    sSQL = sSQL & "     )                                                                                                                       " & vbNewLine
    sSQL = sSQL & "Create Index PK_Dt_Inicio_Estimativa             on #EstimativaPagamento_Estimativa (Seq_Estimativa, Dt_Inicio_Estimativa)   " & vbNewLine

    sSQL = sSQL & vbCrLf
    sSQL = sSQL & " create table #Estimativa_Evento " & vbCrLf
    sSQL = sSQL & " (dt_evento                  smalldatetime, " & vbCrLf
    sSQL = sSQL & " situacao                    char(1), " & vbCrLf
    sSQL = sSQL & " evento_SEGBR                varchar(250), " & vbCrLf
    sSQL = sSQL & " descricao                   varchar(250), " & vbCrLf
    sSQL = sSQL & " tipo                        char(1), " & vbCrLf
    sSQL = sSQL & " usuario                     varchar(250), " & vbCrLf
    sSQL = sSQL & " seq_evento                  tinyint, " & vbCrLf
    sSQL = sSQL & " localizacao                 tinyint) " & vbCrLf
    sSQL = sSQL & vbCrLf

    sSQL = sSQL & "Insert into #EstimativaPagamento_Estimativa                                                                                  " & vbNewLine
    sSQL = sSQL & "     ( Dt_Inicio_Estimativa                                                                                                  " & vbNewLine
    sSQL = sSQL & "     , Dt_Fim_Estimativa                                                                                                     " & vbNewLine
    sSQL = sSQL & "     , Item_Val_Estimativa                                                                                                   " & vbNewLine
    sSQL = sSQL & "     , Seq_Estimativa                                                                                                        " & vbNewLine
    sSQL = sSQL & "     , Val_Estimado                                                                                                          " & vbNewLine
    sSQL = sSQL & "     , Val_Pago                                                                                                              " & vbNewLine
    sSQL = sSQL & "     , Val_Resseguro                                                                                                         " & vbNewLine
    sSQL = sSQL & "     , Situacao                                                                                                              " & vbNewLine
    sSQL = sSQL & "     , Usuario                                                                                                               " & vbNewLine
    sSQL = sSQL & "     , Num_Carta_Seguro_Aceito                                                                                               " & vbNewLine
    sSQL = sSQL & "     , Perc_Re_Seguro_Quota                                                                                                  " & vbNewLine
    sSQL = sSQL & "     , Perc_Re_Seguro_Er                                                                                                     " & vbNewLine
    sSQL = sSQL & "     , Tipo                                                                                                                  " & vbNewLine
    sSQL = sSQL & "     , Nossa_Parte                                                                                                           " & vbNewLine
    sSQL = sSQL & "     , Dt_Inclusao                                                                                                           " & vbNewLine
    sSQL = sSQL & "     , Moeda_ID                                                                                                              " & vbNewLine
    sSQL = sSQL & "     , pgto_parcial_co_seguro                                                                                                " & vbNewLine
    sSQL = sSQL & "     )                                                                                                                       " & vbNewLine
    sSQL = sSQL & "Select Dt_Inicio_Estimativa                                                                                                  " & vbNewLine
    sSQL = sSQL & "     , Dt_Fim_Estimativa                                                                                                     " & vbNewLine
    sSQL = sSQL & "     , Item_Val_Estimativa                                                                                                   " & vbNewLine
    sSQL = sSQL & "     , Seq_Estimativa                                                                                                        " & vbNewLine
    sSQL = sSQL & "     , Val_Estimado                                  = IsNull(Val_Estimado,0)                                                " & vbNewLine
    sSQL = sSQL & "     , Val_Pago                                      = IsNull(Val_Pago,0)                                                    " & vbNewLine
    sSQL = sSQL & "     , Val_Resseguro                                 = IsNull(Val_Resseguro,0)                                               " & vbNewLine
    sSQL = sSQL & "     , Situacao                                      = IsNull(Sinistro_Estimativa_Tb.Situacao, '')                           " & vbNewLine
    sSQL = sSQL & "     , Sinistro_Estimativa_Tb.Usuario                                                                                        " & vbNewLine
    sSQL = sSQL & "     , Num_Carta_Seguro_Aceito                       = Isnull(Num_Carta_Seguro_Aceito, '')                                   " & vbNewLine
    sSQL = sSQL & "     , Perc_Re_Seguro_Quota                          = IsNull(Perc_Re_Seguro_Quota,0)                                        " & vbNewLine
    sSQL = sSQL & "     , Perc_Re_Seguro_Er                             = IsNull(Perc_Re_Seguro_Er,0)                                           " & vbNewLine
    sSQL = sSQL & "     , Tipo                                          = 'E'                                                                   " & vbNewLine
    sSQL = sSQL & "     , Nossa_Parte                                   = 0                                                                     " & vbNewLine
    sSQL = sSQL & "     , Sinistro_Estimativa_Tb.Dt_Inclusao                                                                                    " & vbNewLine
    sSQL = sSQL & "     , #Sinistro_Principal.Moeda_ID                                                                                          " & vbNewLine
    sSQL = sSQL & "     , #Sinistro_Principal.pgto_parcial_co_seguro                                                                            " & vbNewLine
    sSQL = sSQL & "  From Seguros_Db.Dbo.Sinistro_Estimativa_Tb          Sinistro_Estimativa_Tb WITH (NOLOCK)                                        " & vbNewLine
    sSQL = sSQL & "  Join #Sinistro_Principal                                                                                                   " & vbNewLine
    sSQL = sSQL & "   on #Sinistro_Principal.Sinistro_ID                = Sinistro_Estimativa_Tb.Sinistro_ID                                    " & vbNewLine
    'Msalema - 17/05/2013
    'sSQL = sSQL & " Where Sinistro_Estimativa_tb.Dt_Fim_Estimativa      is Null                                                                 " & vbCrLf
   sSQL = sSQL & "Where Sinistro_Estimativa_tb.seq_estimativa = (Select max (ALT.seq_estimativa) From Sinistro_Estimativa_tb ALT WITH (NOLOCK) where ALT.sinistro_id=#Sinistro_Principal.sinistro_id)" & vbNewLine
    'msalema  - 17/05/2013
    sSQL = sSQL & "Order By Item_Val_Estimativa                                                                                                 " & vbNewLine

    If bytTipoRamo = bytTipoRamo_Vida Then
        sSQL = sSQL & "Select @Tp_Componente_ID                         = Tp_Componente_ID" & vbNewLine
        sSQL = sSQL & "  From Seguros_Db.Dbo.Escolha_Tp_Cob_Vida_Tb     Escolha_Tp_Cob_Vida_Tb WITH (NOLOCK)" & vbNewLine
        sSQL = sSQL & "  Join #Sinistro_Principal" & vbNewLine
        sSQL = sSQL & "    on #Sinistro_Principal.Proposta_ID           = Escolha_Tp_Cob_Vida_Tb.Proposta_ID" & vbNewLine
        sSQL = sSQL & " Where Escolha_Tp_Cob_Vida_Tb.Tp_Componente_ID   in (19,20)" & vbNewLine
        sSQL = sSQL & " Order by Tp_Componente_ID" & vbNewLine
    End If

    sSQL = sSQL & "Select *                                                                                                                     " & vbNewLine
    sSQL = sSQL & "     , Tp_Componente_ID      = @Tp_Componente_ID                                                                             " & vbNewLine
    sSQL = sSQL & "  From #Sinistro_Principal                                                                                                   " & vbNewLine

    Set rsRecordSet = Conexao_ExecutarSQL(gsSIGLASISTEMA _
                                         , glAmbiente_id _
                                         , App.Title _
                                         , App.FileDescription _
                                         , sSQL _
                                         , lConexaoLocal _
                                         , True)
                                         
    
    
    If Not rsRecordSet.EOF Then

        SinDT_Ocorrencia = rsRecordSet!dt_ocorrencia_sinistro
        vTpComponente = "T"
        SinApolice_Id = rsRecordSet!Apolice_id
        gbllRamo_ID = rsRecordSet!ramo_id
        gbllProposta_ID = rsRecordSet!proposta_id
        SinSeguradora_Id = rsRecordSet!seguradora_cod_susep
        SinSucursal_Id = rsRecordSet!sucursal_seguradora_id
        If vProduto = "" Then
            vProduto = rsRecordSet!produto_id
        End If
        SinCliente_Id = rsRecordSet!Cliente_id
        SinSituacao_id = rsRecordSet!Situacao
            
        gblsTp_Componente_ID = rsRecordSet!tp_componente_id
        
        'C00216281-ESTEIRA-PAGAMENTO-IMEDIATO-VIDA-INDIVIDUAL
         SinEvento_sinistro_id = rsRecordSet!evento_sinistro_id
         SinRamo_Id = rsRecordSet!ramo_id
         PgtoImediatoSituacao = 0
        'C00216281 - FIM

    End If


    SelecionaDados_AbaEstimativa_CoberturaAfetadaLocal lConexaoLocal


    Exit Sub

TrataErro:
    Call TrataErroGeral("CarregaDadosEstimativa", "FrmEstimativa")
    Call FinalizarAplicacao

End Sub

Private Sub SelecionaDados_AbaEstimativa_CoberturaAfetadaLocal(lConexaoLocal As Integer)
'    SelecionaDados_AbaEstimativa_CoberturaAfetada lConexaoLocal
    Dim sSQL As String

    On Error GoTo Erro
    sSQL = ""

    '' GENESCO SILVA - FLOW 18313521 - MIGRA��O SQL SERVER
    'sSQL = sSQL & " if isnull(OBJECT_ID('tempdb..#EstimativaPagamento_CoberturaAfetada'),0) >0" & vbNewLine
    sSQL = sSQL & " if OBJECT_ID('tempdb..#EstimativaPagamento_CoberturaAfetada') IS NOT NULL " & vbNewLine
    sSQL = sSQL & " BEGIN" & vbNewLine
    sSQL = sSQL & "     DROP TABLE #EstimativaPagamento_CoberturaAfetada" & vbNewLine
    sSQL = sSQL & " END" & vbNewLine
    sSQL = sSQL & "" & vbNewLine

    sSQL = sSQL & "Create Table #EstimativaPagamento_CoberturaAfetada                                                                           " & vbNewLine
    sSQL = sSQL & "     ( Tp_Cobertura_ID                               Int                                                                     " & vbNewLine
    sSQL = sSQL & "     , Correcao                                      Numeric(15,2)                                                           " & vbNewLine
    sSQL = sSQL & "     , Estimado                                      Numeric(15,2)                                                           " & vbNewLine
    sSQL = sSQL & "     , Tp_Cobertura_Nome                             VarChar(60)                                                             " & vbNewLine
    sSQL = sSQL & "     , Tipo                                          VarChar(1)                                                              " & vbNewLine
    sSQL = sSQL & "  )                                                                                                                          " & vbNewLine
    sSQL = sSQL & "Select Sinistro_Cobertura_Tb.Tp_Cobertura_ID                                                                                 " & vbNewLine
    sSQL = sSQL & "     , Correcao                                      = isnull(Pgto_Sinistro_Cobertura_Tb.Val_Correcao,0)                     " & vbNewLine
    sSQL = sSQL & "     , Estimado                                      = isnull(Sinistro_Cobertura_Tb.Val_Estimativa,0)                        " & vbNewLine
    sSQL = sSQL & "  Into #Cobertura_Afetada                                                                                                    " & vbNewLine
    sSQL = sSQL & "  From Seguros_Db.Dbo.Sinistro_Cobertura_Tb          Sinistro_Cobertura_Tb WITH (NOLOCK)                                          " & vbNewLine
    sSQL = sSQL & "  Left Outer Join (Seguros_Db.Dbo.Pgto_Sinistro_Tb   Pgto_Sinistro_Tb WITH (NOLOCK)                                               " & vbNewLine
    sSQL = sSQL & "                inner Join Seguros_Db.Dbo.Pgto_Sinistro_Cobertura_Tb Pgto_Sinistro_Cobertura_Tb WITH (NOLOCK)                     " & vbNewLine
    sSQL = sSQL & "                   on Pgto_Sinistro_Cobertura_Tb.Sinistro_ID         = Pgto_Sinistro_Tb.Sinistro_ID                          " & vbNewLine
    sSQL = sSQL & "                  and Pgto_Sinistro_Cobertura_Tb.Beneficiario_ID     = Pgto_Sinistro_Tb.Beneficiario_ID                      " & vbNewLine
    sSQL = sSQL & "                  and Pgto_Sinistro_Cobertura_Tb.Seq_Pgto            = Pgto_Sinistro_Tb.Seq_Pgto                             " & vbNewLine
    sSQL = sSQL & "                  and Pgto_Sinistro_Cobertura_Tb.Item_Val_Estimativa = Pgto_Sinistro_Tb.Item_Val_Estimativa                  " & vbNewLine
    sSQL = sSQL & "                  and Pgto_Sinistro_Cobertura_Tb.Seq_Estimativa      = Pgto_Sinistro_Tb.Seq_Estimativa                       " & vbNewLine
    sSQL = sSQL & "                  and Pgto_Sinistro_Cobertura_Tb.Num_Parcela         = Pgto_Sinistro_Tb.Num_Parcela           )              " & vbNewLine
    sSQL = sSQL & "    on Sinistro_Cobertura_Tb.Sinistro_ID             = Pgto_Sinistro_Tb.Sinistro_ID                                          " & vbNewLine
    sSQL = sSQL & "   and Sinistro_Cobertura_Tb.Tp_Cobertura_ID         = Pgto_Sinistro_Cobertura_Tb.Tp_Cobertura_ID                            " & vbNewLine
    sSQL = sSQL & "   and Pgto_Sinistro_Tb.Situacao_OP                  not in ('c', 'e')                                                       " & vbNewLine
    sSQL = sSQL & "   and Pgto_Sinistro_Tb.Item_Val_Estimativa          = 1                                                                     " & vbNewLine
    sSQL = sSQL & "  Join #Sinistro_Principal                                                                                                   " & vbNewLine
    sSQL = sSQL & " on #Sinistro_Principal.Sinistro_ID                  = Sinistro_Cobertura_Tb.SInistro_ID                                     " & vbNewLine
    sSQL = sSQL & " Where Sinistro_Cobertura_Tb.Dt_Fim_Vigencia         is null                                                                 " & vbNewLine

    sSQL = sSQL & "Insert into #EstimativaPagamento_CoberturaAfetada                                                                            " & vbNewLine
    sSQL = sSQL & "Select Sinistro_Cobertura_Tb.Tp_Cobertura_ID                                                                                 " & vbNewLine
    sSQL = sSQL & "     , Sum(Correcao)                                                                                                         " & vbNewLine
    sSQL = sSQL & "     , Estimado - Sum(Correcao)                                                                                              " & vbNewLine
    sSQL = sSQL & "     , Tp_Cobertura_Tb.Nome                                                                                                  " & vbNewLine
    sSQL = sSQL & "     , 'E'                                                                                                                   " & vbNewLine
    sSQL = sSQL & "  From #Cobertura_Afetada                                                                                                    " & vbNewLine
    sSQL = sSQL & "  Join Seguros_Db.Dbo.Sinistro_Cobertura_Tb          Sinistro_Cobertura_Tb WITH (NOLOCK)                                          " & vbNewLine
    sSQL = sSQL & "    on Sinistro_Cobertura_Tb.Tp_Cobertura_ID         = #Cobertura_Afetada.Tp_Cobertura_ID                                    " & vbNewLine
    sSQL = sSQL & "  Join Seguros_Db.Dbo.Tp_Cobertura_Tb                Tp_Cobertura_Tb WITH (NOLOCK)                                                " & vbNewLine
    sSQL = sSQL & "    on Tp_Cobertura_Tb.Tp_Cobertura_ID               = #Cobertura_Afetada.Tp_Cobertura_ID                                    " & vbNewLine
    sSQL = sSQL & "  Join #Sinistro_Principal                                                                                                   " & vbNewLine
    sSQL = sSQL & "    on #Sinistro_Principal.Sinistro_ID               = Sinistro_Cobertura_Tb.Sinistro_ID                                     " & vbNewLine
    sSQL = sSQL & " Where Sinistro_Cobertura_Tb.Dt_Fim_Vigencia is null                                                                         " & vbNewLine
    sSQL = sSQL & " Group By Sinistro_Cobertura_Tb.Tp_Cobertura_ID                                                                              " & vbNewLine
    sSQL = sSQL & "     , Estimado                                                                                                              " & vbNewLine
    sSQL = sSQL & "     , Tp_Cobertura_Tb.Nome                                                                                                  " & vbNewLine
    sSQL = sSQL & "Drop table #Cobertura_Afetada                                                                                                " & vbNewLine

    Call Conexao_ExecutarSQL(gsSIGLASISTEMA, _
                             glAmbiente_id, _
                             App.Title, _
                             App.FileDescription, _
                             sSQL, _
                             lConexao, _
                             False)

    Exit Sub

Erro:
    Call TrataErroGeral("SelecionaDados_AbaEstimativa_CoberturaAfetada", "modSEGP1285.Bas")
    Call FinalizarAplicacao


End Sub

Private Sub btnCoberturaAdicionar_Click()

    Dim i As Integer
    Dim vValor As String
    Dim vEstima As String
    Dim sSQL As String
    Dim Valor_is As Double
    'Dim Val_limite_aprovacao As Double
    Dim vTecnico_id As Integer
    Dim iProdutoExterno As Integer

    On Error GoTo TrataErro

    MousePointer = vbHourglass

    If mskValorPrejuizo.Text = "" Then mskValorPrejuizo.Text = "0,0"

    If cmbCoberturaAfetada.ListIndex = -1 _
       Or (mskValorPrejuizo.Text = "" _
       Or Not IsNumeric(mskValorPrejuizo.Text) _
       Or CDbl(mskValorPrejuizo.Text) = 0) Then
        MsgBox "Valores incorretos.", vbInformation
        mskValorPrejuizo.SetFocus
        MousePointer = vbNormal
        Exit Sub
    End If

    'Checa se a cobertura j� est� no grid
    If grdEstimativasPagamentos_Coberturas.Rows > 1 Then
        For i = 1 To grdEstimativasPagamentos_Coberturas.Rows - 1
            If cmbCoberturaAfetada.ItemData(cmbCoberturaAfetada.ListIndex) = grdEstimativasPagamentos_Coberturas.RowData(i) Then
                MsgBox "Cobertura j� selecionada!", vbInformation
                cmbCoberturaAfetada.SetFocus
                MousePointer = vbNormal
                Exit Sub
            End If
        Next
    End If
    
    If vProduto = "400" Then 'NTendencia - 17/12/2019
        iProdutoExterno = Obter_produto_externo(gbllProposta_ID)
        If iProdutoExterno = 0 Or iProdutoExterno <> "999" Then
            MsgBox "Produto n�o possui c�digo de personalizado (produto externo). Proposta com Id = " & lPropostaId & " e cobertura com Id = " & lCoberturaId & "." & " (Produto externo diferente de 999 para produto 400)"
            Exit Sub
        End If
        'busca IS
        Valor_is = Obter_valor_IS_400(gbllProposta_ID, vProduto, cmbCoberturaAfetada.ItemData(cmbCoberturaAfetada.ListIndex), iProdutoExterno)
        'se tiver
        If Valor_is <> 0 Then
            If CDbl("0" & mskValorPrejuizo.Text) > Valor_is Then
                    vTecnico_id = SelecionaDados_TecnicoAtual
                    If Limite_aprovacao_estimativa(vTecnico_id, vProduto, gbllRamo_ID, 1) = False Then
                        MsgBox "Valor da estimativa � superior ao valor da I.S. e n�o h� t�cnico cadastrado para o Produto/Ramo.", vbExclamation
                        Screen.MousePointer = vbNormal
                        Exit Sub
                    End If
            End If
        End If
        'Se valor da IS � igual a zero significa que n�o tem IS, nesse caso deixa alterar
    ElseIf vProduto = "811" Then 'NTendencia - 16/12/2019
        iProdutoExterno = Obter_produto_externo(gbllProposta_ID)
        If iProdutoExterno = 0 Then
            MsgBox "Produto n�o possui c�digo de personalizado (produto externo). Proposta com Id = " & lPropostaId & " e cobertura com Id = " & lCoberturaId & "."
            Exit Sub
        End If
    
        'busca IS
        Valor_is = Obter_valor_IS_811(gbllProposta_ID, vProduto, cmbCoberturaAfetada.ItemData(cmbCoberturaAfetada.ListIndex), iProdutoExterno)
        'se tiver
        If Valor_is <> 0 Then
            If CDbl("0" & mskValorPrejuizo.Text) > Valor_is Then
                    vTecnico_id = SelecionaDados_TecnicoAtual
                    If Limite_aprovacao_estimativa(vTecnico_id, vProduto, gbllRamo_ID, 1) = False Then
                        MsgBox "Valor da estimativa � superior ao valor da I.S. e n�o h� t�cnico cadastrado para o Produto/Ramo.", vbExclamation
                        Screen.MousePointer = vbNormal
                        Exit Sub
                    End If
            End If
        End If
        'Se valor da IS � igual a zero significa que n�o tem IS, nesse caso deixa alterar
    ElseIf vProduto = "680" And cmbCoberturaAfetada.ItemData(cmbCoberturaAfetada.ListIndex) = 527 Then
        'busca IS
        Valor_is = Obter_valor_IS_680(gbllProposta_ID)
        'se tiver
        If Valor_is <> 0 Then
            If CDbl("0" & mskValorPrejuizo.Text) > Valor_is Then
                    vTecnico_id = SelecionaDados_TecnicoAtual
                    If Limite_aprovacao_estimativa(vTecnico_id, vProduto, gbllRamo_ID, 1) = False Then
                        MsgBox "Valor da estimativa � superior ao valor da I.S. e n�o h� t�cnico cadastrado para o Produto/Ramo.", vbExclamation
                        Screen.MousePointer = vbNormal
                        Exit Sub
                    End If
            End If
        End If
        'Se valor da IS � igual a zero significa que n�o tem IS, nesse caso deixa alterar
    ElseIf vProduto = "115" Or vProduto = "150" Or vProduto = "123" Then
        'busca IS
        Valor_is = Obter_valor_IS_115_150(dSinistro_ID)
        'se tiver
        If Valor_is <> 0 Then
            If CDbl("0" & mskValorPrejuizo.Text) > Valor_is Then
                    vTecnico_id = SelecionaDados_TecnicoAtual
                    If Limite_aprovacao_estimativa(vTecnico_id, vProduto, gbllRamo_ID, 1) = False Then
                        MsgBox "Valor da estimativa � superior ao valor da I.S. e n�o h� t�cnico cadastrado para o Produto/Ramo.", vbExclamation
                        Screen.MousePointer = vbNormal
                        Exit Sub
                    End If
            End If
        End If
        'Se valor da IS � igual a zero significa que n�o tem IS, nesse caso deixa alterar
    ElseIf vProduto = "1206" Then
        'busca IS
        Valor_is = Obter_valor_IS_1206(dSinistro_ID, cmbCoberturaAfetada.ItemData(cmbCoberturaAfetada.ListIndex))
        'se tiver
        If Valor_is <> 0 Then
            If CDbl("0" & mskValorPrejuizo.Text) > Valor_is Then
                    vTecnico_id = SelecionaDados_TecnicoAtual
                    If Limite_aprovacao_estimativa(vTecnico_id, vProduto, gbllRamo_ID, 1) = False Then
                        MsgBox "Valor da estimativa � superior ao valor da I.S. e n�o h� t�cnico cadastrado para o Produto/Ramo.", vbExclamation
                        Screen.MousePointer = vbNormal
                        Exit Sub
                    End If
            End If
        End If
        'Se valor da IS � igual a zero significa que n�o tem IS, nesse caso deixa alterar
    ElseIf vProduto = "721" Then
        'busca IS
        Valor_is = Obter_valor_IS_721(dSinistro_ID, cmbCoberturaAfetada.ItemData(cmbCoberturaAfetada.ListIndex))
        'se tiver
        If Valor_is <> 0 Then
            If CDbl("0" & mskValorPrejuizo.Text) > Valor_is Then
                    vTecnico_id = SelecionaDados_TecnicoAtual
                    If Limite_aprovacao_estimativa(vTecnico_id, vProduto, gbllRamo_ID, 1) = False Then
                        MsgBox "Valor da estimativa � superior ao valor da I.S. e n�o h� t�cnico cadastrado para o Produto/Ramo.", vbExclamation
                        Screen.MousePointer = vbNormal
                        Exit Sub
                    End If
            End If
        End If
    ElseIf vProduto = "1204" Then
        'busca IS
        Valor_is = Obter_valor_IS_1204(dSinistro_ID, cmbCoberturaAfetada.ItemData(cmbCoberturaAfetada.ListIndex), gbllProposta_ID)
        'se tiver
        If Valor_is <> 0 Then
            If CDbl("0" & mskValorPrejuizo.Text) > Valor_is Then
                    vTecnico_id = SelecionaDados_TecnicoAtual
                    If Limite_aprovacao_estimativa(vTecnico_id, vProduto, gbllRamo_ID, 1) = False Then
                        MsgBox "Valor da estimativa � superior ao valor da I.S. e n�o h� t�cnico cadastrado para o Produto/Ramo.", vbExclamation
                        Screen.MousePointer = vbNormal
                        Exit Sub
                    End If
            End If
        End If
    Else
    'Se sinistro n�o tem sinistro l�der, verifica estimativa
    If Not Tem_Sinistro_lider(dSinistro_ID) Then
    
        vTecnico_id = SelecionaDados_TecnicoAtual
        
        'Se ramo � vida
        If Ramo_Vida(gbllRamo_ID) Then
        
                'Produto 718 entra nessa valida��o
            
            'Recebe valor IS
            Valor_is = Obter_valor_IS_Vida(gbllProposta_ID, cmbCoberturaAfetada.ItemData(cmbCoberturaAfetada.ListIndex))
            If Valor_is = 0 Then 'N�o tem IS
                    'N�o aplica regra
            Else
            'Se valor da estimativa � maior que valor da IS ent�o
            If CDbl("0" & mskValorPrejuizo.Text) > Valor_is Then
                If Limite_aprovacao_estimativa(vTecnico_id, vProduto, gbllRamo_ID, 1) = False Then
                    MsgBox "Valor da estimativa � superior ao valor da I.S. e n�o h� t�cnico cadastrado para o Produto/Ramo.", vbExclamation
                    Screen.MousePointer = vbNormal
                    Exit Sub
                End If
            End If
                End If
        Else
            Valor_is = (obter_valor_is(gbllProposta_ID, vProduto, cmbCoberturaAfetada.ItemData(cmbCoberturaAfetada.ListIndex)))
    Estimativa_menor_que_IS_via_SEGS7442 = True
            
                '----NTendencia - 27/102/2019
                If Valor_is <> 0 Then 'N�o aplica regra se n�o tem IS
                '---
    If strTipoProcesso <> strTipoProcesso_Avisar And strTipoProcesso <> strTipoProcesso_Avisar_Cosseguro Then
        Estimativa_menor_que_IS_via_SEGS7442 = Valida_IS_via_SEGS7442(cmbCoberturaAfetada.ItemData(cmbCoberturaAfetada.ListIndex), CDbl("0" & mskValorPrejuizo.Text))
    End If
    
            'vTecnico_id = SelecionaDados_TecnicoAtual
    
            'Se valor do preju�zo � maior que IS, verifica se tem t�cnico
    If CDbl("0" & mskValorPrejuizo.Text) > Valor_is Or Not Estimativa_menor_que_IS_via_SEGS7442 Then
        If Not Limite_aprovacao_estimativa(vTecnico_id, vProduto, gbllRamo_ID, 1) Then
            'Se for maior, n�o permite
            MsgBox "N�o h� t�cnico cadastrado ou o valor da estimativa � superior ao valor do limite cadastrado para o t�cnico.", vbExclamation
            MousePointer = vbNormal
            Exit Sub
        End If
            End If
        End If
    End If
    End If
    End If
    '--------------------------
    
    '----
    'NTendencia - 10/12/2019
    'Foi retirado o produto 680 da verifica��o abaixo para receber tratativa diferencia no c�digo acima
    '----
    
    
'+-----------------------------------------------------------
'| 19406709 - Melhoria nos limites de sinistro e emissao
'| RN04 - petrauskas - 03/2017
'+---------------------------------------------------------------------------------------------
'|00421597-seguro-faturamento-pecuario -ADICIONADO NOVO PRODUTO "1240" NO IF ABAIXO |06/06/18|
    If InStr(",155,156,227,228,229,300,650,701,1152,1201,1204,1210,1226,1227,1240,", "," & vProduto & ",") > 0 Then
      If CDbl("0" & mskValorPrejuizo.Text) > (obter_valor_is(gbllProposta_ID, vProduto, cmbCoberturaAfetada.ItemData(cmbCoberturaAfetada.ListIndex)) * 2) Then
        MsgBox "N�o � permitido que o valor da estimativa seja superior ao dobro da IS (import�ncia segurada) do bem.", vbExclamation
        MousePointer = vbNormal
        Exit Sub
      End If
    End If
'| 19406709 - Melhoria nos limites de sinistro e emissao
'+-----------------------------------------------------------

    If sDecimal = "." Then
        vValor = Format(TrocaValorBrasPorAme(mskValorPrejuizo.Text), "############0.00")
        vEstima = Format(TrocaValorBrasPorAme(grdEstimativasPagamentos_Estimativas.TextMatrix(1, 1)), "############0.00")
    Else
        vValor = mskValorPrejuizo.Text
        vEstima = grdEstimativasPagamentos_Estimativas.TextMatrix(1, 1)
    End If


    '---------- NTendencia - 07/11/2019 - Valida��o efetuada no in�cio dessa sub
    ''* cristovao.rodrigues
    'If strTipoProcesso <> strTipoProcesso_Avisar _
    '   And strTipoProcesso <> strTipoProcesso_Avisar_Cosseguro Then
'
'        If Verifica_valor_estimado(cmbCoberturaAfetada.ItemData(cmbCoberturaAfetada.ListIndex), CDbl(vValor)) Then
'            '   Apenas exibe a mensagem, n�o deve sair da fun��o.
'            '    Exit Sub
'        End If

'    End If
    '-----------


    grdEstimativasPagamentos_Coberturas.AddItem cmbCoberturaAfetada.Text & vbTab & mskValorPrejuizo.Text & vbTab & "0,00"    ' nCorrecao
    grdEstimativasPagamentos_Coberturas.RowData(grdEstimativasPagamentos_Coberturas.Rows - 1) = cmbCoberturaAfetada.ItemData(cmbCoberturaAfetada.ListIndex)

    'cristovao.rodrigues 18/07/2012
    sSQL = ""
    sSQL = sSQL & " insert into #EstimativaPagamento_CoberturaAfetada "
    sSQL = sSQL & " values ( "
    sSQL = sSQL & cmbCoberturaAfetada.ItemData(cmbCoberturaAfetada.ListIndex) & ","
    sSQL = sSQL & "0.00 ,"
    sSQL = sSQL & Replace(TrocaValorBrasPorAme(vValor), ",", "") & ",'"
    sSQL = sSQL & cmbCoberturaAfetada.Text & "',"
    sSQL = sSQL & "'N')"

    Call Conexao_ExecutarSQL(gsSIGLASISTEMA, _
                             glAmbiente_id, _
                             App.Title, _
                             App.FileDescription, _
                             sSQL, _
                             lConexaoLocal, _
                             False)

    'cristovao.rodrigues 18/07/2012
    grdEstimativasPagamentos_Estimativas.TextMatrix(1, 1) = Format(CDbl(vValor) + CDbl(vEstima), "###,###,##0.00")
    Call grdEstimativasPagamentos_Estimativas_Recalcula(1)

    cmbCoberturaAfetada.ListIndex = -1
    mskValorPrejuizo.Text = "0,00"
    cmbCoberturaAfetada.SetFocus

    If strTipoProcesso = strTipoProcesso_Avisar Then
        '        Aplica_Alteracoes_Coberturas
    End If

    sAdicionar = True

    MousePointer = vbNormal

    Exit Sub
TrataErro:
    Call TrataErroGeral("btnCoberturaAdicionar", "frmEstimativa")
    Call FinalizarAplicacao

End Sub

Private Sub btnCoberturaAlterar_Click()

    Dim Encontrou As Boolean, i As Integer
    Dim vValor As String, vEstima, vTecnico_id As String
    Dim vValor_anterior As String
    Dim sSQL As String, rs As ADODB.Recordset
    Dim Estimativa_menor_que_IS_via_SEGS7442 As Boolean
    Dim iProdutoExterno As Integer
    Dim Valor_is As Double

    On Error GoTo TrataErro

    MousePointer = vbHourglass
    
    '--- NTendencia - 11/12/2019 - 16:47
    If cmbCoberturaAfetada.ListIndex = -1 Then
        MsgBox "N�o possui Cobertura selecionada."
        cmbCoberturaAfetada.SetFocus
        MousePointer = vbNormal
        Exit Sub
    End If
    
    If vProduto = "400" Then 'NTendencia - 17/12/2019
        iProdutoExterno = Obter_produto_externo(gbllProposta_ID)
        If iProdutoExterno = 0 Or iProdutoExterno <> "999" Then
            MsgBox "Produto n�o possui c�digo de personalizado (produto externo). Proposta com Id = " & lPropostaId & " e cobertura com Id = " & lCoberturaId & "." & " (Produto externo diferente de 999 para produto 400)"
            Exit Sub
        End If
        'busca IS
        Valor_is = Obter_valor_IS_400(gbllProposta_ID, vProduto, cmbCoberturaAfetada.ItemData(cmbCoberturaAfetada.ListIndex), iProdutoExterno)
        'se tiver
        If Valor_is <> 0 Then
            If CDbl("0" & mskValorPrejuizo.Text) > Valor_is Then
                    vTecnico_id = SelecionaDados_TecnicoAtual
                    If Limite_aprovacao_estimativa(vTecnico_id, vProduto, gbllRamo_ID, 1) = False Then
                        MsgBox "Valor da estimativa � superior ao valor da I.S. e n�o h� t�cnico cadastrado para o Produto/Ramo.", vbExclamation
                        Screen.MousePointer = vbNormal
                        Exit Sub
                    End If
            End If
        End If
        'Se valor da IS � igual a zero significa que n�o tem IS, nesse caso deixa alterar
    ElseIf vProduto = "811" Then 'NTendencia - 16/12/2019
        iProdutoExterno = Obter_produto_externo(gbllProposta_ID)
        If iProdutoExterno = 0 Then
            MsgBox "Produto n�o possui c�digo de personalizado (produto externo). Proposta com Id = " & lPropostaId & " e cobertura com Id = " & lCoberturaId & "."
            Exit Sub
        End If
        
        'busca IS
        Valor_is = Obter_valor_IS_811(gbllProposta_ID, vProduto, cmbCoberturaAfetada.ItemData(cmbCoberturaAfetada.ListIndex), iProdutoExterno)
        'se tiver
        If Valor_is <> 0 Then
            If CDbl("0" & mskValorPrejuizo.Text) > Valor_is Then
                    vTecnico_id = SelecionaDados_TecnicoAtual
                    If Limite_aprovacao_estimativa(vTecnico_id, vProduto, gbllRamo_ID, 1) = False Then
                        MsgBox "Valor da estimativa � superior ao valor da I.S. e n�o h� t�cnico cadastrado para o Produto/Ramo.", vbExclamation
                        Screen.MousePointer = vbNormal
                        Exit Sub
                    End If
            End If
        End If
        'Se valor da IS � igual a zero significa que n�o tem IS, nesse caso deixa alterar
    ElseIf vProduto = "680" And cmbCoberturaAfetada.ItemData(cmbCoberturaAfetada.ListIndex) = 527 Then
        'busca IS
        Valor_is = Obter_valor_IS_680(gbllProposta_ID)
        'se tiver
        If Valor_is <> 0 Then
            If CDbl("0" & mskValorPrejuizo.Text) > Valor_is Then
                    vTecnico_id = SelecionaDados_TecnicoAtual
                    If Limite_aprovacao_estimativa(vTecnico_id, vProduto, gbllRamo_ID, 1) = False Then
                        MsgBox "Valor da estimativa � superior ao valor da I.S. e n�o h� t�cnico cadastrado para o Produto/Ramo.", vbExclamation
                        Screen.MousePointer = vbNormal
                        Exit Sub
                    End If
            End If
        End If
        'Se valor da IS � igual a zero significa que n�o tem IS, nesse caso deixa alterar
    ElseIf vProduto = "115" Or vProduto = "150" Or vProduto = "123" Then
        'busca IS
        Valor_is = Obter_valor_IS_115_150(dSinistro_ID)
        'se tiver
        If Valor_is <> 0 Then
            If CDbl("0" & mskValorPrejuizo.Text) > Valor_is Then
                    vTecnico_id = SelecionaDados_TecnicoAtual
                    If Limite_aprovacao_estimativa(vTecnico_id, vProduto, gbllRamo_ID, 1) = False Then
                        MsgBox "Valor da estimativa � superior ao valor da I.S. e n�o h� t�cnico cadastrado para o Produto/Ramo.", vbExclamation
                        Screen.MousePointer = vbNormal
                        Exit Sub
                    End If
            End If
        End If
        'Se valor da IS � igual a zero significa que n�o tem IS, nesse caso deixa alterar
    ElseIf vProduto = "1206" Then
        'busca IS
        Valor_is = Obter_valor_IS_1206(dSinistro_ID, cmbCoberturaAfetada.ItemData(cmbCoberturaAfetada.ListIndex))
        'se tiver
        If Valor_is <> 0 Then
            If CDbl("0" & mskValorPrejuizo.Text) > Valor_is Then
                    vTecnico_id = SelecionaDados_TecnicoAtual
                    If Limite_aprovacao_estimativa(vTecnico_id, vProduto, gbllRamo_ID, 1) = False Then
                        MsgBox "Valor da estimativa � superior ao valor da I.S. e n�o h� t�cnico cadastrado para o Produto/Ramo.", vbExclamation
                        Screen.MousePointer = vbNormal
                        Exit Sub
                    End If
            End If
        End If
        'Se valor da IS � igual a zero significa que n�o tem IS, nesse caso deixa alterar
    ElseIf vProduto = "721" Then
        'busca IS
        Valor_is = Obter_valor_IS_721(dSinistro_ID, cmbCoberturaAfetada.ItemData(cmbCoberturaAfetada.ListIndex))
        'se tiver
        If Valor_is <> 0 Then
            If CDbl("0" & mskValorPrejuizo.Text) > Valor_is Then
                    vTecnico_id = SelecionaDados_TecnicoAtual
                    If Limite_aprovacao_estimativa(vTecnico_id, vProduto, gbllRamo_ID, 1) = False Then
                        MsgBox "Valor da estimativa � superior ao valor da I.S. e n�o h� t�cnico cadastrado para o Produto/Ramo.", vbExclamation
                        Screen.MousePointer = vbNormal
                        Exit Sub
                    End If
            End If
        End If
    ElseIf vProduto = "1204" Then
        'busca IS
        Valor_is = Obter_valor_IS_1204(dSinistro_ID, cmbCoberturaAfetada.ItemData(cmbCoberturaAfetada.ListIndex), gbllProposta_ID)
        'se tiver
        If Valor_is <> 0 Then
            If CDbl("0" & mskValorPrejuizo.Text) > Valor_is Then
                    vTecnico_id = SelecionaDados_TecnicoAtual
                    If Limite_aprovacao_estimativa(vTecnico_id, vProduto, gbllRamo_ID, 1) = False Then
                        MsgBox "Valor da estimativa � superior ao valor da I.S. e n�o h� t�cnico cadastrado para o Produto/Ramo.", vbExclamation
                        Screen.MousePointer = vbNormal
                        Exit Sub
                    End If
            End If
        End If
    Else
        'Se valor da IS � igual a zero significa que n�o tem IS, nesse caso deixa alterar
    'Se sinistro n�o tem sinistro l�der, verifica estimativa
    If Not Tem_Sinistro_lider(dSinistro_ID) Then
    
    vTecnico_id = SelecionaDados_TecnicoAtual
    
    'Se ramo � vida
    If Ramo_Vida(gbllRamo_ID) Then
    
                'Produto 718 entra nessa valida��o
            
            
        'Recebe valor IS
        Valor_is = Obter_valor_IS_Vida(gbllProposta_ID, cmbCoberturaAfetada.ItemData(cmbCoberturaAfetada.ListIndex))
                If Valor_is = 0 Then 'N�o tem IS
                    'N�o aplica regra
                Else
        'Se valor da estimativa � maior que valor da IS ent�o
        If CDbl("0" & mskValorPrejuizo.Text) > Valor_is Then
            If Limite_aprovacao_estimativa(vTecnico_id, vProduto, gbllRamo_ID, 1) = False Then
                MsgBox "Valor da estimativa � superior ao valor da I.S. e n�o h� t�cnico cadastrado para o Produto/Ramo.", vbExclamation
                Screen.MousePointer = vbNormal
                Exit Sub
            End If
        End If
                End If
    Else
        'Se ramo n�o � vida
            'H� duas formas de calcular a IS para produto n�o Vida. Atrav�s da fun��o obter_valor_is e atrav�s da procedure SEGS7442_SPS.
            'Por quest�o de seguran�a utilizo as duas formas aqui
    Valor_is = (obter_valor_is(gbllProposta_ID, vProduto, cmbCoberturaAfetada.ItemData(cmbCoberturaAfetada.ListIndex)))
    Estimativa_menor_que_IS_via_SEGS7442 = Valida_IS_via_SEGS7442(grdEstimativasPagamentos_Coberturas.RowData(grdEstimativasPagamentos_Coberturas.Row), CDbl("0" & mskValorPrejuizo.Text))
    
                '----NTendencia - 27/102/2019
                'N�o aplica regra se n�o tem IS
                If Valor_is <> 0 Then
                '---
    'Se valor do preju�zo � maior que IS, verifica se tem t�cnico
    If CDbl("0" & mskValorPrejuizo.Text) > Valor_is Or Not Estimativa_menor_que_IS_via_SEGS7442 Then
         If Limite_aprovacao_estimativa(vTecnico_id, vProduto, gbllRamo_ID, 1) = False Then
            MsgBox "Valor da estimativa � superior ao valor da I.S. e n�o h� t�cnico cadastrado para o Produto/Ramo.", vbExclamation
            Screen.MousePointer = vbNormal
            Exit Sub
        End If
    End If
        End If
    End If
        End If
    End If 'Tratamento 115/150
    '--- NTendencia - 11/12/2019 --------------------------
    
    '----
    'NTendencia - 10/12/2019
    'Foi retirado o produto 680 da verifica��o abaixo para receber tratativa diferencia no c�digo acima
    '----
    
    
    '+-----------------------------------------------------------
    '| 19406709 - Melhoria nos limites de sinistro e emissao
    '| RN04 - petrauskas - 03/2017
    '+---------------------------------------------------------------------------------------------
    '|00421597-seguro-faturamento-pecuario -ADICIONADO NOVO PRODUTO "1240" NO IF ABAIXO |06/06/18|
    If InStr(",155,156,227,228,229,300,650,701,1152,1201,1204,1210,1226,1227,1240,", "," & vProduto & ",") > 0 Then
      If CDbl("0" & mskValorPrejuizo.Text) > (obter_valor_is(gbllProposta_ID, vProduto, cmbCoberturaAfetada.ItemData(cmbCoberturaAfetada.ListIndex)) * 2) Then
        MsgBox "N�o � permitido que o valor da estimativa seja superior ao dobro da IS (import�ncia segurada) do bem.", vbExclamation
        MousePointer = vbNormal
        Exit Sub
      End If
    End If
    '| 19406709 - Melhoria nos limites de sinistro e emissao
    '+-----------------------------------------------------------

    If cmbCoberturaAfetada.ListIndex = -1 Then
        MsgBox "N�o possui Cobertura selecionada."
        cmbCoberturaAfetada.SetFocus
        MousePointer = vbNormal
        Exit Sub
    End If

    If CDbl(mskValorPrejuizo.Text) <= 0 Then
        MsgBox "Valor Preju�zo incorreto."
        mskValorPrejuizo.SetFocus
        MousePointer = vbNormal
        Exit Sub
    End If

    'Checa se a cobertura j� est� no grid
    Encontrou = False
    If grdEstimativasPagamentos_Coberturas.Rows > 1 Then
        For i = 1 To grdEstimativasPagamentos_Coberturas.Rows - 1
            If cmbCoberturaAfetada.ItemData(cmbCoberturaAfetada.ListIndex) = grdEstimativasPagamentos_Coberturas.RowData(i) Then
                Encontrou = True
                If sDecimal = "." Then
                    vValor_anterior = Format(TrocaValorBrasPorAme(grdEstimativasPagamentos_Coberturas.TextMatrix(i, 1)), "############0.00")
                    vValor = Format(TrocaValorBrasPorAme(mskValorPrejuizo.Text), "############0.00")
                    vEstima = Format(TrocaValorBrasPorAme(grdEstimativasPagamentos_Estimativas.TextMatrix(1, 1)), "############0.00")
                Else
                    vValor_anterior = grdEstimativasPagamentos_Coberturas.TextMatrix(i, 1)
                    vValor = mskValorPrejuizo.Text
                    vEstima = grdEstimativasPagamentos_Estimativas.TextMatrix(1, 1)
                End If

                'cristovao.rodrigues 03/10/2012 strtipoprocesso estava tipooperacao
                If strTipoProcesso <> strTipoProcesso_Avisar Then

                    'sSQL = "SELECT  a.tp_cobertura_id, b.situacao_op, total = ISNULL(b.val_acerto,0) " 'cristovao.rodrigues 05/04/2013 linha substituida
                    sSQL = "SELECT  a.tp_cobertura_id, total = sum(ISNULL(b.val_acerto,0)) "
                    sSQL = sSQL & "FROM pgto_sinistro_cobertura_tb a WITH (NOLOCK), pgto_sinistro_tb b WITH (NOLOCK) "
                    sSQL = sSQL & "WHERE a.Sinistro_id = " & dSinistro_ID
                    sSQL = sSQL & "  and a.sinistro_id = b.sinistro_id"
                    sSQL = sSQL & "  and a.num_parcela = b.num_parcela"
                    sSQL = sSQL & "  and a.seq_pgto = b.seq_pgto"
                    sSQL = sSQL & "  and a.item_val_estimativa = b.item_val_estimativa"
                    'Cleber Sardo - In�cio - 17/11/2014 - INC000004453549 - Corrigindo a altera��o da Indeniza��o
                    'sSQL = sSQL & "  and a.seq_estimativa = b.seq_estimativa"
                    sSQL = sSQL & "  AND a.item_val_estimativa = 1"
                    'Cleber Sardo - Fim - 17/11/2014 - INC000004453549 - Corrigindo a altera��o da Indeniza��o
                    sSQL = sSQL & "  and a.beneficiario_id = b.beneficiario_id"
                    sSQL = sSQL & "  and a.seq_estimativa = b.seq_estimativa"
                    sSQL = sSQL & "  and b.situacao_op not in ('c', 'e')"
                    sSQL = sSQL & "  and a.tp_cobertura_id = " & grdEstimativasPagamentos_Coberturas.RowData(grdEstimativasPagamentos_Coberturas.Row)
                    sSQL = sSQL & "  group by a.tp_cobertura_id " 'cristovao.rodrigues 05/04/2013
                    
                    Set rs = Conexao_ExecutarSQL(gsSIGLASISTEMA, _
                                                 glAmbiente_id, _
                                                 App.Title, _
                                                 App.FileDescription, _
                                                 sSQL, _
                                                 lConexaoLocal, _
                                                 True)

                    If Not rs.EOF Then
                        If CDbl(vValor) < Val(rs!total) Then
                            MsgBox "N�o � permitido alterar a estimativa para um valor menor do que j� foi pago para esta cobertura."
                            rs.Close
                            MousePointer = vbNormal
                            Exit Sub
                        End If
                    End If

                    rs.Close

                End If

                'NTendencia - C00190161 - 07/11/2019 - O "if" abaixo foi suprimido. A verifica��o de valor estimado passou a ser feita no in�cio dessa sub
                ''AKIO.OKUNO - INC4077419 - INICIO - 28/08/2013
                'If Verifica_valor_estimado(grdEstimativasPagamentos_Coberturas.RowData(grdEstimativasPagamentos_Coberturas.Row), CDbl(vEstima)) Then
                'End If
                ''AKIO.OKUNO - INC4077419 - FIM - 28/08/2013
                
                grdEstimativasPagamentos_Coberturas.TextMatrix(i, 1) = mskValorPrejuizo.Text
                grdEstimativasPagamentos_Estimativas.TextMatrix(1, 1) = Format(CDbl(vValor) + CDbl(vEstima) - CDbl(vValor_anterior), "###,###,##0.00")
                'cristovao.rodrigues 05/04/2013
                grdEstimativasPagamentos_Estimativas.TextMatrix(1, 3) = Format(CDbl(vValor - grdEstimativasPagamentos_Estimativas.TextMatrix(1, 2)), "###,###,##0.00")
                'Call grdEstimativasPagamentos_Estimativas_Recalcula(1)
                
                
                'cristovao.rodrigues 19/07/2012
                sSQL = ""
                sSQL = sSQL & " select * from #EstimativaPagamento_CoberturaAfetada "
                sSQL = sSQL & " where tp_cobertura_id = " & grdEstimativasPagamentos_Coberturas.RowData(grdEstimativasPagamentos_Coberturas.Row)

                Set rs = Conexao_ExecutarSQL(gsSIGLASISTEMA, _
                                             glAmbiente_id, _
                                             App.Title, _
                                             App.FileDescription, _
                                             sSQL, _
                                             lConexaoLocal, _
                                             True)

                If rs!Tipo <> "N" Then

                    sSQL = ""
                    sSQL = sSQL & " update #EstimativaPagamento_CoberturaAfetada "
                    sSQL = sSQL & " set "
                    sSQL = sSQL & " estimado = " & Replace(TrocaValorBrasPorAme(vValor), ",", "") & ","
                    sSQL = sSQL & " tipo = 'A' " & vbCrLf
                    sSQL = sSQL & " where "
                    sSQL = sSQL & " tp_cobertura_id = " & grdEstimativasPagamentos_Coberturas.RowData(grdEstimativasPagamentos_Coberturas.Row)

                Else

                    sSQL = ""
                    sSQL = sSQL & " update #EstimativaPagamento_CoberturaAfetada "
                    sSQL = sSQL & " set "
                    sSQL = sSQL & " estimado = " & Replace(TrocaValorBrasPorAme(vValor), ",", "") & vbCrLf
                    sSQL = sSQL & " where "
                    sSQL = sSQL & " tp_cobertura_id = " & grdEstimativasPagamentos_Coberturas.RowData(grdEstimativasPagamentos_Coberturas.Row)

                End If

                Conexao_ExecutarSQL gsSIGLASISTEMA, _
                                    glAmbiente_id, _
                                    App.Title, _
                                    App.FileDescription, _
                                    sSQL, _
                                    lConexaoLocal, _
                                    False

                Call grdEstimativasPagamentos_Estimativas_Recalcula(1)

            End If

        Next

    End If

    If Not Encontrou Then
        MsgBox "Cobertura n�o encontrada para altera��o!"
        MousePointer = vbNormal
        Exit Sub
    End If

    cmbCoberturaAfetada.ListIndex = -1
    mskValorPrejuizo.Text = "0,00"
    cmbCoberturaAfetada.SetFocus

    If strTipoProcesso = strTipoProcesso_Avisar Then
        'Aplica_Alteracoes_Coberturas
    End If

    Set rs = Nothing

    sAlterar = True

    MousePointer = vbNormal

    Exit Sub
TrataErro:
    Call TrataErroGeral("btnCoberturaAlterar", "frmEstimativa")
    Call FinalizarAplicacao
End Sub

Public Function BuscaLogin(sCPF As String, Optional ByVal vintConectaAB As Integer = 0)
    'NTendencia - Vagner - 15/10/2019 - Fun��o copiada do SEGP1293
    
    Dim rsRecordSet As ADODB.Recordset
    Dim sSql_Servidor As String

    MousePointer = vbHourglass

    If vintConectaAB = 1 Then
        '        sSql_Servidor = "ABBS."
        sSql_Servidor = "ABSS."
    End If

    sSQL = ""
    sSQL = sSQL & "Select Login_Rede " & vbNewLine
    sSQL = sSQL & "  From " & sSql_Servidor & "SegAb_Db.dbo.Usuario_Tb WITH (NOLOCK) " & vbNewLine
    sSQL = sSQL & " Where CPF = '" & sCPF & "'" & vbNewLine
    sSQL = sSQL & "   and Situacao = 'A'"
    Set rsRecordSet = Conexao_ExecutarSQL(gsSIGLASISTEMA, _
                                          glAmbiente_id, _
                                          App.Title, _
                                          App.FileDescription, _
                                          sSQL, _
                                          lConexaoLocal, True)
    With rsRecordSet
        If .EOF Then
            BuscaLogin = ""
        Else
            If IsNull(Trim(.Fields(0))) Then
                BuscaLogin = ""
            Else
                BuscaLogin = Trim(.Fields(0))
            End If
        End If
    End With

    MousePointer = vbNormal

    Set rsRecordSet = Nothing

    Exit Function

Erro:
    Call TrataErroGeral("BuscaLogin", "frmEstimativa.frm")
    Call FinalizarAplicacao

End Function


Private Function SelecionaDados_TecnicoAtual() As Integer
    'NTendencia - Vagner - 15/10/2019 - Fun��o copiada do SEGP1293
    
    Dim sSQL As String
    Dim rsRecordSet As ADODB.Recordset

    On Error GoTo Erro

    If glAmbiente_id = 2 Or glAmbiente_id = 3 Then
        sLoginRede = BuscaLogin(gsCPF)    'Passando CPF para buscar
    Else
        sLoginRede = BuscaLogin(cUserName, 1)    'Passando CPF para buscar
    End If

    sSQL = ""
    sSQL = sSQL & "Select Tecnico_ID"
    sSQL = sSQL & "  From Tecnico_tb WITH (NOLOCK)"
    sSQL = sSQL & " Where case CHARINDEX('.', email) when 0 "
    sSQL = sSQL & "           then email "
    sSQL = sSQL & "           else LEFT(email,(CHARINDEX('.', email)-1)) "
    sSQL = sSQL & "       end = '" & Trim(sLoginRede) & "'"

    Set rsRecordSet = Conexao_ExecutarSQL(gsSIGLASISTEMA _
                    , glAmbiente_id _
                    , App.Title _
                    , App.FileDescription _
                    , sSQL _
                    , lConexaoLocal _
                    , True)
    With rsRecordSet
        If Not .EOF Then
            SelecionaDados_TecnicoAtual = .Fields!tecnico_id
        'Else 'cristovao.rodrigues 31/03/2016 16429073 PMBC Amparo Familiar -- teste
        '    iTecnico_atual = 543
        End If
    End With

    Set rsRecordSet = Nothing

    Exit Function

Erro:
    TrataErroGeral "SelecionaDados_TecnicoAtual", Me.name
    FinalizarAplicacao

End Function

'cristovao.rodrigues 03/10/2012
Private Function ValidaValorEstimativas() As Boolean
    Dim i As Integer
    Dim nIndice As Integer
    Dim vValor As String
    Dim vPago As String
    Dim vResseguro As String

    On Error GoTo TrataErro

    ValidaValorEstimativas = False
    nIndice = IIf(bytTipoRamo = 1, 3, 7)

    With grdEstimativasPagamentos_Estimativas

        For i = 1 To nIndice

            If sDecimal = "." Then
                vValor = Format(TrocaValorBrasPorAme(.TextMatrix(i, 1)), "############0.00")
                vPago = Format(TrocaValorBrasPorAme(.TextMatrix(i, 2)), "############0.00")
                vResseguro = Format(TrocaValorBrasPorAme(.TextMatrix(i, 4)), "############0.00")
            Else
                vValor = .TextMatrix(i, 1)
                vPago = .TextMatrix(i, 2)
                vResseguro = .TextMatrix(i, 4)
            End If

            If CDbl(vValor) < CDbl(vPago) Then
                mensagem_erro 6, "Valor estimado menor que o valor j� pago/recebido!"
                Exit Function
            End If

            If CDbl(vValor) = 0 And CDbl(vResseguro) > 0 Then
                mensagem_erro 6, "Resseguro maior que zero para estimativa igual a zero!"
                '            If i <> 1 Then
                '                mskResseguro(i).SetFocus
                '            End If
                Exit Function
            End If

            'AKIO.OKUNO - INICIO - 04/10/2012
            If grdEstimativasPagamentos_Coberturas.Rows <= 1 Then
                MsgBox "Campo COBERTURAS AFETADAS inv�lido ou n�o informado !", 16, "Mensagem ao Usu�rio"
                cmbCoberturaAfetada.SetFocus
                Exit Function
            End If
            'AKIO.OKUNO - FIM - 04/10/2012

        Next

    End With

    ValidaValorEstimativas = True

    Exit Function
TrataErro:
    Call TrataErroGeral("ValidaValorEstimativas", "frmEstimativa")
    Call FinalizarAplicacao

End Function

Private Sub btnCoberturaRemover_Click()

    Dim vValor As String
    Dim vEstima As String
    Dim sSQL As String
    Dim rs As ADODB.Recordset

    On Error GoTo TrataErro

    MousePointer = vbHourglass

    ' If grdEstimativasPagamentos_Estimativas.Rows > 1 Then
    If grdEstimativasPagamentos_Coberturas.Rows > 1 And cmbCoberturaAfetada.ListIndex <> -1 Then
        If strTipoProcesso <> strTipoProcesso_Avisar Then  'cristovao.rodrigues 03/10/2012

            sSQL = "SELECT  a.tp_cobertura_id, b.situacao_op, b.val_acerto "
            sSQL = sSQL & "FROM pgto_sinistro_cobertura_tb a WITH (NOLOCK), pgto_sinistro_tb b WITH (NOLOCK) "
            sSQL = sSQL & "WHERE a.Sinistro_id = " & dSinistro_ID
            sSQL = sSQL & "  and a.sinistro_id = b.sinistro_id"
            sSQL = sSQL & "  and a.num_parcela = b.num_parcela"
            sSQL = sSQL & "  and a.seq_pgto = b.seq_pgto"
            sSQL = sSQL & "  and a.item_val_estimativa = b.item_val_estimativa"
            sSQL = sSQL & "  and a.seq_estimativa = b.seq_estimativa"
            sSQL = sSQL & "  and a.beneficiario_id = b.beneficiario_id"
            sSQL = sSQL & "  and a.seq_estimativa = b.seq_estimativa"
            sSQL = sSQL & "  and b.situacao_op <> 'c'"
            sSQL = sSQL & "  and a.tp_cobertura_id = " & grdEstimativasPagamentos_Coberturas.RowData(grdEstimativasPagamentos_Coberturas.Row)

'AKIO.OKUNO - INICIO - 27/12/2012
'            Set rs = ExecutarSQL(gsSIGLASISTEMA, _
                                 glAmbiente_id, _
                                 App.Title, _
                                 App.FileDescription, _
                                 sSQL, _
                                 True)
            Set rs = Conexao_ExecutarSQL(gsSIGLASISTEMA, _
                                 glAmbiente_id, _
                                 App.Title, _
                                 App.FileDescription, _
                                 sSQL, _
                                 lConexaoLocal, _
                                 True)
'AKIO.OKUNO - FIM - 27/12/2012

            If Not rs.EOF Then
                MsgBox "Imposs�vel excluir. Existem pagamentos para essa cobertura."
                MousePointer = vbNormal
                rs.Close
                Exit Sub
            End If
            'rs.Close
        End If

        If sDecimal = "." Then
            vValor = Format(TrocaValorBrasPorAme(flexCoberturas.TextMatrix(flexCoberturas.Row, 1)), "############0.00")
            vEstima = Format(TrocaValorBrasPorAme(mskValorPrejuizo.Text), "############0.00")
        Else
            vValor = mskValorPrejuizo.Text
            vEstima = grdEstimativasPagamentos_Estimativas.TextMatrix(1, 1)
        End If
        'cristovao.rodrigues 02/10/2012 tracar de posicao
        '        If grdEstimativasPagamentos_Coberturas.Rows = 2 Then
        '            grdEstimativasPagamentos_Coberturas.Rows = 1
        '        Else

        'cristovao.rodrigues 19/07/2012
        sSQL = ""
        sSQL = sSQL & " select * from #EstimativaPagamento_CoberturaAfetada "
        sSQL = sSQL & " where tp_cobertura_id = " & grdEstimativasPagamentos_Coberturas.RowData(grdEstimativasPagamentos_Coberturas.Row)

        Set rs = Conexao_ExecutarSQL(gsSIGLASISTEMA, _
                                     glAmbiente_id, _
                                     App.Title, _
                                     App.FileDescription, _
                                     sSQL, _
                                     lConexaoLocal, _
                                     True)


        If rs!Tipo = "N" Then

            sSQL = ""
            sSQL = sSQL & " delete  #EstimativaPagamento_CoberturaAfetada "
            sSQL = sSQL & " where tp_cobertura_id = " & grdEstimativasPagamentos_Coberturas.RowData(grdEstimativasPagamentos_Coberturas.Row)

            Call Conexao_ExecutarSQL(gsSIGLASISTEMA, _
                                     glAmbiente_id, _
                                     App.Title, _
                                     App.FileDescription, _
                                     sSQL, _
                                     lConexaoLocal, _
                                     False)

        Else

            sSQL = ""
            sSQL = sSQL & " update  #EstimativaPagamento_CoberturaAfetada set "
            sSQL = sSQL & " tipo = 'D'"
            sSQL = sSQL & " where tp_cobertura_id = " & grdEstimativasPagamentos_Coberturas.RowData(grdEstimativasPagamentos_Coberturas.Row)

            Call Conexao_ExecutarSQL(gsSIGLASISTEMA, _
                                     glAmbiente_id, _
                                     App.Title, _
                                     App.FileDescription, _
                                     sSQL, _
                                     lConexaoLocal, _
                                     False)

        End If

        'cristovao.rodrigues 02/10/2012
        If grdEstimativasPagamentos_Coberturas.Rows = 2 Then
            grdEstimativasPagamentos_Coberturas.Rows = 1
        Else
            grdEstimativasPagamentos_Coberturas.RemoveItem (grdEstimativasPagamentos_Coberturas.Row)
        End If

        'End If

        vValor = Format(CDbl(vEstima) - CDbl(vValor), "###,###,##0.00")
        grdEstimativasPagamentos_Estimativas.TextMatrix(1, 1) = vValor

        grdEstimativasPagamentos_Estimativas.TextMatrix(1, 3) = Format(CDbl(vValor - grdEstimativasPagamentos_Estimativas.TextMatrix(1, 2)), "###,###,##0.00")

        Call grdEstimativasPagamentos_Estimativas_Recalcula(1)

    End If

    cmbCoberturaAfetada.ListIndex = -1
    mskValorPrejuizo.Text = "0,00"
    cmbCoberturaAfetada.SetFocus

    If strTipoProcesso = strTipoProcesso_Avisar Then
        'Aplica_Alteracoes_Coberturas
    End If

    sExcluir = True
    'rs.Close

    MousePointer = vbNormal

    Exit Sub
    Resume
TrataErro:
    Call TrataErroGeral("btnCoberturaRemover", "frmEstimativa")
    Call FinalizarAplicacao

End Sub

Private Sub btnOK_Estimativa_Click()

    Dim sSQL As String
    Dim Mensagem As String
    Dim lOk As Boolean
    Dim intAltera_Coberturas As Integer     'AKIO.OKUNO - 06/05/2013
    Dim intAltera_Estimativas As Integer    'AKIO.OKUNO - 06/05/2013

    lOk = True
    sSQL = ""

    On Error GoTo TrataErro


''+-----------------------------------------------------------
''| 19406709 - Melhoria nos limites de sinistro e emissao
''| RN04 - petrauskas - 03/2017
'    If InStr(",155,156,227,228,229,300,650,680,701,1152,1201,1204,1210,1226,1227,", "," & vProduto & ",") > 0 Then
'      If CDbl("0" & ObterValorTotalEstimadoCobertura) > (obter_valor_is(gbllProposta_ID, vProduto) * 2) Then
'        MsgBox "N�o � permitido que o valor da estimativa seja superior ao dobro da IS (import�ncia segurada) do bem.", vbExclamation
'
'        Exit Sub
'      End If
'    End If
''| 19406709 - Melhoria nos limites de sinistro e emissao
''| ALTERADO O ESCOPO DO PROJETO - VALIDA��O APLICADA AGORA NOS BOT�ES DE ADICIONAR/ALTERAR COBERTURAS ANTES DO BOT�O APLICAR
''+-----------------------------------------------------------
    
    'cristovao.rodrigues 03/10/2012
    If Not ValidaValorEstimativas Then
        Exit Sub
    End If

    ' Pede confirma��o
    If MsgBox("Confirma altera��o de valores ? ", vbYesNo, "Aten��o !") = vbNo Then
        Exit Sub
    Else

        If ObterValorTotalEstimadoCobertura <> grdEstimativasPagamentos_Estimativas.TextMatrix(1, 1) Then
            MsgBox "Valor total estimado de cobertura diferente do valor da indeniza��o!", vbInformation
            Exit Sub
        End If

        MousePointer = vbHourglass

        '''''''''''''''''' cristovao.rodrigues 02/10/2012 incluida verificacao para AVALIAR
        If strTipoProcesso = strTipoProcesso_Avisar _
           Or strTipoProcesso = strTipoProcesso_Avisar_Cosseguro _
           Or strTipoProcesso = strTipoProcesso_Avaliar _
           Or strTipoProcesso = strTipoProcesso_AvaliastrTipoProcesso_Avaliar_Cosseguro _
           Or strTipoProcesso = strTipoProcesso_Avaliar_Com_Imagem Then    'Neste caso a estimativa ainda n�o foi para a cole��o
            If CDbl(grdEstimativasPagamentos_Estimativas.TextMatrix(1, 1)) = 0 Then
                mensagem_erro 6, "A primeira estimativa deve ter valor de indeniza��o!"
                FlagOk = False
                MousePointer = vbNormal
                Exit Sub
            End If
            '                    btnEstimativaAdicionar.Caption = "Ok"
            '                    btnEstimativaAdicionar_Click        'Faz valida��o da estimativa
            '                    If vErroEstimativa Then Exit Sub    'Ocorreu erro
        
        '-------------------------------------------------------------------------
        'Ricardo Toledo (Confitec) : 11/06/2013 : INC000004077419 : Inicio
        'Conforme confirmado com o analista Akio Okuno, n�o h� necessidade do else
        'Else
        'Ricardo Toledo (Confitec) : 11/06/2013 : INC000004077419 : fim
        '-------------------------------------------------------------------------
            
            '                    For Each Cob In CoberturasAfetadas
            For x = 1 To grdEstimativasPagamentos_Coberturas.Rows - 1
                If sDecimal = "." Then
                    vValor = Format(TrocaValorBrasPorAme(grdEstimativasPagamentos_Coberturas.TextMatrix(x, 1)), "############0.00")
                Else
                    vValor = Format(grdEstimativasPagamentos_Coberturas.TextMatrix(x, 1), "############0.00")
                End If
                
                '--- NTendencia - C00190161 - 07/11/2019 - A valida��o abaixo � efetuada no bot�o btnCoberturaAlterar
                'If Val(grdEstimativasPagamentos_Estimativas.TextMatrix(1, 1)) > 0 Then
                '    If Verifica_valor_estimado(grdEstimativasPagamentos_Coberturas.RowData(x), grdEstimativasPagamentos_Coberturas.TextMatrix(x, 1)) Then
                '        '   Apenas exibe a mensagem, n�o deve sair da fun��o.
                '        '    Exit Sub
                '    End If
                '--- End If
                
            Next
        End If

        '''''''''''''''''''

        ''* cristovao.rodrigues
        '        If Verifica_valor_estimado(Cob.tp_Cobertura_id, Cob.Val_Estimativa) Then
        '        '   Apenas exibe a mensagem, n�o deve sair da fun��o.
        '        End If

        '        Inativar_Sinistros
        '''        'Monta_TelaEstimativa nSeq_Estimativa  'vai pegar os valoes da seq_estimativa

        If sAlterar Or sAdicionar Or sExcluir Then

        'AKIO.OKUNO - INICIO - 06/05/2013
        '            If Altera_Coberturas Then
        '                If Altera_Estimativas Then
            intAltera_Coberturas = Altera_Coberturas
            If intAltera_Coberturas = 0 Then
                intAltera_Estimativas = Altera_Estimativas
                    
                'C00216281-ESTEIRA-PAGAMENTO-IMEDIATO-VIDA-INDIVIDUAL
                'CLASSIFICA��O DO SINISTRO
                'Valida produto/ramo/cobertura/evento
                If Valida_Fluxo_Pagamento_Imediato Then
                        'CLASSIFICA E GRAVA O DETALHAMENTO
                       If Classifica_Pagamento_Imediato Then
                        PgtoImediatoSituacao = 1
                       End If
                      Gravar_classificacao_Pagamento_Imediato
                End If
                
                If PgtoImediatoSituacao = 1 Then
                                        'INICIA GRAVA��O DO HISTORICO
                    Grava_Historico_Classificacao_Sinistro
                End If
                'C00216281 - FIM
                    
                If intAltera_Estimativas = 0 Then
        'AKIO.OKUNO - FIM - 06/05/2013
                    sSQL = "commit"
                    txtAuxiliar.Text = "OK"

                    sAlterar = False
                    sAdicionar = False
                    sExcluir = False
                    'Mensagem = "Altera��o efetuada."       'MATHAYDE (POR AKIO.OKUNO - 27/09/2012)
                    Mensagem = "Estimativa registrada com sucesso!."
                Else
                    lOk = False
                    'MsgBox "Erro na Altera��o da Estimativa" 'cristovao.rodrigues comentada
                End If

            Else

                lOk = False
                Mensagem = "Erro na Altera��o de Cobertura"

            End If

            If Not lOk Then
                sSQL = "rollback"
                Mensagem = "Altera��o n�o foi efetuada."
                'txtAuxiliar.Text = "Cancelado"
            End If


            Call Conexao_ExecutarSQL(gsSIGLASISTEMA, _
                                     glAmbiente_id, _
                                     App.Title, _
                                     App.FileDescription, _
                                     sSQL, _
                                     lConexaoLocal, _
                                     False)
            'AKIO.OKUNO - INICIO - 06/05/2013
            If Not lOk Then
                Select Case intAltera_Coberturas
                    Case 1
                        Mensagem = "Erro Inclus�o Evento Exclus�o Cobertura Afetada"
                    Case 2
                        Mensagem = "Erro Inclus�o Evento Inclus�o Cobertura Afetada"
                    Case 10000
                        Mensagem = "Altera��o n�o foi efetuada."
                End Select
                Select Case intAltera_Estimativas
                    Case 1
                        Mensagem = "Erro Inclus�o Evento Altera��o de Estimativa"
                    Case 10000
                        Mensagem = "Altera��o n�o foi efetuada."
                End Select
            End If
            'AKIO.OKUNO - FIM - 06/05/2013
        Else

            Mensagem = "Nenhuma altera��o foi efetuada."

        End If

        If lOk Then
            btnCoberturaAdicionar.Enabled = False
            btnCoberturaAlterar.Enabled = False
            btnCoberturaRemover.Enabled = False
        End If

        MousePointer = vbNormal
        MsgBox Mensagem
        
        
        'C00216281-ESTEIRA-PAGAMENTO-IMEDIATO-VIDA-INDIVIDUAL
        If PgtoImediatoSituacao = 1 And lOk Then
            frmMsgPgtoImediato.Show vbModal, Me
        End If
        'C00216281 - FIM
        
        btnVoltar_Estimativa_Click

    End If

    Exit Sub

TrataErro:
    Call TrataErroGeral("btnOK_Estimativa", "frmEstimativa")
    Call FinalizarAplicacao

Resume
End Sub
'AKIO.OKUNO - Altera_Coberturas - Altera��o tipo de valor do retorno - 06/05/2013
Public Function Altera_Coberturas() As Integer
'Public Function Altera_Coberturas() As Boolean

    Dim rs As ADODB.Recordset
    Dim rsRecordSet As ADODB.Recordset
    Dim vSql As String
    Dim vAlterou As Boolean
    Dim vValor As String
    Dim vValorTemp As Double
    Dim Itens_historico(0) As String

'    Altera_Coberturas = True   'AKIO.OKUNO - 06/05/2013
    Altera_Coberturas = 0       'AKIO.OKUNO - 06/05/2013
    vAlterou = False
    On Error GoTo Trata_Erro

    vSql = ""
    vSql = vSql & " begin tran "
    vSql = vSql & "Select * " & vbCrLf
    vSql = vSql & "  From #EstimativaPagamento_CoberturaAfetada " & vbCrLf
    vSql = vSql & " Order By Tp_Cobertura_Nome"

    Set rsRecordSet = Conexao_ExecutarSQL(gsSIGLASISTEMA, _
                                          glAmbiente_id, _
                                          App.Title, _
                                          App.FileDescription, _
                                          vSql, _
                                          lConexaoLocal, _
                                          True)
    If Not rsRecordSet.EOF Then
        Do While Not rsRecordSet.EOF

            If rsRecordSet!Tipo = "D" Then    'excluida
                'cristovao.rodrigues 03/09/2012
                Itens_historico(0) = "COBERTURA EXCLU�DA: " & rsRecordSet!Tp_Cobertura_Nome
'AKIO.OKUNO - INICIO - 06/05/2013
'                If Not Inclui_Evento("10016", SinSituacao_id, "NULL", Itens_historico) Then
'                    Altera_Coberturas = False
'                    MsgBox "Erro Inclus�o Evento Exclus�o Cobertura Afetada", vbOKOnly + vbCritical
                If Not Inclui_Evento("10016", SinSituacao_id, "NULL", Itens_historico, , , sSinistro_BB, , , , , , , , , lConexaoLocal, "SEGP1287", bytGTR_Implantado) Then
                    Altera_Coberturas = 1
'AKIO.OKUNO - FIM - 06/05/2013
                    Exit Function
                End If
                vSql = "EXEC sinistro_cobertura_spd " & _
                       dSinistro_ID & "," & _
                       SinApolice_Id & "," & _
                       SinSucursal_Id & "," & _
                       SinSeguradora_Id & "," & _
                       gbllRamo_ID & "," & _
                       rsRecordSet!tp_Cobertura_id & ",'" & _
                       cUserName & "'"
'AKIO.OKUNO - INICIO - 27/12/2012
'                ExecutarSQL gsSIGLASISTEMA, _
                            glAmbiente_id, _
                            App.Title, _
                            App.FileDescription, _
                            vSql, _
                            lConexaoLocal, _
                            False
                Conexao_ExecutarSQL gsSIGLASISTEMA, _
                            glAmbiente_id, _
                            App.Title, _
                            App.FileDescription, _
                            vSql, _
                            lConexaoLocal, _
                            False
'AKIO.OKUNO - FIM - 27/12/2012

            End If


            If rsRecordSet!Tipo <> "D" Then

                If rsRecordSet!Tipo = "N" Then
                    'cristovao.rodrigues 03/09/2012
                    Itens_historico(0) = "COBERTURA INCLU�DA: " & rsRecordSet!Tp_Cobertura_Nome
'AKIO.OKUNO - INICIO - 06/05/2013
'                    If Not Inclui_Evento("10017", SinSituacao_id, "NULL", Itens_historico) Then
'                        Altera_Coberturas = False
'                        MsgBox "Erro Inclus�o Evento Inclus�o Cobertura Afetada", vbOKOnly + vbCritical
                    If Not Inclui_Evento("10017", SinSituacao_id, "NULL", Itens_historico, , , sSinistro_BB, , , , , , , , , lConexaoLocal, "SEGP1287", bytGTR_Implantado) Then
                        Altera_Coberturas = 2
'AKIO.OKUNO - FIM - 06/05/2013
                        Exit Function
                    End If
                End If

                'If rsRecordSet!Tipo = "N" Or rsRecordSet!Tipo = "A" Then
                If sDecimal = "." Then
                    vValorTemp = CDbl(Format(TrocaValorBrasPorAme(rsRecordSet!Estimado), "############0.00")) + _
                                 CDbl(Format(TrocaValorBrasPorAme(rsRecordSet!Correcao), "############0.00"))
                Else
                    vValorTemp = CDbl(rsRecordSet!Estimado) + CDbl(rsRecordSet!Correcao)
                End If

                vValor = MudaVirgulaParaPonto(Format(vValorTemp, "############0.00"))

                vSql = "EXEC sinistro_cobertura_spi " & _
                       dSinistro_ID & "," & _
                       SinApolice_Id & "," & _
                       SinSucursal_Id & "," & _
                       SinSeguradora_Id & "," & _
                       gbllRamo_ID & "," & _
                       rsRecordSet!tp_Cobertura_id & "," & _
                     1 & "," & _
                       vValor & _
                       ",'" & cUserName & "'"

                'Passa sempre um pois n�o est� sendo usado o tp_indeniza��o
'AKIO.OKUNO - INICIO - 27/12/2012
'                ExecutarSQL gsSIGLASISTEMA, _
                            glAmbiente_id, _
                            App.Title, _
                            App.FileDescription, _
                            vSql, _
                            lConexaoLocal, _
                            False
                Conexao_ExecutarSQL gsSIGLASISTEMA, _
                            glAmbiente_id, _
                            App.Title, _
                            App.FileDescription, _
                            vSql, _
                            lConexaoLocal, _
                            False
'AKIO.OKUNO - FIM - 27/12/2012
                'End If
            End If

            rsRecordSet.MoveNext

        Loop

    End If


    rsRecordSet.Close

    Exit Function
    'Resume
Trata_Erro:
    TrataErroGeral "Altera��o Coberturas"
    Call FinalizarAplicacao
'    Altera_Coberturas = False      'AKIO.OKUNO - 06/05/2013
    Altera_Coberturas = 1000           'AKIO.OKUNO - 06/05/2013

End Function

Private Sub InicializaInterface_Grid_AbaEstimativasPagamentos_Estimativas()
    Dim bLinhas As Byte
    Dim bColunas As Byte
    Dim sLinhas As String

    LimpaGrid grdEstimativasPagamentos_Estimativas

    With grdEstimativasPagamentos_Estimativas
        If .Rows < 8 Then
            .Rows = 8
        End If
        For bLinhas = 1 To 7
            Select Case bLinhas
            Case 1
                sLinha = "Indeniza��o"
            Case 2
                sLinha = "Honor�rios"
            Case 3
                sLinha = "Despesas"
            Case 4
                sLinha = "Ressarcimento"
            Case 5
                sLinha = "Desp. ressarcimento"
            Case 6
                sLinha = "Salvados"
            Case 7
                sLinha = "Desp. salvados"
            End Select
            .TextMatrix(bLinhas, 0) = sLinha
            For bColunas = 1 To 4
                .TextMatrix(bLinhas, bColunas) = "0,00"
            Next
        Next

    End With

End Sub

Private Sub InicializaInterfaceLocal_Grid_AbaEstimativasPagamentos()
'Ajusta Largura das colunas que cont�m valores
    Dim bColunas As Byte
    Dim lLarguraPadrao As Long

    lLarguraPadrao = 1300
    'Grid de Coberturas
    With grdEstimativasPagamentos_Coberturas
        If .ColWidth(1) <> lLarguraPadrao Then
            For bColunas = 1 To 2
                .ColWidth(bColunas) = lLarguraPadrao
                .Col = bColunas
            Next
        End If
    End With

    lLarguraPadrao = 1220
    'Grid de Itens de Estimativa
    With grdEstimativasPagamentos_Estimativas
        If .ColWidth(1) <> lLarguraPadrao Then
            For bColunas = 1 To 4
                .ColWidth(bColunas) = lLarguraPadrao
                .Col = bColunas
            Next
        End If
    End With

    '
    '    'Grid de Vouchers
    '    With grdEstimativasPagamentos_Voucher
    '        If .ColWidth(4) <> lLarguraPadrao Then
    '            .ColWidth(4) = lLarguraPadrao
    '        End If
    '    End With

End Sub

Private Sub CarregaDadosGrid_AbaEstimativasPagamentos_Valores(bTipo As Byte)
    Dim rsRecordSet As ADODB.Recordset
    Dim rsRecordSet2 As ADODB.Recordset
    Dim sSQL As String
    Dim sLinha As String

    On Error GoTo TrataErro

    Select Case bTipo
    Case 1      'Grid 1
        LimpaGrid grdEstimativasPagamentos_Coberturas

        'FLAVIO.BEZERRA - INICIO - 07/03/2013
        sSQL = ""
        sSQL = sSQL & " Select * " & vbCrLf
        sSQL = sSQL & " From #EstimativaPagamento_CoberturaAfetada " & vbCrLf
        sSQL = sSQL & " Order By tp_cobertura_Nome"

        'sSQL = ""
        'sSQL = sSQL & " Select * " & vbCrLf
        'sSQL = sSQL & " From #Cobertura_Afetada " & vbCrLf
        'sSQL = sSQL & " Order By Nome"
        'FLAVIO.BEZERRA - FIM - 07/03/2013

        Set rsRecordSet2 = Conexao_ExecutarSQL(gsSIGLASISTEMA, _
                                               glAmbiente_id, _
                                               App.Title, _
                                               App.FileDescription, _
                                               sSQL, _
                                               lConexaoLocal, _
                                               True)

        Do While Not rsRecordSet2.EOF
            '                    sLinha = rsRecordSet2!tp_cobertura_Nome & vbTab
            '                    sLinha = sLinha & Format(rsRecordSet2!pago_cob, "#,##0.00") & vbTab   ' ----- estimado
            '                    sLinha = sLinha & Format(rsRecordSet2!Correcao_COB, "#,##0.00") & ""

            'sLinha = rsRecordSet2!Nome & vbTab                                    'FLAVIO.BEZERRA - 07/03/2013
            sLinha = rsRecordSet2!Tp_Cobertura_Nome & vbTab
            sLinha = sLinha & Format(rsRecordSet2!Estimado, "#,##0.00") & vbTab   ' ----- estimado
            sLinha = sLinha & Format(rsRecordSet2!Correcao, "#,##0.00") & ""

            grdEstimativasPagamentos_Coberturas.AddItem sLinha
            grdEstimativasPagamentos_Coberturas.RowData(grdEstimativasPagamentos_Coberturas.Rows - 1) = rsRecordSet2!tp_Cobertura_id

            'nCorrecao = rsRecordSet2!Correcao

            rsRecordSet2.MoveNext

        Loop

        rsRecordSet2.Close
        InicializaModoProcessamentoGrid grdEstimativasPagamentos_Coberturas, 2

    Case 2      'Grid 2

        InicializaInterface_Grid_AbaEstimativasPagamentos_Estimativas

        sSQL = ""
        sSQL = sSQL & " Select top 1 * " & vbCrLf
        sSQL = sSQL & " From #EstimativaPagamento_Estimativa" & vbCrLf
        'sSql = sSql & " Where Dt_Fim_Estimativa is null " & vbCrLf 'cristovao.rodrigues 17/07/2012 verificar o IS NULL para o where
        '            If nSeq_Estimativa <> 0 Then
        '                sSql = sSql & "   and Seq_Estimativa = " & nSeq_Estimativa
        '            End If
        sSQL = sSQL & "order by Seq_Estimativa desc "    'cristovao.rodrigues 28/08/2012

        Set rsRecordSet = Conexao_ExecutarSQL(gsSIGLASISTEMA, _
                                              glAmbiente_id, _
                                              App.Title, _
                                              App.FileDescription, _
                                              sSQL, _
                                              lConexaoLocal, _
                                              True)
        With rsRecordSet
            If Not .EOF Then
                nSeq_Estimativa_Ultima = .Fields!seq_estimativa       'cristovao.rodrigues 02/07/2012
                nSeq_Estima_Prox = .Fields!seq_estimativa + 1   'nSeq_Estimativa + 1
            End If
        End With

        sSQL = ""
        sSQL = sSQL & " Select * " & vbCrLf
        sSQL = sSQL & " From #EstimativaPagamento_Estimativa" & vbCrLf
        sSQL = sSQL & " Where " & vbCrLf

        If nSeq_Estimativa <> 0 Then
            sSQL = sSQL & " Seq_Estimativa = " & nSeq_Estimativa
        Else
            sSQL = sSQL & " Seq_Estimativa = " & nSeq_Estimativa_Ultima
        End If

        Set rsRecordSet = Conexao_ExecutarSQL(gsSIGLASISTEMA, _
                                              glAmbiente_id, _
                                              App.Title, _
                                              App.FileDescription, _
                                              sSQL, _
                                              lConexaoLocal, _
                                              True)

        With rsRecordSet
            Do While Not .EOF
                'If .Seq_Estimativa = nSeq_Estimativa Then
                grdEstimativasPagamentos_Estimativas.TextMatrix(.Fields!Item_Val_Estimativa, 1) = Format(.Fields!Val_Estimado, "#,##0.00")
                grdEstimativasPagamentos_Estimativas.TextMatrix(.Fields!Item_Val_Estimativa, 2) = Format(.Fields!Val_Pago, "#,##0.00")
                grdEstimativasPagamentos_Estimativas.TextMatrix(.Fields!Item_Val_Estimativa, 3) = Format(.Fields!Val_Estimado - .Fields!Val_Pago, "#,##0.00")
                grdEstimativasPagamentos_Estimativas.TextMatrix(.Fields!Item_Val_Estimativa, 4) = Format(.Fields!Val_Resseguro, "#,##0.00")
                grdEstimativasPagamentos_Estimativas.TextMatrix(.Fields!Item_Val_Estimativa, 5) = .Fields!Situacao
                'End If

                mskPercER.Text = Format(.Fields!Perc_Re_Seguro_Er, "#,##0.00")
                mskPercResseguro.Text = Format(.Fields!Perc_Re_Seguro_Quota, "#,##0.00")

                vValorAnt(.Fields!Item_Val_Estimativa) = Format(.Fields!Val_Estimado, "#,##0.00")
                vResseguroAnt(.Fields!Item_Val_Estimativa) = Format(.Fields!Val_Resseguro, "#,##0.00")
                'vCalcRessAnt(.Fields!item_val_estimativa) = Format(.Fields!Val_Pago, "#,##0.00")
                vSaldoAnt(.Fields!Item_Val_Estimativa) = Format(.Fields!Val_Estimado - .Fields!Val_Pago, "#,##0.00")
                vPercResseguroAnt = mskPercResseguro.Text
                vPercERAnt = mskPercER.Text

                .MoveNext
            Loop

            If nSeq_Estima_Prox = 0 Then nSeq_Estima_Prox = 1

        End With

        InicializaModoProcessamentoGrid grdEstimativasPagamentos_Estimativas, 2

        rsRecordSet.Close

    End Select

    Exit Sub

TrataErro:
    Call TrataErroGeral("CarregaDadosGrid_AbaEstimativasPagamentos_Valores", "frmEstimativa")
    Call FinalizarAplicacao

End Sub

Private Sub LimpaCamposTelaLocal(objObjeto As Object)
    On Error GoTo Erro

    If Not objObjeto Is Nothing Then
        LimpaCamposTela objObjeto
    End If

    Exit Sub

Erro:
    Call TrataErroGeral("LimpaCamposTelaLocal", "FrmEstimativa")
    Call FinalizarAplicacao

End Sub

Private Sub LimpaGrid(grdFlexGrid As MSFlexGrid)
    Dim bColunas As Byte
    Dim lLarguraPadrao As Long

    Select Case UCase(grdFlexGrid.name)
    Case "GRDESTIMATIVASPAGAMENTOS_ESTIMATIVAS"
        With grdFlexGrid
            lLarguraPadrao = 1220
            If .ColWidth(1) <> 1220 Then
                For bColunas = 1 To 4
                    .ColWidth(bColunas) = lLarguraPadrao
                    .Col = bColunas
                Next
            End If
        End With
    Case Else
        grdFlexGrid.Rows = 1
    End Select

End Sub

'AKIO.OKUNO - CarregaDadosSinistro (REFORMULA��O) - 06/05/2013
'Private Sub CarregaDadosSinistro()
'    Dim rsRecordSet As ADODB.Recordset
'    Dim sSQL As String
'
'    On Error GoTo TrataErro
'
'
''AKIO.OKUNO - MP - INICIO - 15/02/2013
'    If vProduto = "" Then
''        sSQL = ""
''        sSQL = sSQL & " select " & vbCrLf
''        sSQL = sSQL & "     sinistro_tb.proposta_id,produto_id,sinistro_id,apolice_id,sucursal_seguradora_id,seguradora_cod_susep,sinistro_tb.ramo_id," & vbCrLf
''        sSQL = sSQL & "     dt_aviso_sinistro,cliente_id,solicitante_id,agencia_id,banco_id,evento_sinistro_id," & vbCrLf
''        sSQL = sSQL & "     dt_ocorrencia_sinistro,endereco,dt_entrada_seguradora,bairro,municipio_id,estado, TpComponente = 'T'," & vbCrLf
''        sSQL = sSQL & "     sinistro_tb.dt_inclusao,sinistro_tb.dt_alteracao,municipio,sinistro_tb.situacao,lock_aviso,sinistro_tb.usuario,sinistro_id_lider" & vbCrLf
''        sSQL = sSQL & " From seguros_db.dbo.sinistro_tb WITH (NOLOCK)" & vbCrLf
''        sSQL = sSQL & " inner join proposta_tb WITH (NOLOCK)" & vbCrLf
''        sSQL = sSQL & "     on proposta_tb.proposta_id = sinistro_tb.proposta_id" & vbCrLf
''        sSQL = sSQL & " Where sinistro_id = " & dSinistro_ID & vbCrLf
'        sSQL = ""
'        sSQL = sSQL & "select Sinistro_Tb.proposta_id                               " & vbNewLine
'        sSQL = sSQL & "     , Sinistro_Tb.sinistro_id                               " & vbNewLine
'        sSQL = sSQL & "     , Sinistro_Tb.apolice_id                                " & vbNewLine
'        sSQL = sSQL & "     , Sinistro_Tb.sucursal_seguradora_id                    " & vbNewLine
'        sSQL = sSQL & "     , Sinistro_Tb.seguradora_cod_susep                      " & vbNewLine
'        sSQL = sSQL & "     , Sinistro_Tb.ramo_id                                   " & vbNewLine
'        sSQL = sSQL & "     , Sinistro_Tb.cliente_id                                " & vbNewLine
'        sSQL = sSQL & "     , Sinistro_Tb.dt_ocorrencia_sinistro                    " & vbNewLine
'        sSQL = sSQL & "     , sinistro_tb.situacao                                  " & vbNewLine
'        sSQL = sSQL & "     , Proposta_Tb.Produto_ID                                " & vbNewLine
'        sSQL = sSQL & "  From seguros_db.dbo.sinistro_tb sinistro_tb WITH (NOLOCK)   " & vbNewLine
'        sSQL = sSQL & "  Join Seguros_Db.Dbo.Proposta_Tb Proposta_tb WITH (NOLOCK)   " & vbNewLine
'        sSQL = sSQL & "    on Proposta_Tb.Proposta_ID = Sinistro_Tb.Proposta_ID     " & vbNewLine
'        sSQL = sSQL & " Where sinistro_id = " & dSinistro_ID & "                    " & vbNewLine
'    Else
''AKIO.OKUNO - MP - FIM - 15/02/2013
'        sSQL = ""
'        sSQL = sSQL & "select proposta_id                                           " & vbNewLine
'        sSQL = sSQL & "     , sinistro_id                                           " & vbNewLine
'        sSQL = sSQL & "     , apolice_id                                            " & vbNewLine
'        sSQL = sSQL & "     , sucursal_seguradora_id                                " & vbNewLine
'        sSQL = sSQL & "     , seguradora_cod_susep                                  " & vbNewLine
'        sSQL = sSQL & "     , ramo_id                                               " & vbNewLine
'        sSQL = sSQL & "     , cliente_id                                            " & vbNewLine
'        sSQL = sSQL & "     , dt_ocorrencia_sinistro                                " & vbNewLine
'        sSQL = sSQL & "     , sinistro_tb.situacao                                  " & vbNewLine
'        sSQL = sSQL & "  From seguros_db.dbo.sinistro_tb WITH (NOLOCK)               " & vbNewLine
'        sSQL = sSQL & " Where sinistro_id = " & dSinistro_ID & "                    " & vbNewLine
'    End If                  'AKIO.OKUNO - MP - 15/02/2013
'
'
'    Set rsRecordSet = Conexao_ExecutarSQL(gsSIGLASISTEMA, _
'                                          glAmbiente_id, _
'                                          App.Title, _
'                                          App.FileDescription, _
'                                          sSQL, _
'                                          lConexaoLocal, _
'                                          True)
'
'    ' ... verificar Titular / Conjuge
'    'select top 10
'    'sinistro_tb.proposta_id,sinistro_id,apolice_id,sucursal_seguradora_id,seguradora_cod_susep,sinistro_tb.ramo_id,
'    'dt_aviso_sinistro,cliente_id,solicitante_id,agencia_id,banco_id,evento_sinistro_id,
'    'dt_ocorrencia_sinistro,endereco,dt_entrada_seguradora,bairro,municipio_id,estado,
'    'sinistro_tb.Dt_Inclusao , sinistro_tb.dt_alteracao, municipio, sinistro_tb.Situacao, lock_aviso, sinistro_tb.Usuario, sinistro_id_lider
'    'From seguros_db.dbo.sinistro_tb
'    'inner join proposta_complementar_tb
'    '    on proposta_complementar_tb.proposta_id = sinistro_tb.proposta_id
'    '--inner join proposta_tb
'    '--  on proposta_tb.proposta_id = proposta_complementar_tb.proposta_id
'    ' Where 1 = 1
'    '-- and sinistro_id = 93201205008 -- 93200100404
'    'and ramo_id = 1
'    'order by sinistro_tb.dt_inclusao desc
'    '
'
'    '* cristovao.rodrigues 13/07/2012
'    If Not rsRecordSet.EOF Then
'
'        SinDT_Ocorrencia = rsRecordSet!dt_ocorrencia_sinistro
''        vTpComponente = rsRecordSet!TpComponente       'AKIO.OKUNO - MP - 15/02/2013
'        vTpComponente = "T"                             'AKIO.OKUNO - MP - 15/02/2013
'        SinApolice_Id = rsRecordSet!Apolice_id
'        gbllRamo_ID = rsRecordSet!ramo_id
'        gbllProposta_ID = rsRecordSet!Proposta_id
'        'SinTpComponenteId = rsRecordSet!                      'FLAVIO.BEZERRA - 01/02/2013
'        SinSeguradora_Id = rsRecordSet!seguradora_cod_susep
'        SinSucursal_Id = rsRecordSet!sucursal_seguradora_id
'        If vProduto = "" Then                           'AKIO.OKUNO - MP - 15/02/2013
'            vProduto = rsRecordSet!Produto_id           'AKIO.OKUNO - MP - 15/02/2013
'        End If                                          'AKIO.OKUNO - MP - 15/02/2013
'        SinCliente_Id = rsRecordSet!Cliente_id
'        SinSituacao_id = rsRecordSet!Situacao
'
'    End If
'
'    rsRecordSet.Close
'
'    If Not bolDefiniu_Operacao_Cosseguro Then   'AKIO.OKUNO - MP - 15/02/2013
'        sSQL = ""
'        sSQL = sSQL & "SELECT tp_emissao" & vbCrLf
'        sSQL = sSQL & "  FROM seguros_db..apolice_tb WITH (NOLOCK)" & vbCrLf
'        sSQL = sSQL & " WHERE apolice_id = " & SinApolice_Id & vbCrLf
'        sSQL = sSQL & "   AND ramo_id = " & gbllRamo_ID & vbCrLf
'
'        Set rsRecordSet = Conexao_ExecutarSQL(gsSIGLASISTEMA, _
'                                              glAmbiente_id, _
'                                              App.Title, _
'                                              App.FileDescription, _
'                                              sSQL, _
'                                              lConexaoLocal, _
'                                              True)
'        If Not rsRecordSet.EOF Then
'            If rsRecordSet!tp_emissao = "A" Then
'                sOperacaoCosseguro = "C"
'            Else
'                sOperacaoCosseguro = ""
'            End If
'        End If
'        bolDefiniu_Operacao_Cosseguro = True            'AKIO.OKUNO - MP - 15/02/2013
'        rsRecordSet.Close                       'AKIO.OKUNO - MP - 15/02/2013
'    End If                                      'AKIO.OKUNO - MP - 15/02/2013
'
'    'FLAVIO.BEZERRA - IN�CIO - 01/02/2013
'    If bytTipoRamo = bytTipoRamo_Vida Then
''        rsRecordSet.Close                      'AKIO.OKUNO - MP - 15/02/2013
'
'        sSQL = ""
'        sSQL = "SELECT tp_componente_id "
'        sSQL = sSQL & " FROM escolha_tp_cob_vida_tb WITH (NOLOCK) "
'        sSQL = sSQL & " WHERE proposta_id = " & gbllProposta_ID
'        sSQL = sSQL & "   AND tp_componente_id IN (19,20)"
'        sSQL = sSQL & " ORDER BY tp_componente_id"
'
'        Set rsRecordSet = Conexao_ExecutarSQL(gsSIGLASISTEMA, _
'                                          glAmbiente_id, _
'                                          App.Title, _
'                                          App.FileDescription, _
'                                          sSQL, _
'                                          lConexaoLocal, _
'                                          True)
'
'        If Not rsRecordSet.EOF Then
'            gblsTp_Componente_ID = rsRecordSet.Fields!tp_componente_id
'        End If
'
'        rsRecordSet.Close
'        Set rsRecordSet = Nothing
'
'    End If
'    'FLAVIO.BEZERRA - FIM - 01/02/2013
'
'    Exit Sub
'
'TrataErro:
'    Call TrataErroGeral("CarregaDadosSinistro", "frmEstimativa")
'    Call FinalizarAplicacao
'End Sub


'AKIO.OKUNO - CarregaDadosSinistro - 06/05/2013
Private Sub CarregaDadosSinistro()
    Dim rsRecordSet As ADODB.Recordset
    Dim sSQL As String

    On Error GoTo TrataErro

    
    If Not bolDefiniu_Operacao_Cosseguro Then
        sSQL = ""
        sSQL = sSQL & "SELECT tp_emissao" & vbCrLf
        sSQL = sSQL & "  FROM seguros_db..apolice_tb WITH (NOLOCK)" & vbCrLf
        sSQL = sSQL & " WHERE apolice_id = " & SinApolice_Id & vbCrLf
        sSQL = sSQL & "   AND ramo_id = " & gbllRamo_ID & vbCrLf
    
        Set rsRecordSet = Conexao_ExecutarSQL(gsSIGLASISTEMA, _
                                              glAmbiente_id, _
                                              App.Title, _
                                              App.FileDescription, _
                                              sSQL, _
                                              lConexaoLocal, _
                                              True)
        If Not rsRecordSet.EOF Then
            If rsRecordSet!tp_emissao = "A" Then
                sOperacaoCosseguro = "C"
            Else
                sOperacaoCosseguro = ""
            End If
        End If
        bolDefiniu_Operacao_Cosseguro = True
        rsRecordSet.Close
    End If
    
    
    Exit Sub

TrataErro:
    Call TrataErroGeral("CarregaDadosSinistro", "frmEstimativa")
    Call FinalizarAplicacao
End Sub

Private Sub grdEstimativasPagamentos_Coberturas_Click()

    Dim i As Integer

    'MATHAYDE
    If grdEstimativasPagamentos_Coberturas.Rows > 1 Then

        For i = 0 To cmbCoberturaAfetada.ListCount - 1
            If cmbCoberturaAfetada.ItemData(i) = grdEstimativasPagamentos_Coberturas.RowData(grdEstimativasPagamentos_Coberturas.Row) Then
                cmbCoberturaAfetada.ListIndex = i
                mskValorPrejuizo.Text = Format(CDbl(grdEstimativasPagamentos_Coberturas.TextMatrix(grdEstimativasPagamentos_Coberturas.Row, 1)), "###########0.00")

            End If
        Next i
    End If

End Sub

Private Sub grdEstimativasPagamentos_Estimativas_DblClick()

    On Error GoTo TrataErro

    gridCol = grdEstimativasPagamentos_Estimativas.Col
    gridRow = grdEstimativasPagamentos_Estimativas.Row

    If (bytTipoRamo = 1 And gridRow > 1 And gridRow < 4) Or (bytTipoRamo <> 1 And gridRow > 1 And gridRow < 8) Then

        If grdEstimativasPagamentos_Estimativas.Rows > 1 And grdEstimativasPagamentos_Coberturas.Enabled Then

            If gridCol = 1 And gridRow > 0 Then    'cristovao.rodrigues 23/07/2012 somente a coluna de VALOR

                'If Trim(grdEstimativasPagamentos_Estimativas.TextMatrix(grdEstimativasPagamentos_Estimativas.Row, 0)) = "x" Then
                Text1.Visible = True
                Text1.Width = grdEstimativasPagamentos_Estimativas.CellWidth
                Text1.Height = grdEstimativasPagamentos_Estimativas.CellHeight
                Text1.Top = grdEstimativasPagamentos_Estimativas.CellTop + grdEstimativasPagamentos_Estimativas.Top
                Text1.Left = grdEstimativasPagamentos_Estimativas.CellLeft + grdEstimativasPagamentos_Estimativas.Left
                Text1.Text = grdEstimativasPagamentos_Estimativas.Text
                Text1.SelStart = 0
                Text1.SelLength = Len(Text1.Text)
                Text1.ZOrder
                Text1.SetFocus
                'End If

            Else

                If grdEstimativasPagamentos_Estimativas.Col = 0 Then
                    If grdEstimativasPagamentos_Estimativas.Text = "" Then
                        grdEstimativasPagamentos_Estimativas.Text = "  x"
                        For i = 0 To grdEstimativasPagamentos_Estimativas.Cols - 1
                            grdEstimativasPagamentos_Estimativas.Col = i
                            grdEstimativasPagamentos_Estimativas.CellBackColor = &HACF2F4
                        Next
                    Else
                        grdEstimativasPagamentos_Estimativas.Text = ""
                        grdEstimativasPagamentos_Estimativas.Col = 5
                        'grdEstimativasPagamentos_Estimativas.Text = "0,00"
                        For i = 0 To grdEstimativasPagamentos_Estimativas.Cols - 1
                            grdEstimativasPagamentos_Estimativas.Col = i
                            grdEstimativasPagamentos_Estimativas.CellBackColor = vbWhite
                        Next
                    End If
                End If

            End If
        End If

    End If

    Exit Sub
TrataErro:
    Call TrataErroGeral("GrdEstimativasPagamentos_Estimativas", "frmEstimativa")
    Call FinalizarAplicacao
End Sub

Private Sub mskValorPrejuizo_LostFocus()
' FormataValorContinuo mskValorPrejuizo, 2, True
' mskValorPrejuizo.Text = Format(mskValorPrejuizo.Text, "###.###.#&&,&&")
End Sub

Private Sub FormataValorContinuo(ByRef obj As Object, tamdec As Integer, IsCurrency As Boolean)

    On Error GoTo TrataErro

    If IsCurrency = True Then

        Dim vrPonto, vr, vp As String
        Dim b, a, Count As Integer

        Count = 0

        If tamdec = 0 Then
            vrPonto = ""
        Else
            vrPonto = "."
        End If

        vr = obj.Text
        b = Len(vr)
        vp = ""

        For F = 1 To b
            If ((Asc(Mid(vr, F, 1)) >= 48) And (Asc(Mid(vr, F, 1)) <= 57) And Count < 15) Then
                vp = vp & Mid(vr, F, 1)
                Count = Count + 1
            End If
        Next

        vr = vp

        If (Len(vr) > 1) Then
            If (Len(vr) <= tamdec) Then
                vr = Left(vr, 1) & "," & Mid(vr, 2, Len(vr))
            Else
                vrIniTemp = ""
                a = 1
                vrIni = Left(vr, Len(vr) - tamdec)
                vrFim = Mid(vr, Len(vr) - tamdec + 1, Len(vr))
                For F = 0 To Len(vrIni) - 1
                    vrIniTemp = Mid(vrIni, Len(vrIni) - F, 1) & vrIniTemp
                    If ((a = 3) And (Len(vrIni) <> F + 1)) Then
                        vrIniTemp = vrPonto & vrIniTemp
                        a = 0
                    End If
                    a = a + 1
                Next
                If (tamdec <> 0) Then
                    vr = vrIniTemp & "," & vrFim
                Else
                    vr = vrIniTemp
                End If
            End If
        End If

        obj.Text = vr
        obj.SelStart = Len(vr)

    End If

    Exit Sub

TrataErro:

    'Resume Next
    Call TrataErroGeral("FormataValorContinuo", "frmEstimativa")
    Call FinalizarAplicacao

End Sub

Private Sub Monta_cmbCoberturaAfetada()

    'FLAVIO.BEZERRA - IN�CIO - 01/02/2013
'    Dim Rst_Cobertura As ADODB.Recordset
'    Dim rsAux As ADODB.Recordset
'    Dim vSql As String
'    Dim vTabela As String
'    Dim Tipo As String

'    vSql = "SELECT tp_cobertura_tb.Tp_cobertura_id, tp_cobertura_tb.nome " & _
'         "  FROM tp_cob_item_prod_tb INNER JOIN tp_cobertura_tb " & _
'         "    ON tp_cob_item_prod_tb.tp_cobertura_id = tp_cobertura_tb.tp_cobertura_id " & _
'         " WHERE tp_cob_item_prod_tb.produto_id = " & vProduto & _
'         "   AND tp_cob_item_prod_tb.ramo_id = " & gbllRamo_ID & _
'         " ORDER BY class_tp_cobertura DESC, tp_cobertura_tb.nome"
'
'    Set Rst_Cobertura = Conexao_ExecutarSQL(gsSIGLASISTEMA, _
'                                            glAmbiente_id, _
'                                            App.Title, _
'                                            App.FileDescription, _
'                                            vSql, _
'                                            lConexaoLocal, _
'                                            True)
'
'    With frmEstimativa.cmbCoberturaAfetada
'        .Clear
'        If Not Rst_Cobertura.EOF Then
'            While Not Rst_Cobertura.EOF
'                .AddItem Rst_Cobertura(1)
'                .ItemData(.NewIndex) = Rst_Cobertura(0)
'                Rst_Cobertura.MoveNext
'            Wend
'        End If
'        Rst_Cobertura.Close
'
'        '* Adiciona a cobertura gen�rica conforme solicitado pelo sr. M�rcio em 01/04/2003
'        .AddItem "COBERTURA GEN�RICA PARA SINISTRO"
'        .ItemData(.NewIndex) = COB_GENERICA_SEGUR
'    End With

        Dim Rst_Cobertura   As ADODB.Recordset
        Dim rsAux           As ADODB.Recordset
        Dim sSQL            As String
        Dim vTabela         As String
        Dim Tipo            As String
        Dim sSql1           As String
    
        On Error GoTo TrataErro
    
            sSQL = ""
    
            ''Cleber.Campos
            'If bytTipoRamo = bytTipoRamo_Vida Then  ' Vida - Instru��o SQL para Vida
             If bytTipoRamo = bytTipoRamo_Vida And Not VerificaProduto680Ramo77 Then
                sSQL = " select e.tp_cobertura_id, f.nome " & vbNewLine
                sSQL = sSQL & " from escolha_plano_tb a WITH (NOLOCK), " & vbNewLine
                sSQL = sSQL & " plano_tb b WITH (NOLOCK), " & vbNewLine
                sSQL = sSQL & " tp_plano_tp_comp_tb c WITH (NOLOCK), " & vbNewLine
                sSQL = sSQL & " tp_cob_comp_plano_tb d WITH (NOLOCK), " & vbNewLine
                sSQL = sSQL & " tp_cob_comp_tb e WITH (NOLOCK), " & vbNewLine
                sSQL = sSQL & " tp_cobertura_tb f WITH (NOLOCK) " & vbNewLine
                sSQL = sSQL & " where a.plano_id = b.plano_id " & vbNewLine
                sSQL = sSQL & "     and a.dt_inicio_vigencia = b.dt_inicio_vigencia" & vbNewLine
                sSQL = sSQL & "     and a.produto_id = b.produto_id" & vbNewLine
                sSQL = sSQL & "     and b.tp_plano_id = c.tp_plano_id" & vbNewLine
                sSQL = sSQL & "     and b.tp_plano_id = d.tp_plano_id" & vbNewLine
                sSQL = sSQL & "     and c.tp_componente_id = e.tp_componente_id" & vbNewLine
                sSQL = sSQL & "     and d.tp_cob_comp_id = e.tp_cob_comp_id" & vbNewLine
                sSQL = sSQL & "     and e.tp_cobertura_id = f.tp_cobertura_id" & vbNewLine
                sSQL = sSQL & "     and a.proposta_id = " & gbllProposta_ID & vbNewLine
                sSQL = sSQL & "     and convert(smalldatetime, convert(varchar(10), a.dt_escolha, 111)) <= '" & Format(SinDT_Ocorrencia, "yyyymmdd") & "' " & vbNewLine
                sSQL = sSQL & "     and ( convert(smalldatetime, convert(varchar(10), a.dt_fim_vigencia, 111)) >= '" & Format(SinDT_Ocorrencia, "yyyymmdd") & "' or " & vbNewLine
                sSQL = sSQL & "     a.dt_fim_vigencia IS NULL ) " & vbNewLine
    
                If vTpComponente = "T" Then
                    sSQL = sSQL & " and c.tp_componente_id = 1" & vbNewLine 'Titular
                Else
                    sSQL = sSQL & " and c.tp_componente_id = 3" & vbNewLine 'Conjuge
                End If
    
                sSQL = sSQL & " union " & vbNewLine
    
                sSQL = sSQL & " select b.tp_cobertura_id, c.nome " & vbNewLine
                sSQL = sSQL & " from escolha_tp_cob_vida_tb a WITH (NOLOCK), " & vbNewLine
                sSQL = sSQL & " tp_cob_comp_tb b WITH (NOLOCK), " & vbNewLine
                sSQL = sSQL & " tp_cobertura_tb c WITH (NOLOCK) " & vbNewLine
                sSQL = sSQL & " where a.tp_cob_comp_id = b.tp_cob_comp_id " & vbNewLine
                sSQL = sSQL & "     and b.tp_cobertura_id = c.tp_cobertura_id " & vbNewLine
    
                If vProduto = 14 Then ' OUROVIDA PRODUTOR RURAL
    
                    ' Buscar cobertua pela proposta_b�sica
                    sSql1 = " select proposta_id " & vbNewLine
                    sSql1 = sSql1 & " from apolice_tb WITH (NOLOCK) " & vbNewLine
                    sSql1 = sSql1 & " where apolice_id = " & SinApolice_Id & vbNewLine
                    sSql1 = sSql1 & " and ramo_id = " & gbllRamo_ID & vbNewLine
    
                    Set rsAux = ExecutarSQL(gsSIGLASISTEMA, _
                                             glAmbiente_id, _
                                             App.Title, _
                                             App.FileDescription, _
                                             sSql1, _
                                             True)
    
    
                    sSQL = sSQL & " and a.proposta_id = " & gbllProposta_ID & vbNewLine
                    rsAux.Close
                    Set rsAux = Nothing
    
                Else
    
                    sSQL = sSQL & " and a.proposta_id = " & gbllProposta_ID & vbNewLine
    
                End If
    
                sSQL = sSQL & " and ( convert(smalldatetime, convert(varchar(10), a.dt_fim_vigencia_esc, 111)) >= '" & Format(SinDT_Ocorrencia, "yyyymmdd") & "' " & vbNewLine
                sSQL = sSQL & " or a.dt_fim_vigencia_esc is null ) " & vbNewLine
    
                'Verifica se o tipo de componente � diferente de 1-TITULAR ou 3-CONJUGE
                'In�cio da altera��o - chamado INC000004303850 - Cleber Sardo - 17/04/2014
                'If gblsTp_Componente_ID <> "" Then
                If gblsTp_Componente_ID <> 0 Then
                'Fim da altera��o - chamado INC000004303850 - Cleber Sardo - 17/04/2014
                    sSQL = sSQL & " and b.tp_componente_id = " & gblsTp_Componente_ID & vbNewLine
                Else
                    If vTpComponente = "T" Then
                        sSQL = sSQL & " and b.tp_componente_id = 1" & vbNewLine 'Titular
                    Else
                        sSQL = sSQL & " and b.tp_componente_id = 3" & vbNewLine 'Conjuge
                    End If
                End If
    
                sSQL = sSQL & " union " & vbNewLine
    
                sSQL = sSQL & " select b.tp_cobertura_id, c.nome " & vbNewLine
                sSQL = sSQL & " from escolha_tp_cob_vida_aceito_tb a WITH (NOLOCK), " & vbNewLine
                sSQL = sSQL & " tp_cob_comp_tb b WITH (NOLOCK), " & vbNewLine
                sSQL = sSQL & " tp_cobertura_tb c WITH (NOLOCK) " & vbNewLine
                sSQL = sSQL & " where a.tp_cob_comp_id = b.tp_cob_comp_id " & vbNewLine
                sSQL = sSQL & " and b.tp_cobertura_id = c.tp_cobertura_id " & vbNewLine
                sSQL = sSQL & " and proposta_id = " & gbllProposta_ID & vbNewLine
                sSQL = sSQL & " and ( convert(smalldatetime, convert(varchar(10), a.dt_inicio_vigencia_esc, 111)) <= '" & Format(SinDT_Ocorrencia, "yyyymmdd") & "' " & vbNewLine
                sSQL = sSQL & " and convert(smalldatetime, convert(varchar(10), a.dt_fim_vigencia_esc, 111)) >= '" & Format(SinDT_Ocorrencia, "yyyymmdd") & "' " & vbNewLine
                sSQL = sSQL & " or  a.dt_fim_vigencia_esc is null ) " & vbNewLine
    
                If vTpComponente = "T" Then
                    sSQL = sSQL & " and b.tp_componente_id = 1" & vbNewLine 'Titular
                Else
                    sSQL = sSQL & " and b.tp_componente_id = 3" & vbNewLine 'Conjuge
                End If
    
                sSQL = sSQL & " union " & vbNewLine
    
                sSQL = sSQL & " select b.tp_cobertura_id, c.nome " & vbNewLine
                sSQL = sSQL & " from escolha_sub_grp_tp_cob_comp_tb a WITH (NOLOCK), " & vbNewLine
                sSQL = sSQL & " tp_cob_comp_tb b WITH (NOLOCK), " & vbNewLine
                sSQL = sSQL & " tp_cobertura_tb c WITH (NOLOCK) " & vbNewLine
                sSQL = sSQL & " where a.tp_cob_comp_id = b.tp_cob_comp_id " & vbNewLine
                sSQL = sSQL & " and b.tp_cobertura_id = c.tp_cobertura_id " & vbNewLine
                sSQL = sSQL & " and a.Apolice_id = " & SinApolice_Id & vbNewLine
                sSQL = sSQL & " and a.ramo_id = " & gbllRamo_ID & vbNewLine
                sSQL = sSQL & " and a.seguradora_cod_susep = " & SinSeguradora_Id & vbNewLine
                sSQL = sSQL & " and a.sucursal_seguradora_id = " & SinSucursal_Id & vbNewLine
                
                If vTpComponente = "T" Then
                    sSQL = sSQL & " and b.tp_componente_id = 1" & vbNewLine 'Titular
                Else
                    sSQL = sSQL & " and b.tp_componente_id = 3" & vbNewLine 'Conjuge
                End If
    
            Else    ' RE (Ramos Elementares) / RURAL
    
                sSQL = " SELECT a.tp_cobertura_id, b.nome " & vbNewLine
                sSQL = sSQL & " from escolha_tp_cob_emp_tb a WITH (NOLOCK) " & vbNewLine
                sSQL = sSQL & " INNER JOIN tp_cobertura_tb b WITH (NOLOCK) " & vbNewLine
                sSQL = sSQL & " on (a.tp_cobertura_id = b.tp_cobertura_id) " & vbNewLine
                sSQL = sSQL & " Where a.Proposta_id = " & gbllProposta_ID & vbNewLine
                sSQL = sSQL & " and a.produto_id = " & vProduto & vbNewLine
                sSQL = sSQL & " and a.ramo_id = " & gbllRamo_ID & vbNewLine
                sSQL = sSQL & " Union " & vbNewLine
                sSQL = sSQL & " select a.tp_cobertura_id, b.nome " & vbNewLine
                sSQL = sSQL & " from escolha_tp_cob_res_tb a WITH (NOLOCK) " & vbNewLine
                sSQL = sSQL & " inner join tp_cobertura_tb b WITH (NOLOCK) " & vbNewLine
                sSQL = sSQL & " on (a.tp_cobertura_id = b.tp_cobertura_id)" & vbNewLine
                sSQL = sSQL & " Where a.Proposta_id = " & gbllProposta_ID & vbNewLine
                sSQL = sSQL & " and  a.produto_id = " & vProduto & vbNewLine
                sSQL = sSQL & " and  a.ramo_id = " & gbllRamo_ID & vbNewLine
                sSQL = sSQL & " Union " & vbNewLine
                sSQL = sSQL & " select a.tp_cobertura_id, b.nome " & vbNewLine
                sSQL = sSQL & " from escolha_tp_cob_cond_tb a WITH (NOLOCK)" & vbNewLine
                sSQL = sSQL & " inner join tp_cobertura_tb b WITH (NOLOCK) " & vbNewLine
                sSQL = sSQL & " on (a.tp_cobertura_id = b.tp_cobertura_id)" & vbNewLine
                sSQL = sSQL & " Where a.Proposta_id = " & gbllProposta_ID & vbNewLine
                sSQL = sSQL & " and a.produto_id = " & vProduto & vbNewLine
                sSQL = sSQL & " and a.ramo_id = " & gbllRamo_ID & vbNewLine
                sSQL = sSQL & " Union " & vbNewLine
                sSQL = sSQL & " select a.tp_cobertura_id, b.nome" & vbNewLine
                sSQL = sSQL & " from escolha_tp_cob_maq_tb a WITH (NOLOCK)" & vbNewLine
                sSQL = sSQL & " inner join tp_cobertura_tb b WITH (NOLOCK)" & vbNewLine
                sSQL = sSQL & " on ( a.tp_cobertura_id = b.tp_cobertura_id) " & vbNewLine
                sSQL = sSQL & " Where a.Proposta_id = " & gbllProposta_ID & vbNewLine
                sSQL = sSQL & " and a.produto_id = " & vProduto & vbNewLine
                sSQL = sSQL & " and a.ramo_id = " & gbllRamo_ID & vbNewLine
                sSQL = sSQL & " Union" & vbNewLine
                sSQL = sSQL & " select a.tp_cobertura_id, b.nome " & vbNewLine
                sSQL = sSQL & " from escolha_tp_cob_aceito_tb a WITH (NOLOCK) " & vbNewLine
                sSQL = sSQL & " inner join tp_cobertura_tb b WITH (NOLOCK) " & vbNewLine
                sSQL = sSQL & " on ( a.tp_cobertura_id = b.tp_cobertura_id) " & vbNewLine
                sSQL = sSQL & " Where a.Proposta_id = " & gbllProposta_ID & vbNewLine
                sSQL = sSQL & " and a.produto_id = " & vProduto & vbNewLine
                sSQL = sSQL & " and a.ramo_id = " & gbllRamo_ID & vbNewLine
                sSQL = sSQL & " Union" & vbNewLine
                sSQL = sSQL & " select a.tp_cobertura_id, b.nome " & vbNewLine
                sSQL = sSQL & " from escolha_tp_cob_avulso_tb a WITH (NOLOCK) " & vbNewLine
                sSQL = sSQL & " inner join tp_cobertura_tb b WITH (NOLOCK) " & vbNewLine
                sSQL = sSQL & " on ( a.tp_cobertura_id = b.tp_cobertura_id) " & vbNewLine
                sSQL = sSQL & " Where a.Proposta_id = " & gbllProposta_ID & vbNewLine
                sSQL = sSQL & " and  a.produto_id = " & vProduto & vbNewLine
                sSQL = sSQL & " and  a.ramo_id = " & gbllRamo_ID & vbNewLine
                sSQL = sSQL & " Union " & vbNewLine
                sSQL = sSQL & " Select a.tp_cobertura_id, b.nome " & vbNewLine
                sSQL = sSQL & " from escolha_tp_cob_generico_tb a WITH (NOLOCK) " & vbNewLine
                sSQL = sSQL & " inner join tp_cobertura_tb b WITH (NOLOCK) " & vbNewLine
                sSQL = sSQL & " on (a.tp_cobertura_id = b.tp_cobertura_id) " & vbNewLine
                sSQL = sSQL & " Where a.Proposta_id = " & gbllProposta_ID & vbNewLine
                sSQL = sSQL & " and a.produto_id = " & vProduto & vbNewLine
                sSQL = sSQL & " and a.ramo_id = " & gbllRamo_ID & vbNewLine
    
            End If
    
        'FLAVIO.BEZERRA - IN�CIO - 27/02/2013
        sSQL = sSQL & " Union " & vbNewLine
        'sSQL = sSQL & " select Tp_Cobertura_ID, nome " & vbNewLine
        'sSQL = sSQL & " from #Cobertura_Afetada " & vbNewLine                    'FLAVIO.BEZERRA - 07/03/2013
        sSQL = sSQL & " select Tp_Cobertura_ID, Tp_Cobertura_Nome " & vbNewLine   'FLAVIO.BEZERRA - 07/03/2013
        sSQL = sSQL & " from #EstimativaPagamento_CoberturaAfetada " & vbNewLine  'FLAVIO.BEZERRA - 07/03/2013
        'FLAVIO.BEZERRA - FIM - 27/02/2013
        
        sSQL = sSQL & " order by 1" & vbNewLine
    
        Set Rst_Cobertura = Conexao_ExecutarSQL(gsSIGLASISTEMA, _
                                                   glAmbiente_id, _
                                                   App.Title, _
                                                   App.FileDescription, _
                                                   sSQL, _
                                                   lConexaoLocal, _
                                                   True)
    
    
        With cmbCoberturaAfetada
            .Clear
            If Not Rst_Cobertura.EOF Then
               While Not Rst_Cobertura.EOF
                     .AddItem Rst_Cobertura(1)
                     .ItemData(.NewIndex) = Rst_Cobertura(0)
                     Rst_Cobertura.MoveNext
               Wend
            End If
    
            .AddItem "COBERTURA GEN�RICA PARA SINISTRO"
            .ItemData(.NewIndex) = 1 'COB_GENERICA_SEGUR
        End With
    
        Rst_Cobertura.Close
        Set Rst_Cobertura = Nothing
    
        Exit Sub
        
        'FLAVIO.BEZERRA - FIM - 01/02/2013
TrataErro:
        Call TrataErroGeral("Monta_cmbCoberturaAfetada", "frmConsultaSinistro")
        Call FinalizarAplicacao
End Sub

Private Sub Text1_KeyDown(KeyCode As Integer, Shift As Integer)

    If KeyCode = vbKeyReturn Then
        With grdEstimativasPagamentos_Estimativas

            If .Row = .Rows - 1 Then
                .Row = .Row
            Else
                .Row = .Row + 1
            End If
            .SetFocus
            Text1.Visible = False

        End With
    ElseIf KeyCode = vbKeyEscape Then
        Text1.Text = grdEstimativasPagamentos_Estimativas.Text
    End If

End Sub

Private Sub Text1_KeyUp(KeyCode As Integer, Shift As Integer)
    FormataValorContinuo Text1, 2, True
End Sub

Private Sub Text1_LostFocus()

    With grdEstimativasPagamentos_Estimativas

        Text1.Text = Format(Text1.Text, "############0.00")

        If Text1.Text <> .TextMatrix(gridRow, gridCol) And IsNumeric(Text1.Text) Then

            sAlterar = True

            .TextMatrix(gridRow, gridCol) = Text1.Text

            'Calcula o saldo 'cristovao.rodrigues 02/10/2012
            .TextMatrix(gridRow, 3) = Format(CDbl(.TextMatrix(gridRow, 1)) - CDbl(.TextMatrix(gridRow, 2)), "#,##0.00")

            If mskPercResseguro.Text = "" Then mskPercResseguro.Text = "0,0"
            If mskPercER.Text = "" Then mskPercER.Text = "0,0"

            'cristovao.rodrigues 16/07/2012
            If CDbl(.TextMatrix(gridRow, 1)) <> CDbl(.TextMatrix(gridRow, 2)) Or .TextMatrix(gridRow, 1) = "0,00" Then    'cristovao.rodrigues 02/10/2012 incluido = "0,00"
                'If (CDbl(IIf(mskPercResseguro.Text = "", 0, mskPercResseguro.Text)) > 0 Or CDbl(IIf(mskPercER.Text = "", 0, mskPercER.Text)) > 0) _
                 '     Or (CDbl(.TextMatrix(gridRow, 1)) >= 0) Then 'cristovao.rodrigues 02/10/2012 if comentado, mudanca de > para >= "0"
                Calcula_Valores_Estimativa gridRow, True
                'sAlterar = True
                'End If
            End If

        End If

    End With

    Text1.Visible = False
    Text1.Text = 0

End Sub
'cristovao.rodrigues 16/07/2012
Private Sub Calcula_Valores_Estimativa(Index As Integer, Pergunta As Boolean)
    Dim vValor As String
    Dim vPago As String
    Dim vResseguro As String
    Dim vCalcRess As String
    Dim vPercResseguro As String
    Dim vPercER As String
    Dim rsRecordSet As ADODB.Recordset
    Dim sSQL As String
    Dim nIndice As Integer

    On Error GoTo TrataErro

    nIndice = 7
    vCalcRess = 0
    gridRow = Index

    With grdEstimativasPagamentos_Estimativas

        If sDecimal = "." Then
            vValor = Format(TrocaValorBrasPorAme(.TextMatrix(gridRow, 1)), "############0.00")
            vPago = Format(TrocaValorBrasPorAme(.TextMatrix(gridRow, 2)), "############0.00")
        Else
            vValor = .TextMatrix(gridRow, 1)
            vPago = .TextMatrix(gridRow, 2)
        End If


        'Calcula o saldo 'cristovao.rodrigues 02/10/2012 comentei passado para funcao anterior de chamada
        '.TextMatrix(gridRow, 3) = Format(CDbl(vValor) - CDbl(vPago), "#,##0.00")

        If strTipoProcesso <> strTipoProcesso_Avisar Then    'cristovao.rodrigues 03/10/2012
            sSQL = ""
            sSQL = sSQL & "Select * " & vbCrLf
            sSQL = sSQL & "  From #EstimativaPagamento_Estimativa " & vbCrLf
            sSQL = sSQL & " order by Seq_Estimativa  "

            Set rsRecordSet = Conexao_ExecutarSQL(gsSIGLASISTEMA, _
                                                  glAmbiente_id, _
                                                  App.Title, _
                                                  App.FileDescription, _
                                                  sSQL, _
                                                  lConexaoLocal, _
                                                  True)

            Do While Not rsRecordSet.EOF

                If rsRecordSet("Item_Val_Estimativa") = Index Then 'cristovao.rodrigues 04/04/2013 incluida varificacao IsNull(rsRecordSet.Fields!Dt_Fim_Estimativa)
                    If rsRecordSet.Fields!Val_Estimado <> vValor And (rsRecordSet.Fields!Dt_Fim_Estimativa = "" Or IsNull(rsRecordSet.Fields!Dt_Fim_Estimativa)) Then
                        'Calcula o valor do resseguro
                        vCalcRess = Calcula_Valores_Resseguro(CDbl(vValor))
                        .TextMatrix(gridRow, 4) = Format(vCalcRess, "############0.00")
                    End If

                End If
                rsRecordSet.MoveNext

            Loop

        Else
            'Calcula o valor do resseguro
            vCalcRess = Calcula_Valores_Resseguro(CDbl(vValor))
            .TextMatrix(gridRow, 4) = Format(vCalcRess, "############0.00")
        End If

        'cristovao.rodrigues 20/07/2012
        'MsgBox "verificar se update da ultima seq_estimada"

        ''''''''''''' cristovao.rodrigues 27/08/2012 incluir caso nao tenha estimativa
        ''''''''''''' obs. ele faz um INSERT com toda a grid a partir do item 1

        If bytTipoRamo = 1 Then nIndice = 3

        For xlinha = 1 To nIndice

            sSQL = ""
            sSQL = sSQL & "Select * " & vbCrLf
            sSQL = sSQL & "  From #EstimativaPagamento_Estimativa " & vbCrLf
            sSQL = sSQL & " where item_val_estimativa = " & xlinha & vbCrLf    'gridRow & vbCrLf
            sSQL = sSQL & " and Seq_Estimativa = " & nSeq_Estimativa_Ultima    'nSeq_Estima_Prox '
            'sSql = sSql & " order by Seq_Estimativa  "

            Set rsRecordSet = Conexao_ExecutarSQL(gsSIGLASISTEMA, _
                                                  glAmbiente_id, _
                                                  App.Title, _
                                                  App.FileDescription, _
                                                  sSQL, _
                                                  lConexaoLocal, _
                                                  True)

            If rsRecordSet.EOF Then

                '             If bytTipoRamo = 1 Then nIndice = 3
                '
                '             For xLinha = 1 To nIndice

                sSQL = ""
                sSQL = sSQL & " Insert into #EstimativaPagamento_Estimativa     " & vbCrLf
                sSQL = sSQL & "      ( Dt_Inicio_Estimativa     , Dt_Fim_Estimativa     , Item_Val_Estimativa  " & vbCrLf
                sSQL = sSQL & "      , Seq_Estimativa           , Val_Estimado          , Val_Pago  " & vbCrLf
                sSQL = sSQL & "      , Val_Resseguro            , Situacao              , Usuario   " & vbCrLf
                sSQL = sSQL & "      , Num_Carta_Seguro_Aceito  , Perc_Re_Seguro_Quota  , Perc_Re_Seguro_Er   " & vbCrLf
                sSQL = sSQL & "      , Tipo                     , Nossa_Parte           , Dt_Inclusao " & vbCrLf
                sSQL = sSQL & "      , Moeda_ID                 , pgto_parcial_co_seguro    " & vbCrLf
                sSQL = sSQL & "      )    " & vbCrLf
                sSQL = sSQL & " values ( '" & Format(Data_Sistema, "yyyymmdd") & "', ''," & xlinha & "," & nSeq_Estimativa_Ultima    'nSeq_Estima_Prox '
                sSQL = sSQL & "," & Replace(TrocaValorBrasPorAme(.TextMatrix(xlinha, 1)), ",", "")
                sSQL = sSQL & "," & Replace(TrocaValorBrasPorAme(.TextMatrix(xlinha, 2)), ",", "")
                sSQL = sSQL & "," & Replace(TrocaValorBrasPorAme(.TextMatrix(xlinha, 4)), ",", "")
                sSQL = sSQL & ",null "
                sSQL = sSQL & ",'" & cUserName & "'"
                sSQL = sSQL & "," & IIf(mskNumCarta.ClipText = "", "''", mskNumCarta.ClipText)
                sSQL = sSQL & "," & Replace(TrocaValorBrasPorAme(mskPercResseguro.Text), ",", "")
                sSQL = sSQL & "," & Replace(TrocaValorBrasPorAme(mskPercER.Text), ",", "")
                sSQL = sSQL & ",'N'"
                sSQL = sSQL & ",null "
                sSQL = sSQL & ",'" & Format(Data_Sistema, "yyyymmdd") & "'"
                sSQL = sSQL & ",null "
                sSQL = sSQL & ",null )"

                If .TextMatrix(xlinha, 1) <> "0,00" Then

                    Conexao_ExecutarSQL gsSIGLASISTEMA, _
                                        glAmbiente_id, _
                                        App.Title, _
                                        App.FileDescription, _
                                        sSQL, _
                                        lConexaoLocal, _
                                        False

                End If
                'Next

            Else

                ''''''''''''' 27/08/2012

                sSQL = ""
                sSQL = sSQL & " update #EstimativaPagamento_Estimativa " & vbCrLf
                'sSql = sSql & " set tipo = 'A' ," & vbCrLf
                sSQL = sSQL & " set tipo = 'N' ," & vbCrLf
                sSQL = sSQL & " Val_Estimado =" & Replace(TrocaValorBrasPorAme(.TextMatrix(xlinha, 1)), ",", "") & ", " & vbCrLf
                sSQL = sSQL & " Val_Pago =" & Replace(TrocaValorBrasPorAme(.TextMatrix(xlinha, 2)), ",", "") & ", " & vbCrLf
                sSQL = sSQL & " Val_Resseguro =" & Replace(TrocaValorBrasPorAme(.TextMatrix(xlinha, 4)), ",", "") & ", " & vbCrLf
                sSQL = sSQL & " Perc_Re_Seguro_Quota =" & Replace(TrocaValorBrasPorAme(mskPercResseguro.Text), ",", "") & ", " & vbCrLf
                sSQL = sSQL & " Perc_Re_Seguro_Er =" & Replace(TrocaValorBrasPorAme(mskPercER.Text), ",", "") & ", " & vbCrLf
                sSQL = sSQL & " dt_inicio_estimativa = '" & Format(Data_Sistema, "yyyymmdd") & "' " & vbCrLf 'cristovao.rodrigues 05/04/2013 colocar data sistema para inicio da estimativa
                
                sSQL = sSQL & " where item_val_estimativa = " & xlinha & vbCrLf
                sSQL = sSQL & " and Seq_Estimativa = " & nSeq_Estimativa_Ultima    'nSeq_Estima_Prox '  'nSeq_Estimativa

                Conexao_ExecutarSQL gsSIGLASISTEMA, _
                                    glAmbiente_id, _
                                    App.Title, _
                                    App.FileDescription, _
                                    sSQL, _
                                    lConexaoLocal, _
                                    False

            End If

        Next

        If CDbl(vValor) = 0 Then
            .TextMatrix(gridRow, 4) = "0,00"
            Exit Sub
        End If

    End With

    Exit Sub
TrataErro:
    Call TrataErroGeral("Calcula_Valores_Estimativa", "frmEstimativa")
    Call FinalizarAplicacao
End Sub

Private Sub grdEstimativasPagamentos_Estimativas_Recalcula(Index As Integer)

    If mskPercResseguro.Text = "" Then mskPercResseguro.Text = "0,0"
    If mskPercER.Text = "" Then mskPercER.Text = "0,0"

    With grdEstimativasPagamentos_Estimativas
        If CDbl(.TextMatrix(Index, 1)) <> CDbl(.TextMatrix(Index, 2)) _
           Or (sAlterar Or sExcluir Or sAdicionar) Then

            'Calcula o saldo 'cristovao.rodrigues 02/10/2012
            .TextMatrix(Index, 3) = Format(CDbl(.TextMatrix(Index, 1)) - CDbl(.TextMatrix(Index, 2)), "#,##0.00")

            If (CDbl(mskPercResseguro.Text) > 0 Or CDbl(mskPercER.Text) > 0) Or (CDbl(.TextMatrix(Index, 1)) > 0) Then
                Calcula_Valores_Estimativa Index, True
            End If
        End If
    End With
End Sub


Private Function Calcula_Valores_Resseguro(vValor As Double) As Currency

    Dim Rst As ADODB.Recordset
    Dim sSQL As String
    Dim vEnd_Risco_id As Integer

    On Error GoTo TrataErro

    vEnd_Risco_id = 0
    
    'Ricardo Toledo (Confitec) : 11/07/2013 : INC000004125853 : Inicio
    gblsCod_Objeto_Segurado = ""
    'Ricardo Toledo (Confitec) : 11/07/2013 : INC000004125853 : fim


    sSQL = ""
    sSQL = sSQL & " select cod_objeto_segurado from sinistro_re_tb WITH (NOLOCK) "
    sSQL = sSQL & " where sinistro_id = " & gbldSinistro_ID

    Set rs = Conexao_ExecutarSQL(gsSIGLASISTEMA, _
                                 glAmbiente_id, _
                                 App.Title, _
                                 App.FileDescription, _
                                 sSQL, _
                                 lConexaoLocal, _
                                 True)
    If Not rs.EOF Then
        vEnd_Risco_id = rs!cod_objeto_segurado
        'Ricardo Toledo (Confitec) : 11/07/2013 : INC000004125853 : Inicio
        'atribui o valor a variavel global para utiliza-la em na chamada da procedure SEGS7442_SPS (na function Verifica_valor_estimado)
        gblsCod_Objeto_Segurado = vEnd_Risco_id
        'Ricardo Toledo (Confitec) : 11/07/2013 : INC000004125853 : Fim
    End If

    
    '* cristovao.rodrigues 16/07/2012 12/04/2013 incluida instrucao SET ANSI_WARNINGS OFF
    sSQL = " SET ANSI_WARNINGS OFF "
    sSQL = sSQL & "EXEC resseg_db.dbo.totaliza_valor_resseguro_sinistro_spu "
    sSQL = sSQL & "NULL, "
    sSQL = sSQL & "NULL, "
    sSQL = sSQL & "NULL, "
    sSQL = sSQL & "NULL, "
    sSQL = sSQL & "NULL, "
    sSQL = sSQL & "NULL, "
    sSQL = sSQL & gbllProposta_ID & ","
    sSQL = sSQL & SinEndosso_Id & ","
    'sSql = sSql & Val(Left(cmbCodObj.Text, 3)) & "," 'cristovao.rodrigues linha substituida
    sSQL = sSQL & vEnd_Risco_id & ","    ' "0,"
    'sSQL = sSQL & MudaVirgulaParaPonto(Replace(mskValorPrejuizo.Text, ".", ""))   'FLAVIO.BEZERRA - 19/04/2013
    sSQL = sSQL & MudaVirgulaParaPonto(Replace(vValor, ".", ""))

    If bytTipoRamo = 1 Then
        sSQL = sSQL & ", " & IIf(SinCliente_Id <> "0", SinCliente_Id, "NULL")
        If cmbCoberturaAfetada.Text = "" Then
            sSQL = sSQL & ", NULL,"
        Else
            sSQL = sSQL & ", " & cmbCoberturaAfetada.ItemData(cmbCoberturaAfetada.ListIndex) & ","
        End If
        sSQL = sSQL & "'" & Format(SinDT_Ocorrencia, "yyyymm") & "'"
    End If
    
    Set Rst = Conexao_ExecutarSQL(gsSIGLASISTEMA, _
                                  glAmbiente_id, _
                                  App.Title, _
                                  App.FileDescription, _
                                  sSQL, _
                                  lConexaoLocal, _
                                  True)

    If Not Rst.EOF Then
        Calcula_Valores_Resseguro = Val(Rst("val_resseguro"))
    End If

    Rst.Close
    
    'FLAVIO.BEZERRA - INICIO - 15/04/2013
    
    Conexao_ExecutarSQL gsSIGLASISTEMA, _
                        glAmbiente_id, _
                        App.Title, _
                        App.FileDescription, _
                        "SET ANSI_WARNINGS ON", _
                        lConexaoLocal, _
                        False
                        
    'FLAVIO.BEZERRA - FIM - 15/04/2013
    
    Exit Function
    Resume
TrataErro:
    Call TrataErroGeral("Calcula_Valores_Resseguro", "frmEstimativa")
    Call FinalizarAplicacao
End Function
'cristovao.rodrigues 01/10/2012 mudanca para funcao retornar valor
Public Function Atualiza_Eventos() As Boolean

    Dim vPercResseguro As String
    Dim vPercER As String

    On Error GoTo TrataErro

    Atualiza_Eventos = False

    'Transforma o Percentual ele n�o pode ser maior que 100
    If sDecimal = "." Then
        vPercResseguro = Format(TrocaValorBrasPorAme(mskPercResseguro.Text), "##0.000")
        vPercER = Format(TrocaValorBrasPorAme(mskPercER.Text), "##0.000")
    Else
        vPercResseguro = mskPercResseguro.Text
        vPercER = mskPercER.Text
    End If

    If CDbl(grdEstimativasPagamentos_Estimativas.TextMatrix(1, 1)) = 0 And CDbl(grdEstimativasPagamentos_Estimativas.TextMatrix(2, 1)) = 0 And _
       CDbl(grdEstimativasPagamentos_Estimativas.TextMatrix(3, 1)) = 0 And CDbl(grdEstimativasPagamentos_Estimativas.TextMatrix(4, 1)) = 0 And _
       CDbl(grdEstimativasPagamentos_Estimativas.TextMatrix(5, 1)) = 0 And CDbl(grdEstimativasPagamentos_Estimativas.TextMatrix(6, 1)) = 0 And _
       CDbl(grdEstimativasPagamentos_Estimativas.TextMatrix(7, 1)) = 0 Then
        If strTipoProcesso <> strTipoProcesso_Reabrir Or _
           strTipoProcesso <> strTipoProcesso_Reabrir_Cosseguro Then
            mensagem_erro 6, "Estimativa inv�lida. Todos os valores iguais a zero."
            btnOK_Estimativa.SetFocus
            vErroEstimativa = True
            Exit Function
        ElseIf strTipoProcesso = strTipoProcesso_Reabrir Or _
               strTipoProcesso = strTipoProcesso_Reabrir_Cosseguro Then
            If vSaldoAnt(1) = 0 And vSaldoAnt(2) = 0 And vSaldoAnt(3) = 0 And _
               vSaldoAnt(4) = 0 And vSaldoAnt(5) = 0 And vSaldoAnt(6) = 0 And vSaldoAnt(7) = 0 Then
                mensagem_erro 6, "Estimativa inv�lida. Todos os valores iguais a " & _
                                 "zero sem saldos para acertar."
                btnOK_Estimativa.SetFocus
                vErroEstimativa = True
                Exit Function
            Else
                If MsgBox("Todos os valores iguais a zero. " & _
                          "Confirma reabertura para acerto de saldos?", _
                          vbYesNo + vbQuestion + vbDefaultButton2) = vbYes Then

                    sSQL = ""
                    sSQL = sSQL & " insert into #Estimativa_Evento(dt_evento, evento_SEGBR, descricao, situacao, tipo, usuario) "
                    sSQL = sSQL & " values ("
                    sSQL = sSQL & "'" & Format(Data_Sistema, "yyyymmdd") & "',"
                    sSQL = sSQL & "'10005','MOTIVO: ACERTO DE SALDOS','0','N','" & cUserName & "')"

                    Conexao_ExecutarSQL gsSIGLASISTEMA, _
                                        glAmbiente_id, _
                                        App.Title, _
                                        App.FileDescription, _
                                        sSQL, _
                                        lConexaoLocal, _
                                        False

                End If
            End If
        End If
    End If

    'Se j� houve pagamento n�o h� altera��o de resseguro
    If (vPercResseguroAnt <> CDbl(vPercResseguro)) Or (vPercERAnt <> CDbl(vPercER)) Then
        vAlterouPerc = True

        sSQL = ""
        sSQL = sSQL & " insert into #Estimativa_Evento(dt_evento, evento_SEGBR, descricao, situacao, tipo, usuario) "
        sSQL = sSQL & " values ("
        sSQL = sSQL & "'" & Format(Data_Sistema, "yyyymmdd") & "',"
        sSQL = sSQL & "'10031',"

        If (vPercResseguroAnt <> CDbl(vPercResseguro)) And (vPercERAnt <> CDbl(vPercER)) Then
            sSQL = sSQL & "'%QUOTA DE: " & Format(vPercResseguroAnt, "##0.000") & _
                 " PARA: " & vPercResseguro & _
                 " E %ER DE: " & Format(vPercERAnt, "##0.000") & _
                 " PARA: " & vPercER
        ElseIf vPercResseguroAnt <> CDbl(vPercResseguro) Then
            sSQL = sSQL & "'%QUOTA DE: " & Format(vPercResseguroAnt, "##0.000") & _
                 " PARA: " & vPercResseguro
        ElseIf vPercERAnt <> CDbl(vPercER) Then
            sSQL = sSQL & "' %ER DE: " & Format(vPercERAnt, "##0.000") & _
                 " PARA: " & vPercER
        End If

        sSQL = sSQL & "','0','N','" & cUserName & "')"

        Conexao_ExecutarSQL gsSIGLASISTEMA, _
                            glAmbiente_id, _
                            App.Title, _
                            App.FileDescription, _
                            sSQL, _
                            lConexaoLocal, _
                            False

    End If

    Atualiza_Eventos = True

    Exit Function

TrataErro:
    Call TrataErroGeral("Atualiza_Eventos", "frmEstimativa")
    Call FinalizarAplicacao
End Function
'AKIO.OKUNO - Altera_Estimativas - Altera��o tipo de valor do retorno - 06/05/2013
Public Function Altera_Estimativas() As Integer
'Public Function Altera_Estimativas() As Boolean

    Dim vSql As String
    Dim rs As ADODB.Recordset
    Dim rsRecordSet As ADODB.Recordset
    Dim vValor(5) As String
    Dim i As Integer
    Dim vResAnt As String
    Dim Max_seq_estimativa As String
    Dim Itens_historico(0) As String

    i = 0
'    Altera_Estimativas = True          'AKIO.OKUNO - 10/05/2013
    On Error GoTo Trata_Erro


    '        Descarrega os eventos guardados pelo inclui estimativa
    '        For Each Even In Eventos
    'cristovao.rodrigues 03/09/2012
    If Not Atualiza_Eventos Then
        Altera_Estimativas = False
        Exit Function
    End If

    sSQL = ""
    sSQL = sSQL & " Select * " & vbCrLf
    sSQL = sSQL & " From #Estimativa_Evento " & vbCrLf
    sSQL = sSQL & " where tipo = 'N'"

    Set rsRecordSet = Conexao_ExecutarSQL(gsSIGLASISTEMA, _
                                          glAmbiente_id, _
                                          App.Title, _
                                          App.FileDescription, _
                                          sSQL, _
                                          lConexaoLocal, _
                                          True)

    If Not rsRecordSet.EOF Then
        While Not rsRecordSet.EOF
            'verificar valores de 'EvenDescricao / EvenEvento_SEGBR
            'If rsRecordSet!Tipo_Evento = "N" Then
            Itens_historico(0) = rsRecordSet!Descricao
'AKIO.OKUNO - INICIO - 06/05/2013
'            If Not Inclui_Evento(rsRecordSet!Evento_SEGBR, SinSituacao_id, "NULL", Itens_historico) Then
'                Altera_Estimativas = False
'                MsgBox "Erro Inclus�o Evento Altera��o de Estimativa", vbOKOnly + vbCritical
            If Not Inclui_Evento(rsRecordSet!evento_SEGBR, SinSituacao_id, "NULL", Itens_historico, , , sSinistro_BB, , , , , , , , , lConexaoLocal, "SEGP1287", bytGTR_Implantado) Then
                Altera_Estimativas = 1
'AKIO.OKUNO - FIM - 06/05/2013
                Exit Function
            End If
            'End If
            rsRecordSet.MoveNext
        Wend
    End If


    sSQL = ""
    sSQL = sSQL & " Select * " & vbCrLf
    sSQL = sSQL & " From #EstimativaPagamento_Estimativa " & vbCrLf
    sSQL = sSQL & " where Seq_Estimativa = " & nSeq_Estimativa_Ultima & vbCrLf    ' nSeq_Estimativa & vbCrLf
    sSQL = sSQL & " order by Item_Val_Estimativa "

    Set rsRecordSet = Conexao_ExecutarSQL(gsSIGLASISTEMA, _
                                          glAmbiente_id, _
                                          App.Title, _
                                          App.FileDescription, _
                                          sSQL, _
                                          lConexaoLocal, _
                                          True)



    '            vSql = "begin tran"
    '
    '            Call Conexao_ExecutarSQL(gsSIGLASISTEMA, _
                 '                     glAmbiente_id, _
                 '                     App.Title, _
                 '                     App.FileDescription, _
                 '                     vSql, _
                 '                     lConexaoLocal, _
                 '                     False)


    With rsRecordSet
        Do While Not .EOF


            'cristovao.rodrigues 20/07/2012 comentei esse if pois ele tem que fazer um insert de todos os item_val_estimativa
            ' sento novo ou se foi alterado
            'If .Fields!Tipo <> "E" Then 'And .Fields!Seq_Estimativa = nSeq_Estimativa Then

            'If .Fields!Seq_Estimativa <> i Then 'cristovao.rodrigues verificar onde grava o proximo numero estimativa
            If nSeq_Estima_Prox <> i Then
                'Coloca data fim em todas as estimativas em aberto
                vSql = "EXEC sinistro_estimativa_spu " & _
                       dSinistro_ID & "," & _
                       SinApolice_Id & "," & _
                       SinSucursal_Id & "," & _
                       SinSeguradora_Id & "," & _
                       gbllRamo_ID & ",'" & _
                       Format(Data_Sistema, "yyyymmdd") & "',NULL,'" & _
                       cUserName & "'"

                Call Conexao_ExecutarSQL(gsSIGLASISTEMA, _
                                         glAmbiente_id, _
                                         App.Title, _
                                         App.FileDescription, _
                                         vSql, _
                                         lConexaoLocal, _
                                         False)


                i = nSeq_Estima_Prox
            End If

            If sDecimal = "." Then
                vValor(0) = Format(TrocaValorBrasPorAme(.Fields!Val_Estimado), "############0.00")
                vValor(1) = Format(TrocaValorBrasPorAme(.Fields!Val_Pago), "############0.00")
                vValor(2) = Format(TrocaValorBrasPorAme(.Fields!Val_Resseguro), "############0.00")
                vValor(3) = Format(TrocaValorBrasPorAme(.Fields!Perc_Re_Seguro_Quota), "############0.000")
                vValor(4) = Format(TrocaValorBrasPorAme(.Fields!Perc_Re_Seguro_Er), "############0.000")
            Else
                vValor(0) = MudaVirgulaParaPonto(Format(.Fields!Val_Estimado, "############0.00"))
                vValor(1) = MudaVirgulaParaPonto(Format(.Fields!Val_Pago, "############0.00"))
                vValor(2) = MudaVirgulaParaPonto(Format(.Fields!Val_Resseguro, "############0.00"))
                vValor(3) = MudaVirgulaParaPonto(Format(.Fields!Perc_Re_Seguro_Quota, "############0.000"))
                vValor(4) = MudaVirgulaParaPonto(Format(.Fields!Perc_Re_Seguro_Er, "############0.000"))
            End If
'faz a inclus�o da nova estimativa
            vSql = "EXEC sinistro_estimativa_spi " & _
                   dSinistro_ID & "," & _
                   SinApolice_Id & "," & _
                   SinSucursal_Id & "," & _
                   SinSeguradora_Id & "," & _
                   gbllRamo_ID & ",'" & _
                   Format(.Fields!Dt_Inicio_Estimativa, "yyyymmdd") & "'," & _
                   .Fields!Item_Val_Estimativa & "," & _
                   nSeq_Estima_Prox & "," & _
                   vValor(0) & "," & vValor(1) & "," & _
                   "NULL, " & _
                   vValor(3) & "," & _
                   vValor(4) & ",'" & _
                   LCase(.Fields!Situacao) & "','" & cUserName & "'" & ", "

            If sOperacaoCoseguro = "C" Then

                If Trim(.Fields!Num_Carta_Seguro_Aceito) <> "" Then
                    vSql = vSql & Trim(.Fields!Num_Carta_Seguro_Aceito)

                    Call Conexao_ExecutarSQL(gsSIGLASISTEMA, _
                                             glAmbiente_id, _
                                             App.Title, _
                                             App.FileDescription, _
                                             vSql, _
                                             lConexaoLocal, _
                                             False)

                    vSql = "EXEC atualiza_numcarta_estima_spu " & _
                           dSinistro_ID & "," & _
                           SinApolice_Id & "," & _
                           SinSucursal_Id & "," & _
                           SinSeguradora_Id & "," & _
                           gbllRamo_ID & "," & _
                           nSeq_Estima_Prox & "," & _
                           Trim(.Fields!Num_Carta_Seguro_Aceito)

                    Call Conexao_ExecutarSQL(gsSIGLASISTEMA, _
                                             glAmbiente_id, _
                                             App.Title, _
                                             App.FileDescription, _
                                             vSql, _
                                             lConexaoLocal, _
                                             False)

                Else
                    vSql = vSql & "NULL"

                    Call Conexao_ExecutarSQL(gsSIGLASISTEMA, _
                                             glAmbiente_id, _
                                             App.Title, _
                                             App.FileDescription, _
                                             vSql, _
                                             lConexaoLocal, _
                                             False)

                End If

            Else
                vSql = vSql & "NULL"

                Call Conexao_ExecutarSQL(gsSIGLASISTEMA, _
                                         glAmbiente_id, _
                                         App.Title, _
                                         App.FileDescription, _
                                         vSql, _
                                         lConexaoLocal, _
                                         False)

            End If

            'Obt�m a �ltima estimativa
            vSql = "SELECT MAX(seq_estimativa) FROM sinistro_estimativa_tb WITH (NOLOCK) "
            vSql = vSql & " WHERE sinistro_id = " & dSinistro_ID

            Set rs = Conexao_ExecutarSQL(gsSIGLASISTEMA, _
                                         glAmbiente_id, _
                                         App.Title, _
                                         App.FileDescription, _
                                         vSql, _
                                         lConexaoLocal, _
                                         True)

            Max_seq_estimativa = rs(0)
            rs.Close
            ' CONFITEC Cleber Sardo 04/04/2019 Corre��o calculo de resseguro  - inicio
            'Calculo resseguro''''''''''''''''''''''''''''''''''''''''''''''''''''''
            'vsql = "EXEC resseg_db.dbo.calcula_resseguro_estimativa_sinistro_spu " & _
            '       dSinistro_ID & "," & _
            '       SinApolice_Id & "," & _
            '       SinSucursal_Id & "," & _
            '       SinSeguradora_Id & "," & _
            '       gbllRamo_ID & "," & _
            '       Max_seq_estimativa & "," & _
            '       IIf(SinCliente_Id <> "0", SinCliente_Id, "NULL") & "," & _
            '       grdEstimativasPagamentos_Coberturas.RowData(1) & "," & _
            '       "'" & Format(SinDT_Ocorrencia, "yyyymm") & "'," & _
            '       "'" & cUserName & "'"
            '
            'Call Conexao_ExecutarSQL(gsSIGLASISTEMA, _
            '                         glAmbiente_id, _
            '                         App.Title, _
            '                         App.FileDescription, _
            '                         vsql, _
            '                         lConexaoLocal, _
            '                         False)
                        ' CONFITEC Cleber Sardo 04/04/2019 Corre��o calculo de resseguro  - Fim
            'End If

            .MoveNext
        Loop

    End With

    'C�lculo do valor de cosseguro
    vSql = "EXEC sinistro_calcula_cosseguro_spu " & _
           dSinistro_ID & "," & _
           SinApolice_Id & "," & _
           SinSucursal_Id & "," & _
           SinSeguradora_Id & "," & _
           gbllRamo_ID

    Call Conexao_ExecutarSQL(gsSIGLASISTEMA, _
                             glAmbiente_id, _
                             App.Title, _
                             App.FileDescription, _
                             vSql, _
                             lConexaoLocal, _
                             False)
                                                         
    ' CONFITEC Cleber Sardo 04/04/2019 Corre��o calculo de resseguro  - Inicio
        vSql = "EXEC resseg_db.dbo.RSGS00437_SPI " & _
           " @usuario = " & cUserName & "," & _
           " @sinistro_id_UNITARIO = " & dSinistro_ID & "," & _
           " @seq_estimativa_UNITARIO =" & Max_seq_estimativa

        Call Conexao_ExecutarSQL(gsSIGLASISTEMA, _
                             glAmbiente_id, _
                             App.Title, _
                             App.FileDescription, _
                             vSql, _
                             lConexaoLocal, _
                             False)
        ' CONFITEC Cleber Sardo 04/04/2019 Corre��o calculo de resseguro  - Fim
        
    'Obt�m a �ltima estimativa
'    vsql = "SELECT MAX(seq_estimativa) FROM sinistro_estimativa_tb WITH (NOLOCK) "      'AKIO.OKUNO - 07/05/2013
    vSql = "SELECT MAX(seq_estimativa)+1 FROM sinistro_estimativa_tb WITH (NOLOCK) "     'AKIO.OKUNO - 07/05/2013
    vSql = vSql & " WHERE sinistro_id = " & dSinistro_ID

    Set rs = Conexao_ExecutarSQL(gsSIGLASISTEMA, _
                                 glAmbiente_id, _
                                 App.Title, _
                                 App.FileDescription, _
                                 vSql, _
                                 lConexaoLocal, _
                                 True)

    'Max_seq_estimativa = rs(0)
'    nSeq_Estima_Prox = rs(0) + 1  'cristovao.rodrigues 20/08/2012                      'AKIO.OKUNO - 07/05/2013
    nSeq_Estima_Prox = rs(0)                                                            'AKIO.OKUNO - 07/05/2013

    rs.Close
    rsRecordSet.Close


    vSql = ""
    vSql = vSql & " update #EstimativaPagamento_CoberturaAfetada "
    vSql = vSql & " set tipo = 'E'" & vbCrLf

    vSql = vSql & " update #EstimativaPagamento_Estimativa "
    vSql = vSql & " set tipo = 'E'" & vbCrLf

    Conexao_ExecutarSQL gsSIGLASISTEMA, _
                        glAmbiente_id, _
                        App.Title, _
                        App.FileDescription, _
                        vSql, _
                        lConexaoLocal, _
                        False


    Exit Function
    'Resume
Trata_Erro:
    TrataErroGeral "Altera��o Estimativa"
    Call FinalizarAplicacao
'    Altera_Estimativas = False 'AKIO.OKUNO - 06/05/2013
    Altera_Estimativas = 10000  'AKIO.OKUNO - 06/05/2013
End Function
'cristovao.rodrigues 23/07/2012
Private Function ObterValorTotalEstimadoCobertura() As String

    Dim i As Integer
    Dim dblValor As Double

    dblValor = 0

    For i = 1 To grdEstimativasPagamentos_Coberturas.Rows - 1
        dblValor = dblValor + CDbl(grdEstimativasPagamentos_Coberturas.TextMatrix(i, 1)) + IIf(IsNumeric(grdEstimativasPagamentos_Coberturas.TextMatrix(i, 2)) = True, CDbl(grdEstimativasPagamentos_Coberturas.TextMatrix(i, 2)), 0)
    Next i

    ObterValorTotalEstimadoCobertura = Format(CStr(dblValor), "###,###,##0.00")

End Function

Private Sub btnVoltar_Estimativa_Click()
    Dim Retorno As Boolean


    If sAlterar Or sAdicionar Or sExcluir Then
        If MsgBox("As altera��es n�o foram salvas. Deseja ignora-las ?", vbYesNo, "Aten��o !") = vbNo Then
            Exit Sub
        End If
    End If


    'MsgBox "verificar se fecha conexao"

    If EstadoConexao(lConexaoLocal) = adStateOpen Then

        'dropar as tabelas temporarias existentes

        '        sSql = ""
        '        sSql = sSql & " drop table #EstimativaPagamento_CoberturaAfetada " & vbCrLf
        '        sSql = sSql & " drop table #Cobertura_Afetada " & vbCrLf
        '        sSql = sSql & " drop table #EstimativaPagamento_Estimativa  " & vbCrLf
        '        'sSql = sSql & " drop table"
        '        'sSql = sSql & " drop table"
        '
        '        Conexao_ExecutarSQL gsSIGLASISTEMA, _
                 '                                glAmbiente_id, _
                 '                                App.Title, _
                 '                                App.FileDescription, _
                 '                                sSql, _
                 '                                lConexaoLocal, _
                 '                                False


        Retorno = FecharConexao(lConexaoLocal)
    End If

    'envia mensagem para aplica��o master
    Retorno = EnvioSaidaAuxiliar(txtAuxiliar.Text, frmEstimativa)

    End

End Sub

Private Sub InicializaModoProcessamentoGrid(grdGrid As MSFlexGrid, bTipo As Byte)
    DoEvents

    With grdGrid
        Select Case bTipo
        Case 1              'Vai carregar
            '            grdGrid.BackColor = &HE0E0E0
            '            grdGrid.Enabled = False
            .BackColorFixed = &HC0C0C0
            .ForeColorFixed = &H808080
        Case 2              'Carregado
            '            grdGrid.BackColor = &H80000005
            '            grdGrid.Enabled = True
            .BackColorFixed = &H8000000F
            .ForeColorFixed = &H80000012
        End Select
    End With
End Sub

Public Function Verifica_valor_estimado(tp_Cobertura_id As String, Val_Estimado As Double) As Boolean

    Dim ja_pago As Double
    Dim vSql As String
    Dim rs As ADODB.Recordset
    Dim vValorEstimado As Double

    Verifica_valor_estimado = True

    vValorEstimado = Val_Estimado

    vSql = "SELECT SUM(a.val_acerto) "
    vSql = vSql & "FROM pgto_sinistro_cobertura_tb a WITH (NOLOCK) , pgto_sinistro_tb b WITH (NOLOCK) "
    vSql = vSql & "WHERE a.Sinistro_id = " & gbldSinistro_ID
    vSql = vSql & "  AND a.item_val_estimativa = 1"
    vSql = vSql & "  AND a.sinistro_id = b.sinistro_id "
    vSql = vSql & "  AND a.seq_pgto = b.seq_pgto "
    vSql = vSql & "  AND a.item_val_estimativa = b.item_val_estimativa "
    vSql = vSql & "  AND a.seq_estimativa = b.seq_estimativa "
    vSql = vSql & "  AND a.beneficiario_id = b.beneficiario_id "
    vSql = vSql & "  AND a.num_parcela = b.num_parcela "
    vSql = vSql & "  AND a.tp_cobertura_id = " & tp_Cobertura_id
    vSql = vSql & "  AND ( b.situacao_op <> 'c' and b.situacao_op <> 'e' ) "
    Set rs = Conexao_ExecutarSQL(gsSIGLASISTEMA, _
                                 glAmbiente_id, _
                                 App.Title, _
                                 App.FileDescription, _
                                 vSql, _
                                 lConexaoLocal, _
                                 True)
    If rs.EOF Then
        ja_pago = 0
    Else
        If IsNull(rs(0)) = True Then
            ja_pago = 0
        Else
            ja_pago = Val(rs(0))
        End If
    End If
    rs.Close
    If Verifica_valor_estimado = True Then


        vSql = "EXEC SEGS7442_SPS " & vbNewLine
        vSql = vSql & " @sinistro_id = " & gbldSinistro_ID & ", " & vbNewLine
        vSql = vSql & " @tp_cobertura_id = " & tp_Cobertura_id & ", " & vbNewLine
        vSql = vSql & " @cod_objeto_segurado = " & IIf(gblsCod_Objeto_Segurado = "", "Null", gblsCod_Objeto_Segurado) & ", " & vbNewLine
        vSql = vSql & " @valor_indenizacao = '" & MudaVirgulaParaPonto(Replace(Val_Estimado, ".", "")) & "', " & vbNewLine
        vSql = vSql & " @tecnico_id = " & Tecnico_Atual & "," & vbNewLine
        vSql = vSql & " @usuario = '" & cUserName & "'" & "," & vbNewLine
        vSql = vSql & " @con_ind_maior_is = 1"
        Set rs = Conexao_ExecutarSQL(gsSIGLASISTEMA, _
                                     glAmbiente_id, _
                                     App.Title, _
                                     App.FileDescription, _
                                     vSql, _
                                     lConexaoLocal, _
                                     True)

        If rs(0) = 0 Then
            MsgBox "O valor da estimativa � superior a Import�ncia Segurada para a cobertura.", vbInformation, "Aten��o"
            Verifica_valor_estimado = False
        End If
        rs.Close
    End If

End Function

Public Sub RetornaValorPercentualResseguro()

    Dim vEnd_Risco_id As Integer
    Dim vEstima As Long
    Dim sSQL As String
    Dim rs As ADODB.Recordset

    Dim sPercER As String
    Dim sPercResseguro As String

    vEnd_Risco_id = 0
    vEstima = grdEstimativasPagamentos_Estimativas.TextMatrix(1, 1)

    'Ricardo Toledo (Confitec) : 11/07/2013 : INC000004125853 : Inicio
    gblsCod_Objeto_Segurado = ""
    'Ricardo Toledo (Confitec) : 11/07/2013 : INC000004125853 : fim

    sSQL = ""
    sSQL = sSQL & " select cod_objeto_segurado from sinistro_re_tb WITH (NOLOCK) "
    sSQL = sSQL & " where sinistro_id = " & gbldSinistro_ID

    Set rs = Conexao_ExecutarSQL(gsSIGLASISTEMA, _
                                 glAmbiente_id, _
                                 App.Title, _
                                 App.FileDescription, _
                                 sSQL, _
                                 lConexaoLocal, _
                                 True)
    If Not rs.EOF Then
        vEnd_Risco_id = rs!cod_objeto_segurado
        'Ricardo Toledo (Confitec) : 11/07/2013 : INC000004125853 : Inicio
        'atribui o valor a variavel global para utiliza-la em na chamada da procedure SEGS7442_SPS (na function Verifica_valor_estimado)
        gblsCod_Objeto_Segurado = vEnd_Risco_id
        'Ricardo Toledo (Confitec) : 11/07/2013 : INC000004125853 : Fim
    End If

    If strTipoProcesso = strTipoProcesso_Avisar Then
        If bytTipoRamo = bytTipoRamo_RE Or _
           bytTipoRamo = bytTipoRamo_Rural Then
            If vEnd_Risco_id <> 0 And vEstima = "0,00" Then
                If Existe_Resseguro(True) = True Then
                    Call Busca_Perc_Resseguro(gbllProposta_ID, SinEndosso_Id, vEnd_Risco_id, sPercER, sPercResseguro)
                    mskPercResseguro.Text = sPercResseguro
                    mskPercER.Text = sPercER
                End If
            End If
        ElseIf vEstima = "0,00" Then
            If Existe_Resseguro(True) = True Then
                Call Busca_Perc_Resseguro(gbllProposta_ID, SinEndosso_Id, vEnd_Risco_id, sPercER, sPercResseguro)
                mskPercResseguro.Text = sPercResseguro
                mskPercER.Text = sPercER
            End If
        End If
    End If

End Sub
'AKIO.OKUNO - CarregaDadosGrid_EnderecoRisco - 08/10/2012
Private Sub CarregaDadosGrid_EnderecoRisco()
    Dim sSQL As String
    Dim rsRecordSet As ADODB.Recordset
    'Dim iEnd_Risco_id As Integer                   'FLAVIO.BEZERRA - 27/03/2013
    Dim dValor_Estimativa As Double
    Dim sPercER As String
    Dim sPercResseguro As String
    Dim iCodObjetoSegurado As Integer               'FLAVIO.BEZERRA - 27/03/2013

    On Error GoTo Erro

    If bytTipoRamo = bytTipoRamo_RE Then
        MousePointer = vbHourglass

        SelecionaDados_EnderecoRisco lConexaoLocal, gbldSinistro_ID

        InicializaModoProcessamentoObjeto grdEnderecoRisco, 1

        sSQL = ""
        sSQL = sSQL & "Select * "
        sSQL = sSQL & "  From #Sinistro_Principal_Endereco_Risco"
        Set rsRecordSet = Conexao_ExecutarSQL(gsSIGLASISTEMA _
                        , glAmbiente_id _
                        , App.Title _
                        , App.FileDescription _
                        , sSQL _
                        , lConexaoLocal _
                        , True)

        LimpaGrid grdEnderecoRisco

        grdEnderecoRisco.ColWidth(4) = 0    'End_Risco_ID

        dValor_Estimativa = grdEstimativasPagamentos_Estimativas.TextMatrix(1, 1)

        With rsRecordSet
            Do While Not .EOF
                If .Fields!Tipo <> 7 Then
                    'iEnd_Risco_id = .Fields!end_risco_id                   'FLAVIO.BEZERRA - 27/03/2013
                    iCodObjetoSegurado = IIf(IsNull(.Fields!cod_objeto_segurado), 0, .Fields!cod_objeto_segurado)

                    grdEnderecoRisco.Rows = grdEnderecoRisco.Rows + 1
                    grdEnderecoRisco.TextMatrix(grdEnderecoRisco.Rows - 1, 0) = .Fields!cod_objeto_segurado
                    grdEnderecoRisco.TextMatrix(grdEnderecoRisco.Rows - 1, 1) = .Fields!Endereco & ""
                    grdEnderecoRisco.TextMatrix(grdEnderecoRisco.Rows - 1, 2) = "0,00"
                    grdEnderecoRisco.TextMatrix(grdEnderecoRisco.Rows - 1, 3) = "0,00"

                    '                If strTipoProcesso = strTipoProcesso_Avisar Then
                    If bytTipoRamo = bytTipoRamo_RE Or _
                       bytTipoRamo = bytTipoRamo_Rural Then
                        'If iEnd_Risco_id <> 0 Then  'And dValor_Estimativa = "0,00" Then                   'FLAVIO.BEZERRA - 27/03/2013
                         If iCodObjetoSegurado <> 0 Then
                            If Existe_Resseguro(True) = True Then
                                'Call Busca_Perc_Resseguro(gbllProposta_ID, SinEndosso_Id, iEnd_Risco_id, sPercER, sPercResseguro)          'FLAVIO.BEZERRA - 27/03/2013
                                Call Busca_Perc_Resseguro(gbllProposta_ID, SinEndosso_Id, iCodObjetoSegurado, sPercER, sPercResseguro)
                                grdEnderecoRisco.TextMatrix(grdEnderecoRisco.Rows - 1, 2) = sPercResseguro
                                grdEnderecoRisco.TextMatrix(grdEnderecoRisco.Rows - 1, 3) = sPercER
                            End If
                        End If
                    ElseIf dValor_Estimativa = "0,00" Then
                        If Existe_Resseguro(True) = True Then
                            'Call Busca_Perc_Resseguro(gbllProposta_ID, SinEndosso_Id, iEnd_Risco_id, sPercER, sPercResseguro)              'FLAVIO.BEZERRA - 27/03/2013
                            Call Busca_Perc_Resseguro(gbllProposta_ID, SinEndosso_Id, iCodObjetoSegurado, sPercER, sPercResseguro)
                            grdEnderecoRisco.TextMatrix(grdEnderecoRisco.Rows - 1, 2) = sPercResseguro
                            grdEnderecoRisco.TextMatrix(grdEnderecoRisco.Rows - 1, 3) = sPercER
                        End If
                    End If
                    '                End If


                    '                grdEnderecoRisco.TextMatrix(grdEnderecoRisco.Rows - 1, 2) = "0,00"  '% Quota
                    '                grdEnderecoRisco.TextMatrix(grdEnderecoRisco.Rows - 1, 3) = "0,00"  '% ER
                    grdEnderecoRisco.TextMatrix(grdEnderecoRisco.Rows - 1, 4) = IIf(IsNull(.Fields!end_risco_id), "", .Fields!end_risco_id) 'FLAVIO.BEZERRA - 27/03/2013
                End If
                .MoveNext
            Loop
            '            .Sort = 0
        End With

        InicializaModoProcessamentoObjeto grdEnderecoRisco, 2


        'Recalcula grid
        CarregaDadosGrid_AbaEstimativasPagamentos_Valores_Resseguro

        MousePointer = vbNormal

        Set rsRecordSet = Nothing

    End If

    Exit Sub
    Resume
Erro:
    Call TrataErroGeral("CarregaDadosGrid_EnderecoRisco", Me.Caption)
    Call FinalizarAplicacao


End Sub
'AKIO.OKUNO - CarregaDadosGrid_AbaEstimativasPagamentos_Valores_Resseguro - 08/10/2012
Private Sub CarregaDadosGrid_AbaEstimativasPagamentos_Valores_Resseguro()
    Dim iLinha As Integer

    On Error GoTo Erro
    
    Call CarregaDadosValores_PercentualResseguro ' IM00339599 - Data 15.05.2018 - Carregando o cod_objeto - Cleber Sardo
    
    With grdEstimativasPagamentos_Estimativas
        For iLinha = 1 To .Rows - 1
            If Val(.TextMatrix(iLinha, 1)) <> 0 Then
                sSQL = ""
                'OTIMIZANDO
                '                sSql = sSql & "EXEC resseg_db.dbo.totaliza_valor_resseguro_sinistro_spu "
                '                sSql = sSql & "NULL, "
                '                sSql = sSql & "NULL, "
                '                sSql = sSql & "NULL, "
                '                sSql = sSql & "NULL, "
                '                sSql = sSql & "NULL, "
                '                sSql = sSql & "NULL, "
                '                sSql = sSql & gbllProposta_ID & ","
                '                sSql = sSql & SinEndosso_Id & ","
                '                sSql = sSql & grdEnderecoRisco.TextMatrix(grdEnderecoRisco.Row, 4) & ","
                '                sSql = sSql & MudaVirgulaParaPonto(Replace(.TextMatrix(iLinha, 1), ".", ""))
                '
                '                If bytTipoRamo = 1 Then
                '                    sSql = sSql & ", " & IIf(SinCliente_Id <> "0", SinCliente_Id, "NULL")
                '                    If cmbCoberturaAfetada.Text = "" Then
                '                        sSql = sSql & ", NULL,"
                '                    Else
                '                        sSql = sSql & ", " & cmbCoberturaAfetada.ItemData(cmbCoberturaAfetada.ListIndex) & ","
                '                    End If
                '                    sSql = sSql & "'" & Format(SinDT_Ocorrencia, "yyyymm") & "'"
                '                End If
                'resseg_db.dbo.obtem_valor_resseguro_sinistro_sps @proposta_id, @endosso_id, @cod_objeto_segurado, @item_val_estimativa, @val_sinistro, @origem  , @tp_cobertura_id   --R.FOUREAUX 16/05/2012 ADD @tp_cobertura_id,@sinistro_id
                'resseg_db.dbo.obtem_valor_resseguro_sinistro_sps 30221555, 0, 1, 2 , 50, 1, NULL   --R.FOUREAUX 16/05/2012 ADD @tp_cobertura_id,@sinistro_id

                sSQL = sSQL & "Exec resseg_db.dbo.obtem_valor_resseguro_sinistro_sps " & vbNewLine
                sSQL = sSQL & "     " & gbllProposta_ID & vbNewLine
                sSQL = sSQL & "   , " & SinEndosso_Id & vbNewLine
'                sSQL = sSQL & "   , " & grdEnderecoRisco.TextMatrix(grdEnderecoRisco.Row, 4) & vbNewLine   'AKIO.OKUNO - 26/03/2013
'                sSQL = sSQL & "   , " & TrocaVirgulaPorPonto(CDbl("0" & .TextMatrix(.Row, 4))) & vbNewLine  'AKIO.OKUNO - 26/03/2013
                sSQL = sSQL & "   , " & IIf(gblsCod_Objeto_Segurado = "", 0, gblsCod_Objeto_Segurado) & vbNewLine ' IM00339599 - Data 15.05.2018 - Carregando o cod_objeto - Cleber Sardo
                sSQL = sSQL & "   , " & iLinha & vbNewLine
                sSQL = sSQL & "   , " & MudaVirgulaParaPonto(Replace(.TextMatrix(iLinha, 1), ".", "")) & vbNewLine
                sSQL = sSQL & "   , 1" & vbNewLine
                'F.BEZERRA - 12/12/2012 - Aguardando aplica��o da demanda do Foureaux, conforme coment�rio acima.
                'sSQL = sSQL & "   , Null" & vbNewLine

                Set Rst = Conexao_ExecutarSQL(gsSIGLASISTEMA _
                        , glAmbiente_id _
                        , App.Title _
                        , App.FileDescription _
                        , sSQL _
                        , lConexaoLocal _
                        , True)

                If Not Rst.EOF Then
                    '                    .TextMatrix(iLinha, 4) = Format(Rst("val_resseguro"), "############0.00")
                    '.TextMatrix(iLinha, 4) = Format(Rst("val_plano"), "############0.00")
                    'Cleber Sardo - INC000005190610 - Data 27/10/2016 - Tratamento no campo
                     .TextMatrix(iLinha, 4) = Format(IIf(IsNull(Rst("val_plano")), "0.00", Rst("val_plano")), "############0.00")
                    
                End If
            End If
        Next
    End With

    Exit Sub

Erro:
    Call TrataErroGeral("CarregaDadosGrid_AbaEstimativasPagamentos_Valores_Resseguro", Me.Caption)
    Call FinalizarAplicacao

End Sub


Public Sub CarregaDadosValores_PercentualResseguro()

    Dim vEnd_Risco_id As Integer
    Dim vEstima As Long
    Dim sSQL As String
    Dim rs As ADODB.Recordset

    Dim sPercER As String
    Dim sPercResseguro As String

    vEnd_Risco_id = 0
    vEstima = grdEstimativasPagamentos_Estimativas.TextMatrix(1, 1)

    'Ricardo Toledo (Confitec) : 11/07/2013 : INC000004125853 : Inicio
    gblsCod_Objeto_Segurado = ""
    'Ricardo Toledo (Confitec) : 11/07/2013 : INC000004125853 : fim

    sSQL = ""
    sSQL = sSQL & " select cod_objeto_segurado from sinistro_re_tb WITH (NOLOCK) "
    sSQL = sSQL & " where sinistro_id = " & gbldSinistro_ID

    Set rs = Conexao_ExecutarSQL(gsSIGLASISTEMA, _
                                 glAmbiente_id, _
                                 App.Title, _
                                 App.FileDescription, _
                                 sSQL, _
                                 lConexaoLocal, _
                                 True)
    If Not rs.EOF Then
        vEnd_Risco_id = rs!cod_objeto_segurado
        'Ricardo Toledo (Confitec) : 11/07/2013 : INC000004125853 : Inicio
        'atribui o valor a variavel global para utiliza-la em na chamada da procedure SEGS7442_SPS (na function Verifica_valor_estimado)
        gblsCod_Objeto_Segurado = vEnd_Risco_id
        'Ricardo Toledo (Confitec) : 11/07/2013 : INC000004125853 : Fim
    End If


End Sub





'cristovao.rodrigues  ... passadas para o MODsegp1285
'Public Function Existe_Resseguro(Aviso As Boolean) As Boolean
'
'Dim rs As ADODB.Recordset
'Dim vSql As String
'
'On Error GoTo Trata_Erro
'
'    If bytTipoRamo = bytTipoRamo_Vida Then   ' Vida n�o calcula resseguro
'        Existe_Resseguro = False
'        Exit Function
'    ElseIf (gbllRamo_ID = 21) Or (gbllRamo_ID = 22) Or (gbllRamo_ID = 23) Or (gbllRamo_ID = 61) Or (gbllRamo_ID = 63) Then
'        Existe_Resseguro = True
'        Exit Function
'    End If
'
'    vSql = "SELECT proposta_id "
'    vSql = vSql & " FROM proposta_fechada_tb WITH (NOLOCK) "
'    vSql = vSql & " WHERE proposta_id = " & gbllProposta_ID
'    vSql = vSql & " AND situacao_re_seguro in ('s','r') "
'    vSql = vSql & " UNION SELECT proposta_id "
'    vSql = vSql & " FROM proposta_adesao_tb WITH (NOLOCK) "
'    vSql = vSql & " WHERE proposta_id = " & gbllProposta_ID
'    vSql = vSql & " AND situacao_re_seguro in ('s','r') "
'
'    Set rs = Conexao_ExecutarSQL(gsSIGLASISTEMA, _
     '                                 glAmbiente_id, _
     '                                 App.Title, _
     '                                 App.FileDescription, _
     '                                 vSql, _
     '                                 lConexaoLocal, _
     '                                 True)
'
'    If (Not rs.EOF) Then
'        rs.Close
'        Existe_Resseguro = True
'    Else
'        rs.Close
'        If strTipoProcesso = strTipoProcesso_Avisar Then
'            Existe_Resseguro = False
'        Else
'            vSql = "SELECT * FROM sinistro_estimativa_tb WITH (NOLOCK) "
'            vSql = vSql & " WHERE sinistro_id = " & gbldSinistro_ID
'            vSql = vSql & "   AND isnull(val_resseguro,0) <> 0 "
'
'            Set rs = Conexao_ExecutarSQL(gsSIGLASISTEMA, _
             '                                 glAmbiente_id, _
             '                                 App.Title, _
             '                                 App.FileDescription, _
             '                                 vSql, _
             '                                 lConexaoLocal, _
             '                                 True)
'
'            If rs.EOF Then
'                Existe_Resseguro = False
'            Else
'                Existe_Resseguro = True
'            End If
'            rs.Close
'        End If
'    End If
'
'Exit Function
'Trata_Erro:
'    TrataErroGeral "Existe Resseguro"
'    Existe_Resseguro = False
'
'End Function
'
'Public Sub Busca_Perc_Resseguro(ByVal lPropostaId As Long, _
 '                                ByVal lEndossoId As Long, _
 '                                ByVal lCodObjetoSegurado As Long, _
 '                                ByRef sPercER As String, _
 '                                ByRef sPercResseguro As String)
'
'Dim rs As ADODB.Recordset
'Dim sSQL As String
'Dim MensalAnual As String
'
'On Error GoTo Trata_Erro
'
'sSQL = "EXEC resseg_db.dbo.totaliza_valor_resseguro_sinistro_spu "
'sSQL = sSQL & "NULL, "
'sSQL = sSQL & "NULL, "
'sSQL = sSQL & "NULL, "
'sSQL = sSQL & "NULL, "
'sSQL = sSQL & "NULL, "
'sSQL = sSQL & "NULL, "
'sSQL = sSQL & lPropostaId & ","
'sSQL = sSQL & lEndossoId & ","
'sSQL = sSQL & lCodObjetoSegurado & ","
'sSQL = sSQL & " 0,"
'sSQL = sSQL & "NULL, "
'sSQL = sSQL & "NULL, "
'sSQL = sSQL & "NULL "
'
'Set rs = Conexao_ExecutarSQL(gsSIGLASISTEMA, _
 '                                 glAmbiente_id, _
 '                                 App.Title, _
 '                                 App.FileDescription, _
 '                                 sSQL, _
 '                                 lConexaoLocal, _
 '                                 True)
'
'If Not rs.EOF Then
'   If IsNull(rs("perc_er")) = False Then
'    'mskPercER.Text = Format(Val(rs("perc_er")), "##0.000")
'    sPercER = Format(Val(rs("perc_er")), "##0.000")
'   Else
'    'mskPercER.Text = "0,000"
'    sPercER = "0,000"
'   End If
'
'   If IsNull(rs("perc_quota")) = False Then
'    'mskPercResseguro.Text = Format(Val(rs("perc_quota")), "##0.000")
'    sPercResseguro = Format(Val(rs("perc_quota")), "##0.000")
'   Else
'    'mskPercResseguro.Text = "0,000"
'    sPercResseguro = "0,000"
'   End If
'
'End If
'
'rs.Close
'
'Exit Sub
'
'Trata_Erro:
'    TrataErroGeral "Busca_Perc_Resseguro"
'
'End Sub



Private Sub InicializaModoProcessamentoObjeto(objObjeto As Object, bTipo As Byte)
    DoEvents

    With objObjeto
        Select Case bTipo
        Case 1              'Vai carregar
            If TypeOf objObjeto Is MSFlexGrid Then
                .BackColorFixed = &HC0C0C0
                .ForeColorFixed = &H808080
            Else
                .BackColor = &HC0C0C0
            End If
        Case 2              'Carregado
            If TypeOf objObjeto Is MSFlexGrid Then
                .BackColorFixed = &H8000000F
                .ForeColorFixed = &H80000012
            Else
                .BackColor = -2147483643
            End If
        End Select
    End With

End Sub

'Cleber Campos - Nova Tend�ncia - 05/10/2016 -
'18548918 - Corre��o dos Problemas de Sinistro do produto 680
Private Function VerificaProduto680Ramo77() As Boolean
    
   Dim rsAux           As ADODB.Recordset
   Dim sSQL            As String
   Dim ramo_id         As Integer
   Dim produto_id      As Integer
        
   VerificaProduto680Ramo77 = False
    
   On Error GoTo TrataErro
        
   sSQL = ""
   sSQL = " SELECT produto_id, ramo_id"
   sSQL = sSQL & " FROM seguros_db.dbo.proposta_tb WITH (NOLOCK)"
   sSQL = sSQL & " WHERE proposta_id = " & gbllProposta_ID
   
   Set rsAux = ExecutarSQL(gsSIGLASISTEMA, _
                           glAmbiente_id, _
                           App.Title, _
                           App.FileDescription, _
                           sSQL, _
                           True)
    
   ramo_id = 0
   produto_id = 0
        
   If Not rsAux.EOF Then
       ramo_id = IIf(IsNull(rsAux.Fields!ramo_id), 0, rsAux.Fields!ramo_id)
       produto_id = IIf(IsNull(rsAux.Fields!produto_id), 0, rsAux.Fields!produto_id)
   End If
    
   If ramo_id = 77 And produto_id = 680 Then
      VerificaProduto680Ramo77 = True
   End If
      
   rsAux.Close
   Set rsAux = Nothing
    
   Exit Function
       
TrataErro:
        Call TrataErroGeral("VerificaProduto680Ramo77", "frmEstimativa")
        Call FinalizarAplicacao
End Function



Public Function Limite_aprovacao_estimativa(ByVal ptecnico As Integer, ByVal pproduto As Integer, ByVal pramo As Integer, ByVal pItem_Estimativa As Integer) As Boolean
    Dim sSQL As String
    Dim rs As ADODB.Recordset
    'Dim val_prejuizo As String
    Dim dt_fim_vigencia As Date

    On Error GoTo Erro
    
    Limite_aprovacao_estimativa = False 'False = n�o permite a altera��o
        
    'val_prejuizo = TrocaValorBrasPorAme(pValPrejuizo) 'Retirei essa fun��o pois estava transformando 95.000,00 em 95,000.00
    'val_prejuizo = pValPrejuizo
        
    sSQL = " SELECT 1"
    sSQL = sSQL & " FROM seguros_db.DBO.LIMITE_APROVACAO_ESTIMATIVA_TB "
    sSQL = sSQL & " WHERE TECNICO_ID = " & ptecnico
    sSQL = sSQL & " AND PRODUTO_ID = " & pproduto
    sSQL = sSQL & " AND RAMO_ID = " & pramo
    sSQL = sSQL & " AND ITEM_ESTIMATIVA = " & pItem_Estimativa
    sSQL = sSQL & " AND (DT_FIM_VIGENCIA > GETDATE() OR DT_FIM_VIGENCIA IS NULL)  "
    'Set rdoset = rdocn.OpenResultset(SQL)
    
    Set rs = Conexao_ExecutarSQL(gsSIGLASISTEMA, glAmbiente_id, App.Title, App.FileDescription, sSQL, lConexaoLocal, True)
        
    If rs.EOF Then
       Limite_aprovacao_estimativa = False
       Screen.MousePointer = vbDefault
       Exit Function
    Else
       Limite_aprovacao_estimativa = True
    End If
    Exit Function
Erro:
   TrataErroGeral "Limite_aprovacao_estimativa"
   TerminaSEGBR
End Function

Public Function Valida_IS_via_SEGS7442(tp_Cobertura_id As String, Val_Estimado As Double) As Boolean
    Dim ja_pago As Double
    Dim vSql As String
    Dim rs As ADODB.Recordset
    Dim vValorEstimado As Double

    Valida_IS_via_SEGS7442 = True

    vValorEstimado = Val_Estimado

    vSql = "EXEC SEGS7442_SPS " & vbNewLine
    vSql = vSql & " @sinistro_id = " & gbldSinistro_ID & ", " & vbNewLine
    vSql = vSql & " @tp_cobertura_id = " & tp_Cobertura_id & ", " & vbNewLine
    vSql = vSql & " @cod_objeto_segurado = " & IIf(gblsCod_Objeto_Segurado = "", "Null", gblsCod_Objeto_Segurado) & ", " & vbNewLine
    vSql = vSql & " @valor_indenizacao = '" & MudaVirgulaParaPonto(Replace(Val_Estimado, ".", "")) & "', " & vbNewLine
    vSql = vSql & " @tecnico_id = " & Tecnico_Atual & "," & vbNewLine
    vSql = vSql & " @usuario = '" & cUserName & "'" & "," & vbNewLine
    vSql = vSql & " @con_ind_maior_is = 1"
    Set rs = Conexao_ExecutarSQL(gsSIGLASISTEMA, _
                                 glAmbiente_id, _
                                 App.Title, _
                                 App.FileDescription, _
                                 vSql, _
                                 lConexaoLocal, _
                                 True)

    If rs(0) = 0 Then
        'Valor da estimativa maior que a IS
        Valida_IS_via_SEGS7442 = False
    End If
    rs.Close
End Function

Public Function Ramo_Vida(IdRamo As Long) As Boolean
    Dim sSQL As String
    
    sSQL = "select * "
    sSQL = sSQL & "From ramo_tb "
    sSQL = sSQL & "where tp_ramo_id = 1 "
    sSQL = sSQL & "and ramo_id = " & IdRamo
    Set rs = Conexao_ExecutarSQL(gsSIGLASISTEMA, _
                                                 glAmbiente_id, _
                                                 App.Title, _
                                                 App.FileDescription, _
                                                 sSQL, _
                                                 lConexaoLocal, _
                                                 True)
    If Not rs.EOF Then
        Ramo_Vida = True
    Else
        Ramo_Vida = False
    End If
End Function

Public Function Obter_valor_IS_Vida(lPropostaId As Long, lCoberturaId As Long) As Double
    Dim sSQL As String

    sSQL = "SELECT TOP(1) A.IMP_SEGURADA "
    sSQL = sSQL & "FROM seguros_db.dbo.escolha_plano_tb a with(NOLOCK) "
    sSQL = sSQL & "INNER JOIN seguros_db.dbo.plano_tb b with(NOLOCK) "
    sSQL = sSQL & "ON a.plano_id = b.plano_id "
    sSQL = sSQL & "AND a.produto_id = b.produto_id "
    sSQL = sSQL & "INNER JOIN seguros_db.dbo.tp_cob_comp_plano_tb c WITH (NOLOCK) "
    sSQL = sSQL & "ON b.tp_plano_id = c.tp_plano_id "
    sSQL = sSQL & "INNER JOIN seguros_db.dbo.tp_cob_comp_tb d WITH (NOLOCK) "
    sSQL = sSQL & "ON d.tp_cob_comp_id = c.tp_cob_comp_id "
    sSQL = sSQL & "Where a.proposta_id = " & lPropostaId
    sSQL = sSQL & " AND a.dt_fim_vigencia IS NULL "
    sSQL = sSQL & "and b.dt_fim_vigencia IS NULL "
    sSQL = sSQL & "AND TP_COBERTURA_ID = " & lCoberturaId

    sSQL = sSQL & " Union "
     
    sSQL = sSQL & "select TOP(1) VAL_IS "
    sSQL = sSQL & "from seguros_db.dbo.escolha_tp_cob_vida_tb a with (nolock) "
    sSQL = sSQL & "inner join seguros_db.dbo.tp_cob_comp_tb b WITH (NOLOCK) "
    sSQL = sSQL & "ON a.tp_cob_comp_id = b.tp_cob_comp_id "
    sSQL = sSQL & "Where a.proposta_id = " & lPropostaId
    sSQL = sSQL & " AND a.dt_fim_vigencia_esc IS NULL "
    sSQL = sSQL & "AND B.TP_COBERTURA_ID = " & lCoberturaId

    sSQL = sSQL & " Union "
     
    sSQL = sSQL & "select TOP(1) VAL_IS "
    sSQL = sSQL & "from seguros_db.dbo.escolha_tp_cob_generico_tb a with (nolock) "
    sSQL = sSQL & "Where a.proposta_id = " & lPropostaId
    sSQL = sSQL & " AND a.dt_fim_vigencia_esc IS NULL "
    sSQL = sSQL & "AND A.TP_COBERTURA_ID = " & lCoberturaId

    Set rs = Conexao_ExecutarSQL(gsSIGLASISTEMA, _
                                                 glAmbiente_id, _
                                                 App.Title, _
                                                 App.FileDescription, _
                                                 sSQL, _
                                                 lConexaoLocal, _
                                                 True)
    If rs.EOF Then
        'MsgBox "N�o foi encontrado valor de IS para proposta com Id = " & lPropostaId & " e cobertura com Id = " & lCoberturaId & "."
        Obter_valor_IS_Vida = 0
    Else
        Obter_valor_IS_Vida = rs(0)
    End If
    Set rs = Nothing
End Function

Public Function Tem_Sinistro_lider(Id_Sinistro As String) As Boolean
    Dim sSQL As String
    
    sSQL = "SELECT  s.sinistro_id_lider FROM  SEGUROS_DB..SINISTRO_TB s WHERE  sinistro_id = " & dSinistro_ID
    Set rs = Conexao_ExecutarSQL(gsSIGLASISTEMA, _
                                                 glAmbiente_id, _
                                                 App.Title, _
                                                 App.FileDescription, _
                                                 sSQL, _
                                                 lConexaoLocal, _
                                                 True)
    If rs.EOF Then
        MsgBox "N�o foi encontrado sinistro para validar e o mesmo tem sinistro l�der. Id_sinistro = " & dSinistro_ID & "."
    Else
        If IsNull(rs(0)) Then
            'N�o tem sinistro l�der
            Tem_Sinistro_lider = False
        Else
            'Tem sinistro l�der
            Tem_Sinistro_lider = True
        End If
    End If
    Set rs = Nothing
End Function

Public Function Obter_valor_IS_1206(dSinistro_ID As String, lCoberturaId As Long) As Double
    Dim sSQL As String

    sSQL = "select d.class_tp_cobertura,  d.tp_cobertura_id, C.* "
    sSQL = sSQL & " from seguros_db.dbo.evento_SEGBR_sinistro_atual_tb  a with (nolock)"
    sSQL = sSQL & " inner join seguros_db.dbo.sinistro_cobertura_tb b with (nolock)"
    sSQL = sSQL & "  on a.sinistro_id = b.sinistro_id"
    sSQL = sSQL & " inner join seguros_db.dbo.escolha_tp_cob_vida_tb c WITH (NOLOCK)"
    sSQL = sSQL & " ON a.proposta_id = c.proposta_id"
    sSQL = sSQL & " AND c.tp_componente_id = CASE a.ind_tp_sinistrado WHEN 'F' THEN 19 ELSE 20 END"
    sSQL = sSQL & " inner join seguros_db.dbo.tp_cob_comp_tb  d WITH (NOLOCK)"
    sSQL = sSQL & " ON c.tp_cob_comp_id = d.tp_cob_comp_id"
    sSQL = sSQL & "  AND c.tp_componente_id = d.tp_componente_id"
    sSQL = sSQL & " AND b.tp_cobertura_id = d.tp_cobertura_id"
    sSQL = sSQL & " inner join seguros_Db.dbo.sinistro_tb e with (nolock)"
    sSQL = sSQL & "  on a.sinistro_id = e.sinistro_id"
    sSQL = sSQL & " Where evento_bb_id = 1100"
    sSQL = sSQL & " and a.sinistro_id = " & dSinistro_ID '93201911880--93201914484--093201800442"
    sSQL = sSQL & " AND d.TP_COBERTURA_ID = " & lCoberturaId
    sSQL = sSQL & " and b.dt_fim_vigencia is null"
    sSQL = sSQL & " and c.dt_fim_vigencia_esc IS NULL"
    'sSQL = sSQL & " and e.dt_aviso_sinistro between c.dt_inicio_vigencia_esc and isnull(c.dt_fim_vigencia_esc ,GETDATE())"
    
    Set rs = Conexao_ExecutarSQL(gsSIGLASISTEMA, _
                                                 glAmbiente_id, _
                                                 App.Title, _
                                                 App.FileDescription, _
                                                 sSQL, _
                                                 lConexaoLocal, _
                                                 True)
    
    If rs.EOF Then
        Obter_valor_IS_1206 = 0
        'MsgBox "N�o foi encontrado valor de IS para proposta com Id = " & lPropostaId & " e cobertura com Id = " & lCoberturaId & "."
    Else
        Obter_valor_IS_1206 = rs("val_is")
    End If
    Set rs = Nothing
End Function




Public Function Obter_valor_IS_115_150(dSinistro_ID As String) As Double
    Dim sSQL As String

    sSQL = "select "
    sSQL = sSQL & "a.sinistro_id, a.apolice_id, produto_id, a.proposta_id, cpf_cgc_segurado, a.tp_cobertura_id, cl.nome, sg.val_capital_segurado, "
    sSQL = sSQL & "sg.sub_grupo_id, dt_inicio_vigencia_sbg, * "
    sSQL = sSQL & "from seguros_db.dbo.evento_SEGBR_sinistro_atual_tb  a with (nolock) "
    sSQL = sSQL & "inner join seguros_db.dbo.sinistro_cobertura_tb b with (nolock) on a.sinistro_id = b.sinistro_id "
    sSQL = sSQL & "inner join seguros_Db.dbo.sinistro_tb e with (nolock) on a.sinistro_id = e.sinistro_id "
    sSQL = sSQL & "inner join seguros_Db.dbo.seguro_vida_sub_grupo_tb sg ON a.apolice_id = sg.apolice_id "
    sSQL = sSQL & "inner join cliente_tb cl  WITH (NOLOCK) ON  sg.prop_cliente_id = cl.cliente_id "
                                          sSQL = sSQL & "AND a.cpf_sinistrado = cl.cpf_cnpj "
    sSQL = sSQL & "Where "
    sSQL = sSQL & "evento_bb_id = 1100 and "
    sSQL = sSQL & "a.sinistro_id = " & dSinistro_ID
    sSQL = sSQL & " and b.dt_fim_vigencia is null "
    sSQL = sSQL & "AND sg.dt_inicio_vigencia_sbg =        (Select TOP 1 sgp2.dt_inicio_vigencia_sbg "
       sSQL = sSQL & "from seguro_vida_sub_grupo_tb sgp2   WITH (NOLOCK) "
       sSQL = sSQL & "Where sgp2.apolice_id = sg.apolice_id "
           sSQL = sSQL & "AND sgp2.sucursal_seguradora_id = sg.sucursal_seguradora_id "
           sSQL = sSQL & "AND sgp2.seguradora_cod_susep = sg.seguradora_cod_susep "
           sSQL = sSQL & "AND sgp2.ramo_id = sg.ramo_id "
           sSQL = sSQL & "AND sgp2.sub_grupo_id = sg.sub_grupo_id "
           sSQL = sSQL & "AND sgp2.dt_inicio_vigencia_apol_sbg = sg.dt_inicio_vigencia_apol_sbg "
           sSQL = sSQL & "AND sgp2.proposta_id = sg.proposta_id "
           sSQL = sSQL & "AND sgp2.prop_cliente_id = sg.prop_cliente_id "
           sSQL = sSQL & "AND sgp2.tp_componente_id = sg.tp_componente_id "
           sSQL = sSQL & "AND sgp2.seq_canc_endosso_seg = sg.seq_canc_endosso_seg "
       sSQL = sSQL & "ORDER BY sgp2.dt_inicio_vigencia_sbg DESC) "
    
    Set rs = Conexao_ExecutarSQL(gsSIGLASISTEMA, _
                                                 glAmbiente_id, _
                                                 App.Title, _
                                                 App.FileDescription, _
                                                 sSQL, _
                                                 lConexaoLocal, _
                                                 True)
    
    If rs.EOF Then
        Obter_valor_IS_115_150 = 0
        'MsgBox "N�o foi encontrado valor de IS para proposta com Id = " & lPropostaId & " e cobertura com Id = " & lCoberturaId & "."
    Else
        Obter_valor_IS_115_150 = rs("val_capital_segurado")
    End If
    Set rs = Nothing
End Function







Public Function Obter_valor_IS_680(lProposta_ID As Long) As Double
    Dim sSQL As String

    sSQL = "SELECT "
    sSQL = sSQL & "imp_segurada = Max(escolha_tp_cob_generico_tb.val_is) "
    sSQL = sSQL & "FROM escolha_tp_cob_generico_tb  WITH (NOLOCK) "
    sSQL = sSQL & "JOIN tp_cobertura_tb  WITH (NOLOCK) "
    sSQL = sSQL & "ON escolha_tp_cob_generico_tb.tp_cobertura_id = tp_cobertura_tb.tp_cobertura_id "
    sSQL = sSQL & "LEFT JOIN administracao_apolice_tb  WITH (NOLOCK) "
    sSQL = sSQL & "ON administracao_apolice_tb.proposta_id = escolha_tp_cob_generico_tb.proposta_id "
    sSQL = sSQL & "AND administracao_apolice_tb.dt_fim_administracao IS NULL "
    sSQL = sSQL & "LEFT JOIN cobert_obj_risco_tb WITH (NOLOCK) "
    sSQL = sSQL & "ON escolha_tp_cob_generico_tb.proposta_id = cobert_obj_risco_tb.proposta_id "
    sSQL = sSQL & "AND escolha_tp_cob_generico_tb.tp_cobertura_id = cobert_obj_risco_tb.tp_cobertura_id "
    sSQL = sSQL & "AND isnull(cobert_obj_risco_tb.endosso_id,0) = (SELECT MAX(isnull(endosso_id,0)) "
                                             sSQL = sSQL & "FROM cobert_obj_risco_tb WITH (NOLOCK) "
                                             sSQL = sSQL & "WHERE proposta_id = escolha_tp_cob_generico_tb.proposta_id) "
     sSQL = sSQL & "Where escolha_tp_cob_generico_tb.dt_fim_vigencia_esc Is Null "
     sSQL = sSQL & "AND escolha_tp_cob_generico_tb.proposta_id = " & lProposta_ID
    
    Set rs = Conexao_ExecutarSQL(gsSIGLASISTEMA, _
                                                 glAmbiente_id, _
                                                 App.Title, _
                                                 App.FileDescription, _
                                                 sSQL, _
                                                 lConexaoLocal, _
                                                 True)
    
    If rs.EOF Then
        Obter_valor_IS_680 = 0
        'MsgBox "N�o foi encontrado valor de IS para proposta com Id = " & lPropostaId & " e cobertura com Id = " & lCoberturaId & "."
    Else
        Obter_valor_IS_680 = rs("imp_segurada")
    End If
    Set rs = Nothing
End Function










Public Function Obter_valor_IS_811(lProposta_ID As Long, iProduto As String, iCobertura As Integer, iProdutoExterno As Integer) As Double
    'NTendencia - 16/12/2019
    
    Dim sSQL As String
    Dim sTabelaEscolha As String
    
    If iProdutoExterno = "1114" Then
        sTabelaEscolha = "escolha_tp_cob_generico_tb"
    ElseIf iProdutoExterno = "1111" Then
        sTabelaEscolha = "escolha_tp_cob_emp_tb"
    End If
    
    sSQL = "SELECT "
    sSQL = sSQL & " e.val_is "
    sSQL = sSQL & " From " & sTabelaEscolha
    sSQL = sSQL & " e  WITH (NOLOCK) , tp_cobertura_tb c  WITH (NOLOCK) , tp_cob_item_prod_tb t  WITH (NOLOCK) "
    sSQL = sSQL & " Where "
    sSQL = sSQL & " (c.tp_cobertura_id = e.tp_cobertura_id) "
    sSQL = sSQL & " AND        (e.proposta_id = " & lProposta_ID & ") "
    sSQL = sSQL & " AND        (e.produto_id = " & iProduto & ") "
    sSQL = sSQL & " AND        (t.produto_id = " & iProduto & ") "
    sSQL = sSQL & " AND        (t.tp_cobertura_id = e.tp_cobertura_id) "
    sSQL = sSQL & " AND        (e.ramo_id = t.ramo_id) "
    sSQL = sSQL & " and e.dt_fim_vigencia_esc IS NULL "
    sSQL = sSQL & " AND e.num_endosso is null "
    sSQL = sSQL & " and t.tp_cobertura_id = " & iCobertura
    
    Set rs = Conexao_ExecutarSQL(gsSIGLASISTEMA, _
                                                 glAmbiente_id, _
                                                 App.Title, _
                                                 App.FileDescription, _
                                                 sSQL, _
                                                 lConexaoLocal, _
                                                 True)
    
    If rs.EOF Then
        Obter_valor_IS_811 = 0
        'MsgBox "N�o foi encontrado valor de IS para proposta com Id = " & lPropostaId & " e cobertura com Id = " & lCoberturaId & "."
    Else
        Obter_valor_IS_811 = rs("val_is")
    End If
    Set rs = Nothing
End Function













Public Function Obter_produto_externo(lPropostaId As Long) As String
    Dim sSQL As String
    
    
    sSQL = "SELECT produto_externo_id FROM proposta_tb  WITH (NOLOCK)   WHERE proposta_id = " & lPropostaId
        
    Set rs = Conexao_ExecutarSQL(gsSIGLASISTEMA, _
                                                 glAmbiente_id, _
                                                 App.Title, _
                                                 App.FileDescription, _
                                                 sSQL, _
                                                 lConexaoLocal, _
                                                 True)
    
    If rs.EOF Then
        Obter_produto_externo = 0
    Else
        Obter_produto_externo = rs("produto_externo_id")
    End If
    Set rs = Nothing
End Function
















Public Function Obter_valor_IS_400(lProposta_ID As Long, iProduto As String, iCobertura As Integer, iProdutoExterno As Integer) As Double
    'NTendencia - 17/12/2019
    'Trata o produto 400
    Dim sSQL As String
    Dim sTabelaEscolha As String
    
    If iProdutoExterno = "999" And iProduto = "400" Then
        If Tem_numero_ordem(lProposta_ID) Then
            sTabelaEscolha = "escolha_tp_cob_aceito_tb"
        Else
            sTabelaEscolha = "escolha_tp_cob_avulso_tb"
        End If
    End If
    
    sSQL = "SELECT "
    sSQL = sSQL & " e.val_is "
    sSQL = sSQL & " From " & sTabelaEscolha
    sSQL = sSQL & " e  WITH (NOLOCK) , tp_cobertura_tb c  WITH (NOLOCK) , tp_cob_item_prod_tb t  WITH (NOLOCK) "
    sSQL = sSQL & " Where "
    sSQL = sSQL & " (c.tp_cobertura_id = e.tp_cobertura_id) "
    sSQL = sSQL & " AND        (e.proposta_id = " & lProposta_ID & ") "
    sSQL = sSQL & " AND        (e.produto_id = " & iProduto & ") "
    sSQL = sSQL & " AND        (t.produto_id = " & iProduto & ") "
    sSQL = sSQL & " AND        (t.tp_cobertura_id = e.tp_cobertura_id) "
    sSQL = sSQL & " AND        (e.ramo_id = t.ramo_id) "
    sSQL = sSQL & " and e.dt_fim_vigencia_esc IS NULL "
    sSQL = sSQL & " AND e.num_endosso is null "
    sSQL = sSQL & " and t.tp_cobertura_id = " & iCobertura
    
    Set rs = Conexao_ExecutarSQL(gsSIGLASISTEMA, _
                                                 glAmbiente_id, _
                                                 App.Title, _
                                                 App.FileDescription, _
                                                 sSQL, _
                                                 lConexaoLocal, _
                                                 True)
    
    If rs.EOF Then
        Obter_valor_IS_400 = 0
        'MsgBox "N�o foi encontrado valor de IS para proposta com Id = " & lPropostaId & " e cobertura com Id = " & lCoberturaId & "."
    Else
        Obter_valor_IS_400 = rs("val_is")
    End If
    Set rs = Nothing
End Function
















Public Function Tem_numero_ordem(lProposta_ID As Long) As Boolean
    Dim rs1 As ADODB.Recordset
    Dim Rs2 As ADODB.Recordset

    '--1. acho ap�lice e ramo
    sSQL = "SELECT a.apolice_id, a.ramo_id FROM apolice_tb a  WITH (NOLOCK) , proposta_tb p  WITH (NOLOCK)  "
    sSQL = sSQL & "WHERE a.proposta_id = p.proposta_id AND       p.proposta_id = " & lProposta_ID
    Set rs1 = Conexao_ExecutarSQL(gsSIGLASISTEMA, _
                                                 glAmbiente_id, _
                                                 App.Title, _
                                                 App.FileDescription, _
                                                 sSQL, _
                                                 lConexaoLocal, _
                                                 True)
    
    
    '--2. vejo se tem num_ordem
    sSQL = "SELECT s.nome, c.num_apolice_lider, c.dt_emissa_apolice_lider,  c.num_ordem_co_seguro_aceito "
    sSQL = sSQL & " FROM co_seguro_aceito_tb c  WITH (NOLOCK) , apolice_terceiros_tb a  WITH (NOLOCK)  ,      seguradora_tb s  WITH (NOLOCK) "
    sSQL = sSQL & " Where "
    sSQL = sSQL & " a.num_ordem_co_seguro_aceito = C.num_ordem_co_seguro_aceito And a.sucursal_seg_lider = C.sucursal_seg_lider And a.seg_cod_susep_lider = C.seg_cod_susep_lider "
    sSQL = sSQL & " AND s.seguradora_cod_susep = c.seg_cod_susep_lider "
    sSQL = sSQL & " AND a.apolice_id = " & rs1("apolice_id") & " AND a.ramo_id = " & rs1("ramo_id")
    Set Rs2 = Conexao_ExecutarSQL(gsSIGLASISTEMA, _
                                                 glAmbiente_id, _
                                                 App.Title, _
                                                 App.FileDescription, _
                                                 sSQL, _
                                                 lConexaoLocal, _
                                                 True)
    rs1.Close
    Set rs1 = Nothing
    Set Rs2 = Conexao_ExecutarSQL(gsSIGLASISTEMA, _
                                                 glAmbiente_id, _
                                                 App.Title, _
                                                 App.FileDescription, _
                                                 sSQL, _
                                                 lConexaoLocal, _
                                                 True)
    '--3. sem tem numero de ordem
        '--escolha_tp_cob_avulso_tb
    '--se n�o tem
        '--escolha_tp_cob_aceito_tb
    If Rs2.EOF Then
        Tem_numero_ordem = False
    Else
        Tem_numero_ordem = True
    End If
    Rs2.Close
    Set Rs2 = Nothing
End Function



Public Function Obter_valor_IS_721(dSinistro_ID As String, lCoberturaId As Long) As Double
    'NTendencia - 16/12/2019
    
    Dim sSQL As String
    Dim sTabelaEscolha As String
    
    sSQL = "select "
    sSQL = sSQL & "a.sinistro_id, a.evento_bb_id, a.proposta_id, "
    sSQL = sSQL & "d.tp_cobertura_id, c.dt_fim_vigencia_cob, c.tp_cob_comp_id, c.val_is, c.* "
    sSQL = sSQL & "From "
    sSQL = sSQL & "seguros_db.dbo.evento_SEGBR_sinistro_atual_tb  a with (nolock) "
    sSQL = sSQL & "inner join seguros_db.dbo.sinistro_cobertura_tb b with (nolock)  on a.sinistro_id = b.sinistro_id "
    sSQL = sSQL & "inner join escolha_plano_tp_cob_tb c on a.proposta_id = c.proposta_id "
    sSQL = sSQL & "inner join tp_cob_comp_tb d ON d.tp_cob_comp_id = c.tp_cob_comp_id "
    sSQL = sSQL & "and d.tp_cobertura_id = " & lCoberturaId
    sSQL = sSQL & " Where "
    sSQL = sSQL & "a.sinistro_id = " & dSinistro_ID
    sSQL = sSQL & " and a.evento_bb_id = 1100 "
    sSQL = sSQL & " and c.dt_fim_vigencia_cob is null "
    sSQL = sSQL & " and d.tp_cobertura_id = " & lCoberturaId
    sSQL = sSQL & " and b.tp_cobertura_id = " & lCoberturaId
    sSQL = sSQL & " and b.dt_fim_vigencia is null"
    
    
    Set rs = Conexao_ExecutarSQL(gsSIGLASISTEMA, _
                                                 glAmbiente_id, _
                                                 App.Title, _
                                                 App.FileDescription, _
                                                 sSQL, _
                                                 lConexaoLocal, _
                                                 True)
    
    If rs.EOF Then
        Obter_valor_IS_721 = 0
        'MsgBox "N�o foi encontrado valor de IS para proposta com Id = " & lPropostaId & " e cobertura com Id = " & lCoberturaId & "."
    Else
        Obter_valor_IS_721 = rs("val_is")
    End If
    Set rs = Nothing
End Function
















Public Function Obter_valor_IS_1204(dSinistro_ID As String, lCoberturaId As Long, lPropostaId) As Double
    'NTendencia - 07/01/2020
    'C�lculo de acordo com o SEGP0162
    
    Dim sSQL As String
   
    sSQL = "SELECT E.val_is, E.dt_fim_vigencia_esc, S.dt_ocorrencia_sinistro "
    sSQL = sSQL & "FROM seguros_db.dbo.escolha_tp_cob_generico_tb E WITH (NOLOCK) "
    sSQL = sSQL & "INNER JOIN (SELECT proposta_id, MAX(num_endosso) as num_endosso "
    sSQL = sSQL & "FROM seguros_db.dbo.escolha_tp_cob_generico_tb WITH (NOLOCK) "
    sSQL = sSQL & "Where proposta_id = " & lPropostaId
    sSQL = sSQL & " GROUP BY proposta_id) M ON E.proposta_id = M.proposta_id "
    sSQL = sSQL & "AND E.num_endosso = M.num_endosso "
    sSQL = sSQL & "INNER JOIN seguros_db.dbo.sinistro_tb S ON E.proposta_id = S.proposta_id "
    sSQL = sSQL & "AND S.sinistro_id = " & dSinistro_ID
    sSQL = sSQL & " Where e.tp_Cobertura_id = " & lCoberturaId
    
    Set rs = Conexao_ExecutarSQL(gsSIGLASISTEMA, _
                                                 glAmbiente_id, _
                                                 App.Title, _
                                                 App.FileDescription, _
                                                 sSQL, _
                                                 lConexaoLocal, _
                                                 True)
    
    If rs.EOF Then
        Obter_valor_IS_1204 = 0
        'MsgBox "N�o foi encontrado valor de IS para proposta com Id = " & lPropostaId & " e cobertura com Id = " & lCoberturaId & "."
    Else
        Obter_valor_IS_1204 = rs("val_is")
    End If
    Set rs = Nothing

End Function
'C00216281-ESTEIRA-PAGAMENTO-IMEDIATO-VIDA-INDIVIDUAL
Public Function Valida_Fluxo_Pagamento_Imediato() As Boolean
 Dim rs As ADODB.Recordset
 Dim rsCob As ADODB.Recordset
 Dim SQL As String
 Dim a As Long

    Valida_Fluxo_Pagamento_Imediato = False
 
    vSql = vSql & "Select * " & vbCrLf
    vSql = vSql & "  From #EstimativaPagamento_CoberturaAfetada " & vbCrLf
    vSql = vSql & " Order By Tp_Cobertura_Nome"

    Set rsCob = Conexao_ExecutarSQL(gsSIGLASISTEMA, _
                                          glAmbiente_id, _
                                          App.Title, _
                                          App.FileDescription, _
                                          vSql, _
                                          lConexaoLocal, _
                                          True)
                                          
                                          
    If Not rsCob.EOF Then
        Do While Not rsCob.EOF
        
            SQL = ""
            SQL = "EXEC DESENV_DB.dbo.SEGS14630_SPS " & vbNewLine
            SQL = SQL & gbllProposta_ID & "," & vbNewLine
            SQL = SQL & SinEvento_sinistro_id & "," & vbNewLine
            SQL = SQL & rsCob!tp_Cobertura_id & "," & vbNewLine
            SQL = SQL & gbldSinistro_ID & ""
            
            Set rs = ExecutarSQL(gsSIGLASISTEMA, _
                             glAmbiente_id, _
                             App.Title, _
                             App.FileDescription, _
                             SQL, _
                             True)
        
             If Not rs.EOF Then
                If rs(0) = 1 Then
                    Valida_Fluxo_Pagamento_Imediato = True
                End If
            End If
            rsCob.MoveNext
        Loop
    End If

    rsCob.Close
     
End Function 'C00216281 - FIM
'C00216281-ESTEIRA-PAGAMENTO-IMEDIATO-VIDA-INDIVIDUAL
Public Function Classifica_Pagamento_Imediato() As Boolean
    
    Dim rs As ADODB.Recordset
    Dim SQL As String
    Dim a As Long
    
    Classifica_Pagamento_Imediato = False
    
    vSql = vSql & "Select * " & vbCrLf
    vSql = vSql & "  From #EstimativaPagamento_CoberturaAfetada " & vbCrLf
    vSql = vSql & " Order By Tp_Cobertura_Nome"

    Set rsCob = Conexao_ExecutarSQL(gsSIGLASISTEMA, _
                                          glAmbiente_id, _
                                          App.Title, _
                                          App.FileDescription, _
                                          vSql, _
                                          lConexaoLocal, _
                                          True)
                                          
                                          
    If Not rsCob.EOF Then
        Do While Not rsCob.EOF
        
            'CLASSIFICA E GRAVA O DETALHAMENTO
            SQL = ""
            SQL = "EXEC desenv_db.dbo.SEGS14666_SPI " & vbNewLine
            SQL = SQL & vProduto & "," & vbNewLine
            SQL = SQL & SinRamo_Id & "," & vbNewLine
            SQL = SQL & gbllProposta_ID & "," & vbNewLine
            SQL = SQL & SinEvento_sinistro_id & "," & vbNewLine
            SQL = SQL & rsCob!tp_Cobertura_id & "," & vbNewLine
            If gblsTp_Componente_ID <> 0 Then
                    SQL = SQL & gblsTp_Componente_ID & "," & vbNewLine
                Else
                    If vTpComponente = "T" Then
                        SQL = SQL & "1" & "," & vbNewLine 'Titular
                    Else
                        SQL = SQL & "3" & "," & vbNewLine 'Conjuge
                    End If
                End If
            SQL = SQL & "'" & Format(SinDT_Ocorrencia, "yyyymmdd") & "'," & vbNewLine
            'EVENTO_ID = NULL E SINISTRO_ID <> NULL
            'GRAVA O DETALHAMENTO NA SINISTRO_DETALHAMENTO/SINISTRO_LINHA_DETALHAMENTO_TB
            SQL = SQL & " Null " & "," & vbNewLine '@evento_id
            SQL = SQL & gbldSinistro_ID & "," & vbNewLine
            SQL = SQL & "'" & cUserName & "'"
           ' SQL = SQL & ", 1 " ' DEBUG(1) NAO GRAVA NO DETALHAMENTO APENAS CLASSIFICA
                        

            Set rs = Conexao_ExecutarSQL(gsSIGLASISTEMA, _
                            glAmbiente_id, _
                            App.Title, _
                            App.FileDescription, _
                            SQL, numCon, _
                            True)
        
             If Not rs.EOF Then
                If rs.Fields("PagtoImediato").Value = 1 Then
                    Classifica_Pagamento_Imediato = True
                End If
                
                If rs.Fields("PagtoImediato").Value = 1 Or rs.Fields("PagtoImediato").Value = 0 Then
                    PgtoTp_sinistro_parametro_id = rs.Fields("tp_sinistro_parametro_id").Value
                End If
            End If
            rs.Close
           rsCob.MoveNext
        Loop
    End If

    rsCob.Close
   
End Function 'C00216281 - FIM
'C00216281-ESTEIRA-PAGAMENTO-IMEDIATO-VIDA-INDIVIDUAL
Public Sub Gravar_classificacao_Pagamento_Imediato()
   Dim SQL As String
   
   
            SQL = ""
            SQL = "EXEC desenv_db.dbo.SEGS14652_SPI " & vbNewLine
            SQL = SQL & gbldSinistro_ID & "," & vbNewLine
            SQL = SQL & PgtoImediatoSituacao & "," & vbNewLine
            SQL = SQL & PgtoTp_sinistro_parametro_id & "," & vbNewLine
            SQL = SQL & "'" & cUserName & "'"

   
   Call Conexao_ExecutarSQL(gsSIGLASISTEMA, _
                            glAmbiente_id, _
                            App.Title, _
                            App.FileDescription, _
                            SQL, numCon, _
                            True)
   

End Sub 'C00216281 - FIM

'C00216281-ESTEIRA-PAGAMENTO-IMEDIATO-VIDA-INDIVIDUAL
Public Sub Grava_Historico_Classificacao_Sinistro()


 Dim SQL As String
   
            SQL = ""
            SQL = "EXEC desenv_db.dbo.SEGS14674_SPI " & vbNewLine
            SQL = SQL & gbldSinistro_ID & "," & vbNewLine
            SQL = SQL & SinApolice_Id & "," & vbNewLine
            SQL = SQL & SinSucursal_Id & "," & vbNewLine
            SQL = SQL & SinSeguradora_Id & "," & vbNewLine
            SQL = SQL & SinRamo_Id & "," & vbNewLine
            SQL = SQL & "'" & cUserName & "'"

   
   Call Conexao_ExecutarSQL(gsSIGLASISTEMA, _
                            glAmbiente_id, _
                            App.Title, _
                            App.FileDescription, _
                            SQL, numCon, _
                            True)


End Sub 'C00216281 - FIM
