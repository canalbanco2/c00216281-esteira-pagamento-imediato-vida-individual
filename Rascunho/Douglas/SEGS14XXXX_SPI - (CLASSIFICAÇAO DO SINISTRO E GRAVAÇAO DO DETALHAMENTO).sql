USE DESENV_DB
GO

ALTER PROCEDURE dbo.SEGS14XXXX_SPI @produto_id INT
	,@ramo_id INT
	,@proposta_id INT
	,@evento_sinistro_id INT --Sinistro.Causa_id
	,@tp_cobertura_id INT
	,@tp_componente_id INT --.Sinistro.SeguradoComponente
	,@dt_ocorrencia SMALLDATETIME --.Sinistro.dt_ocorrencia
	,@evento_id INT 
	,@usuario varchar(20) = 'SEGS14XXXX_SPI'
	--,@SINISTRO_ID INT = NULL
AS
/*  
--Avisos_Sinistros(i).
 NTENDENCIA 09/01/2019  
 DEMANDA: 00210408-ESTEIRA_FLASH  
 DESCRI��O: PROCEDURE PARA VALIDAR PAGTO IMEDIATO E GRAVAR O DETALHAMENTO.  
 BANCO: SEGUROS_DB   
*/
-- BLOCO DE TESTE   
/*    
  BEGIN TRAN  
    IF @@TRANCOUNT > 0 EXEC desenv_db.dbo.SEGS14XXXX_SPI 1183,77,24730523,28,5,1,'20200217',45448763,'C00216281'  ELSE SELECT 'Erro. A transa��o n�o foi aberta para executar o teste.'

	    SELECT a.sinistro_id
			,B.*
		FROM SEGUROS_DB.DBO.EVENTO_SEGBR_SINISTRO_ATUAL_TB A WITH (NOLOCK)
		INNER JOIN SEGUROS_DB.DBO.EVENTO_SEGBR_SINISTRO_DETALHE_ATUAL_TB B WITH (NOLOCK)
			ON A.EVENTO_ID = B.EVENTO_ID
		WHERE a.proposta_id = 24730523
			AND a.dt_inclusao > '2020-01-01'
  ROLLBACK    
*/
BEGIN
	SET NOCOUNT ON

	-- Declara��o e tratamento de variaveis (inicio)  
	DECLARE @PgtoImediato AS SMALLINT = 1 -- (1-Sim / N�o)  
	DECLARE @Detalhamento AS VARCHAR(3000) = ''
	DECLARE @QuebraLinha AS NVARCHAR(20) = ''

	SET @QuebraLinha = CONCAT (
			CHAR(13)
			,CHAR(10)
			)

	-----------  
	BEGIN TRY
		--VALIDA��O DOS PAR�MENTROS  
		DECLARE @sinistro_parametro_chave_id INT
			--,@CPF_CNPJ VARCHAR(30)
			--,@SQL AS NVARCHAR(1000)
			--,@ParmDefinition AS NVARCHAR(300)

		IF OBJECT_ID('TEMPDB..#DETALHAMENTO') IS NOT NULL
		BEGIN
			DROP TABLE #DETALHAMENTO
		END

		CREATE TABLE #DETALHAMENTO (
			id INT IDENTITY(1, 1) NOT NULL
			,linha VARCHAR(60) NULL -- TAMANHO MAXIMO ACEITO NO DETALHAMENTO
			)

		--INICIALIZANDO O DETALHAMENTO


		INSERT INTO #DETALHAMENTO (linha)
		SELECT '--------------------------'

		INSERT INTO #DETALHAMENTO (linha)
		SELECT '' --

		INSERT INTO #DETALHAMENTO (linha)
		SELECT '' --

		INSERT INTO #DETALHAMENTO (linha)
		SELECT 'CLASSIFICA��O DO SINISTRO:'

		INSERT INTO #DETALHAMENTO (linha)
		SELECT '--------------------------'

		INSERT INTO #DETALHAMENTO (linha)
		SELECT '' -- GRAVA��O DA CLASSIFICA��O, ID 6 

		INSERT INTO #DETALHAMENTO (linha)
		SELECT '' --

		INSERT INTO #DETALHAMENTO (linha)
		SELECT 'Informa��es:'

		INSERT INTO #DETALHAMENTO (linha)
		SELECT '' --

		--PEGANDO A CHAVE DOS PARAMETROS  
		--ENCONTRANDO A CHAVE VIGENTE DA PROPOSTA  
		SELECT DISTINCT @sinistro_parametro_chave_id = B.sinistro_parametro_chave_id
		FROM desenv_db.dbo.tp_sinistro_parametro_tb a WITH (NOLOCK)
		INNER JOIN desenv_db.dbo.sinistro_parametro_chave_tb b WITH (NOLOCK)
			ON a.tp_sinistro_parametro_id = b.tp_sinistro_parametro_id
		INNER JOIN desenv_db.dbo.sinistro_parametro_chave_regra_tb c WITH (NOLOCK)
			ON b.sinistro_parametro_chave_id = c.sinistro_parametro_chave_id
		INNER JOIN desenv_db.dbo.sinistro_parametro_regra_tb d WITH (NOLOCK)
			ON c.sinistro_parametro_regra_id = d.sinistro_parametro_regra_id
		WHERE 1 = 1
			AND b.produto_id = @produto_id -- 1183  
			AND b.ramo_id = @ramo_id -- 77   
			AND b.evento_sinistro_id = @evento_sinistro_id -- 28 --  
			AND b.tp_cobertura_id = @tp_cobertura_id -- 5 --  
			AND b.dt_fim_vigencia IS NULL

		IF @sinistro_parametro_chave_id IS NULL
		BEGIN
			SET @PgtoImediato = 0

			INSERT INTO #DETALHAMENTO (linha)
			SELECT 'O produto,ramo,evento e cobertura selecionados no aviso'

			INSERT INTO #DETALHAMENTO (linha)
			SELECT 'n�o est�o habilitados para o fluxo do pagamento imediato'

			INSERT INTO #DETALHAMENTO (linha)
			SELECT ''

			SET @Detalhamento = CONCAT (
					@Detalhamento
					,@QuebraLinha
					--,'O produto ' + convert(VARCHAR(60), @produto_id) + ' ramo ' + convert(VARCHAR(60), @ramo_id) + ' evento ' + convert(VARCHAR(60), @evento_sinistro_id) + ' cobertura ' + convert(VARCHAR(60), @tp_cobertura_id)  
					,'O produto,ramo,evento e cobertura selecionados no aviso'
					,@QuebraLinha
					,'n�o est�o habilitados para o fluxo do pagamento imediato'
					)
		END
		ELSE
		BEGIN
			----------------------------------------------------------------------------------------------------------------------------------------------------- 
			--###################################################################################################################################################  
			--1� VALIDA��O DA VIG�NCIA COBERTURA E APOLICE 'verifica_vigencia_cobertura_apolice'  
			--Regra: VALIDA��O DE TELA: OCORRENCIA DENTRO DA VIG�NCIA DA (AP�LICE E COBERTURA)  
			--BUSCANDO A REGRA PARAMETRIZADA  
			/*  
		   SELECT REGRA.descricao,CHAVE.valor,CHAVE.operador,REGRA.tipo,*  FROM desenv_db.dbo.sinistro_parametro_chave_regra_tb CHAVE  
			INNER JOIN desenv_db.dbo.sinistro_parametro_regra_tb REGRA  
		   ON CHAVE.sinistro_parametro_regra_id = REGRA.sinistro_parametro_regra_id  
		   WHERE 1=1  
			 AND CHAVE.sinistro_parametro_chave_id = 96 --@sinistro_parametro_chave_id  
			 AND REGRA.NOME = 'verifica_vigencia_cobertura_apolice'  
  
		 */
			DECLARE @Dt_ini AS SMALLDATETIME
			DECLARE @Dt_fim AS SMALLDATETIME
			DECLARE @VALIDA_VIGENCIA_APOLICE_COBERTURA CHAR(1)

			SELECT @VALIDA_VIGENCIA_APOLICE_COBERTURA = CHAVE.VALOR
			FROM desenv_db.dbo.sinistro_parametro_chave_regra_tb CHAVE WITH (NOLOCK)
			INNER JOIN desenv_db.dbo.sinistro_parametro_regra_tb REGRA WITH (NOLOCK)
				ON CHAVE.sinistro_parametro_regra_id = REGRA.sinistro_parametro_regra_id
			WHERE CHAVE.sinistro_parametro_chave_id = 96 --@sinistro_parametro_chave_id --1079 --  
				AND REGRA.NOME = 'verifica_vigencia_cobertura_apolice'

			IF @VALIDA_VIGENCIA_APOLICE_COBERTURA = 'S'
			BEGIN
				DECLARE @cod_objeto_segurado INT
					,@tp_ramo_id INT

				SELECT @tp_ramo_id = TP_RAMO_ID
				FROM seguros_db.dbo.ramo_tb WITH (NOLOCK)
				WHERE ramo_id = @RAMO_ID --77

				--VALIDA��O DE TELA: OCORRENCIA DENTRO DA VIG�NCIA DA (AP�LICE E COBERTURA)  
				IF OBJECT_ID('TEMPDB..#vigencia') IS NOT NULL
				BEGIN
					DROP TABLE #vigencia
				END

				IF OBJECT_ID('TEMPDB..#COBERTURAS_VIGENTES') IS NOT NULL
				BEGIN
					DROP TABLE #COBERTURAS_VIGENTES
				END

				CREATE TABLE #VIGENCIA (
					id INT IDENTITY(1, 1) NOT NULL PRIMARY KEY
					,dt_inicio_vigencia SMALLDATETIME NULL
					,dt_fim_vigencia SMALLDATETIME NULL
					,vigencia CHAR(1) NULL --(A)police / (C)obertura  
					)

				CREATE TABLE #COBERTURAS_VIGENTES (
					tp_cobertura_id INT NULL
					,nome_COBERTURA VARCHAR(600) NULL
					,val_is NUMERIC(15, 2) NULL
					,dt_inicio_vigencia SMALLDATETIME NULL
					,dt_fim_vigencia SMALLDATETIME NULL
					,cod_obejeto_segurado INT NULL
					,texto_franquia VARCHAR(600) NULL
					)

				--AP�LICE  
				INSERT INTO #vigencia (
					dt_inicio_vigencia
					,dt_fim_vigencia
					,vigencia
					)
				SELECT ISNULL(APOLICE.dt_inicio_vigencia, ISNULL(ADESAO.dt_inicio_vigencia, FECHADA.dt_inicio_vig))
					,ISNULL(APOLICE.dt_fim_vigencia, ISNULL(ADESAO.dt_fim_vigencia, FECHADA.dt_fim_vig))
					,'A' --AP�LICE  
				FROM seguros_db.dbo.proposta_tb proposta WITH (NOLOCK)
				LEFT JOIN seguros_db.dbo.proposta_fechada_tb fechada WITH (NOLOCK)
					ON proposta.proposta_id = fechada.proposta_id
				LEFT JOIN seguros_db.dbo.proposta_adesao_tb adesao WITH (NOLOCK)
					ON proposta.proposta_id = adesao.proposta_id
				LEFT JOIN seguros_db.dbo.apolice_tb apolice WITH (NOLOCK)
					ON proposta.proposta_id = apolice.proposta_id
				WHERE proposta.proposta_id = @proposta_id --24730523--

				--COBERTURAS DA PROPOSTA
				INSERT INTO #COBERTURAS_VIGENTES (
					tp_cobertura_id
					,nome_COBERTURA
					,val_is
					,dt_inicio_vigencia
					,dt_fim_vigencia
					,cod_obejeto_segurado
					,texto_franquia
					)
				EXEC seguros_db.dbo.consultar_coberturas_sps '20200212' --DT_OCORRENCIA
					,@PROPOSTA_ID --24730523
					,@tp_componente_id --1              
					,NULL -- @sub_grupo_id            
					,@tp_ramo_id --1-

				INSERT INTO #vigencia (
					dt_inicio_vigencia
					,dt_fim_vigencia
					,vigencia
					)
				SELECT dt_inicio_vigencia
					,dt_fim_vigencia
					,'C' --COBERTURA    
				FROM #COBERTURAS_VIGENTES
				WHERE 1 = 1
					AND tp_cobertura_id = @tp_cobertura_id --5   

				--select * from seguros_db.dbo.evento_segbr_sinistro_atual_tb where proposta_id = 24730523
				--select * from seguros_db.dbo.evento_segbr_sinistro_cobertura_tb where evento_id = 45448763
				--45448763	77202000005	31121
				--BUSCANDO TODAS AS COBERTURAS ATINGIDAS
				-------------------------------------------------------------------  
				--INSERT INTO #VIGENCIA (
				--	dt_inicio_vigencia
				--	,dt_fim_vigencia
				--	,vigencia
				--	)
				--SELECT ESTIMATIVA_SINISTRO.dt_inicio_vigencia
				--	,ESTIMATIVA_SINISTRO.dt_fim_vigencia
				--	,'C' AS vigencia
				--FROM SEGUROS_DB.DBO.EVENTO_SEGBR_SINISTRO_COBERTURA_TB SINISTRO_COB WITH (NOLOCK)
				--INNER JOIN SEGUROS_DB.DBO.TP_COBERTURA_TB TP_COBERTURA WITH (NOLOCK)
				--	ON SINISTRO_COB.TP_COBERTURA_ID = TP_COBERTURA.TP_COBERTURA_ID
				--INNER JOIN SEGUROS_DB.DBO.PRODUTO_ESTIMATIVA_SINISTRO_TB ESTIMATIVA_SINISTRO WITH (NOLOCK)
				--	ON SINISTRO_COB.TP_COBERTURA_ID = ESTIMATIVA_SINISTRO.TP_COBERTURA_ID
				--INNER JOIN SEGUROS_DB.DBO.PROPOSTA_TB PROPOSTA WITH (NOLOCK)
				--ON ESTIMATIVA_SINISTRO.PRODUTO_ID = PROPOSTA.PRODUTO_ID
				--AND ESTIMATIVA_SINISTRO.RAMO_ID = PROPOSTA.RAMO_ID
				--AND ESTIMATIVA_SINISTRO.SUBRAMO_ID = PROPOSTA.SUBRAMO_ID
				--WHERE 1 = 1
				--	AND PROPOSTA.PROPOSTA_ID =@PROPOSTA_ID --24730523 --
				--	AND SINISTRO_COB.EVENTO_ID = @evento_id --45448763 -- --45448763 
				--	AND SINISTRO_COB.TP_COBERTURA_ID = @TP_COBERTURA_ID --5 -- --5 
				--	AND ESTIMATIVA_SINISTRO.evento_sinistro_id = @EVENTO_SINISTRO_ID  --28 --
				--	AND ESTIMATIVA_SINISTRO.tp_componente_id = @tp_componente_id --1 --
				--	AND ESTIMATIVA_SINISTRO.situacao = 'A'
				--AND C.situacao = 'A'
				--and c.tp_componente_id = 1
				--AND c.evento_sinistro_id = 28
				--and d.proposta_id = 24730523
				--and produto_id = 1183 -- @produto_id
				--AND ramo_id = 77 -- ramo_id
				--AND subramo_id = 7719
				--and d.proposta_id = --@proposta_id
				--INSERT INTO #COBERTURAS_ATINGIDAS (
				--		tp_cobertura_id
				--		,nome
				--		--,dt_inicio_vigencia
				--		--,dt_fim_vigencia
				--		)
				--	SELECT A.TP_COBERTURA_ID
				--		,B.NOME
				--		--,dt_inicio_vigencia
				--		--,dt_fim_vigencia
				--	FROM SEGUROS_DB.DBO.EVENTO_SEGBR_SINISTRO_COBERTURA_TB A WITH (NOLOCK)
				--	INNER JOIN SEGUROS_DB.DBO.TP_COBERTURA_TB B WITH (NOLOCK)
				--		ON A.TP_COBERTURA_ID = B.TP_COBERTURA_ID
				--	--INNER JOIN SEGUROS_DB.DBO.PRODUTO_ESTIMATIVA_SINISTRO_TB C WITH (NOLOCK)
				--	--ON A.TP_COBERTURA_ID = C.TP_COBERTURA_ID
				--	WHERE EVENTO_ID = 45448763--@evento_id --45448763 
				--		AND A.TP_COBERTURA_ID = 5 --@TP_COBERTURA_ID --5 
				--		--AND C.situacao = 'A'
				--------------------------------------------------------------------------------------------------
				----VALIDA COBERTURA ATINGIDA
				--SELECT val_inicial
				--	,perc_estimativa
				--	,isnull(utiliza_percentual_subevento, 'N') utiliza_percentual_subevento
				--	,nome
				--	,a.cobertura_bb
				--	,dt_inicio_vigencia
				--	,dt_fim_vigencia
				--	,situacao
				--FROM produto_estimativa_sinistro_tb a WITH (NOLOCK)
				--	inner join seguros_db.dbo.tp_cobertura_tb b WITH (NOLOCK)
				--	on a.tp_cobertura_id = b.tp_cobertura_id
				--WHERE produto_id = 1183
				--	AND ramo_id = 77
				--	AND subramo_id = 7719
				--	AND tp_componente_id = 1
				--	AND evento_sinistro_id = 28
				--	AND a.tp_cobertura_id = 5
				--	AND convert(CHAR(8), dt_inicio_vigencia, 112) <= '20200214'
				--	AND (
				--		convert(CHAR(8), dt_fim_vigencia, 112) >= '20200214'
				--		OR dt_fim_vigencia IS NULL
				--		)
				--	--AND a.situacao = 'A'
				--------------------------------------------------------------------------------------------------
				--SELECT * FROM desenv_db.dbo.sinistro_parametro_chave_tb A 
				--INNER JOIN #COBERTURAS_ATINGIDAS B
				--ON A.tp_cobertura_id = B.tp_cobertura_id
				--WHERE  1 = 1
				--AND A.produto_id = 1183--@produto_id -- 1241  
				--AND A.ramo_id = 77--@ramo_id -- 14   
				--AND A.evento_sinistro_id = 28-- @evento_sinistro_id -- 3 -- 
				--AND COUNT()
				------------------------------------------------------------------  
				--OCORRENCIA VIG�NCIA AP�LICE  
				IF NOT EXISTS (
						SELECT TOP 1 1
						FROM #vigencia v
						WHERE v.vigencia = 'A'
							AND @dt_ocorrencia BETWEEN v.dt_inicio_vigencia
								AND v.dt_fim_vigencia
						)
				BEGIN
					--RECUSA (SEM VIG�NCIA)  
					SELECT @Dt_ini = v.dt_inicio_vigencia
						,@Dt_fim = v.dt_fim_vigencia
					FROM #vigencia v
					WHERE v.vigencia = 'A'

					SET @PgtoImediato = 0

					--DELETE #DETALHAMENTO
					INSERT INTO #DETALHAMENTO (linha)
					SELECT 'Pagamento imediato negado:'

					INSERT INTO #DETALHAMENTO (linha)
					SELECT 'A data de ocorrencia ' + FORMAT(@dt_ocorrencia, 'dd/MM/yyyy') + ' do aviso'

					INSERT INTO #DETALHAMENTO (linha)
					SELECT 'n�o esta dentro do per�odo de vig�ncia da ap�lice.'

					INSERT INTO #DETALHAMENTO (linha)
					SELECT convert(VARCHAR(20), @dt_ini, 103) + ' � ' + convert(VARCHAR(20), @dt_fim, 103)

					INSERT INTO #DETALHAMENTO (linha)
					SELECT ''

					SET @Detalhamento = CONCAT (
							@Detalhamento
							,@QuebraLinha
							,'Pagamento imediato negado:'
							,@QuebraLinha
							,'A data de ocorrencia ' + FORMAT(@dt_ocorrencia, 'dd/MM/yyyy') + ' do aviso'
							,@QuebraLinha
							,'n�o esta dentro do per�odo de vig�ncia da ap�lice.'
							,@QuebraLinha
							,convert(VARCHAR(20), @dt_ini, 103) + ' � ' + convert(VARCHAR(20), @dt_fim, 103)
							)
				END
				ELSE
				BEGIN
					INSERT INTO #DETALHAMENTO (linha)
					SELECT 'A data de ocorrencia do aviso'

					INSERT INTO #DETALHAMENTO (linha)
					SELECT 'esta dentro do per�odo de vig�ncia da ap�lice.'

					INSERT INTO #DETALHAMENTO (linha)
					SELECT ''

					SET @Detalhamento = CONCAT (
							@Detalhamento
							,@QuebraLinha
							,'A data de ocorrencia do aviso'
							,@QuebraLinha
							,'esta dentro do per�odo de vig�ncia da ap�lice.'
							)
				END

				--OCORRENCIA VIG�NCIA COBERTURA    
				IF NOT EXISTS (
						SELECT TOP 1 1
						FROM #vigencia v
						WHERE v.vigencia = 'C'
							AND @dt_ocorrencia BETWEEN v.dt_inicio_vigencia
								AND isnull(v.dt_fim_vigencia, getdate() + 1)
						)
				BEGIN
					--RECUSA (SEM VIG�NCIA)    
					SELECT @Dt_ini = v.dt_inicio_vigencia
						,@Dt_fim = v.dt_fim_vigencia
					FROM #vigencia v
					WHERE v.vigencia = 'C'

					SET @PgtoImediato = 0

					INSERT INTO #DETALHAMENTO (linha)
					SELECT 'Pagamento imediato negado:'

					INSERT INTO #DETALHAMENTO (linha)
					SELECT 'A data de ocorrencia ' + FORMAT(@dt_ocorrencia, 'dd/MM/yyyy') + ' do aviso'

					INSERT INTO #DETALHAMENTO (linha)
					SELECT 'n�o esta dentro do per�odo de vig�ncia da cobertura.'

					SET @Detalhamento = CONCAT (
							@Detalhamento
							,@QuebraLinha
							,'Pagamento imediato negado:'
							,@QuebraLinha
							,'A data de ocorrencia ' + FORMAT(@dt_ocorrencia, 'dd/MM/yyyy') + ' do aviso'
							,@QuebraLinha
							,'n�o esta dentro do per�odo de vig�ncia da cobertura.'
							)

					IF @Dt_ini IS NOT NULL
						AND @Dt_fim IS NOT NULL
					BEGIN
						INSERT INTO #DETALHAMENTO (linha)
						SELECT FORMAT(@dt_ini, 'dd/MM/yyyy') + ' � ' + FORMAT(@dt_fim, 'dd/MM/yyyy')

						SET @Detalhamento = CONCAT (
								@Detalhamento
								,@QuebraLinha
								,FORMAT(@dt_ini, 'dd/MM/yyyy') + ' � ' + FORMAT(@dt_fim, 'dd/MM/yyyy')
								)
					END

					INSERT INTO #DETALHAMENTO (linha)
					SELECT ''
				END
				ELSE
				BEGIN
					INSERT INTO #DETALHAMENTO (linha)
					SELECT 'A data de ocorrencia do aviso ' + FORMAT(@dt_ocorrencia, 'dd/MM/yyyy')

					INSERT INTO #DETALHAMENTO (linha)
					SELECT 'esta dentro do per�odo de vig�ncia da cobertura.'

					INSERT INTO #DETALHAMENTO (linha)
					SELECT ''

					SET @Detalhamento = CONCAT (
							@Detalhamento
							,@QuebraLinha
							,'A data de ocorrencia do aviso ' + FORMAT(@dt_ocorrencia, 'dd/MM/yyyy')
							,@QuebraLinha
							,'esta dentro do per�odo de vig�ncia da cobertura.'
							)
				END
			END

			-----------------------------------------------------------------------------------------------------------------------------------------------------  
			--###################################################################################################################################################  
			--FIM DA CLASSIFICA��O  
			-----------------------------------------------------------------------------------------------------------------------------------------------------  
			IF @PgtoImediato = 1
			BEGIN
				UPDATE A
				SET LINHA = 'O SINISTRO FOI CLASSIFICADO COMO PAGAMENTO IMEDIATO'
				FROM #DETALHAMENTO A
				WHERE ID = 6

				SET @detalhamento = CONCAT (
						'O SINISTRO FOI CLASSIFICADO COMO PAGAMENTO IMEDIATO'
						,@QuebraLinha
						,@QuebraLinha
						,'Informa��es:'
						,@QuebraLinha
						,@detalhamento
						)
			END
			ELSE
			BEGIN
				UPDATE A
				SET LINHA = 'O SINISTRO N�O FOI CLASSIFICADO COMO PAGAMENTO IMEDIATO'
				FROM #DETALHAMENTO A
				WHERE ID = 6

				SET @detalhamento = CONCAT (
						'O SINISTRO N�O FOI CLASSIFICADO COMO PAGAMENTO IMEDIATO'
						,@QuebraLinha
						,@QuebraLinha
						,'Informa��es:'
						,@QuebraLinha
						,@detalhamento
						)
			END

			--FINALIZANDO A CRIACAO DO DETALHAMENTO
			INSERT INTO #DETALHAMENTO (linha)
			SELECT ''

			INSERT INTO #DETALHAMENTO (linha)
			SELECT '--------------------------'

			--INICIANDO A GRAVA��O DO DETALHAMENTO

			DECLARE @MAX_SEQ_DETALHE INT

			SELECT @MAX_SEQ_DETALHE = seq_detalhe
			FROM SEGUROS_DB.DBO.evento_SEGBR_sinistro_detalhe_atual_tb WITH (NOLOCK)
			WHERE EVENTO_ID = @evento_id

			--GRAVANDO O DETALHAMENTO
			INSERT INTO SEGUROS_DB.DBO.evento_SEGBR_sinistro_detalhe_atual_tb (
				evento_id
				,tp_detalhe
				,seq_detalhe
				,descricao
				,usuario
				,dt_inclusao
				)
			SELECT @evento_id AS evento_id
				,'ANOTACAO' as tp_detalhe
				,@MAX_SEQ_DETALHE + ID as seq_detalhe
				,LINHA as descricao
				,@usuario as usuario
				,GETDATE() as dt_inclusao
			FROM #DETALHAMENTO WITH (NOLOCK)

			--SELECT @Detalhamento AS Detalhamento
			--	,@PgtoImediato AS PagtoImediato
			SELECT @PgtoImediato AS PagtoImediato
		END

		-- (fim) Bloco de codifica��o da procedure  
		-----------      
		RETURN
	END TRY

	BEGIN CATCH
		DECLARE @ErrorMessage NVARCHAR(4000)
		DECLARE @ErrorSeverity INT
		DECLARE @ErrorState INT

		SELECT @ErrorMessage = ERROR_PROCEDURE() + ' - Linha ' + CONVERT(VARCHAR(15), ERROR_LINE()) + ' - ' + ERROR_MESSAGE()
			,@ErrorSeverity = ERROR_SEVERITY()
			,@ErrorState = ERROR_STATE()

		RAISERROR (
				@ErrorMessage
				,@ErrorSeverity
				,@ErrorState
				)
	END CATCH
END
GO


