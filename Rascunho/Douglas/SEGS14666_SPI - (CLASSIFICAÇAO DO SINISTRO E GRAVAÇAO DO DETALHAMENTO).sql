CREATE PROCEDURE dbo.SEGS14666_SPI @produto_id INT
	,@ramo_id INT
	,@proposta_id INT
	,@evento_sinistro_id INT
	,@tp_cobertura_id INT
	,@tp_componente_id INT
	,@dt_ocorrencia SMALLDATETIME
	,@evento_id INT = NULL --AVISO REALIZADO PELO SEGP1296
	,@sinistro_id NUMERIC(11, 0) = NULL --AVISO REALIZADO PELO SEGP0794
	,@usuario VARCHAR(20) = 'SEGS14666_SPI'
	,@debug SMALLINT = 0 -- MODO DEBUG(1) NAO REALIZA O INSERT NA TABELA FINAL
AS
/*  
 NTENDENCIA 18/002/2020  
 DEMANDA: C00216281-ESTEIRA-PAGAMENTO-IMEDIATO-VIDA-INDIVIDUAL 
 DESCRI��O: PROCEDURE PARA VALIDAR PAGTO IMEDIATO E GRAVAR O DETALHAMENTO.  
 BANCO: SEGUROS_DB   
*/
-- BLOCO DE TESTE   
/*
		BEGIN TRAN

		DECLARE @produto_id INT = 1183
			,@ramo_id INT = 77
			,@proposta_id INT = 24730523
			,@evento_sinistro_id INT = 28
			,@tp_cobertura_id INT = 5
			,@tp_componente_id INT = 1
			,@dt_ocorrencia SMALLDATETIME = '20200217'
			,@evento_id INT = 45448763
			,@sinistro_id NUMERIC(11,0) = NULL,
			,@usuario VARCHAR(20) = 'C00216281'
			,@debug SMALLINT = 0

		IF @@TRANCOUNT > 0
		BEGIN
			EXEC seguros_db.dbo.SEGS14666_SPI @produto_id
				,@ramo_id
				,@proposta_id
				,@evento_sinistro_id
				,@tp_cobertura_id
				,@tp_componente_id
				,@dt_ocorrencia
				,@evento_id
				,@sinistro_id
				,@usuario
				,@debug

			IF @DEBUG <> 1
				SELECT a.sinistro_id
					,B.*
				FROM SEGUROS_DB.DBO.EVENTO_SEGBR_SINISTRO_ATUAL_TB A WITH (NOLOCK)
				INNER JOIN SEGUROS_DB.DBO.EVENTO_SEGBR_SINISTRO_DETALHE_ATUAL_TB B WITH (NOLOCK)
					ON A.EVENTO_ID = B.EVENTO_ID
				WHERE a.proposta_id = @proposta_id
					AND b.evento_id = @evento_id
		END
		ELSE
			SELECT 'Erro. A transa��o n�o foi aberta para executar o teste.'

		ROLLBACK

    
*/
BEGIN
	SET NOCOUNT ON

	-- Declara��o e tratamento de variaveis (inicio)  
	DECLARE @PgtoImediato AS SMALLINT = 1 -- (1-Sim / N�o)  

	-----------  
	BEGIN TRY
		--VALIDA��O DOS PAR�MENTROS  
		DECLARE @sinistro_parametro_chave_id INT
			,@tp_sinistro_parametro_id INT

		IF OBJECT_ID('TEMPDB..#DETALHAMENTO') IS NOT NULL
		BEGIN
			DROP TABLE #DETALHAMENTO
		END

		CREATE TABLE #DETALHAMENTO (
			id INT IDENTITY(1, 1) NOT NULL
			,linha VARCHAR(60) NULL -- TAMANHO MAXIMO ACEITO NO DETALHAMENTO
			)

		--INICIALIZANDO O DETALHAMENTO
		INSERT INTO #DETALHAMENTO (linha)
		SELECT '--------------------------'

		INSERT INTO #DETALHAMENTO (linha)
		SELECT '' --

		INSERT INTO #DETALHAMENTO (linha)
		SELECT '' --

		INSERT INTO #DETALHAMENTO (linha)
		SELECT 'CLASSIFICA��O DO SINISTRO:'

		INSERT INTO #DETALHAMENTO (linha)
		SELECT '--------------------------'

		INSERT INTO #DETALHAMENTO (linha)
		SELECT '' -- GRAVA��O DA CLASSIFICA��O, ID 6 

		INSERT INTO #DETALHAMENTO (linha)
		SELECT '' --

		INSERT INTO #DETALHAMENTO (linha)
		SELECT 'Informa��es:'

		INSERT INTO #DETALHAMENTO (linha)
		SELECT '' --

		--PEGANDO A CHAVE DOS PARAMETROS  
		--ENCONTRANDO A CHAVE VIGENTE DA PROPOSTA  
		SELECT DISTINCT @sinistro_parametro_chave_id = B.sinistro_parametro_chave_id
			,@tp_sinistro_parametro_id = A.tp_sinistro_parametro_id
		FROM desenv_db.dbo.tp_sinistro_parametro_tb a WITH (NOLOCK)
		INNER JOIN desenv_db.dbo.sinistro_parametro_chave_tb b WITH (NOLOCK)
			ON a.tp_sinistro_parametro_id = b.tp_sinistro_parametro_id
		INNER JOIN desenv_db.dbo.sinistro_parametro_chave_regra_tb c WITH (NOLOCK)
			ON b.sinistro_parametro_chave_id = c.sinistro_parametro_chave_id
		INNER JOIN desenv_db.dbo.sinistro_parametro_regra_tb d WITH (NOLOCK)
			ON c.sinistro_parametro_regra_id = d.sinistro_parametro_regra_id
		WHERE 1 = 1
			AND b.produto_id = @produto_id -- 1183  
			AND b.ramo_id = @ramo_id -- 77   
			AND b.evento_sinistro_id = @evento_sinistro_id -- 28 --  
			AND b.tp_cobertura_id = @tp_cobertura_id -- 5 --  
			AND b.dt_fim_vigencia IS NULL

		IF @sinistro_parametro_chave_id IS NULL
		BEGIN
			SET @PgtoImediato = 0

			INSERT INTO #DETALHAMENTO (linha)
			SELECT 'O produto,ramo,evento e cobertura selecionados no aviso'

			INSERT INTO #DETALHAMENTO (linha)
			SELECT 'n�o est�o habilitados para o fluxo do pagamento imediato'

			INSERT INTO #DETALHAMENTO (linha)
			SELECT ''
		END
		ELSE
		BEGIN
			----------------------------------------------------------------------------------------------------------------------------------------------------- 
			--###################################################################################################################################################  
			--1� VALIDA��O DA VIG�NCIA COBERTURA E APOLICE 'verifica_vigencia_cobertura_apolice'  
			--Regra: VALIDA��O DE TELA: OCORRENCIA DENTRO DA VIG�NCIA DA (AP�LICE E COBERTURA)  
			--BUSCANDO A REGRA PARAMETRIZADA  
			/*  
		   SELECT REGRA.descricao,CHAVE.valor,CHAVE.operador,REGRA.tipo,*  FROM desenv_db.dbo.sinistro_parametro_chave_regra_tb CHAVE  
			INNER JOIN desenv_db.dbo.sinistro_parametro_regra_tb REGRA  
		   ON CHAVE.sinistro_parametro_regra_id = REGRA.sinistro_parametro_regra_id  
		   WHERE 1=1  
			 AND CHAVE.sinistro_parametro_chave_id = 96 --@sinistro_parametro_chave_id  
			 AND REGRA.NOME = 'verifica_vigencia_cobertura_apolice'  
  
		 */
			DECLARE @Dt_ini AS SMALLDATETIME
			DECLARE @Dt_fim AS SMALLDATETIME
			DECLARE @VALIDA_VIGENCIA_APOLICE_COBERTURA CHAR(1)

			SELECT @VALIDA_VIGENCIA_APOLICE_COBERTURA = CHAVE.VALOR
			FROM desenv_db.dbo.sinistro_parametro_chave_regra_tb CHAVE WITH (NOLOCK)
			INNER JOIN desenv_db.dbo.sinistro_parametro_regra_tb REGRA WITH (NOLOCK)
				ON CHAVE.sinistro_parametro_regra_id = REGRA.sinistro_parametro_regra_id
			WHERE CHAVE.sinistro_parametro_chave_id = @sinistro_parametro_chave_id --1079 --  
				AND REGRA.NOME = 'verifica_vigencia_cobertura_apolice'

			IF @VALIDA_VIGENCIA_APOLICE_COBERTURA = 'S'
			BEGIN
				DECLARE @tp_ramo_id INT

				SELECT @tp_ramo_id = TP_RAMO_ID
				FROM seguros_db.dbo.ramo_tb WITH (NOLOCK)
				WHERE ramo_id = @RAMO_ID --77

				--VALIDA��O DE TELA: OCORRENCIA DENTRO DA VIG�NCIA DA (AP�LICE E COBERTURA)  
				IF OBJECT_ID('TEMPDB..#vigencia') IS NOT NULL
				BEGIN
					DROP TABLE #vigencia
				END

				IF OBJECT_ID('TEMPDB..#COBERTURAS_VIGENTES') IS NOT NULL
				BEGIN
					DROP TABLE #COBERTURAS_VIGENTES
				END

				CREATE TABLE #VIGENCIA (
					id INT IDENTITY(1, 1) NOT NULL PRIMARY KEY
					,dt_inicio_vigencia SMALLDATETIME NULL
					,dt_fim_vigencia SMALLDATETIME NULL
					,vigencia CHAR(1) NULL --(A)police / (C)obertura  
					)

				CREATE TABLE #COBERTURAS_VIGENTES (
					tp_cobertura_id INT NULL
					,nome_COBERTURA VARCHAR(600) NULL
					,val_is NUMERIC(15, 2) NULL
					,dt_inicio_vigencia SMALLDATETIME NULL
					,dt_fim_vigencia SMALLDATETIME NULL
					,cod_obejeto_segurado INT NULL
					,texto_franquia VARCHAR(600) NULL
					)

				--AP�LICE  
				INSERT INTO #vigencia (
					dt_inicio_vigencia
					,dt_fim_vigencia
					,vigencia
					)
				SELECT ISNULL(APOLICE.dt_inicio_vigencia, ISNULL(ADESAO.dt_inicio_vigencia, FECHADA.dt_inicio_vig))
					,ISNULL(APOLICE.dt_fim_vigencia, ISNULL(ADESAO.dt_fim_vigencia, FECHADA.dt_fim_vig))
					,'A' --AP�LICE  
				FROM seguros_db.dbo.proposta_tb proposta WITH (NOLOCK)
				LEFT JOIN seguros_db.dbo.proposta_fechada_tb fechada WITH (NOLOCK)
					ON proposta.proposta_id = fechada.proposta_id
				LEFT JOIN seguros_db.dbo.proposta_adesao_tb adesao WITH (NOLOCK)
					ON proposta.proposta_id = adesao.proposta_id
				LEFT JOIN seguros_db.dbo.apolice_tb apolice WITH (NOLOCK)
					ON proposta.proposta_id = apolice.proposta_id
				WHERE proposta.proposta_id = 24194757 --@proposta_id --24730523--
					AND proposta.situacao = 'i'

				--VALIDANDO PROPOSTA CANCELADA
				--CASO A PROPOSTA ESTEJA CANCELADA NA DATA DE OCORRENCIA DO SINISTRO 
				--ATUALIZA A DATA DE FIM DE VIGENCIA DA APOLICE PARA A DATA DE CENCELAMENTO
				DECLARE @MAX_CANC_ENDOSSO_ID INT

				SELECT @MAX_CANC_ENDOSSO_ID = MAX(CANCELAMENTO.ENDOSSO_ID)
				FROM SEGUROS_DB.DBO.cancelamento_proposta_tb CANCELAMENTO WITH (NOLOCK)
				WHERE PROPOSTA_ID = 24194757 --@proposta_id

				SELECT @MAX_CANC_ENDOSSO_ID

				--PROPOSTA CANCELADA NA DATA DE OCORRENCIA DO SINISTRO
				UPDATE A
				SET dt_fim_vigencia = CANCELAMENTO.dt_inicio_cancelamento
				--SELECT *
				FROM #vigencia A
					,SEGUROS_DB.DBO.cancelamento_proposta_tb CANCELAMENTO
				WHERE 1 = 1
					AND CANCELAMENTO.PROPOSTA_ID = 24194757 --@PROPOSTA_ID
					AND A.vigencia = 'A'
					AND CANCELAMENTO.endosso_id = 1 --@MAX_CANC_ENDOSSO_ID
					AND dt_fim_cancelamento IS NULL --PROPOSTA CANCELADA
					AND GETDATE() --@DT_OCORRENCIA
					> CANCELAMENTO.dt_inicio_cancelamento

				--PROPOSTA DATA OCORRENCIA ANTES DA REATIVACAO DA PROPOSTA
				UPDATE A
				SET dt_fim_vigencia = CANCELAMENTO.dt_fim_cancelamento
				--SELECT *
				FROM #vigencia A
					,SEGUROS_DB.DBO.cancelamento_proposta_tb CANCELAMENTO
				WHERE 1 = 1
					AND CANCELAMENTO.PROPOSTA_ID = 24194757 --@PROPOSTA_ID
					AND A.vigencia = 'A'
					AND CANCELAMENTO.endosso_id = 1 --@MAX_CANC_ENDOSSO_ID
					AND dt_fim_cancelamento IS NOT NULL --PROPOSTA CANCELADA
					AND GETDATE() --@DT_OCORRENCIA
					> CANCELAMENTO.dt_fim_cancelamento

				--COBERTURAS DA PROPOSTA
				INSERT INTO #COBERTURAS_VIGENTES (
					tp_cobertura_id
					,nome_COBERTURA
					,val_is
					,dt_inicio_vigencia
					,dt_fim_vigencia
					,cod_obejeto_segurado
					,texto_franquia
					)
				--VALIDAR SE A PROCEDURE ABAXO RETORNA AS COBERTURAS DOS PRODUTOS BESC
				EXEC seguros_db.dbo.consultar_coberturas_sps '2020-02-28' --@dt_ocorrencia
					,24194757 --@PROPOSTA_ID --24730523
					,1 --@tp_componente_id --1              
					,NULL -- @sub_grupo_id            
					,1 --@tp_ramo_id --1-

				INSERT INTO #vigencia (
					dt_inicio_vigencia
					,dt_fim_vigencia
					,vigencia
					)
				SELECT dt_inicio_vigencia
					,isnull(dt_fim_vigencia, (
							SELECT dt_fim_vigencia
							FROM #vigencia
							WHERE vigencia = 'A'
							))
					,'C' --COBERTURA    
				FROM #COBERTURAS_VIGENTES
				WHERE 1 = 1
					AND tp_cobertura_id = @tp_cobertura_id --5   

				--OCORRENCIA VIG�NCIA AP�LICE  
				IF NOT EXISTS (
						SELECT TOP 1 1
						FROM #vigencia v
						WHERE v.vigencia = 'A'
							AND @dt_ocorrencia BETWEEN v.dt_inicio_vigencia
								AND isnull(v.dt_fim_vigencia, getdate() + 1) --CASO A PROPOSTA NAO ESTEJA CANCELADA O FIM DE VIGENCIA PODE SER NULL
						)
					OR NOT EXISTS (
						SELECT TOP 1 1
						FROM #vigencia v
						WHERE v.vigencia = 'A'
						) --PROPOSTA CANCELADA SITUACAO C NA PROPOSTA TB
				BEGIN
					--RECUSA (SEM VIG�NCIA)  
					SELECT @Dt_ini = v.dt_inicio_vigencia
						,@Dt_fim = v.dt_fim_vigencia
					FROM #vigencia v
					WHERE v.vigencia = 'A'

					SET @PgtoImediato = 0

					--GRAVA��O DETALHAMENTO
					INSERT INTO #DETALHAMENTO (linha)
					SELECT 'Pagamento imediato negado:'

					INSERT INTO #DETALHAMENTO (linha)
					SELECT 'A data de ocorrencia ' + FORMAT(@dt_ocorrencia, 'dd/MM/yyyy') + ' do aviso'

					INSERT INTO #DETALHAMENTO (linha)
					SELECT 'n�o esta dentro do per�odo de vig�ncia da ap�lice.'

					INSERT INTO #DETALHAMENTO (linha)
					SELECT convert(VARCHAR(20), @dt_ini, 103) + ' � ' + convert(VARCHAR(20), @dt_fim, 103)

					INSERT INTO #DETALHAMENTO (linha)
					SELECT ''
				END
				ELSE
				BEGIN
					--GRAVA��O DETALHAMENTO
					INSERT INTO #DETALHAMENTO (linha)
					SELECT 'A data de ocorrencia do aviso'

					INSERT INTO #DETALHAMENTO (linha)
					SELECT 'esta dentro do per�odo de vig�ncia da ap�lice.'

					INSERT INTO #DETALHAMENTO (linha)
					SELECT ''
				END

				--OCORRENCIA VIG�NCIA COBERTURA    
				IF NOT EXISTS (
						SELECT TOP 1 1
						FROM #vigencia v
						WHERE v.vigencia = 'C'
							AND @dt_ocorrencia BETWEEN v.dt_inicio_vigencia
								AND isnull(v.dt_fim_vigencia, getdate() + 1)
						)
				BEGIN
					--RECUSA (SEM VIG�NCIA)    
					SELECT @Dt_ini = v.dt_inicio_vigencia
						,@Dt_fim = v.dt_fim_vigencia
					FROM #vigencia v
					WHERE v.vigencia = 'C'

					SET @PgtoImediato = 0

					--GRAVA��O DETALHAMENTO
					INSERT INTO #DETALHAMENTO (linha)
					SELECT 'Pagamento imediato negado:'

					INSERT INTO #DETALHAMENTO (linha)
					SELECT 'A data de ocorrencia ' + FORMAT(@dt_ocorrencia, 'dd/MM/yyyy') + ' do aviso'

					INSERT INTO #DETALHAMENTO (linha)
					SELECT 'n�o esta dentro do per�odo de vig�ncia da cobertura.'

					IF @Dt_ini IS NOT NULL
						AND @Dt_fim IS NOT NULL
					BEGIN
						INSERT INTO #DETALHAMENTO (linha)
						SELECT FORMAT(@dt_ini, 'dd/MM/yyyy') + ' � ' + FORMAT(@dt_fim, 'dd/MM/yyyy')
					END

					INSERT INTO #DETALHAMENTO (linha)
					SELECT ''
				END
				ELSE
				BEGIN
					INSERT INTO #DETALHAMENTO (linha)
					SELECT 'A data de ocorrencia do aviso ' + FORMAT(@dt_ocorrencia, 'dd/MM/yyyy')

					INSERT INTO #DETALHAMENTO (linha)
					SELECT 'esta dentro do per�odo de vig�ncia da cobertura.'

					INSERT INTO #DETALHAMENTO (linha)
					SELECT ''
				END
			END

			-----------------------------------------------------------------------------------------------------------------------------------------------------  
			--###################################################################################################################################################  
			--FIM DA CLASSIFICA��O  
			-----------------------------------------------------------------------------------------------------------------------------------------------------  
			IF @PgtoImediato = 1
			BEGIN
				--ATUALIZA��O DA CLASSIFICA��O
				UPDATE A
				SET LINHA = 'O SINISTRO FOI CLASSIFICADO COMO PAGAMENTO IMEDIATO'
				FROM #DETALHAMENTO A
				WHERE ID = 6
			END
			ELSE
			BEGIN
				--ATUALIZA��O DA CLASSIFICA��O
				UPDATE A
				SET LINHA = 'O SINISTRO N�O FOI CLASSIFICADO COMO PAGAMENTO IMEDIATO'
				FROM #DETALHAMENTO A
				WHERE ID = 6
			END

			--FINALIZANDO A CRIACAO DO DETALHAMENTO
			INSERT INTO #DETALHAMENTO (linha)
			SELECT ''

			INSERT INTO #DETALHAMENTO (linha)
			SELECT '--------------------------'
		END

		DECLARE @MAX_SEQ_DETALHE INT
			,@MAX_LINHA_DETALHE INT
			,@TP_DETALHAMENTO INT

		--' TP_DETALHAMENTO:
		--' 0- Anota��o
		--' 1- Exig�ncia
		--' 2- Recibo
		--' 3- Indeferimento "A Exig�ncia dever� ser emitida Manualmente no SEGUR."
		--' 4- Solicita��o de cancelamento
		--' 5- Comunica��es feitas pelo GTR
		SET @TP_DETALHAMENTO = 0

		--INICIANDO A GRAVA��O DO DETALHAMENTO
		IF @debug = 0
		BEGIN
			IF @evento_id IS NOT NULL
			BEGIN
				--PRODUTOS AVISADOS PELO SEGP0794 GRAVAM NAS TABELAS DE EVENTO DO SINISTRO 
				SELECT @MAX_SEQ_DETALHE = MAX(seq_detalhe)
				FROM SEGUROS_DB.DBO.evento_SEGBR_sinistro_detalhe_atual_tb WITH (NOLOCK)
				WHERE EVENTO_ID = @evento_id

				--GRAVANDO O DETALHAMENTO
				INSERT INTO SEGUROS_DB.DBO.evento_SEGBR_sinistro_detalhe_atual_tb (
					evento_id
					,tp_detalhe
					,seq_detalhe
					,descricao
					,usuario
					,dt_inclusao
					)
				SELECT @evento_id AS evento_id
					,'ANOTACAO' AS tp_detalhe
					,ISNULL(@MAX_SEQ_DETALHE, 0) + ID AS seq_detalhe
					,LINHA AS descricao
					,@usuario AS usuario
					,GETDATE() AS dt_inclusao
				FROM #DETALHAMENTO WITH (NOLOCK)
			END
			ELSE
			BEGIN
				IF @SINISTRO_ID IS NOT NULL
				BEGIN
					--PRODUTOS AVISADOS PELO SEGP1296 GRAVAM DIRETO NAS TABELAS DE SINISTRO
					--GRAVANDO DIRETO NO DETALHAMENTO
					DECLARE @APOLICE_ID INT
						,@SUCURSAL_ID INT
						,@SEGURADORA_ID INT
						,@DETALHAMENTO_ID INT
						,@DT_SISTEMA SMALLDATETIME

					SELECT @APOLICE_ID = APOLICE_ID
						,@SUCURSAL_ID = sucursal_seguradora_id
						,@SEGURADORA_ID = seguradora_cod_susep
					FROM SEGUROS_DB.DBO.SINISTRO_TB WITH (NOLOCK)
					WHERE SINISTRO_ID = @SINISTRO_ID --82202000021

					SELECT @DT_SISTEMA = DT_OPERACIONAL
					FROM SEGUROS_DB.DBO.PARAMETRO_GERAL_TB WITH (NOLOCK)

					--GERA��O DO DETALHAMENTO ID
					EXEC @detalhamento_id = SEGUROS_DB.DBO.SINISTRO_DETALHAMENTO_SPI @SINISTRO_ID
						,@APOLICE_ID
						,@SUCURSAL_ID
						,@SEGURADORA_ID
						,@RAMO_ID
						,@DT_SISTEMA
						,@TP_DETALHAMENTO
						,'S'
						,@USUARIO

					SELECT @MAX_LINHA_DETALHE = MAX(LINHA_ID)
					FROM SEGUROS_DB.DBO.SINISTRO_LINHA_DETALHAMENTO_TB WITH (NOLOCK)
					WHERE SINISTRO_ID = @SINISTRO_ID -- 82202000020
						AND DETALHAMENTO_ID = @DETALHAMENTO_ID

					--GRAVANDO O DETALHAMENTO (sinistro_linha_detalhe_spi)
					INSERT INTO SEGUROS_DB.DBO.SINISTRO_LINHA_DETALHAMENTO_TB (
						SINISTRO_ID
						,APOLICE_ID
						,SUCURSAL_SEGURADORA_ID
						,SEGURADORA_COD_SUSEP
						,RAMO_ID
						,DETALHAMENTO_ID
						,LINHA_ID
						,LINHA
						,DT_INCLUSAO
						,USUARIO
						)
					SELECT @SINISTRO_ID
						,@APOLICE_ID
						,@SUCURSAL_ID
						,@SEGURADORA_ID
						,@RAMO_ID
						,@DETALHAMENTO_ID
						,ISNULL(@MAX_LINHA_DETALHE, 0) + ID
						,LINHA
						,GETDATE()
						,@USUARIO
					FROM #DETALHAMENTO WITH (NOLOCK)
				END
			END

			--RETORNO DA CLASSIFICA��O
			SELECT @PgtoImediato AS PagtoImediato
				,@tp_sinistro_parametro_id AS tp_sinistro_parametro_id
		END
		ELSE
		BEGIN
			SELECT @PGTOIMEDIATO AS PAGTOIMEDIATO
				,@TP_SINISTRO_PARAMETRO_ID AS TP_SINISTRO_PARAMETRO_ID
				,@EVENTO_ID AS EVENTO_ID
				,@SINISTRO_ID AS SINISTRO_ID
				,'ANOTACAO' AS TP_DETALHE
				,@TP_DETALHAMENTO AS TP_DETALHAMENTO
				,ISNULL(@MAX_SEQ_DETALHE, 0) + ID AS SEQ_DETALHE
				,LINHA AS DESCRICAO
				,@USUARIO AS USUARIO
				,GETDATE() AS DT_INCLUSAO
			FROM #DETALHAMENTO WITH (NOLOCK)
		END

		-- (fim) Bloco de codifica��o da procedure  
		-----------      
		RETURN
	END TRY

	BEGIN CATCH
		DECLARE @ErrorMessage NVARCHAR(4000)
		DECLARE @ErrorSeverity INT
		DECLARE @ErrorState INT

		SELECT @ErrorMessage = ERROR_PROCEDURE() + ' - Linha ' + CONVERT(VARCHAR(15), ERROR_LINE()) + ' - ' + ERROR_MESSAGE()
			,@ErrorSeverity = ERROR_SEVERITY()
			,@ErrorState = ERROR_STATE()

		RAISERROR (
				@ErrorMessage
				,@ErrorSeverity
				,@ErrorState
				)
	END CATCH
END
GO


