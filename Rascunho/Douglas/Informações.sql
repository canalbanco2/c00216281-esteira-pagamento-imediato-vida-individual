--Deferimento Imediato Vida Individual
/*

Incluir uma etapa no processo de aviso de sinistro, para identificar se o CPF sinistrado atender� todas as regras de elegibilidade definidas,
a fim de disponibilizar uma mensagem informativa em tela, indicando que o sinistro j� possui deferimento aceito no momento do aviso de sinistro,
ficando pendente o envio de documenta��o para libera��o do pagamento. Este projeto visa agilizar o pagamento de indeniza��o para sinistros que
atendam determinados crit�rios, com o objetivo de desonerar o processo de an�lise e aumentar o grau de satisfa��o do cliente.

	1183 - BB Seguro Vida Prestamista Ex�rcito
	136 - OUROVIDA GRUPO ESPECIAL
	200 - BESC SEGURO F�CIL
	201 - BESC CLUBE INDIVIDUAL VG - APL. 495
	202 - BESC CLUBE INDIVIDUAL VG - APL. 561
	203 - BESC CLUBE INDIVIDUAL PLUS
	204 - BESC INDIVIDUAL SENIOR
	205 - BESC INDIVIDUAL VIDA PREMIADA
	206 - BESC JUNIOR VG
	207 - BESC NOBRE VG
	208 - BESC INDIVIDUAL SERVIDOR VIDA
	209 - BESC INDIVIDUAL FUNC PUBLICOS ESTADUAIS
	210 - NOVOS FUNCIONARIOS BESC - APL. 488
	211 - BESC FUNCION�RIOS
	212 - CONTA GARANTIDA TRIMESTRAL - CGT
	213 - BESC CLUBE INDIVIDUAL APC - APL. 001
	214 - BESC JUNIOR APC
	215 - BESC NOBRE APC
	217 - BESC APC MAIS
	218 - BESC AP PREMIADO
	219 - BESC SEGURO 24 HORAS
	224 - BESC CLUBE INDIVIDUAL VG - APL. 473
	225 - BESC CLUBE INDIVIDUAL APC - APL. 029

	Morte Natural - (Evento de Morte Natural) 

	REGRAS:

	Sinistro com registro de Evento de Morte Natural 
	Sinistro Passiveis de Aviso � Considera este, onde a data de ocorr�ncia seja menor ou igual a data de fim de vig�ncia ou cancelamento da proposta, ou seja, quando a proposta sinistrada, estiver dentro das condi��es aceitas.
	Que a proposta sinistrada seja de um dos produtos listados no item 2. Produtos Afetados.

*/
SELECT TOP 2 *
FROM seguros_db.dbo.proposta_tb a
inner join produto_tb b
on a.produto_id = b.produto_id
WHERE a.produto_id = 1183
	AND situacao = 'i'

SELECT TOP 1 *
FROM seguros_db.dbo.proposta_tb
WHERE produto_id = 136
	AND situacao = 'i'

SELECT TOP 1 ramo_id,*
FROM seguros_db.dbo.proposta_tb
WHERE produto_id = 200
	AND situacao = 'i'
	ORDER BY DT_INCLUSAO DESC

SELECT TOP 1 ramo_id,*
FROM seguros_db.dbo.proposta_tb
WHERE produto_id = 201
	AND situacao = 'i'

SELECT TOP 1 ramo_id,*
FROM seguros_db.dbo.proposta_tb
WHERE produto_id = 202
	AND situacao = 'i'

SELECT TOP 1 ramo_id,*
FROM seguros_db.dbo.proposta_tb
WHERE produto_id = 203
	AND situacao = 'i'

SELECT TOP 1 ramo_id,*
FROM seguros_db.dbo.proposta_tb
WHERE produto_id = 204
	AND situacao = 'i'

SELECT TOP 1 ramo_id,*
FROM seguros_db.dbo.proposta_tb
WHERE produto_id = 205
	AND situacao = 'i'

SELECT TOP 1 ramo_id,*
FROM seguros_db.dbo.proposta_tb
WHERE produto_id = 206
	AND situacao = 'i'

SELECT TOP 1 ramo_id,*
FROM seguros_db.dbo.proposta_tb
WHERE produto_id = 207
	AND situacao = 'i'

SELECT TOP 1 ramo_id,*
FROM seguros_db.dbo.proposta_tb
WHERE produto_id = 208
	AND situacao = 'i'

SELECT TOP 1 ramo_id,*
FROM seguros_db.dbo.proposta_tb
WHERE produto_id = 209
	AND situacao = 'i'

SELECT TOP 1 ramo_id,*
FROM seguros_db.dbo.proposta_tb
WHERE produto_id = 210
	AND situacao = 'i'

SELECT TOP 1 ramo_id,*
FROM seguros_db.dbo.proposta_tb
WHERE produto_id = 211
	AND situacao = 'i'

SELECT TOP 1 ramo_id,*
FROM seguros_db.dbo.proposta_tb
WHERE produto_id = 212
	AND situacao = 'i'

SELECT TOP 1 ramo_id,*
FROM seguros_db.dbo.proposta_tb
WHERE produto_id = 213
	AND situacao = 'i'

--select top 1 * from seguros_db.dbo.proposta_tb where produto_id = 214 	and situacao ='i'
SELECT TOP 1 ramo_id,*
FROM seguros_db.dbo.proposta_tb
WHERE produto_id = 215
	AND situacao = 'i'

SELECT TOP 1 ramo_id,*
FROM seguros_db.dbo.proposta_tb
WHERE produto_id = 217
	AND situacao = 'i'

SELECT TOP 1 ramo_id,*
FROM seguros_db.dbo.proposta_tb
WHERE produto_id = 218
	AND situacao = 'i'

SELECT TOP 1 ramo_id,*
FROM seguros_db.dbo.proposta_tb
WHERE produto_id = 219
	AND situacao = 'i'

SELECT TOP 1 ramo_id,*
FROM seguros_db.dbo.proposta_tb
WHERE produto_id = 224
	AND situacao = 'i'

SELECT TOP 1 ramo_id,*
FROM seguros_db.dbo.proposta_tb
WHERE produto_id = 225
	AND situacao = 'i'

--24730523	1183	2010-11-05 00:00:00	2010-11-03 00:00:00	12388508	i	n	NULL	2010-11-06 03:09:00	2010-11-13 12:29:00	26916069880	0x00000001E7F16CCF	NULL	NULL	2010-12-01 00:00:00	7719	NULL	NULL	N	15	77	7719	2010-10-07 00:00:00	NULL	NULL	NULL	NULL	2	NULL	0	NULL	NULL	NULL	1	NULL	NULL	NULL	NULL	NULL	NULL	NULL	NULL	NULL	NULL	NULL	NULL	NULL	NULL	NULL	NULL	NULL
--24730516	1183	2010-11-05 00:00:00	2010-11-03 00:00:00	2997823	i	n	NULL	2010-11-06 03:09:00	2010-11-13 12:29:00	26916069880	0x00000001E7F20F6C	NULL	NULL	2010-12-01 00:00:00	7719	NULL	NULL	N	15	77	7719	2010-10-07 00:00:00	NULL	NULL	NULL	NULL	2	NULL	0	NULL	NULL	NULL	1	NULL	NULL	NULL	NULL	NULL	NULL	NULL	NULL	NULL	NULL	NULL	NULL	NULL	NULL	NULL	NULL	NULL
--24195974	200		2010-10-11 00:00:00	2006-06-06 00:00:00	12120882	i	n	NULL	2010-10-09 20:28:00	2010-10-09 20:30:00	FlowBR3658050	0x00000001CECAD95F	NULL	0	1900-01-01 00:00:00	NULL	NULL	NULL	N	NULL	93	9373	2010-03-30 00:00:00	NULL	NULL	NULL	NULL	1	NULL	NULL	NULL	NULL	NULL	1	NULL	NULL	NULL	NULL	NULL	NULL	NULL	NULL	NULL	NULL	NULL	NULL	NULL	NULL	NULL	NULL	NULL
--24370465	225	2010-10-13 00:00:00	2005-06-10 00:00:00	12245182	i	n	NULL	2010-10-13 17:18:00	2018-05-10 19:25:00	FNC_MBT	0x0000001283DE3A6D	NULL	0	1900-01-01 00:00:00	NULL	NULL	NULL	N	NULL	82	8237	2010-09-29 00:00:00	NULL	NULL	NULL	NULL	1	NULL	NULL	NULL	NULL	NULL	1	NULL	NULL	NULL	NULL	NULL	NULL	NULL	NULL	NULL	NULL	NULL	NULL	NULL	NULL	NULL	NULL	NULL
--24152980	201	2010-10-11 00:00:00	2006-01-08 00:00:00	12080491	i	n	NULL	2010-10-09 20:20:00	2010-10-09 20:21:00	FlowBR3658050	0x00000001CE92DB6C	NULL	0	1900-01-01 00:00:00	NULL	NULL	NULL	N	NULL	93	9374	2010-03-31 00:00:00	NULL	NULL	NULL	NULL	1	NULL	NULL	NULL	NULL	NULL	1	NULL	NULL	NULL	NULL	NULL	NULL	NULL	NULL	NULL	NULL	NULL	NULL	NULL	NULL	NULL	NULL	NULL
--82	24370430	225	2010-10-13 00:00:00	2005-05-06 00:00:00	12245100	i	n	NULL	2010-10-13 17:18:00	2010-10-13 17:18:00	FLOWBR3658050	0x00000001D1B611B9	NULL	0	1900-01-01 00:00:00	NULL	NULL	NULL	N	NULL	82	8237	2010-09-29 00:00:00	NULL	NULL	NULL	NULL	1	NULL	NULL	NULL	NULL	NULL	1	NULL	NULL	NULL	NULL	NULL	NULL	NULL	NULL	NULL	NULL	NULL	NULL	NULL	NULL	NULL	NULL	NULL
--93	24217980	200	2010-10-11 00:00:00	2007-08-10 00:00:00	12119997	i	n	NULL	2010-10-09 20:28:00	2010-10-09 20:30:00	FlowBR3658050	0x00000001CECB2FAB	NULL	0	1900-01-01 00:00:00	NULL	NULL	NULL	N	NULL	93	9373	2010-03-30 00:00:00	NULL	NULL	NULL	NULL	1	NULL	NULL	NULL	NULL	NULL	1	NULL	NULL	NULL	NULL	NULL	NULL	NULL	NULL	NULL	NULL	NULL	NULL	NULL	NULL	NULL	NULL	NULL

SELECT a.sinistro_id
	,B.*
FROM SEGUROS_DB.DBO.EVENTO_SEGBR_SINISTRO_ATUAL_TB A WITH (NOLOCK)
INNER JOIN SEGUROS_DB.DBO.EVENTO_SEGBR_SINISTRO_DETALHE_ATUAL_TB B WITH (NOLOCK)
	ON A.EVENTO_ID = B.EVENTO_ID
WHERE a.produto_id = 1183
	AND sinistro_id = 77201201158

SELECT A.PROPOSTA_ID
	,A.evento_bb_id
	,b.descricao
	,A.cpf_cgc_segurado
	,A.cpf_sinistrado
	,*
FROM seguros_db.dbo.evento_SEGBR_sinistro_atual_tb a
INNER JOIN seguros_db.dbo.evento_SEGBR_tb b
	ON a.evento_segbr_id = b.evento_segbr_id
WHERE sinistro_id = 77201201158
ORDER BY a.dt_inclusao

--and a.dt_inclusao > '2020-01-01'
EXEC consultar_coberturas_sps '20200214' --@dt_ocorrencia                     
	,24730516 --@proposta_id                    
	,1 --@tp_componente_id                    
	,NULL --@sub_grupo_id             
	,1 --@tipo_ramo  
	
	
	--SEGS9411_SPS

--SEGP0794
--EVENTOS 
SELECT DISTINCT es.evento_sinistro_id
	,es.nome
FROM evento_sinistro_ramo_tb esr WITH (NOLOCK)
	,ramo_tb r WITH (NOLOCK)
	,evento_sinistro_tb es WITH (NOLOCK)
	,produto_estimativa_sinistro_tb pes WITH (NOLOCK)
WHERE esr.ramo_id = r.ramo_id
	AND esr.evento_sinistro_id = es.evento_sinistro_id
	AND pes.evento_sinistro_id = esr.evento_sinistro_id
	AND r.tp_ramo_id = 1
	AND pes.situacao = 'A'
ORDER BY es.nome

--###############  BTN ATIVAR PROPOSTA PARA AVISAR ###############
-- ExisteEventoSinistroProduto
SELECT *
FROM produto_estimativa_sinistro_tb WITH (NOLOCK)
WHERE 1 = 1
	AND produto_id = 1183
	AND evento_sinistro_id = 28

--VerificaPropostaBB
EXEC seguros_db.dbo.obtem_Versao_Endosso_sps 2561610
	,'20200212'

--ExisteProdutoBB
SELECT produto_bb
FROM produto_bb_sinistro_tb b WITH (NOLOCK)
INNER JOIN proposta_tb a WITH (NOLOCK)
	ON a.proposta_id = 24730523
WHERE b.produto_id = a.produto_id
	AND b.dt_inicio_vigencia <= a.dt_contratacao
	AND (
		dt_fim_vigencia >= a.dt_contratacao
		OR dt_fim_vigencia IS NULL
		)

UNION

SELECT PRODUTO_ID
FROM PROPOSTA_TB WITH (NOLOCK)
WHERE PROPOSTA_ID = 24730523
	AND ORIGEM_PROPOSTA_ID = 2

--Selecionar os dados de produtos do ALS
EXEC SEGUROS_DB.DBO.obtem_produto_sps 1
	,1183

--VerificaReanalise
--se existe aviso, verifica se est� aberto
EXEC seguros_db.dbo.verifica_reanalise_sps 24730523
	,28
	,'20200212'
	,'04416607890'
	,'19551010'

------------------------------------------------------------------------------------------------------
--###############  BTN AVISAR ###############
--ObtemDadosSinistros
--Obtem_Evento_SEGBR
SELECT evento_SEGBR_id
	,prox_localizacao_processo
FROM SEGUROS_DB.DBO.evento_segbr_tb WITH (NOLOCK)
WHERE evento_bb_id = 1100

--Busca_Numero_Remessa
SET NOCOUNT ON

EXEC seguros_db.DBO.chave_spu 'NUMERO REMESSA MQ'
	,'MQ_USER'
	,'0'

--
SELECT produto_bb
FROM produto_bb_sinistro_tb b WITH (NOLOCK)
INNER JOIN proposta_tb a WITH (NOLOCK)
	ON a.proposta_id = 24730523
WHERE b.produto_id = a.produto_id
	AND b.dt_inicio_vigencia <= a.dt_contratacao
	AND (
		dt_fim_vigencia >= a.dt_contratacao
		OR dt_fim_vigencia IS NULL
		)

UNION

SELECT PRODUTO_ID
FROM PROPOSTA_TB WITH (NOLOCK)
WHERE PROPOSTA_ID = 24730523
	AND ORIGEM_PROPOSTA_ID = 2

EXEC seguros_db.dbo.obtem_produto_sps 1
	,1183

--Busca_Numero_Sinistro
EXEC seguros_db..SEGS8151_SPS '77'
	,0
	,6785
	,'C00216281'

--77202000005 --sinistro_id
--'--------------------------------------------------------------
--'Obten��o de dados da cobertura
--'--------------------------------------------------------------
--ObtemCoberturas
EXEC seguros_db.dbo.consultar_coberturas_sps '20200212'
	,24730523
	,1
	,NULL
	,1

--getEstimativa
SELECT a.ramo_id
	,b.subramo_id
	,b.produto_id
FROM apolice_tb a WITH (NOLOCK)
	,proposta_tb b WITH (NOLOCK)
WHERE a.proposta_id = b.proposta_id
	AND b.proposta_id = 24730523

UNION

SELECT a.ramo_id
	,b.subramo_id
	,b.produto_id
FROM proposta_adesao_tb a WITH (NOLOCK)
	,proposta_tb b WITH (NOLOCK)
WHERE a.proposta_id = b.proposta_id
	AND b.proposta_id = 24730523

--VALIDA SE A COBERTURA FOI ATINGIDA -- getEstimativa(proposta_id,tp_cobertura_id,Causa_id,SubCausa_id,SubCausa_id,dt_ocorrencia)
SELECT val_inicial
	,perc_estimativa
	,isnull(utiliza_percentual_subevento, 'N') utiliza_percentual_subevento
	,nome
	,a.cobertura_bb
FROM seguros_db.dbo.produto_estimativa_sinistro_tb a WITH (NOLOCK)
	,tp_cobertura_tb b WITH (NOLOCK)
WHERE produto_id = 1183
	AND a.tp_cobertura_id = b.tp_cobertura_id
	AND ramo_id = 77
	AND subramo_id = 7719
	AND tp_componente_id = 1
	AND evento_sinistro_id = 28
	AND a.tp_cobertura_id = 5
	AND convert(CHAR(8), dt_inicio_vigencia, 112) <= '20200212'
	AND (
		convert(CHAR(8), dt_fim_vigencia, 112) >= '20200212'
		OR dt_fim_vigencia IS NULL
		)
	AND a.situacao = 'A'

--ObtemEstimativaAbertura
SELECT a.ramo_id
	,b.subramo_id
	,b.produto_id
FROM apolice_tb a WITH (NOLOCK)
	,proposta_tb b WITH (NOLOCK)
WHERE a.proposta_id = b.proposta_id
	AND b.proposta_id = 24730523

UNION

SELECT a.ramo_id
	,b.subramo_id
	,b.produto_id
FROM proposta_adesao_tb a WITH (NOLOCK)
	,proposta_tb b WITH (NOLOCK)
WHERE a.proposta_id = b.proposta_id
	AND b.proposta_id = 24730523

--SEGL0144 --ObtemEstimativaAberturaVida
--ObtemVaMinimoEstimativa
SELECT val_parametro
FROM SEGUROS_DB.DBO.ps_parametro_tb a
WHERE parametro = 'VLR MINIMO ESTIMATIVA'

--GravaSinistros
EXEC seguros_db.dbo.obtem_Versao_Endosso_sps 2561610
	,'20200212'

SET NOCOUNT ON

EXEC evento_SEGBR_sinistro_CA_spi 77202000005
	,31121
	,6785
	,0
	,77
	,10142
	,1100
	,'20200212'
	,12
	,'1'
	,'C00216281'
	,1183
	,24730523
	,2561610
	,635
	,12388508
	,'CRISTINA MARGARETE FENERICH SANTIAGO                        '
	,'04416607890'
	,'F'
	,'19551010'
	,'CRISTINA MARGARETE FENERICH SANTIAGO                        '
	,'04416607890'
	,'F'
	,'19551010'
	,'A'
	,'RUA OMAR BAPTISTA DE OLIVEIRA'
	,'MILHO BRANCO'
	,'36083180'
	,'JUIZ DE FORA'
	,666
	,'MG'
	,18
	,11
	,NULL
	,NULL
	,NULL
	,NULL
	,111111111
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,1
	,NULL
	,NULL
	,NULL
	,NULL
	,''
	,'20200212'
	,28
	,'MORTE NATURAL'
	,'N'
	,'N'
	,1
	,'0000000023801976'
	,'0001'
	,'N'
	,'3'
	,'T'
	,'N'
	,790
	,NULL
	,NULL
	,NULL
	,NULL
	,'0000'
	,'A'
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,952
	,39
	,7719
	,2561610
	,1
	,26103.11
	,0
	,NULL
	,NULL
	,NULL
	,NULL
	,NULL
	,N
	,NULL
	,12345678909

SELECT *
FROM EVENTO_SEGBR_SINISTRO_ATUAL_TB WITH (NOLOCK)
WHERE SINISTRO_ID = 77202000005

--GravarTabelao() --obs detalhamento
--'Grava na evento_segbr_sinistro_detalhe_tb
SELECT sinistro_bb
FROM sinistro_bb_tb WITH (NOLOCK)
WHERE sinistro_id = 77202000005
	AND encerrado = 'n'

--BuscaNome
SELECT nome
FROM segab_db.dbo.usuario_tb WITH (NOLOCK)
WHERE cpf = ''

--
SELECT *
FROM seguros_db.dbo.evento_segbr_sinistro_documento_tb WITH (NOLOCK)
WHERE EVENTO_ID = 45448763

--temSinistrosAnteriores
EXEC seguros_db.dbo.sinistros_anteriores_sps '04416607890'
	,1
	,'12388508'

EXEC evento_SEGBR_sinistro_detalhe_spi 45448763
	,'ANOTACAO'
	,1
	,'AVISO DE SINISTRO'
	,'C00216281'

--GravarDescricaoOcorrencia
--Grava na evento_segbr_sinistro_descricao_tb
EXEC evento_SEGBR_sinistro_descricao_spi 45448763
	,1
	,'A'
	,'C00216281'

--GravarCoberturas
--'Grava na evento_segbr_sinistro_cobertura_tb
EXEC evento_SEGBR_sinistro_cobertura_spi 45448763
	,5
	,'50'
	,'C00216281'
	,0
	,''
	,'50'

--IncluirDocumentos
--SEGL0144  ObterDocumentos()
SELECT DISTINCT copia = isnull(tp_copia_tb.nome, '-')
	,documento = d.descricao
	,LOCAL = isnull(origem_documento_sinistro_tb.nome, '-')
	,d.documento_id
FROM seguros_db..documento_aviso_sinistro_tb pds
INNER JOIN documento_tb d
	ON pds.documento_id = d.documento_id
LEFT JOIN tp_copia_tb
	ON tp_copia_tb.tp_copia = pds.tp_copia
LEFT JOIN origem_documento_sinistro_tb
	ON origem_documento_sinistro_tb.origem_documento_sinistro_id = d.origem_documento_sinistro_id
WHERE pds.produto_id = 1183
	AND pds.ramo_id = 77
	AND pds.subramo_id = 7719
	AND pds.tp_ramo = 1
	AND pds.evento_sinistro_id = 28
ORDER BY d.documento_id

--GravaGTREmissaoLayout
EXEC interface_db.DBO.gtr_emissao_layout_CA_spi 45448763
	,10142
	,'C00216281'

--COMMIT GRAVA��O DO SINISTRO
--rsConsultarQuestionario
EXEC SEGUROS_DB.DBO.SEGS9411_SPS 1183
	,28

--EXIBIR DOCUMENTOS
--CarregaCabecalho()
--CarregaGrid()
CREATE TABLE #temp_documento (
	tp_copia VARCHAR(60)
	,descricao VARCHAR(1000)
	,LOCAL VARCHAR(1000)
	)

SELECT evento_id
FROM SEGUROS_DB.DBO.evento_segbr_sinistro_atual_tb WITH (NOLOCK)
WHERE sinistro_id = 77202000005
	AND evento_bb_id = 1100
	AND proposta_id = 24730523
	AND ramo_id = 77

SELECT *
FROM SEGUROS_DB.DBO.evento_segbr_sinistro_documento_tb essd WITH (NOLOCK)
WHERE essd.evento_id = 45448763

--CarregaDocumentos()
SELECT DISTINCT isnull(tc.nome, '-')
	,d.descricao
	,isnull(od.nome, '-')
FROM seguros_db..documento_aviso_sinistro_tb pds WITH (NOLOCK)
INNER JOIN documento_tb d WITH (NOLOCK)
	ON pds.documento_id = d.documento_id
LEFT JOIN origem_documento_sinistro_tb od WITH (NOLOCK)
	ON d.origem_documento_sinistro_id = od.origem_documento_sinistro_id
LEFT JOIN tp_copia_tb tc WITH (NOLOCK)
	ON pds.tp_copia = tc.tp_copia
WHERE pds.produto_id = 1183
	AND pds.ramo_id = 77
	AND pds.evento_sinistro_id = 28

INSERT INTO #temp_documento (
	tp_copia
	,descricao
	,LOCAL
	)
SELECT DISTINCT isnull(tc.nome, '-')
	,d.descricao
	,isnull(od.nome, '-')
FROM seguros_db..documento_aviso_sinistro_tb pds WITH (NOLOCK)
INNER JOIN documento_tb d WITH (NOLOCK)
	ON pds.documento_id = d.documento_id
LEFT JOIN origem_documento_sinistro_tb od WITH (NOLOCK)
	ON d.origem_documento_sinistro_id = od.origem_documento_sinistro_id
LEFT JOIN tp_copia_tb tc WITH (NOLOCK)
	ON pds.tp_copia = tc.tp_copia
WHERE pds.produto_id = 1183
	AND pds.ramo_id = 77
	AND pds.evento_sinistro_id = 28

SET NOCOUNT ON

SELECT 'Quantidade' = count(*)
	,'C�pia' = tp_copia
	,'Documento' = descricao
	,'Local' = LOCAL
FROM #temp_documento
GROUP BY tp_copia
	,descricao
	,LOCAL
ORDER BY descricao

DROP TABLE #temp_documento

--SEGP0794
--SEGP0794_7 - (FORM DE AVISO DO SINISTRO)
--SEGP0794_fim.FRM - (FORM EXIBI��O DO PROCESSAMENTO) - INCLUS�O DA SITUA��O
--
SELECT a.tp_cobertura_id
	,evento_id
	,*
FROM seguros_db.dbo.evento_segbr_sinistro_cobertura_tb a WITH (NOLOCK)
--WHERE a.tp_cobertura_id = 5
WHERE EXISTS (
		SELECT 1
		FROM seguros_db.dbo.evento_segbr_sinistro_cobertura_tb b WITH (NOLOCK)
		WHERE a.evento_id = b.evento_id
			AND b.tp_cobertura_id = 5
		)
--AND a.tp_cobertura_id <> 5
ORDER BY a.evento_id
GROUP BY a.tp_cobertura_id

SELECT *
FROM seguros_db.dbo.tp_cobertura_tb a WITH (NOLOCK)
WHERE a.tp_cobertura_id IN (
		819
		,1025
		,888
		,373
		,374
		,832
		,2
		,455
		)

SELECT *
FROM seguros_db.dbo.tp_cobertura_tb a WITH (NOLOCK)
WHERE a.tp_cobertura_id IN (5)

SELECT *
FROM seguros_db.dbo.evento_segbr_sinistro_cobertura_tb a WITH (NOLOCK)
WHERE evento_id IN (45269068)

SELECT *
FROM evento_SEGBR_sinistro_atual_tb
WHERE sinistro_id = 93201900015
ORDER BY evento_id

EXEC seguros_db.dbo.SEGS14630_SPS 24730516
	,45448770
	,5







--SEGP1296

BEGIN TRAN

--SET NOCOUNT ON EXEC seguros_db.dbo.SEGS8151_SPS '82',0,6785,''

SET NOCOUNT ON EXEC seguros_db.dbo.SEGS8151_SPS '93',0,6785,''


--ERROR NAS CHAVE DO SINISTRO, ATUALIZANDO A CHAVE DO RAMO 93 EM QUALIDADE
SELECT MAX(SINISTRO_ID) FROM SEGUROS_DB.DBO.sinistro_tb WHERE SINISTRO_ID LIKE '932020%'
SELECT MAX(SINISTRO_ID) FROM SEGUROS_DB.DBO.evento_SEGBR_sinistro_atual_tb WHERE SINISTRO_ID LIKE '932020%'


			use desenv_db

			go

			alter procedure dbo.alternova
			as

			begin

			update a
			set sinistro_id = 93202000104
			--SELECT sinistro_id
			FROM seguros_db.dbo.chave_sinistro_tb a
			WHERE ramo_id = 93-- @ramo_id
				AND sucursal_seguradora_id =0-- @sucursal_seguradora_id
				AND seguradora_cod_susep = 6785--@seguradora_cod_susep
			end

			begin tran
			exec dbo.alternova

ROLLBACK
--commit

93202000002
93202000001

SELECT max(SINISTRO_ID) FROM SEGUROS_DB.DBO.sinistro_tb with(nolock) WHERE ramo_id = 93


select dt_inclusao ,* from  SEGUROS_DB.DBO.sinistro_tb with(nolock) where sinistro_id BETWEEN  82202000010 and 82202000021 
select * from seguros_db.dbo.sinistro_tb where sinistro_id = 82202000020
select * from seguros_db.dbo.sinistro_cobertura_tb where sinistro_id = 82202000020
select * from evento_segbr_sinistro_atual_tb where sinistro_id = 82202000020 order by dt_inclusao


SELECT A.PROPOSTA_ID
	,A.evento_bb_id
	,b.descricao
	,A.cpf_cgc_segurado
	,A.cpf_sinistrado
	,*
FROM seguros_db.dbo.evento_SEGBR_sinistro_atual_tb a
INNER JOIN seguros_db.dbo.evento_SEGBR_tb b
	ON a.evento_segbr_id = b.evento_segbr_id
WHERE sinistro_id = 82202000020
ORDER BY a.dt_inclusao



select distinct top 10  * from seguros_db.dbo.sinistro_cobertura_tb a with(nolock) 
inner join evento_segbr_sinistro_atual_tb b with(nolock)
on a.sinistro_id=b.sinistro_id
inner join proposta_tb c with(nolock) 
on b.proposta_id = c.proposta_id
where a.ramo_id in( 82,93) and a.tp_cobertura_id = 5 and c.produto_id in(
200
,201
,202
,203
,204
,205
,206
,207
,208
,209
,210
,211
,212
,213
,214
,215
,217
,218
,219
,224
,225
)
 order by a.dt_inclusao desc


select dt_inclusao, * from seguros_db.dbo.sinistro_tb where sinistro_id = 93201911374

SELECT a.dt_inclusao,
A.PROPOSTA_ID
	,A.evento_bb_id
	,b.descricao
	,A.cpf_cgc_segurado
	,A.cpf_sinistrado
	,*
FROM seguros_db.dbo.evento_SEGBR_sinistro_atual_tb a
INNER JOIN seguros_db.dbo.evento_SEGBR_tb b
	ON a.evento_segbr_id = b.evento_segbr_id
WHERE sinistro_id = 93201911374
ORDER BY a.evento_id
--ORDER BY a.dt_inclusao
select dt_inclusao,* from seguros_db.dbo.sinistro_cobertura_tb where sinistro_id = 93201911374




sp_tables '%sinistro%'


CLASSIFICACAO_SINISTRO_TB

82202000020

EXEC sinistro_spi @sinistro_id = 82202000021
	,@apolice_id = 2635
	,@sucursal_seguradora_id = 0
	,@seguradora_cod_susep = 6785
	,@ramo_id = 82
	,@solicitante_id = NULL
	,@proposta_id = 24370465
	,@evento_sinistro_id = 28
	,@dt_aviso_sinistro = '20200219'
	,@dt_ocorrencia_sinistro = '20200219'
	,@dt_entrada_seguradora = '20200219'
	,@endereco = NULL
	,@bairro = NULL
	,@municipio_id = 999
	,@municipio = NULL
	,@estado = 'XX'
	,@agencia_id = NULL
	,@banco_id = 1
	,@cliente_id = 12245182
	,@situacao = '0'
	,@usuario = ''
	,@sinistro_id_lider = NULL
	,@endosso_id = NULL
	,@moeda_id = 790
	,@processa_reintegracao_is = 'n'
	,@subevento_sinistro_id = NULL
	,@tp_ramo_id = NULL
	,@ignorabloqueio = 1



	EXEC sinistro_marca_pgto_parc_spu @sinistro_id = 82202000021
	,@apolice_id = 2635
	,@sucursal_seguradora_id = 0
	,@seguradora_cod_susep = 6785
	,@ramo_id = 82
	,@usuario = ''
	,@pgto_parcial = 'n'
	,@ignorabloqueio = 1


	 EXEC interface_db.dbo.SMQS00193_SPI 82202000021 


	 SELECT count(1)
		FROM interface_db.dbo.entrada_gtr_tb entrada_gtr_tb WITH (NOLOCK)
		JOIN seguros_db.dbo.sinistro_bb_tb sinistro_bb_tb WITH (NOLOCK)
			ON sinistro_bb_tb.sinistro_bb = entrada_gtr_tb.sinistro_bb
		WHERE sinistro_bb_tb.sinistro_id = 82202000021
			AND entrada_gtr_tb.evento_bb = 2000


			 select count(1)
  from interface_db.dbo.entrada_gtr_tb entrada_gtr_tb WITH (NOLOCK)
  join seguros_db.dbo.sinistro_bb_tb sinistro_bb_tb WITH (NOLOCK)
    on sinistro_bb_tb.sinistro_bb =  entrada_gtr_tb.sinistro_bb
 Where sinistro_bb_tb.sinistro_id = 93202000105
   and entrada_gtr_tb.evento_bb   = 2000


		SELECT sinistro_tb.apolice_id
			,sinistro_tb.ramo_id
			,sucursal_id = sinistro_tb.sucursal_seguradora_id
			,seguradora_id = sinistro_tb.seguradora_cod_susep
			,proposta_tb.produto_id
			,proposta_tb.proposta_id
			,AgenciaAviso = isnull(sinistro_tb.agencia_id, 0)
			,dt_aviso = sinistro_tb.dt_aviso_sinistro
			,proposta_tb.prop_cliente_id
			,NomeCliente = cliente_tb.Nome
			,Dt_Ocorrencia = sinistro_tb.Dt_Ocorrencia_Sinistro
			,endereco = isnull(sinistro_tb.endereco, '')
			,sinistro_tb.situacao
		FROM SEGUROS_DB.DBO.sinistro_tb WITH (NOLOCK)
		JOIN SEGUROS_DB.DBO.proposta_tb WITH (NOLOCK)
			ON proposta_tb.proposta_id = sinistro_tb.proposta_id
		JOIN SEGUROS_DB.DBO.cliente_tb WITH (NOLOCK)
			ON cliente_tb.cliente_id = proposta_tb.prop_cliente_id
		WHERE sinistro_tb.sinistro_id = 82202000021


		select sinistro_tb.apolice_id
     , sinistro_tb.ramo_id
     , sucursal_id   = sinistro_tb.sucursal_seguradora_id
     , seguradora_id = sinistro_tb.seguradora_cod_susep
     , proposta_tb.produto_id
     , proposta_tb.proposta_id
     , AgenciaAviso  = isnull(sinistro_tb.agencia_id,0)
     , dt_aviso      = sinistro_tb.dt_aviso_sinistro
     , proposta_tb.prop_cliente_id
     , NomeCliente   = cliente_tb.Nome
     , Dt_Ocorrencia = sinistro_tb.Dt_Ocorrencia_Sinistro
     , endereco      = isnull(sinistro_tb.endereco,'')
     , sinistro_tb.situacao
  From SEGUROS_DB.DBO.sinistro_tb WITH (NOLOCK)
  Join SEGUROS_DB.DBO.proposta_tb WITH (NOLOCK)
    on proposta_tb.proposta_id = sinistro_tb.proposta_id
  Join SEGUROS_DB.DBO.cliente_tb WITH (NOLOCK)
    on cliente_tb.cliente_id = proposta_tb.prop_cliente_id
 where sinistro_tb.sinistro_id =93202000105



		select solicitante_sinistro_tb.Nome
     , isnull(solicitante_sinistro_tb.endereco,'') endereco
     , isnull(solicitante_sinistro_tb.DDD,'') DDD 
     , isnull(solicitante_sinistro_tb.telefone,'') telefone 
	  From SEGUROS_DB.DBO.solicitante_sinistro_tb WITH (NOLOCK)
	  join SEGUROS_DB.DBO.sinistro_tb WITH (NOLOCK)
		on solicitante_sinistro_tb.solicitante_id = sinistro_tb.solicitante_id
	 where sinistro_tb.sinistro_id = 82202000021


	 select solicitante_sinistro_tb.Nome
     , isnull(solicitante_sinistro_tb.endereco,'') endereco
     , isnull(solicitante_sinistro_tb.DDD,'') DDD 
     , isnull(solicitante_sinistro_tb.telefone,'') telefone 
  From SEGUROS_DB.DBO.solicitante_sinistro_tb WITH (NOLOCK)
  join SEGUROS_DB.DBO.sinistro_tb WITH (NOLOCK)
    on solicitante_sinistro_tb.solicitante_id = sinistro_tb.solicitante_id
 where sinistro_tb.sinistro_id = 93202000105



	rollback



	SELECT  * FROM sinistro_desc_objeto_tb WITH (NOLOCK)  WHERE SINISTRO_ID = 82202000021


	---COBERTURAS PRODUTOS BESC

	
SELECT 
distinct 
a.produto_id
	,b.tp_cobertura_id
	,b.nome
	,*
FROM tp_cob_item_prod_tb a
INNER JOIN tp_cobertura_tb b
	ON a.tp_cobertura_id = b.tp_cobertura_id
WHERE A.produto_id in(
200
,201
,202
,203
,204
,205
,206
,207
,208
,209
,210
,211
,212
,213
,214
,215
,217
,218
,219
,224
,225
)
--and b.tp_cobertura_id = 5
ORDER BY A.produto_id




SELECT 
--DISTINCT
	g.produto_id
	,e.tp_cobertura_id
	 ,f.nome 
	--a.proposta_id
	--,g.*

	,COUNT(1) AS TOTAL_PROPOSTAS
FROM SEGUROS_DB.DBO.PROPOSTA_TB G WITH (NOLOCK)
INNER JOIN SEGUROS_DB.DBO.escolha_plano_tb A
	ON G.PROPOSTA_ID = A.PROPOSTA_ID
INNER JOIN SEGUROS_DB.DBO.plano_tb B  WITH (NOLOCK)
	ON A.dt_inicio_vigencia = B.dt_inicio_vigencia
		AND A.PRODUTO_ID = B.PRODUTO_ID
INNER JOIN SEGUROS_DB.DBO.tp_plano_tp_comp_tb c WITH (NOLOCK)
	ON b.tp_plano_id = c.tp_plano_id
INNER JOIN SEGUROS_DB.DBO.tp_cob_comp_plano_tb d WITH (NOLOCK)
	ON b.tp_plano_id = d.tp_plano_id
INNER JOIN SEGUROS_DB.DBO.tp_cob_comp_tb e WITH (NOLOCK)
	ON d.tp_cob_comp_id = e.tp_cob_comp_id
INNER JOIN SEGUROS_DB.DBO.tp_cobertura_tb f WITH (NOLOCK)
	ON e.tp_cobertura_id = f.tp_cobertura_id
WHERE 1 = 1
	--AND a.proposta_id = 1129775 --24370465
	--AND convert(SMALLDATETIME, convert(VARCHAR(10), a.dt_escolha, 111)) <= '20200212'
	--AND (
	--	convert(SMALLDATETIME, convert(VARCHAR(10), a.dt_fim_vigencia, 111)) >= '20200212'
	--	OR a.dt_fim_vigencia IS NULL
	--	)
	--AND c.tp_componente_id = 1
	AND g.produto_id IN (
		200
		,201
		,202
		,203
		,204
		,205
		,206
		,207
		,208
		,209
		,210
		,211
		,212
		,213
		,214
		,215
		,217
		,218
		,219
		,224
		,225
		)
	AND g.situacao = 'i'
	AND e.tp_cobertura_id = 5
	and a.dt_fim_vigencia is not null
	GROUP BY g.produto_id,e.tp_cobertura_id,f.nome
	--ORDER BY g.produto_id



	SELECT 
	--DISTINCT 
	H.PRODUTO_ID,
	b.tp_cobertura_id
	,c.nome
	,H.PROPOSTA_ID
	,G.apolice_id
	 FROM SEGUROS_DB.DBO.APOLICE_TB  G WITH (NOLOCK)
	INNER JOIN SEGUROS_DB.DBO.PROPOSTA_TB H  WITH (NOLOCK)
	ON G.proposta_id = H.proposta_id
	INNER JOIN escolha_sub_grp_tp_cob_comp_tb a WITH (NOLOCK)
	ON G.apolice_id = A.apolice_id
	INNER JOIN tp_cob_comp_tb b WITH (NOLOCK)
	ON a.tp_cob_comp_id = b.tp_cob_comp_id
	INNER JOIN tp_cobertura_tb c WITH (NOLOCK)
	ON b.tp_cobertura_id = c.tp_cobertura_id
	WHERE H.produto_id IN (
		--200
		--,201
		--,202
		--,203
		--,204
		--,205
		206 --
		,207 --
		--,208
		--,209
		--,210
		--,211
		--,212
		--,213
		--,214
		--,215
		--,217
		--,218
		--,219
		--,224
		--,225
		) 
		AND C.tp_cobertura_id = 5
		ORDER BY H.produto_id




		select b.tp_cobertura_id, c.nome 
 from escolha_tp_cob_vida_aceito_tb a WITH (NOLOCK), 
 tp_cob_comp_tb b WITH (NOLOCK), 
 tp_cobertura_tb c WITH (NOLOCK) 
 where a.tp_cob_comp_id = b.tp_cob_comp_id 
 and b.tp_cobertura_id = c.tp_cobertura_id 
 and proposta_id = 24370465
 and b.tp_componente_id = 1

SELECT b.tp_cobertura_id
	,c.nome
FROM SEGUROS_DB.DBO.PROPOSTA_TB G
INNER JOIN escolha_tp_cob_vida_aceito_tb a WITH (NOLOCK)
	ON G.PROPOSTA_ID = A.PROPOSTA_ID
INNER JOIN tp_cob_comp_tb b WITH (NOLOCK)
	ON a.tp_cob_comp_id = b.tp_cob_comp_id
INNER JOIN tp_cobertura_tb c WITH (NOLOCK)
	ON b.tp_cobertura_id = c.tp_cobertura_id
WHERE G.produto_id IN (
		200
		,201
		,202
		,203
		,204
		,205
		,206 --
		,207 --
		,208
		,209
		,210
		,211
		,212
		,213
		,214
		,215
		,217
		,218
		,219
		,224
		,225
		)
--AND C.tp_cobertura_id = 5
ORDER BY G.produto_id




---SEGP1287


select sinistro_tb.apolice_id
     , sinistro_tb.ramo_id
     , sucursal_id   = sinistro_tb.sucursal_seguradora_id
     , seguradora_id = sinistro_tb.seguradora_cod_susep
     , proposta_tb.produto_id
     , proposta_tb.proposta_id
     , AgenciaAviso  = isnull(sinistro_tb.agencia_id,0)
     , dt_aviso      = sinistro_tb.dt_aviso_sinistro
     , proposta_tb.prop_cliente_id
     , NomeCliente   = cliente_tb.Nome
     , Dt_Ocorrencia = sinistro_tb.Dt_Ocorrencia_Sinistro
     , endereco      = isnull(sinistro_tb.endereco,'')
     , sinistro_tb.situacao
  From sinistro_tb WITH (NOLOCK)
  Join proposta_tb WITH (NOLOCK)
    on proposta_tb.proposta_id = sinistro_tb.proposta_id
  Join cliente_tb WITH (NOLOCK)
    on cliente_tb.cliente_id = proposta_tb.prop_cliente_id
 where sinistro_tb.sinistro_id =82202000021



--
select solicitante_sinistro_tb.Nome
     , isnull(solicitante_sinistro_tb.endereco,'') endereco
     , isnull(solicitante_sinistro_tb.DDD,'') DDD 
     , isnull(solicitante_sinistro_tb.telefone,'') telefone 
  From solicitante_sinistro_tb WITH (NOLOCK)
  join sinistro_tb WITH (NOLOCK)
    on solicitante_sinistro_tb.solicitante_id = sinistro_tb.solicitante_id
 where sinistro_tb.sinistro_id = 82202000021

 SELECT ISNULL(MAX(seq_estimativa),1)  FROM sinistro_estimativa_tb with(nolock)  WHERE sinistro_id             = 82202000021


  SELECT * FROM sinistro_estimativa_tb with(nolock)  WHERE sinistro_id             = 82202000021


  EXEC sinistro_historico_spi 82202000021, 2635, 0, 6785, 82, 10017,'0', NULL,'20200221',' ', 1, NULL

  Exec Sinistro_Item_Historico_SPI 82202000021, 2635, 0, 6785, 82, 3,'COBERTURA INCLU�DA: COBERTURA GEN�RICA PARA SINISTRO',''

  select b.descricao, * from  Seguros_Db.dbo.sinistro_historico_tb  a
  inner join  seguros_db.dbo.evento_segbr_tb b
  on a.evento_SEGBR_id = b.evento_segbr_id
  WHERE sinistro_id             = 82202000021




  --ALTERAR O LOCK AVISO DO SEGP1285
 USE DESENV_DB
 GO
 ALTER PROCEDURE DBO.ALTERNOVA
 AS
 BEGIN

  UPDATE A
  SET lock_aviso = 'n'
  --select lock_aviso  
  from seguros_db.dbo.sinistro_tb A 
  where sinistro_id = 82202000020

  END

  BEGIN TRAN
  EXEC ALTERNOVA

  rollback




  EXEC sinistro_historico_spi 82202000021, 2635, 0, 6785, 82, 10017,'0', NULL,'20200221',' ', 1, NULL

  Exec Sinistro_Item_Historico_SPI 82202000021, 2635, 0, 6785, 82, 3,'COBERTURA INCLU�DA: COBERTURA GEN�RICA PARA SINISTRO',''

	SELECT b.descricao
		,*
	FROM Seguros_Db.dbo.sinistro_historico_tb a
	INNER JOIN seguros_db.dbo.evento_segbr_tb b
		ON a.evento_SEGBR_id = b.evento_segbr_id
	WHERE sinistro_id = 82202000021

	SELECT *
	FROM sinistro_item_historico_tb
	WHERE sinistro_id = 82202000021



	SELECT numero = sinistro_bb
     , dt_fim_vigencia
     , situacao = case
                when dt_fim_vigencia Is Null
                then 'Ativo'
                Else 'Inativo'
             End
     , tipo = 'E'
  From sinistro_bb_tb WITH (NOLOCK)
 WHERE sinistro_id            = 82202000021
 ORDER BY sinistro_bb

EXEC sinistro_cobertura_spi 82202000021,2635,0,6785,82,1,1,1000.00,''
EXEC sinistro_cobertura_spi 82202000021,2635,0,6785,82,49,1,20000.00,''
EXEC sinistro_estimativa_spu 82202000021,2635,0,6785,82,'20200221',NULL,''
EXEC sinistro_estimativa_spi 82202000021,2635,0,6785,82,'20200221',1,2,21000.00,0.00,NULL, 0.000,0.000,' ','', NULL


SELECT * FROM evento_segbr_sinistro_cobertura_tb WHERE EVENTO_ID IN (
45574309
,45574602
,45574701
) 



	  SELECT * FROM evento_segbr_sinistro_ATUAL_tb WHERE sinistro_id = 82202000021

	  select * from sinistro_tb where sinistro_id = 82202000021


		SELECT *
		FROM seguros_db.dbo.sinistro_tb
		WHERE sinistro_id = 82202000021

		SELECT *
		FROM seguros_db.dbo.sinistro_cobertura_tb
		WHERE sinistro_id = 82202000021

		SELECT b.descricao
			,*
		FROM Seguros_Db.dbo.sinistro_historico_tb a
		INNER JOIN seguros_db.dbo.evento_segbr_tb b
			ON a.evento_SEGBR_id = b.evento_segbr_id
		WHERE sinistro_id = 82202000021


		select * from seguros_db.dbo.evento_segbr_tb order by evento_SEGBR_id

		SELECT *
		FROM sinistro_item_historico_tb
		WHERE sinistro_id = 82202000021



	select Sinistro_Tb.proposta_id                               
     , Sinistro_Tb.sinistro_id                               
     , Sinistro_Tb.apolice_id                                
     , Sinistro_Tb.sucursal_seguradora_id                    
     , Sinistro_Tb.seguradora_cod_susep                      
     , Sinistro_Tb.ramo_id                                   
     , Sinistro_Tb.cliente_id                                
     , Sinistro_Tb.dt_ocorrencia_sinistro                    
     , sinistro_tb.situacao                                  
     , Proposta_Tb.Produto_ID                                
     , Sinistro_Tb.Moeda_ID                                  
     , Sinistro_Tb.Pgto_Parcial_Co_Seguro   
	 ,sinistro_tb.evento_sinistro_id
	 ,sinistro_tb.tp_componente_id                 
  From seguros_db.dbo.sinistro_tb sinistro_tb WITH (NOLOCK)   
  Join Seguros_Db.Dbo.Proposta_Tb Proposta_tb WITH (NOLOCK)   
    on Proposta_Tb.Proposta_ID = Sinistro_Tb.Proposta_ID     
 Where sinistro_id = 82202000021

 evento_sinistro_id
28



26/02/2020.0.225.2.0.82.2635.0.6785.0

,19,80,113,170,224,187,216,129,129,24,93,8,109,120,120,235,225,255,209,198,194,93,85,73,97

sinistro_detalhamento_spi
sinistro_linha_detalhe_spi



SELECT @seq_evento        = isnull(max(seq_evento),0)     
  FROM Seguros_Db.Dbo.sinistro_historico_tb  with(nolock)     
 WHERE sinistro_id        = 932020000105--82202000021--@sinistro_id   

 select a.evento_segbr_id,b.descricao, *  FROM Seguros_Db.Dbo.sinistro_historico_tb a  with(nolock) 
 inner join seguros_db.dbo.evento_segbr_tb  b
 on a.evento_segbr_id = b.evento_segbr_id   
 WHERE sinistro_id        = 93202000105--82202000021--@sinistro_id   
 order by a.seq_evento

  select *  FROM Seguros_Db.Dbo.sinistro_item_historico_tb  with(nolock)     
 WHERE sinistro_id        = 93202000105--82202000021--@sinistro_id   

 
 SELECT * FROM SEGUROS_DB.DBO.SINISTRO_TB WHERE SINISTRO_ID =93202000105



 ---------
   --200
  --,201
  --,202
  --,203
  --,204
  --,205
  --,206
  --,207 --OI
  --,208
  --,209
  --,210
  --,211
  --,212
  ----,213
  ----,214
  ----,215
  ----,217
  ----,218
  ----,219
  --,224 -- OI
  --,225 -- OI
  ---------


 -- ANALISE DA VIGENCIA DA APOLICE BESC
 SELECT B.PRODUTO_ID,COUNT(1) FROM SEGUROS_DB.DBO.PROPOSTA_ADESAO_TB  A
 INNER JOIN SEGUROS_DB.DBO.PROPOSTA_TB B
 ON A.PROPOSTA_ID = B.PROPOSTA_ID
 INNER JOIN SEGUROS_DB.DBO.PRODUTO_TB C
 ON B.PRODUTO_ID = C.PRODUTO_ID
  WHERE 1=1
  --AND A.PROPOSTA_ID = 24217980
  AND C.PRODUTO_ID IN(
200
,201
,202
,203
,204
,205
,206
,208
,209
,210
,211
,212
  )
  AND dt_fim_vigencia IS NULL
  GROUP BY B.PRODUTO_ID
  ORDER BY B.PRODUTO_ID
   SELECT B.PRODUTO_ID,COUNT(1) FROM seguros_db.dbo.proposta_adesao_tb  A
 INNER JOIN SEGUROS_DB.DBO.PROPOSTA_TB B
 ON A.PROPOSTA_ID = B.PROPOSTA_ID
 INNER JOIN SEGUROS_DB.DBO.PRODUTO_TB C
 ON B.PRODUTO_ID = C.PRODUTO_ID
  WHERE 1=1
  --AND A.PROPOSTA_ID = 24217980
 AND C.PRODUTO_ID IN(
200
,201
,202
,203
,204
,205
,206
,208
,209
,210
,211
,212
  )
  --AND dt_fim_vigencia IS NOT NULL
  GROUP BY B.PRODUTO_ID
  ORDER BY B.PRODUTO_ID


  SELECT * FROM SEGUROS_DB.DBO.PROPOSTA_TB WHERE PROPOSTA_ID = 24217980

  SELECT TOP 1 * FROM SEGUROS_DB.DBO.CANCELAMENTO_PROPOSTA_TB A
  INNER JOIN SEGUROS_DB.DBO.PROPOSTA_tB B
  ON A.PROPOSTA_ID = B.PROPOSTA_ID
  WHERE B.PRODUTO_ID = 200

    SELECT B.PRODUTO_ID,COUNT(1) FROM SEGUROS_DB.DBO.cancelamento_proposta_tb A
  INNER JOIN SEGUROS_DB.DBO.PROPOSTA_tB B
  ON A.PROPOSTA_ID = B.PROPOSTA_ID
  WHERE B.PRODUTO_ID = 200
  GROUP BY B.PRODUTO_ID

    SELECT A.PRODUTO_ID, COUNT(1) FROM SEGUROS_DB.DBO.PROPOSTA_TB A WHERE PRODUTO_ID IN(
	 200
  ,201
  ,202
  ,203
  ,204
  ,205
  ,206
  ,207 --OI
  ,208
  ,209
  ,210
  ,211
  ,212
  --,213
  --,214
  --,215
  --,217
  --,218
  --,219
  ,224 -- OI
  ,225 -- OI
	)
	AND A.PROPOSTA_ID NOT IN (SELECT B.PROPOSTA_ID FROM SEGUROS_DB.DBO.cancelamento_proposta_tb B WHERE B.dt_fim_cancelamento > GETDATE() )
	GROUP BY A.PRODUTO_ID


	

  

  SELECT TOP 1 B.PROPOSTA_ID FROM SEGUROS_DB.DBO.cancelamento_proposta_tb B
  INNER JOIN SEGUROS_DB.DBO.PROPOSTA_TB A
  ON A.PROPOSTA_ID= B.proposta_id
  WHERE PRODUTO_ID = 200

   WHERE PROPOSTA_ID = 24217980


  cancelamento_proposta_tb

  SP_TABLES '%CANCE%'


   SELECT B.PRODUTO_ID,COUNT(1) FROM seguros_db.dbo.proposta_adesao_tb  A
 INNER JOIN SEGUROS_DB.DBO.PROPOSTA_TB B
 ON A.PROPOSTA_ID = B.PROPOSTA_ID
 INNER JOIN SEGUROS_DB.DBO.PRODUTO_TB C
 ON B.PRODUTO_ID = C.PRODUTO_ID
  WHERE 1=1
  --AND A.PROPOSTA_ID = 24217980
  AND C.PRODUTO_ID IN(
224,225
)
  GROUP BY B.PRODUTO_ID
  ORDER BY B.PRODUTO_ID




SELECT * FROM [SISAB051\EXTRATOR].SEGUROS_DB.DBO.PROPOSTA_TB WHERE PRODUTO_ID = 207

SP_LINKEDSERVERS

 --93202000105

 EXEC DESENV_DB.dbo.SEGS14630_SPS 
24217980,
28,
5,
93202000105


EXEC desenv_db.dbo.SEGS14666_SPI 
200,
93,
24217980,
28,
5,
1,
'20200228',
 Null ,
93202000105,
'C00216281',1




 sp_tables '%evento%'

 