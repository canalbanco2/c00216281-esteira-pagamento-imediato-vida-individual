CREATE PROCEDURE dbo.SEGS14737_SPU (
					  @SINISTRO_ID   NUMERIC(11,0)
					  )
  
AS     
    
 /**************************************************************************************************    
 Data Cria��o  : 21/02/2020 
 Projeto       : c00216281-esteira-pagamento-imediato-vida-individual Complemento da fase 1
 Autores       : Sergio Ricardo
 Descri��o     : Procedure criada para atualizar o estado do aviso de sinistro classificado como deferido
				 ja enviado na relacao diaria.
 '**************************************************************************************************/    
 
 -- BLOCO DE TESTE 
/*   
 DECLARE @SINISTRO_ID   NUMERIC(11,0)

  BEGIN TRAN
    IF @@TRANCOUNT > 0 
		 EXEC seguros_db.dbo.SEGS14737_SPU 
		 @SINISTRO_ID = 77201903829 
    ELSE 
		SELECT 'Erro. A transa��o n�o foi aberta para executar o teste.'
  ROLLBACK  
*/

BEGIN
	SET NOCOUNT ON

	BEGIN TRY
		
		UPDATE c
		   SET c.enviado_planilha = 'S'
		  FROM seguros_db.dbo.sinistro_classificacao_parametro_tb c
		 WHERE c.sinistro_id = @SINISTRO_ID  
						
	END TRY

	BEGIN CATCH
		DECLARE @ErrorMessage NVARCHAR(4000)
		DECLARE @ErrorSeverity INT
		DECLARE @ErrorState INT

		SELECT @ErrorMessage = ERROR_PROCEDURE() + ' - Linha ' + CONVERT(VARCHAR(15), ERROR_LINE()) + ' - ' + ERROR_MESSAGE()
			,@ErrorSeverity = ERROR_SEVERITY()
			,@ErrorState = ERROR_STATE()

		RAISERROR (
				@ErrorMessage
				,@ErrorSeverity
				,@ErrorState
				)
	END CATCH
END	
GO


 









