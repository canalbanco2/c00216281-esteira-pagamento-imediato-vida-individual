DECLARE @total_registro INT
DECLARE @total_registro_afetados  INT
DECLARE @mensagem VARCHAR(500)

/*qtd registro que serao afetados*/
SELECT @total_registro = 1
	   
	UPDATE tp_sinistro_parametro_tb
	   SET tp_sinistro_parametro_tb.tp_ramo_id = 2
	  FROM seguros_db.dbo.tp_sinistro_parametro_tb tp_sinistro_parametro_tb
	 WHERE tp_sinistro_parametro_tb.nome = 'Esteira Pagamento Imediato - Sinistros de Danos Elétricos Residenciais'
	   
SET @total_registro_afetados= @@ROWCOUNT

IF ((@@SERVERNAME = 'SISAB003' OR @@SERVERNAME = 'SISAS003\ABS')  AND @total_registro <> @total_registro_afetados) 
 BEGIN
	SET @mensagem='qtd de registros que deveriam ser afetados: ' + CONVERT(VARCHAR(10),@total_registro) + 
										 'qtd de registros afetados: ' + CONVERT(VARCHAR(10),@total_registro_afetados)
	RAISERROR (@mensagem,16,1) 
 END	

	UPDATE tp_sinistro_parametro_tb
	   SET tp_sinistro_parametro_tb.tp_ramo_id = 1,
		   tp_sinistro_parametro_tb.lista_destino_producao = 'cvieira@brasilseg.com.br; joabreu@brasilseg.com.br; dsaraujo@brasilseg.com.br; albborja@brasilseg.com.br; sinistrosdepessoas@brasilseg.com.br',
		   tp_sinistro_parametro_tb.lista_destino_homologacao = 'vmorrone@brasilseg.com.br; lamsantos@brasilseg.com.br; rapaula@brasilseg.com.br; ARMARAUJO@brasilseg.com.br',
		   tp_sinistro_parametro_tb.envia_relatorio_diario = 'S'
	  FROM seguros_db.dbo.tp_sinistro_parametro_tb tp_sinistro_parametro_tb
	 WHERE tp_sinistro_parametro_tb.nome = 'Esteira Pagamento Imediato – Vida Individual'
	   
SET @total_registro_afetados= @@ROWCOUNT

IF ((@@SERVERNAME = 'SISAB003' OR @@SERVERNAME = 'SISAS003\ABS')  AND @total_registro <> @total_registro_afetados) 
 BEGIN
	SET @mensagem='qtd de registros que deveriam ser afetados: ' + CONVERT(VARCHAR(10),@total_registro) + 
										 'qtd de registros afetados: ' + CONVERT(VARCHAR(10),@total_registro_afetados)
	RAISERROR (@mensagem,16,1) 
 END	
