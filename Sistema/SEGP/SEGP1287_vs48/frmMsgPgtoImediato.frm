VERSION 5.00
Begin VB.Form frmMsgPgtoImediato 
   Caption         =   "Classifica��o do Sinistro"
   ClientHeight    =   2280
   ClientLeft      =   60
   ClientTop       =   450
   ClientWidth     =   5820
   ControlBox      =   0   'False
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   2280
   ScaleWidth      =   5820
   StartUpPosition =   2  'CenterScreen
   Begin VB.CommandButton btnOK 
      Caption         =   "OK"
      Height          =   420
      Left            =   2370
      TabIndex        =   1
      Top             =   1650
      Width           =   1200
   End
   Begin VB.Label lblMsgPagtoImediato 
      BackColor       =   &H0000FFFF&
      Caption         =   $"frmMsgPgtoImediato.frx":0000
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   1080
      Left            =   240
      TabIndex        =   0
      Top             =   240
      Width           =   5445
   End
End
Attribute VB_Name = "frmMsgPgtoImediato"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
'C00216281-ESTEIRA-PAGAMENTO-IMEDIATO-VIDA-INDIVIDUAL
Private Sub btnOK_Click()
    Unload Me
    MsgPgtoImediato = 1
End Sub
'C00216281 - FIM
Private Sub Form_Load()

   lblMsgPagtoImediato.Caption = "Sinistro Pr�-Aprovado, necess�rio o envio" & Chr(13) & _
                                 "somente dos documentos" & Chr(13) & _
                                 "(Certid�o de �bito e documentos de benefici�rios)" & Chr(13) & _
                                 "atrav�s do site BBSeguros.com.br."

End Sub
