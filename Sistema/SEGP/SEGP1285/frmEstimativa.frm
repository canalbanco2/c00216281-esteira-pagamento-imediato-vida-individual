VERSION 5.00
Object = "{5E9E78A0-531B-11CF-91F6-C2863C385E30}#1.0#0"; "MSFLXGRD.OCX"
Object = "{FE0065C0-1B7B-11CF-9D53-00AA003C9CB6}#1.1#0"; "COMCT232.OCX"
Begin VB.Form frmEstimativa 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "Form1"
   ClientHeight    =   8010
   ClientLeft      =   4065
   ClientTop       =   1395
   ClientWidth     =   7800
   LinkMode        =   1  'Source
   LinkTopic       =   "FrmConsultaSinistro"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   8010
   ScaleWidth      =   7800
   ShowInTaskbar   =   0   'False
   Begin VB.TextBox txtRetornoAuxiliar 
      Height          =   315
      Left            =   120
      TabIndex        =   28
      Text            =   "CANCELADO"
      Top             =   7560
      Width           =   1215
   End
   Begin VB.CommandButton btnCancelar 
      Caption         =   "&Sair"
      Height          =   375
      Left            =   6240
      TabIndex        =   27
      Top             =   7560
      Width           =   1452
   End
   Begin VB.Frame fmeEstimativasPagamentos_Dados 
      Caption         =   "Dados"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   2100
      Left            =   120
      TabIndex        =   3
      Top             =   120
      Width           =   7560
      Begin VB.CheckBox chkPgto_Parcial_Co_Seguro 
         Caption         =   "Pgto. sobre nossa parte"
         Height          =   255
         Left            =   240
         TabIndex        =   16
         Top             =   1680
         Width           =   2175
      End
      Begin VB.TextBox txtSeq_Estimativa 
         Alignment       =   2  'Center
         Height          =   315
         Left            =   3030
         TabIndex        =   15
         Text            =   "6"
         Top             =   510
         Width           =   305
      End
      Begin VB.TextBox txtMoeda_Sigla 
         Height          =   315
         Left            =   240
         TabIndex        =   14
         Top             =   510
         Width           =   1095
      End
      Begin VB.TextBox txtNossa_Parte 
         Height          =   315
         Left            =   1560
         TabIndex        =   13
         Top             =   510
         Width           =   1215
      End
      Begin VB.TextBox txtUsuario 
         Height          =   315
         Left            =   1680
         TabIndex        =   12
         Text            =   "Usu�rio"
         Top             =   1230
         Width           =   2535
      End
      Begin VB.TextBox txtCountEstimativa 
         Alignment       =   2  'Center
         Height          =   315
         Left            =   3555
         Locked          =   -1  'True
         TabIndex        =   11
         Text            =   "6"
         Top             =   510
         Width           =   560
      End
      Begin VB.TextBox txtPerc_Re_Seguro_Quota 
         Height          =   315
         Left            =   4440
         TabIndex        =   9
         Top             =   1230
         Width           =   735
      End
      Begin VB.TextBox txtPerc_Re_Seguro_Er 
         Height          =   315
         Left            =   5160
         TabIndex        =   8
         Top             =   1230
         Width           =   735
      End
      Begin VB.TextBox txtDt_Inicio_Estimativa 
         Height          =   315
         Left            =   4560
         TabIndex        =   7
         Top             =   510
         Width           =   1215
      End
      Begin VB.TextBox txtDt_Fim_Estimativa 
         Height          =   315
         Left            =   6000
         TabIndex        =   6
         Top             =   510
         Width           =   1215
      End
      Begin VB.TextBox txtDt_Inclusao_Estimativa 
         Height          =   315
         Left            =   240
         TabIndex        =   5
         Top             =   1230
         Width           =   1215
      End
      Begin VB.TextBox txtNum_Carta_Seguro_Aceito 
         Height          =   315
         Left            =   6120
         TabIndex        =   4
         Top             =   1230
         Width           =   1095
      End
      Begin ComCtl2.UpDown btnSeq_Estimativa 
         Height          =   315
         Left            =   3336
         TabIndex        =   10
         Top             =   510
         Width           =   255
         _ExtentX        =   450
         _ExtentY        =   556
         _Version        =   327681
         BuddyControl    =   "txtSeq_Estimativa"
         BuddyDispid     =   196613
         OrigLeft        =   3570
         OrigTop         =   520
         OrigRight       =   3825
         OrigBottom      =   820
         SyncBuddy       =   -1  'True
         BuddyProperty   =   65547
         Enabled         =   -1  'True
      End
      Begin VB.Label lblEstimativasPagamentos_Dados 
         Caption         =   "Data de in�cio:"
         Height          =   255
         Index           =   3
         Left            =   4560
         TabIndex        =   26
         Top             =   300
         Width           =   1095
      End
      Begin VB.Label lblEstimativasPagamentos_Dados 
         Caption         =   "% Quota:"
         Height          =   255
         Index           =   7
         Left            =   4440
         TabIndex        =   25
         Top             =   1020
         Width           =   735
      End
      Begin VB.Label lblEstimativasPagamentos_Dados 
         AutoSize        =   -1  'True
         Caption         =   "% ER:"
         Height          =   195
         Index           =   8
         Left            =   5160
         TabIndex        =   24
         Top             =   1020
         Width           =   435
      End
      Begin VB.Label lblEstimativasPagamentos_Dados 
         AutoSize        =   -1  'True
         Caption         =   "N�m. da carta:"
         Height          =   195
         Index           =   9
         Left            =   6120
         TabIndex        =   23
         Top             =   1020
         Width           =   1050
      End
      Begin VB.Label lblEstimativasPagamentos_Dados 
         Caption         =   "Data fim:"
         Height          =   255
         Index           =   4
         Left            =   6000
         TabIndex        =   22
         Top             =   300
         Width           =   1335
      End
      Begin VB.Label lblEstimativasPagamentos_Dados 
         Caption         =   "Seq. da estimativa:"
         Height          =   255
         Index           =   2
         Left            =   3030
         TabIndex        =   21
         Top             =   300
         Width           =   1455
      End
      Begin VB.Label lblEstimativasPagamentos_Dados 
         Caption         =   "Data de inclus�o:"
         Height          =   255
         Index           =   5
         Left            =   240
         TabIndex        =   20
         Top             =   1020
         Width           =   1335
      End
      Begin VB.Label lblEstimativasPagamentos_Dados 
         Caption         =   "Moeda seguro:"
         Height          =   255
         Index           =   0
         Left            =   240
         TabIndex        =   19
         Top             =   300
         Width           =   1215
      End
      Begin VB.Label lblEstimativasPagamentos_Dados 
         Caption         =   "% Nossa parte:"
         Height          =   255
         Index           =   1
         Left            =   1560
         TabIndex        =   18
         Top             =   300
         Width           =   1215
      End
      Begin VB.Label lblEstimativasPagamentos_Dados 
         Caption         =   "Usu�rio:"
         Height          =   255
         Index           =   6
         Left            =   1680
         TabIndex        =   17
         Top             =   1020
         Width           =   855
      End
   End
   Begin VB.Frame fmeEstimativasPagamentos_Valores 
      Caption         =   "Valores"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   5130
      Left            =   120
      TabIndex        =   0
      Top             =   2280
      Width           =   7545
      Begin MSFlexGridLib.MSFlexGrid grdEstimativasPagamentos_Estimativas 
         Height          =   2385
         Left            =   240
         TabIndex        =   1
         Top             =   2640
         Width           =   7095
         _ExtentX        =   12515
         _ExtentY        =   4207
         _Version        =   393216
         Rows            =   9
         Cols            =   7
         ScrollBars      =   0
         GridLineWidth   =   2
         FormatString    =   "^Item                          |^Valor          |^Pago/Recebido |^Saldo          |^Resseguro                |^Sit.| $ "
      End
      Begin MSFlexGridLib.MSFlexGrid grdEstimativasPagamentos_Coberturas 
         Height          =   2145
         Left            =   240
         TabIndex        =   2
         Top             =   360
         Width           =   7095
         _ExtentX        =   12515
         _ExtentY        =   3784
         _Version        =   393216
         Rows            =   7
         Cols            =   3
         FixedCols       =   0
         AllowBigSelection=   0   'False
         FormatString    =   "Cobertura                                                                              | Valor estimado   | Corre��o monet."
      End
   End
End
Attribute VB_Name = "frmEstimativa"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Private lConexaoLocal                           As Integer

Private cTipoCobertura                          As New Collection
Private cDados_Estimativa                       As New Collection

Private bytModoOperacao                         As Byte '1-Inclusao / 2-Alteracao/Exclus�o / 3-Consulta
Private bCarregou_EstimativasPagamentos         As Boolean
Private bAlterou_Seq_Estimativa                 As Boolean
Private dApolice_Dt_Fim_Vigencia                As Date
Private dApolice_Dt_Inicio_Vigencia             As Date

Private sAuxiliarExecutado                      As String       'AKIO.OKUNO - 18/10/2012


Private Sub btnCancelar_Click()
    On Error GoTo Erro
    
    DestroiInterface
    
    Unload Me
    Exit Sub

Erro:
    TrataErroGeral "BtnCancelar_Click", Me.Caption
    FinalizarAplicacao
End Sub

Private Sub DestroiInterface()
    On Error GoTo Erro
    
    Set rsRecordSet = Nothing
    Set cDados_Estimativa = Nothing
    
    bCarregou_EstimativasPagamentos = False
    bAlterou_Seq_Estimativa = False
    
    Exit Sub

Erro:
    TrataErroGeral "DestroiInterface", Me.Caption
    FinalizarAplicacao

End Sub


Private Sub Form_Load()

    On Error GoTo Erro
    
    IniciarConexao
    
    InicializaVariaveisLocal
    
    InicializaInterfaceLocal
    
    Exit Sub
    
Erro:
    TrataErroGeral "Form_Load", Me.name
    FinalizarAplicacao
    
End Sub

Private Sub InicializaVariaveisLocal()

    bytModoOperacao = 3
    
End Sub


Private Sub IniciarConexao()
    On Error GoTo Erro

'AKIO.OKUNO - INICIO - 30/04/2013
    lConexaoLocal = vNunCx
'    vNunCx = lConexaoLocal
'    If EstadoConexao(vNunCx) = adStateClosed Then
'        lConexaoLocal = AbrirConexao(gsSIGLASISTEMA, glAmbiente_id, App.Title, App.FileDescription)
'    End If
'AKIO.OKUNO - FIM - 30/04/2013
    Exit Sub

Erro:
    TrataErroGeral "IniciarConexao", Me.name
    FinalizarAplicacao

End Sub

Private Sub InicializaInterfaceLocal()
    On Error GoTo Erro
    
    Screen.MousePointer = vbHourglass
    
    Call CentraFrm(Me)
        
    InicializaInterfaceLocal_Form
        
    InicializaInterfaceLocal_Grid_Estimativas
        
    InicializaInterfaceLocal_Grid_AbaEstimativasPagamentos
        
    LimpaCamposTelaLocal Me                                         'SERA ALTERADO
        
    CarregaDadosTela 1
    
    Screen.MousePointer = vbNormal
    
    Exit Sub

Erro:
    TrataErroGeral "InicializaInterfaceLocal", Me.name
    FinalizarAplicacao
    
End Sub


Private Sub InicializaInterfaceLocal_Form()

    txtRetornoAuxiliar.Visible = False 'cristovao.rodrigues 16/10/2012
    
    Me.Caption = App.EXEName & " - Sele��o de Estimativa - Pagamento de sinistro - " & Ambiente

    InicializaModoOperacaoLocal
    
End Sub

Private Sub LimpaCamposTelaLocal(objObjeto As Object)
    On Error GoTo Erro
    
    If Not objObjeto Is Nothing Then
        LimpaCamposTela objObjeto
    End If
    
    Exit Sub

Erro:
    TrataErroGeral "LimpaCamposTelaLocal", Me.name
    FinalizarAplicacao

End Sub


Private Sub CarregaDadosColecao_Estimativa()
    Dim rsRecordSet                             As ADODB.Recordset
    Dim sSQL                                    As String
    Dim oEstimativa                             As New clsEstimativa
    
    On Error GoTo Erro
    
    MousePointer = vbHourglass
       
    sSQL = sSQL & "Select * " & vbCrLf
    sSQL = sSQL & "  From #EstimativaPagamento_Estimativa"
    
    Set rsRecordSet = Conexao_ExecutarSQL(gsSIGLASISTEMA, _
                                          glAmbiente_id, _
                                          App.Title, _
                                          App.FileDescription, _
                                          sSQL, _
                                          lConexaoLocal, _
                                          True)
    With rsRecordSet
        Do While Not .EOF
            Set oEstimativa = New clsEstimativa
            oEstimativa.Seq_Estimativa = .Fields!Seq_Estimativa
            If .Fields!Dt_Inicio_Estimativa <> "00:00:00" Then
                oEstimativa.Dt_Inicio_Estimativa = .Fields!Dt_Inicio_Estimativa
            End If
            
            If .Fields!Dt_Fim_Estimativa <> "00:00:00" Then
                oEstimativa.Dt_Fim_Estimativa = .Fields!Dt_Fim_Estimativa
            Else
                oEstimativa.Dt_Fim_Estimativa = "1900-01-01"
            End If
            
            oEstimativa.Item_Val_Estimativa = .Fields!Item_Val_Estimativa
            oEstimativa.Val_Estimado = .Fields!Val_Estimado
            oEstimativa.Val_Pago = .Fields!Val_Pago
            oEstimativa.Val_Resseguro = .Fields!Val_Resseguro
            oEstimativa.Situacao = .Fields!Situacao
            oEstimativa.Usuario = .Fields!Usuario
            oEstimativa.Num_Carta_Seguro_Aceito = .Fields!Num_Carta_Seguro_Aceito
            oEstimativa.Perc_Re_Seguro_Quota = .Fields!Perc_Re_Seguro_Quota
            oEstimativa.Perc_Re_Seguro_Er = .Fields!Perc_Re_Seguro_Er
            oEstimativa.Tipo = .Fields!Tipo
            oEstimativa.Nossa_Parte = .Fields!Nossa_Parte
            
            If .Fields!Dt_Inclusao <> "00:00:00" Then
                oEstimativa.Dt_Inclusao = .Fields!Dt_Inclusao
            End If
            
            cDados_Estimativa.Add oEstimativa
            .MoveNext
        Loop
        
    End With
    
    MousePointer = vbNormal
    
    Set rsRecordSet = Nothing
    Set oEstimativa = Nothing
    Exit Sub
    
Erro:
    Call TrataErroGeral("CarregaDadosColecao_Estimativa", Me.Caption)
    Call FinalizarAplicacao

End Sub


Private Sub CarregaDadosTela(iSeq_Estimativa As Integer)
    Dim rsRecordSet                             As ADODB.Recordset
    Dim sSQL                                    As String
    Dim oEstimativa                             As clsEstimativa
    Dim bSemEstimativa                          As Boolean  'AKIO.OKUNO - 05/10/2012
    
    On Error GoTo Erro
    
    If Not bCarregou_EstimativasPagamentos Or bAlterou_Seq_Estimativa Then
    
        MousePointer = vbHourglass
        
        If Not bCarregou_EstimativasPagamentos Then
        
            InicializaModoProcessamentoObjeto grdEstimativasPagamentos_Coberturas, 1
            InicializaModoProcessamentoObjeto grdEstimativasPagamentos_Estimativas, 1
            
            SelecionaDados
        
            CarregaDadosColecao_Estimativa
        
            sSQL = ""
            sSQL = sSQL & "Select * " & vbCrLf
            sSQL = sSQL & "  From #EstimativaPagamento_Estimativa" & vbCrLf
'AKIO.OKUNO - INICIO - 23/10/2012
'            If iSeq_Estimativa <> 0 Then
'                sSQL = sSQL & " Where Seq_Estimativa = " & iSeq_Estimativa
'            End If
'AKIO.OKUNO - FIM - 23/10/2012
    
            Set rsRecordSet = Conexao_ExecutarSQL(gsSIGLASISTEMA, _
                                                  glAmbiente_id, _
                                                  App.Title, _
                                                  App.FileDescription, _
                                                  sSQL, _
                                                  lConexaoLocal, _
                                                  True)
                                                  
            bSemEstimativa = rsRecordSet.EOF 'AKIO.OKUNO - 05/10/2012
            
            If rsRecordSet.EOF Then
                sSQL = ""
                sSQL = sSQL & "Select Sigla  AS Moeda_Sigla                                                                                                                           " & vbNewLine
                sSQL = sSQL & "     , Moeda_ID                                                                                                                          " & vbNewLine
                sSQL = sSQL & "  From #Sinistro_Principal                                                                                                               " & vbNewLine
                sSQL = sSQL & "  Left Join Seguros_Db.Dbo.Proposta_Fechada_Tb       Proposta_Fechada_Tb  WITH (NOLOCK)                                                         " & vbNewLine
                sSQL = sSQL & "    on Proposta_Fechada_Tb.Proposta_ID               = #Sinistro_Principal.Proposta_ID                                                   " & vbNewLine
                sSQL = sSQL & "  Left Join Seguros_Db.Dbo.Proposta_Adesao_Tb        Proposta_Adesao_Tb  WITH (NOLOCK)                                                          " & vbNewLine
                sSQL = sSQL & "    on Proposta_Adesao_Tb.Proposta_ID                = #Sinistro_Principal.Proposta_ID                                                   " & vbNewLine
                sSQL = sSQL & "  Join Seguros_Db.Dbo.Moeda_Tb                       Moeda_Tb  WITH (NOLOCK)                                                                    " & vbNewLine
                sSQL = sSQL & "    on Moeda_Tb.Moeda_ID                             = isnull(Proposta_Fechada_Tb.Seguro_Moeda_ID, Proposta_Adesao_Tb.Seguro_Moeda_ID)   " & vbNewLine
                
                Set rsRecordSet = Conexao_ExecutarSQL(gsSIGLASISTEMA, _
                                                      glAmbiente_id, _
                                                      App.Title, _
                                                      App.FileDescription, _
                                                      sSQL, _
                                                      lConexaoLocal, _
                                                      True)
                                                      
                
                
            End If
            
            With rsRecordSet
'                If Not .EOF Then       'AKIO.OKUNO - 05/10/2012
                If Not .EOF And Not bSemEstimativa Then
                    '.MoveLast   'AKIO.OKUNO - 18/10/2012   'RETIRADO EM 23/10/2012
                    .MoveLast   'AKIO.OKUNO - 13/11/2012

                    'Dados do Frame: Dados
                    btnSeq_Estimativa.Max = Val("0" & .Fields!CountEstimativa)
                    btnSeq_Estimativa.Value = .Fields!Seq_Estimativa
                    btnSeq_Estimativa.Min = 1
    
                    txtCountEstimativa = " de " & Val("0" & .Fields!CountEstimativa)
                    txtCountEstimativa.Tag = Val("0" & .Fields!CountEstimativa)
    
                    txtNossa_Parte = .Fields!Nossa_Parte & ""
                    txtSeq_Estimativa = .Fields!Seq_Estimativa
                    
                    'Dados da Aba: Estimativas/Pagamentos
                    txtMoeda_Sigla = .Fields!Moeda_Sigla & ""
                    If Not IsNull(.Fields!Pgto_Parcial_Co_Seguro) Then
                        If UCase(.Fields!Pgto_Parcial_Co_Seguro) = "N" Then
                            chkPgto_Parcial_Co_Seguro.Value = 0
                            'mathayde
                            If sOperacaoCosseguro <> "C" Then
                                chkPgto_Parcial_Co_Seguro.Visible = False
                            End If
                        Else
                            chkPgto_Parcial_Co_Seguro.Value = 1
                        End If
                    Else
                        chkPgto_Parcial_Co_Seguro.Value = 0
                    End If
                'AKIO.OKUNO - INICIO - 05/10/2012
                Else
                    txtCountEstimativa = " de 0"
                    txtDt_Inclusao_Estimativa.Text = Format(Now(), "dd/mm/yyyy")
                    txtMoeda_Sigla = .Fields!Moeda_Sigla & ""
                    txtNossa_Parte = "100,00"
                    txtUsuario = cUserName
                    txtPerc_Re_Seguro_Quota = "0.000"
                    txtPerc_Re_Seguro_Er = "0.000"
                    btnSeq_Estimativa.Min = 0
                    btnSeq_Estimativa.Max = 0
                'AKIO.OKUNO - FIM - 05/10/2012
                End If
            End With
        End If
        
'        If Val(txtSeq_Estimativa) <> 0 Or iSeq_Estimativa <> 0 Then        'AKIO.OKUNO - 05/10/2012
        If Val(txtSeq_Estimativa) <> 0 Then
'            If iSeq_Estimativa <> 0 Then        'AKIO.OKUNO - 18/10/2012
'                txtSeq_Estimativa = iSeq_Estimativa
'            End If                              'AKIO.OKUNO - 18/10/2012
            Set oEstimativa = cDados_Estimativa(1)
            For Each oEstimativa In cDados_Estimativa
                If oEstimativa.Seq_Estimativa = txtSeq_Estimativa Then
                    'Dados do Frame: Dados
                    If Not IsNull(oEstimativa.Dt_Inicio_Estimativa) Then
                        txtDt_Inicio_Estimativa = Format(oEstimativa.Dt_Inicio_Estimativa, "dd/mm/yyyy")
                    End If
                    If Not IsNull(oEstimativa.Dt_Fim_Estimativa) Then
                        If oEstimativa.Dt_Fim_Estimativa <> "1900-01-01" Then
                            txtDt_Fim_Estimativa = Format(oEstimativa.Dt_Fim_Estimativa, "dd/mm/yyyy")
                        Else
                            txtDt_Fim_Estimativa = ""
                        End If
                    End If
                    If Not IsNull(oEstimativa.Dt_Inclusao) Then
                        txtDt_Inclusao_Estimativa = Format(oEstimativa.Dt_Inclusao, "dd/mm/yyyy")
                    Else
                        txtDt_Inclusao_Estimativa = "__/__/____"
                    End If
                    txtUsuario = oEstimativa.Usuario & ""
                    txtPerc_Re_Seguro_Quota = Format("0" & oEstimativa.Perc_Re_Seguro_Quota, "#0.000")
                    txtPerc_Re_Seguro_Er = Format("0" & oEstimativa.Perc_Re_Seguro_Er, "#0.000")
                    txtNum_Carta_Seguro_Aceito = oEstimativa.Num_Carta_Seguro_Aceito
                    'AKIO.OKUNO - INICIO - 05/10/2012
                    'CONFORME REGRA NO frmConsultaSinistro
                    If (txtNum_Carta_Seguro_Aceito.Text = 0 Or txtNum_Carta_Seguro_Aceito.Text = "") And sOperacaoCosseguro <> "C" Then
                        txtNum_Carta_Seguro_Aceito.Visible = False
                        lblEstimativasPagamentos_Dados(9).Visible = False
                    End If
                    'AKIO.OKUNO - FIM - 05/10/2012
                End If
            Next
    
            Set oEstimativa = Nothing
        End If
        
        'Dados do Frame: Valores
        CarregaDadosGrid_Estimativas 2

        If Not bCarregou_EstimativasPagamentos Then
            CarregaDadosGrid_Estimativas 1
        End If
        
        bCarregou_EstimativasPagamentos = True
    
        MousePointer = vbNormal
        
        bAlterou_Seq_Estimativa = False
        
    End If
    Exit Sub
    Resume
Erro:
    TrataErroGeral "CarregaDadosTela", Me.name
    FinalizarAplicacao

End Sub


Private Sub CarregaDadosGrid_Estimativas(bTipo As Byte)
    Dim rsRecordSet                             As ADODB.Recordset
    Dim sSQL                                    As String
    Dim sLinha                                  As String
    Dim oEstimativa                             As clsEstimativa
    
    On Error GoTo Erro
    
    Select Case bTipo
        Case 1      'Grid 1
            LimpaGrid grdEstimativasPagamentos_Coberturas
'AKIO.OKUNO - #RETIRADA - INICIO - 10/05/2013
'            sSQL = ""
'            sSQL = sSQL & "Select * " & vbCrLf
'            sSQL = sSQL & "  From #EstimativaPagamento_CoberturaAfetada " & vbCrLf
'            sSQL = sSQL & " Order By Tp_Cobertura_Nome"
'            Set rsRecordSet = Conexao_ExecutarSQL(gsSIGLASISTEMA, _
'                                                  glAmbiente_id, _
'                                                  App.Title, _
'                                                  App.FileDescription, _
'                                                  sSQL, _
'                                                  lConexaoLocal, _
'                                                  True)
'
'           With rsRecordSet
'                Do While Not .EOF
'                    sLinha = .Fields!Tp_Cobertura_Nome & vbTab
'                    sLinha = sLinha & Format(.Fields!Estimado, "#,##0.00") & vbTab
'                    sLinha = sLinha & Format(.Fields!Correcao, "#,##0.00") & ""
'                    grdEstimativasPagamentos_Coberturas.AddItem sLinha
'                    .MoveNext
'                Loop
'            End With
            Set rsRecordSet = SelecionaDados_AbaEstimativa_CoberturaAfetada_Local(lConexaoLocal)
        
            CarregaDadosGrid grdEstimativasPagamentos_Coberturas, rsRecordSet
            
            With grdEstimativasPagamentos_Coberturas
                .Cols = 5
                .ColWidth(3) = 0
                .ColWidth(4) = 0
            End With
'AKIO.OKUNO - #RETIRADA - FIM - 10/05/2013
            InicializaModoProcessamentoObjeto grdEstimativasPagamentos_Coberturas, 2
        
        Case 2      'Grid 2
            
            InicializaInterfaceLocal_Grid_Estimativas
            
            If cDados_Estimativa.Count <> 0 Then
                If LenB(Trim(txtSeq_Estimativa)) = 0 Then
                    txtSeq_Estimativa = "1"
                End If
                For Each oEstimativa In cDados_Estimativa
                    With oEstimativa
                        If .Seq_Estimativa = txtSeq_Estimativa Then
                            grdEstimativasPagamentos_Estimativas.TextMatrix(.Item_Val_Estimativa, 1) = Format(.Val_Estimado, "#,##0.00")
                            grdEstimativasPagamentos_Estimativas.TextMatrix(.Item_Val_Estimativa, 2) = Format(.Val_Pago, "#,##0.00")
                            grdEstimativasPagamentos_Estimativas.TextMatrix(.Item_Val_Estimativa, 3) = Format(.Val_Estimado - .Val_Pago, "#,##0.00")
                            grdEstimativasPagamentos_Estimativas.TextMatrix(.Item_Val_Estimativa, 4) = Format(.Val_Resseguro, "#,##0.00")
                            grdEstimativasPagamentos_Estimativas.TextMatrix(.Item_Val_Estimativa, 5) = .Situacao
                        End If
                    End With
                Next
            End If
            
            If LenB(Trim(txtDt_Fim_Estimativa)) <> 0 Then
                grdEstimativasPagamentos_Estimativas.ColWidth(6) = 0
            Else
                grdEstimativasPagamentos_Estimativas.ColWidth(6) = 270
            End If
            
            Set oEstimativa = Nothing

            InicializaModoProcessamentoObjeto grdEstimativasPagamentos_Estimativas, 2
    End Select
     
    Set rsRecordSet = Nothing
    
    Exit Sub
Resume
Erro:
    Call TrataErroGeral("CarregaDadosGrid_Estimativas", Me.Caption)
    Call FinalizarAplicacao
    
End Sub


Private Sub InicializaModoOperacaoLocal()
    On Error GoTo Erro
    
    InicializaModoOperacao Me, bytModoOperacao
        
    Exit Sub
    
Erro:
    TrataErroGeral "InicializaModoOperacao", Me.name
    FinalizarAplicacao

End Sub


 
Private Sub InicializaToolTipTextLocal()
    On Error GoTo Erro

    InicializaToolTipText Me
    
    Exit Sub

Erro:
    TrataErroGeral "InicializaToolTipTextLocal", Me.name
    FinalizarAplicacao

End Sub
    
 
Private Sub SelecionaDados()
    Dim sSQL                                    As String
    
    On Error GoTo Erro
    
    MousePointer = vbHourglass
        
    '-- Estimativa (Valores - Grid inferior)
    sSQL = ""
    
    'sSQL = sSQL & " if isnull(OBJECT_ID('tempdb..#EstimativaPagamento_Estimativa'),0) >0" & vbNewLine
    sSQL = sSQL & " if OBJECT_ID('tempdb..#EstimativaPagamento_Estimativa') IS NOT NULL" & vbNewLine
    sSQL = sSQL & " BEGIN" & vbNewLine
    sSQL = sSQL & "     DROP TABLE #EstimativaPagamento_Estimativa" & vbNewLine
    sSQL = sSQL & " END" & vbNewLine
    
    sSQL = sSQL & "Create table #EstimativaPagamento_Estimativa                                                                                 " & vbNewLine
    sSQL = sSQL & "     ( Dt_Inicio_Estimativa                          SmallDateTime                                                           " & vbNewLine
    sSQL = sSQL & "     , Dt_Fim_Estimativa                             SmallDateTime                                                           " & vbNewLine
    sSQL = sSQL & "     , Item_Val_Estimativa                           TinyInt                                                                 " & vbNewLine
    sSQL = sSQL & "     , Seq_Estimativa                                TinyInt                                                                 " & vbNewLine
    sSQL = sSQL & "     , Val_Estimado                                  Numeric(15,2)                                                           " & vbNewLine
    sSQL = sSQL & "     , Val_Pago                                      Numeric(15,2)                                                           " & vbNewLine
    sSQL = sSQL & "     , Val_Resseguro                                 Numeric(15,2)                                                           " & vbNewLine
    sSQL = sSQL & "     , Situacao                                      Char(1)                                                                 " & vbNewLine
    sSQL = sSQL & "     , Usuario                                       VarChar(20)                                                             " & vbNewLine
    sSQL = sSQL & "     , Num_Carta_Seguro_Aceito                       Int                                                                     " & vbNewLine
    sSQL = sSQL & "     , Perc_Re_Seguro_Quota                          Numeric(9,6)                                                            " & vbNewLine
    sSQL = sSQL & "     , Perc_Re_Seguro_Er                             Numeric(9,6)                                                            " & vbNewLine
    sSQL = sSQL & "     , Tipo                                          Char(1)                                                                 " & vbNewLine
    sSQL = sSQL & "     , Nossa_Parte                                   Numeric(9,6)                                                            " & vbNewLine
    sSQL = sSQL & "     , Dt_Inclusao                                   SmallDateTime                                                           " & vbNewLine
    sSQL = sSQL & "     , CountEstimativa                               Numeric(3)                                                              " & vbNewLine
    sSQL = sSQL & "     , Moeda_ID                                      Numeric(3)                                                              " & vbNewLine           '-- Sinistro_Tb (Estimativa)
    sSQL = sSQL & "     , Moeda_Sigla                                   VarChar(4)                                                              " & vbNewLine           '-- Moeda_Tb (Estimativa)
    sSQL = sSQL & "     , pgto_parcial_co_seguro                        Char(1)                                                                 " & vbNewLine           '-- Sinistro_Tb.pgto_parcial_co_seguro
    'mathayde
    sSQL = sSQL & "     , val_custo_medio                               Numeric(15,2)                                                           "
    sSQL = sSQL & "     , sinistro_id                                   numeric(11)                                                             " & vbNewLine
    sSQL = sSQL & "     )                                                                                                                       " & vbNewLine
    sSQL = sSQL & "Create Index PK_Dt_Inicio_Estimativa             on #EstimativaPagamento_Estimativa (Seq_Estimativa, Dt_Inicio_Estimativa)   " & vbNewLine
    
'+----------------------------------------------------------------------
'| Flow 18313521 - Melhoria de performance Pos-Implanta��o - 05/09/2016
'|
    Call Conexao_ExecutarSQL(gsSIGLASISTEMA, _
                             glAmbiente_id, _
                             App.Title, _
                             App.FileDescription, _
                             sSQL, _
                             lConexaoLocal, _
                             False)
    
    sSQL = "SET NOCOUNT ON   EXEC SEGS13138_SPS "
    
'    sSQL = sSQL & "Insert into #EstimativaPagamento_Estimativa                                                                                  " & vbNewLine
'    sSQL = sSQL & "     ( Dt_Inicio_Estimativa                                                                                                  " & vbNewLine
'    sSQL = sSQL & "     , Dt_Fim_Estimativa                                                                                                     " & vbNewLine
'    sSQL = sSQL & "     , Item_Val_Estimativa                                                                                                   " & vbNewLine
'    sSQL = sSQL & "     , Seq_Estimativa                                                                                                        " & vbNewLine
'    sSQL = sSQL & "     , Val_Estimado                                                                                                          " & vbNewLine
'    sSQL = sSQL & "     , Val_Pago                                                                                                              " & vbNewLine
'    sSQL = sSQL & "     , Val_Resseguro                                                                                                         " & vbNewLine
'    sSQL = sSQL & "     , Situacao                                                                                                              " & vbNewLine
'    sSQL = sSQL & "     , Usuario                                                                                                               " & vbNewLine
'    sSQL = sSQL & "     , Num_Carta_Seguro_Aceito                                                                                               " & vbNewLine
'    sSQL = sSQL & "     , Perc_Re_Seguro_Quota                                                                                                  " & vbNewLine
'    sSQL = sSQL & "     , Perc_Re_Seguro_Er                                                                                                     " & vbNewLine
'    sSQL = sSQL & "     , Tipo                                                                                                                  " & vbNewLine
'    sSQL = sSQL & "     , Nossa_Parte                                                                                                           " & vbNewLine
'    sSQL = sSQL & "     , Dt_Inclusao                                                                                                           " & vbNewLine
'    sSQL = sSQL & "     , Moeda_ID                                                                                                              " & vbNewLine
'    sSQL = sSQL & "     , pgto_parcial_co_seguro                                                                                                " & vbNewLine
'    'mathayde
'    sSQL = sSQL & "     , sinistro_id                                                                                                           " & vbNewLine
'    sSQL = sSQL & "     )                                                                                                                       " & vbNewLine
'    sSQL = sSQL & "Select Dt_Inicio_Estimativa                                                                                                  " & vbNewLine
'    sSQL = sSQL & "     , Dt_Fim_Estimativa                                                                                                     " & vbNewLine
'    sSQL = sSQL & "     , Item_Val_Estimativa                                                                                                   " & vbNewLine
'    sSQL = sSQL & "     , Seq_Estimativa                                                                                                        " & vbNewLine
'    sSQL = sSQL & "     , Val_Estimado                                  = IsNull(Val_Estimado,0)                                                " & vbNewLine
'    sSQL = sSQL & "     , Val_Pago                                      = IsNull(Val_Pago,0)                                                    " & vbNewLine
'    sSQL = sSQL & "     , Val_Resseguro                                 = IsNull(Val_Resseguro,0)                                               " & vbNewLine
'    sSQL = sSQL & "     , Situacao                                      = IsNull(Sinistro_Estimativa_Tb.Situacao, '')                           " & vbNewLine
'    sSQL = sSQL & "     , Sinistro_Estimativa_Tb.Usuario                                                                                        " & vbNewLine
'    sSQL = sSQL & "     , Num_Carta_Seguro_Aceito                       = Isnull(Num_Carta_Seguro_Aceito, '')                                   " & vbNewLine
'    sSQL = sSQL & "     , Perc_Re_Seguro_Quota                          = IsNull(Perc_Re_Seguro_Quota,0)                                        " & vbNewLine
'    sSQL = sSQL & "     , Perc_Re_Seguro_Er                             = IsNull(Perc_Re_Seguro_Er,0)                                           " & vbNewLine
'    sSQL = sSQL & "     , Tipo                                          = 'E'                                                                   " & vbNewLine
'    sSQL = sSQL & "     , Nossa_Parte                                   = 0                                                                     " & vbNewLine
'    sSQL = sSQL & "     , Sinistro_Estimativa_Tb.Dt_Inclusao                                                                                    " & vbNewLine
'    sSQL = sSQL & "     , Sinistro_Tb.Moeda_ID                                                                                                  " & vbNewLine
'    sSQL = sSQL & "     , Sinistro_Tb.pgto_parcial_co_seguro                                                                                    " & vbNewLine
'    'mathayde
'    sSQL = sSQL & "     , Sinistro_Tb.sinistro_id                                                                                               " & vbNewLine
'    sSQL = sSQL & " From Seguros_Db.Dbo.Sinistro_Estimativa_Tb          Sinistro_Estimativa_Tb  WITH (NOLOCK)                                          " & vbNewLine
''    sSQL = sSQL & " Join #Sinistro_Principal                                                                                                    " & vbNewLine
'    sSQL = sSQL & " Left Join #Sinistro_Principal                                                                                                    " & vbNewLine
'    sSQL = sSQL & "   on #Sinistro_Principal.Sinistro_ID                = Sinistro_Estimativa_Tb.Sinistro_ID                                    " & vbNewLine
'    sSQL = sSQL & " Join Seguros_Db.Dbo.Sinistro_Tb                     Sinistro_Tb  WITH (NOLOCK)                                                     " & vbNewLine
'    sSQL = sSQL & "   on Sinistro_Tb.Sinistro_ID                        = #Sinistro_Principal.Sinistro_ID                                       " & vbNewLine
''    sSQL = sSQL & "Order By Seq_Estimativa                                                                                                      " & vbNewLine
''    sSQL = sSQL & " , Item_Val_Estimativa                                                                                                       " & vbNewLine
'    sSQL = sSQL & "Order By Seq_Estimativa                              " & vbNewLine   'AKIO.OKUNO - 14/11/2012
'    sSQL = sSQL & " , Item_Val_Estimativa                               " & vbNewLine   'AKIO.OKUNO - 14/11/2012
'
'    '-- Nossa Parte                                                                                                               " & vbNewLine
'    'sSQL = sSQL & " if isnull(OBJECT_ID('tempdb..#Nossa_Parte'),0) >0" & vbNewLine
'    sSQL = sSQL & " if OBJECT_ID('tempdb..#Nossa_Parte') IS NOT NULL " & vbNewLine
'    sSQL = sSQL & " BEGIN" & vbNewLine
'    sSQL = sSQL & "     DROP TABLE #Nossa_Parte" & vbNewLine
'    sSQL = sSQL & " END" & vbNewLine
'    sSQL = sSQL & "Select Nossa_Parte                                   = 100 - IsNull(Sum(Perc_Participacao),0)                                " & vbNewLine
'    sSQL = sSQL & "  Into #Nossa_Parte                                                                                                          " & vbNewLine
'    sSQL = sSQL & "  From Seguros_Db.Dbo.Co_Seguro_Repassado_Tb         Co_Seguro_Repassado_Tb  WITH (NOLOCK)                                          " & vbNewLine
'    sSQL = sSQL & "  Join #Sinistro_Principal                                                                                                   " & vbNewLine
'    sSQL = sSQL & "    on #Sinistro_Principal.Sucursal_Seguradora_ID    = Co_Seguro_Repassado_Tb.Sucursal_Seguradora_ID                         " & vbNewLine
'    sSQL = sSQL & "   and #Sinistro_Principal.Seguradora_Cod_Susep      = Co_Seguro_Repassado_Tb.Seguradora_Cod_Susep                           " & vbNewLine
'    sSQL = sSQL & "   and #Sinistro_Principal.Apolice_ID                = Co_Seguro_Repassado_Tb.Apolice_ID                                     " & vbNewLine
'    sSQL = sSQL & "   and #Sinistro_Principal.Ramo_ID                   = Co_Seguro_Repassado_Tb.Ramo_ID                                        " & vbNewLine
'    sSQL = sSQL & " Where Dt_Inicio_Participacao                        <= #Sinistro_Principal.Dt_Ocorrencia_Sinistro                           " & vbNewLine
'    sSQL = sSQL & "   and (Dt_Fim_Participacao                          >= #Sinistro_Principal.Dt_Ocorrencia_Sinistro                           " & vbNewLine
'    sSQL = sSQL & "        or Dt_Fim_Participacao                       is null)                                                                " & vbNewLine
'
'    sSQL = sSQL & "Update #EstimativaPagamento_Estimativa                                                                                       " & vbNewLine
'    sSQL = sSQL & "   Set Nossa_Parte                                   = #Nossa_Parte.Nossa_Parte                                              " & vbNewLine
'    sSQL = sSQL & "  From #Nossa_Parte                                                                                                          " & vbNewLine
'    '--Quantidade de Estimativas                                                                                                  " & vbNewLine
'    sSQL = sSQL & "UPdate #EstimativaPagamento_Estimativa                                                                                       " & vbNewLine
'    sSQL = sSQL & "   Set CountEstimativa                               = (Select max(Seq_Estimativa) From #EstimativaPagamento_Estimativa)     " & vbNewLine

'| Flow 18313521 - Melhoria de performance Pos-Implanta��o - 05/09/2016
'+----------------------------------------------------------------------
    Call Conexao_ExecutarSQL(gsSIGLASISTEMA, _
                             glAmbiente_id, _
                             App.Title, _
                             App.FileDescription, _
                             sSQL, _
                             lConexaoLocal, _
                             False)
    
    
    '-->> Dados do Frame: Valores                                                                                                 " & vbNewLine
    '-- Cobertura Afetada - Valores (Grid Superior)                                                                               " & vbNewLine
'    SelecionaDados_AbaEstimativa_CoberturaAfetada_Local    'AKIO.OKUNO - #RETIRADA - 10/05/2013
    
    sSQL = ""
    'sSQL = sSQL & " if isnull(OBJECT_ID('tempdb..#EstimativaPagamento_Estimativa_Atual'),0) >0" & vbNewLine
    sSQL = sSQL & " if OBJECT_ID('tempdb..#EstimativaPagamento_Estimativa_Atual') IS NOT NULL " & vbNewLine
    sSQL = sSQL & " BEGIN" & vbNewLine
    sSQL = sSQL & "     DROP TABLE #EstimativaPagamento_Estimativa_Atual" & vbNewLine
    sSQL = sSQL & " END" & vbNewLine
    
    '-- Itens de Estimativa (Grid Inferior)                                                                                       " & vbNewLine
    sSQL = sSQL & "Select *                                                                                                                     " & vbNewLine
    sSQL = sSQL & "  Into #EstimativaPagamento_Estimativa_Atual                                                                                 " & vbNewLine
    sSQL = sSQL & "  From #EstimativaPagamento_Estimativa                                                                                       " & vbNewLine
    sSQL = sSQL & " Where Dt_Fim_Estimativa                         is  not null                                                                " & vbNewLine

'AKIO.OKUNO - #RETIRADA - INICIO - 02/05/2013
'    sSQL = sSQL & " if isnull(OBJECT_ID('tempdb..#EstimativaPagamento_Voucher'),0) >0" & vbNewLine
'    sSQL = sSQL & " BEGIN" & vbNewLine
'    sSQL = sSQL & "     DROP TABLE #EstimativaPagamento_Voucher" & vbNewLine
'    sSQL = sSQL & " END" & vbNewLine
'
'    'Dados do Subframe: Voucher
'    sSQL = sSQL & "Create Table #EstimativaPagamento_Voucher                                                                                    " & vbNewLine
'    sSQL = sSQL & "     ( Dt_Acerto_Contas_Sinistro                     SmallDateTime                                                           " & vbNewLine
'    sSQL = sSQL & "     , Voucher_ID                                    Int                                                                     " & vbNewLine
'    sSQL = sSQL & "     , Beneficiario_ID                               TinyInt                                                                 " & vbNewLine
'    sSQL = sSQL & "     , Sinistro_Benef_Nome                           VarChar(60)                                                             " & vbNewLine
'    sSQL = sSQL & "     , Tot_Acerto                                    Numeric(15,2)                                                           " & vbNewLine
'    sSQL = sSQL & "     , Tot_Correcao                                  Numeric(15,2)                                                           " & vbNewLine
'    sSQL = sSQL & "     , Dt_Remessa                                    SmallDateTime                                                           " & vbNewLine
'    sSQL = sSQL & "     , Remessa_ID                                    Int                                                                     " & vbNewLine
'    sSQL = sSQL & "     , Estornado                                     Char(1)                                                                 " & vbNewLine
'    sSQL = sSQL & "     , Item_Val_Estimativa                           TinyInt                                                                 " & vbNewLine
'    sSQL = sSQL & "     , Forma_Pgto_Nome                               VarChar(60)                                                             " & vbNewLine
'    sSQL = sSQL & "     , Num_Nota_Fiscal                               Int                                                                     " & vbNewLine
'    sSQL = sSQL & "     , Serie_Nota_Fiscal                             VarChar(5)                                                              " & vbNewLine
'    sSQL = sSQL & "     )                                                                                                                       " & vbNewLine
'    sSQL = sSQL & "Create Index PK_Voucher_ID                           on #EstimativaPagamento_Voucher (Voucher_ID)                            " & vbNewLine
'    sSQL = sSQL & "Insert into #EstimativaPagamento_Voucher                                                                                     " & vbNewLine
'    sSQL = sSQL & "     ( Dt_Acerto_Contas_Sinistro                                                                                             " & vbNewLine
'    sSQL = sSQL & "     , Voucher_ID                                                                                                            " & vbNewLine
'    sSQL = sSQL & "     , Beneficiario_ID                                                                                                       " & vbNewLine
'    sSQL = sSQL & "     , Sinistro_Benef_Nome                                                                                                   " & vbNewLine
'    sSQL = sSQL & "     , Tot_Acerto                                                                                                            " & vbNewLine
'    sSQL = sSQL & "     , Tot_Correcao                                                                                                          " & vbNewLine
'    sSQL = sSQL & "     , Dt_Remessa                                                                                                            " & vbNewLine
'    sSQL = sSQL & "     , Remessa_ID                                                                                                            " & vbNewLine
'    sSQL = sSQL & "     , Estornado                                                                                                             " & vbNewLine
'    sSQL = sSQL & "     , Item_Val_Estimativa                                                                                                   " & vbNewLine
'    sSQL = sSQL & "     , Forma_Pgto_Nome                                                                                                       " & vbNewLine
'    sSQL = sSQL & "     , Num_Nota_Fiscal                                                                                                       " & vbNewLine
'    sSQL = sSQL & "     , Serie_Nota_Fiscal                                                                                                     " & vbNewLine
'    sSQL = sSQL & "     )                                                                                                                       " & vbNewLine
'    sSQL = sSQL & "Select Pgto_Sinistro_Tb.Dt_Acerto_Contas_Sinistro --Dt_Acerto                                                                " & vbNewLine
'    sSQL = sSQL & "     , Ps_Acerto_Pagamento_Tb.Voucher_ID                                                                                     " & vbNewLine
'    sSQL = sSQL & "     , Pgto_Sinistro_Tb.Beneficiario_ID                                                                                      " & vbNewLine
'    sSQL = sSQL & "     , Sinistro_Benef_Tb.Nome                                                                                                " & vbNewLine
'    sSQL = sSQL & "     , Tot_Acerto                                    = Sum(Pgto_Sinistro_Tb.Val_Acerto)                                      " & vbNewLine
'    sSQL = sSQL & "     , Tot_Correcao                                  = 0                                                                     " & vbNewLine
'    sSQL = sSQL & "     , Ps_Remessa_Tb.Dt_Remessa                      -- Dt_Geracao                                                           " & vbNewLine
'    sSQL = sSQL & "     , Ps_Remessa_Tb.Remessa_ID                                                                                              " & vbNewLine
'    sSQL = sSQL & "     , Ps_Voucher_Tb.Estornado                                                                                               " & vbNewLine
'    sSQL = sSQL & "     , Pgto_Sinistro_Tb.Item_Val_Estimativa                                                                                  " & vbNewLine
'    sSQL = sSQL & "     , Forma_Pgto_Tb.Nome                                                                                                    " & vbNewLine
'    sSQL = sSQL & "     , Pgto_Sinistro_Tb.Num_Nota_Fiscal                                                                                      " & vbNewLine
'    sSQL = sSQL & "     , Pgto_Sinistro_Tb.Serie_Nota_Fiscal                                                                                    " & vbNewLine
'    sSQL = sSQL & "  From Seguros_Db.Dbo.Pgto_Sinistro_Tb               Pgto_Sinistro_Tb  WITH (NOLOCK)                                                " & vbNewLine
'    sSQL = sSQL & "  Join #Sinistro_Principal                                                                                                   " & vbNewLine
'    sSQL = sSQL & "    on #Sinistro_Principal.Sinistro_ID               = Pgto_Sinistro_Tb.Sinistro_ID                                          " & vbNewLine
'    sSQL = sSQL & "  Join Seguros_Db.Dbo.Ps_Acerto_Pagamento_Tb         Ps_Acerto_Pagamento_Tb  WITH (NOLOCK)                                          " & vbNewLine
'    sSQL = sSQL & " on Ps_Acerto_Pagamento_Tb.Acerto_ID                 = Pgto_Sinistro_Tb.Acerto_ID                                            " & vbNewLine
'    sSQL = sSQL & "  Join Seguros_Db.Dbo.Ps_Acerto_Tb                   Ps_Acerto_Tb  WITH (NOLOCK)                                                    " & vbNewLine
'    sSQL = sSQL & "    on Ps_Acerto_Tb.Acerto_ID                        = Ps_Acerto_Pagamento_Tb.Acerto_ID                                      " & vbNewLine
'    sSQL = sSQL & "  Join Seguros_Db.Dbo.Ps_Remessa_Voucher_Tb          Ps_Remessa_Voucher_Tb  WITH (NOLOCK)                                           " & vbNewLine
'    sSQL = sSQL & " on Ps_Remessa_Voucher_Tb.Voucher_ID                 = Ps_Acerto_Pagamento_Tb.Voucher_ID                                     " & vbNewLine
'    sSQL = sSQL & "  Join Seguros_Db.Dbo.Ps_Remessa_Tb                  Ps_Remessa_Tb  WITH (NOLOCK)                                                   " & vbNewLine
'    sSQL = sSQL & " on Ps_Remessa_Tb.Remessa_ID                         = Ps_Remessa_Voucher_Tb.Remessa_ID                                      " & vbNewLine
'    sSQL = sSQL & "   and Ps_Remessa_Tb.Cod_Sistema_PS                  = Ps_Remessa_Voucher_Tb.Cod_Sistema_PS                                  " & vbNewLine
'    sSQL = sSQL & "   and Ps_Remessa_Voucher_Tb.Cod_Sistema_PS          = 'AP'                                                                  " & vbNewLine
'    sSQL = sSQL & "   and Ps_Remessa_Voucher_Tb.Cod_Origem              = 'SN'                                                                  " & vbNewLine
'    sSQL = sSQL & "  Join Seguros_Db.Dbo.Ps_Voucher_Tb                  Ps_Voucher_Tb  WITH (NOLOCK)                                                   " & vbNewLine
'    sSQL = sSQL & " on Ps_Voucher_Tb.Voucher_ID                         = Ps_Remessa_Voucher_Tb.Voucher_ID                                      " & vbNewLine
'    sSQL = sSQL & "   and Ps_Voucher_Tb.Cod_Origem                      = Ps_Remessa_Voucher_Tb.Cod_Origem                                      " & vbNewLine
'    sSQL = sSQL & "  Join Seguros_Db.Dbo.Sinistro_Benef_Tb              Sinistro_Benef_Tb  WITH (NOLOCK)                                               " & vbNewLine
'    sSQL = sSQL & " on Sinistro_Benef_Tb.Sinistro_ID                    = Pgto_Sinistro_Tb.Sinistro_ID                                          " & vbNewLine
'    sSQL = sSQL & "   and Sinistro_Benef_Tb.Beneficiario_ID             = Pgto_Sinistro_Tb.Beneficiario_ID                                      " & vbNewLine
'    sSQL = sSQL & "  Join Seguros_Db.Dbo.Forma_Pgto_Tb                  Forma_Pgto_Tb  WITH (NOLOCK)                                                   " & vbNewLine
'    sSQL = sSQL & "    on Forma_Pgto_Tb.Forma_Pgto_ID                   = Pgto_Sinistro_Tb.Forma_Pgto_ID                                        " & vbNewLine
'    sSQL = sSQL & " Where Pgto_Sinistro_Tb.Tp_Operacao                  = 'd'                                                                   " & vbNewLine
'    sSQL = sSQL & " Group By Pgto_Sinistro_Tb.Dt_Acerto_Contas_Sinistro                                                                         " & vbNewLine
'    sSQL = sSQL & "     , Ps_Acerto_Pagamento_Tb.Voucher_ID                                                                                     " & vbNewLine
'    sSQL = sSQL & "     , Pgto_Sinistro_Tb.Beneficiario_Id                                                                                      " & vbNewLine
'    sSQL = sSQL & "     , Sinistro_benef_Tb.Nome                                                                                                " & vbNewLine
'    sSQL = sSQL & "     , Ps_Remessa_Tb.Dt_Remessa                                                                                              " & vbNewLine
'    sSQL = sSQL & "     , Ps_Remessa_Tb.Remessa_ID                                                                                              " & vbNewLine
'    sSQL = sSQL & "     , Ps_Voucher_Tb.Estornado                                                                                               " & vbNewLine
'    sSQL = sSQL & "     , Pgto_Sinistro_tb.Item_Val_Estimativa                                                                                  " & vbNewLine
'    sSQL = sSQL & "     , Forma_Pgto_Tb.Nome                                                                                                    " & vbNewLine
'    sSQL = sSQL & "     , Pgto_Sinistro_Tb.Num_Nota_Fiscal                                                                                      " & vbNewLine
'    sSQL = sSQL & "     , Pgto_Sinistro_Tb.Serie_Nota_Fiscal                                                                                    " & vbNewLine
    
'    sSQL = sSQL & " if isnull(OBJECT_ID('tempdb..#EstimativaPagamento_Beneficiario'),0) >0" & vbNewLine
'    sSQL = sSQL & " BEGIN" & vbNewLine
'    sSQL = sSQL & "     DROP TABLE #EstimativaPagamento_Beneficiario" & vbNewLine
'    sSQL = sSQL & " END" & vbNewLine
'
'    '-->> Dados do Subframe: Benefici�rio                                                                                                       " & vbNewLine
'    sSQL = sSQL & "Select Sinistro_Benef_Tb.Sinistro_ID                                                                                         " & vbNewLine
'    sSQL = sSQL & "     , Sinistro_Benef_Tb.Beneficiario_ID                                                                                     " & vbNewLine
'    sSQL = sSQL & "     , Sinistro_Benef_Tb.Nome                                                                                                " & vbNewLine
'    sSQL = sSQL & "     , Sinistro_Benef_Tb.Dt_Nascimento                                                                                       " & vbNewLine
'    sSQL = sSQL & "     , Sinistro_Benef_Tb.Nome_Responsavel                                                                                    " & vbNewLine
'    sSQL = sSQL & "     , Sinistro_Benef_Tb.Tp_Responsavel                                                                                      " & vbNewLine
'    sSQL = sSQL & "     , Sinistro_Benef_Tb.Banco                                                                                               " & vbNewLine
'    sSQL = sSQL & "     , Sinistro_Benef_Tb.Agencia                                                                                             " & vbNewLine
'    sSQL = sSQL & "     , Sinistro_Benef_Tb.Agencia_Dv                                                                                          " & vbNewLine
'    sSQL = sSQL & "     , Sinistro_Benef_Tb.Conta_Corrente                                                                                      " & vbNewLine
'    sSQL = sSQL & "     , Sinistro_Benef_Tb.Conta_Corrente_DV                                                                                   " & vbNewLine
'    sSQL = sSQL & "     , Sinistro_Benef_Tb.Tp_Documento_ID                                                                                     " & vbNewLine
'    sSQL = sSQL & "     , Tp_Documento_Nome                             = ''                                                                    " & vbNewLine
'    sSQL = sSQL & "     , Sinistro_Benef_Tb.Orgao                                                                                               " & vbNewLine
'    sSQL = sSQL & "     , Sinistro_Benef_Tb.Numero                                                                                              " & vbNewLine
'    sSQL = sSQL & "     , Sinistro_Benef_Tb.Serie                                                                                               " & vbNewLine
'    sSQL = sSQL & "     , Sinistro_Benef_Tb.Complemento                                                                                         " & vbNewLine
'    sSQL = sSQL & "     , Sinistro_Benef_Tb.Sexo                                                                                                " & vbNewLine
'    sSQL = sSQL & "     , Sexo_Descricao                                = Case When Upper(Sinistro_Benef_Tb.Sexo) = 'M'                         " & vbNewLine
'    sSQL = sSQL & "                                                        Then 'Masc.'                                                         " & vbNewLine
'    sSQL = sSQL & "                                                        Else Case When Upper(Sinistro_Benef_Tb.Sexo) = 'F'                   " & vbNewLine
'    sSQL = sSQL & "                                                                  Then 'Fem.'                                                " & vbNewLine
'    sSQL = sSQL & "                                                                  Else ' '                                                   " & vbNewLine
'    sSQL = sSQL & "                                                             end                                                             " & vbNewLine
'    sSQL = sSQL & "                                                    end                                                                      " & vbNewLine
'    sSQL = sSQL & "     , Sinistro_Benef_Tb.CGC                                                                                                 " & vbNewLine
'    sSQL = sSQL & "     , Sinistro_Benef_Tb.CPF                                                                                                 " & vbNewLine
'    sSQL = sSQL & "     , Sinistro_Benef_Tb.Usuario                                                                                             " & vbNewLine
'    'sSQL = sSQL & "     , Cong_Cod_Susep                                = ''                                                                    " & vbNewLine
'    sSQL = sSQL & "     , Cong_Cod_Susep                                = 0                                                                     " & vbNewLine   'cristovao.rodrigues 29/10/2012
'
'    sSQL = sSQL & "     , Sinistro_Benef_Tb.Variacao_Poupanca                                                                                   " & vbNewLine
'    sSQL = sSQL & "     , Sinistro_Benef_Tb.Id_Beneficiario_BB                                                                                  " & vbNewLine
'    sSQL = sSQL & "     , Sinistro_Benef_Tb.EMail                                                                                               " & vbNewLine
'    sSQL = sSQL & "  Into #EstimativaPagamento_Beneficiario                                                                                     " & vbNewLine
'    sSQL = sSQL & "  From Seguros_Db.Dbo.Sinistro_Benef_Tb              Sinistro_Benef_Tb  WITH (NOLOCK)                                               " & vbNewLine
'    sSQL = sSQL & "  Join #Sinistro_Principal                                                                                                   " & vbNewLine
'    sSQL = sSQL & "    on #Sinistro_Principal.Sinistro_ID               = Sinistro_Benef_Tb.Sinistro_ID                                         " & vbNewLine
'    sSQL = sSQL & "  Left Join Seguros_Db.Dbo.Sinistro_Benef_Prestador_Tb  Sinistro_Benef_Prestador_Tb  WITH (NOLOCK)                                  " & vbNewLine
'    sSQL = sSQL & "    on Sinistro_Benef_Tb.Sinistro_ID                 = Sinistro_Benef_Prestador_Tb.Sinistro_ID                               " & vbNewLine
'    sSQL = sSQL & "   and Sinistro_Benef_Tb.Beneficiario_ID             = Sinistro_Benef_Prestador_Tb.Beneficiario_ID                           " & vbNewLine
'    sSQL = sSQL & "  Left Join Seguros_Db.Dbo.Sinistro_Benef_Regulador_Tb  Sinistro_Benef_Regulador_Tb  WITH (NOLOCK)                                  " & vbNewLine
'    sSQL = sSQL & "    on Sinistro_Benef_Tb.Sinistro_ID                 = Sinistro_Benef_Regulador_Tb.Sinistro_ID                               " & vbNewLine
'    sSQL = sSQL & "   and Sinistro_Benef_Tb.Beneficiario_ID             = Sinistro_Benef_Regulador_Tb.Beneficiario_ID                           " & vbNewLine
'    sSQL = sSQL & " Where Sinistro_Benef_Regulador_Tb.Regulador_ID      is null                                                                 " & vbNewLine
'    sSQL = sSQL & "   and Sinistro_Benef_Prestador_Tb.Prestador_ID      is null                                                                 " & vbNewLine
'    sSQL = sSQL & "                                                                                                                             " & vbNewLine
'    sSQL = sSQL & "Union All                                                                                                                    " & vbNewLine
'    sSQL = sSQL & "Select Sinistro_Benef_Temp_Tb.Sinistro_ID                                                                                    " & vbNewLine
'    sSQL = sSQL & "     , Sinistro_Benef_Temp_Tb.Beneficiario_ID                                                                                " & vbNewLine
'    sSQL = sSQL & "     , Sinistro_Benef_Temp_Tb.Nome                                                                                           " & vbNewLine
'    sSQL = sSQL & "     , Sinistro_Benef_Temp_Tb.Dt_Nascimento                                                                                  " & vbNewLine
'    sSQL = sSQL & "     , Sinistro_Benef_Temp_Tb.Nome_Responsavel                                                                               " & vbNewLine
'    sSQL = sSQL & "     , Sinistro_Benef_Temp_Tb.Tp_Responsavel                                                                                 " & vbNewLine
'    sSQL = sSQL & "     , Sinistro_Benef_Temp_Tb.Banco                                                                                          " & vbNewLine
'    sSQL = sSQL & "     , Sinistro_Benef_Temp_Tb.Agencia                                                                                        " & vbNewLine
'    sSQL = sSQL & "     , Sinistro_Benef_Temp_Tb.Agencia_Dv                                                                                     " & vbNewLine
'    sSQL = sSQL & "     , Sinistro_Benef_Temp_Tb.Conta_Corrente                                                                                 " & vbNewLine
'    sSQL = sSQL & "     , Sinistro_Benef_Temp_Tb.Conta_Corrente_DV                                                                              " & vbNewLine
'    sSQL = sSQL & "     , Sinistro_Benef_Temp_Tb.Tp_Documento_ID                                                                                " & vbNewLine
'    sSQL = sSQL & "     , Tp_Documento_Nome                             = convert(varchar(60),'')                                               " & vbNewLine
'    sSQL = sSQL & "     , Sinistro_Benef_Temp_Tb.Orgao                                                                                          " & vbNewLine
'    sSQL = sSQL & "     , Sinistro_Benef_Temp_Tb.Numero                                                                                         " & vbNewLine
'    sSQL = sSQL & "     , Sinistro_Benef_Temp_Tb.Serie                                                                                          " & vbNewLine
'    sSQL = sSQL & "     , Sinistro_Benef_Temp_Tb.Complemento                                                                                    " & vbNewLine
'    sSQL = sSQL & "     , Sinistro_Benef_Temp_Tb.Sexo                                                                                           " & vbNewLine
'    sSQL = sSQL & "     , Sexo_Descricao                                = Case When Sinistro_Benef_Temp_Tb.Sexo = 'M'                           " & vbNewLine
'    sSQL = sSQL & "                                                        Then 'Masc.'                                                         " & vbNewLine
'    sSQL = sSQL & "                                                        Else Case When Sinistro_Benef_Temp_Tb.Sexo = 'F'                     " & vbNewLine
'    sSQL = sSQL & "                                                                  Then 'Fem.'                                                " & vbNewLine
'    sSQL = sSQL & "                                                                  Else ' '                                                   " & vbNewLine
'    sSQL = sSQL & "                                                             end                                                             " & vbNewLine
'    sSQL = sSQL & "                                                    end                                                                      " & vbNewLine
'    sSQL = sSQL & "     , Sinistro_Benef_Temp_Tb.CGC                                                                                            " & vbNewLine
'    sSQL = sSQL & "     , Sinistro_Benef_Temp_Tb.CPF                                                                                            " & vbNewLine
'    sSQL = sSQL & "     , Sinistro_Benef_Temp_Tb.Usuario                                                                                        " & vbNewLine
'    sSQL = sSQL & "     , Cong_Cod_Susep                                = ''                                                                    " & vbNewLine
'    sSQL = sSQL & "     , Sinistro_Benef_Temp_Tb.Variacao_Poupanca                                                                              " & vbNewLine
'    sSQL = sSQL & "     , Id_Beneficiario_BB                            = 'BTTB'                                                                " & vbNewLine
'    sSQL = sSQL & "     , Email                                         = ''                                                                    " & vbNewLine
'    sSQL = sSQL & "  From Seguros_Db.Dbo.Sinistro_Benef_Temp_Tb         Sinistro_Benef_Temp_Tb  WITH (NOLOCK)                                          " & vbNewLine
'    sSQL = sSQL & "  Join #Sinistro_Principal                                                                                                   " & vbNewLine
'    sSQL = sSQL & "    on #Sinistro_Principal.Sinistro_ID               = Sinistro_Benef_Temp_Tb.Sinistro_ID                                    " & vbNewLine
'    sSQL = sSQL & "                                                                                                                             " & vbNewLine
'    sSQL = sSQL & "Update #EstimativaPagamento_Beneficiario                                                                                     " & vbNewLine
'    sSQL = sSQL & "   Set Tp_Documento_Nome                             = Tp_Documento_Tb.Nome                                                  " & vbNewLine
'    sSQL = sSQL & "  From Seguros_Db.Dbo.Tp_Documento_Tb                Tp_Documento_Tb  WITH (NOLOCK)                                                 " & vbNewLine
'    sSQL = sSQL & "  Join #EstimativaPagamento_Beneficiario                                                                                     " & vbNewLine
'    sSQL = sSQL & "    on #EstimativaPagamento_Beneficiario.TP_Documento_ID     = Tp_Documento_Tb.Tp_Documento_ID                               " & vbNewLine
'    sSQL = sSQL & "                                                                                                                             " & vbNewLine
'    sSQL = sSQL & "Update #EstimativaPagamento_Beneficiario                                                                                     " & vbNewLine
'    sSQL = sSQL & "   Set Cong_Cod_Susep                                = isnull(Sinistro_Benef_Congenere_Tb.Cong_Cod_Susep,0)                  " & vbNewLine
'    sSQL = sSQL & "  From #EstimativaPagamento_Beneficiario                                                                                     " & vbNewLine
'    sSQL = sSQL & "  Left Join Seguros_Db.Dbo.Sinistro_Benef_Congenere_Tb Sinistro_Benef_Congenere_Tb WITH (NOLOCK)                                    " & vbNewLine
'    sSQL = sSQL & "    on Sinistro_Benef_Congenere_Tb.Sinistro_ID       = #EstimativaPagamento_Beneficiario.Sinistro_ID                         " & vbNewLine
'    sSQL = sSQL & "   and Sinistro_Benef_Congenere_Tb.Beneficiario_ID   = #EstimativaPagamento_Beneficiario.Beneficiario_ID                     " & vbNewLine
'    sSQL = sSQL & "                                                                                                                             " & vbNewLine
'AKIO.OKUNO - #RETIRADA - FIM - 02/05/2013
    
    '-- Moeda_Tb                                                                                                                                " & vbNewLine
    sSQL = sSQL & "Update #EstimativaPagamento_Estimativa                                                                                       " & vbNewLine
    sSQL = sSQL & "   Set Moeda_Sigla                                   = Moeda_Tb.Sigla                                                        " & vbNewLine
    sSQL = sSQL & "  From Seguros_Db.Dbo.Moeda_Tb                       Moeda_Tb  WITH (NOLOCK)                                                        " & vbNewLine
    sSQL = sSQL & "  Join #EstimativaPagamento_Estimativa                                                                                       " & vbNewLine
    sSQL = sSQL & " on #EstimativaPagamento_Estimativa.Moeda_ID         = Moeda_Tb.Moeda_Id                                                     " & vbNewLine
         
    sSQL = sSQL & "Update #EstimativaPagamento_Estimativa "
    sSQL = sSQL & "   Set Moeda_ID                                      = isnull(Proposta_Fechada_Tb.Seguro_Moeda_ID, Proposta_Adesao_Tb.Seguro_Moeda_ID)   " & vbNewLine
    sSQL = sSQL & "     , Moeda_Sigla                                   = Moeda_Tb.Sigla"
    sSQL = sSQL & "  From #EstimativaPagamento_Estimativa                                                                                                    " & vbNewLine
    sSQL = sSQL & "  Join #Sinistro_Principal"
    sSQL = sSQL & "    on #Sinistro_Principal.Sinistro_ID               = #EstimativaPagamento_Estimativa.Sinistro_ID"
    sSQL = sSQL & "  Left Join Seguros_Db.Dbo.Proposta_Fechada_Tb       Proposta_Fechada_Tb  WITH (NOLOCK)                                                         " & vbNewLine
    sSQL = sSQL & "    on Proposta_Fechada_Tb.Proposta_ID               = #Sinistro_Principal.Proposta_ID                                                   " & vbNewLine
    sSQL = sSQL & "  Left Join Seguros_Db.Dbo.Proposta_Adesao_Tb        Proposta_Adesao_Tb  WITH (NOLOCK)                                                          " & vbNewLine
    sSQL = sSQL & "    on Proposta_Adesao_Tb.Proposta_ID                = #Sinistro_Principal.Proposta_ID                                                   " & vbNewLine
    sSQL = sSQL & "  Join Seguros_Db.Dbo.Moeda_Tb                       Moeda_Tb  WITH (NOLOCK)                                                                    " & vbNewLine
    sSQL = sSQL & "    on Moeda_Tb.Moeda_ID                             = isnull(Proposta_Fechada_Tb.Seguro_Moeda_ID, Proposta_Adesao_Tb.Seguro_Moeda_ID)   " & vbNewLine
    sSQL = sSQL & " Where #EstimativaPagamento_Estimativa.Moeda_ID is null"
     
         
         
    Call Conexao_ExecutarSQL(gsSIGLASISTEMA, _
                             glAmbiente_id, _
                             App.Title, _
                             App.FileDescription, _
                             sSQL, _
                             lConexaoLocal, _
                             False)
    
    
    
    Exit Sub
    
Erro:
    TrataErroGeral "SelecionaDados", Me.Caption
    FinalizarAplicacao
   ' Resume
End Sub

Private Sub CarregaDadosCombo_Cobertura()
    Dim sSQL                                     As String
    Dim rsRecordSet                              As ADODB.Recordset
    
    On Error GoTo Erro
    
    sSQL = ""
    
    Set rsRecordSet = ExecutarSQL(gsSIGLASISTEMA, _
                                  glAmbiente_id, _
                                  App.Title, _
                                  App.FileDescription, _
                                  sSQL, _
                                  True)
            
    cmbCobertura.Clear
    
    With rsRecordSet
        While Not .EOF
            cmbCobertura.AddItem .Fields(0)
            .MoveNext
        Wend
        .Close
    End With

    Set rsRecordSet = Nothing
    Exit Sub

Erro:
    TrataErroGeral "CarregaDadosCombo_Cobertura", Me.name
    FinalizarAplicacao
    
End Sub


Private Sub InicializaInterfaceLocal_Grid_Estimativas()
'Preenche com dados pr�-definidos a coluna 0 da Grid de Itens de Estimativas (da direita)
    Dim bLinhas                                 As Byte
    Dim bColunas                                As Byte
    Dim sLinhas                                 As String
        
    LimpaGrid grdEstimativasPagamentos_Estimativas
            
    With grdEstimativasPagamentos_Estimativas
'        If .Rows < 9 Then 'cristovao.rodrigues 31/03/2016 16429073 PMBC Amparo Familiar alterado para 9
'            .Rows = 9
'        End If
        For bLinhas = 1 To 8
            Select Case bLinhas
                Case 1
                    sLinha = "Indeniza��o"
                Case 2
                    sLinha = "Honor�rios"
                Case 3
                    sLinha = "Despesas"
                Case 4
                    sLinha = "Ressarcimento"
                Case 5
                    sLinha = "Desp. ressarcimento"
                Case 6
                    sLinha = "Salvados"
                Case 7
                    sLinha = "Desp. salvados"
'                Case 8 'cristovao.rodrigues 31/03/2016 16429073 PMBC Amparo Familiar incluido 8
'                    sLinha = "Indeniz. renda"
                
            End Select
            .TextMatrix(bLinhas, 0) = sLinha
            For bColunas = 1 To 4
                .TextMatrix(bLinhas, bColunas) = "0,00"
            Next
            .Col = 6
            .Row = bLinhas
            .CellBackColor = &H8000000F
            .CellForeColor = &H8000&
            .TextMatrix(.Row, .Col) = "$"
        Next
    End With
End Sub




Private Sub LimpaGrid(grdFlexGrid As MSFlexGrid)
    
    grdFlexGrid.Rows = 1

End Sub
'AKIO.OKUNO - #RETIRADA - SelecionaDados_AbaEstimativa_CoberturaAfetada_Local - 10/05/2013
'FUN��O SUBSTITUIDA
'Private Sub SelecionaDados_AbaEstimativa_CoberturaAfetada_Local()
'
'    sSQL = ""
'    sSQL = sSQL & " if isnull(OBJECT_ID('tempdb..#EstimativaPagamento_CoberturaAfetada'),0) >0" & vbNewLine
'    sSQL = sSQL & " BEGIN" & vbNewLine
'    sSQL = sSQL & "     DROP TABLE #EstimativaPagamento_CoberturaAfetada" & vbNewLine
'    sSQL = sSQL & " END" & vbNewLine
'    Conexao_ExecutarSQL gsSIGLASISTEMA, glAmbiente_id, App.Title, App.FileDescription, sSQL, lConexaoLocal, False
'
''    SelecionaDados_AbaEstimativa_CoberturaAfetada  'AKIO.OKUNO - 26/09/2012
'    SelecionaDados_AbaEstimativa_CoberturaAfetada lConexaoLocal
'
'
'End Sub
'AKIO.OKUNO - #RETIRADA - SelecionaDados_AbaEstimativa_CoberturaAfetada_Local - 10/05/2013
Private Function SelecionaDados_AbaEstimativa_CoberturaAfetada_Local(lConexao As Integer) As ADODB.Recordset
    
    Set SelecionaDados_AbaEstimativa_CoberturaAfetada_Local = SelecionaDados_AbaEstimativa_CoberturaAfetada(lConexao)

End Function


Private Sub InicializaModoProcessamentoObjeto(objObjeto As Object, bTipo As Byte)
    DoEvents
    
    With objObjeto
        Select Case bTipo
            Case 1              'Vai carregar
                If TypeOf objObjeto Is MSFlexGrid Then
                    .BackColorFixed = &HC0C0C0
                    .ForeColorFixed = &H808080
                Else
                    .BackColor = &HC0C0C0
                End If
            Case 2              'Carregado
                If TypeOf objObjeto Is MSFlexGrid Then
                    .BackColorFixed = &H8000000F
                    .ForeColorFixed = &H80000012
                Else
                    .BackColor = -2147483643
                End If
        End Select
    End With

End Sub

Private Sub btnSeq_Estimativa_DownClick()
    On Error GoTo Erro
    
    With btnSeq_Estimativa
        If .Value >= 1 Then
            bAlterou_Seq_Estimativa = True
            Seleciona_Estimativa .Value
        End If
    End With
    Exit Sub
    
Erro:
    Call TrataErroGeral("btnSeq_Estimativa_DownClick", "frmConsultaSinistro")
    Call FinalizarAplicacao

End Sub


Private Sub btnSeq_Estimativa_UpClick()
    On Error GoTo Erro
    
    With btnSeq_Estimativa
        If .Value <= Val(txtCountEstimativa.Tag) Then
            bAlterou_Seq_Estimativa = True
            Seleciona_Estimativa .Value
        End If
    End With
    Exit Sub
    
Erro:
    Call TrataErroGeral("btnSeq_Estimativa_UpClick", "frmConsultaSinistro")
    Call FinalizarAplicacao

End Sub

Private Sub Seleciona_Estimativa(iSeq_Estimativa As Integer)
    On Error GoTo Erro
    
    CarregaDadosTela iSeq_Estimativa
    Exit Sub
    
Erro:
    TrataErroGeral "Seleciona_Estimativa", Me.name
    FinalizarAplicacao

End Sub

Private Sub Form_Resize()
    lngontop = SetWindowPos(Me.hwnd, HWND_TOPMOST, 0, 0, 0, 0, (SWP_NOMOVE Or SWP_NOSIZE))  'AKIO.OKUNO - 30/04/2013
End Sub

Private Sub Form_Unload(Cancel As Integer)
    If Not FrmConsultaSinistrocon.GravaDados_BloqueiaSinistro(False, gbldSinistro_ID) Then Exit Sub           'FLAVIO.BEZERRA - 17/04/2013
End Sub

Private Sub grdEstimativasPagamentos_Estimativas_Click()
    Dim sChaveComposta                          As String
    Dim bProcessa                               As Boolean
    
    On Error GoTo Erro
    
    MousePointer = vbHourglass
    
    With grdEstimativasPagamentos_Estimativas
        If .Col = 6 Then
            If .TextMatrix(.Row, 3) <> "0,00" Then 'cristovao.rodrigues 29/10/212
            'If .TextMatrix(.Row, 1) <> "0,00" Then 'cristovao.rodrigues 29/10/212 comentei
'                If MsgBox("Deseja realizar o pagamento do item " & .TextMatrix(.Row, 0) & " ?", 282, "Confirma��o para pagamento") = 6 Then
                    sChaveComposta = txtCountEstimativa.Tag & "." & .Row
                    
                    'AKIO.OKUNO - INICIO - 13/11/2012
                    If LenB(Trim(sOperacaoCosseguro)) <> 0 Then
                        sChaveComposta = sChaveComposta & "." & sOperacaoCosseguro
                    Else
                        sChaveComposta = sChaveComposta & ".-"
                    End If
                    sChaveComposta = sChaveComposta & "." & strTipoProcesso
                    'AKIO.OKUNO - FIM - 13/11/2012
            
    
'                    If PegaJanela(gblAuxiliarExecutando) = 0 Then
                    If PegaJanela(gblAuxiliarExecutando) = 0 Or gblAuxiliarExecutando = 0 Then
                        ExecutaAplicacao "SEGP1293", 2, CStr(gbldSinistro_ID), sChaveComposta
                        sAuxiliarExecutado = "Estimativa"   'AKIO.OKUNO - 18/10/2012
                        
                        'cristovao.rodrigues 16/10/2012
                        DestroiInterface
                        'bCarregou_EstimativasPagamentos = False
                        'bAlterou_Seq_Estimativa = False
                        
                    End If
'                End If
            Else
                MsgBox "Item n�o possui valor a pagar!", 16, "Mensagem ao Usu�rio"
            End If
        End If
    End With
    
    MousePointer = vbNormal
    
    Exit Sub
    
Erro:
    TrataErroGeral "grdEstimativasPagamentos_Estimativas_Click", Me.Caption

End Sub


Private Sub InicializaInterfaceLocal_Grid_AbaEstimativasPagamentos()
'Ajusta Largura das colunas que cont�m valores
    Dim bColunas                                        As Byte
    Dim lLarguraPadrao                                  As Long
    
    lLarguraPadrao = 1220
    'Grid de Itens de Estimativa
    With grdEstimativasPagamentos_Estimativas
        If .ColWidth(1) <> lLarguraPadrao Then
            For bColunas = 1 To 4
                .ColWidth(bColunas) = lLarguraPadrao
                .Col = bColunas
            Next
        End If
    End With
End Sub

'cristovao.rodrigues 16/10/2012
Private Sub txtRetornoAuxiliar_Change()
    Dim sChaveComposta                  As String

 If UCase(txtRetornoAuxiliar.Text) <> "CANCELADO" And Len(txtRetornoAuxiliar.Text) > 0 Then
        'Call FrmConsultaSinistrocon.btnPagamento
'        Call Form_Load     'AKIO.OKUNO - 18/10/2012
        
        'AKIO.OKUNO - INICIO - 18/10/2012
        If sAuxiliarExecutado = "Estimativa" Then
            InicializaInterfaceLocal_Form
            InicializaInterfaceLocal_Grid_Estimativas
            InicializaInterfaceLocal_Grid_AbaEstimativasPagamentos
            bCarregou_EstimativasPagamentos = False
            Set cDados_Estimativa = Nothing 'AKIO.OKUNO - 18/10/2012
            CarregaDadosTela 0
            If ValidaDados_EncerramentoTotal_Local Then
                If MsgBox("J� foi pago total todos os itens do sinistro. Deseja encerrar? ", 292) = vbYes Then
                    strTipoProcesso_ORIGINAL = strTipoProcesso
                    If sOperacaoCosseguro = "C" Then
                        strTipoProcesso = strTipoProcesso_Encerrar_Cosseguro
                    Else
                        strTipoProcesso = strTipoProcesso_Encerrar
                    End If
                    Unload Me
                    FrmConsultaSinistro.Show
                    FrmConsultaSinistrocon.btnPagamento.Enabled = False
                    FrmConsultaSinistrocon.Hide
                    sAuxiliarExecutado = "EnceSinistro"
                End If
            End If
        ElseIf sAuxiliarExecutado = "EnceSinistro" Then
            FrmConsultaSinistrocon.btnPagamento.Enabled = False
        End If
        'AKIO.OKUNO - FIM - 18/10/2012
        
    End If
    'txtRetornoAuxiliar.Text = ""
End Sub

'AKIO.OKUNO - ValidaDados_EncerramentoTotal_Local - 17/10/2012
Private Function ValidaDados_EncerramentoTotal_Local() As Boolean
    ValidaDados_EncerramentoTotal_Local = ValidaDados_EncerramentoTotal(Me)
End Function

