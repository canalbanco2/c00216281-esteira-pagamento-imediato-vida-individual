VERSION 5.00
Begin VB.Form frmAtribuicaoAssessoria 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "SEGP0246 - Atribui��o de Assessoria"
   ClientHeight    =   2475
   ClientLeft      =   4560
   ClientTop       =   5130
   ClientWidth     =   8505
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   2475
   ScaleWidth      =   8505
   Begin VB.Frame frmAtribui��o 
      Height          =   2415
      Left            =   120
      TabIndex        =   3
      Top             =   0
      Width           =   8295
      Begin VB.CommandButton btnAplicar 
         Caption         =   "Aplicar"
         Height          =   375
         Left            =   4920
         TabIndex        =   7
         Top             =   1800
         Width           =   1452
      End
      Begin VB.CommandButton BtnCancelar 
         Caption         =   "&Sair"
         Height          =   375
         Left            =   6600
         TabIndex        =   6
         Top             =   1800
         Width           =   1452
      End
      Begin VB.ComboBox cboTecnico_Atribuido 
         Height          =   315
         Left            =   225
         TabIndex        =   2
         Top             =   1320
         Width           =   7815
      End
      Begin VB.TextBox txtNmTecnico_Responsavel 
         Enabled         =   0   'False
         Height          =   315
         Left            =   1020
         TabIndex        =   1
         Top             =   600
         Width           =   6975
      End
      Begin VB.TextBox txtCdTecnico_Responsavel 
         Enabled         =   0   'False
         Height          =   315
         Left            =   225
         TabIndex        =   0
         Top             =   600
         Width           =   735
      End
      Begin VB.TextBox txtCdTecnico_Atribuido 
         Enabled         =   0   'False
         Height          =   315
         Left            =   240
         TabIndex        =   4
         Top             =   1320
         Visible         =   0   'False
         Width           =   735
      End
      Begin VB.TextBox txtNmTecnico_Atribuido 
         Enabled         =   0   'False
         Height          =   315
         Left            =   1035
         TabIndex        =   5
         Top             =   1320
         Visible         =   0   'False
         Width           =   6975
      End
      Begin VB.Label Label1 
         Caption         =   "T�cnico a ser atribu�do:"
         Height          =   255
         Left            =   240
         TabIndex        =   9
         Top             =   1080
         Width           =   3375
      End
      Begin VB.Label lblPesquisaPor 
         Caption         =   "T�cnico respons�vel pelo aviso:"
         Height          =   255
         Left            =   240
         TabIndex        =   8
         Top             =   360
         Width           =   3375
      End
   End
End
Attribute VB_Name = "frmAtribuicaoAssessoria"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Private lConexaoLocal                           As Integer

Private bytModoOperacao                         As Byte '1-Inclusao / 2-Alteracao/Exclus�o / 3-Consulta

Private lTecnico_ID                             As Long
Private sNome                                   As String
Private sEmail                                  As String
Private sEmpresa                                As String
Private sAssessoria                             As String
Private dDt_Fim_Vigencia                        As Date

Private aAssessoria()                           As Variant

Private iAssessoria                             As Integer



Private Sub Form_Load()

    On Error GoTo Erro
    
    IniciarConexao
    
    InicializaVariaveisLocal
    
    InicializaInterfaceLocal
    
    Exit Sub
    
Erro:
    TrataErroGeral "Form_Load", Me.name
    FinalizarAplicacao
    
End Sub



Private Sub BtnAplicar_Click()
    Dim sSQL                                            As String

    On Error GoTo Trata_Erro
    
    With cboTecnico_Atribuido
        If .Visible = True And _
           .Text <> "" Then
            If txtCdTecnico_Responsavel.Visible = True Then
                AtualizaDadosBD_Tipo1 1
            Else
                AtualizaDadosBD_Tipo1 2
            End If
        ElseIf .Visible = False Then
            AtualizaDadosBD_Tipo2
        End If
    End With
    
    Mensagem "Altera��o de t�cnico realizada com sucesso!"
    
    Unload Me
    
    Exit Sub
        
Trata_Erro:
    TrataErroGeral "btnAplicar_Click", Me.name
    FinalizarAplicacao
    
End Sub

Private Sub btnCancelar_Click()
    On Error GoTo Erro
    
    DestroiInterface
    
    Unload Me
    Exit Sub

Erro:
    TrataErroGeral "BtnCancelar_Click", Me.Caption
    FinalizarAplicacao
End Sub


Private Sub DestroiInterface()
    On Error GoTo Erro
    
    Set rsRecordSet = Nothing
    Set cDados_Estimativa = Nothing
    
    bCarregou_AbaEstimativasPagamentos = False
    bAlterou_Seq_Estimativa = False
    
    Exit Sub

Erro:
    Call TrataErroGeral("DestroiInterface", Me.Caption)
    Call FinalizarAplicacao

End Sub


Private Sub InicializaVariaveisLocal()

    bytModoOperacao = 3
    
End Sub

Private Sub IniciarConexao()
    On Error GoTo Erro

    'Tempor�rio - Confirmar se a conex�o vir� pelo par�metro ou n�o.
    vNunCx = 0

    If EstadoConexao(vNunCx) = adStateClosed Then
        lConexaoLocal = AbrirConexao(gsSIGLASISTEMA, glAmbiente_id, App.Title, App.FileDescription)
    End If

    Exit Sub

Erro:
    TrataErroGeral "IniciarConexao", Me.name
    FinalizarAplicacao

End Sub

Private Sub InicializaInterfaceLocal()
    On Error GoTo Erro
    
    Screen.MousePointer = vbHourglass
    
    Call CentraFrm(Me)
        
    InicializaInterfaceLocal_Form
        
    LimpaCamposTelaLocal Me                                         'SERA ALTERADO
        
    CarregaDadosTela
    
    Screen.MousePointer = vbNormal
    
    Exit Sub

Erro:
    TrataErroGeral "InicializaInterfaceLocal", Me.name
    FinalizarAplicacao
    
End Sub



Private Sub InicializaInterfaceLocal_Form()
    
    Me.Caption = App.EXEName & " - Sele��o de Estimativa - Pagamento de sinistro - " & Ambiente
    
End Sub

Private Sub LimpaCamposTelaLocal(objObjeto As Object)
    On Error GoTo Erro
    
    If Not objObjeto Is Nothing Then
        LimpaCamposTela objObjeto
    End If
    
    Exit Sub

Erro:
    TrataErroGeral "LimpaCamposTelaLocal", Me.name
    FinalizarAplicacao

End Sub




Private Sub CarregaDadosTela()
    Dim rsRecordSet                             As ADODB.Recordset
    Dim sSQL                                    As String
    
    On Error GoTo Erro
    
    MousePointer = vbHourglass
    
    SelecionaDados
        
    sSQL = ""
    sSQL = sSQL & "Select * " & vbCrLf
    sSQL = sSQL & "  From #Sinistro_Assessoria" & vbCrLf
    
    Set rsRecordSet = Conexao_ExecutarSQL(gsSIGLASISTEMA, _
                                          glAmbiente_id, _
                                          App.Title, _
                                          App.FileDescription, _
                                          sSQL, _
                                          lConexaoLocal, _
                                          True)
    iAssessoria = 0
    
    With rsRecordSet
        If .EOF Then
            Label1.Visible = False
            txtCdTecnico_Responsavel.Visible = False
            txtNmTecnico_Responsavel.Visible = False
            
            CarregaDadosCombo_Tecnico
            
            Me.BtnAplicar.Enabled = True
            
            Set rsRecordSet = Nothing
            
            Exit Sub
        Else
            Do While Not .EOF
                If iAssessoria = 0 Then
                    ReDim aAssessoria(iAssessoria, 6)
                Else
                    ReDim Preserve aAssessoria(iAssessoria, 6)
                End If
                aAssessoria(iAssessoria, 0) = .Fields!tecnico_id  '.tecnico_id
                aAssessoria(iAssessoria, 1) = .Fields!Nome & ""   '.Nome
                aAssessoria(iAssessoria, 2) = .Fields!Email       '.Email
                aAssessoria(iAssessoria, 3) = .Fields!Empresa     '.Empresa
                aAssessoria(iAssessoria, 4) = .Fields!assessoria  '.assessoria
                aAssessoria(iAssessoria, 5) = .Fields!Dt_Fim_Vigencia '.dt_fim_vigencia
                .MoveNext
               iAssessoria = iAssessoria + 1
            Loop
            
            If aAssessoria(iAssessoria - 1, 4) = "" Or _
               aAssessoria(iAssessoria - 1, 4) = "n" Then
                lTecnico_ID = aAssessoria(iAssessoria - 1, 0)
                txtCdTecnico_Responsavel = aAssessoria(iAssessoria - 1, 0)
                txtNmTecnico_Responsavel = aAssessoria(iAssessoria - 1, 1)
                
                CarregaDadosCombo_Tecnico
                
            Else
                lTecnico_ID = aAssessoria(iAssessoria - 1).tecnico_id
                txtCdTecnico_Responsavel = aAssessoria(iAssessoria - 1).tecnico_id
                txtNmTecnico_Responsavel = aAssessoria(iAssessoria - 1).Nome
                cboTecnico_Atribuido.Visible = False
                txtCdTecnico_Atribuido.Visible = True
                txtNmTecnico_Atribuido.Visible = True
                If (iAssessoria - 2) >= 0 Then
                    txtCdTecnico_Atribuido.Text = aAssessoria(iAssessoria - 2).tecnico_id
                    txtNmTecnico_Atribuido.Text = aAssessoria(iAssessoria - 2).Nome
                Else
                    txtCdTecnico_Atribuido.Visible = False
                    txtNmTecnico_Atribuido.Visible = False
                End If
                BtnAplicar.Enabled = True
            End If
        End If
    End With

    Set rsRecordSet = Nothing

    MousePointer = vbNormal
    
    Exit Sub

Erro:
    TrataErroGeral "CarregaDadosTela", Me.name
    FinalizarAplicacao

End Sub

 
Private Sub InicializaToolTipTextLocal()
    On Error GoTo Erro

    InicializaToolTipText Me
    
    Exit Sub

Erro:
    TrataErroGeral "InicializaToolTipTextLocal", Me.name
    FinalizarAplicacao

End Sub

Private Sub SelecionaDados()
    Dim sSQL                                    As String
    
    On Error GoTo Erro
    
    MousePointer = vbHourglass
        
    sSQL = ""
    sSQL = sSQL & " if isnull(OBJECT_ID('tempdb..#Sinistro_Assessoria'),0) >0" & vbNewLine
    sSQL = sSQL & " BEGIN" & vbNewLine
    sSQL = sSQL & "     DROP TABLE #Sinistro_Assessoria" & vbNewLine
    sSQL = sSQL & " END" & vbNewLine
    sSQL = sSQL & "Select Numeracao                             = 1                                 " & vbNewLine
    sSQL = sSQL & "     , Tecnico_Tb.Tecnico_ID                                                     " & vbNewLine
    sSQL = sSQL & "     , Tecnico_Tb.Nome                                                           " & vbNewLine
    sSQL = sSQL & "     , Tecnico_Tb.Email                                                          " & vbNewLine
    sSQL = sSQL & "     , Tecnico_Tb.Empresa                                                        " & vbNewLine
    sSQL = sSQL & "     , Assessoria                            = Lower(Tecnico_Tb.Assessoria)      " & vbNewLine
    sSQL = sSQL & "     , Sinistro_Tecnico_Tb.Dt_Fim_Vigencia                                       " & vbNewLine
    sSQL = sSQL & "     , Sinistro_Tecnico_Tb.Dt_Inicio_Vigencia                                    " & vbNewLine
    sSQL = sSQL & "  Into #Sinistro_Assessoria                                                      " & vbNewLine
    sSQL = sSQL & "  From Seguros_Db.Dbo.Tecnico_Tb             Tecnico_Tb (NoLock)                 " & vbNewLine
    sSQL = sSQL & "  Join Seguros_Db.Dbo.Sinistro_Tecnico_Tb    Sinistro_Tecnico_Tb (NoLock)        " & vbNewLine
    sSQL = sSQL & "    on Tecnico_Tb.Tecnico_ID                 = Sinistro_Tecnico_Tb.Tecnico_ID    " & vbNewLine
    sSQL = sSQL & " Where Sinistro_Tecnico_Tb.Sinistro_ID       = " & gbldSinistro_ID & vbNewLine
    sSQL = sSQL & "   and Sinistro_Tecnico_Tb.Dt_Fim_Vigencia   is not Null                         " & vbNewLine
    sSQL = sSQL & " Union                                                                           " & vbNewLine
    sSQL = sSQL & "Select Numeracao                             = 2                                 " & vbNewLine
    sSQL = sSQL & "     , Tecnico_Tb.Tecnico_ID                                                     " & vbNewLine
    sSQL = sSQL & "     , Tecnico_Tb.Nome                                                           " & vbNewLine
    sSQL = sSQL & "     , Tecnico_Tb.Email                                                          " & vbNewLine
    sSQL = sSQL & "     , Tecnico_Tb.Empresa                                                        " & vbNewLine
    sSQL = sSQL & "     , Assessoria                            = Lower(Tecnico_Tb.Assessoria)      " & vbNewLine
    sSQL = sSQL & "     , Sinistro_Tecnico_Tb.Dt_Fim_Vigencia                                       " & vbNewLine
    sSQL = sSQL & "     , Sinistro_Tecnico_Tb.Dt_Inicio_Vigencia                                    " & vbNewLine
    sSQL = sSQL & "  From Seguros_Db.Dbo.Tecnico_Tb             Tecnico_Tb (NoLock)                 " & vbNewLine
    sSQL = sSQL & "  Join Seguros_Db.Dbo.Sinistro_Tecnico_Tb    Sinistro_Tecnico_Tb (NoLock)        " & vbNewLine
    sSQL = sSQL & "    on Tecnico_Tb.Tecnico_ID                 = Sinistro_Tecnico_Tb.Tecnico_ID    " & vbNewLine
    sSQL = sSQL & " Where Sinistro_Tecnico_Tb.Sinistro_ID       = " & gbldSinistro_ID & vbNewLine
    sSQL = sSQL & "   and Sinistro_Tecnico_Tb.Dt_Fim_Vigencia   is Null                             " & vbNewLine
    sSQL = sSQL & " Order by 1                                                                      " & vbNewLine
    sSQL = sSQL & "     , Sinistro_Tecnico_Tb.Dt_Inicio_Vigencia                                    " & vbNewLine
         
    Call Conexao_ExecutarSQL(gsSIGLASISTEMA, _
                             glAmbiente_id, _
                             App.Title, _
                             App.FileDescription, _
                             sSQL, _
                             lConexaoLocal, _
                             False)
    
    Exit Sub
    
Erro:
    TrataErroGeral "SelecionaDados", Me.name
    FinalizarAplicacao
                                                                                                                                                
End Sub


Private Sub CarregaDadosCombo_Tecnico()
    Dim rsRecordSet                                         As ADODB.Recordset
    Dim sSQL                                                As String
    
    On Error GoTo Erro
    
    sSQL = ""
    sSQL = sSQL & "Select Tecnico_ID                        " & vbNewLine
    sSQL = sSQL & "     , Nome                              " & vbNewLine
    sSQL = sSQL & "  From Tecnico_Tb                        " & vbNewLine
    sSQL = sSQL & " Where Assessoria            = 's'       " & vbNewLine
    sSQL = sSQL & " Order By Tecnico_ID                     " & vbNewLine
    
    Set rsRecordSet = Conexao_ExecutarSQL(gsSIGLASISTEMA, _
                                          glAmbiente_id, _
                                          App.Title, _
                                          App.FileDescription, _
                                          sSQL, _
                                          lConexaoLocal, _
                                          True)
    With rsRecordSet
        Do While Not .EOF
            cboTecnico_Atribuido.AddItem .Fields!Nome
            cboTecnico_Atribuido.ItemData(cboTecnico_Atribuido.NewIndex) = .Fields!tecnico_id
            .MoveNext
        Loop
    End With
    
    Set rsRecordSet = Nothing
    
    Exit Sub
    
Erro:
    TrataErroGeral "CarregaDadosCombo_Tecnico", Me.name
    FinalizarAplicacao
    
End Sub


Sub AtualizaDadosBD_Tipo1(ByVal iParametro As Integer)
    Dim sMail                                   As String
    Dim lSeq                                    As Long
    Dim rsRecordSet                             As ADODB.Recordset
    Dim sSQL                                    As String
    
    On Error GoTo Trata_Erro
    
    bConex = AbrirTransacao(lConexaoLocal)
    
    sSQL = ""
    sSQL = sSQL & "Exec sinistro_tecnico_spi " & gbldSinistro_ID
    sSQL = sSQL & "                        , " & gbllApolice_ID
    sSQL = sSQL & "                        , " & gbllSucursal_Seguradora_ID
    sSQL = sSQL & "                        , " & gbllSeguradora_Cod_Susep
    sSQL = sSQL & "                        , " & gbllRamo_ID
    sSQL = sSQL & "                        , " & cboTecnico_Atribuido.ItemData(cboTecnico_Atribuido.ListIndex)
    sSQL = sSQL & "                        ,'" & cUserName & "'"
    Call Conexao_ExecutarSQL(gsSIGLASISTEMA, _
                             glAmbiente_id, _
                             App.Title, _
                             App.FileDescription, _
                             sSQL, _
                             lConexaoLocal, _
                             False)
        
        
        
    If iParametro = 1 Then
        sSQL = ""
        sSQL = sSQL & "Select EMail                                         " & vbNewLine
        sSQL = sSQL & "     , Isnull(Empresa,'')                            " & vbNewLine
        sSQL = sSQL & "  From Tecnico_Tb            Tecnico_Tb (NoLock)     " & vbNewLine
        sSQL = sSQL & " Where Tecnico_ID            = " & lTecnico_ID & vbNewLine
        sSQL = sSQL & " Order By Tecnico_ID                                 " & vbNewLine
        
        Set rsRecordSet = Conexao_ExecutarSQL(gsSIGLASISTEMA, _
                                              glAmbiente_id, _
                                              App.Title, _
                                              App.FileDescription, _
                                              sSQL, _
                                              lConexaoLocal, _
                                              True)
        With rsRecordSet
            If Not .EOF Then
                sMail = .Fields!Email
                sMail = sMail & "." & .Fields!Empresa
                sMail = "@aliancadobrasil.com.br"
            End If
        End With
        
        sSQL = ""
        sSQL = sSQL & "Exec sinistro_tecnico_spu " & gbldSinistro_ID
        sSQL = sSQL & "                        , " & gbllApolice_ID
        sSQL = sSQL & "                        , " & gbllSucursal_Seguradora_ID
        sSQL = sSQL & "                        , " & gbllSeguradora_Cod_Susep
        sSQL = sSQL & "                        , " & gbllRamo_ID
        sSQL = sSQL & "                        , " & lTecnico_ID
        sSQL = sSQL & "                        ,'" & cUserName & "'"
        Call Conexao_ExecutarSQL(gsSIGLASISTEMA, _
                                 glAmbiente_id, _
                                 App.Title, _
                                 App.FileDescription, _
                                 sSQL, _
                                 lConexaoLocal, _
                                 False)
        
        sSQL = ""
        sSQL = sSQL & "Exec envia_email_sp '" & sMail & "'"
        sSQL = sSQL & "                  , 'Transfer�ncia de Sinistro(s)'"
        sSQL = sSQL & "                  , 'O aviso de sinistro n� " & gbldSinistro_ID & " foi transferido de sua responsabilidade para outro t�cnico.'"
        Call Conexao_ExecutarSQL(gsSIGLASISTEMA, _
                                 glAmbiente_id, _
                                 App.Title, _
                                 App.FileDescription, _
                                 sSQL, _
                                 lConexaoLocal, _
                                 False)
        
     End If
    
    
    'Cria��o de historico......
    sSQL = ""
    sSQL = sSQL & "Select IsNull(Max(Seq_Estimativa),0)+1               " & vbNewLine
    sSQL = sSQL & "  From Sinistro_Estimativa_Tb (NoLock)               " & vbNewLine
    sSQL = sSQL & " Where Sinistro_ID               = " & gbldSinistro_ID & vbNewLine
    
    Set rsRecordSet = Conexao_ExecutarSQL(gsSIGLASISTEMA, _
                                          glAmbiente_id, _
                                          App.Title, _
                                          App.FileDescription, _
                                          sSQL, _
                                          lConexaoLocal, _
                                          True)
    
    lSeq = rsRecordSet.Fields(0)
    Set rsRecordSet = Nothing
    
    sSQL = ""
    sSQL = sSQL & "Select Motivo_Encerramento                                       " & vbNewLine
    sSQL = sSQL & "     , Situacao                                                  " & vbNewLine
    sSQL = sSQL & "  From Sinistro_Historico_Tb (NoLock)                            " & vbNewLine
    sSQL = sSQL & " Where Sinistro_ID               = " & gbldSinistro_ID & vbNewLine
    sSQL = sSQL & "   and Seq_Evento in (Select Max(Seq_Evento)                     " & vbNewLine
    sSQL = sSQL & "                        From Sinistro_Historico_Tb (NoLock)      " & vbNewLine
    sSQL = sSQL & "                       Where Sinistro_ID = " & gbldSinistro_ID & vbNewLine
    sSQL = sSQL & "                      )                                          " & vbNewLine
    
    Set rsRecordSet = Conexao_ExecutarSQL(gsSIGLASISTEMA, _
                                          glAmbiente_id, _
                                          App.Title, _
                                          App.FileDescription, _
                                          sSQL, _
                                          lConexaoLocal, _
                                          True)
    
    With rsRecordSet
        sSQL = ""
        sSQL = sSQL & "Exec sinistro_historico_spi " & gbldSinistro_ID & vbNewLine
        sSQL = sSQL & "                          , " & gbllApolice_ID & vbNewLine
        sSQL = sSQL & "                          , " & gbllSucursal_Seguradora_ID & vbNewLine
        sSQL = sSQL & "                          , " & gbllSeguradora_Cod_Suse & vbNewLinep
        sSQL = sSQL & "                          , " & gbllRamo_ID & vbNewLine
        sSQL = sSQL & "                          , 10003 & vbNewLine"
        sSQL = sSQL & "                          ,'" & .Fields(1) & "'" & vbNewLine
        sSQL = sSQL & "                          ,'" & .Fields(0) & "'" & vbNewLine
        sSQL = sSQL & "                          ,'" & Format(Data_Sistema, "yyyymmdd") & "'" & vbNewLine
        sSQL = sSQL & "                          ,'" & cUserName & "'" & vbNewLine
        sSQL = sSQL & "                          , " & lSeq & vbNewLine
        sSQL = sSQL & "                          , Null " & vbNewLine
        sSQL = sSQL & "                          ,'s'" & vbNewLine
        
        Call Conexao_ExecutarSQL(gsSIGLASISTEMA, _
                                 glAmbiente_id, _
                                 App.Title, _
                                 App.FileDescription, _
                                 sSQL, _
                                 lConexaoLocal, _
                                 False)
    
    End With

    sSQL = ""
    sSQL = sSQL & "Exec evento_SEGBR_sinistro_spi " & gbldSinistro_ID & vbNewLine
    sSQL = sSQL & "                             , " & gbllApolice_ID & vbNewLine
    sSQL = sSQL & "                             , " & gbllSeguradora_Cod_Susep & vbNewLine
    sSQL = sSQL & "                             , " & gbllSucursal_Seguradora_ID & vbNewLine
    sSQL = sSQL & "                             , " & gbllRamo_ID & vbNewLine
    sSQL = sSQL & "                             , 10003" & vbNewLine
    sSQL = sSQL & "                             ,'" & Format(Data_Sistema, "yyyymmdd") & "'" & vbNewLine
    sSQL = sSQL & "                             , 1" & vbNewLine
    sSQL = sSQL & "                             ,'SEGBR'" & vbNewLine
    sSQL = sSQL & "                             ,'" & cUserName & "'" & vbNewLine
    sSQL = sSQL & "                             , " & gbllProduto_ID & vbNewLine
    sSQL = sSQL & "                             , " & IIf(gbldSinistro_BB <> 0, gbllSinistro_BB, "Null") & vbNewLine             'VER SINISTRO_BB
    sSQL = sSQL & "                             , " & gbllProposta_ID & vbNewLine
    sSQL = sSQL & "                             , Null " & vbNewLine
    
    Call Conexao_ExecutarSQL(gsSIGLASISTEMA, _
                             glAmbiente_id, _
                             App.Title, _
                             App.FileDescription, _
                             sSQL, _
                             lConexaoLocal, _
                             False)

    

    sSQL = ""
    sSQL = sSQL & "Select Isnull(Max(Seq_Evento),0)+1                   " & vbNewLine
    sSQL = sSQL & "  From Sinistro_Item_Historico_Tb (NoLock)           " & vbNewLine
    sSQL = sSQL & " Where Sinistro_ID               = " & gbldSinistro_ID & vbNewLine
    
    Set rsRecordSet = Conexao_ExecutarSQL(gsSIGLASISTEMA, _
                                          glAmbiente_id, _
                                          App.Title, _
                                          App.FileDescription, _
                                          sSQL, _
                                          lConexaoLocal, _
                                          True)
    
    With rsRecordSet
        If Not .EOF Then
            lSeq = .Fields(0)
        End If
    End With
    Set rsRecordSet = Nothing
        
    sSQL = "" & vbNewLine
    sSQL = sSQL & "Exec sinistro_item_historico_spi " & gbldSinistro_ID & vbNewLine
    sSQL = sSQL & "                               , " & gbllApolice_ID & vbNewLine
    sSQL = sSQL & "                               , " & gbllSucursal_Seguradora_ID & vbNewLine
    sSQL = sSQL & "                               , " & gbllSeguradora_Cod_Susep & vbNewLine
    sSQL = sSQL & "                               , " & gbllRamo_ID & vbNewLine
    sSQL = sSQL & "                               , " & lSeq & vbNewLine
    sSQL = sSQL & "                               ,'" & cboTecnico_Atribuido.Text & "'" & vbNewLine
    sSQL = sSQL & "                               ,'" & cUserName & "'" & vbNewLine
    
    Call Conexao_ExecutarSQL(gsSIGLASISTEMA, _
                             glAmbiente_id, _
                             App.Title, _
                             App.FileDescription, _
                             sSQL, _
                             lConexaoLocal, _
                             False)
    
    
    'Email de aviso de recebimento...
    sSQL = ""
    sSQL = sSQL & "Select Email                                         " & vbNewLine
    sSQL = sSQL & "     , Isnull(Empresa,'')                            " & vbNewLine
    sSQL = sSQL & "  From Tecnico_Tb (NoLock)                           " & vbNewLine
    sSQL = sSQL & " Where Tecnico_ID                = " & cboTecnico_Atribuido.ItemData(cboTecnico_Atribuido.ListIndex) & vbNewLine
    
    Set rsRecordSet = Conexao_ExecutarSQL(gsSIGLASISTEMA, _
                                          glAmbiente_id, _
                                          App.Title, _
                                          App.FileDescription, _
                                          sSQL, _
                                          lConexaoLocal, _
                                          True)
    
    With rsRecordSet
        If Not .EOF Then
            sMail = .Fields(0)
            If .Fields(1) <> "" Then
                sMail = sMail & "." & .Fields(1)
            End If
            sMail = sMail & "@aliancadobrasil.com.br"
        End If
    End With
    Set rsRecordSet = Nothing
    
    sSQL = ""
    sSQL = sSQL & "Exec Envia_eMail_Sp '" & sMail & "'" & vbNewLine
    sSQL = sSQL & "                  ,'Atribui��o de Sinistro'" & vbNewLine
    sSQL = sSQL & "                  ,'Voc� acaba de receber o seguinte aviso de sinistro: " & gbldSinistro_ID & "'" & vbNewLine
    
    Call Conexao_ExecutarSQL(gsSIGLASISTEMA, _
                             glAmbiente_id, _
                             App.Title, _
                             App.FileDescription, _
                             sSQL, _
                             lConexaoLocal, _
                             False)
    
        
    bConex = ConfirmarTransacao(lConexaoLocal)
                
    Exit Sub

Trata_Erro:
    bConex = RetornarTransacao(lConexaoLocal)
    TrataErroGeral "AtualizaDadosBD_Tipo1", Me.name
    FinalizarAplicacao
    
End Sub




Sub AtualizaDadosBD_Tipo2()
    Dim sMail                                   As String
    Dim lSeq                                    As Long
    Dim rsRecordSet                             As ADODB.Recordset
    Dim sSQL                                    As String
    
    On Error GoTo Trata_Erro
    
    bConex = AbrirTransacao(lConexaoLocal)
    
    sSQL = ""
    sSQL = sSQL & "Select Email                                         " & vbNewLine
    sSQL = sSQL & "     , Isnull(Empresa,'')                            " & vbNewLine
    sSQL = sSQL & "  From Tecnico_Tb (NoLock)                           " & vbNewLine
    sSQL = sSQL & " Where Tecnico_ID                = " & lTecnico_ID & vbNewLine
    
    Set rsRecordSet = Conexao_ExecutarSQL(gsSIGLASISTEMA, _
                                          glAmbiente_id, _
                                          App.Title, _
                                          App.FileDescription, _
                                          sSQL, _
                                          lConexaoLocal, _
                                          True)
    
    With rsRecordSet
        If Not .EOF Then
            sMail = .Fields(0)
            If .Fields(1) <> "" Then
                sMail = sMail & "." & .Fields(1)
            End If
            sMail = sMail & "@aliancadobrasil.com.br"
        End If
    End With
    Set rsRecordSet = Nothing
    
    
    sSQL = ""
    sSQL = sSQL & "Exec sinistro_tecnico_spu " & gbldSinistro_ID
    sSQL = sSQL & "                        , " & gbllApolice_ID
    sSQL = sSQL & "                        , " & gbllSucursal_Seguradora_ID
    sSQL = sSQL & "                        , " & gbllSeguradora_Cod_Susep
    sSQL = sSQL & "                        , " & gbllRamo_ID
    sSQL = sSQL & "                        , " & lTecnico_ID
    sSQL = sSQL & "                        ,'" & cUserName & "'"
    
    Call Conexao_ExecutarSQL(gsSIGLASISTEMA, _
                             glAmbiente_id, _
                             App.Title, _
                             App.FileDescription, _
                             sSQL, _
                             lConexaoLocal, _
                             False)
    
    If (iAssessoria - 2) >= 0 Then
        sSQL = ""
        sSQL = sSQL & "Exec sinistro_tecnico_spi " & gbldSinistro_ID & vbNewLine
        sSQL = sSQL & "                        , " & gbllApolice_ID & vbNewLine
        sSQL = sSQL & "                        , " & gbllSucursal_Seguradora_ID & vbNewLine
        sSQL = sSQL & "                        , " & gbllSeguradora_Cod_Susep & vbNewLine
        sSQL = sSQL & "                        , " & gbllRamo_ID & vbNewLine
        sSQL = sSQL & "                        , " & txtCdTecnico_Atribuido.Text & vbNewLine
        sSQL = sSQL & "                        ,'" & cUserName & "'"
        
        Call Conexao_ExecutarSQL(gsSIGLASISTEMA, _
                                 glAmbiente_id, _
                                 App.Title, _
                                 App.FileDescription, _
                                 sSQL, _
                                 lConexaoLocal, _
                                 False)
    
    End If
    
    sSQL = ""
    sSQL = sSQL & "Exec envia_email_sp '" & sMail & "'"
    sSQL = sSQL & "                  , 'Transfer�ncia de Sinistro(s)'"
    sSQL = sSQL & "                  , 'O aviso de sinistro n� " & gbldSinistro_ID & " foi transferido de sua responsabilidade para outro t�cnico.'"
    Call Conexao_ExecutarSQL(gsSIGLASISTEMA, _
                             glAmbiente_id, _
                             App.Title, _
                             App.FileDescription, _
                             sSQL, _
                             lConexaoLocal, _
                             False)
    
    
    'Cria��o de historico......
    sSQL = ""
    sSQL = sSQL & "Select IsNull(Max(Seq_Estimativa),0)+1               " & vbNewLine
    sSQL = sSQL & "  From Sinistro_Estimativa_Tb (NoLock)               " & vbNewLine
    sSQL = sSQL & " Where Sinistro_ID               = " & gbldSinistro_ID & vbNewLine
    
    Set rsRecordSet = Conexao_ExecutarSQL(gsSIGLASISTEMA, _
                                          glAmbiente_id, _
                                          App.Title, _
                                          App.FileDescription, _
                                          sSQL, _
                                          lConexaoLocal, _
                                          True)
    
    lSeq = rsRecordSet.Fields(0)
    Set rsRecordSet = Nothing
    
    
    sSQL = ""
    sSQL = sSQL & "Select Motivo_Encerramento                           " & vbNewLine
    sSQL = sSQL & "     , Situacao                                      " & vbNewLine
    sSQL = sSQL & "  From Sinistro_Historico_Tb (NoLock)                " & vbNewLine
    sSQL = sSQL & " Where Sinistro_ID               = " & gbldSinistro_ID & vbNewLine
    sSQL = sSQL & "   and Seq_Evento in (Select Max(Seq_Evento)         " & vbNewLine
    sSQL = sSQL & "                        From Sinistro_Historico_Tb (NoLock)" & vbNewLine
    sSQL = sSQL & "                       Where Sinistro_ID = " & gbldSinistro_ID & vbNewLine
    sSQL = sSQL & "                      )                              " & vbNewLine
    
    Set rsRecordSet = Conexao_ExecutarSQL(gsSIGLASISTEMA, _
                                          glAmbiente_id, _
                                          App.Title, _
                                          App.FileDescription, _
                                          sSQL, _
                                          lConexaoLocal, _
                                          True)
    
    With rsRecordSet
        If Not .EOF Then

            sSQL = sSQL & "Exec sinistro_historico_spi " & gbldSinistro_ID
            sSQL = sSQL & "                          , " & gbllApolice_ID
            sSQL = sSQL & "                          , " & gbllSucursal_Seguradora_ID
            sSQL = sSQL & "                          , " & gbllSeguradora_Cod_Susep
            sSQL = sSQL & "                          , " & gbllRamo_ID
            sSQL = sSQL & "                          , 10003"
            sSQL = sSQL & "                          ,'" & .Fields(1) & "'"
            sSQL = sSQL & "                          ,'" & .Fields(0) & "'"
            sSQL = sSQL & "                          ,'" & Format(Data_Sistema, "yyyymmdd") & "'"
            sSQL = sSQL & "                          ,'" & cUserName & "'"
            sSQL = sSQL & "                          , " & lSeq
            sSQL = sSQL & "                          , Null "
            sSQL = sSQL & "                          ,'s'"
            
            Call Conexao_ExecutarSQL(gsSIGLASISTEMA, _
                                     glAmbiente_id, _
                                     App.Title, _
                                     App.FileDescription, _
                                     sSQL, _
                                     lConexaoLocal, _
                                     False)
    
        End If
    End With
    
    sSQL = ""
    sSQL = sSQL & "Exec evento_SEGBR_sinistro_spi " & gbldSinistro_ID
    sSQL = sSQL & "                             , " & gbllApolice_ID
    sSQL = sSQL & "                             , " & gbllSeguradora_Cod_Susep
    sSQL = sSQL & "                             , " & gbllSucursal_Seguradora_ID
    sSQL = sSQL & "                             , " & gbllRamo_ID
    sSQL = sSQL & "                             , 10003"
    sSQL = sSQL & "                             ,'" & Format(Data_Sistema, "yyyymmdd") & "'"
    sSQL = sSQL & "                             , 1"
    sSQL = sSQL & "                             ,'SEGBR'"
    sSQL = sSQL & "                             ,'" & cUserName & "'"
    sSQL = sSQL & "                             , " & gbllProduto_ID
    sSQL = sSQL & "                             , " & IIf(gbldSinistro_BB <> 0, gbllSinistro_BB, "Null")             'VER SINISTRO_BB
    sSQL = sSQL & "                             , " & gbllProposta_ID
    sSQL = sSQL & "                             , Null "
    
    Call Conexao_ExecutarSQL(gsSIGLASISTEMA, _
                             glAmbiente_id, _
                             App.Title, _
                             App.FileDescription, _
                             sSQL, _
                             lConexaoLocal, _
                             False)
                                 
    sSQL = ""
    sSQL = sSQL & "Select Isnull(Max(Seq_Evento),0)+1                   " & vbNewLine
    sSQL = sSQL & "  From Sinistro_Item_Historico_Tb (NoLock)           " & vbNewLine
    sSQL = sSQL & " Where Sinistro_ID               = " & gbldSinistro_ID & vbNewLine
    
    Set rsRecordSet = Conexao_ExecutarSQL(gsSIGLASISTEMA, _
                                          glAmbiente_id, _
                                          App.Title, _
                                          App.FileDescription, _
                                          sSQL, _
                                          lConexaoLocal, _
                                          True)
    
    With rsRecordSet
        If Not .EOF Then
            lSeq = .Fields(0)
        End If
    End With
    Set rsRecordSet = Nothing
                                 
    sSQL = ""
    sSQL = sSQL & "Exec sinistro_item_historico_spi " & gbldSinistro_ID
    sSQL = sSQL & "                               , " & gbllApolice_ID
    sSQL = sSQL & "                               , " & gbllSucursal_Seguradora_ID
    sSQL = sSQL & "                               , " & gbllSeguradora_Cod_Susep
    sSQL = sSQL & "                               , " & gbllRamo_ID
    sSQL = sSQL & "                               , " & lSeq
    sSQL = sSQL & "                               ,'" & txtNmTecnico_Atribuido.Text & "'"
    sSQL = sSQL & "                               ,'" & cUserName & "'"
    
    Call Conexao_ExecutarSQL(gsSIGLASISTEMA, _
                             glAmbiente_id, _
                             App.Title, _
                             App.FileDescription, _
                             sSQL, _
                             lConexaoLocal, _
                             False)
                                 
    If (iAssessoria - 2) >= 0 Then
        'Email de aviso de recebimento...
        sSQL = ""
        sSQL = sSQL & "Select Email                                         " & vbNewLine
        sSQL = sSQL & "     , Isnull(Empresa,'')                            " & vbNewLine
        sSQL = sSQL & "  From Tecnico_Tb (NoLock)                           " & vbNewLine
        sSQL = sSQL & " Where Tecnico_ID                = " & aAssessoria(iAssessoria - 2).tecnico_id & vbNewLine
        
        Set rsRecordSet = Conexao_ExecutarSQL(gsSIGLASISTEMA, _
                                              glAmbiente_id, _
                                              App.Title, _
                                              App.FileDescription, _
                                              sSQL, _
                                              lConexaoLocal, _
                                              True)
        
        With rsRecordSet
            If Not .EOF Then
                sMail = .Fields(0)
                If .Fields(1) <> "" Then
                    sMail = sMail & "." & .Fields(1)
                End If
                sMail = sMail & "@aliancadobrasil.com.br"
            End If
        End With
        Set rsRecordSet = Nothing
        
        sSQL = ""
        sSQL = sSQL & "Exec Envia_eMail_Sp '" & sMail & "'" & vbNewLine
        sSQL = sSQL & "                  ,'Atribui��o de Sinistro'"
        sSQL = sSQL & "                  ,'Voc� acaba de receber o seguinte aviso de sinistro: " & gbldSinistro_ID & "'"
        
        Call Conexao_ExecutarSQL(gsSIGLASISTEMA, _
                                 glAmbiente_id, _
                                 App.Title, _
                                 App.FileDescription, _
                                 sSQL, _
                                 lConexaoLocal, _
                                 False)
        
            
        
    End If
    
    bConex = ConfirmarTransacao(lConexaoLocal)
    
    Exit Sub
    
'Luciana - 21/10/2005
Trata_Erro:
    bConex = RetornarTransacao(lConexaoLocal)
    TrataErroGeral "AtualizaDadosBD_Tipo2", Me.name
    End
End Sub

Private Sub cboTecnico_Atribuido_Click()
    BtnAplicar.Enabled = True
End Sub


