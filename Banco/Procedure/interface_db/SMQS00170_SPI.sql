CREATE PROCEDURE SMQS00170_SPI
 @USUARIO VARCHAR(20)    
AS BEGIN    
    
 /**************************************************************************************************    
 Data Cria��o  : 07/06/2013 (�ltima Atualiza��o: 26/07/2013)    
 Projeto       : Melhorias do SEGBR    
 Autores       : Thiago Andre Erustes    
 Descri��o     : Procedure criada em substitui��o ao SMQP0005. Todo o processo ser� executado    
      por esta procedure, que ir� rodar como um servi�o do Windows.    
 '**************************************************************************************************/    
    
 /*    
 ETAPAS    
 1 - solicitante()        - Aviso de sinistro com comunica��o, Aviso de sinistro temporariamente "sem comunica��o", Aviso de sinistro sem comunica��o, Aviso de sinistro rean�lise    
 2 - sinistro()      - Aviso de sinistro com comunica��o, Aviso de sinistro temporariamente "sem comunica��o", Aviso de sinistro sem comunica��o    
 3 - sinistro_cobertura()   - Aviso de sinistro com comunica��o, Aviso de sinistro temporariamente "sem comunica��o", Aviso de sinistro sem comunica��o    
 4 - sinistro_estimativa()  - Aviso de sinistro com comunica��o, Aviso de sinistro temporariamente "sem comunica��o", Aviso de sinistro sem comunica��o    
 5 - sinistro_historico()   - Aviso de sinistro com comunica��o, Aviso de sinistro temporariamente "sem comunica��o", Aviso de sinistro sem comunica��o    
 6 - detalhamento inicial() - Aviso de sinistro com comunica��o, Aviso de sinistro temporariamente "sem comunica��o", Aviso de sinistro sem comunica��o    
 7 - sinistro_vida/re()     - Aviso de sinistro com comunica��o, Aviso de sinistro temporariamente "sem comunica��o", Aviso de sinistro sem comunica��o    
 8 - questionario()     - Aviso de sinistro com comunica��o, Aviso de sinistro temporariamente "sem comunica��o", Aviso de sinistro sem comunica��o    
 9 - regulador()      - Aviso de sinistro com comunica��o, Aviso de sinistro temporariamente "sem comunica��o", Aviso de sinistro sem comunica��o    
 10- tecnico()         - Aviso de sinistro com comunica��o, Aviso de sinistro temporariamente "sem comunica��o", Aviso de sinistro sem comunica��o    
 11- Calcula_cosseguro()    - Aviso de sinistro com comunica��o, Aviso de sinistro temporariamente "sem comunica��o", Aviso de sinistro sem comunica��o, Aviso de sinistro rean�lise    
 12- Calcula_resseguro()    - Aviso de sinistro com comunica��o, Aviso de sinistro temporariamente "sem comunica��o", Aviso de sinistro sem comunica��o, Aviso de sinistro rean�lise    
 13- sinistro_bb()     - Aviso de sinistro com comunica��o, Aviso de sinistro rean�lise, Altera��o de aviso de sinistro para "com comunica��o"    
 14- evento 2000()     - Aviso de sinistro com comunica��o, Aviso de sinistro rean�lise, Altera��o de aviso de sinistro para "com comunica��o"    
 15- Situacao A()     - Aviso de sinistro com comunica��o, Aviso de sinistro sem comunica��o, Aviso de sinistro rean�lise, Altera��o de aviso de sinistro para "com comunica��o"    
 16- det_com_com()     - Altera��o de aviso de sinistro para "com comunica��o"    
 17- det_sem_com()     - Aviso de sinistro temporariamente "sem comunica��o",    
 18- Situacao S()     - Aviso de sinistro temporariamente "sem comunica��o",    
 19- Reabrir estimativa()   - Aviso de sinistro rean�lise    
 20- Reabertura_sinistro()  - Aviso de sinistro rean�lise    
 21- Detalhamento reanalise - Aviso de sinistro rean�lise    
    
 PROCESSOS - TP_PROCESSO    
 1 - Aviso de sinistro com comunica��o     
 2 - Aviso de sinistro temporariamente "sem comunica��o"    
 3 - Aviso de sinistro sem comunica��o    
 4 - Aviso de sinistro rean�lise    
 5 - Altera��o de aviso de sinistro para "com comunica��o"    
 */    
/*
ALTERA��ES

- GENJUNIOR - 02/04/2018
  LINHA 1035 A 1044 - INCLUS�O DO PRODUTO 1239 PARA REAN�LISE E READEQUA��O DA REGRA DE OBTEN��O DO COUNT PARA N�O PEGAR NO CODE GUARD
- GENJUNIOR / PETRAUSKAS - hi@gro - timeline
  linhas 470 a 478 - grava��o do sinistro_id na timeline
- hi@gro Aviso
  linhas 238, 342 e 465 - Inclus�o da coluna nr_ptc_aviso na tabela sinistro_tb

-- HI AGRO - 23/07/2018
   GENJUNIOR
   LINHA 246 - DECLARA��O DE NOVA VARI�VEL DO TIPO INT @timeline_id
   LINHA 490 A 547 - APLICA��O DE REGRA PARA VALIDAR SE O SINISTRO PROCESSADO � DO RURAL E POSSUI REGISTRO NA TIMELINE. CASO N�O, INCLUA-SE NOVO REGISTRO DO SINISTRO.

-- Petrauskas - 06/09/2018 - IM00475336
   Linhas 523 a 549 - incluida grava��o campo dt_timeline_sub_evento e excluido op��o CASE = 3 que passa a ser tratada no ELSE.

-  Anderson Molina Silva - 15/08/2019
   Tratativa para propostas a serem enviadas para cloud

*/    
 set nocount on    
     
 --Verifica o Status do Sistema  
 ----------------------------------------------------------------
 --Ricardo Toledo (Confitec) : 16/01/2017 : INC000005298926 : INI
 --Inclus�o da vari�vel @data_sistema para corre��o da estimativa
 --declare @status char(1)    
 --select @status = status_sistema from seguros_db.dbo.parametro_geral_tb with(nolock)    
 declare @status char(1), @data_sistema smalldatetime
 select @status = status_sistema, @data_sistema =  dt_operacional from seguros_db.dbo.parametro_geral_tb with(nolock)    
 --Ricardo Toledo (Confitec) : 16/01/2017 : INC000005298926 : FIM
 ----------------------------------------------------------------
 
 
 --S� executa se o sistema estiver Liberado    
 if @status = 'L' or @usuario = 'SEGBR' begin        
  declare @parametro table    
  (    
    tp_processo smallint    
    ,etapa smallint    
  )    
    
  --TABELA DE PARAMETROS QUE DETERMINA QUAIS ETAPAS S�O EXECUTADAS PARA CADA PROCESSO DESCRITO ACIMA    
  insert into @parametro    
     select 1,1    
  union select 1,2    
  union select 1,3    
  union select 1,4    
  union select 1,5    
  union select 1,6    
  union select 1,7    
  union select 1,8    
  union select 1,9    
  union select 1,10    
  union select 1,11    
  union select 1,12    
  union select 1,13    
  union select 1,14    
  union select 1,15    
  union select 2,1    
  union select 2,2    
  union select 2,3    
  union select 2,4    
  union select 2,5    
  union select 2,6    
  union select 2,7    
  union select 2,8    
  union select 2,9    
  union select 2,10    
  union select 2,11    
  union select 2,12    
  union select 2,17    
  union select 2,18    
  union select 3,1    
  union select 3,2    
  union select 3,3    
  union select 3,4    
  union select 3,5    
  union select 3,6    
  union select 3,7    
  union select 3,8    
  union select 3,9    
  union select 3,10    
  union select 3,11    
  union select 3,12    
  union select 3,15    
  union select 4,1    
  union select 4,11    
  union select 4,12    
  union select 4,13    
  union select 4,14    
  union select 4,15    
  union select 4,19    
  union select 4,20    
  union select 4,21    
  union select 5,13    
  union select 5,14    
  union select 5,15    
  union select 5,16    
    
  declare @debug int,    
  @data_atual smalldatetime,    
  @identificacao_retorno numeric,    
  @texto_auxiliar varchar(80),    
  @num_auxiliar int,    
  @ambiente varchar(3),    
  @etapa varchar(10),    
  @error_message nvarchar(4000)    
    
  set @etapa = 'ETAPA 00'    
  set @debug = 0 --Se precisar testar e olhar os resultados, colocar = 1    
  set @data_atual = getdate()    
  set @ambiente = RTRIM(seguros_db.dbo.fn_retornaempresa('SEGBR'))    
    
  declare @result table (res numeric)    

  --Demanda - MU00416971- Efici�ncia_Operacional_Detalhamento_Sinistro - In�cio 
  declare @verifica_restrito_web int
  declare @permite_sinistro_detalhamento char(1)
  --Demanda - MU00416971- Efici�ncia_Operacional_Detalhamento_Sinistro - Fim
     
  -- Demanda 18283294 - IN�CIO  
  Declare @sinistro_bb_ant numeric(13,0)  
  Declare @data_ant smalldatetime   
    
  Set @data_ant = DateAdd(Day, -1, Convert(Datetime, Convert(Varchar(10),@data_atual,103),103))      
  -- Demanda 18283294 - FIM    
    
  declare @evento_id int    
    ,@tp_processo smallint    
    ,@situacao_aviso char(1)    
    ,@cod_remessa varchar(16)    
    --Dados do solicitante    
    ,@sinistro_solicitante_id int    
    ,@solicitante_nome varchar(60)    
    ,@solicitante_endereco varchar(60)    
    ,@solicitante_bairro varchar(30)    
    ,@solicitante_municipio_id numeric    
    ,@solicitante_municipio varchar(60)    
    ,@solicitante_estado char(2)    
    ,@solicitante_ddd varchar(4)    
    ,@solicitante_telefone varchar(10)    
    ,@solicitante_ddd_fax varchar(4)    
    ,@solicitante_telefone_fax varchar(10)    
    ,@solicitante_cep varchar(8)    
    ,@solicitante_cpf varchar(11) -- 07/11/2019 - (ntendencia) - Aviso de Sinistro WEB   
    ,@solicitante_grau_parentesco char(2)    
    ,@solicitante_email varchar(60) --Demanda 18679319 - Adriano Pinheiro - Confitec - 20/03/2015
    --Dados do sinistro    
    ,@sinistro_bb numeric(13,0)    
    ,@banco_aviso_id bit    
    ,@ind_reanalise char(1)    
    ,@dt_aviso smalldatetime    
    ,@sinistro_id numeric(11,0)    
    ,@apolice_id numeric(9,0)    
    ,@sucursal_seguradora_id numeric(5,0)    
    ,@seguradora_cod_susep numeric(5,0)    
    ,@ramo_id int    
    ,@proposta_id int    
    ,@evento_sinistro_id int    
    ,@subevento_sinistro_id int    
    ,@dt_aviso_sinistro smalldatetime    
    ,@dt_ocorrencia_sinistro smalldatetime    
    ,@produto_id int    
    ,@sinistro_endereco varchar(60)    
    ,@sinistro_bairro varchar(30)    
    ,@sinistro_municipio_id numeric(3,0)     
    ,@sinistro_municipio varchar(30)    
    ,@sinistro_estado char(2)    
    ,@sinistro_agencia_id numeric(4,0)    
    ,@sinistro_cliente_id int    
    ,@sinistro_moeda_id numeric(3,0)    
    ,@sinistro_reintegracao_is char(1)    
    ,@sinistro_tp_ramo_id int    
    ,@sinistro_val_estimado numeric(15,2)    
    ,@sinistro_val_pago numeric(15,2)    
    ,@sinistro_tp_cobertura_id int    
    ,@sinistro_cobertura_val_estimado numeric(15,2)    
    ,@sinistro_cobertura_val_prejuizo numeric(15, 2)    
    ------ Demanda 18652121 - Inicio  
    ,@sinistro_cod_objeto_segurado int    
    ---,@sinistro_cod_objeto_segurado tinyint    
    ------ Demanda 18652121 - Fim  
    ,@sinistro_regulador_id int    
    ,@sinistro_tecnico_id int    
    ,@sinistro_tp_aviso_id int    
    ,@sinistro_tecnico_nome varchar(50)    
    ,@sinistro_tecnico_email varchar(80)    
    --Dados do sinistrado    
    ,@sinistrado_nome varchar(60)    
    ,@sinistrado_cpf varchar(11)    
    ,@sinistrado_sexo char(1)    
    ,@sinistrado_dt_nascimento smalldatetime    
    --Questionario(RE)    
    ,@questionario_id int    
    ,@questionario_pergunta_id int    
    ,@questionario_dominio_resposta_id smallint    
    ,@questionario_texto_resposta varchar(100)    
    --Email    
    ,@email_assunto varchar(60)    
    ,@email_mensagem varchar(255)    
    --Resseguro    
    ,@perc_resseguro_quota numeric(9,6)       
    ,@perc_resseguro_er numeric(9,6)    
    --Estimativa      
    ,@estimativa_seq int    
	,@nr_ptc_aviso varchar(20) -- demanda Hi@gro 06/12/2017
	,@timeline_id int -- genjunior - hi agro / 23/07/2018
    ,@endosso_id_resseg INT -- MU-2017-042360 -- Rogerio Melo 17/09/2018

	--Insere agravamentos para o novo evento_bb_id 54332 - Agravamentos Pecuário 
    exec SEGUROS_DB.dbo.SEGS13953_SPI @USUARIO

 -- genjunior - novo prote��o ouro
 -- adequa��o da contagem de sinistro_bb para n�o ser afetado no code guard
 declare @count_sinistro_bb int
 -- fim genjunior 

  --Buscar avisos pendente de processamento    
  select a.evento_id    
  into #sinistros    
  from seguros_db.dbo.evento_segbr_sinistro_atual_tb a with(nolock) 
  inner join seguros_db.dbo.ramo_tb b with(nolock)    
   on b.ramo_id = a.ramo_id    
  where a.evento_bb_id = 1100 --AVISO SINISTRO PELA CENTRAL DE ATENDIMENTO    
  and isnull(a.sinistro_id,0) > 0    
  and isnull(a.ramo_id,0) > 0    
  --Flow 17905311 - In�cio    
  --and isnull(a.apolice_id,0) > 0    
  and isnull(a.apolice_id,0) > Case When produto_id in (1152,1204, 1240) Then -1 Else 0 End     
  --Flow 17905311 - Fim    
  and (isnull(b.cod_empresa,'AB') = @ambiente)    
  and ( (a.situacao_aviso in ('N', 'C')    
     and (     
       (isnull(a.sinistro_bb,0) = 0 and a.banco_aviso_id = 0 and ind_reanalise = 'N') --Sinistro_bb zero e indicador de comunica��o N�o    
       or    
       (isnull(a.sinistro_bb,0) > 0 and a.banco_aviso_id = 1) --Sinistro_bb preenchido e indicador de comunica��o Sim    
       or    
       (isnull(a.sinistro_bb,0) = 0 and a.banco_aviso_id = 1 and datediff(hour,a.dt_inclusao,getdate()) >= 4 and a.ind_reanalise = 'N') --Sinistro_bb n�o preenchido por mais de 4 horas e indicador de comunica��o Sim    
      )    
    )    
    or    
    (a.situacao_aviso = 'S' and a.banco_aviso_id = 1 and isnull(a.sinistro_bb,0) > 0)    
   )    
   
    --C00149269 - EMISS�O RURAL NA ABS - In�cio      
   union 
   
   select a.evento_id    
  from seguros_db.dbo.evento_segbr_sinistro_atual_tb a with(nolock)    
  inner join seguros_db.dbo.ramo_tb b with(nolock)    
   on b.ramo_id = a.ramo_id    
  where a.evento_bb_id = 1100 --AVISO SINISTRO PELA CENTRAL DE ATENDIMENTO    
  and isnull(a.sinistro_id,0) > 0    
  and isnull(a.ramo_id,0) > 0    
  and isnull(a.apolice_id,0) > Case When produto_id in (1152,1204, 1240) Then -1 Else 0 End     
  and produto_id in (128,230) 
  and ( (a.situacao_aviso in ('N', 'C')    
     and (     
       (isnull(a.sinistro_bb,0) = 0 and a.banco_aviso_id = 0 and ind_reanalise = 'N') --Sinistro_bb zero e indicador de comunica��o N�o    
       or    
       (isnull(a.sinistro_bb,0) > 0 and a.banco_aviso_id = 1) --Sinistro_bb preenchido e indicador de comunica��o Sim    
       or    
       (isnull(a.sinistro_bb,0) = 0 and a.banco_aviso_id = 1 and datediff(hour,a.dt_inclusao,getdate()) >= 4 and a.ind_reanalise = 'N') --Sinistro_bb n�o preenchido por mais de 4 horas e indicador de comunica��o Sim    
      )    
    )    
    or    
    (a.situacao_aviso = 'S' and a.banco_aviso_id = 1 and isnull(a.sinistro_bb,0) > 0)    
   )  
      --C00149269 - EMISS�O RURAL NA ABS - fim   
      
  declare cursor_sinistros INSENSITIVE cursor --Demanda 18679319(INSENSITIVE) - Adriano Pinheiro - Confitec - 01/04/2015
  for    
   select evento_id from #sinistros    
       
   open cursor_sinistros    
    fetch next from cursor_sinistros into @evento_id    
         
    while @@fetch_status = 0    
    begin    
     begin try    
          
      select     
       --Dados do sinistro    
        @sinistro_bb = isnull(a.sinistro_bb,0)    
       ,@banco_aviso_id = a.banco_aviso_id    
       ,@dt_aviso = a.dt_inclusao  
       ,@ind_reanalise = a.ind_reanalise         
       ,@sinistro_id = a.sinistro_id    
       ,@apolice_id = a.apolice_id    
       ,@sucursal_seguradora_id = a.sucursal_seguradora_id    
       ,@seguradora_cod_susep = a.seguradora_cod_susep    
       ,@ramo_id = a.ramo_id    
       ,@proposta_id = a.proposta_id    
       ,@evento_sinistro_id = a.evento_sinistro_id     
       ,@subevento_sinistro_id = a.subevento_sinistro_id    
       ,@dt_aviso_sinistro = a.dt_aviso_sinistro    
       ,@dt_ocorrencia_sinistro = a.dt_ocorrencia_sinistro     
       ,@produto_id = a.produto_id    
       ,@situacao_aviso = a.situacao_aviso    
       ,@sinistro_endereco = a.endereco_sinistro    
       ,@sinistro_bairro = a.bairro_sinistro    
       ,@sinistro_municipio_id = a.municipio_id_sinistro    
       ,@sinistro_municipio = a.municipio_sinistro    
       ,@sinistro_estado = isnull(a.estado_sinistro,'XX')    
       ,@sinistro_agencia_id = a.agencia_aviso_id    
       ,@sinistro_cliente_id = a.cliente_id    
       ,@sinistro_moeda_id = a.moeda_id    
       ,@sinistro_reintegracao_is = a.processa_reintegracao_is  

       -- MFrasca - 15/08/14 - Tratamento para produto Microsseguro que apesar do ramo ser de Danos deve ser tratado como Vida  
       --,@sinistro_tp_ramo_id = b.tp_ramo_id  
       ,@sinistro_tp_ramo_id = case a.produto_id when 1218 then 1  
        -- Gleison.Pimentel - Nova Consultoria - 23/10/14 - Tratamento para produto habitacionais que apesar do ramo ser de Vida deve ser tratado como Danos   
         when 1226 then 2   
         when 1227 then 2   
         else b.tp_ramo_id end    
       -- Fim - MFrasca      

       --,@sinistro_val_estimado = case isnull(a.val_informado,0) when 0 then 50 else a.val_informado end    
       --Dados do solicitante    
       ,@solicitante_nome = a.nome_solicitante    
       ,@solicitante_endereco = a.endereco_solicitante    
       ,@solicitante_bairro = a.solicitante_bairro    
       ,@solicitante_municipio_id = a.solicitante_municipio_id    
       ,@solicitante_municipio = a.solicitante_municipio    
       ,@solicitante_estado = isnull(a.solicitante_estado,'XX')    
       ,@solicitante_ddd = a.ddd_solicitante    
       ,@solicitante_telefone = a.telefone_solicitante    
       ,@solicitante_ddd_fax = a.ddd1    
       ,@solicitante_telefone_fax = a.telefone1    
       ,@solicitante_cep = a.solicitante_cep    
       ,@solicitante_cpf = a.solicitante_cpf -- 07/11/2019 - (ntendencia) - Aviso de Sinistro WEB
       ,@solicitante_grau_parentesco = a.solicitante_grau_parentesco
       ,@solicitante_email = a.solicitante_email --Demanda 18679319 - Adriano Pinheiro - Confitec - 20/03/2015    
       --Dados do sinistrado    
       ,@sinistrado_nome = a.nome_sinistrado    
       ,@sinistrado_cpf = a.cpf_sinistrado    
       ,@sinistrado_sexo = case isnull(a.sexo_sinistrado,0) when '0' then null when 'I' then 'N' else a.sexo_sinistrado end    
       ,@sinistrado_dt_nascimento = a.dt_nascimento_sinistrado    
       ,@cod_remessa = a.num_remessa    
	   ,@nr_ptc_aviso = a.nr_ptc_aviso -- demanda Hi@gro 06/12/2017  
	   ,@endosso_id_resseg = a.endosso_id_resseg  -- MU-2017-042360 -- Rogério Melo 17/09/2018
      from seguros_db.dbo.evento_segbr_sinistro_atual_tb a with(nolock)    
      inner join seguros_db.dbo.ramo_tb b with(nolock)    
       on b.ramo_id = a.ramo_id    
      where evento_id = @evento_id    
    
      begin tran        


       --Identifica��o do tipo do processo    
       if @banco_aviso_id = 1     
        if @sinistro_bb > 0    
         if @ind_reanalise = 'S'    
          set @tp_processo = 4 --Aviso de sinistro rean�lise    
         else    
          if @situacao_aviso = 'S'    
           set @tp_processo = 5 --Altera��o de aviso de sinistro para "com comunica��o"    
          else    
           set @tp_processo = 1 --Aviso de sinistro com comunica��o     
        else    
         set @tp_processo = 2 --Aviso de sinistro temporariamente "sem comunica��o"    
       else --banco_aviso_id = 0    
        set @tp_processo = 3 --Aviso de sinistro sem comunica��o    
            
        -- IM00298465 - (MU00416967 - Melhorias_SEGP0794_Aviso_Sinistro - Corretiva) In�cio - Evaldo Porto 11/04/2018
        IF (@produto_id = 1201 AND @ramo_id = 61) OR 
           (@produto_id IN (1226, 1227) AND @ramo_id = 77) 
        BEGIN
            IF @evento_sinistro_id IS NOT NULL 
            BEGIN
                -- As coberturas foram tratadas por evento sinistro devido ao campo tp_cobertura_id vir NULL na tabela evento_segbr_sinistro_atual_tb.
                -- Caso seja acrescentada uma nova cobertura MIP para os produtos/ramos tratados acima, � necess�rio acrescentar os eventos de sinistro relacionados no IN abaixo.
                IF @evento_sinistro_id IN (28 ,99 ,100 ,104 ,278 ,426 ,9301 ,277) SET @sinistro_tp_ramo_id = 1 -- PARA AS COBERTURAS MIP tp_ramo_id = 1 VD
                ELSE SET @sinistro_tp_ramo_id = 2                                                              -- PARA AS COBERTURAS DFI tp_ramo_id = 2 RE
            END
            ELSE
            BEGIN 
                -- Trata os casos com @evento_sinistro_id = null - problema comum para as produtos 1226 e 1227
                -- Reajusta o sinistro_tp_ramo_id de acordo com o Ramo 77 (desfazendo o Hard-Code acima)
                SELECT @sinistro_tp_ramo_id = b.tp_ramo_id
                  FROM seguros_db.dbo.ramo_tb b WITH(NOLOCK)    
                 WHERE b.ramo_id = @ramo_id
                   AND @produto_id IN (1226, 1227)
            END
        END
        -- IM00298465 - (MU00416967 - Melhorias_SEGP0794_Aviso_Sinistro - Corretiva) Fim
       
       if exists(select 1 from @parametro where tp_processo = @tp_processo and etapa = 1)    
       begin    
        --Etapa 1: Gera��o do solicitante do sinistro    
        set @etapa = 'ETAPA 01'    
            
        select @sinistro_solicitante_id = max(isnull(solicitante_id,0)) + 1      
        from seguros_db.dbo.solicitante_sinistro_tb      
    
        exec seguros_db.dbo.solicitante_sinistro_spi      
         @solicitante_id = @sinistro_solicitante_id      
         ,@nome = @solicitante_nome      
		 ,@endereco = @solicitante_endereco    
         ,@bairro = @solicitante_bairro      
         ,@municipio_id = @solicitante_municipio_id      
         ,@municipio = @solicitante_municipio      
         ,@estado = @solicitante_estado      
         ,@ddd = @solicitante_ddd      
         ,@telefone = @solicitante_telefone  
         ,@ddd_fax = @solicitante_ddd_fax    
         ,@fax = @solicitante_telefone_fax      
         ,@usuario = @usuario  
         ,@email = @solicitante_email --Demanda 18679319 - Adriano Pinheiro - Confitec - 20/03/2015
         ,@CPF = @solicitante_cpf -- 07/11/2019 - (ntendencia) - Aviso de Sinistro WEB
		 ,@CEP = @solicitante_cep -- 06/04/2020 - (ntendencia) - Aviso de Sinistro WEB
            
        if @debug = 1    
         select 'ETAPA 01', *   
         from seguros_db.dbo.solicitante_sinistro_tb with(nolock)    
         where solicitante_id = @sinistro_solicitante_id    
       end    
           
       if exists(select 1 from @parametro where tp_processo = @tp_processo and etapa = 2)    
       begin    
        --Etapa 2: Inclus�o do sinistro    
   set @etapa = 'ETAPA 02'    
        
        -- MU00416967 - Melhorias_SEGP0794_Aviso_Sinistro - Laurent Silva - 27/03/2018
        --if @sinistro_tp_ramo_id = 1 or ( @produto_id = 1201 and @ramo_id = 61 ) or ( @produto_id = 1226 and @ramo_id = 77 ) or ( @produto_id = 1227 and @ramo_id = 77 ) 
                        --Se o ramo for Vida, ent�o os dados do endere�o do sinistro ser�o os dados do solicitante
        -- IM00298465 - Desfeito - Evaldo Porto 11/04/2018
		if @sinistro_tp_ramo_id = 1 --Se o ramo for Vida, ent�o os dados do endere�o do sinistro ser�o os dados do solicitante    
        begin    
         --set @sinistro_tp_ramo_id = 1
         set @sinistro_endereco = @solicitante_endereco    
         set @sinistro_bairro = @solicitante_bairro    
         set @sinistro_municipio_id = @solicitante_municipio_id    
         set @sinistro_municipio = @solicitante_municipio    
         set @sinistro_estado = @solicitante_estado    
        end    

		-- Evaldo porto - 27/08/2018 - Demanda de produto agropecu�rio 1240
        ---------------------------------------------------------------------          
        declare @situacao_pecuario char(1),
                @pecuario_basica char(1)
        set @situacao_pecuario = '0'
        set @pecuario_basica = 'N'
            
        select @situacao_pecuario = '8',
               @pecuario_basica = 'S'
        from seguros_db.dbo.evento_segbr_sinistro_cobertura_tb cobertura with(nolock)
		inner Join seguros_db.dbo.evento_segbr_sinistro_atual_tb atual with(nolock) -- D�nis Tavares 31/05/2019 - Demanda de produto agropecu�rio 1240 Evento Sinistro 430 - FATURAMENTO OBTIDO MENOR QUE O FATURAMENTO GARANTIDO 
		on cobertura.evento_id = atual.evento_id 
        where cobertura.evento_id = @evento_id
        and cobertura.tp_cobertura_id = 1169 
		and evento_sinistro_id <> 430 -- D�nis Tavares 31/05/2019 - Demanda de produto agropecu�rio 1240 Evento Sinistro 430 - FATURAMENTO OBTIDO MENOR QUE O FATURAMENTO GARANTIDO 
        -- Evaldo porto - 27/08/2018 - Demanda de produto agropecu�rio 1240
        ---------------------------------------------------------------------
                  
        exec seguros_db.dbo.sinistro_spi      
         @sinistro_id = @sinistro_id      
         ,@apolice_id = @apolice_id      
         ,@sucursal_seguradora_id = @sucursal_seguradora_id      
         ,@seguradora_cod_susep = @seguradora_cod_susep      
         ,@ramo_id = @ramo_id      
         ,@solicitante_id = @sinistro_solicitante_id      
         ,@proposta_id = @proposta_id      
         ,@evento_sinistro_id = @evento_sinistro_id      
         ,@dt_aviso_sinistro = @dt_aviso_sinistro      
         ,@dt_ocorrencia_sinistro = @dt_ocorrencia_sinistro
		 ,@dt_entrada_seguradora = @data_sistema     -- Altera��o devido impacto no fechamento - Cleber Sardo - 22.05.2019
         --,@dt_entrada_seguradora = @dt_aviso_sinistro      
         ,@endereco = @sinistro_endereco    
         ,@bairro = @sinistro_bairro    
         ,@municipio_id = @sinistro_municipio_id    
         ,@municipio = @sinistro_municipio    
         ,@estado = @sinistro_estado    
   ,@agencia_id = @sinistro_agencia_id      
         ,@banco_id = @banco_aviso_id     
         ,@cliente_id = @sinistro_cliente_id    
         --,@situacao = '0'    
         -- Evaldo porto - 27/08/2018 - Demanda de produto agropecu�rio 1240
         ,@situacao = @situacao_pecuario    
         -- Evaldo porto        
         ,@usuario = @usuario      
         ,@sinistro_id_lider = null      
         ,@moeda_id = @sinistro_moeda_id    
         ,@processa_reintegracao_is = @sinistro_reintegracao_is  
         ,@tp_ramo_id =  @sinistro_tp_ramo_id    
         ,@subevento_sinistro_id = @subevento_sinistro_id  
		 ,@nr_ptc_aviso = @nr_ptc_aviso -- demanda Hi@gro 06/12/2017
		 ,@endosso_id_resseg = @endosso_id_resseg -- MU-2017-042360 -- Rogerio Melo 17/09/2018
              
           if @debug = 1    
              select 'ETAPA 02', *     
              from seguros_db.dbo.sinistro_tb with(nolock)    
              where sinistro_id = @sinistro_id 
			  
			  -- Evaldo Porto 30/08/2018 - Demanda de produto agropecu�rio 1240
        -- Somente cobertura b�sica 1169 insere agravamento
        IF @pecuario_basica = 'S'
        BEGIN
            DECLARE @qtd_machos int,
                    @qtd_femeas int,
                    @val_prejuizo_agravado numeric(17,2)
 
            SELECT @qtd_machos = qtd_machos,
                   @qtd_femeas = qtd_femeas,
                   @val_prejuizo_agravado = val_prejuizo_agravado
              FROM seguros_db.dbo.evento_segbr_sinistro_agravamento_pecuario_tb with(nolock)
              --FROM seguros_db..dbo.evento_segbr_agravamento_pecuario_tb with(nolock)
             WHERE evento_id = @evento_id
 
             --EXEC desenv_db.dbo.sinistro_agravamento_pecuario_spi @sinistro_id, @qtd_machos, @qtd_femeas, @val_prejuizo_agravado, @USUARIO
             EXEC seguros_db.dbo.SEGS13929_SPI @evento_id, @sinistro_id, @qtd_machos, @qtd_femeas, @val_prejuizo_agravado, @USUARIO          
 
             IF @debug = 1    
                SELECT 'ETAPA 02.pecu�rio.1240', *     
                  FROM seguros_db.dbo.sinistro_agravamento_pecuario_tb with(nolock)    
                  --FROM seguros_db.dbo.sinistro_agravamento_pecuario_tb with(nolock)    
                 WHERE sinistro_id = @sinistro_id   
    END
        -- Evaldo Porto 30/08/2018 - Demanda de produto agropecu�rio 1240 (fim)   
         
		-- genjunior / petrauskas - hi@gro - atualiza��o da tabela de timeline
		IF isnull(@nr_ptc_aviso,'') <> ''
		 begin
		    update a
		       set a.sinistro_id = @sinistro_id
		      from interface_dados_db.dbo.aviso_web_comunicacao_sinistro_timeline_tb a
		     where a.num_protocolo = @nr_ptc_aviso
         end
		-- fim genjunior / petrauskas

		-- GENJUNIOR - HI AGRO - 23/07/2018 - INCLUS�O DE REGISTRO DA TIMELINE PARA SINISTROS DO RURAL
		if exists(select 1
		            from interface_dados_db.dbo.aviso_web_produto_tb with (nolock)
				   where produto_id = @produto_id)
			begin
				if exists(select 1
				            from seguros_db.dbo.ramo_tb with (nolock)
						   where ramo_id = @ramo_id
						     and tp_area = 'RU') -- genjunior ==> inclus�o inicialmente apenas para produtos do Rural
					begin
						if not exists(select 1
						                from interface_dados_db.dbo.aviso_web_comunicacao_sinistro_timeline_tb with (nolock)
									   where sinistro_id = @sinistro_id
									      or sinistro_bb = @sinistro_bb)
							begin
								insert 
								  into interface_dados_db.dbo.aviso_web_comunicacao_sinistro_timeline_tb
									 ( sinistro_id
									 , sinistro_bb
									 , num_protocolo
									 , dt_inclusao
									 , usuario
									 )
								values 
								     ( @sinistro_id
									 , @sinistro_bb
									 , @nr_ptc_aviso
									 , getdate()
									 , @usuario
									 )
								
								set @timeline_id = scope_identity()
                                /*+--------------------------------------------------------------
                                  | 06/09/2018 - IM00475336 - incluido grava��o campo dt_timeline_sub_evento / excluido op��o case = 3
                                */
								insert 
								  into interface_dados_db.dbo.aviso_web_comunicacao_sinistro_timeline_atividade_tb
                                     ( aviso_web_comunicacao_sinistro_timeline_id
			                         , aviso_web_timeline_evento_id 
			                         , aviso_web_timeline_sub_evento_id 
			                         , aviso_web_timeline_status_id 
			                         , dt_inclusao 
                                     , dt_timeline_sub_evento 
			                         , usuario 
									 )
                                select @timeline_id
								     , evento.aviso_web_timeline_evento_id
									 , subevento.aviso_web_timeline_sub_evento_id
                                     , case when subevento.aviso_web_timeline_sub_evento_id = 1 Then 1 
									        when subevento.aviso_web_timeline_sub_evento_id = 2 Then 1 
									        --when subevento.aviso_web_timeline_sub_evento_id = 3 Then 1 
									        when subevento.aviso_web_timeline_sub_evento_id = 4 Then 2 
                                            else 4 end
                                     , getdate()
                                     , getdate()
									 , @usuario
                                  from interface_dados_db.dbo.aviso_web_timeline_evento_tb evento with (nolock)
                                 inner join interface_dados_db.dbo.aviso_web_timeline_sub_evento_tb subevento with (nolock) 
                                    on evento.aviso_web_timeline_evento_id = subevento.aviso_web_timeline_evento_id
							end
					end

			end
		-- FIM GENJUNIOR

---18225203- ajuste no m�dulo de exigencia --CRIADA APARTI DO SEGP9088 =>INCLUIDOCUMENTACAOBASIC
        EXEC interface_db.dbo.SMQS00193_SPI  @sinistro_id--INCLUIR A DOCUMENTA��O BASICA NA ABERTURA DO SINISTRO AO GERAR O EVENTO 1100.
        --EXEC SMQS00194_SPU   --UPDATE PARA SALDO DEVEDOR (SEGUROS_DB.DBO.EXIGENCIA_TB)

		----------- In�cio: Captura de propostas aptas para envio Cloud - Anderson Molina Silva - Confitec -------------- 15/08/2019
		IF NOT EXISTS(SELECT TOP (1) C.evento_id
					    FROM interface_dados_db.DBO.proposta_envio_cloud_tb c with (nolock)
			      INNER JOIN seguros_db.dbo.evento_segbr_sinistro_atual_tb a with(nolock)
				 		  ON c.sinistro_id = a.sinistro_id
				  INNER JOIN seguros_db.dbo.ramo_tb b with(nolock)      
						  ON b.ramo_id = a.ramo_id          
				  INNER JOIN interface_dados_db.dbo.parametro_produto_envio_cloud_tb parametro WITH (NOLOCK)
						  ON parametro.produto_id = @produto_id
					   WHERE c.evento_id = @evento_id)
			BEGIN
				IF (SELECT TOP (1) ativo
					  FROM interface_dados_db.DBO.processo_envio_cloud_tb
					 WHERE processo_envio_cloud_id = 8) = 'S'

					BEGIN
						INSERT INTO interface_dados_db.dbo.proposta_envio_cloud_tb 
									(proposta_id,
									status_envio,
									sinistro_id,
									evento_id,
									num_processo,
									dt_inclusao,
									usuario)
							 SELECT @proposta_id,
									'N',
									a.sinistro_id,
									@evento_id,
									8, -- Processo Sinistro 
									getdate(),
									@usuario
							   FROM seguros_db.dbo.evento_segbr_sinistro_atual_tb a with(nolock)
						 inner join seguros_db.dbo.ramo_tb b with(nolock)      
								 ON b.ramo_id = a.ramo_id          
						 INNER JOIN interface_dados_db.dbo.parametro_produto_envio_cloud_tb parametro WITH (NOLOCK)
								 ON parametro.produto_id = @produto_id
							  WHERE a.evento_id = @evento_id
					END
			END
				
		
	
	----------- Fim: Captura de propostas aptas para envio Cloud - Anderson Molina Silva - Confitec -------------- 15/08/2019


--18225203- ajuste no m�dulo de exigencia            
             if @debug = 1    
              select 'ETAPA 02.1', *     
              from seguros_db.dbo.exigencia_tb with(nolock)    
              where sinistro_id = @sinistro_id 
  
       end    
           
       if exists(select 1 from @parametro where tp_processo = @tp_processo and etapa = 3)    
       begin    
        --Etapa 3: Inclus�o de cobertura    
        set @etapa = 'ETAPA 03'    
            
        set @sinistro_val_estimado = 0    
    
        IF @DEBUG = 1 PRINT CONVERT(VARCHAR(MAX), GETDATE(), 14) + ' - (ETAPA 03) SELECIONANDO ESTIMATIVAS DA EVENTO_SEGBR_SINISTRO_COBERTURA_TB'    
        declare cursor_cobertura INSENSITIVE cursor --Demanda 18679319(INSENSITIVE) - Adriano Pinheiro - Confitec - 01/04/2015
        for    
         select tp_cobertura_id, val_estimativa, val_prejuizo    
         from seguros_db.dbo.evento_segbr_sinistro_cobertura_tb with(nolock)    
         where evento_id = @evento_id    
             
         open cursor_cobertura    
          fetch next from cursor_cobertura into @sinistro_tp_cobertura_id, @sinistro_cobertura_val_estimado, @sinistro_cobertura_val_prejuizo    
          while @@fetch_status = 0    
          begin    
              
           if @sinistro_cobertura_val_prejuizo < @sinistro_cobertura_val_estimado begin    
            if @sinistro_cobertura_val_prejuizo = 0 begin    
            set @sinistro_cobertura_val_estimado = 50    
            end    
            else begin    
             set @sinistro_cobertura_val_estimado = @sinistro_cobertura_val_prejuizo  
            end    
           end    
  else begin    
            if @sinistro_cobertura_val_estimado = 0 begin    
             set @sinistro_cobertura_val_estimado = 50    
            end    
            else begin    
             set @sinistro_cobertura_val_estimado = @sinistro_cobertura_val_estimado    
            end    
           end    
              
           IF @DEBUG = 1 PRINT CONVERT(VARCHAR(MAX), GETDATE(), 14) + ' - (ETAPA 03) INSERINDO TP_COBERTURA_ID: ' + CONVERT(VARCHAR(MAX), @sinistro_tp_cobertura_id) + ' COM VALOR DE ESTIMATIVA: ' + CONVERT(VARCHAR(MAX), @sinistro_cobertura_val_estimado) 










           exec seguros_db.dbo.sinistro_cobertura_spi      
             @sinistro_id = @sinistro_id      
            ,@apolice_id = @apolice_id                  ,@sucursal_seguradora_id = @sucursal_seguradora_id      
            ,@seguradora_cod_susep = @seguradora_cod_susep      
            ,@ramo_id = @ramo_id      
            ,@tp_cobertura_id = @sinistro_tp_cobertura_id    
            ,@tp_indenizacao = 1      
            ,@val_estimativa = @sinistro_cobertura_val_estimado    
            ,@usuario = @usuario      
            ,@Processamento = 'B'    
    
            if @sinistro_cobertura_val_prejuizo <= @sinistro_cobertura_val_estimado begin    
             if @sinistro_cobertura_val_prejuizo = 0 begin    
              set @sinistro_val_estimado = @sinistro_val_estimado + 50    
             end    
             else begin    
              set @sinistro_val_estimado = @sinistro_val_estimado + @sinistro_cobertura_val_prejuizo    
             end     
            end    
         else begin    
             if @sinistro_cobertura_val_estimado = 0 begin    
              set @sinistro_val_estimado = @sinistro_val_estimado + 50    
             end    
             else begin    
              set @sinistro_val_estimado = @sinistro_val_estimado + @sinistro_cobertura_val_estimado    
             end    
     end    
              
           fetch next from cursor_cobertura into @sinistro_tp_cobertura_id, @sinistro_cobertura_val_estimado, @sinistro_cobertura_val_prejuizo    
          end    
         close cursor_cobertura    
        deallocate cursor_cobertura    
    
        IF @DEBUG = 1 PRINT CONVERT(VARCHAR(MAX), GETDATE(), 14) + ' - (ETAPA 03) VALOR TOTAL DA ESTIMATIVA: ' + CONVERT(VARCHAR(MAX), @sinistro_val_estimado)    
            
        if @debug = 1    
         select 'ETAPA 03',*     
         from seguros_db.dbo.sinistro_cobertura_tb with(nolock)    
         where sinistro_id = @sinistro_id    
       end    
           
       if exists(select 1 from @parametro where tp_processo = @tp_processo and etapa = 4)    
       begin    
        --Etapa 4: Inclus�o de estimativa    
        set @etapa = 'ETAPA 04'    
           
        if @sinistro_tp_ramo_id = 2 --RE    
        begin    
         IF @DEBUG = 1 PRINT CONVERT(VARCHAR(MAX), GETDATE(), 14) + ' - (ETAPA 04) SINISTRO_TP_RAMO_ID = 2 (RE) '     
         if isnull(@proposta_id,0) = 0    
         begin    
          IF @DEBUG = 1 PRINT CONVERT(VARCHAR(MAX), GETDATE(), 14) + ' - (ETAPA 04) SEM PROPOSTA_ID'     
          set @perc_resseguro_quota = 0    
          set @perc_resseguro_er = 0    
         end    
         else    
         begin    
          IF @DEBUG = 1 PRINT CONVERT(VARCHAR(MAX), GETDATE(), 14) + ' - (ETAPA 04) SELECIONANDO PERCENTUAL DE COTA'     
          SELECT @perc_resseguro_quota = isnull(cr.perc_negociado,0)     
          FROM resseg_db.dbo.re_seguro_item_tb rs with(nolock)    
           JOIN resseg_db.dbo.composicao_res_item_tb cr with(nolock)    
            ON cr.re_seguro_item_id = rs.re_seguro_item_id    
           JOIN resseg_db.dbo.plano_tb pr with(nolock)    
            ON pr.cod_plano = cr.cod_plano_re_seguro    
          WHERE rs.proposta_id = @proposta_id    
           AND cr.cod_item_financeiro = 'PREMIO'    
           AND pr.nome = 'QUOTA' 
           AND rs.cod_objeto_segurado = 1    
    
          IF @DEBUG = 1 PRINT CONVERT(VARCHAR(MAX), GETDATE(), 14) + ' - (ETAPA 04) SELECIONANDO PERCENTUAL DE ER'     
          SELECT @perc_resseguro_er = isnull(cr.perc_negociado,0)     
          FROM resseg_db.dbo.re_seguro_item_tb rs with(nolock)    
           JOIN resseg_db.dbo.composicao_res_item_tb cr with(nolock)    
            ON cr.re_seguro_item_id = rs.re_seguro_item_id    
           JOIN resseg_db.dbo.plano_tb pr with(nolock)     
            ON pr.cod_plano = cr.cod_plano_re_seguro    
          WHERE rs.proposta_id = @proposta_id    
           AND cr.cod_item_financeiro = 'PREMIO'    
           AND pr.nome = 'EXCEDENTE DE RESPONSABILIDADE'    
           AND rs.cod_objeto_segurado = 1    
         end    
        end    
    
        IF @DEBUG = 1 PRINT CONVERT(VARCHAR(MAX), GETDATE(), 14) + ' - (ETAPA 04) GRAVANDO A ESTIMATIVA'     
        exec seguros_db.dbo.sinistro_estimativa_spi      
          @sinistro_id = @sinistro_id      
         ,@apolice_id = @apolice_id      
         ,@sucursal_seguradora_id = @sucursal_seguradora_id      
         ,@seguradora_cod_susep = @seguradora_cod_susep      
         ,@ramo_id = @ramo_id      
		 ----------------------------------------------------------------
		 --Ricardo Toledo (Confitec) : 16/01/2017 : INC000005298926 : INI
		 --Altera��o da vari�vel @data_sistema para corre��o da estimativa
         --,@dt_inicio_estimativa = @data_atual      
         ,@dt_inicio_estimativa = @data_sistema
		 --Ricardo Toledo (Confitec) : 16/01/2017 : INC000005298926 : FIM
		 ----------------------------------------------------------------
         ,@item_val_estimativa = 1      
         ,@seq_estimativa = 1      
         ,@val_estimado = @sinistro_val_estimado    
         ,@val_pago = 0      
         ,@val_resseguro = null      
         ,@perc_re_seguro_quota = @perc_resseguro_quota    
         ,@perc_re_seguro_er = @perc_resseguro_er    
         ,@situacao = ''      
         ,@usuario = @usuario      
         ,@num_carta_seguro_aceito = null      
         ,@Processamento = 'B'    
    
        if @debug = 1    
         select 'ETAPA 04', *     
         from seguros_db.dbo.sinistro_estimativa_tb with(nolock)    
         where sinistro_id = @sinistro_id    
       end    
           
       if exists(select 1 from @parametro where tp_processo = @tp_processo and etapa = 5)    
       begin    
        --Etapa 5: Inclus�o de hist�rico    
        set @etapa = 'ETAPA 05'    
            
        exec @identificacao_retorno = seguros_db.dbo.sinistro_historico_spi      
          @sinistro_id = @sinistro_id      
         ,@apolice_id = @apolice_id      
         ,@sucursal_seguradora_id = @sucursal_seguradora_id      
         ,@seguradora_cod_susep = @seguradora_cod_susep      
         ,@ramo_id = @ramo_id      
         ,@evento_SEGBR_id = 10142      
         ,@situacao = '0'      
         ,@motivo = null      
         --,@dt_evento = @data_atual   Alterado a data devido impacto no fechamento na gera��o de reserva 02.10.2018  - CLEBER SARDO 
		 ,@dt_evento = @data_sistema
         ,@usuario = @usuario      
         ,@retorna_seq_evento = 'n'    
            
        set @texto_auxiliar = 'N. DO SINISTRO: ' + cast(@sinistro_id as varchar)      
        exec seguros_db.dbo.sinistro_item_historico_spi      
          @sinistro_id = @sinistro_id      
         ,@apolice_id = @apolice_id      
         ,@sucursal_seguradora_id = @sucursal_seguradora_id      
         ,@seguradora_cod_susep = @seguradora_cod_susep      
         ,@ramo_id = @ramo_id      
         ,@seq_evento = @identificacao_retorno      
         ,@item = @texto_auxiliar      
         ,@usuario = @usuario      
		 
	    --Nova Tendencia (C00216281-esteira-pagamento-imediato-vida-individual)
		
		exec seguros_db.dbo.SEGS14674_SPI 
		  @sinistro_id = @sinistro_id      
         ,@apolice_id = @apolice_id      
         ,@sucursal_seguradora_id = @sucursal_seguradora_id      
         ,@seguradora_cod_susep = @seguradora_cod_susep      
         ,@ramo_id = @ramo_id                
         ,@usuario = @usuario      
                  
        if @debug = 1    
        begin    
         select 'ETAPA 05', *     
         from seguros_db.dbo.sinistro_historico_tb with(nolock)    
         where sinistro_id = @sinistro_id    
     and seq_evento = @identificacao_retorno    
    
         select 'ETAPA 05', *    
         from seguros_db.dbo.sinistro_item_historico_tb with(nolock)    
         where sinistro_id = @sinistro_id    
          and seq_evento = @identificacao_retorno    
        end    
       end    
           
   if exists(select 1 from @parametro where tp_processo = @tp_processo and etapa = 6)    
       begin    
        --Etapa 6: Inclus�o de detalhamento    
        set @etapa = 'ETAPA 06'    
        		    
        insert into @result    
        exec @identificacao_retorno = seguros_db.dbo.sinistro_detalhamento_spi    
          @sinistro_id = @sinistro_id    
         ,@apolice_id = @apolice_id    
         ,@sucursal_seguradora_id = @sucursal_seguradora_id    
         ,@seguradora_cod_susep = @seguradora_cod_susep    
         ,@ramo_id = @ramo_id    
         ,@dt_detalhamento = @data_atual    
         ,@tp_detalhamento = 0    
         ,@restrito = 'n'    
         ,@usuario = @usuario    
            
        declare cursor_detalhamento INSENSITIVE cursor --Demanda 18679319(INSENSITIVE) - Adriano Pinheiro - Confitec - 01/04/2015
        for    
         select seq_detalhe, isnull(left(descricao,80), '')    
         from seguros_db.dbo.evento_segbr_sinistro_detalhe_atual_tb with(nolock)    
         where evento_id = @evento_id    
         order by seq_detalhe    
    
         open cursor_detalhamento    
          fetch next from cursor_detalhamento into @num_auxiliar, @texto_auxiliar    
          while @@fetch_status = 0    
          begin    
           exec seguros_db.dbo.sinistro_linha_detalhe_spi    
             @sinistro_id = @sinistro_id    
            ,@apolice_id = @apolice_id    
            ,@sucursal_seguradora_id = @sucursal_seguradora_id     
            ,@seguradora_cod_susep = @seguradora_cod_susep    
            ,@ramo_id = @ramo_id    
            ,@detalhamento_id = @identificacao_retorno 
            ,@linha_id = @num_auxiliar    
            ,@linha = @texto_auxiliar    
            ,@usuario = @usuario    
                
           fetch next from cursor_detalhamento into @num_auxiliar, @texto_auxiliar    
          end    
         close cursor_detalhamento    
        deallocate cursor_detalhamento    
            
        if @debug = 1    
        begin    
         select 'ETAPA 06', *     
         from seguros_db.dbo.sinistro_detalhamento_tb with(nolock)    
         where sinistro_id = @sinistro_id and detalhamento_id = @identificacao_retorno    
             
         select 'ETAPA 06', *     
         from seguros_db.dbo.sinistro_linha_detalhamento_tb with(nolock)    
         where sinistro_id = @sinistro_id and detalhamento_id = @identificacao_retorno    
        end    
       end    
           
       if exists(select 1 from @parametro where tp_processo = @tp_processo and etapa = 7)    
       begin    
        --Etapa 7: Inclus�o de Sinistro_Vida/RE    
        set @etapa = 'ETAPA 07'    
        
        /* Os produtos 1201 ramo 61 e 1226 e 1227 ramo 77 devem fazer o processo de RE independente de @sinistro_tp_ramo_id */
        IF (@produto_id = 1201 AND @ramo_id = 61) OR 
           (@produto_id IN (1226, 1227) AND @ramo_id = 77) 
        begin    
         if isnull(@proposta_id,0) > 0    
         begin    
          select @dt_ocorrencia_sinistro = dt_inicio_vigencia    
          from seguros_db.dbo.apolice_tb with(nolock)    
          where apolice_id = @apolice_id    
           and sucursal_seguradora_id = @sucursal_seguradora_id    
           and ramo_id = @ramo_id    
         end    
             
         select @sinistro_cod_objeto_segurado = cod_objeto_sinistrado    
         from seguros_db.dbo.evento_segbr_sinistro_objeto_sinistrado_tb with(nolock)    
         where evento_id = @evento_id    
             
         exec seguros_db.dbo.sinistro_re_spi    
 @sinistro_id = @sinistro_id    
          ,@apolice_id = @apolice_id    
      ,@sucursal_seguradora_id = @sucursal_seguradora_id    
          ,@seguradora_cod_susep = @seguradora_cod_susep    
          ,@ramo_id = @ramo_id    
          ,@cod_objeto_segurado = @sinistro_cod_objeto_segurado   
          ,@dt_inicio_vigencia_seg = @dt_ocorrencia_sinistro    
          ,@usuario = @usuario    
             
         if @debug = 1    
          select 'ETAPA 07', *     
          from seguros_db.dbo.sinistro_re_tb with(nolock)    
          where sinistro_id = @sinistro_id     
        end    
        else
begin
            if @sinistro_tp_ramo_id = 1 --Vida    
            begin    
             exec seguros_db.dbo.sinistro_vida_spi        
               @Sinistro_id = @Sinistro_id      
              ,@Apolice_id = @Apolice_id      
              ,@Sucursal_seguradora_id = @Sucursal_seguradora_id      
              ,@Seguradora_cod_susep = @Seguradora_cod_susep      
              ,@Ramo_id = @Ramo_id      
              ,@Nome = @sinistrado_nome    
              ,@CPF = @sinistrado_cpf    
              ,@Sexo = @sinistrado_sexo    
              ,@Dt_Nascimento = @sinistrado_dt_nascimento    
              ,@usuario = @usuario     
              
             if @debug = 1    
              select 'ETAPA 07', *     
              from seguros_db.dbo.sinistro_vida_tb with(nolock)    
              where sinistro_id = @sinistro_id     
            end    
           else --RE    
            begin    
             if isnull(@proposta_id,0) > 0    
             begin    
              select @dt_ocorrencia_sinistro = dt_inicio_vigencia    
              from seguros_db.dbo.apolice_tb with(nolock)    
              where apolice_id = @apolice_id    
               and sucursal_seguradora_id = @sucursal_seguradora_id    
               and ramo_id = @ramo_id    
             end    
             
             select @sinistro_cod_objeto_segurado = cod_objeto_sinistrado    
             from seguros_db.dbo.evento_segbr_sinistro_objeto_sinistrado_tb with(nolock)    
             where evento_id = @evento_id    
             
             exec seguros_db.dbo.sinistro_re_spi    
               @sinistro_id = @sinistro_id    
              ,@apolice_id = @apolice_id    
              ,@sucursal_seguradora_id = @sucursal_seguradora_id    
              ,@seguradora_cod_susep = @seguradora_cod_susep    
              ,@ramo_id = @ramo_id    
              ,@cod_objeto_segurado = @sinistro_cod_objeto_segurado    
              ,@dt_inicio_vigencia_seg = @dt_ocorrencia_sinistro    
              ,@usuario = @usuario    
             
             if @debug = 1    
              select 'ETAPA 07', *     
              from seguros_db.dbo.sinistro_re_tb with(nolock)    
              where sinistro_id = @sinistro_id     
            end    
        end
       end    
    
       if exists(select 1 from @parametro where tp_processo = @tp_processo and etapa = 8) and @sinistro_tp_ramo_id = 2--Somente RE    
       begin    
        --Etapa 8: Inclus�o de Questionario (RE)    
        set @etapa = 'ETAPA 08'    
            
        declare cursor_questionario INSENSITIVE cursor --Demanda 18679319(INSENSITIVE) - Adriano Pinheiro - Confitec - 01/04/2015
        for    
         select questionario_id, pergunta_id, dominio_resposta_id, texto_resposta    
         from seguros_db.dbo.evento_segbr_sinistro_questionario_tb with(nolock)    
         where evento_id = @evento_id    
            
         open cursor_questionario    
          fetch next from cursor_questionario into @questionario_id, @questionario_pergunta_id, @questionario_dominio_resposta_id, @questionario_texto_resposta    
          while @@fetch_status = 0    
          begin    
           exec seguros_db.dbo.sinistro_questionario_spi    
             @sinistro_id = @sinistro_id    
            ,@questionario_id = @questionario_id    
            ,@pergunta_id = @questionario_pergunta_id    
            ,@dominio_resposta_id = @questionario_dominio_resposta_id    
            ,@texto_resposta = @questionario_texto_resposta    
            ,@usuario = @usuario    
    
         fetch next from cursor_questionario into @questionario_id, @questionario_pergunta_id, @questionario_dominio_resposta_id, @questionario_texto_resposta    
          end    
         close cursor_questionario    
        deallocate cursor_questionario    
            
        if @debug = 1    
         select 'ETAPA 08', *    
         from seguros_db.dbo.sinistro_questionario_tb with(nolock)    
         where sinistro_id = @sinistro_id    
       end    
           
       if exists(select 1 from @parametro where tp_processo = @tp_processo and etapa = 9) and @sinistro_tp_ramo_id = 2--Somente RE     
       begin    
        --Etapa 9: Acionamento de regulador(RE)    
        set @etapa = 'ETAPA 09'  
		-- Cleber Sardo - 02/06/2015 - INC000004611592 - limpando o valor da vari�vel antes da consulta  
        set @sinistro_regulador_id = null   
		-- Cleber Sardo - 02/06/2015 - INC000004611592 
       select @sinistro_regulador_id = regulador_id     
        from seguros_db.dbo.produto_vistoria_tb with(nolock)    
        where produto_id = @produto_id    
         and estado = @sinistro_estado
		 and municipio_id = isnull(@sinistro_municipio_id,0)    -- Demanda 19444239 - Cleiton Queiroz - Confitec - 10/05/2017
         and @sinistro_val_estimado >= val_inicio_faixa    
         and (@sinistro_val_estimado <= val_fim_faixa or val_fim_faixa is null)    
         and (@data_atual <= dt_fim_vigencia or dt_fim_vigencia is null)    
            
        if @sinistro_regulador_id is not null    
         exec seguros_db.dbo.vistoria_spi    
           @sinistro_id = @sinistro_id    
          ,@apolice_id = @apolice_id    
          ,@sucursal_seguradora_id = @sucursal_seguradora_id    
          ,@seguradora_cod_susep = @seguradora_cod_susep    
          ,@ramo_id = @ramo_id    
          ,@dt_pedido_vistoria = @data_Atual    
          ,@regulador_id = @sinistro_regulador_id    
          ,@dt_fim_pre = null    
          ,@resultado_pre = null    
          ,@parecer_pre = null    
          ,@dt_parecer_vistoria = null    
          ,@resultado_vistoria = null    
          ,@parecer_vistoria = null    
          ,@usuario = @usuario    
            
        if @debug = 1    
         select 'ETAPA 09', *    
         from seguros_db.dbo.vistoria_tb with(nolock)    
         where sinistro_id = @sinistro_id    
       end    
           
       --trecho para log - inicio    
       declare @variaveis varchar(255)    
       set @variaveis = '@sinistro_id = ' + cast(isnull(@sinistro_id, 0) as varchar(11))    
               + ', @proposta_id = ' + cast(isnull(@proposta_id, 99) as varchar(20))    
       + ', @sinistro_cliente_id = ' + cast(isnull(@sinistro_cliente_id, 99) as varchar(10))    
               + ', @ramo_id = ' + cast(isnull(@ramo_id , 99) as varchar(10))    
               + ', @sinistro_val_estimado = ' + cast(isnull(@sinistro_val_estimado, 99) as varchar(20))    
               + ', @sinistro_tp_aviso_id = ' + cast(isnull(@sinistro_tp_aviso_id, 99) as varchar(5))    
               + ', @sinistro_tecnico_id = ' + cast(isnull(@sinistro_tecnico_id, 99) as varchar(5))    
    
       exec controle_sistema_db..log_erro_spi 1, @variaveis, 'SMQP0059', 'SMQP0059', @usuario, '', '', ''    
       --trecho para log - fim    
    
       -- Nova Consultoria - Jofilho - 19/02/2014 - Demanda: 17787319 - Classifica��o e Distribui��o de Sinistro - Tipifica��o    
 -- A etapa 10 ser� executada somente para sinistros do ambiente AB. Os sinistros do ambiente ABS passar�o a fazer a distribui��o    
       -- do sinistro (inclus�o de t�cnico) atrav�s de um servi�o (SEGP1323).    
       if exists(select 1 from @parametro where tp_processo = @tp_processo and etapa = 10) and @ambiente = 'AB'    
       begin    
        set @sinistro_tecnico_id = 0    
        set @sinistro_tp_aviso_id = 0    

     --Etapa 10: Inclus�o de T�cnico    
        set @etapa = 'ETAPA 10'    
if @produto_id = 1140    
        begin    
         set @sinistro_tecnico_id = 229    
        end    
        else    
        begin    
         if @produto_id in (1176,1177,1178,1179,1180,1181,1182)--Somente produto BB Prote��o    
         begin    
          exec @sinistro_tecnico_id = seguros_db.dbo.SEGS7592_SPS    
            @proposta_id = @proposta_id    
           ,@ramo_id = @ramo_id    
           ,@retorna_tecnico_id = 's'    
         end    
             
         --trecho para log - inicio    
         set @variaveis = null    
         set @variaveis = '@sinistro_id = ' + cast(isnull(@sinistro_id, 0) as varchar(11))    
               + ', @proposta_id = ' + cast(isnull(@proposta_id, 99) as varchar(20))    
               + ', @sinistro_cliente_id = ' + cast(isnull(@sinistro_cliente_id, 99) as varchar(10))    
               + ', @ramo_id = ' + cast(isnull(@ramo_id , 99) as varchar(10))    
               + ', @sinistro_val_estimado = ' + cast(isnull(@sinistro_val_estimado, 99) as varchar(20))    
               + ', @sinistro_tp_aviso_id = ' + cast(isnull(@sinistro_tp_aviso_id, 99) as varchar(5))    
               + ', @sinistro_tecnico_id = ' + cast(isnull(@sinistro_tecnico_id, 99) as varchar(5))    
               + ', @tp_processo = ' + cast(isnull(@tp_processo, 99) as varchar(2))    
                   
         exec controle_sistema_db..log_erro_spi 2, @variaveis, 'SMQP0059', 'SMQP0059', @usuario, '', '', ''    
         --trecho para log - fim    
             
         --Demais produtos ou quando n�o houver tecnico espec�fico para a proposta no bbprote��o           
         if @sinistro_tecnico_id is null or @sinistro_tecnico_id  = 0    
         begin    
    
          exec @sinistro_tp_aviso_id = seguros_db.dbo.SEGS8415_SPS     
           @sinistro_id = @sinistro_id    
           ,@retorno = 's'    
               
          --trecho para log - inicio    
          set @variaveis = null    
          set @variaveis = '@sinistro_id = ' + cast(isnull(@sinistro_id, 0) as varchar(11))    
                + ', @proposta_id = ' + cast(isnull(@proposta_id, 99) as varchar(20))    
                + ', @sinistro_cliente_id = ' + cast(isnull(@sinistro_cliente_id, 99) as varchar(10))    
                + ', @ramo_id = ' + cast(isnull(@ramo_id , 99) as varchar(10))    
                + ', @sinistro_val_estimado = ' + cast(isnull(@sinistro_val_estimado, 99) as varchar(20))    
                + ', @sinistro_tp_aviso_id = ' + cast(isnull(@sinistro_tp_aviso_id, 99) as varchar(5))    
          exec controle_sistema_db..log_erro_spi 3, @variaveis, 'SMQP0059', 'SMQP0059', @usuario, '', '', ''    
          --trecho para log - fim    
              
          select @sinistro_tp_aviso_id = isnull(@sinistro_tp_aviso_id, 0);    
    
          exec @sinistro_tecnico_id = seguros_db.dbo.segs7296_sps    
            @cliente_id = @sinistro_cliente_id    
           ,@ramo_id = @ramo_id    
           ,@val_estimativa = @sinistro_val_estimado    
           ,@tp_aviso = @sinistro_tp_aviso_id    
           ,@retorno = 's'    
    
          --trecho para log - inicio    
          set @variaveis = null    
          set @variaveis = '@sinistro_id = ' + cast(isnull(@sinistro_id, 0) as varchar(11))    
                + ', @sinistro_tecnico_id = ' + cast(isnull(@sinistro_tecnico_id, 99) as varchar(10))    
          exec controle_sistema_db..log_erro_spi 4, @variaveis, 'SMQP0059', 'SMQP0059', @usuario, '', '', ''    
          --trecho para log - fim    
         end    
        end    
            
        select   @sinistro_tecnico_nome = 'TECNICO: ' + nome    
          ,@sinistro_tecnico_email = case len(rtrim(ltrim(empresa))) when 0 then email+'@brasilseg.com.br' else email+'.'+empresa+'@brasilseg.com.br' end --SD02115848 - Enviar e-mail - 30/03/2020 - De: aliancadobrasil Para: brasilseg    
        from seguros_db.dbo.tecnico_tb with(nolock)    
        where tecnico_id = @sinistro_tecnico_id    
    
   exec seguros_db.dbo.sinistro_tecnico_spi    
          @sinistro_id = @sinistro_id    
         ,@apolice_id = @apolice_id    
         ,@sucursal_seguradora_id = @sucursal_seguradora_id    
         ,@seguradora_cod_susep = @seguradora_cod_susep    
         ,@ramo_id = @ramo_id    
         ,@tecnico_id = @sinistro_tecnico_id    
         ,@usuario = @usuario    
    
        exec @identificacao_retorno = seguros_db.dbo.sinistro_historico_spi    
          @sinistro_id = @sinistro_id    
         ,@apolice_id = @apolice_id    
         ,@sucursal_seguradora_id = @sucursal_seguradora_id    
         ,@seguradora_cod_susep = @seguradora_cod_susep    
         ,@ramo_id = @ramo_id    
         ,@evento_SEGBR_id = 10003    
         ,@situacao = 0    
         ,@motivo = null    
         ,@dt_evento = @data_atual    
         ,@usuario = @usuario    
         ,@seq_estimativa = 1    
        ,@retorna_seq_evento = 'n'    
            
        exec seguros_db.dbo.sinistro_item_historico_spi    
          @sinistro_id = @sinistro_id    
         ,@apolice_id = @apolice_id    
         ,@sucursal_seguradora_id = @sucursal_seguradora_id    
         ,@seguradora_cod_susep = @seguradora_cod_susep    
         ,@ramo_id = @ramo_id    
         ,@seq_evento = @identificacao_retorno    
         ,@item = @sinistro_tecnico_nome    
         ,@usuario = @usuario    
            
        set @email_assunto = 'Aviso de Sinistro - N� ' + cast(@sinistro_id as varchar)    
        set @email_mensagem = 'Aviso de Sinistro N� ' + cast(@sinistro_id as varchar)    
        set @email_mensagem = @email_mensagem + char(13) + 'Aviso de Sinistro pela Central de Atendimento'    
        if isnull(@sinistro_bb,0) > 0    
         set @email_mensagem = @email_mensagem + char(13) + 'Sinistro BB N� ' + cast(@sinistro_bb as varchar)    
        set @email_mensagem = @email_mensagem + char(13)    
        set @email_mensagem = @email_mensagem + char(13) + '------------------------------------------------------------------'    
        set @email_mensagem = @email_mensagem + char(13) + 'Comunica��o Corporativa da Alian�a do Brasil'    
            
		-- MU00416967 Evitando erros em QLD ao tentar enviar emails
		IF @@servername <> 'VBR008002-016\QUALID_ABS' and @@servername <> 'VBR008002-016\QUALID' BEGIN
			exec seguros_db.dbo.envia_email_sp    
			@email = @sinistro_tecnico_email    
			,@assunto = @email_assunto    
			,@mensagem = @email_mensagem    
		END

        if @debug = 1    
        begin    
         select 'ETAPA 10', *    
         from seguros_db.dbo.sinistro_tecnico_tb with(nolock)    
     where sinistro_id = @sinistro_id    
             
         select 'ETAPA 10', *    
         from seguros_db.dbo.sinistro_item_historico_tb with(nolock)    
         where sinistro_id = @sinistro_id    
        end    
       end    
    
       if exists(select 1 from @parametro where tp_processo = @tp_processo and etapa = 11)    
       begin    
        --Etapa 11: C�lculo de cosseguro 
        set @etapa = 'ETAPA 11'    
            
        if @ramo_id is not null    
         exec seguros_db.dbo.sinistro_calcula_cosseguro_spu    
           @sinistro_id = @sinistro_id    
          ,@apolice_id = @apolice_id    
          ,@sucursal_seguradora_id = @sucursal_seguradora_id    
          ,@seguradora_cod_susep = @seguradora_cod_susep   
          ,@ramo_id = @ramo_id    
       end    
    
       if exists(select 1 from @parametro where tp_processo = @tp_processo and etapa = 12)    
       begin    
        --Etapa 12: C�lculo de resseguro    
       set @etapa = 'ETAPA 12'
	   
	   --Altera��o Henrique Horich - Data 08.08.2019 - Impacto no fechamento e c�lculo no resseguro 	   
		 select @estimativa_seq = isnull(max(seq_estimativa),1)    
		 from seguros_db.dbo.sinistro_estimativa_tb with(nolock)    
		 where sinistro_id = @sinistro_id  
	 
	     exec resseg_db.dbo.RSGS00437_SPI 
		      @usuario = @usuario
             ,@sinistro_id_UNITARIO = @sinistro_id
             ,@seq_estimativa_UNITARIO = @estimativa_seq    
            
       -- exec resseg_db.dbo.calcula_resseguro_estimativa_sinistro_spu    
       --    @sinistro_id = @sinistro_id    
       --   ,@apolice_id = @apolice_id    
       --   ,@sucursal_seguradora_id = @sucursal_seguradora_id    
       --   ,@seguradora_cod_susep = @seguradora_cod_susep    
       --   ,@ramo_id = @ramo_id    
       --  ,@seq_estimativa = 1    
       --   ,@cliente_id = @sinistro_cliente_id    
       --   ,@usuario = @usuario    
       end    
    
       if exists(select 1 from @parametro where tp_processo = @tp_processo and etapa = 13)    
       begin    
        --Etapa 13: Inclus�o de sinistro_bb    
        set @etapa = 'ETAPA 13'    
          
  -- Demanda 18283294 - IN�CIO  
  -- Em caso de Rean�lise, encerrar a vig�ncia do sinistro_bb_anterior. Apenas para os produtos 155, 156, 300, 1152 e 1204    
  -- GENJUNIOR - NOVO PROTE��O OURO - INCLUS�O DO PRODUTO NOVO PARA REANALISE

  select @count_sinistro_bb = count(1)
    from seguros_db.dbo.sinistro_bb_tb 
   where sinistro_id = @sinistro_id
     and dt_fim_vigencia is null

  --If @produto_id in (155, 156, 300, 1152, 1204, 1239, 1240) And (Select Count(1) From seguros_db.dbo.sinistro_bb_tb Where dt_fim_vigencia is null and sinistro_id = @sinistro_id) > 0  
  If @produto_id in (155, 156, 300, 1152, 1204, 1239, 1240) And @count_sinistro_bb > 0 
  -- fim genjunior
  Begin  
   Set @sinistro_bb_ant = (Select top 1 sinistro_bb From seguros_db.dbo.sinistro_bb_tb Where dt_fim_vigencia is null and sinistro_id = @sinistro_id Order by dt_inclusao desc)  
     
   Exec seguros_db.dbo.sinistro_bb_spu   
    @sinistro_id = @sinistro_id,  
    @apolice_id = @apolice_id,  
    @sucursal_seguradora_id = @sucursal_seguradora_id,  
    @seguradora_cod_susep = @seguradora_cod_susep,  
    @ramo_id = @ramo_id,  
    @sinistro_bb = @sinistro_bb_ant,  
    @usuario = @usuario,  
    @dt_fim_vigencia = @data_ant,  
    @atualiza_sinistro_bb = 'N'  
        
  End  
  -- Demanda 18283294 - FIM          
            
        exec seguros_db.dbo.sinistro_bb_spi    
          @sinistro_id = @sinistro_id    
         ,@apolice_id = @apolice_id    
         ,@sucursal_seguradora_id = @sucursal_seguradora_id    
         ,@seguradora_cod_susep = @seguradora_cod_susep    
         ,@ramo_id = @ramo_id    
         ,@sinistro_bb = @sinistro_bb 
         ,@usuario = @usuario    
         ,@dt_inicio_vigencia = @data_atual          
            
        if @debug = 1    
         select 'ETAPA 13', *    
         from seguros_db.dbo.sinistro_bb_tb with(nolock)    
         where sinistro_id = @sinistro_id    
       end         
    
       if exists(select 1 from @parametro where tp_processo = @tp_processo and etapa = 14)    
       begin    
        --Etapa 14: Inclus�o de evento 2000    
        set @etapa = 'ETAPA 14'    
		--HENRIQUE H. HENRIQUES - AJUSTE GERA��O DO EVENTO 2000 (AVISO FAKE) PARA PRODUTOS ALS - INI
		IF (SELECT COUNT(1) FROM INTERFACE_DB.DBO.ENTRADA_GTR_ATUAL_TB WITH(NOLOCK) WHERE SINISTRO_BB = @SINISTRO_BB AND EVENTO_BB = 2000) = 0 --n�o existe ainda
			BEGIN
				DECLARE @COD_TRAN CHAR(4)
				SET @COD_TRAN = CASE WHEN @produto_id >= 1000 THEN 'ST00' ELSE 'SN00' END --COD_TRANSACAO correto (ALS = ST / SEG = SN)

				insert into @result    
				exec interface_db.dbo.entrada_gtr_spi    
					@cod_remessa = @cod_remessa    
					,@qtd_registros = 0    
					,@cod_transacao = @COD_TRAN
					,@evento_BB = 2000    
					,@sinistro_BB = @sinistro_bb    
					,@situacao = 'T'           
					,@usuario = @usuario    
            
				if @debug = 1    
					select 'ETAPA 14', *    
					from interface_db.dbo.entrada_gtr_atual_tb with(nolock)    
					where sinistro_bb = @sinistro_bb and evento_bb = 2000    
			END
		  --HENRIQUE H. HENRIQUES - AJUSTE GERA��O DO EVENTO 2000 (AVISO FAKE) PARA PRODUTOS ALS - FIM
       end    
    
       if exists(select 1 from @parametro where tp_processo = @tp_processo and etapa = 15)    
       begin    
        --Etapa 15: Atualiza��o da situa��o do aviso para 'A' - Processado    
        set @etapa = 'ETAPA 15'    
            
        exec seguros_db.dbo.evento_segbr_sinistro_situacao_aviso_spu    
 @evento_id = @evento_id    
         ,@tp_alteracao = 'SI'    
         ,@usuario = @usuario    
         ,@situacao_aviso = 'A'    
         ,@tp_log_sinistro_id = null    
       
        if @debug = 1    
         select 'ETAPA 15', situacao_aviso, *    
         from seguros_db.dbo.evento_segbr_sinistro_atual_tb with(nolock)    
         where evento_id = @evento_id    
       end    
           
       if exists(select 1 from @parametro where tp_processo = @tp_processo and etapa = 16)    
       begin    
        --Etapa 16: Inclus�o do detalhamento de inclus�o do sinistro_bb - Somente para os avisos que foram     
        -- processados temporariamente sem o sinistro_bb(sem retorno do evento 1100 por mais de 4 horas),    
        -- e que agora houve o retorno do 1100 com o sinistro_bb.    
        -- Retirada da flag de marca��o de Sinistro Sem Comunica��o    
        set @etapa = 'ETAPA 16'    
    
        insert into @result    
        exec @identificacao_retorno = seguros_db.dbo.sinistro_detalhamento_spi    
          @sinistro_id = @sinistro_id    
         ,@apolice_id = @apolice_id    
      ,@sucursal_seguradora_id = @sucursal_seguradora_id    
         ,@seguradora_cod_susep = @seguradora_cod_susep    
         ,@ramo_id = @ramo_id    
         ,@dt_detalhamento = @data_atual    
         ,@tp_detalhamento = 0    
         ,@restrito = 'n'    
         ,@usuario = @usuario    
            
        set @texto_auxiliar = 'Disponibiliza��o do Sinistro BB N� ' + cast(@sinistro_bb as varchar)    
             
        exec seguros_db.dbo.sinistro_linha_detalhe_spi    
          @sinistro_id = @sinistro_id    
         ,@apolice_id = @apolice_id    
         ,@sucursal_seguradora_id = @sucursal_seguradora_id     
         ,@seguradora_cod_susep = @seguradora_cod_susep    
         ,@ramo_id = @ramo_id    
         ,@detalhamento_id = @identificacao_retorno    
         ,@linha_id = 1    
         ,@linha = @texto_auxiliar    
         ,@usuario = @usuario    
            
        -- Retirada da flag de marca��o de Sinistro Sem Comunica��o    
        exec seguros_db.dbo.SEGS10268_SPU     
         @sinistro_id = @sinistro_id    
         ,@sem_comunicacao_bb = NULL    
            
        if @debug = 1    
        begin    
         select 'ETAPA 16', *     
         from seguros_db.dbo.sinistro_detalhamento_tb with(nolock)    
         where sinistro_id = @sinistro_id and detalhamento_id = @identificacao_retorno    
             
         select 'ETAPA 16', *     
         from seguros_db.dbo.sinistro_linha_detalhamento_tb with(nolock)    
         where sinistro_id = @sinistro_id and detalhamento_id = @identificacao_retorno    
        end    
       end    
           
       if exists(select 1 from @parametro where tp_processo = @tp_processo and etapa = 17)    
       begin    
        --Etapa 17: Inclus�o do detalhamento de pend�ncia sinistro_bb - Somente para os avisos que foram     
        -- processados temporariamente sem o sinistro_bb(sem retorno do evento 1100 por mais de 4 horas)    
        -- Inclus�o da flag de Sinistro Sem Comunica��o na Sinistro_Tb    
        set @etapa = 'ETAPA 17'    
            
        insert into @result    
        exec @identificacao_retorno = seguros_db.dbo.sinistro_detalhamento_spi    
          @sinistro_id = @sinistro_id    
         ,@apolice_id = @apolice_id    
         ,@sucursal_seguradora_id = @sucursal_seguradora_id    
         ,@seguradora_cod_susep = @seguradora_cod_susep    
         ,@ramo_id = @ramo_id    
         ,@dt_detalhamento = @data_atual    
         ,@tp_detalhamento = 0    
     ,@restrito = 'n'    
         ,@usuario = @usuario    
            
        set @texto_auxiliar = 'Aviso de sinistro processado temporariamente sem Sinistro BB.'    
             
        exec seguros_db.dbo.sinistro_linha_detalhe_spi    
          @sinistro_id = @sinistro_id    
    ,@apolice_id = @apolice_id    
         ,@sucursal_seguradora_id = @sucursal_seguradora_id     
         ,@seguradora_cod_susep = @seguradora_cod_susep    
         ,@ramo_id = @ramo_id    
         ,@detalhamento_id = @identificacao_retorno    
         ,@linha_id = 1    
         ,@linha = @texto_auxiliar   
         ,@usuario = @usuario    
    
        -- Inclus�o da flag de Sinistro Sem Comunica��o na Sinistro_Tb    
        exec seguros_db.dbo.SEGS10268_SPU    
          @sinistro_id = @sinistro_id    
          ,@sem_comunicacao_bb = 'T'    
            
        if @debug = 1    
        begin    
         select 'ETAPA 17', *     
         from seguros_db.dbo.sinistro_detalhamento_tb with(nolock)    
         where sinistro_id = @sinistro_id and detalhamento_id = @identificacao_retorno    
             
         select 'ETAPA 17', *     
         from seguros_db.dbo.sinistro_linha_detalhamento_tb with(nolock)    
         where sinistro_id = @sinistro_id and detalhamento_id = @identificacao_retorno    
        end    
       end         
    
       if exists(select 1 from @parametro where tp_processo = @tp_processo and etapa = 18)    
       begin    
        --Etapa 18: Atualiza��o da situa��o do aviso para 'S' - Processado temporariamente sem Sinistro BB    
        --Somente para os avisos que foram processados temporariamente sem o sinistro_bb(sem retorno do evento 1100 por mais de 4 horas)    
        set @etapa = 'ETAPA 18'    
            
        exec seguros_db.dbo.evento_segbr_sinistro_situacao_aviso_spu    
          @evento_id = @evento_id    
         ,@tp_alteracao = 'SI'    
         ,@usuario = @usuario    
         ,@situacao_aviso = 'S'    
         ,@tp_log_sinistro_id = null    
    
        if @debug = 1    
         select 'ETAPA 18', situacao_aviso, *    
         from seguros_db.dbo.evento_segbr_sinistro_atual_tb with(nolock)    
         where evento_id = @evento_id    
       end    
    
       if exists(select 1 from @parametro where tp_processo = @tp_processo and etapa = 19)    
       begin    
        --Etapa 19: Reabertura de estimativa para sinistro rean�lise    
        set @etapa = 'ETAPA 19'    
            
        --Verificar qual � a nova seq_estimativa    
        select @estimativa_seq = isnull(max(seq_estimativa),1)+1    
        from seguros_db.dbo.sinistro_estimativa_tb with(nolock)    
        where sinistro_id = @sinistro_id    
        -- and item_val_estimativa = 1     
            
        --trecho para log - inicio    
        declare @var varchar(255)    
        set @var = null    
        set @var = '@sinistro_id = ' + cast(isnull(@sinistro_id, 0) as varchar(11))    
                + ', @proposta_id = ' + cast(isnull(@proposta_id, 99) as varchar(20))    
                + ', @ramo_id = ' + cast(isnull(@ramo_id , 99) as varchar(10))    
                + ', @sinistro_val_estimado = ' + cast(isnull(@sinistro_val_estimado, 99) as varchar(20))    
      + ', @estimativa_seq = ' + cast(isnull(@estimativa_seq, 99) as varchar(255))   
    
        exec controle_sistema_db..log_erro_spi 5, @var, 'SMQP0059', 'SMQP0059', @usuario, '', '', ''    
        --trecho para log - fim    
    
        --Encerrar as estimativas anteriores    
        exec seguros_db.dbo.sinistro_estimativa_spu     
         @sinistro_id = @sinistro_id    
         ,@apolice_id = NULL    
         ,@sucursal_seguradora_id = NULL    
         ,@seguradora_cod_susep = NULL    
         ,@ramo_id = NULL    
         ,@dt_fim = @data_atual    
         ,@item_val_estimativa = NULL    
         ,@usuario = @usuario    
         
        --Reabertura das coberturas(Encerramento das anteriores e cria��o das novas)    
        declare cursor_reabertura_cobertura INSENSITIVE cursor --Demanda 18679319(INSENSITIVE) - Adriano Pinheiro - Confitec - 01/04/2015   
        for    
         select tp_cobertura_id, val_estimativa 
         from seguros_db.dbo.sinistro_cobertura_tb with(nolock)    
         where sinistro_id = @sinistro_id    
          and dt_fim_vigencia is null    
    
         open cursor_reabertura_cobertura    
          fetch next from cursor_reabertura_cobertura into @sinistro_tp_cobertura_id, @sinistro_cobertura_val_estimado    
          while @@fetch_status = 0    
          begin    
               
          if isnull(@sinistro_cobertura_val_estimado,0) = 0    
       set @sinistro_cobertura_val_estimado = 50    
               
           --trecho para log - inicio    
           set @var = null    
           set @var = '@sinistro_id = ' + cast(isnull(@sinistro_id, 0) as varchar(11))    
                   + ', @proposta_id = ' + cast(isnull(@proposta_id, 99) as varchar(20))    
                   + ', @ramo_id = ' + cast(isnull(@ramo_id , 99) as varchar(10))    
                   + ', @sinistro_val_estimado = ' + cast(isnull(@sinistro_val_estimado, 99) as varchar(20))    
                   + ', @estimativa_seq = ' + cast(isnull(@estimativa_seq, 99) as varchar(255))    
                   + ', @sinistro_cobertura_val_estimado = ' + cast(isnull(@sinistro_cobertura_val_estimado, 99) as varchar(255))    
    
           exec controle_sistema_db..log_erro_spi 6, @var, 'SMQP0059', 'SMQP0059', @usuario, '', '', ''    
           --trecho para log - fim    
               
           exec seguros_db.dbo.sinistro_cobertura_spi      
             @sinistro_id = @sinistro_id      
            ,@apolice_id = @apolice_id      
            ,@sucursal_seguradora_id = @sucursal_seguradora_id      
            ,@seguradora_cod_susep = @seguradora_cod_susep      
            ,@ramo_id = @ramo_id      
            ,@tp_cobertura_id = @sinistro_tp_cobertura_id    
            ,@tp_indenizacao = 1      
            ,@val_estimativa = @sinistro_cobertura_val_estimado    
            ,@usuario = @usuario      
            ,@Processamento = 'B'     
                
           fetch next from cursor_reabertura_cobertura into @sinistro_tp_cobertura_id, @sinistro_cobertura_val_estimado    
          end    
         close cursor_reabertura_cobertura    
        deallocate cursor_reabertura_cobertura    
            
        --Somatoria dos valores das coberturas inseridas acima    
        select @sinistro_val_estimado = sum(val_estimativa)    
        from seguros_db.dbo.sinistro_cobertura_tb with(nolock)    
        where sinistro_id = @sinistro_id    
         and dt_fim_vigencia is null    



		 --Henrique H Henriques - CONFITEC SP - 23/03/2017 - Ajuste para obter o valor de resseguro da ultima estimativa para n�o apresentar irregularidade no contabil. - INI
		 declare @sinistro_val_pago_resseguro numeric(15,2)
            
        --Recuperar os valores de resseguro, val_pago e estimado    
        SELECT   @perc_resseguro_quota = isnull(perc_re_seguro_quota,0)    
          ,@perc_resseguro_er = isnull(perc_re_seguro_er,0)    
          ,@sinistro_val_pago = val_pago
		  ,@sinistro_val_pago_resseguro = val_resseguro
        from seguros_db.dbo.sinistro_estimativa_tb with(nolock)    
        where sinistro_id = @sinistro_id    
         and seq_estimativa = @estimativa_seq-1    
         and item_val_estimativa = 1   

		 --Henrique H Henriques - CONFITEC SP - 23/03/2017 - Ajuste para obter o valor de resseguro da ultima estimativa para n�o apresentar irregularidade no contabil. - FIM


            
        --trecho para log - inicio    
        set @var = null    
        set @var = '@sinistro_id = ' + cast(isnull(@sinistro_id, 0) as varchar(11))    
             + ', @proposta_id = ' + cast(isnull(@proposta_id, 99) as varchar(20))    
                   + ', @ramo_id = ' + cast(isnull(@ramo_id , 99) as varchar(10))    
                   + ', @sinistro_val_estimado = ' + cast(isnull(@sinistro_val_estimado, 99) as varchar(20))    
                   + ', @estimativa_seq = ' + cast(isnull(@estimativa_seq, 99) as varchar(255))    
                   + ', @sinistro_cobertura_val_estimado = ' + cast(isnull(@sinistro_cobertura_val_estimado, 99) as varchar(255))    
                   + ', @perc_resseguro_quota = ' + cast(isnull(@perc_resseguro_quota, 99) as varchar(255))    
                   + ', @perc_resseguro_er = ' + cast(isnull(@perc_resseguro_er, 99) as varchar(255))    
                   + ', @sinistro_val_pago = ' + cast(isnull(@sinistro_val_pago, 99) as varchar(255))    
    
        exec controle_sistema_db..log_erro_spi 7, @var, 'SMQP0059', 'SMQP0059', @usuario, '', '', ''    
        --trecho para log - fim    
               
        --Nova estimativa Indeniza��o    
        exec seguros_db.dbo.sinistro_estimativa_spi      
          @sinistro_id = @sinistro_id      
         ,@apolice_id = @apolice_id      
         ,@sucursal_seguradora_id = @sucursal_seguradora_id      
         ,@seguradora_cod_susep = @seguradora_cod_susep      
         ,@ramo_id = @ramo_id      
         ,@dt_inicio_estimativa = @data_atual      
         ,@item_val_estimativa = 1    
         ,@seq_estimativa = @estimativa_seq    
         ,@val_estimado = @sinistro_val_estimado    
         ,@val_pago = @sinistro_val_pago      
		 -- CONFITEC 12/03/2020 - INCIDENTE (IM01215317) Inicio - Ajuste para calculo de resseguro
         ,@val_resseguro = NULL 
         ,@perc_re_seguro_quota = NULL    
         ,@perc_re_seguro_er = NULL
		 -- CONFITEC 12/03/2020 - INCIDENTE (IM01215317) Fim - Ajuste para calculo de resseguro
         ,@situacao = ''      
         ,@usuario = @usuario      
         ,@num_carta_seguro_aceito = null      
         ,@Processamento = 'B'    
    
        --trecho para log - inicio    
        set @var = null    
        set @var = '@sinistro_id = ' + cast(isnull(@sinistro_id, 0) as varchar(11))    
                   + ', @proposta_id = ' + cast(isnull(@proposta_id, 99) as varchar(20))    
                   + ', @ramo_id = ' + cast(isnull(@ramo_id , 99) as varchar(10))    
                   + ', @sinistro_val_estimado = ' + cast(isnull(@sinistro_val_estimado, 99) as varchar(20))    
                   + ', @estimativa_seq = ' + cast(isnull(@estimativa_seq, 99) as varchar(255))    
    
        exec controle_sistema_db..log_erro_spi 8, @var, 'SMQP0059', 'SMQP0059', @usuario, '', '', ''    
        --trecho para log - fim    
        
        --Replicar a �ltima estimativa de Honor�rios    
        exec seguros_db.dbo.sinistro_estimativa_item_spi    
          @sinistro_id = @sinistro_id    
         ,@dt_inicio_estimativa = @data_atual    
         ,@item_val_estimativa = 2--Honor�rios    
         ,@seq_estimativa_nova = @estimativa_seq    
         ,@usuario = @usuario    
         ,@distribuicao_sinistro = 'S' -- CONFITEC 12/03/2020 - INCIDENTE (IM01215317) - Ajuste para calculo de resseguro   
        --trecho para log - inicio    
        set @var = null    
        set @var = '@sinistro_id = ' + cast(isnull(@sinistro_id, 0) as varchar(11))    
                   + ', @proposta_id = ' + cast(isnull(@proposta_id, 99) as varchar(20))    
    + ', @ramo_id = ' + cast(isnull(@ramo_id , 99) as varchar(10))    
                   + ', @sinistro_val_estimado = ' + cast(isnull(@sinistro_val_estimado, 99) as varchar(20))    
                   + ', @estimativa_seq = ' + cast(isnull(@estimativa_seq, 99) as varchar(255))    
    
        exec controle_sistema_db..log_erro_spi 9, @var, 'SMQP0059', 'SMQP0059', @usuario, '', '', ''    
        --trecho para log - fim    
    
        --Replicar a �ltima estimativa de Despesas    
        exec seguros_db.dbo.sinistro_estimativa_item_spi    
          @sinistro_id = @sinistro_id    
         ,@dt_inicio_estimativa = @data_atual    
         ,@item_val_estimativa = 3--Despesas    
         ,@seq_estimativa_nova = @estimativa_seq    
         ,@usuario = @usuario    
         ,@distribuicao_sinistro = 'S' -- CONFITEC 12/03/2020 - INCIDENTE (IM01215317) - Ajuste para calculo de resseguro  
        --trecho para log - inicio    
        set @var = null    
        set @var = '@sinistro_id = ' + cast(isnull(@sinistro_id, 0) as varchar(11))    
             + ', @proposta_id = ' + cast(isnull(@proposta_id, 99) as varchar(20))    
   + ', @ramo_id = ' + cast(isnull(@ramo_id , 99) as varchar(10))    
                   + ', @sinistro_val_estimado = ' + cast(isnull(@sinistro_val_estimado, 99) as varchar(20))    
                   + ', @estimativa_seq = ' + cast(isnull(@estimativa_seq, 99) as varchar(255))    
    
        exec controle_sistema_db..log_erro_spi 10, @var, 'SMQP0059', 'SMQP0059', @usuario, '', '', ''    
        --trecho para log - fim    
		
			-- CONFITEC 16/03/2020 - INCIDENTE (IM01215317) Inicio - Ajuste para calculo de cosseguro
	   if @ramo_id is not null
	   begin    
			 exec seguros_db.dbo.sinistro_calcula_cosseguro_spu    
			   @sinistro_id = @sinistro_id    
			  ,@apolice_id = @apolice_id    
			  ,@sucursal_seguradora_id = @sucursal_seguradora_id    
			  ,@seguradora_cod_susep = @seguradora_cod_susep   
			  ,@ramo_id = @ramo_id    
       end   
		-- CONFITEC 16/03/2020 - INCIDENTE (IM01215317) Fim - Ajuste para calculo de cosseguro

		-- CONFITEC 12/03/2020 - INCIDENTE (IM01215317) Inicio - Ajuste para calculo de resseguro
		exec resseg_db.dbo.RSGS00437_SPI   
        @usuario = @usuario  
             ,@sinistro_id_UNITARIO = @sinistro_id  
             ,@seq_estimativa_UNITARIO = @estimativa_seq     
		-- CONFITEC 12/03/2020 - INCIDENTE (IM01215317) Fim - Ajuste para calculo de resseguro

        if @debug = 1    
         select 'ETAPA 19', *    
         from seguros_db.dbo.sinistro_estimativa_tb with(nolock)    
         where sinistro_id = @sinistro_id    
         order by item_val_estimativa, seq_estimativa    
       end    
           
       if exists(select 1 from @parametro where tp_processo = @tp_processo and etapa = 20)    
       begin    
        --Etapa 20: Reabertura do sinistro_id e grava��o no hist�rico da rean�lise    
        set @etapa = 'ETAPA 20'    
            
        --Reabertura do sinistro    
        exec seguros_db.dbo.sinistro_situacao_spu @sinistro_id,'1'    
            
        exec @identificacao_retorno = seguros_db.dbo.sinistro_historico_spi      
          @sinistro_id = @sinistro_id      
         ,@apolice_id = @apolice_id      
         ,@sucursal_seguradora_id = @sucursal_seguradora_id      
         ,@seguradora_cod_susep = @seguradora_cod_susep      
         ,@ramo_id = @ramo_id      
         ,@evento_SEGBR_id = 10103  --Rean�lise    
         ,@situacao = '1'      
         ,@motivo = null      
         ,@dt_evento = @data_atual      
         ,@usuario = @usuario      
         ,@retorna_seq_evento = 'n'    
            
        set @texto_auxiliar = 'REAN�LISE - N. DO SINISTRO: ' + cast(@sinistro_id as varchar)      
        exec seguros_db.dbo.sinistro_item_historico_spi      
          @sinistro_id = @sinistro_id      
         ,@apolice_id = @apolice_id      
         ,@sucursal_seguradora_id = @sucursal_seguradora_id      
         ,@seguradora_cod_susep = @seguradora_cod_susep      
         ,@ramo_id = @ramo_id      
         ,@seq_evento = @identificacao_retorno      
         ,@item = @texto_auxiliar      
         ,@usuario = @usuario      
                  
        if @debug = 1    
        begin    
 select 'ETAPA 20', *     
         from seguros_db.dbo.sinistro_historico_tb with(nolock)    
         where sinistro_id = @sinistro_id    
          and seq_evento = @identificacao_retorno    
             
         select 'ETAPA 20', *    
         from seguros_db.dbo.sinistro_item_historico_tb with(nolock)    
         where sinistro_id = @sinistro_id    
          and seq_evento = @identificacao_retorno    
        end    
       end         
           
       if exists(select 1 from @parametro where tp_processo = @tp_processo and etapa = 21)    
       begin    
        --Etapa 21: Inclus�o de detalhamento da rean�lise 
   set @etapa = 'ETAPA 21'    
            
        insert into @result    
        exec @identificacao_retorno = seguros_db.dbo.sinistro_detalhamento_spi    
          @sinistro_id = @sinistro_id    
         ,@apolice_id = @apolice_id    
         ,@sucursal_seguradora_id = @sucursal_seguradora_id    
         ,@seguradora_cod_susep = @seguradora_cod_susep    
         ,@ramo_id = @ramo_id    
         ,@dt_detalhamento = @data_atual    
         ,@tp_detalhamento = 0    
         ,@restrito = 'n'    
         ,@usuario = @usuario    
            
        declare cursor_detalhamento INSENSITIVE cursor --Demanda 18679319(INSENSITIVE) - Adriano Pinheiro - Confitec - 01/04/2015
        for    
         select sequencial, left(descricao_evento,80)    
         from seguros_db.dbo.evento_segbr_sinistro_descricao_tb with(nolock)    
         where evento_id = @evento_id    
         order by sequencial    
    
         open cursor_detalhamento    
          fetch next from cursor_detalhamento into @num_auxiliar, @texto_auxiliar    
          while @@fetch_status = 0    
          begin    
           exec seguros_db.dbo.sinistro_linha_detalhe_spi    
             @sinistro_id = @sinistro_id    
            ,@apolice_id = @apolice_id    
            ,@sucursal_seguradora_id = @sucursal_seguradora_id     
            ,@seguradora_cod_susep = @seguradora_cod_susep    
            ,@ramo_id = @ramo_id    
            ,@detalhamento_id = @identificacao_retorno    
            ,@linha_id = @num_auxiliar    
            ,@linha = @texto_auxiliar    
            ,@usuario = @usuario    
                
           fetch next from cursor_detalhamento into @num_auxiliar, @texto_auxiliar    
          end    
         close cursor_detalhamento    
        deallocate cursor_detalhamento    
            
      if @debug = 1    
        begin    
         select 'ETAPA 21', *     
         from seguros_db.dbo.sinistro_detalhamento_tb with(nolock)    
         where sinistro_id = @sinistro_id and detalhamento_id = @identificacao_retorno    
             
         select 'ETAPA 21', *     
         from seguros_db.dbo.sinistro_linha_detalhamento_tb with(nolock)    
         where sinistro_id = @sinistro_id and detalhamento_id = @identificacao_retorno    
        end    
       end    

		-- gustavo.cruz - 26/04/2018 - Projeto MU00416971- Efici�ncia_Operacional_Detalhamento_Sinistro
		-- in�cio

		select @verifica_restrito_web = count(*)
		from seguros_db.dbo.sinistro_detalhamento_tb 
		where sinistro_id = @sinistro_id 
			and upper(restrito_web) = 'N'

		select @permite_sinistro_detalhamento = upper(PERMITE_SINISTRO_DETALHAMENTO) 
		from seguros_db.dbo.produto_tb 
		where produto_id = @produto_id

		if(@verifica_restrito_web = 0 and @permite_sinistro_detalhamento = 'S')
		begin

			-- insere no detalhamento
			exec @identificacao_retorno = seguros_db.dbo.sinistro_detalhamento_spi    
			  @sinistro_id = @sinistro_id    
			 ,@apolice_id = @apolice_id    
			 ,@sucursal_seguradora_id = @sucursal_seguradora_id    
			 ,@seguradora_cod_susep = @seguradora_cod_susep    
			 ,@ramo_id = @ramo_id    
			 ,@dt_detalhamento = @data_atual    
			 ,@tp_detalhamento = 0    
			 ,@restrito = 'n'    
			 ,@usuario = @usuario
			 ,@restrito_web = 'N'  

			 -- insere linha detalhamento
			 exec seguros_db.dbo.sinistro_linha_detalhe_spi    
             @sinistro_id = @sinistro_id    
            ,@apolice_id = @apolice_id    
            ,@sucursal_seguradora_id = @sucursal_seguradora_id     
            ,@seguradora_cod_susep = @seguradora_cod_susep    
            ,@ramo_id = @ramo_id    
            ,@detalhamento_id = @identificacao_retorno 
            ,@linha_id = 1    
            ,@linha = 'Abertura do comunicado de sinistro - OBS: O processo de an�lise de sinistro '    
            ,@usuario = @usuario 

			--insere linha detalhamento
			exec seguros_db.dbo.sinistro_linha_detalhe_spi    
             @sinistro_id = @sinistro_id    
            ,@apolice_id = @apolice_id    
            ,@sucursal_seguradora_id = @sucursal_seguradora_id     
            ,@seguradora_cod_susep = @seguradora_cod_susep    
            ,@ramo_id = @ramo_id    
            ,@detalhamento_id = @identificacao_retorno 
          ,@linha_id = 2   
            ,@linha = 'apenas tem in�cio quando a documenta��o completa est� dispon�vel � Seguradora.'    
            ,@usuario = @usuario 
			 
		end
		-- fim
		    
      commit tran    
     end try    
     begin catch    
      if @@trancount > 0    
       rollback tran  
          
      set @error_message = error_message()    
      set @error_message = left(isnull(@etapa, 'ETAPA ??') + ' - ' + isnull(@error_message, 'Erro n�o identificado'), 70)    
          
      if @error_message not like '%Sistema bloqueado%' begin    
       --Em caso de erro o registro dever� ser removido da fila para n�o entrar em Loop(situacao_aviso = 'P')    
       exec seguros_db.dbo.evento_segbr_sinistro_situacao_aviso_spu @evento_id, 'SI', @usuario, 'P', null,     
         @descr_erro_remessa = @error_message    
      end    
          
      if @debug = 1    
       select @error_message    
     end catch    
         
     fetch next from cursor_sinistros into @evento_id    
    end    
   close cursor_sinistros    
  deallocate cursor_sinistros    
 end --if @status = 'L' or @usuario = 'SEGBR' begin    
END     













