CREATE PROCEDURE dbo.SEGS14666_SPI @produto_id INT
	,@ramo_id INT
	,@proposta_id INT
	,@evento_sinistro_id INT
	,@tp_cobertura_id INT
	,@dt_ocorrencia SMALLDATETIME
	,@evento_id INT = NULL --AVISO REALIZADO PELO SEGP1296
	,@sinistro_id NUMERIC(11, 0) = NULL --AVISO REALIZADO PELO SEGP0794
	,@usuario VARCHAR(20) = 'SEGS14666_SPI'
	,@debug SMALLINT = 0 -- MODO DEBUG(1) NAO REALIZA O INSERT NA TABELA FINAL
AS
/*  
 NTENDENCIA 18/002/2020  
 DEMANDA: C00216281-ESTEIRA-PAGAMENTO-IMEDIATO-VIDA-INDIVIDUAL 
 DESCRI��O: PROCEDURE PARA VALIDAR PAGTO IMEDIATO E GRAVAR O DETALHAMENTO.  
 BANCO: SEGUROS_DB   
*/
-- BLOCO DE TESTE   
/*
		BEGIN TRAN

		DECLARE @produto_id INT = 1183
			,@ramo_id INT = 77
			,@proposta_id INT = 24730523
			,@evento_sinistro_id INT = 28
			,@tp_cobertura_id INT = 5
			,@dt_ocorrencia SMALLDATETIME = '20200217'
			,@evento_id INT = 45448763
			,@sinistro_id NUMERIC(11,0) = NULL
			,@usuario VARCHAR(20) = 'C00216281'
			,@debug SMALLINT = 1

		IF @@TRANCOUNT > 0
		BEGIN
			EXEC seguros_db.dbo.SEGS14666_SPI @produto_id
				,@ramo_id
				,@proposta_id
				,@evento_sinistro_id
				,@tp_cobertura_id
				,@dt_ocorrencia
				,@evento_id
				,@sinistro_id
				,@usuario
				,@debug
		END
		ELSE
			SELECT 'Erro. A transa��o n�o foi aberta para executar o teste.'

		ROLLBACK

    
*/
BEGIN
	SET NOCOUNT ON

	-- Declara��o e tratamento de variaveis (inicio)  
	DECLARE @PgtoImediato AS SMALLINT = 1 -- (1-Sim / N�o)  

	-----------  
	BEGIN TRY
		--VALIDA��O DOS PAR�MENTROS  
		DECLARE @sinistro_parametro_chave_id INT
			,@tp_sinistro_parametro_id INT

		IF OBJECT_ID('TEMPDB..#DETALHAMENTO') IS NOT NULL
		BEGIN
			DROP TABLE #DETALHAMENTO
		END

		CREATE TABLE #DETALHAMENTO (
			id INT IDENTITY(1, 1) NOT NULL
			,linha VARCHAR(60) NULL -- TAMANHO MAXIMO ACEITO NO DETALHAMENTO
			)

		--INICIALIZANDO O DETALHAMENTO
		INSERT INTO #DETALHAMENTO (linha)
		SELECT '--------------------------'

		INSERT INTO #DETALHAMENTO (linha)
		SELECT '' --

		INSERT INTO #DETALHAMENTO (linha)
		SELECT '' --

		INSERT INTO #DETALHAMENTO (linha)
		SELECT 'CLASSIFICA��O DO SINISTRO:'

		INSERT INTO #DETALHAMENTO (linha)
		SELECT '--------------------------'

		INSERT INTO #DETALHAMENTO (linha)
		SELECT '' -- GRAVA��O DA CLASSIFICA��O, ID 6 

		INSERT INTO #DETALHAMENTO (linha)
		SELECT '' --

		INSERT INTO #DETALHAMENTO (linha)
		SELECT 'Informa��es:'

		INSERT INTO #DETALHAMENTO (linha)
		SELECT '' --

		--PEGANDO A CHAVE DOS PARAMETROS  
		--ENCONTRANDO A CHAVE VIGENTE DA PROPOSTA  
		SELECT DISTINCT @sinistro_parametro_chave_id = B.sinistro_parametro_chave_id
			,@tp_sinistro_parametro_id = A.tp_sinistro_parametro_id
		FROM SEGUROS_DB.dbo.tp_sinistro_parametro_tb a WITH (NOLOCK)
		INNER JOIN SEGUROS_DB.dbo.sinistro_parametro_chave_tb b WITH (NOLOCK)
			ON a.tp_sinistro_parametro_id = b.tp_sinistro_parametro_id
		INNER JOIN SEGUROS_DB.dbo.sinistro_parametro_chave_regra_tb c WITH (NOLOCK)
			ON b.sinistro_parametro_chave_id = c.sinistro_parametro_chave_id
		INNER JOIN SEGUROS_DB.dbo.sinistro_parametro_regra_tb d WITH (NOLOCK)
			ON c.sinistro_parametro_regra_id = d.sinistro_parametro_regra_id
		WHERE   b.produto_id = @produto_id -- 1183  
			AND b.ramo_id = @ramo_id -- 77   
			AND b.evento_sinistro_id = @evento_sinistro_id -- 28 --  
			AND b.tp_cobertura_id = @tp_cobertura_id -- 5 --  
			AND b.dt_fim_vigencia IS NULL

		IF @sinistro_parametro_chave_id IS NULL
		BEGIN
			SET @PgtoImediato = 0

			INSERT INTO #DETALHAMENTO (linha)
			SELECT 'O produto, ramo, evento e cobertura selecionados no aviso'

			INSERT INTO #DETALHAMENTO (linha)
			SELECT 'n�o est�o habilitados para o fluxo do pagamento imediato'

			INSERT INTO #DETALHAMENTO (linha)
			SELECT ''
		END
		ELSE
		BEGIN
			----------------------------------------------------------------------------------------------------------------------------------------------------- 
			--###################################################################################################################################################  
			--1� VALIDA��O DA VIG�NCIA COBERTURA E APOLICE 'verifica_vigencia_cobertura_apolice'  
			--Regra: VALIDA��O DE TELA: OCORRENCIA DENTRO DA VIG�NCIA DA (AP�LICE E COBERTURA)  
			--BUSCANDO A REGRA PARAMETRIZADA  
			/*  
		   SELECT REGRA.descricao,CHAVE.valor,CHAVE.operador,REGRA.tipo,*  FROM SEGUROS_DB.dbo.sinistro_parametro_chave_regra_tb CHAVE  
			INNER JOIN SEGUROS_DB.dbo.sinistro_parametro_regra_tb REGRA  
		   ON CHAVE.sinistro_parametro_regra_id = REGRA.sinistro_parametro_regra_id  
		   WHERE 1=1  
			 AND CHAVE.sinistro_parametro_chave_id = 96 --@sinistro_parametro_chave_id  
			 AND REGRA.NOME = 'verifica_vigencia_cobertura_apolice'  
  
		 */
			DECLARE @Dt_ini AS SMALLDATETIME
			DECLARE @Dt_fim AS SMALLDATETIME
			DECLARE @VALIDA_VIGENCIA_APOLICE_COBERTURA CHAR(1)

			SELECT @VALIDA_VIGENCIA_APOLICE_COBERTURA = CHAVE.VALOR
			FROM SEGUROS_DB.dbo.sinistro_parametro_chave_regra_tb CHAVE WITH (NOLOCK)
			INNER JOIN SEGUROS_DB.dbo.sinistro_parametro_regra_tb REGRA WITH (NOLOCK)
				ON CHAVE.sinistro_parametro_regra_id = REGRA.sinistro_parametro_regra_id
			WHERE CHAVE.sinistro_parametro_chave_id = @sinistro_parametro_chave_id --1079 --  
				AND REGRA.NOME = 'verifica_vigencia_cobertura_apolice'

			IF @VALIDA_VIGENCIA_APOLICE_COBERTURA = 'S'
			BEGIN

				--VALIDA��O DE TELA: OCORRENCIA DENTRO DA VIG�NCIA DA (AP�LICE E COBERTURA)  
				IF OBJECT_ID('TEMPDB..#vigencia') IS NOT NULL
				BEGIN
					DROP TABLE #vigencia
				END

				CREATE TABLE #VIGENCIA (
					id INT IDENTITY(1, 1) NOT NULL PRIMARY KEY
					,dt_inicio_vigencia SMALLDATETIME NULL
					,dt_fim_vigencia SMALLDATETIME NULL
					,vigencia CHAR(1) NULL --(A)police / (C)obertura
					,proposta_id INT NULL
					)

				
				--AP�LICE  
				INSERT INTO #vigencia (
					dt_inicio_vigencia
					,dt_fim_vigencia
					,vigencia
					,proposta_id
					)
				SELECT ISNULL(APOLICE.dt_inicio_vigencia, ISNULL(ADESAO.dt_inicio_vigencia, FECHADA.dt_inicio_vig))
					,ISNULL(APOLICE.dt_fim_vigencia, ISNULL(ADESAO.dt_fim_vigencia, FECHADA.dt_fim_vig))
					,'A' --AP�LICE
					,proposta.proposta_id
				FROM seguros_db.dbo.proposta_tb proposta WITH (NOLOCK)
				LEFT JOIN seguros_db.dbo.proposta_fechada_tb fechada WITH (NOLOCK)
					ON proposta.proposta_id = fechada.proposta_id
				LEFT JOIN seguros_db.dbo.proposta_adesao_tb adesao WITH (NOLOCK)
					ON proposta.proposta_id = adesao.proposta_id
				LEFT JOIN seguros_db.dbo.apolice_tb apolice WITH (NOLOCK)
					ON proposta.proposta_id = apolice.proposta_id
				WHERE proposta.proposta_id = @proposta_id
					AND proposta.situacao = 'i'
				
					--VALIDANDO PROPOSTA CANCELADA
					--INSERINDO VIGENCIA DOS CANCELAMENTOS EXISTENTES				
					IF OBJECT_ID('TEMPDB..#VIGENCIA_CANCELAMENTO') IS NOT NULL
					BEGIN
						DROP TABLE #VIGENCIA_CANCELAMENTO
					END

					CREATE TABLE #VIGENCIA_CANCELAMENTO (
						ID INT IDENTITY(1, 1) NOT NULL PRIMARY KEY
						,DT_INICIO_CANCELAMENTO SMALLDATETIME NULL
						,DT_FIM_CANCELAMENTO SMALLDATETIME NULL
						,PROPOSTA_ID INT NULL
						)

					INSERT INTO #VIGENCIA_CANCELAMENTO (
						DT_INICIO_CANCELAMENTO
						,DT_FIM_CANCELAMENTO
						,PROPOSTA_ID
						)
					SELECT DT_INICIO_CANCELAMENTO
						,ISNULL(DT_FIM_CANCELAMENTO, GETDATE() + 1)
						,PROPOSTA_ID
					FROM SEGUROS_DB.DBO.CANCELAMENTO_PROPOSTA_TB CANCELAMENTO_PROPOSTA_TB WITH (NOLOCK)
					WHERE CANCELAMENTO_PROPOSTA_TB.PROPOSTA_ID = @PROPOSTA_ID
					

				--DEFINIR A VIGENCIA DAS PROPOSTAS DOS PRODUTOS BESC QUE NAO POSSUEM DT_FIM_VIGENCIA PREENCHIDA - inicio
				IF @PRODUTO_ID IN (200,201,202,203,204,205,206,207,208,209,210,211,212,225,224)
				BEGIN
					--VALIDANDO PROPOSTA COM ENDOSSO DE RENOVACAO (203) - Inicio
					--INSERINDO VIGENCIA DOS ENDOSSOS DE RENOVA��O				
					IF OBJECT_ID('TEMPDB..#VIGENCIA_ENDOSSO') IS NOT NULL
					BEGIN
						DROP TABLE #VIGENCIA_ENDOSSO
					END

					CREATE TABLE #VIGENCIA_ENDOSSO (
						 ID INT IDENTITY(1, 1) NOT NULL PRIMARY KEY
						,DT_FIM_VIGENCIA_END SMALLDATETIME NULL
						,PROPOSTA_ID INT NULL
						)

					INSERT INTO #VIGENCIA_ENDOSSO (
						 DT_FIM_VIGENCIA_END
						,PROPOSTA_ID
						)
				   SELECT TOP (1) ISNULL(DT_FIM_VIGENCIA_END, GETDATE() + 1)
						 ,PROPOSTA_ID
					 FROM SEGUROS_DB.DBO.ENDOSSO_TB ENDOSSO_TB WITH (NOLOCK)
					WHERE ENDOSSO_TB.TP_ENDOSSO_ID = 203
					  AND ENDOSSO_TB.PROPOSTA_ID = @PROPOSTA_ID
			     ORDER BY ENDOSSO_TB.ENDOSSO_ID 
				     DESC
					 
					--TRATAMENTO PARA PROPOSTAS BESC SEM ENDOSSO 203 (DEVEM SER INDEFERIDAS)
					DECLARE @DT_FIM_VIGENCIA_END SMALLDATETIME
					SELECT @DT_FIM_VIGENCIA_END = DT_FIM_VIGENCIA_END FROM #VIGENCIA_ENDOSSO
					--SE O ULTIMO ENDOSSO NAO TEM FIM DE VIGENCIA ENTAO INDEFERE 
					IF @DT_FIM_VIGENCIA_END IS NULL
					BEGIN
					   UPDATE A
						  SET A.DT_FIM_VIGENCIA = A.DT_INICIO_VIGENCIA
						 FROM #VIGENCIA A
						WHERE A.PROPOSTA_ID = @PROPOSTA_ID  
					END
					ELSE
					BEGIN
						UPDATE A
						   SET A.DT_FIM_VIGENCIA = VIGENCIA_ENDOSSO.DT_FIM_VIGENCIA_END
						  FROM #VIGENCIA A
					INNER JOIN #VIGENCIA_ENDOSSO VIGENCIA_ENDOSSO
							ON A.PROPOSTA_ID = VIGENCIA_ENDOSSO.PROPOSTA_ID
					END
					--VALIDANDO PROPOSTA COM ENDOSSO DE RENOVACAO (203) - Fim
				
					--DEFINIR SE AS PROPOSTAS DOS PRODUTOS BESC ESTAO ADIMPLENTES - INICO
					--TROCAR A DATA DE FIM DE VIGENCIA CASO EXISTA ALGUMA INADIMPLENCIA
					IF EXISTS (
						SELECT TOP (1) 1
						  FROM SEGUROS_DB.DBO.AGENDAMENTO_COBRANCA_ATUAL_TB COBRANCA WITH (NOLOCK)
						 WHERE COBRANCA.PROPOSTA_ID = @PROPOSTA_ID
						   AND COBRANCA.DT_AGENDAMENTO < @DT_OCORRENCIA -- cobran�as vencidas  
						   AND ((COBRANCA.SITUACAO = 'p' AND COBRANCA.CANC_ENDOSSO_ID IS NULL) -- cobran�as pendente n�o canceladas 
							OR COBRANCA.SITUACAO = 'i') --cobran�as inadimpletes n�o canceladas
					) 
					BEGIN
						UPDATE A
						   SET A.DT_FIM_VIGENCIA = (
								SELECT MAX(DT_AGENDAMENTO)
								FROM SEGUROS_DB.DBO.AGENDAMENTO_COBRANCA_ATUAL_TB COBRANCA WITH (NOLOCK)
								WHERE COBRANCA.PROPOSTA_ID = @PROPOSTA_ID
								  AND COBRANCA.DT_AGENDAMENTO < @DT_OCORRENCIA -- cobran�as vencidas  
								  AND ((COBRANCA.SITUACAO = 'p' AND COBRANCA.CANC_ENDOSSO_ID IS NULL) -- cobran�as pendente n�o canceladas 
								   OR COBRANCA.SITUACAO = 'i') --cobran�as inadimpletes n�o canceladas
						)
						FROM #VIGENCIA A
						WHERE A.PROPOSTA_ID = @PROPOSTA_ID
					END
					
					IF NOT EXISTS 
						(SELECT TOP (1) 1
						   FROM SEGUROS_DB.DBO.AGENDAMENTO_COBRANCA_ATUAL_TB COBRANCA WITH (NOLOCK) 
						  WHERE COBRANCA.PROPOSTA_ID = @PROPOSTA_ID
						)
					BEGIN
						UPDATE A
						   SET A.DT_FIM_VIGENCIA = A.DT_INICIO_VIGENCIA
						  FROM #VIGENCIA A
						 WHERE A.PROPOSTA_ID = @PROPOSTA_ID
					END
					--DEFINIR SE AS PROPOSTAS DOS PRODUTOS BESC ESTAO ADIMPLENTES - INICO
					
						
					
				END
				--DEFINIR A VIGENCIA DAS PROPOSTAS DOS PRODUTOS BESC QUE NAO POSSUEM DT_FIM_VIGENCIA PREENCHIDA - inicio

												
				--VALIDACAO FINAL DA VIGENCIA(INCLUI ADIMPLENCIA), CANCELAMENTO E ENDOSSO
				--SE A PROPOSTA NAO ESTA VIGENTE NA DATA DE OCORRENCIA
				IF NOT EXISTS (
						SELECT TOP (1) 1
						FROM #VIGENCIA V
						WHERE V.VIGENCIA = 'A'
							AND @DT_OCORRENCIA BETWEEN V.DT_INICIO_VIGENCIA
								AND ISNULL(V.DT_FIM_VIGENCIA, GETDATE() + 1) --CASO A PROPOSTA NAO ESTEJA CANCELADA O FIM DE VIGENCIA PODE SER NULL
						)
				--OU SE EXISTE ALGUM CANCELAMENTO NA DATA DE OCORRENCIA
				OR EXISTS (
							SELECT TOP (1) 1
							  FROM #VIGENCIA_CANCELAMENTO VC
							 WHERE @DT_OCORRENCIA 
						   BETWEEN VC.DT_INICIO_CANCELAMENTO
						       AND VC.DT_FIM_CANCELAMENTO
					      )
				BEGIN
					--RECUSA (SEM VIG�NCIA)  
					SELECT @Dt_ini = v.dt_inicio_vigencia
						,@Dt_fim = v.dt_fim_vigencia
					FROM #vigencia v
					WHERE v.vigencia = 'A'

					SET @PgtoImediato = 0

					--GRAVA��O DETALHAMENTO
					INSERT INTO #DETALHAMENTO (linha)
					SELECT 'Pagamento imediato negado:'

					INSERT INTO #DETALHAMENTO (linha)
					SELECT 'A data de ocorr�ncia ' + FORMAT(@dt_ocorrencia, 'dd/MM/yyyy') + ' do aviso'

					INSERT INTO #DETALHAMENTO (linha)
					SELECT 'n�o esta dentro do per�odo de vig�ncia da ap�lice.'

					IF @Dt_ini IS NOT NULL
						-- AND @Dt_fim IS NOT NULL
					BEGIN
						INSERT INTO #DETALHAMENTO (linha)
						SELECT convert(VARCHAR(20), @dt_ini, 103) + ' � ' + convert(VARCHAR(20), @dt_fim, 103)
					END

					INSERT INTO #DETALHAMENTO (linha)
					SELECT ''
				END
				ELSE
				BEGIN
					INSERT INTO #DETALHAMENTO (linha)
					SELECT 'A data de ocorr�ncia do evento ' + FORMAT(@dt_ocorrencia, 'dd/MM/yyyy')

					INSERT INTO #DETALHAMENTO (linha)
					SELECT 'est� dentro do per�odo de vig�ncia da proposta.'

					INSERT INTO #DETALHAMENTO (linha)
					SELECT ''
					
				END
						
			END

			-----------------------------------------------------------------------------------------------------------------------------------------------------  
			--###################################################################################################################################################  
			--FIM DA CLASSIFICA��O  
			-----------------------------------------------------------------------------------------------------------------------------------------------------  
			IF @PgtoImediato = 1
			BEGIN
				--ATUALIZA��O DA CLASSIFICA��O
				UPDATE A
				SET LINHA = 'O SINISTRO FOI CLASSIFICADO COMO DEFERIDO'
				FROM #DETALHAMENTO A
				WHERE ID = 6
			END
			ELSE
			BEGIN
				--ATUALIZA��O DA CLASSIFICA��O
				UPDATE A
				SET LINHA = 'O SINISTRO FOI CLASSIFICADO COMO INDEFERIDO'
				FROM #DETALHAMENTO A
				WHERE ID = 6
			END

			--FINALIZANDO A CRIACAO DO DETALHAMENTO
			INSERT INTO #DETALHAMENTO (linha)
			SELECT ''

			INSERT INTO #DETALHAMENTO (linha)
			SELECT '--------------------------'
		END

		DECLARE @MAX_SEQ_DETALHE INT
			,@MAX_LINHA_DETALHE INT
			,@TP_DETALHAMENTO INT

		--' TP_DETALHAMENTO:
		--' 0- Anota��o
		--' 1- Exig�ncia
		--' 2- Recibo
		--' 3- Indeferimento "A Exig�ncia dever� ser emitida Manualmente no SEGUR."
		--' 4- Solicita��o de cancelamento
		--' 5- Comunica��es feitas pelo GTR
		SET @TP_DETALHAMENTO = 0

		--INICIANDO A GRAVA��O DO DETALHAMENTO
		IF @debug = 0 AND @PgtoImediato = 1
		BEGIN
			IF @evento_id IS NOT NULL
			BEGIN
				--PRODUTOS AVISADOS PELO SEGP0794 GRAVAM NAS TABELAS DE EVENTO DO SINISTRO 
				SELECT @MAX_SEQ_DETALHE = MAX(seq_detalhe)
				FROM SEGUROS_DB.DBO.evento_SEGBR_sinistro_detalhe_atual_tb WITH (NOLOCK)
				WHERE EVENTO_ID = @evento_id

				--GRAVANDO O DETALHAMENTO
				INSERT INTO SEGUROS_DB.DBO.evento_SEGBR_sinistro_detalhe_atual_tb (
					evento_id
					,tp_detalhe
					,seq_detalhe
					,descricao
					,usuario
					,dt_inclusao
					)
				SELECT @evento_id AS evento_id
					,'ANOTACAO' AS tp_detalhe
					,ISNULL(@MAX_SEQ_DETALHE, 0) + ID AS seq_detalhe
					,LINHA AS descricao
					,@usuario AS usuario
					,GETDATE() AS dt_inclusao
				FROM #DETALHAMENTO WITH (NOLOCK)
			END
			ELSE
			BEGIN
				IF @SINISTRO_ID IS NOT NULL
				BEGIN
					--PRODUTOS AVISADOS PELO SEGP1296 GRAVAM DIRETO NAS TABELAS DE SINISTRO
					--GRAVANDO DIRETO NO DETALHAMENTO
					DECLARE @APOLICE_ID INT
						,@SUCURSAL_ID INT
						,@SEGURADORA_ID INT
						,@DETALHAMENTO_ID INT
						,@DT_SISTEMA SMALLDATETIME
						,@RESTRITO CHAR(1)    
					
					SET @RESTRITO = 'S'
					
					SELECT @APOLICE_ID = APOLICE_ID
						,@SUCURSAL_ID = sucursal_seguradora_id
						,@SEGURADORA_ID = seguradora_cod_susep
					FROM SEGUROS_DB.DBO.SINISTRO_TB WITH (NOLOCK)
					WHERE SINISTRO_ID = @SINISTRO_ID --82202000021

					SELECT @DT_SISTEMA = DT_OPERACIONAL
					FROM SEGUROS_DB.DBO.PARAMETRO_GERAL_TB WITH (NOLOCK)

					--GERA��O DO DETALHAMENTO ID
					EXEC @detalhamento_id = SEGUROS_DB.DBO.SINISTRO_DETALHAMENTO_SPI @SINISTRO_ID
						,@APOLICE_ID
						,@SUCURSAL_ID
						,@SEGURADORA_ID
						,@RAMO_ID
						,@DT_SISTEMA
						,@TP_DETALHAMENTO
						,@RESTRITO
						,@USUARIO
						
						

					SELECT @MAX_LINHA_DETALHE = MAX(LINHA_ID)
					  FROM SEGUROS_DB.DBO.SINISTRO_LINHA_DETALHAMENTO_TB WITH (NOLOCK)
					 WHERE SINISTRO_ID = @SINISTRO_ID -- 82202000020
					   AND DETALHAMENTO_ID = @DETALHAMENTO_ID

					--GRAVANDO O DETALHAMENTO (sinistro_linha_detalhe_spi)
					INSERT INTO SEGUROS_DB.DBO.SINISTRO_LINHA_DETALHAMENTO_TB (
						SINISTRO_ID
						,APOLICE_ID
						,SUCURSAL_SEGURADORA_ID
						,SEGURADORA_COD_SUSEP
						,RAMO_ID
						,DETALHAMENTO_ID
						,LINHA_ID
						,LINHA
						,DT_INCLUSAO
						,USUARIO
						)
					SELECT @SINISTRO_ID
						,@APOLICE_ID
						,@SUCURSAL_ID
						,@SEGURADORA_ID
						,@RAMO_ID
						,@DETALHAMENTO_ID
						,ISNULL(@MAX_LINHA_DETALHE, 0) + ID
						,LINHA
						,GETDATE()
						,@USUARIO
					FROM #DETALHAMENTO WITH (NOLOCK)
				END
			END

			--RETORNO DA CLASSIFICA��O
			SELECT @PgtoImediato AS PagtoImediato
				,@tp_sinistro_parametro_id AS tp_sinistro_parametro_id
		END
		ELSE
		BEGIN
			SELECT @PGTOIMEDIATO AS PAGTOIMEDIATO
				,@TP_SINISTRO_PARAMETRO_ID AS TP_SINISTRO_PARAMETRO_ID
				,@EVENTO_ID AS EVENTO_ID
				,@SINISTRO_ID AS SINISTRO_ID
				,'ANOTACAO' AS TP_DETALHE
				,@TP_DETALHAMENTO AS TP_DETALHAMENTO
				,ISNULL(@MAX_SEQ_DETALHE, 0) + ID AS SEQ_DETALHE
				,LINHA AS DESCRICAO
				,@USUARIO AS USUARIO
				,GETDATE() AS DT_INCLUSAO
			FROM #DETALHAMENTO WITH (NOLOCK)
		END

		-- (fim) Bloco de codifica��o da procedure  
		-----------      
		RETURN
	END TRY

	BEGIN CATCH
		DECLARE @ErrorMessage NVARCHAR(4000)
		DECLARE @ErrorSeverity INT
		DECLARE @ErrorState INT

		SELECT @ErrorMessage = ERROR_PROCEDURE() + ' - Linha ' + CONVERT(VARCHAR(15), ERROR_LINE()) + ' - ' + ERROR_MESSAGE()
			,@ErrorSeverity = ERROR_SEVERITY()
			,@ErrorState = ERROR_STATE()

		RAISERROR (
				@ErrorMessage
				,@ErrorSeverity
				,@ErrorState
				)
	END CATCH
END
GO


