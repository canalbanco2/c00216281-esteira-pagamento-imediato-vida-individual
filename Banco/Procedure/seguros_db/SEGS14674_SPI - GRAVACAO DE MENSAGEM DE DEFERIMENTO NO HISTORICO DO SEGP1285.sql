CREATE PROCEDURE dbo.SEGS14674_SPI 
@SINISTRO_ID   NUMERIC(11,0),    
@APOLICE_ID  NUMERIC(9,0),    
@SUCURSAL_SEGURADORA_ID NUMERIC(5,0),    
@SEGURADORA_COD_SUSEP NUMERIC(5,0),    
@RAMO_ID  INT,         
@USUARIO  VARCHAR(20) 
  
AS     
    
 /**************************************************************************************************    
 Data Cria��o  : 21/02/2020 
 Projeto       : c00216281-esteira-pagamento-imediato-vida-individual    
 Autores       : Ricardo Morais    
 Descri��o     : Procedure criada para inserir item no hist�rico de acordo com a classifica��o do sinistro    
 '**************************************************************************************************/    
 
 -- BLOCO DE TESTE 
/*   
 DECLARE @SINISTRO_ID   NUMERIC(11,0),    
		 @APOLICE_ID  NUMERIC(9,0),    
		 @SUCURSAL_SEGURADORA_ID NUMERIC(5,0),    
		 @SEGURADORA_COD_SUSEP NUMERIC(5,0),    
		 @RAMO_ID  INT,         
		 @USUARIO  VARCHAR(20) 

  BEGIN TRAN
    IF @@TRANCOUNT > 0 
		 EXEC seguros_db.dbo.SEGS14674_SPI 
		 @SINISTRO_ID = 77201903829 ,
		 @APOLICE_ID = 31121,
		 @USUARIO = 'c00216281',
		 @SUCURSAL_SEGURADORA_ID = 0 ,
		 @SEGURADORA_COD_SUSEP = 6785,
		 @RAMO_ID = 77
    ELSE 
		SELECT 'Erro. A transa��o n�o foi aberta para executar o teste.'
  ROLLBACK  
*/

BEGIN
	SET NOCOUNT ON

	BEGIN TRY
		DECLARE @SEQ_EVENTO INT 
		DECLARE @TP_SINISTRO_PARAMETRO_ID INT
		DECLARE @RESULTADO INT
		DECLARE @ITEM   VARCHAR(100)
		SET @ITEM = 'Sinistro Deferido'
		
		        --Pegando o SEQ_EVENTO dos eventos
				--Aviso de Sinistro pela Central de Atendimento - 10142 (SMQP0059)
				--Abertura de Sinistro - 10002 (SEGP1287)
				SELECT @SEQ_EVENTO = MAX(SEQ_EVENTO)
				  FROM SEGUROS_DB.DBO.SINISTRO_HISTORICO_TB  WITH(NOLOCK)     
				 WHERE SINISTRO_ID = @SINISTRO_ID 
				   AND EVENTO_SEGBR_ID IN (10142,10002)
				   
			   SELECT @TP_SINISTRO_PARAMETRO_ID = TP_SINISTRO_PARAMETRO_ID
		         FROM SEGUROS_DB.DBO.TP_SINISTRO_PARAMETRO_TB WITH(NOLOCK) 
		        WHERE NOME = 'Esteira Pagamento Imediato � Vida Individual'
				
				SELECT @RESULTADO = RESULTADO
				  FROM SEGUROS_DB.DBO.SINISTRO_CLASSIFICACAO_PARAMETRO_TB SINISTRO_CLASSIFICACAO_PARAMETRO_TB WITH(NOLOCK)
				 WHERE SINISTRO_CLASSIFICACAO_PARAMETRO_TB.TP_SINISTRO_PARAMETRO_ID = @TP_SINISTRO_PARAMETRO_ID
				   AND SINISTRO_CLASSIFICACAO_PARAMETRO_TB.SINISTRO_ID = @SINISTRO_ID
				   
				IF @RESULTADO = 1 AND @SEQ_EVENTO IS NOT NULL
				BEGIN
					exec seguros_db.dbo.sinistro_item_historico_spi      
						  @sinistro_id = @sinistro_id      
						 ,@apolice_id = @apolice_id      
						 ,@sucursal_seguradora_id = @sucursal_seguradora_id      
						 ,@seguradora_cod_susep = @seguradora_cod_susep      
						 ,@ramo_id = @ramo_id      
						 ,@seq_evento = @seq_evento      
						 ,@item = @item      
						 ,@usuario = @usuario      
		 
				END
				
	END TRY

	BEGIN CATCH
		DECLARE @ErrorMessage NVARCHAR(4000)
		DECLARE @ErrorSeverity INT
		DECLARE @ErrorState INT

		SELECT @ErrorMessage = ERROR_PROCEDURE() + ' - Linha ' + CONVERT(VARCHAR(15), ERROR_LINE()) + ' - ' + ERROR_MESSAGE()
			,@ErrorSeverity = ERROR_SEVERITY()
			,@ErrorState = ERROR_STATE()

		RAISERROR (
				@ErrorMessage
				,@ErrorSeverity
				,@ErrorState
				)
	END CATCH
END	
GO


 









