CREATE PROC dbo.SEGS14652_SPI (
			@sinistro_id NUMERIC(11,0),
			@resultado INT,
			@tp_sinistro_parametro_id INT,
			@usuario VARCHAR(20))
			
AS

/*
	Sergio Ricardo (nova consultoria) - 30/01/2020
	Demanda: 00210408-esteira_flash	
	Descri��o: procedure para inserir o resultado da classifica��o do aviso de sinistro
	Banco: seguros_db	
*/
-- BLOCO DE TESTE 
/*   
  DECLARE @sinistro_id NUMERIC(11,0),
		  @resultado INT,
		  @usuario VARCHAR(20)
		  
  SELECT @sinistro_id = 50575976,
		 @resultado = 1,
		 @usuario = '00210408'

  BEGIN TRAN
    IF @@TRANCOUNT > 0 
		 EXEC seguros_db.dbo.SEGS14652_SPI @sinistro_id, @resultado, @usuario
    ELSE 
		SELECT 'Erro. A transa��o n�o foi aberta para executar o teste.'
  ROLLBACK  
*/
BEGIN
	SET NOCOUNT ON

	BEGIN TRY

		 INSERT INTO seguros_db.dbo.sinistro_classificacao_parametro_tb (tp_sinistro_parametro_id,
																		 sinistro_id, 
																	     resultado, 
																	     dt_inclusao, 
																	     usuario) 
																  SELECT @tp_sinistro_parametro_id,
																		 @sinistro_id,
																	     @resultado,
																	     GETDATE(),
																	     @usuario
															   
			
		 RETURN
	END TRY

	BEGIN CATCH
		DECLARE @ErrorMessage NVARCHAR(4000)
		DECLARE @ErrorSeverity INT
		DECLARE @ErrorState INT

		SELECT @ErrorMessage = ERROR_PROCEDURE() + ' - Linha ' + CONVERT(VARCHAR(15), ERROR_LINE()) + ' - ' + ERROR_MESSAGE()
			,@ErrorSeverity = ERROR_SEVERITY()
			,@ErrorState = ERROR_STATE()

		RAISERROR (
				@ErrorMessage
				,@ErrorSeverity
				,@ErrorState
				)
	END CATCH
END	