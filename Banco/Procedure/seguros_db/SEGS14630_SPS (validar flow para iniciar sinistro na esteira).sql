CREATE PROCEDURE dbo.SEGS14630_SPS (
	@proposta_id NUMERIC(11, 0)
	,@evento_id INT
	,@cobertura_id INT
	,@sinistro_id NUMERIC(11, 0) = NULL
	)
AS
/*
    Nova consultoria - 26/12/2019
	Demanda: C00216281-ESTEIRA-PAGAMENTO-IMEDIATO-VIDA-INDIVIDUAL
	Descri��o: procedure para validar se o aviso de sinistro � passivel da esteira flash, ou se o mesmo j� foi classificado.
	Banco: seguros_db	
*/
-- BLOCO DE TESTE 
/*   
  DECLARE @proposta_id NUMERIC(11,0),
		  @evento_id INT,
		  @cobertura_id INT
		  
  SELECT @proposta_id = 50575976,
		 @evento_id = 300,
		 @cobertura_id = 498

  BEGIN TRAN
    IF @@TRANCOUNT > 0 
		 EXEC seguros_db.dbo.SEGS14630_SPS @proposta_id, @evento_id, @cobertura_id
    ELSE 
		SELECT 'Erro. A transa��o n�o foi aberta para executar o teste.'
  ROLLBACK  

*/
BEGIN
	SET NOCOUNT ON

	BEGIN TRY
		DECLARE @VALIDA_FLOW INT

		SET @VALIDA_FLOW = 0

		SELECT @VALIDA_FLOW = COUNT(s.sinistro_parametro_chave_id)
		FROM seguros_db.dbo.proposta_tb p WITH (NOLOCK)
		INNER JOIN seguros_db.dbo.sinistro_parametro_chave_tb s WITH (NOLOCK)
			ON p.produto_id = s.produto_id
				AND p.ramo_id = s.ramo_id
		WHERE p.proposta_id = @proposta_id
			AND s.evento_sinistro_id = @evento_id
			AND s.tp_cobertura_id = @cobertura_id
			AND s.dt_fim_vigencia IS NULL

		--C00216281-ESTEIRA-PAGAMENTO-IMEDIATO-VIDA-INDIVIDUAL
		--VALIDA SE O SINISTRO JA FOI CLASSIFICADO, EM CASO POSITIVO N�O CLASSIFICA NOVAMENTE
		IF @VALIDA_FLOW <> 0
			AND @sinistro_id IS NOT NULL
		BEGIN
			IF EXISTS (
					SELECT 1
					FROM seguros_db.dbo.sinistro_classificacao_parametro_tb WITH (NOLOCK)
					WHERE SINISTRO_ID = @sinistro_id
					)
			BEGIN
				SET @VALIDA_FLOW = 0
			END
		END

		--C00216281 - FIM
		SELECT @VALIDA_FLOW AS VALIDA_FLOW

		RETURN
	END TRY

	BEGIN CATCH
		DECLARE @ErrorMessage NVARCHAR(4000)
		DECLARE @ErrorSeverity INT
		DECLARE @ErrorState INT

		SELECT @ErrorMessage = ERROR_PROCEDURE() + ' - Linha ' + CONVERT(VARCHAR(15), ERROR_LINE()) + ' - ' + ERROR_MESSAGE()
			,@ErrorSeverity = ERROR_SEVERITY()
			,@ErrorState = ERROR_STATE()

		RAISERROR (
				@ErrorMessage
				,@ErrorSeverity
				,@ErrorState
				)
	END CATCH
END
GO


