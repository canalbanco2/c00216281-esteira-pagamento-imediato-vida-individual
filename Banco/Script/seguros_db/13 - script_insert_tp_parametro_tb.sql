/*-----------------------------------------------------------------------------------------------------            
	AUTOR: RICARDO (NTENDENCIA)
	DEMANDA: 00210408-esteira_flash
	TIPO: Script
	NOME: script_insert_tp_parametro_tb
	OBJETIVO: Script para inserir na tp_parametro_tb parametriacao de pgto imediato
	BANCO: seguros_db	
	BASE: AB/ABS    
*/-----------------------------------------------------------------------------------------------------  
DECLARE @total_registro INT
DECLARE @total_registro_afetados INT
DECLARE @mensagem VARCHAR(500)
DECLARE @usuario VARCHAR(20)
DECLARE @tp_sinistro_parametro_id INT

SELECT @total_registro_afetados = 0

/*qtd registro que serao afetados*/
SELECT @total_registro = 35

SELECT @usuario = 'C00216281'

---========================================
--tp_sinistro_parametro_tb
---========================================


	INSERT INTO seguros_db.dbo.tp_sinistro_parametro_tb ( nome, msg_parametro_classificacao, dt_inclusao, usuario )
	     VALUES ( 'Esteira Pagamento Imediato � Vida Individual', 'Deferido Autom�tico', GETDATE(), @USUARIO )
		    SET @total_registro_afetados= @total_registro_afetados + @@rowcount 
		--01 registro

	SELECT @tp_sinistro_parametro_id  = tp_sinistro_parametro_id 
	  FROM seguros_db.dbo.tp_sinistro_parametro_tb 
	 WHERE NOME = 'Esteira Pagamento Imediato � Vida Individual'

   INSERT INTO seguros_db.dbo.sinistro_parametro_chave_tb (tp_sinistro_parametro_id,produto_id,ramo_id,evento_sinistro_id,tp_cobertura_id,dt_ini_vigencia,dt_inclusao,usuario)
        VALUES 

( @tp_sinistro_parametro_id,136,	93,	28,	     5  ,getdate(),getdate(),@usuario),
( @tp_sinistro_parametro_id,200,	93,	28,	     5  ,getdate(),getdate(),@usuario),
( @tp_sinistro_parametro_id,201,	93,	28,	     5  ,getdate(),getdate(),@usuario),
( @tp_sinistro_parametro_id,202,	93,	28,	     5  ,getdate(),getdate(),@usuario),
( @tp_sinistro_parametro_id,203,	93,	28,	     5  ,getdate(),getdate(),@usuario),
( @tp_sinistro_parametro_id,204,	93,	28,	     5  ,getdate(),getdate(),@usuario),
( @tp_sinistro_parametro_id,205,	93,	28,	     5  ,getdate(),getdate(),@usuario),                     
( @tp_sinistro_parametro_id,206,	93,	28,	     5  ,getdate(),getdate(),@usuario),
( @tp_sinistro_parametro_id,207,	93,	28,	     5  ,getdate(),getdate(),@usuario),
( @tp_sinistro_parametro_id,208,	93,	28,	     5  ,getdate(),getdate(),@usuario),
( @tp_sinistro_parametro_id,209,	93,	28,	     5  ,getdate(),getdate(),@usuario),
( @tp_sinistro_parametro_id,210,	93,	28,	     5  ,getdate(),getdate(),@usuario),
( @tp_sinistro_parametro_id,211,	93,	28,	     5  ,getdate(),getdate(),@usuario),
( @tp_sinistro_parametro_id,212,	93,	28,	     5  ,getdate(),getdate(),@usuario),
--fora de estimativa - inicio
( @tp_sinistro_parametro_id,225,	82,	28,	     5  ,getdate(),getdate(),@usuario),
--fora de estimativa - fim
( @tp_sinistro_parametro_id,224,	93,	28,	     5  ,getdate(),getdate(),@usuario),
( @tp_sinistro_parametro_id,1183,	77,	28,	     5  ,getdate(),getdate(),@usuario)
      -- 17 registros
SET @total_registro_afetados = @total_registro_afetados + @@rowcount

DECLARE @sinistro_parametro_regra_id int


SELECT @sinistro_parametro_regra_id = sinistro_parametro_regra_id FROM SEGUROS_DB.DBO.SINISTRO_PARAMETRO_REGRA_TB  where nome = 'verifica_vigencia_cobertura_apolice'



-- Cursor para inserir as regras de acordo com os parametros ja cadastrados na sinistro_parametro_tb
DECLARE @SINISTRO_PARAMETRO_CHAVE_ID int

DECLARE CURSOR_INSERE_REGRA CURSOR LOCAL FAST_FORWARD FOR
    SELECT
        SINISTRO_PARAMETRO_CHAVE_ID
    FROM
        SEGUROS_DB.DBO.SINISTRO_PARAMETRO_CHAVE_TB
    WHERE
        TP_SINISTRO_PARAMETRO_ID = @TP_SINISTRO_PARAMETRO_ID
	ORDER BY 
	    SINISTRO_PARAMETRO_CHAVE_ID ASC
		

OPEN CURSOR_INSERE_REGRA

FETCH NEXT FROM CURSOR_INSERE_REGRA INTO @sinistro_parametro_chave_id

WHILE @@FETCH_STATUS = 0
BEGIN

	INSERT INTO SEGUROS_DB.DBO.SINISTRO_PARAMETRO_CHAVE_REGRA_TB (sinistro_parametro_chave_id,sinistro_parametro_regra_id,valor,operador,dt_inclusao,usuario)
		VALUES
		(@sinistro_parametro_chave_id,@sinistro_parametro_regra_id,'s',null,getdate(),@usuario)
		 SET @total_registro_afetados= @total_registro_afetados + @@rowcount 
    -- Lendo a pr�xima linha
    FETCH NEXT FROM CURSOR_INSERE_REGRA INTO @sinistro_parametro_chave_id
END

-- Fechando Cursor para leitura
CLOSE CURSOR_INSERE_REGRA

-- Desalocando o cursor
DEALLOCATE CURSOR_INSERE_REGRA 


--17 registros afetados
SELECT @total_registro as '@total_registro'
	,@total_registro_afetados as '@total_registro_afetados'

IF ((@@SERVERNAME = 'SISAB003' OR @@SERVERNAME = 'SISAS003\ABS')  AND @total_registro <> @total_registro_afetados) 
BEGIN
	SET @mensagem = 'qtd de registros que deveriam ser afetados: ' + convert(VARCHAR(10), @total_registro) + 'qtd de registros afetados: ' + convert(VARCHAR(10), @total_registro_afetados)

	RAISERROR (
			@mensagem
			,16
			,1
			)
END
GO


