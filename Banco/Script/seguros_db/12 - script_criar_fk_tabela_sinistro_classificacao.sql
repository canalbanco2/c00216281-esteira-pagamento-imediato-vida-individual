IF (OBJECT_ID('FK_tp_sinistro_parametro_X_sinistro_classificacao_parametro_T', 'F') IS NULL)
BEGIN
  ALTER TABLE [dbo].[sinistro_classificacao_parametro_tb]
	  WITH CHECK ADD CONSTRAINT [FK_tp_sinistro_parametro_X_sinistro_classificacao_parametro_T] FOREIGN KEY ([tp_sinistro_parametro_id]) REFERENCES [dbo].[tp_sinistro_parametro_tb]([tp_sinistro_parametro_id])


  ALTER TABLE [dbo].[sinistro_classificacao_parametro_tb] CHECK CONSTRAINT [FK_tp_sinistro_parametro_X_sinistro_classificacao_parametro_T]
END  
