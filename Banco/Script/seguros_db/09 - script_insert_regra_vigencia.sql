/*-----------------------------------------------------------------------------------------------------            
	AUTOR: RICARDO (NTENDENCIA)
	DEMANDA: 00210408-esteira_flash
	TIPO: Script
	NOME: script_insert_regra_vigencia
	OBJETIVO: Script para inserir nova regra de vigencia de cobertura e apolice
	BANCO: seguros_db	
	BASE: AB/ABS    
*/-----------------------------------------------------------------------------------------------------  
DECLARE @total_registro INT
DECLARE @total_registro_afetados INT
DECLARE @mensagem VARCHAR(500)
DECLARE @usuario VARCHAR(20)
DECLARE @tp_sinistro_parametro_id INT

SELECT @total_registro_afetados = 0

/*qtd registro que serao afetados*/
SELECT @total_registro = 82

SELECT @usuario = 'C00210408'

---========================================
--tp_sinistro_parametro_tb
---========================================

    SELECT TOP (1) @tp_sinistro_parametro_id  = tp_sinistro_parametro_id 
	  FROM seguros_db.dbo.tp_sinistro_parametro_tb order by tp_sinistro_parametro_id

	UPDATE seguros_db.dbo.tp_sinistro_parametro_tb 
	   SET NOME = 'Esteira Pagamento Imediato - Sinistros de Danos El�tricos Residenciais'
	 WHERE tp_sinistro_parametro_id = @tp_sinistro_parametro_id
		SET @total_registro_afetados= @total_registro_afetados + @@rowcount 

 

--inserindo regras
INSERT INTO SEGUROS_DB.DBO.SINISTRO_PARAMETRO_REGRA_TB (nome,descricao,tipo,dt_inclusao,usuario)
		VALUES
			('verifica_vigencia_cobertura_apolice','Campo que define se deve ser verificada se a proposta est� com ap�lice e cobertura vigente na data da ocorr�ncia','s ou n',getdate(),@usuario)
		 SET @total_registro_afetados= @total_registro_afetados + @@rowcount 
		     --6 registros

DECLARE @sinistro_parametro_regra_id_7 int

SELECT @sinistro_parametro_regra_id_7 = sinistro_parametro_regra_id FROM SEGUROS_DB.DBO.SINISTRO_PARAMETRO_REGRA_TB  where nome = 'verifica_vigencia_cobertura_apolice'


-- Cursor para inserir as regras de acordo com os parametros ja cadastrados na sinistro_parametro_tb
DECLARE @SINISTRO_PARAMETRO_CHAVE_ID int

DECLARE CURSOR_INSERE_REGRA CURSOR LOCAL FAST_FORWARD FOR
    SELECT
        SINISTRO_PARAMETRO_CHAVE_ID
    FROM
        SEGUROS_DB.DBO.SINISTRO_PARAMETRO_CHAVE_TB
    WHERE
        TP_SINISTRO_PARAMETRO_ID = @TP_SINISTRO_PARAMETRO_ID
	ORDER BY 
	    SINISTRO_PARAMETRO_CHAVE_ID ASC
		

OPEN CURSOR_INSERE_REGRA

FETCH NEXT FROM CURSOR_INSERE_REGRA INTO @sinistro_parametro_chave_id

WHILE @@FETCH_STATUS = 0
BEGIN

	INSERT INTO SEGUROS_DB.DBO.SINISTRO_PARAMETRO_CHAVE_REGRA_TB (sinistro_parametro_chave_id,sinistro_parametro_regra_id,valor,operador,dt_inclusao,usuario)
		VALUES
		(@sinistro_parametro_chave_id,@sinistro_parametro_regra_id_7,'s',null,getdate(),@usuario)
		 SET @total_registro_afetados= @total_registro_afetados + @@rowcount 
    -- Lendo a pr�xima linha
    FETCH NEXT FROM CURSOR_INSERE_REGRA INTO @sinistro_parametro_chave_id
END

-- Fechando Cursor para leitura
CLOSE CURSOR_INSERE_REGRA

-- Desalocando o cursor
DEALLOCATE CURSOR_INSERE_REGRA 


--1 registro afetado
SELECT @total_registro as '@total_registro'
	,@total_registro_afetados as '@total_registro_afetados'

IF ((@@SERVERNAME = 'SISAB003' OR @@SERVERNAME = 'SISAS003\ABS')  AND @total_registro <> @total_registro_afetados) 
BEGIN
	SET @mensagem = 'qtd de registros que deveriam ser afetados: ' + convert(VARCHAR(10), @total_registro) + 'qtd de registros afetados: ' + convert(VARCHAR(10), @total_registro_afetados)

	RAISERROR (
			@mensagem
			,16
			,1
			)
END
GO


