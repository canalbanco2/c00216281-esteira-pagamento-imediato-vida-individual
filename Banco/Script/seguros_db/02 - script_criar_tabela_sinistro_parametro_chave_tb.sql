/*-----------------------------------------------------------------------------------------------------            
	AUTOR: RICARDO (NTENDENCIA)
	DEMANDA: 00210408-esteira_flash
	TIPO: Script
	NOME: script_criar_tabela_sinistro_parametro_chave_tb
	OBJETIVO: Script para criar tabela que ira gravar os parametros de acordo com os nomes ja previamente cadastrados
	BANCO: seguros_db	
	BASE: AB/ABS    
*/-----------------------------------------------------------------------------------------------------  

USE [seguros_db]
GO
----------------------------------------------------------------------------------------------------- 


IF Object_id('sinistro_parametro_chave_tb', 'U') IS NULL
BEGIN
CREATE TABLE [dbo].[sinistro_parametro_chave_tb](
	[sinistro_parametro_chave_id] [int] IDENTITY(1,1) NOT NULL,
	[tp_sinistro_parametro_id] [int] NOT NULL,
	[produto_id] [int] NOT NULL,
	[ramo_id] [int] NOT NULL,
	[evento_sinistro_id] [int] NOT NULL,
	[tp_cobertura_id] [int] NOT NULL,
	[dt_ini_vigencia] [smalldatetime] NOT NULL,
	[dt_fim_vigencia] [smalldatetime] NULL,
	[dt_inclusao] [dbo].[UD_dt_inclusao] NOT NULL,
	[dt_alteracao] [dbo].[UD_dt_alteracao] NULL,
	[lock] [timestamp] NOT NULL,
	[usuario] [dbo].[UD_usuario] NULL,
 CONSTRAINT [PK_sinistro_parametro_chave] PRIMARY KEY CLUSTERED 
(
	[sinistro_parametro_chave_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) 
) 
END
GO
