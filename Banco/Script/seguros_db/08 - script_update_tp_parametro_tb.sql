/*-----------------------------------------------------------------------------------------------------            
	AUTOR: RICARDO (NTENDENCIA)
	DEMANDA: 00210408-esteira_flash
	TIPO: Script
	NOME: script_update_tp_parametro_tb
	OBJETIVO: Script para atualizar a regra de pre-analise para re-analise
	BANCO: seguros_db	
	BASE: AB/ABS    
*/-----------------------------------------------------------------------------------------------------  
DECLARE @total_registro INT
DECLARE @total_registro_afetados INT
DECLARE @mensagem VARCHAR(500)
DECLARE @usuario VARCHAR(20)


SELECT @total_registro_afetados = 0

/*qtd registro que serao afetados*/
SELECT @total_registro = 6

SELECT @usuario = 'C00210408'

UPDATE SEGUROS_DB.DBO.SINISTRO_PARAMETRO_REGRA_TB 
	  SET NOME = 'verifica_reanalise_sinistro',
	      DESCRICAO = 'Campo que define se deve ser verificado se o cliente j� silicitou reanalise de sinistro em qualquer momento',
		  USUARIO = @USUARIO
WHERE NOME = 'verifica_pre_analise'
SET @total_registro_afetados= @total_registro_afetados + @@rowcount 

UPDATE SEGUROS_DB.DBO.SINISTRO_PARAMETRO_REGRA_TB 
	  SET DESCRICAO = 'Campo que define se deve ser verificada restricao A��o Judicial/Indicio de Fraude',
		  USUARIO = @USUARIO
WHERE NOME = 'verifica_restricao'
SET @total_registro_afetados= @total_registro_afetados + @@rowcount 

UPDATE SEGUROS_DB.DBO.SINISTRO_PARAMETRO_REGRA_TB 
	  SET DESCRICAO = 'Valor da estimativa/Or�amento',
		  USUARIO = @USUARIO
WHERE NOME = 'valor_estimativa'
SET @total_registro_afetados= @total_registro_afetados + @@rowcount 


UPDATE SEGUROS_DB.DBO.SINISTRO_PARAMETRO_REGRA_TB 
	  SET DESCRICAO = 'Per�odo de relacionamento do cliente com a BrasilSeg em dias',
	      USUARIO = @USUARIO
WHERE NOME = 'periodo_relacionamento'
SET @total_registro_afetados= @total_registro_afetados + @@rowcount 

UPDATE SEGUROS_DB.DBO.SINISTRO_PARAMETRO_REGRA_TB 
	  SET DESCRICAO = 'Per�odo sem sinistro',
	      USUARIO = @USUARIO
WHERE NOME = 'periodo_sem_sinistro'
SET @total_registro_afetados= @total_registro_afetados + @@rowcount 

UPDATE SEGUROS_DB.DBO.SINISTRO_PARAMETRO_REGRA_TB 
	  SET DESCRICAO = 'Campo que define se deve ser verificado se o pagamento est� em dia',
	      USUARIO = @USUARIO
WHERE NOME = 'verifica_pgto_em_dia'
SET @total_registro_afetados= @total_registro_afetados + @@rowcount 


SELECT @total_registro as '@total_registro'
	  ,@total_registro_afetados as '@total_registro_afetados'

IF ((@@SERVERNAME = 'SISAB003' OR @@SERVERNAME = 'SISAS003\ABS')  AND @total_registro <> @total_registro_afetados) 

BEGIN
	SET @mensagem = 'qtd de registros que deveriam ser afetados: ' + convert(VARCHAR(10), @total_registro) + 'qtd de registros afetados: ' + convert(VARCHAR(10), @total_registro_afetados)

	RAISERROR (
			@mensagem
			,16
			,1
			)
END
GO



