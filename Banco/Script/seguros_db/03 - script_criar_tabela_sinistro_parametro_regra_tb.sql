/*-----------------------------------------------------------------------------------------------------            
	AUTOR: RICARDO (NTENDENCIA)
	DEMANDA: 00210408-esteira_flash
	TIPO: Script
	NOME: script_criar_tabela_SINISTRO_PARAMETRO_REGRA_TB
	OBJETIVO: Script para criar tabela que ira gravar as regras dos parametros
	BANCO: seguros_db	
	BASE: AB/ABS    
*/-----------------------------------------------------------------------------------------------------  

USE [seguros_db]
GO
----------------------------------------------------------------------------------------------------- 


IF Object_id('sinistro_parametro_regra_tb', 'U') IS NULL
BEGIN
CREATE TABLE [dbo].[sinistro_parametro_regra_tb](
	[sinistro_parametro_regra_id] [int] IDENTITY(1,1) NOT NULL,
	[nome] [varchar](40) NOT NULL,
	[descricao] [varchar](200) NULL,
	[tipo] [varchar](100) NOT NULL,
	[dt_inclusao] [dbo].[UD_dt_inclusao] NOT NULL,
	[dt_alteracao] [dbo].[UD_dt_alteracao] NULL,
	[lock] [timestamp] NOT NULL,
	[usuario] [dbo].[UD_usuario] NULL,
 CONSTRAINT [PK_sinistro_parametro_regra] PRIMARY KEY CLUSTERED 
(
	[sinistro_parametro_regra_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
)
END
GO
