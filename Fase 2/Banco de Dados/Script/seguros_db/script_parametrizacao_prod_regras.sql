/*-----------------------------------------------------------------------------------------------------            
	AUTOR: RICARDO (NTENDENCIA)
	DEMANDA: c00216281-esteira-pagamento-imediato-vida-individual - Fase 2
	TIPO: Script
	NOME: script_insert_tp_parametro_tb
	OBJETIVO: Script para inserir na tp_parametro_tb parametriacao de pgto imediato
	BANCO: seguros_db	
	BASE: AB/ABS    
*/-----------------------------------------------------------------------------------------------------  
DECLARE @total_registro INT
DECLARE @total_registro_afetados INT
DECLARE @mensagem VARCHAR(500)
DECLARE @usuario VARCHAR(20)
DECLARE @tp_sinistro_parametro_id INT

SELECT @total_registro_afetados = 0

/*qtd registro que serao afetados*/
SELECT @total_registro = 92

SELECT @usuario = 'C00216281'

---========================================
--tp_sinistro_parametro_tb
---========================================


INSERT INTO seguros_db.dbo.tp_sinistro_parametro_tb ( nome, dt_inclusao, usuario, msg_parametro_classificacao, tp_ramo_id,lista_destino_producao,lista_destino_homologacao,envia_relatorio_diario )
	     VALUES ( 'Esteira Pagamento Imediato � Vida Individual Fase 2', GETDATE(), @USUARIO,'Deferido Autom�tico',1,'cvieira@brasilseg.com.br; joabreu@brasilseg.com.br; dsaraujo@brasilseg.com.br; albborja@brasilseg.com.br; sinistrosdepessoas@brasilseg.com.br','vmorrone@brasilseg.com.br; lamsantos@brasilseg.com.br; rapaula@brasilseg.com.br; ARMARAUJO@brasilseg.com.br','S' )
		    SET @total_registro_afetados= @total_registro_afetados + @@rowcount 
		--01 registro

	SELECT @tp_sinistro_parametro_id  = tp_sinistro_parametro_id 
	  FROM seguros_db.dbo.tp_sinistro_parametro_tb 
	 WHERE NOME = 'Esteira Pagamento Imediato � Vida Individual Fase 2'

---========================================
--sinistro_parametro_chave_tb
---========================================

			INSERT INTO seguros_db.dbo.sinistro_parametro_chave_tb (tp_sinistro_parametro_id,produto_id,ramo_id,evento_sinistro_id,tp_cobertura_id,dt_ini_vigencia,dt_inclusao,usuario)
			VALUES 
			( @tp_sinistro_parametro_id,11,		77,	28,	     5  ,getdate(),getdate(),@usuario),
			( @tp_sinistro_parametro_id,12,		93,	28,	     5  ,getdate(),getdate(),@usuario),
			( @tp_sinistro_parametro_id,14,		77,	28,	     5  ,getdate(),getdate(),@usuario),
			( @tp_sinistro_parametro_id,14,		93,	28,	     5  ,getdate(),getdate(),@usuario),
			( @tp_sinistro_parametro_id,14,		98,	28,	     5  ,getdate(),getdate(),@usuario),
			( @tp_sinistro_parametro_id,121,	93,	28,	     5  ,getdate(),getdate(),@usuario),                     
			( @tp_sinistro_parametro_id,135,	93,	28,	     5  ,getdate(),getdate(),@usuario),
			( @tp_sinistro_parametro_id,716,	93,	28,	     5  ,getdate(),getdate(),@usuario),
			( @tp_sinistro_parametro_id,718,	77,	28,	     5  ,getdate(),getdate(),@usuario),
			( @tp_sinistro_parametro_id,721,	93,	28,	     5  ,getdate(),getdate(),@usuario),
			( @tp_sinistro_parametro_id,722,	98,	28,	     5  ,getdate(),getdate(),@usuario),
			( @tp_sinistro_parametro_id,1174,	93,	28,	     5  ,getdate(),getdate(),@usuario),
			( @tp_sinistro_parametro_id,1175,	93,	28,	     5  ,getdate(),getdate(),@usuario),
			( @tp_sinistro_parametro_id,1196,	93,	28,	     5  ,getdate(),getdate(),@usuario),
			( @tp_sinistro_parametro_id,1198,	77,	28,	     5  ,getdate(),getdate(),@usuario),
			( @tp_sinistro_parametro_id,1205,	77,	28,	     5  ,getdate(),getdate(),@usuario),
			( @tp_sinistro_parametro_id,1208,	77,	28,	     5  ,getdate(),getdate(),@usuario),
			( @tp_sinistro_parametro_id,1211,	77,	28,	     5  ,getdate(),getdate(),@usuario),
			--( @tp_sinistro_parametro_id,1217,	77,	28,	     5  ,getdate(),getdate(),@usuario),
			( @tp_sinistro_parametro_id,1235,	91,	28,	     5  ,getdate(),getdate(),@usuario),
			( @tp_sinistro_parametro_id,1236,	91,	28,	     5  ,getdate(),getdate(),@usuario),
			( @tp_sinistro_parametro_id,1237,	91,	28,	     5  ,getdate(),getdate(),@usuario)

			 SET @total_registro_afetados = @total_registro_afetados + @@rowcount   
			-- 21 registros


			--inserindo regras
			INSERT INTO SEGUROS_DB.DBO.SINISTRO_PARAMETRO_REGRA_TB (nome,descricao,tipo,dt_inclusao,usuario)
			VALUES
			('verifica_vigencia_diferenciada','Campo que define como deve ser verificada a vigencia','s ou n',getdate(),@usuario),
			--11,14,722
			('verifica_vigencia_normal','Campo que define como deve ser verificada a vigencia','s ou n',getdate(),@usuario),
			--12, 121, 135, 716, 1196, 1198, 1205, 1208, 1211, 1235, 1236, 1237
			('verifica_vigencia_SEGP1285','Campo que define como deve ser verificada a vigencia','s ou n',getdate(),@usuario),
			--721
			('verifica_vigencia_assembleia','Campo que define como deve ser verificada a vigencia','s ou n',getdate(),@usuario),
			--718
			('verifica_vigencia_proposta_bb_ant','Campo que define como deve ser verificada a vigencia','s ou n',getdate(),@usuario),
			--1174,1175
			('verifica_vigencia_1ano_x_valor_is','Regra de periodo de vigencia min 1 ano limitado ao valor da IS','numeric 15,2',getdate(),@usuario),
			--todos produtos
			('verifica_vigencia_60dias_x_valor_is','Regra de periodo de vigencia min 60 dias com acumulo de IS','s ou n',getdate(),@usuario)
			--todos produtos
			SET @total_registro_afetados= @total_registro_afetados + @@rowcount 
			--7 registros	

			--ASSOCIANDO REGRAS DE VIGENCIA A DETERMINADO GRUPO DE PRODUTOS
			DECLARE @sinistro_parametro_regra_vigencia_diferenciada int
			DECLARE @sinistro_parametro_regra_vigencia_normal int
			DECLARE @sinistro_parametro_regra_vigencia_SEGP1285 int
			DECLARE @sinistro_parametro_regra_vigencia_assembleia int
			DECLARE @sinistro_parametro_regra_vigencia_proposta_bb_ant int

			SELECT @sinistro_parametro_regra_vigencia_diferenciada = sinistro_parametro_regra_id FROM SEGUROS_DB.DBO.SINISTRO_PARAMETRO_REGRA_TB  where nome = 'verifica_vigencia_diferenciada'
			SELECT @sinistro_parametro_regra_vigencia_normal = sinistro_parametro_regra_id FROM SEGUROS_DB.DBO.SINISTRO_PARAMETRO_REGRA_TB  where nome = 'verifica_vigencia_normal'
			SELECT @sinistro_parametro_regra_vigencia_SEGP1285 = sinistro_parametro_regra_id FROM SEGUROS_DB.DBO.SINISTRO_PARAMETRO_REGRA_TB  where nome = 'verifica_vigencia_SEGP1285'
			SELECT @sinistro_parametro_regra_vigencia_assembleia = sinistro_parametro_regra_id FROM SEGUROS_DB.DBO.SINISTRO_PARAMETRO_REGRA_TB  where nome = 'verifica_vigencia_assembleia'
			SELECT @sinistro_parametro_regra_vigencia_proposta_bb_ant = sinistro_parametro_regra_id FROM SEGUROS_DB.DBO.SINISTRO_PARAMETRO_REGRA_TB  where nome = 'verifica_vigencia_proposta_bb_ant'
			
			--insert da primeira regra de vigencia_diferenciada
			DECLARE @SINISTRO_PARAMETRO_CHAVE_ID int		
			INSERT INTO SEGUROS_DB.DBO.SINISTRO_PARAMETRO_CHAVE_REGRA_TB (sinistro_parametro_chave_id,sinistro_parametro_regra_id,valor,operador,dt_inclusao,usuario)
			 SELECT	SINISTRO_PARAMETRO_CHAVE_ID, @sinistro_parametro_regra_vigencia_diferenciada, 's', null,getdate(),@usuario
			   FROM	SEGUROS_DB.DBO.SINISTRO_PARAMETRO_CHAVE_TB
			  WHERE TP_SINISTRO_PARAMETRO_ID = @TP_SINISTRO_PARAMETRO_ID
			  	AND PRODUTO_ID IN (11,14,722)
			SET @total_registro_afetados= @total_registro_afetados + @@rowcount 
			--5 registros
			
			--insert da primeira regra de vigencia_normal		
			INSERT INTO SEGUROS_DB.DBO.SINISTRO_PARAMETRO_CHAVE_REGRA_TB (sinistro_parametro_chave_id,sinistro_parametro_regra_id,valor,operador,dt_inclusao,usuario)
			 SELECT	SINISTRO_PARAMETRO_CHAVE_ID, @sinistro_parametro_regra_vigencia_normal, 's', null,getdate(),@usuario
			   FROM	SEGUROS_DB.DBO.SINISTRO_PARAMETRO_CHAVE_TB
			  WHERE TP_SINISTRO_PARAMETRO_ID = @TP_SINISTRO_PARAMETRO_ID
			  	AND PRODUTO_ID IN (12,121,135,716,1196,1198,1205,1208,1211,1235,1236,1237 )
			SET @total_registro_afetados= @total_registro_afetados + @@rowcount 
			--12 registros
			
			--insert da primeira regra de vigencia_SEGP1285		
			INSERT INTO SEGUROS_DB.DBO.SINISTRO_PARAMETRO_CHAVE_REGRA_TB (sinistro_parametro_chave_id,sinistro_parametro_regra_id,valor,operador,dt_inclusao,usuario)
			 SELECT	SINISTRO_PARAMETRO_CHAVE_ID, @sinistro_parametro_regra_vigencia_SEGP1285, 's', null,getdate(),@usuario
			   FROM	SEGUROS_DB.DBO.SINISTRO_PARAMETRO_CHAVE_TB
			  WHERE TP_SINISTRO_PARAMETRO_ID = @TP_SINISTRO_PARAMETRO_ID
			  	AND PRODUTO_ID = 721
			SET @total_registro_afetados= @total_registro_afetados + @@rowcount 
			--1 registros		

			--insert da primeira regra de vigencia_assembleia		
			INSERT INTO SEGUROS_DB.DBO.SINISTRO_PARAMETRO_CHAVE_REGRA_TB (sinistro_parametro_chave_id,sinistro_parametro_regra_id,valor,operador,dt_inclusao,usuario)
			 SELECT	SINISTRO_PARAMETRO_CHAVE_ID, @sinistro_parametro_regra_vigencia_assembleia, 's', null,getdate(),@usuario
			   FROM	SEGUROS_DB.DBO.SINISTRO_PARAMETRO_CHAVE_TB
			  WHERE TP_SINISTRO_PARAMETRO_ID = @TP_SINISTRO_PARAMETRO_ID
			  	AND PRODUTO_ID = 718
			SET @total_registro_afetados= @total_registro_afetados + @@rowcount 
			--1 registros	
			
			--insert da primeira regra de vigencia_proposta_bb_ant		
			INSERT INTO SEGUROS_DB.DBO.SINISTRO_PARAMETRO_CHAVE_REGRA_TB (sinistro_parametro_chave_id,sinistro_parametro_regra_id,valor,operador,dt_inclusao,usuario)
			 SELECT	SINISTRO_PARAMETRO_CHAVE_ID, @sinistro_parametro_regra_vigencia_proposta_bb_ant, 's', null,getdate(),@usuario
			   FROM	SEGUROS_DB.DBO.SINISTRO_PARAMETRO_CHAVE_TB
			  WHERE TP_SINISTRO_PARAMETRO_ID = @TP_SINISTRO_PARAMETRO_ID
			  	AND PRODUTO_ID IN (1174,1175)
			SET @total_registro_afetados= @total_registro_afetados + @@rowcount 
			--2 registros				
			
			
			--ASSOCIANDO REGRAS CRIADAS AOS VALORES SETADOS PARA ESTAS REGRAS 
			DECLARE @sinistro_parametro_regra_vigencia_1ano_x_valor_is int
			DECLARE @sinistro_parametro_regra_vigencia_60dias_x_valor_is int

			SELECT @sinistro_parametro_regra_vigencia_1ano_x_valor_is = sinistro_parametro_regra_id FROM SEGUROS_DB.DBO.SINISTRO_PARAMETRO_REGRA_TB  where nome = 'verifica_vigencia_1ano_x_valor_is'
			SELECT @sinistro_parametro_regra_vigencia_60dias_x_valor_is = sinistro_parametro_regra_id FROM SEGUROS_DB.DBO.SINISTRO_PARAMETRO_REGRA_TB  where nome = 'verifica_vigencia_60dias_x_valor_is'

			-- Cursor para inserir as regras de valores de acordo com os parametros ja cadastrados na sinistro_parametro_tb
			DECLARE CURSOR_INSERE_REGRA CURSOR LOCAL FAST_FORWARD FOR
				SELECT
					SINISTRO_PARAMETRO_CHAVE_ID
				FROM
					SEGUROS_DB.DBO.SINISTRO_PARAMETRO_CHAVE_TB
				WHERE
					TP_SINISTRO_PARAMETRO_ID = @TP_SINISTRO_PARAMETRO_ID
				ORDER BY 
					SINISTRO_PARAMETRO_CHAVE_ID ASC
		

			OPEN CURSOR_INSERE_REGRA

			FETCH NEXT FROM CURSOR_INSERE_REGRA INTO @sinistro_parametro_chave_id

			WHILE @@FETCH_STATUS = 0
			BEGIN

				INSERT INTO SEGUROS_DB.DBO.SINISTRO_PARAMETRO_CHAVE_REGRA_TB (sinistro_parametro_chave_id,sinistro_parametro_regra_id,valor,operador,dt_inclusao,usuario)
					VALUES
					(@sinistro_parametro_chave_id,@sinistro_parametro_regra_vigencia_1ano_x_valor_is,'200000.00','>=',getdate(),@usuario),
					(@sinistro_parametro_chave_id,@sinistro_parametro_regra_vigencia_60dias_x_valor_is,'s',null,getdate(),@usuario)
					 SET @total_registro_afetados= @total_registro_afetados + @@rowcount 
					 --42 registros
				-- Lendo a pr�xima linha
				FETCH NEXT FROM CURSOR_INSERE_REGRA INTO @sinistro_parametro_chave_id
			END

			-- Fechando Cursor para leitura
			CLOSE CURSOR_INSERE_REGRA

			-- Desalocando o cursor
			DEALLOCATE CURSOR_INSERE_REGRA 




SELECT @total_registro as '@total_registro'
	,@total_registro_afetados as '@total_registro_afetados'

IF ((@@SERVERNAME = 'SISAB003' OR @@SERVERNAME = 'SISAS003\ABS')  AND @total_registro <> @total_registro_afetados) 
BEGIN
	SET @mensagem = 'qtd de registros que deveriam ser afetados: ' + convert(VARCHAR(10), @total_registro) + 'qtd de registros afetados: ' + convert(VARCHAR(10), @total_registro_afetados)

	RAISERROR (
			@mensagem
			,16
			,1
			)
END
GO


